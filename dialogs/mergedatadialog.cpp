#include "mergedatadialog.h"

MergeDataDialog::MergeDataDialog(BaseDialog *parent, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices) :
    BaseDialog(parent, "merge data", "mergedatadialog"),
    d_baseAnnotatedMatrices(baseAnnotatedMatrices)
{

    BaseDialog::d_parameters = QSharedPointer<MergeDataParameters>(new MergeDataParameters);

    BaseDialog::initWidgets();

}

void MergeDataDialog::initWidgets_()
{

    d_itemSelectorWidget_baseAnnotedMatrices = new ItemSelectorWidget(this, "d_itemSelectorWidget_baseAnnotedMatrices");

    d_itemSelectorWidget_baseAnnotedMatrices->label()->setText("select data to merge");

    d_gridLayout->addWidget(d_itemSelectorWidget_baseAnnotedMatrices, 0, 0, 1, 1);

    QList<QString> labels;

    for (qsizetype i = 0; i < d_baseAnnotatedMatrices.count(); ++i)
        labels.append(d_baseAnnotatedMatrices.at(i)->property("label").toString());

    d_itemSelectorWidget_baseAnnotedMatrices->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(labels);

    this->connect(d_itemSelectorWidget_baseAnnotedMatrices->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemLabels); MergeDataParameters *mergeDataParameters = static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data()); mergeDataParameters->baseAnnotatedMatrices.clear(); for(qsizetype itemIndex : itemIndexes) mergeDataParameters->baseAnnotatedMatrices.append(d_baseAnnotatedMatrices.at(itemIndex));});


    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    d_orientationWidget->label()->setText("select merge orientation");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 1, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_mergeRowAnnotations = new ComboBoxWidget(this, "comboBoxWidget_mergeRowAnnotations");

    this->connect(comboBoxWidget_mergeRowAnnotations->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_mergeRowAnnotations](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->mergeRowAnnotations = (comboBoxWidget_mergeRowAnnotations->comboBox()->currentText() == "enable") ? true : false;});

    comboBoxWidget_mergeRowAnnotations->label()->setText("merge row annotations");

    comboBoxWidget_mergeRowAnnotations->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_mergeRowAnnotations->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_mergeRowAnnotations, 2, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_mergeColumnAnnotations = new ComboBoxWidget(this, "comboBoxWidget_annotatedMatrixTabToExport");

    this->connect(comboBoxWidget_mergeColumnAnnotations->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_mergeColumnAnnotations](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->mergeColumnAnnotations = (comboBoxWidget_mergeColumnAnnotations->comboBox()->currentText() == "enable") ? true : false;});

    comboBoxWidget_mergeColumnAnnotations->label()->setText("merge column annotations");

    comboBoxWidget_mergeColumnAnnotations->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_mergeColumnAnnotations->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_mergeColumnAnnotations, 3, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_addDataSpecificSuffixToIdentifiers = new ComboBoxWidget(this, "comboBoxWidget_addDataSpecificSuffixToIdentifiers");

    this->connect(comboBoxWidget_addDataSpecificSuffixToIdentifiers->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_addDataSpecificSuffixToIdentifiers](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->addDataSpecificSuffixtoIdentifiers = (comboBoxWidget_addDataSpecificSuffixToIdentifiers->comboBox()->currentText() == "enable") ? true : false;});

    comboBoxWidget_addDataSpecificSuffixToIdentifiers->label()->setText("add data specific suffix to identifiers");

    comboBoxWidget_addDataSpecificSuffixToIdentifiers->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_addDataSpecificSuffixToIdentifiers->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_addDataSpecificSuffixToIdentifiers, 4, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_addDataSpecificSuffixToAnnotationLabels = new ComboBoxWidget(this, "comboBoxWidget_addDataSpecificSuffixToAnnotationLabels");

    this->connect(comboBoxWidget_addDataSpecificSuffixToAnnotationLabels->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_addDataSpecificSuffixToAnnotationLabels](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->addDataSpecificSuffixtoAnnotationLabels = (comboBoxWidget_addDataSpecificSuffixToAnnotationLabels->comboBox()->currentText() == "enable") ? true : false;});

    comboBoxWidget_addDataSpecificSuffixToAnnotationLabels->label()->setText("add data specific suffix to annotation labels");

    comboBoxWidget_addDataSpecificSuffixToAnnotationLabels->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_addDataSpecificSuffixToAnnotationLabels->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_addDataSpecificSuffixToAnnotationLabels, 5, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_mergeMode = new ComboBoxWidget(this, "comboBoxWidget_mergeMode");

    this->connect(comboBoxWidget_mergeMode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_mergeMode](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->mergeMode = comboBoxWidget_mergeMode->comboBox()->currentText();});

    comboBoxWidget_mergeMode->label()->setText("merge mode");

    comboBoxWidget_mergeMode->comboBox()->addItems({"intersection", "union"});

    comboBoxWidget_mergeMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_mergeMode, 6, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_mergeSelectedOnly = new ComboBoxWidget(this, "comboBoxWidget_mergeSelectedOnly");

    this->connect(comboBoxWidget_mergeSelectedOnly->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_mergeSelectedOnly](){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->selectedOnly = comboBoxWidget_mergeSelectedOnly->comboBox()->currentData().toBool();});

    comboBoxWidget_mergeSelectedOnly->label()->setText("merge selection mode");

    comboBoxWidget_mergeSelectedOnly->comboBox()->addItem("merge selected only", true);

    comboBoxWidget_mergeSelectedOnly->comboBox()->addItem("merge all", false);

    comboBoxWidget_mergeSelectedOnly->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_mergeSelectedOnly, 7, 0, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<MergeDataParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 8, 0, 1, 1);

}
