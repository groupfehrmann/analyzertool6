#ifndef MATHUTILITYFUNCTIONS_H
#define MATHUTILITYFUNCTIONS_H

#include <QList>
#include <QHash>

#include <iterator>
#include <span>
#include <utility>
#include <stdlib.h>
#include <tuple>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    almost_equal(T x, T y, int ulp)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)

    return std::fabs(x-y) <= std::numeric_limits<T>::epsilon() * std::fabs(x+y) * static_cast<double>(ulp)
        // unless the result is subnormal
        || std::fabs(x-y) < std::numeric_limits<T>::min();
}

namespace MathUtilityFunctions {

    template <typename T> QList<std::span<T>> rowMajor_matrix1d_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount);

    template <typename T> QList<std::span<T>> rowMajor_matrix1d_rowSpans(std::vector<T> &rowMajor_matrix1d, qsizetype columnCount);

    template <typename T> QList<std::pair<qsizetype, std::span<T>>> rowMajor_matrix1d_rowIndexes_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount);

    template <typename T> QHash<qsizetype, std::span<T>> rowMajor_matrix1d_hash_rowIndexes_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount);

    void setNumberOfThreadsUsedForBLASandLAPACKroutines(int numberOfThreads);

    int getNumberOfThreadsUsedForBLASandLAPACKroutines();

    template <typename T> QList<std::span<T>> elementWiseDispatcher(QList<T> &vector, qsizetype numberOfThreads);

    QList<std::pair<qsizetype, qsizetype> > createIndexBlocksForMultiThreading(qsizetype sizeOfSequence, qsizetype numberOfThreads);

}

template <typename T>
QList<std::span<T>> MathUtilityFunctions::rowMajor_matrix1d_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount)
{

    qsizetype rowCount = rowMajor_matrix1d.count() / columnCount;

    QList<std::span<T>> rowSpans;

    rowSpans.reserve(rowCount);

    T *it = rowMajor_matrix1d.data();

    for (qsizetype i = 0; i < rowCount; ++i) {

        rowSpans.append(std::span<T>(it, columnCount));

        std::advance(it, columnCount);

    }

    return rowSpans;

}

template <typename T>
QList<std::span<T>> MathUtilityFunctions::rowMajor_matrix1d_rowSpans(std::vector<T> &rowMajor_matrix1d, qsizetype columnCount)
{

    qsizetype rowCount = rowMajor_matrix1d.size() / columnCount;

    QList<std::span<T>> rowSpans;

    rowSpans.reserve(rowCount);

    T *it = rowMajor_matrix1d.data();

    for (qsizetype i = 0; i < rowCount; ++i) {

        rowSpans.append(std::span<T>(it, columnCount));

        std::advance(it, columnCount);

    }

    return rowSpans;

}

template <typename T>
QList<std::pair<qsizetype, std::span<T>>> MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount)
{

    qsizetype rowCount = rowMajor_matrix1d.count() / columnCount;

    QList<std::pair<qsizetype, std::span<T>>> rowIndexes_rowSpans;

    rowIndexes_rowSpans.reserve(rowCount);

    T *it = rowMajor_matrix1d.data();

    for (qsizetype i = 0; i < rowCount; ++i) {

        rowIndexes_rowSpans.append(std::pair<qsizetype, std::span<T>>(i, std::span<T>(it, columnCount)));

        std::advance(it, columnCount);
    }

    return rowIndexes_rowSpans;

}

template <typename T> QHash<qsizetype, std::span<T>>
MathUtilityFunctions::rowMajor_matrix1d_hash_rowIndexes_rowSpans(QList<T> &rowMajor_matrix1d, qsizetype columnCount)
{

    qsizetype rowCount = rowMajor_matrix1d.count() / columnCount;

    QHash<qsizetype, std::span<T>> hash_rowIndexes_rowSpans;

    hash_rowIndexes_rowSpans.reserve(rowCount);

    T *it = rowMajor_matrix1d.data();

    for (qsizetype i = 0; i < rowCount; ++i) {

        hash_rowIndexes_rowSpans.insert(i, std::span<T>(it, columnCount));

        std::advance(it, columnCount);

    }

    return hash_rowIndexes_rowSpans;

}

template <typename T> QList<std::span<T>> MathUtilityFunctions::elementWiseDispatcher(QList<T> &vector, qsizetype numberOfThreads)
{

    if (numberOfThreads <= 1)
        return QList<std::span<T>>(1, std::span<T>(vector.data(), vector.count()));

    QList<std::span<T>> spans;

    T *it = vector.data();

    lldiv_t div = std::lldiv(vector.count(), numberOfThreads);

    if (div.quot != 0) {

        for (qsizetype i = 0; i < numberOfThreads - 1; ++i) {

            spans.append(std::span<T>(it, div.quot));

            std::advance(it, div.quot);

        }

        spans.append(std::span<T>(it, std::distance(it, (&vector.back() + 1))));

    } else {

        for (qsizetype i = 0; i < div.rem; ++i) {

            spans.append(std::span<T>(it, 1));

            std::advance(it, 1);

        }

    }

    return spans;

}

#endif // MATHUTILITYFUNCTIONS_H
