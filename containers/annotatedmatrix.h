#ifndef ANNOTATEDMATRIX_H
#define ANNOTATEDMATRIX_H

#include <QObject>
#include <QSharedDataPointer>
#include <QList>
#include <QMultiHash>
#include <QVariant>
#include <QReadWriteLock>

#include <algorithm>
#include <numeric>

#include "matrix.h"

class BaseAnnotatedMatrixData : public QSharedData
{

public:

        BaseAnnotatedMatrixData();

        BaseAnnotatedMatrixData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers);

        BaseAnnotatedMatrixData(qsizetype rowCount, qsizetype columnCount, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

        void setDataForIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation);

        void setDataForIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier = QString());

        QList<QString> d_columnAnnotationLabels;

        QList<bool> d_columnAnnotationLabelSelectionStatus;

        QHash<QString, qsizetype> d_columnAnnotationLabelToIndex;

        QHash<QString, QList<QVariant> > d_columnIdentifierToAnnotations;

        QMultiHash<QString, qsizetype> d_columnIdentifierToIndexes;

        QList<QString> d_columnIdentifiers;

        QList<bool> d_columnIdentifierSelectionStatus;

        QList<QString> d_rowAnnotationLabels;

        QList<bool> d_rowAnnotationLabelSelectionStatus;

        QHash<QString, qsizetype> d_rowAnnotationLabelToIndex;

        QHash<QString, QList<QVariant> > d_rowIdentifierToAnnotations;

        QMultiHash<QString, qsizetype> d_rowIdentifierToIndexes;

        QList<QString> d_rowIdentifiers;

        QList<bool> d_rowIdentifierSelectionStatus;

};

class BaseAnnotatedMatrix : public QObject
{

    Q_OBJECT

public:

    explicit BaseAnnotatedMatrix(BaseAnnotatedMatrix &&baseAnnotatedMatrix);

    explicit BaseAnnotatedMatrix(QObject *parent = nullptr);

    explicit BaseAnnotatedMatrix(const BaseAnnotatedMatrix &baseAnnotatedMatrix);

    BaseAnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QObject *parent = nullptr);

    BaseAnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    const QString &annotationLabel(qsizetype index, Qt::Orientation orientation) const;

    qsizetype annotationLabelCount(Qt::Orientation orientation, bool selectedOnly = false) const;

    QList<qsizetype> annotationLabelIndexes(Qt::Orientation orientation, bool selectedOnly = false) const;

    const QList<QString> &annotationLabels(Qt::Orientation orientation) const;

    QList<QString> annotationLabels(Qt::Orientation orientation, bool selectedOnly) const;

    QList<QString> annotationLabels(const QList<qsizetype> indexes, Qt::Orientation orientation) const;

    QVariant annotationValue(const QString &identifier, const QString &annotationLabel, Qt::Orientation orientation) const;

    QVariant annotationValue(qsizetype indexOfIdentifier, qsizetype indexOfAnnotationLabel, Qt::Orientation orientation) const;

    QList<QString> appendAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation);

    QList<QString> appendAnnotationLabels(const QList<QString> &annotationLabels, Qt::Orientation orientation);

    QList<QString> appendAnnotationLabels(qsizetype count, Qt::Orientation orientation);

    bool annotationLabelSelectionStatus(qsizetype index, Qt::Orientation orientation) const;

    void beginResetColumnAnnotations();

    void beginResetData();

    void beginResetRowAnnotations();

    virtual qsizetype columnCount() const = 0;

    bool containsAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation) const;

    bool containsIdentifier(const QString &identifier, Qt::Orientation orientation, bool selectedOnly = false) const;

    void endResetColumnAnnotations();

    void endResetData();

    void endResetRowAnnotations();

    const QString &identifier(qsizetype index, Qt::Orientation orientation) const;

    qsizetype identifierCount(Qt::Orientation orientation, bool selectedOnly = false) const;

    const QList<QString> &identifiers(Qt::Orientation orientation) const;

    QList<QString> identifiers(Qt::Orientation orientation, bool selectedOnly) const;

    QList<qsizetype> indexes(const QString &identifier, Qt::Orientation orientation, bool selectedOnly = false) const;

    QList<qsizetype> indexes(Qt::Orientation orientation, bool selectedOnly = false) const;

    QList<QString> insertAnnotationLabel(qsizetype index, const QString &annotationLabel, Qt::Orientation orientation);

    QList<QString> insertAnnotationLabels(qsizetype index, const QList<QString> &annotationLabels, Qt::Orientation orientation);

    QList<QString> insertAnnotationLabels(qsizetype index, qsizetype count, Qt::Orientation orientation);

    virtual void insertLists(qsizetype index, qsizetype count, Qt::Orientation orientation) = 0;

    void lockForRead();

    void lockForWrite();

    QList<QString> nonSelectedAnnotationLabels(Qt::Orientation orientation) const;

    QList<qsizetype> nonSelectedAnnotationLabelIndexes(Qt::Orientation orientation) const;

    QList<QString> nonSelectedIdentifiers(Qt::Orientation orientation) const;

    QList<qsizetype> nonSelectedIdentifierIndexes(Qt::Orientation orientation) const;

    BaseAnnotatedMatrix &operator=(BaseAnnotatedMatrix &&baseAnnotatedMatrix);

    BaseAnnotatedMatrix &operator=(const BaseAnnotatedMatrix &baseAnnotatedMatrix);

    QList<QString> prependAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation);

    QList<QString> prependAnnotationLabels(const QList<QString> &annotationLabels, Qt::Orientation orientation);

    QList<QString> prependAnnotationLabels(qsizetype count, Qt::Orientation orientation);

    void removeAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation);

    void removeAnnotationLabel(qsizetype index, Qt::Orientation orientation);

    void removeAnnotationLabels(QList<qsizetype> indexes, Qt::Orientation orientation);

    template <typename U> void removeIndexes(const QList<qsizetype> &sortedIndexesInAscendingOrder, U &container);

    void removeList(const QString &identifier, Qt::Orientation orientation, bool selectedOnly = false);

    void removeList(qsizetype index, Qt::Orientation orientation);

    virtual void removeLists(QList<qsizetype> indexes, Qt::Orientation orientation) = 0;

    void removeLists(const QList<QString> &identifiers, Qt::Orientation orientation, bool selectedOnly = false);

    bool identifierSelectionStatus(qsizetype index, Qt::Orientation orientation) const;

    virtual qsizetype rowCount() const = 0;

    bool setAnnotationLabel(qsizetype index, const QString &annotationLabel, Qt::Orientation orientation);

    void setAnnotationLabelSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation);

    bool setAnnotationValue(const QString &identifier, const QString &annotationLabel, const QVariant &value, Qt::Orientation orientation);

    bool setAnnotationValue(qsizetype identifierIndex, qsizetype annotationLabelIndex, const QVariant &value, Qt::Orientation orientation);

    void setIdentifier(qsizetype index, const QString &identifier, Qt::Orientation orientation);

    void setIdentifierSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation);

    virtual void setValueAsVariant(qsizetype rowIndex, qsizetype columnIndex, QVariant value) = 0;

    virtual void transpose() = 0;

    bool tryLockForRead();

    bool tryLockForWrite();

    void unlock();

    virtual QVariant valueAsVariant(qsizetype rowIndex, qsizetype columnIndex) const = 0;

    virtual qsizetype valueCount() const = 0;

    virtual ~BaseAnnotatedMatrix();

    static Qt::Orientation switchOrientation(Qt::Orientation orientation);

    friend QDataStream& operator<<(QDataStream& out, BaseAnnotatedMatrix &baseAnnotatedMatrix);

    friend QDataStream& operator>>(QDataStream& in, BaseAnnotatedMatrix &baseAnnotatedMatrix);

    virtual const char* typeName() const = 0;

protected:

    QSharedDataPointer<BaseAnnotatedMatrixData> d_data;

    void appendIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique = false);

    void appendIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier = QString(), bool makeUnique = false);

    void insertIdentifiers(qsizetype index, const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique = false);

    void insertIdentifiers(qsizetype index, qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier = QString(), bool makeUnique = false);

    void prependIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique = false);

    void prependIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier = QString(), bool makeUnique = false);

    void removeIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool selectedOnly = false);

    void removeIdentifiers(QList<qsizetype> indexes, Qt::Orientation orientation);

    void updateIdentifierToIndexes(Qt::Orientation orientation);

    virtual QDataStream &writeToDataStream(QDataStream &out) = 0;

    virtual QDataStream &readFromDataStream(QDataStream &in) = 0;

private:

    QReadWriteLock d_readWriteLock;

signals:

    void resetColumnAnnotationsAboutToBegin();

    void resetDataAboutToBegin();

    void resetRowAnnotationsAboutToBegin();

    void columnAnnotationsIsReset();

    void dataIsReset();

    void rowAnnotationsIsReset();

};



template <typename T>
class AnnotatedMatrix : public BaseAnnotatedMatrix, public Matrix<T>
{

    template<typename U> friend class AnnotatedMatrix;

public:

    explicit AnnotatedMatrix(AnnotatedMatrix &&annotatedMatrix);

    template <typename U> explicit AnnotatedMatrix(AnnotatedMatrix<U> &&annotatedMatrix);

    explicit AnnotatedMatrix(QObject *parent = nullptr);

    explicit AnnotatedMatrix(const AnnotatedMatrix &annotatedMatrix);

    template <typename U> explicit AnnotatedMatrix(const AnnotatedMatrix<U> &annotatedMatrix);

    AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<T> &&values, QObject *parent = nullptr);

    template <typename U> AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<U> &&values, QObject *parent = nullptr);

    AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<T> &values, QObject *parent = nullptr);

    template <typename U> AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<U> &values, QObject *parent = nullptr);

    AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const T &value, QObject *parent = nullptr);

    AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, QList<T> &&values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    template <typename U> AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, QList<U> &&values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QList<T> &values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    template <typename U> AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QList<U> &values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const T &value, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString(), QObject *parent = nullptr);

    void appendList(const QString &identifier, const QList<T> &list, Qt::Orientation orientation);

    template <typename U> void appendList(const QString &identifier, const QList<U> &list, Qt::Orientation orientation);

    void appendLists_1d(const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation);

    template <typename U> void appendLists_1d(const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation);

    void appendLists_2d(const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation);

    template <typename U> void appendLists_2d(const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation);

    qsizetype columnCount() const override;

    void insertList(qsizetype index, const QString &identifier, const QList<T> &list, Qt::Orientation orientation);

    template <typename U> void insertList(qsizetype index, const QString &identifier, const QList<U> &list, Qt::Orientation orientation);

    void insertLists(qsizetype index, qsizetype count, Qt::Orientation orientation) override;

    void insertLists_1d(qsizetype index, const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation);

    template <typename U> void insertLists_1d(qsizetype index, const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation);

    void insertLists_2d(qsizetype index, const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation);

    template <typename U> void insertLists_2d(qsizetype index, const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation);

    template <typename U> friend QTextStream& operator<<(QTextStream& out, const AnnotatedMatrix<U> &annotatedMatrix);

    AnnotatedMatrix<T> &operator=(AnnotatedMatrix<T> &&annotatedMatrix);

    template <typename U> AnnotatedMatrix<T> &operator=(AnnotatedMatrix<U> &&annotatedMatrix);

    AnnotatedMatrix<T> &operator=(const AnnotatedMatrix<T> &annotatedMatrix);

    template <typename U> AnnotatedMatrix<T> &operator=(const AnnotatedMatrix<U> &annotatedMatrix);

    void prependList(const QString &identifier, const QList<T> &list, Qt::Orientation orientation);

    template <typename U> void prependList(const QString &identifier, const QList<U> &list, Qt::Orientation orientation);

    void prependLists_1d(const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation);

    template <typename U> void prependLists_1d(const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation);

    void prependLists_2d(const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation);

    template <typename U> void prependLists_2d(const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation);

    void removeLists(QList<qsizetype> indexes, Qt::Orientation orientation) override;

    qsizetype rowCount() const override;

    void setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<T> &&values);

    template <typename U> void setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<U> &&values);

    void setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<T> &values);

    template <typename U> void setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<U> &values);

    void setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const T &value);

    void setData(qsizetype rowCount, qsizetype columnCount, QList<T> &&values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

    template <typename U> void setData(qsizetype rowCount, qsizetype columnCount, QList<U> &&values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

    void setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

    template <typename U> void setData(qsizetype rowCount, qsizetype columnCount, const QList<U> &values, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

    void setData(qsizetype rowCount, qsizetype columnCount, const T &value, const QString &prefixRowIdentifier = QString(), const QString &prefixColumnIdentifier = QString());

    void setValueAsVariant(qsizetype rowIndex, qsizetype columnIndex, QVariant value) override;

    void transpose() override;

    const char* typeName() const override;

    QVariant valueAsVariant(qsizetype rowIndex, qsizetype columnIndex) const override;

    qsizetype valueCount() const override;

    QDataStream &writeToDataStream(QDataStream &out) override;

    QDataStream &readFromDataStream(QDataStream &in) override;

    ~AnnotatedMatrix();

};

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(AnnotatedMatrix &&annotatedMatrix) :
    BaseAnnotatedMatrix(std::move(annotatedMatrix)),
    Matrix<T>(std::move(annotatedMatrix))
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(AnnotatedMatrix<U> &&annotatedMatrix) :
    BaseAnnotatedMatrix(std::move(annotatedMatrix)),
    Matrix<T>(std::move(annotatedMatrix))
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(QObject *parent) :
    BaseAnnotatedMatrix(parent),
    Matrix<T>()
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(const AnnotatedMatrix &annotatedMatrix) :
    BaseAnnotatedMatrix(annotatedMatrix),
    Matrix<T>(annotatedMatrix)
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(const AnnotatedMatrix<U> &annotatedMatrix) :
    BaseAnnotatedMatrix(annotatedMatrix),
    Matrix<T>(annotatedMatrix)
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<T> &&values, QObject *parent) :
    BaseAnnotatedMatrix(rowIdentifiers, columnIdentifiers, parent),
    Matrix<T>(rowIdentifiers.count(), columnIdentifiers.count(), std::move(values))
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<U> &&values, QObject *parent) :
    BaseAnnotatedMatrix(rowIdentifiers, columnIdentifiers, parent),
    Matrix<T>(rowIdentifiers.count(), columnIdentifiers.count(), std::move(values))
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<T> &values, QObject *parent) :
    BaseAnnotatedMatrix(rowIdentifiers, columnIdentifiers, parent),
    Matrix<T>(rowIdentifiers.count(), columnIdentifiers.count(), values)
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<U> &values, QObject *parent) :
    BaseAnnotatedMatrix(rowIdentifiers, columnIdentifiers, parent),
    Matrix<T>(rowIdentifiers.count(), columnIdentifiers.count(), values)
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const T &value, QObject *parent) :
    BaseAnnotatedMatrix(rowIdentifiers, columnIdentifiers, parent),
    Matrix<T>(rowIdentifiers.count(), columnIdentifiers.count(), value)
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, QList<T> &&values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    BaseAnnotatedMatrix(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier, parent),
    Matrix<T>(rowCount, columnCount, std::move(values))
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, QList<U> &&values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    BaseAnnotatedMatrix(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier, parent),
    Matrix<T>(rowCount, columnCount, std::move(values))
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QList<T> &values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    BaseAnnotatedMatrix(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier, parent),
    Matrix<T>(rowCount, columnCount, values)
{

}

template <typename T>
template <typename U> AnnotatedMatrix<T>::AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QList<U> &values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    BaseAnnotatedMatrix(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier, parent),
    Matrix<T>(rowCount, columnCount, values)
{

}

template <typename T>
AnnotatedMatrix<T>::AnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const T &value, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    BaseAnnotatedMatrix(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier, parent),
    Matrix<T>(rowCount, columnCount, value)
{

}

template <typename T>
void AnnotatedMatrix<T>::appendList(const QString &identifier, const QList<T> &list, Qt::Orientation orientation)
{

    this->insertList<T>(BaseAnnotatedMatrix::identifierCount(orientation), identifier, list, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::appendList(const QString &identifier, const QList<U> &list, Qt::Orientation orientation)
{

    this->insertList<U>(BaseAnnotatedMatrix::identifierCount(orientation), identifier, list, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::appendLists_1d(const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation)
{

    this->insertLists_1d<T>(BaseAnnotatedMatrix::identifierCount(orientation), identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::appendLists_1d(const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation)
{

    this->insertLists_1d<U>(BaseAnnotatedMatrix::identifierCount(orientation), identifiers, lists, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::appendLists_2d(const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation)
{

    this->insertLists_2d<T>(BaseAnnotatedMatrix::identifierCount(orientation), identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::appendLists_2d(const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation)
{

    this->insertLists_2d<U>(BaseAnnotatedMatrix::identifierCount(orientation), identifiers, lists, orientation);

}

template <typename T>
qsizetype AnnotatedMatrix<T>::columnCount() const
{

    return BaseMatrix::columnCount();

}

template <typename T>
void AnnotatedMatrix<T>::insertList(qsizetype index, const QString &identifier, const QList<T> &list, Qt::Orientation orientation)
{

    this->insertLists_1d<T>(index, {identifier}, list, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::insertList(qsizetype index, const QString &identifier, const QList<U> &list, Qt::Orientation orientation)
{

    this->insertLists_1d<U>(index, {identifier}, list, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::insertLists(qsizetype index, qsizetype count, Qt::Orientation orientation)
{

    QList<QString> identifiers;

    qsizetype currentCount = this->identifierCount(orientation);

    if (orientation == Qt::Vertical) {

        for (qsizetype i = 0; i < count; ++i)
            identifiers.append("row_" + QString::number(currentCount + i));

    } else {

        for (qsizetype i = 0; i < count; ++i)
            identifiers.append("column_" + QString::number(currentCount + i));

    }

    this->insertLists_1d<T>(index, identifiers, QList<T>(count * this->identifierCount(BaseAnnotatedMatrix::switchOrientation(orientation))), orientation);

}

template <typename T>
void AnnotatedMatrix<T>::insertLists_1d(qsizetype index, const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation)
{

    this->insertLists_1d<T>(index, identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::insertLists_1d(qsizetype index, const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation)
{

    this->insertIdentifiers(index, identifiers, orientation);

    if (BaseAnnotatedMatrix::identifierCount(BaseAnnotatedMatrix::switchOrientation(orientation)) == 0) {

        if (orientation == Qt::Vertical)
            BaseAnnotatedMatrix::d_data->setDataForIdentifiers(lists.count() / identifiers.count(), Qt::Horizontal, "column_");
        else
            BaseAnnotatedMatrix::d_data->setDataForIdentifiers(lists.count() / identifiers.count(), Qt::Vertical, "row_");

    }

    if (orientation == Qt::Vertical)
        Matrix<T>::insertRows_1d(index, lists);
    else
        Matrix<T>::insertColumns_1d(index, lists);

}

template <typename T>
void AnnotatedMatrix<T>::insertLists_2d(qsizetype index, const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation)
{

    this->insertLists_2d<T>(index, identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::insertLists_2d(qsizetype index, const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation)
{

    BaseAnnotatedMatrix::insertIdentifiers(index, identifiers, orientation);

    if (BaseAnnotatedMatrix::identifierCount(BaseAnnotatedMatrix::switchOrientation(orientation)) == 0) {

        if (orientation == Qt::Vertical)
            BaseAnnotatedMatrix::d_data->setDataForIdentifiers(identifiers.count(), Qt::Horizontal, "column_");
        else
            BaseAnnotatedMatrix::d_data->setDataForIdentifiers(identifiers.count(), Qt::Vertical, "row_");

    }

    if (orientation == Qt::Vertical)
        Matrix<T>::insertRows_2d(index, lists);
    else
        Matrix<T>::insertColumns_2d(index, lists);

}

template <typename U> QTextStream& operator<<(QTextStream& out, const AnnotatedMatrix<U> &annotatedMatrix)
{

    for (qsizetype i = 0; i < annotatedMatrix.columnCount(); ++i)
        out << "\t" << annotatedMatrix.BaseAnnotatedMatrix::d_data->d_columnIdentifiers.at(i);

    out << Qt::endl;

    for (qsizetype i = 0; i < annotatedMatrix.rowCount(); ++i) {

        const QList<U> &row(annotatedMatrix.rowAt(i));

        out << annotatedMatrix.BaseAnnotatedMatrix::d_data->d_rowIdentifiers.at(i);

        for (qsizetype j = 0; j < row.count(); ++j)
            out << "\t" << row.at(j);

        out << Qt::endl;

    }

    return out;

}

template <typename T>
AnnotatedMatrix<T> &AnnotatedMatrix<T>::operator=(const AnnotatedMatrix &annotatedMatrix)
{

    return this->operator=<T>(annotatedMatrix);

}

template <typename T>
template <typename U> AnnotatedMatrix<T> &AnnotatedMatrix<T>::operator=(const AnnotatedMatrix<U> &annotatedMatrix)
{

    if (this != &annotatedMatrix) {

        Matrix<T>::operator=(annotatedMatrix);

        BaseAnnotatedMatrix::d_data.operator=(annotatedMatrix.data);

    }

    return *this;

}

template <typename T>
void AnnotatedMatrix<T>::prependList(const QString &identifier, const QList<T> &list, Qt::Orientation orientation)
{

    this->insertList<T>(0, identifier, list, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::prependList(const QString &identifier, const QList<U> &list, Qt::Orientation orientation)
{

    this->insertList<U>(0, identifier, list, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::prependLists_1d(const QList<QString> &identifiers, const QList<T> &lists, Qt::Orientation orientation)
{

    this->insertLists_1d<T>(0, identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::prependLists_1d(const QList<QString> &identifiers, const QList<U> &lists, Qt::Orientation orientation)
{

    this->insertLists_1d<U>(0, identifiers, lists, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::prependLists_2d(const QList<QString> &identifiers, const QList<QList<T>> &lists, Qt::Orientation orientation)
{

    this->insertLists_2d<T>(0, identifiers, lists, orientation);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::prependLists_2d(const QList<QString> &identifiers, const QList<QList<U>> &lists, Qt::Orientation orientation)
{

    this->insertLists_2d<U>(0, identifiers, lists, orientation);

}

template <typename T>
void AnnotatedMatrix<T>::removeLists(QList<qsizetype> indexes, Qt::Orientation orientation)
{

    BaseAnnotatedMatrix::removeIdentifiers(indexes, orientation);

    if (orientation == Qt::Vertical)
        Matrix<T>::removeRows(indexes);
    else
        Matrix<T>::removeColumns(indexes);

}

template <typename T>
qsizetype AnnotatedMatrix<T>::rowCount() const
{

    return BaseMatrix::rowCount();

}

template <typename T>
void AnnotatedMatrix<T>::setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<T> &&values)
{

    Matrix<T>::setData(rowIdentifiers.count(), columnIdentifiers.count(), std::move(values));

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QList<U> &&values)
{

    Matrix<T>::setData(rowIdentifiers.count(), columnIdentifiers.count(), std::move(values));

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

template <typename T>
void AnnotatedMatrix<T>::setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<T> &values)
{

    Matrix<T>::setData(rowIdentifiers.count(), columnIdentifiers.count(), values);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const QList<U> &values)
{

    Matrix<T>::setData(rowIdentifiers.count(), columnIdentifiers.count(), values);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

template <typename T>
void AnnotatedMatrix<T>::setData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, const T &value)
{

    Matrix<T>::setData(rowIdentifiers.count(), columnIdentifiers.count(), value);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

template <typename T>
void AnnotatedMatrix<T>::setData(qsizetype rowCount, qsizetype columnCount, QList<T> &&values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    Matrix<T>::setData(rowCount, columnCount, std::move(values));

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}

template <typename T>
template <typename U> void AnnotatedMatrix<T>::setData(qsizetype rowCount, qsizetype columnCount, QList<U> &&values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    Matrix<T>::setData(rowCount, columnCount, std::move(values));

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}

template <typename T>
void AnnotatedMatrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    Matrix<T>::setData(rowCount, columnCount, values);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}


template <typename T>
template <typename U> void AnnotatedMatrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<U> &values, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    Matrix<T>::setData(rowCount, columnCount, values);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}

template <typename T>
void AnnotatedMatrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const T &value, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    Matrix<T>::setData(rowCount, columnCount, value);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    BaseAnnotatedMatrix::d_data->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}

template <typename T>
void AnnotatedMatrix<T>::setValueAsVariant(qsizetype rowIndex, qsizetype columnIndex, QVariant value)
{

    if (value.convert(QMetaType::fromType<T>()))
        Matrix<T>::d_data->d_values[rowIndex * BaseMatrix::d_data->d_columnCount + columnIndex] = value.value<T>();
    else
        Matrix<T>::d_data->d_values[rowIndex * BaseMatrix::d_data->d_columnCount + columnIndex] = std::numeric_limits<T>::quiet_NaN();

}

template <typename T>
void AnnotatedMatrix<T>::transpose()
{

    std::swap(d_data->d_columnAnnotationLabels, d_data->d_rowAnnotationLabels);

    std::swap(d_data->d_columnAnnotationLabelSelectionStatus, d_data->d_rowAnnotationLabelSelectionStatus);

    std::swap(d_data->d_columnAnnotationLabelToIndex, d_data->d_rowAnnotationLabelToIndex);

    std::swap(d_data->d_columnIdentifierToAnnotations, d_data->d_rowIdentifierToAnnotations);

    std::swap(d_data->d_columnIdentifierToIndexes, d_data->d_rowIdentifierToIndexes);

    std::swap(d_data->d_columnIdentifiers, d_data->d_rowIdentifiers);

    std::swap(d_data->d_columnIdentifierSelectionStatus, d_data->d_rowIdentifierSelectionStatus);

    Matrix<T>::transpose();

}

template <typename T>
const char* AnnotatedMatrix<T>::typeName() const
{

    return BaseMatrix::typeName();

}

template <typename T>
QVariant AnnotatedMatrix<T>::valueAsVariant(qsizetype rowIndex, qsizetype columnIndex) const
{

    return QVariant::fromValue(Matrix<T>::d_data->d_values.at(rowIndex * BaseMatrix::d_data->d_columnCount + columnIndex));

}

template <typename T>
qsizetype AnnotatedMatrix<T>::valueCount() const
{

    return this->values().count();

}


template <typename T>
AnnotatedMatrix<T>::~AnnotatedMatrix()
{

}

template <typename T>
QDataStream &AnnotatedMatrix<T>::writeToDataStream(QDataStream &out)
{

    out << QString("_AnnotatedMatrix_begin#");

    out << (qint32)1;

    out << QString("property_uuidProject") << this->property("uuidProject");

    out << QString("property_projectLabel") << this->property("projectLabel");

    out << QString("property_label") << this->property("label");

    out << QString("property_uuidObject") << this->property("uuidObject");

    out << QString("d_columnAnnotationLabels") << d_data->d_columnAnnotationLabels;

    out << QString("d_columnAnnotationLabelSelectionStatus") << d_data->d_columnAnnotationLabelSelectionStatus;

    out << QString("d_columnAnnotationLabelToIndex") << d_data->d_columnAnnotationLabelToIndex;

    out << QString("d_columnIdentifierToAnnotations") << d_data->d_columnIdentifierToAnnotations;

    out << QString("d_columnIdentifierToIndexes") << d_data->d_columnIdentifierToIndexes;

    out << QString("d_columnIdentifiers") << d_data->d_columnIdentifiers;

    out << QString("d_columnIdentifierSelectionStatus") << d_data->d_columnIdentifierSelectionStatus;

    out << QString("d_rowAnnotationLabels") << d_data->d_rowAnnotationLabels;

    out << QString("d_rowAnnotationLabelSelectionStatus") << d_data->d_rowAnnotationLabelSelectionStatus;

    out << QString("d_rowAnnotationLabelToIndex") << d_data->d_rowAnnotationLabelToIndex;

    out << QString("d_rowIdentifierToAnnotations") << d_data->d_rowIdentifierToAnnotations;

    out << QString("d_rowIdentifierToIndexes") << d_data->d_rowIdentifierToIndexes;

    out << QString("d_rowIdentifiers") << d_data->d_rowIdentifiers;

    out << QString("d_rowIdentifierSelectionStatus") << d_data->d_rowIdentifierSelectionStatus;

    out << QString("Matrix<T>");

    this->Matrix<T>::writeToDataStream(out);

    out << QString("_AnnotatedMatrix_end#");

    return out;


}

template <typename T>
QDataStream &AnnotatedMatrix<T>::readFromDataStream(QDataStream &in)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_AnnotatedMatrix_begin#")) {

        in.device()->seek(pos);

        in.setStatus(QDataStream::ReadCorruptData);

        return in;

    }

    qint32 version;

    in >> version;

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("property_uuidProject")) {

            QVariant variant;

            in >> variant;

            this->setProperty("uuidProject", variant);

        } else if (label == QString("property_projectLabel")) {

            QVariant variant;

            in >> variant;

            this->setProperty("projectLabel", variant);

        } else if (label == QString("property_label")) {

            QVariant variant;

            in >> variant;

            this->setProperty("label", variant);

        } else if (label == QString("property_uuidObject")) {

            QVariant variant;

            in >> variant;

            this->setProperty("uuidObject", variant);

        } else if (label == QString("d_columnAnnotationLabels"))
            in >> d_data->d_columnAnnotationLabels;
        else if (label == QString("d_columnAnnotationLabelSelectionStatus"))
            in >> d_data->d_columnAnnotationLabelSelectionStatus;
        else if (label == QString("d_columnAnnotationLabelToIndex"))
            in >> d_data->d_columnAnnotationLabelToIndex;
        else if (label == QString("d_columnIdentifierToAnnotations"))
            in >> d_data->d_columnIdentifierToAnnotations;
        else if (label == QString("d_columnIdentifierToIndexes"))
            in >> d_data->d_columnIdentifierToIndexes;
        else if (label == QString("d_columnIdentifiers"))
            in >> d_data->d_columnIdentifiers;
        else if (label == QString("d_columnIdentifierSelectionStatus"))
            in >> d_data->d_columnIdentifierSelectionStatus;
        else if (label == QString("d_rowAnnotationLabels"))
            in >> d_data->d_rowAnnotationLabels;
        else if (label == QString("d_rowAnnotationLabelSelectionStatus"))
            in >> d_data->d_rowAnnotationLabelSelectionStatus;
        else if (label == QString("d_rowAnnotationLabelToIndex"))
            in >> d_data->d_rowAnnotationLabelToIndex;
        else if (label == QString("d_rowIdentifierToAnnotations"))
            in >> d_data->d_rowIdentifierToAnnotations;
        else if (label == QString("d_rowIdentifierToIndexes"))
            in >> d_data->d_rowIdentifierToIndexes;
        else if (label == QString("d_rowIdentifiers"))
            in >> d_data->d_rowIdentifiers;
        else if (label == QString("d_rowIdentifierSelectionStatus"))
            in >> d_data->d_rowIdentifierSelectionStatus;
        else if (label == QString("Matrix<T>")) {
            this->Matrix<T>::readFromDataStream(in);

            if (in.status() == QDataStream::ReadCorruptData) {

                in.device()->seek(pos);

                return in;

            }

        } else if (label == QString("_AnnotatedMatrix_end#"))
            return in;
        else {

            in.setStatus(QDataStream::ReadCorruptData);

            in.device()->seek(pos);

            return in;

        }

    }

    return in;

}

#endif // ANNOTATEDMATRIX_H
