#include "distancesimilaritymatrixworker.h"

DistanceSimilarityMatrixParameters::DistanceSimilarityMatrixParameters(QObject *parent) :
    BaseParameters(parent, "distancesimilaritymatrix")
{

}

bool DistanceSimilarityMatrixParameters::isValid_()
{

    if (selectedVariableIdentifiers_data1.isEmpty()) {

        d_lastError = "no variables selected in data 1";

        return false;

    }

    if (selectedItemIdentifiers_data1.isEmpty()) {

        d_lastError = "no items selected in data 1";

        return false;

    }

    if (selectedVariableIdentifiers_data2.isEmpty()) {

        d_lastError = "no variables selected in data 2";

        return false;

    }

    if (selectedItemIdentifiers_data2.isEmpty()) {

        d_lastError = "no items selected in data 2";

        return false;

    }

    if (selectedItemIdentifiers_data1.count() != QSet<QString>(selectedItemIdentifiers_data1.begin(), selectedItemIdentifiers_data1.end()).count()) {

        d_lastError = "cannot create distance matrix -> data 1 \"" + data1->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (selectedItemIdentifiers_data2.count() != QSet<QString>(selectedItemIdentifiers_data2.begin(), selectedItemIdentifiers_data2.end()).count()) {

        d_lastError = "cannot create distance matrix -> data 2 \"" + data1->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString DistanceSimilarityMatrixParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[data 1 with label = \"" + data1->property("label").toString() + "\" and uuid = \"" + data1->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in data 1 containing variables = " << ((orientation_data1 == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in data 1 = [\"" << selectedVariableIdentifiers_data1.first() << "\", " << selectedVariableIndexes_data1.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers_data1.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers_data1.at(i) << "\", " << selectedVariableIndexes_data1.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items in data 1 = [\"" << selectedItemIdentifiers_data1.first() << "\", " << selectedItemIndexes_data1.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers_data1.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers_data1.at(i) << "\", " << selectedItemIndexes_data1.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[data 2 with label = \"" + data2->property("label").toString() + "\" and uuid = \"" + data2->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in data 2 containing variables = " << ((orientation_data2 == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in data 2 = [\"" << selectedVariableIdentifiers_data2.first() << "\", " << selectedVariableIndexes_data2.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers_data2.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers_data2.at(i) << "\", " << selectedVariableIndexes_data2.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items in data 2 = [\"" << selectedItemIdentifiers_data2.first() << "\", " << selectedItemIndexes_data2.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers_data2.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers_data2.at(i) << "\", " << selectedItemIndexes_data2.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[orientation in data 2 containing variables = " << ((orientation_data2 == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[distance function = " << distanceFunction << "]";

    if ((distanceFunction == "count overlap for ABS(value) > threshold") || (distanceFunction == "Pearson R for union ABS(value) > threshold"))
        stream << Qt::endl << "\t[threshold for overlap based metrics = " << thresholdForOverlapBasedComparisions << "]";

    if (distanceFunction == "rank-biased overlap") {

        stream << Qt::endl << "\t[ranked biased overlap : p = " << p_rankBiasedOverlap << "]";

        stream << Qt::endl << "\t[ranked biased overlap : maximum depth = " << maxDepth_rankBiasedOverlap << "]";

    }

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

DistanceSimilarityMatrixWorker::DistanceSimilarityMatrixWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("distance/similarity matrix"), parameters, "distancesimilaritymatrix")
{

}

void DistanceSimilarityMatrixWorker::doWork_()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName_data1 = parameters->data1->typeName();

    const char *typeName_data2 = parameters->data2->typeName();

    if (*typeName_data1 != *typeName_data2) {

        this->reportError("for creating distance matrix, data 1 and data 2 need to be the same numerical type");

        QThread::currentThread()->requestInterruption();

    }

    if (*typeName_data1 == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->data2));
    else if (*typeName_data1 == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->data2));
    else if (*typeName_data1 == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->data2));
    else if (*typeName_data1 == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->data2));
    else if (*typeName_data1 == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->data2));
    else if (*typeName_data1 == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->data2));
    else if (*typeName_data1 == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->data2));
    else if (*typeName_data1 == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->data1), qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->data2));
    else {

        this->reportError("for creating distance matrix, data 1 and data 2 need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool DistanceSimilarityMatrixWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    parameters->data1->lockForRead();

    parameters->data2->lockForRead();

    return true;

}

void DistanceSimilarityMatrixWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    parameters->data1->unlock();

    parameters->data2->unlock();

}

void DistanceSimilarityMatrixWorker::processForCovarianceFunction()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    this->startProgress("center data 1", QtConcurrent::map(&d_threadPool, d_data1_aligned_rowSpans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("center data 2", QtConcurrent::map(&d_threadPool, d_data2_aligned_rowSpans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    QUuid uuidProgress = this->startProgress("calculating covariance", 0, 0);

    QList<double> covarianceMatrix(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count(), d_alignedItemIdentifiers.count(), (1.0 / (static_cast<double>(d_alignedItemIdentifiers.count()) - 1.0)), d_data1_aligned.data(), d_alignedItemIdentifiers.count(), d_data2_aligned.data(), d_alignedItemIdentifiers.count(), 0.0, covarianceMatrix.data(), parameters->selectedVariableIndexes_data2.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_covarianceMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, covarianceMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("covariance matrix", result_covarianceMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_covarianceMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "covariance_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(covarianceMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "covariance"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of covariance matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_covariance_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForPearsonCorrelationFunction()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    this->startProgress("center data 1", QtConcurrent::map(&d_threadPool, d_data1_aligned_rowSpans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("center data 2", QtConcurrent::map(&d_threadPool, d_data2_aligned_rowSpans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->startProgress("scale data 1", QtConcurrent::map(&d_threadPool, d_data1_aligned_rowSpans, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("scale data 2", QtConcurrent::map(&d_threadPool, d_data2_aligned_rowSpans, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    QUuid uuidProgress = this->startProgress("calculating Pearson R matrix", 0, 0);

    QList<double> pearsonRMatrix(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count(), d_alignedItemIdentifiers.count(), 1.0, d_data1_aligned.data(), d_alignedItemIdentifiers.count(), d_data2_aligned.data(), d_alignedItemIdentifiers.count(), 0.0, pearsonRMatrix.data(), parameters->selectedVariableIndexes_data2.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_pearsonRMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, pearsonRMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("Pearson R matrix", result_pearsonRMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_pearsonRMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "Pearson_R_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(pearsonRMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "Pearson R"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of Pearson R matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_pearson_r_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForSpearmanCorrelationFunction()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    QList<double> data1_tiesCorrectionFactor(d_data1_aligned_rowSpans.count());

    this->startProgress("ranking data 1", QtConcurrent::map(&d_threadPool, d_data1_aligned_rowIndexes_rowSpans, [&data1_tiesCorrectionFactor](std::pair<qsizetype, std::span<double>> &index_span){data1_tiesCorrectionFactor[index_span.first] = MathOperations::crankInplace(index_span.second);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    QList<double> data2_tiesCorrectionFactor(d_data2_aligned_rowSpans.count());

    this->startProgress("ranking data 2", QtConcurrent::map(&d_threadPool, d_data2_aligned_rowIndexes_rowSpans, [&data2_tiesCorrectionFactor](std::pair<qsizetype, std::span<double>> &index_span){data2_tiesCorrectionFactor[index_span.first] = MathOperations::crankInplace(index_span.second);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    QUuid uuidProgress = this->startProgress("calculating Spearman R matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    QList<double> spearmanRMatrix;

    spearmanRMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters, &data1_tiesCorrectionFactor, &data2_tiesCorrectionFactor](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0.0);

        double n = index_span.second.size();

        for (qsizetype i = 0; i < d_data2_aligned_rowIndexes_rowSpans.count(); ++i) {

            const std::span<double> &currentSpan(d_data2_aligned_rowIndexes_rowSpans.at(i).second);

            double d = 0.0;

            for (unsigned int j = 0; j < index_span.second.size(); ++j)
                d += (index_span.second[j] - currentSpan[j]) * (index_span.second[j] - currentSpan[j]);

            double en3n = n * n * n - n;

            double fac = (1.0 - data1_tiesCorrectionFactor.at(index_span.first) / en3n) * (1.0 - data2_tiesCorrectionFactor.at(i) / en3n);

            intermediateResults[i] = (1.0 - (6.0 / en3n) * (d + (data1_tiesCorrectionFactor.at(index_span.first) + data2_tiesCorrectionFactor.at(i)) / 12.0)) / std::sqrt(fac);

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    spearmanRMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, d_data1_aligned_rowIndexes_rowSpans, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_spearmanRMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, spearmanRMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("Spearman R matrix", result_spearmanRMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_spearmanRMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "spearman_r_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(spearmanRMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "Spearman R"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of Spearman R matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_spearman_r_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForDistanceCovarianceFunction()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    QFuture<QList<std::tuple<double, qsizetype, qsizetype, double>>> future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1;

    QFuture<QList<std::tuple<double, qsizetype, qsizetype, double>>> future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2;

    QFuture<double> future_variance_data1;

    QFuture<double> future_variance_data2;

    auto mapFunctor1 = [](const std::span<double> &span) {

        return FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(span);

    };

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1 = QtConcurrent::mapped(&d_threadPool, d_data1_aligned_rowSpans, std::function<QList<std::tuple<double, qsizetype, qsizetype, double>>(const std::span<double> &span)>(mapFunctor1));

    this->startProgress("creating order statistics for data 1", future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1);

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1.waitForFinished();

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2 = QtConcurrent::mapped(&d_threadPool, d_data2_aligned_rowSpans, std::function<QList<std::tuple<double, qsizetype, qsizetype, double>>(const std::span<double> &span)>(mapFunctor1));

    this->startProgress("creating order statistics for data 2", future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2);

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2.waitForFinished();


    auto mapFunctor2 = [](const std::span<double> &span) {

        return boost::math::statistics::sample_variance(span);

    };

    future_variance_data1 = QtConcurrent::mapped(&d_threadPool, d_data1_aligned_rowSpans, std::function<double (const std::span<double> &span)>(mapFunctor2));

    this->startProgress("calculating variance data 1", future_variance_data1);

    future_variance_data1.waitForFinished();

    future_variance_data2 = QtConcurrent::mapped(&d_threadPool, d_data2_aligned_rowSpans, std::function<double (const std::span<double> &span)>(mapFunctor2));

    this->startProgress("calculating variance data 2", future_variance_data2);

    future_variance_data2.waitForFinished();


    qsizetype L = static_cast<int>(std::ceil(std::log2(static_cast<double>(d_alignedItemIdentifiers.count()))));

    QList<qsizetype> powersOfTwo(L + 2);

    for (qsizetype i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<qsizetype>(std::pow(2.0, static_cast<double>(i)));



    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data1_aligned, d_alignedItemIdentifiers.count());

    QUuid uuidProgress = this->startProgress("calculating distance covariance matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    QList<double> distanceCovarianceMatrix;

    distanceCovarianceMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters,  &L, &powersOfTwo, &future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1, &future_variance_data1, &future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2, &future_variance_data2](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0.0);

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

            intermediateResults[i] = std::sqrt(FastDistanceCorrelation::fastDistanceCovariance(index_span.second, d_data2_aligned_rowSpans.at(i), future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1.resultAt(index_span.first), future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2.resultAt(i), future_variance_data1.resultAt(index_span.first), future_variance_data2.resultAt(i), powersOfTwo, L).first());

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    distanceCovarianceMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_distanceCovarianceMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, distanceCovarianceMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("distance covariance matrix", result_distanceCovarianceMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_distanceCovarianceMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "distance_covariance_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(distanceCovarianceMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "distance covariance"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of distance covariance matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_distance_covariance_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForDistanceCorrelationFunction()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    QFuture<QList<std::tuple<double, qsizetype, qsizetype, double>>> future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1;

    QFuture<QList<std::tuple<double, qsizetype, qsizetype, double>>> future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2;

    QFuture<double> future_variance_data1;

    QFuture<double> future_variance_data2;

    auto mapFunctor1 = [](const std::span<double> &span) {

        return FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(span);

    };

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1 = QtConcurrent::mapped(&d_threadPool, d_data1_aligned_rowSpans, std::function<QList<std::tuple<double, qsizetype, qsizetype, double>>(const std::span<double> &span)>(mapFunctor1));

    this->startProgress("creating order statistics for data 1", future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1);

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1.waitForFinished();

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2 = QtConcurrent::mapped(&d_threadPool, d_data2_aligned_rowSpans, std::function<QList<std::tuple<double, qsizetype, qsizetype, double>>(const std::span<double> &span)>(mapFunctor1));

    this->startProgress("creating order statistics for data 2", future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2);

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2.waitForFinished();


    auto mapFunctor2 = [](const std::span<double> &span) {

        return boost::math::statistics::sample_variance(span);

    };

    future_variance_data1 = QtConcurrent::mapped(&d_threadPool, d_data1_aligned_rowSpans, std::function<double (const std::span<double> &span)>(mapFunctor2));

    this->startProgress("calculating variance data 1", future_variance_data1);

    future_variance_data1.waitForFinished();

    future_variance_data2 = QtConcurrent::mapped(&d_threadPool, d_data2_aligned_rowSpans, std::function<double (const std::span<double> &span)>(mapFunctor2));

    this->startProgress("calculating variance data 2", future_variance_data2);

    future_variance_data2.waitForFinished();


    qsizetype L = static_cast<int>(std::ceil(std::log2(static_cast<double>(d_alignedItemIdentifiers.count()))));

    QList<qsizetype> powersOfTwo(L + 2);

    for (qsizetype i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<qsizetype>(std::pow(2.0, static_cast<double>(i)));



    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data1_aligned, d_alignedItemIdentifiers.count());

    QUuid uuidProgress = this->startProgress("calculating distance correlation matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    QList<double> distanceCorrelationMatrix;

    distanceCorrelationMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters,  &L, &powersOfTwo, &future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1, &future_variance_data1, &future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2, &future_variance_data2](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0.0);

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

            intermediateResults[i] = FastDistanceCorrelation::fastDistanceCorrelation(index_span.second, d_data2_aligned_rowSpans.at(i), future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data1.resultAt(index_span.first), future_vector_orderStatistics_orderIndices_rank_cummulativeSum_data2.resultAt(i), future_variance_data1.resultAt(index_span.first), future_variance_data2.resultAt(i), powersOfTwo, L);

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    distanceCorrelationMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_distanceCorrelationMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, distanceCorrelationMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("distance correlation matrix", result_distanceCorrelationMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_distanceCorrelationMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "distance_correlation_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(distanceCorrelationMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "distance correlation"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of distance correlation matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_distance_correlation_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForChatterjeeCorrelation()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    std::random_device rd;

    std::mt19937 g(rd());

    this->startProgress("ranking data 1", QtConcurrent::map(&d_threadPool, d_data1_aligned_rowSpans, [&g](std::span<double> &span) {

        QList<double *> pointerCopyOfSpan;

        pointerCopyOfSpan.reserve(span.size());

        for (unsigned long i = 0; i < span.size(); ++i)
            pointerCopyOfSpan.append(&span[i]);

        std::shuffle(pointerCopyOfSpan.begin(), pointerCopyOfSpan.end(), g);

        std::sort(pointerCopyOfSpan.begin(), pointerCopyOfSpan.end(), [](const double *value1, const double *value2){return *value1 < *value2;});

        for (qsizetype i = 0; i < pointerCopyOfSpan.count(); ++i)
            *pointerCopyOfSpan[i] = i + 1;

    ;}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("ranking data 2", QtConcurrent::map(&d_threadPool, d_data2_aligned_rowSpans, [&g](std::span<double> &span){

        QList<double *> pointerCopyOfSpan;

        pointerCopyOfSpan.reserve(span.size());

        for (unsigned long i = 0; i < span.size(); ++i)
            pointerCopyOfSpan.append(&span[i]);

        std::shuffle(pointerCopyOfSpan.begin(), pointerCopyOfSpan.end(), g);

        std::sort(pointerCopyOfSpan.begin(), pointerCopyOfSpan.end(), [](const double *value1, const double *value2){return *value1 < *value2;});

        for (qsizetype i = 0; i < pointerCopyOfSpan.count(); ++i)
            *pointerCopyOfSpan[i] = i + 1;

    ;}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    QUuid uuidProgress = this->startProgress("calculating Chatterjee correlation matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters](const std::span<double> &data1_aligned_rowSpan) {

        QList<double> intermediateResults;

        intermediateResults.reserve(parameters->selectedVariableIndexes_data2.count());

        for (std::span<double> &data2_aligned_rowSpan : d_data2_aligned_rowSpans) {

            double xiCorrelation_1 = MathDescriptives::xiCorrelation_preRanked(data1_aligned_rowSpan, data2_aligned_rowSpan);

            double xiCorrelation_2 = MathDescriptives::xiCorrelation_preRanked(data2_aligned_rowSpan, data1_aligned_rowSpan);

            intermediateResults.append(std::max(xiCorrelation_1, xiCorrelation_2));

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<double> chatterjeeCorrelationMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, d_data1_aligned_rowSpans, std::function<QList<double>(const std::span<double> &span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_chatterjeeCorrelationMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, chatterjeeCorrelationMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("chatterjee correlation matrix", result_chatterjeeCorrelationMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_chatterjeeCorrelationMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "chatterjee_correlation_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(chatterjeeCorrelationMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "distance correlation"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of chatterjee correlation matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_chatterjee_correlation_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForDotProduct()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    QUuid uuidProgress = this->startProgress("calculating dot product", 0, 0);

    QList<double> dotProductMatrix(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count(), d_alignedItemIdentifiers.count(), (1.0 / (static_cast<double>(d_alignedItemIdentifiers.count()) - 1.0)), d_data1_aligned.data(), d_alignedItemIdentifiers.count(), d_data2_aligned.data(), d_alignedItemIdentifiers.count(), 0.0, dotProductMatrix.data(), parameters->selectedVariableIndexes_data2.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_covarianceMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, dotProductMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("dot product matrix", result_covarianceMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_covarianceMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "dot_product_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(dotProductMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "dot product"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of dot product matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_dot_product_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForCountOverlapForAbsoluteValueAboveThreshold()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());


    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data1_aligned, d_alignedItemIdentifiers.count());

    QList<qsizetype> countOverlapMatrix;

    countOverlapMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());


    QUuid uuidProgress = this->startProgress("calculating count overlap matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<qsizetype> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0);

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

            const std::span<double> & data2_span_i(d_data2_aligned_rowSpans.at(i));

            qsizetype countOverlap = 0;

            for (unsigned long j = 0; j < index_span.second.size(); ++j) {

                if ((std::abs(index_span.second[j]) >= parameters->thresholdForOverlapBasedComparisions) && (std::abs(data2_span_i[j]) >= parameters->thresholdForOverlapBasedComparisions))
                    countOverlap++;
            }

            intermediateResults[i] = countOverlap;

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<qsizetype> &result, const QList<qsizetype> &intermediateResult) {

        result.append(intermediateResult);

    };

    countOverlapMatrix = QtConcurrent::blockingMappedReduced<QList<qsizetype>>(&d_threadPool, dispatcher, std::function<QList<qsizetype>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<qsizetype> &, const QList<qsizetype> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);


    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<qsizetype> *result_countOverlapMatrix = new AnnotatedMatrix<qsizetype>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, countOverlapMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("count overlap matrix", result_countOverlapMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<qsizetype>> sharedPointerToAnnotatedMatrix(result_countOverlapMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "count_overlap_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(countOverlapMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "count overlap"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of count overlap matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_count_overlap_matrix");

        }

    }

}

void DistanceSimilarityMatrixWorker::processForPearsonCorrelationForUnionAbsoluteValueAboveThreshold()
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());


    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data1_aligned, d_alignedItemIdentifiers.count());

    QList<double> pearsonRMatrix;

    pearsonRMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());


    QUuid uuidProgress = this->startProgress("calculating Pearson R matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0);

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

            const std::span<double> & data2_span_i(d_data2_aligned_rowSpans.at(i));

            QList<double> vector1;

            QList<double> vector2;

            for (unsigned long j = 0; j < index_span.second.size(); ++j) {

                double &value1(index_span.second[j]);

                double &value2(data2_span_i[j]);

                if ((std::abs(value1) >= parameters->thresholdForOverlapBasedComparisions) || (std::abs(value2) >= parameters->thresholdForOverlapBasedComparisions)) {

                    vector1.append(value1);

                    vector2.append(value2);

                }

            }

            intermediateResults[i] = MathDescriptives::pearsonRCorrelation(vector1, vector2);

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    pearsonRMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);


    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_pearsonRMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, pearsonRMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("pearson R matrix", result_pearsonRMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_pearsonRMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "pearson_r_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(pearsonRMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "Pearson R"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of Pearson R matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_pearson_r_matrix");

        }

    }

}

double DistanceSimilarityMatrixWorker::rankBiasedOverlap(const std::span<qsizetype> &span1, const std::span<qsizetype> &span2, qsizetype maxDepth, long double p)
{

    qsizetype k = std::max(span1.size(), span2.size());

    if (maxDepth < k)
        k = maxDepth;

    QSet<qsizetype> set1;

    if (k <= static_cast<qsizetype>(span1.size()))
        set1 = QSet<qsizetype>(span1.begin(), span1.begin() + k);
    else
        set1 = QSet<qsizetype>(span1.begin(), span1.end());

    QSet<qsizetype> set2;

    if (k <= static_cast<qsizetype>(span2.size()))
        set2 = QSet<qsizetype>(span2.begin(), span2.begin() + k);
    else
        set2 = QSet<qsizetype>(span2.begin(), span2.end());

    qsizetype x_k = set1.intersect(set2).count();

    long double summation = 0.0;

    qsizetype i = 1;

    while (k != i) {

        if (i <= static_cast<qsizetype>(span1.size()))
            set1 = QSet<qsizetype>(span1.begin(), span1.begin() + i);
        else
            set1 = QSet<qsizetype>(span1.begin(), span1.end());

        if (i <= static_cast<qsizetype>(span2.size()))
            set2 = QSet<qsizetype>(span2.begin(), span2.begin() + i);
        else
            set2 = QSet<qsizetype>(span2.begin(), span2.end());

        long double a_d = static_cast<double>(set1.intersect(set2).count()) / static_cast<double>(i);

        summation += std::pow(p, static_cast<long double>(i)) * a_d;

        ++i;

    }

    return ((static_cast<long double>(x_k) / static_cast<long double>(k)) * std::pow(p, static_cast<long double>(k))) + ( (static_cast<long double>(1) - p) / p * summation);

}
