#include "selectdatawithtokensworker.h"

SelectDataWithTokensParameters::SelectDataWithTokensParameters(QObject *parent) :
    BaseParameters(parent, "selectdatawithtokens")
{

}

bool SelectDataWithTokensParameters::isValid_()
{

    if (selectedIdentifiersLabels.isEmpty()) {

        d_lastError = "no identifiers selected";

        return false;

    }

    if (selectedLabelsToApplyOn.isEmpty()) {

        if (orientation == Qt::Vertical)
            d_lastError = "no row identifier column, data columns, or row annotations to perform matching in selected";
        else
            d_lastError = "no column identifier row, data rows, or column annotations to perform matching in selected";

        return false;

    }

    if (tokensToMatch.isEmpty()) {

        d_lastError = "no tokens to match";

        return false;

    }

    if (((selectionMode1 == "match with all tokens in at least one field") || (selectionMode1 == "match with all tokens in all fields")) && (matchType == "exact match")) {

        d_lastError = "\"match with all tokens\" in combination with \"exact match\" is not possible";

        return false;

    }

    return true;

}

QString SelectDataWithTokensParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[select data with tokens in data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected identifiers to apply matching on = [\"" << selectedIdentifiersLabels.first() << "\", " << selectedIdentifiersIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedIdentifiersLabels.count(); ++i)
        stream << ", [\"" << selectedIdentifiersLabels.at(i) << "\", " << selectedIdentifiersIndexes.at(i) << "]";

    stream << "]";

    if (orientation == Qt::Vertical)
        stream << Qt::endl << "\t[row identifier column, data columns, or row annotations to perform matching in = [\"" << selectedLabelsToApplyOn.first() << "\", " << selectedIndexesToApplyOn.first() << "]";
    else
        stream << Qt::endl << "\t[select column identifier row, data rows, or column annotations to perform matching in = [\"" << selectedLabelsToApplyOn.first() << "\", " << selectedIndexesToApplyOn.first() << "]";

    for (qsizetype i = 1; i < selectedLabelsToApplyOn.count(); ++i)
        stream << ", [\"" << selectedLabelsToApplyOn.at(i) << "\", " << selectedIndexesToApplyOn.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selection mode = " << selectionMode1 << "]";

    stream << Qt::endl << "\t[selection mode = " << selectionMode2 << "]";

    stream << Qt::endl << "\t[match type = " << matchType << "]";

    stream << Qt::endl << "\t[case sensitivity = " << ((caseSensitivity == Qt::CaseInsensitive) ? "case insensitive" : "case sensitive") << "]";

    stream << Qt::endl << "\t[tokens to match = \"" << tokensToMatch.first() << "\"";

    for (qsizetype i = 1; i < tokensToMatch.count(); ++i)
        stream << ", \"" << tokensToMatch.at(i) << "\"";

    stream << "]";

    return prettyPrintString;

}

SelectDataWithTokensWorker::SelectDataWithTokensWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("select data with tokens"), parameters, "selectdatawithtokens")
{

}

void SelectDataWithTokensWorker::doWork_()
{

    QSharedPointer<SelectDataWithTokensParameters> parameters(d_data->d_parameters.dynamicCast<SelectDataWithTokensParameters>());

    bool exactMatch = (parameters->matchType == "exact match");

    QList<QRegularExpression> regularExpressions;

    QList<QList<QString>> tokensToMatchDividedInSmallerChunks;

    std::lldiv_t lldiv = std::lldiv(parameters->tokensToMatch.count(), 250);

    for (qsizetype i = 0; i < lldiv.quot; ++i) {

        QList<QString> temp(250);

        for (qsizetype j = 0; j < 250; ++j)
            temp[j] = parameters->tokensToMatch.at(i * 250 + j);

        tokensToMatchDividedInSmallerChunks.append(temp);

    }

    QList<QString> temp(lldiv.rem);

    for (qsizetype i = 0; i < lldiv.rem; ++i)
        temp[i] = parameters->tokensToMatch.at(lldiv.quot * 250 + i);

    tokensToMatchDividedInSmallerChunks.append(temp);

    for (const QList<QString> &tokensToMatch :tokensToMatchDividedInSmallerChunks) {

        QString pattern;

        if ((parameters->selectionMode1 == "match with at least one token in at least one field") || (parameters->selectionMode1 == "match with at least one token in all fields")) {

            if (exactMatch) {

                pattern = "^" + tokensToMatch.first() + "$";

                for (qsizetype i = 1; i < tokensToMatch.count(); ++i)
                    pattern += "|^" + tokensToMatch.at(i) + "$";

            } else {

                pattern = tokensToMatch.first();

                for (qsizetype i = 1; i < tokensToMatch.count(); ++i)
                    pattern += "|" + tokensToMatch.at(i);

            }

        } else {

            pattern = "^(?=.*" + tokensToMatch.first() +")";

            for (qsizetype i = 1; i < tokensToMatch.count(); ++i)
                pattern += "(?=.*" + tokensToMatch.at(i) +")";

        }

        regularExpressions.append(QRegularExpression(pattern, (parameters->caseSensitivity == Qt::CaseInsensitive) ? QRegularExpression::CaseInsensitiveOption : QRegularExpression::NoPatternOption));

    }

    bool matchWithIdentifier = (parameters->selectedIndexesToApplyOn.first() == -1);

    bool matchWithOnlyOneFields = true;

    bool inverseSelection = (parameters->selectionMode2 == "if match then select") ? false : true;

    if ((parameters->selectionMode1 == "match with at least one token in all fields") || (parameters->selectionMode1 == "match with all tokens in all fields"))
        matchWithOnlyOneFields = false;

    QList<qsizetype> dataIndexes;

    QList<qsizetype> annotationIndexes;

    QRegularExpression regularExpression2("^.ROW ANNOTATION.|^.COLUMN ANNOTATION.");

    for (qsizetype i = (matchWithIdentifier) ? 1 : 0; i < parameters->selectedLabelsToApplyOn.count(); ++i) {

        if (regularExpression2.match(parameters->selectedLabelsToApplyOn.at(i)).hasMatch())
            annotationIndexes.append(parameters->selectedIndexesToApplyOn.at(i));
        else
            dataIndexes.append(parameters->selectedIndexesToApplyOn.at(i));

    }

    QUuid uuidProgress = this->startProgress("selecting identifiers", 0, parameters->selectedIdentifiersIndexes.count());

    for (qsizetype i = 0; i < parameters->selectedIdentifiersIndexes.count(); ++i) {

        if (matchWithOnlyOneFields) {

            if ((matchWithIdentifier) && (this->hashMatch(parameters->selectedIdentifiersLabels.at(i), regularExpressions)))
                parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? false : true, parameters->orientation);
            else {

                bool succesfullMatch = false;

                for (qsizetype j = 0; j < dataIndexes.count(); ++j) {

                    if (parameters->orientation == Qt::Vertical) {

                        if (this->hashMatch(parameters->baseAnnotatedMatrix->valueAsVariant(parameters->selectedIdentifiersIndexes.at(i), dataIndexes.at(j)).toString(), regularExpressions)) {

                            succesfullMatch = true;

                            break;

                        }

                    } else {

                        if (this->hashMatch(parameters->baseAnnotatedMatrix->valueAsVariant(dataIndexes.at(j), parameters->selectedIdentifiersIndexes.at(i)).toString(), regularExpressions)) {

                            succesfullMatch = true;

                            break;

                        }

                    }

                }

                if (succesfullMatch)
                    parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? false : true, parameters->orientation);
                else {

                    bool succesfullMatch = false;

                    for (qsizetype j = 0; j < annotationIndexes.count(); ++j) {


                        if (this->hashMatch(parameters->baseAnnotatedMatrix->annotationValue(parameters->selectedIdentifiersIndexes.at(i), annotationIndexes.at(j), parameters->orientation).toString(), regularExpressions)) {

                            succesfullMatch = true;

                            break;

                        }

                    }

                    if (succesfullMatch)
                        parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? false : true, parameters->orientation);
                    else
                        parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? true : false, parameters->orientation);

                }

            }

        } else {

            bool succesfullMatch = true;

            if ((matchWithIdentifier) && (!this->hashMatch(parameters->selectedIdentifiersLabels.at(i), regularExpressions)))
                succesfullMatch = false;
            else {

                for (qsizetype j = 0; j < dataIndexes.count(); ++j) {

                    if (parameters->orientation == Qt::Vertical) {

                        if (!this->hashMatch(parameters->baseAnnotatedMatrix->valueAsVariant(parameters->selectedIdentifiersIndexes.at(i), dataIndexes.at(j)).toString(), regularExpressions)) {

                            succesfullMatch = false;

                            break;

                        }

                    } else {

                        if (!this->hashMatch(parameters->baseAnnotatedMatrix->valueAsVariant(dataIndexes.at(j), parameters->selectedIdentifiersIndexes.at(i)).toString(), regularExpressions)) {

                            succesfullMatch = false;

                            break;

                        }

                    }

                }

                if (succesfullMatch) {

                    for (qsizetype j = 0; j < annotationIndexes.count(); ++j) {

                        if (!this->hashMatch(parameters->baseAnnotatedMatrix->annotationValue(parameters->selectedIdentifiersIndexes.at(i), annotationIndexes.at(j), parameters->orientation).toString(), regularExpressions)) {

                            succesfullMatch = false;

                            break;

                        }

                    }

                }

            }

            if (succesfullMatch)
                parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? false : true, parameters->orientation);
            else
                parameters->baseAnnotatedMatrix->setIdentifierSelectionStatus(parameters->selectedIdentifiersIndexes.at(i), inverseSelection ? true : false, parameters->orientation);

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->reportMessage(QString::number(parameters->baseAnnotatedMatrix->identifierCount(parameters->orientation, true)) + ((parameters->orientation == Qt::Horizontal)  ? " column" : " row") + " identifiers selected");

}

bool SelectDataWithTokensWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<SelectDataWithTokensParameters> parameters(d_data->d_parameters.dynamicCast<SelectDataWithTokensParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    parameters->baseAnnotatedMatrix->beginResetData();

    if (parameters->orientation == Qt::Horizontal)
        parameters->baseAnnotatedMatrix->beginResetColumnAnnotations();
    else
        parameters->baseAnnotatedMatrix->beginResetRowAnnotations();

    return true;

}

void SelectDataWithTokensWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<SelectDataWithTokensParameters> parameters(d_data->d_parameters.dynamicCast<SelectDataWithTokensParameters>());

    parameters->baseAnnotatedMatrix->endResetData();

    if (parameters->orientation == Qt::Horizontal)
        parameters->baseAnnotatedMatrix->endResetColumnAnnotations();
    else
        parameters->baseAnnotatedMatrix->endResetRowAnnotations();

    parameters->baseAnnotatedMatrix->unlock();

}

bool SelectDataWithTokensWorker::hashMatch(const QString &str, QList<QRegularExpression> regularExpressions)
{
    bool hasMatch = false;

    for (const QRegularExpression &regularExpression : regularExpressions) {

        if (regularExpression.match(str).hasMatch())
            return true;

    }

    return hasMatch;

}
