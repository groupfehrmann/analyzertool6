#ifndef ICAIMAGEANALYSISWORKER_H
#define ICAIMAGEANALYSISWORKER_H

#include <QObject>
#include <QString>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QUuid>
#include <QApplication>
#include <QList>

#include "opencv2/opencv.hpp"
#include <opencv2/core/mat.hpp>

#include <boost/math/statistics/univariate_statistics.hpp>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "math/complexfastica.h"

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

class ICAImageAnalysisParameters : public BaseParameters
{

public:

    ICAImageAnalysisParameters(QObject *parent = nullptr);

    int windowSize;

    QString directoryToScanForTrainingImages;

    QString directoryToScanForTestingImages;

    QString exportDirectory;

    qsizetype numberOfThreadsToUse;

    QList<QString> selectedResultItems;

    qsizetype threshold_maximumNumberOfComponents;

    double threshold_cumulativeExplainedVariance;

    int indexOfPageToReadFromMultiPageImage;

    QString contrastFunction;

    double epsilon;

    qsizetype maximumNumberOfIterations;

    double mu;

    qsizetype numberOfRunsToPerform;

    QString multiThreadMode;

    double threshold_credibilityIndex;

    double pearsonCorrelationThresholdForConsensus;

    double xiCorrelationThresholdForConsensus;

    qsizetype maximumLibrarySize;

    double xiCorrelationLowerThresholdForLibrary;

    double xiCorrelationUpperThresholdForLibrary;

    qsizetype numberOfPermutationsForNullDistribution;

    bool reconstructTrainingImagesWithLibrary;

    double frequencyThresholdForLibraryReconstruction;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ICAImageAnalysisWorker : public BaseWorker
{

public:

    ICAImageAnalysisWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    QList<QString> d_cvMatrixType;

    QList<QString> d_cvMatrixDepth;

    QList<double> d_maximumAbsoluteValueForDepth;

    QString d_exportPathForCurrentImage;

    QString d_suffixForCurrentImage;

    cv::Mat d_currentPaddedImage;

    QList<cv::Mat> d_currentImageWindows;

    int d_depthForCurrentImage;

    int d_nChannelsForCurrentImage;

    qsizetype d_nImageWindowsForCurrentImage;

    qsizetype d_nFeaturesForCurrentImage;

    int d_numberOfRowWindowsInCurrentImage;

    int d_numberOfColumnWindowsInCurrentImage;

    QList<std::pair<qsizetype, QList<std::complex<double>>>> d_library;

    QList<qsizetype> d_sizeOfLibraryTracker;

    qsizetype d_numberOfTrainingImagesUsed;

    QList<double> d_ssimMetric_ReconstructedImageWindowMode;

    QList<double> d_ssimMetric_ReconstructedImageMode;

    QList<double> d_explainedVariance_whitening;

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    QList<QString> scanDirectoryForValidImages(const QString &directory);

    bool readImage(const QString &pathToImage, cv::Mat &image, int indexOfPageToReadFromMultiPageImage = 0);

    void createPath(const QString &path);

    QList<cv::Mat> splitImageInWindows(const cv::Mat &currentImage, int windowSize);

    cv::Mat createOne(QList<cv::Mat> &images, int cols, int min_gap_size, bool normalize = false);

    std::vector<std::complex<double>> convertImageWindowsToFourierTransformedVectors(const QList<cv::Mat> &imageWindows);

    QList<cv::Mat> convertFourierTransformVectorsToImageWindows(const QList<std::complex<double>> &whitenedFourierTransformedVectorOfImageWindows, bool normalize = false);

    QList<std::complex<double> > calculateCovarianceMatrix(std::vector<std::complex<double> > &matrix);

    QList<std::complex<double> > whitening(std::vector<std::complex<double>> &fourierTransformVectorOfImageBlocks, QList<std::complex<double> > &dewhiteningMatrix, QList<std::complex<double> > &whiteningMatrix, qsizetype &numberOfValidComponents);

    QList<std::complex<double>> performICARun(qsizetype runNumber, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &whiteningMatrix, const QList<std::complex<double>> &dewhiteningMatrix, qsizetype numberOfValidComponents, int contrastFunction, int numberOfThreadsToUse);

    QList<std::complex<double>> calculateConsensusComponents(const QList<std::complex<double> > &independentComponentsForAllRuns, qsizetype numberOfValidComponents);

    QList<std::complex<double>> calculateConsensusMixingMatrix(const QList<std::complex<double> > &consensusIndependentComponents, qsizetype numberOfValidComponents, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &dewhiteningMatrix);

    QList<std::complex<double>> calculateConsensusMixingMatrix_shuffled(qsizetype shuffleNumber, const QList<std::complex<double> > &consensusIndependentComponents, qsizetype numberOfValidComponents, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &dewhiteningMatrix);

    QList<std::complex<double>> calculateConsensusMixingMatrix_applyLibraryMode(const QList<std::complex<double> > &pseudoInverseLibraryConsensusIndependentComponents, const QList<std::complex<double> > &libraryConsensusIndependentComponents, qsizetype numberOfValidComponents, const std::vector<std::complex<double>> &fourierTransformedVectorOfImageWindows);

    QList<std::complex<double>> calculateConsensusMixingMatrix_shuffled_applyLibraryMode(qsizetype shuffleNumber, const QList<std::complex<double> > &libraryConsensusIndependentComponents, qsizetype numberOfValidComponents, const std::vector<std::complex<double>> &fourierTransformedVectorOfImageWindows);

    QList<QList<qsizetype>> createConsensusClusterToMemberIndexes(QList<std::complex<double>> &symmetricDistanceMatrix, qsizetype leadingDimension, double threshold, qsizetype numberOfThreadsToUse);

    void updateLibrary(const QList<std::complex<double> > &consensusIndependentComponents);

    void updateLibrary_test(const QList<std::complex<double> > &consensusIndependentComponents);

    void exportLibrary();

    void applyLibraryToImages(const QList<QString> &pathOfImages, const QString &outputDir);

    QList<double> xiCorrelationSymmetricalMatrix(const QList<std::complex<double> > &vectors, qsizetype leadingDimension);

    double xiCorrelation(QList<std::complex<double> > vector1, QList<std::complex<double> > vector2);

    cv::Scalar getMSSIM(const cv::Mat& i1, const cv::Mat& i2) const;

};

#endif // ICAIMAGEANALYSISWORKER_H
