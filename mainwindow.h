#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QSharedPointer>
#include <QAction>

#include "controller.h"
#include "datarepository.h"
#include "models/datarepositorymodel.h"
#include "widgets/datarepositorywidget.h"
#include "logmonitor.h"
#include "models/logmonitormodel.h"
#include "widgets/logmonitorwidget.h"
#include "progressmonitor.h"
#include "models/progressmonitormodel.h"
#include "widgets/progressmonitorwidget.h"
#include "widgets/stackedobjectwidget.h"
#include "containers/annotatedmatrix.h"
#include "dialogs/datafromasinglefiledialog.h"
#include "workerclasses/datafromasinglefileworker.h"
#include "dialogs/annotationsfromasinglefiledialog.h"
#include "workerclasses/annotationsfromasinglefileworker.h"
#include "workerclasses/saveprojectdataresultworker.h"
#include "workerclasses/openprojectdataresultworker.h"
#include "dialogs/exportdialog.h"
#include "workerclasses/exportworker.h"
#include "dialogs/selectdatawithtokensdialog.h"
#include "workerclasses/selectdatawithtokensworker.h"
#include "dialogs/mergedatadialog.h"
#include "workerclasses/mergedataworker.h"
#include "workerclasses/transposedataworker.h"
#include "dialogs/independentcomponentanalysisdialog.h"
#include "workerclasses/independentcomponentanalysisworker.h"
#include "dialogs/projectdataonindependentcomponentsdialog.h"
#include "workerclasses/projectdataonindependentcomponentsworker.h"
#include "dialogs/principalcomponentanalysisdialog.h"
#include "workerclasses/principalcomponentanalysisworker.h"
#include "dialogs/setenrichmentanalysisdialog.h"
#include "workerclasses/setenrichmentanalysisworker.h"
#include "dialogs/transformdatawithpredefinedmetricsdialog.h"
#include "workerclasses/transformdatawithpredefinedmetricsworker.h"
#include "dialogs/transformdatausingsequenceoffunctionsdialog.h"
#include "workerclasses/transformdatausingsequenceoffunctionsworker.h"
#include "dialogs/descriptivesdialog.h"
#include "workerclasses/descriptivesworker.h"
#include "dialogs/distancesimilaritymatrixdialog.h"
#include "workerclasses/distancesimilaritymatrixworker.h"
#include "dialogs/replaceidentifierwithannotationvaluedialog.h"
#include "workerclasses/replaceidentifierwithannotationvalueworker.h"
#include "dialogs/replaceidentifierwithannotationvaluedialog.h"
#include "workerclasses/replaceidentifierwithannotationvalueworker.h"
#include "dialogs/randomselectdialog.h"
#include "workerclasses/randomselectworker.h"
#include "dialogs/shuffleidentifiersdialog.h"
#include "workerclasses/shuffleidentifiersworker.h"
#include "dialogs/shuffledatadialog.h"
#include "workerclasses/shuffledataworker.h"
#include "dialogs/comparesamplescontinuousdialog.h"
#include "workerclasses/comparesamplescontinuousworker.h"
#include "dialogs/splitidentifiersdialog.h"
#include "workerclasses/splitidentifiersworker.h"
#include "dialogs/removeduplicateidentifiersdialog.h"
#include "workerclasses/removeduplicateidentifiersworker.h"
#include "dialogs/guiltbyassociationanalysisdialog.h"
#include "workerclasses/guiltbyassociationanalysisworker.h"
#include "dialogs/icaimageanalysisdialog.h"
#include "workerclasses/icaimageanalysisworker.h"
#include "dialogs/transformdatausingmedianpolishdialog.h"
#include "workerclasses/transformdatausingmedianpolishworker.h"
#include "dialogs/calculateconsensusforindependentcomponentsdialog.h"
#include "workerclasses/calculateconsensusforindependentcomponentsworker.h"
#include "dialogs/sysmexdataprocessordialog.h"
#include "workerclasses/sysmexdataprocessorworker.h"
#include "dialogs/transformdatausingquantilenormalizationdialog.h"
#include "workerclasses/transformdatausingquantilenormalizationworker.h"
#include "dialogs/transformdatabyremovingprincipalcomponentsdialog.h"
#include "dialogs/transformdatabyremovingprincipalcomponentsdialog.h"

QT_BEGIN_NAMESPACE

namespace Ui {

    class MainWindow;

}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{

    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private:

    Ui::MainWindow *ui;

    QSharedPointer<Controller> d_controller;

    QSharedPointer<DataRepository> d_dataRepository;

    QSharedPointer<DataRepositoryModel> d_dataRepositoryModel;

    QSharedPointer<LogMonitor> d_logMonitor;

    QSharedPointer<LogMonitorModel> d_logMonitorModel;

    QSharedPointer<ProgressMonitor> d_progressMonitor;

    QSharedPointer<ProgressMonitorModel> d_progressMonitorModel;


    QAction *d_clearRecentlyOpenedFilesAction;

    QList<QString> d_recentlyOpenedLabel;

    QList<QString> d_recentlyOpenedPathToFile;

    QList<QString> d_recentlyOpenedObjectType;

    QList<QAction *> d_recentlyOpenedActions;

    void readApplicationSettings();

    QString selectFile(QFileDialog::AcceptMode acceptMode, const QString &directory, const QString &suggestedFileName, const QStringList &filters, const QString &defaultSuffix) const;

    void setupDataRepositoryWidget();

    void setupLogMonitorWidget();

    void setupMainWindow();

    void setupProgressMonitorWidget();

    void setupRecentlyOpenedActions();

    void setupSplashScreen();

    void updateRecentlyOpenedActions();

    void writeApplicationSettings();

private slots:

    void addToRecentlyOpenedFiles(const QString &objectType, const QString &label, const QString &pathToFile);

    void openRecentlyOpened_project_data_result();

    void on_actionclose_all_triggered();

    void on_actionclose_triggered();

    void on_actioncolumn_annotations_triggered();

    void on_actiondata_from_a_single_file_triggered();

    void on_actionexport_triggered();

    void on_actionindependent_component_analysis_triggered();

    void on_actionproject_data_on_independent_components_triggered();

    void on_actionprincipal_component_analysis_triggered();

    void on_actionmerge_triggered();

    void on_actionnew_project_triggered();

    void on_actionopen_project_data_result_triggered();

    void on_actionrow_annotations_triggered();

    void on_actionsave_project_data_result_triggered();

    void on_actionset_enrichment_analysis_triggered();

    void on_actiontranspose_triggered();

    void on_actionwith_tokens_triggered();

    void on_actionusing_predefined_metrics_triggered();

    void on_actiondescriptives_triggered();

    void on_actionusing_sequence_of_functions_triggered();

    void on_actiondistance_similarity_matrix_triggered();

    void on_actionreplace_with_annotation_value_triggered();

    void on_actionsplit_identifiers_triggered();

    void on_actionrandom_select_triggered();

    void on_actionconvert_to_data_triggered();

    void on_actionshuffle_identifiers_triggered();

    void on_actionshuffle_data_triggered();

    void on_actioncompare_samples_continuous_data_triggered();

    void on_actionremove_duplicate_identifiers_triggered();

    void on_actionguilt_by_association_analysis_triggered();

    void on_actionICA_image_analysis_triggered();

    void on_actionmedian_polish_triggered();

    void on_actioncalculate_consensus_for_independent_components_triggered();

    void on_actionSysmex_data_processor_triggered();

    void on_actionquantile_normalization_triggered();

    void on_actionremove_principal_components_triggered();

};

#endif // MAINWINDOW_H
