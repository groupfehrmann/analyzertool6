#include "randomselectworker.h"

RandomSelectParameters::RandomSelectParameters(QObject *parent) :
    BaseParameters(parent, "randomselect")
{

}

bool RandomSelectParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString RandomSelectParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[random select in data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[proportion to select = \"" << proportionToSelect << "\"]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    return prettyPrintString;

}

RandomSelectWorker::RandomSelectWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("random select"), parameters, "randomselect")
{

}

void RandomSelectWorker::doWork_()
{

    QSharedPointer<RandomSelectParameters> parameters(d_data->d_parameters.dynamicCast<RandomSelectParameters>());

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(QString).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned char).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned char>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(bool).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<bool>>(parameters->baseAnnotatedMatrix));

}

bool RandomSelectWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<RandomSelectParameters> parameters(d_data->d_parameters.dynamicCast<RandomSelectParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    return true;

}

void RandomSelectWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<RandomSelectParameters> parameters(d_data->d_parameters.dynamicCast<RandomSelectParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}
