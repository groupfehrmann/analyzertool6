#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    qRegisterMetaType<QList<qsizetype>>("list_qsizetype");

    QCoreApplication::setOrganizationName("Rudolf S.N. Fehrmann");

    QCoreApplication::setOrganizationDomain("www.rudolffehrmann.nl");

    QCoreApplication::setApplicationName("AnalyzerTool 6");

    QCoreApplication::setApplicationVersion("Nov 13th, 2023");

    MainWindow w;

    w.show();

    return a.exec();

}
