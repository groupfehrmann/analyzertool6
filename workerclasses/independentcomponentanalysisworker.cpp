#include "independentcomponentanalysisworker.h"

IndependentComponentAnalysisParameters::IndependentComponentAnalysisParameters(QObject *parent) :
    BaseParameters(parent, "independentcomponentanalysis")
{

}

bool IndependentComponentAnalysisParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    return true;

}

QString IndependentComponentAnalysisParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[independent componenent analysis on data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[contrast function = \"" << contrastFunction << "\"]";

    stream << Qt::endl << "\t[threshold cumulative explained variance in whitening step = " << threshold_cumulativeExplainedVariance << "]";

    stream << Qt::endl << "\t[threshold maximum number of components in whitening step = " << threshold_maximumNumberOfComponents << "]";

    stream << Qt::endl << "\t[maximum number of iterations = " << maximumNumberOfIterations << "]";

    stream << Qt::endl << "\t[finetune mode = " << ((finetuneMode) ? "enabled" : "disabled") << "]";

    stream << Qt::endl << "\t[stabilization mode = " << ((stabilizationMode) ? "enabled" : "disabled") << "]";

    stream << Qt::endl << "\t[step size (mu) in stabilization mode = " << mu << "]";

    stream << Qt::endl << "\t[stopping criteria (epsilon) = " << epsilon << "]";

    stream << Qt::endl << "\t[proportion of samples to use in an iteration = " << proportionOfSamplesToUseInAnIteration << "]";

    stream << Qt::endl << "\t[number of runs to perform = " << numberOfRunsToPerform << "]";

    if (numberOfRunsToPerform > 1) {

        stream << Qt::endl << "\t[pearson correlation threshold for clustering independent components from multiple runs = " << pearsonCorrelationThresholdForConsensus << "]";

        stream << Qt::endl << "\t[metric to determine independence = \"" << metricToDetermineIndependence << "\"]";

        stream << Qt::endl << "\t[distance or XI correlation threshold for clustering independent components from multiple runs = " << distanceCorrelationThresholdForConsensus << "]";

        stream << Qt::endl << "\t[threshold for credibility index = " << threshold_credibilityIndex << "]";

    }

    stream << Qt::endl << "\t[multithreading mode = " << multiThreadMode << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

IndependentComponentAnalysisWorker::IndependentComponentAnalysisWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("independent component analysis"), parameters, "independentcomponentanalysis")
{

}

void IndependentComponentAnalysisWorker::doWork_()
{

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("to perform independent component analysis values in data need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool IndependentComponentAnalysisWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void IndependentComponentAnalysisWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}

QList<double> IndependentComponentAnalysisWorker::performICARun(qsizetype runNumber, const QList<double> &whitenedMatrix, const QList<double> &whiteningMatrix, const QList<double> &dewhiteningMatrix, qsizetype numberOfValidComponents, int contrastFunction, const QList<QString> &componentLabels, int numberOfThreadsToUse)
{

    QString insertRunStringPrettyPrint;

    QString insertRunStringFileName;

    if (runNumber != -1)
        insertRunStringPrettyPrint = " run " + QString::number(runNumber) + " ";

    if (runNumber != -1)
        insertRunStringFileName = "_run_" + QString::number(runNumber);

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    bool succesfullConvergence = false;

    FastICA fastICA(whitenedMatrix, whiteningMatrix, dewhiteningMatrix, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), numberOfValidComponents, contrastFunction, parameters->maximumNumberOfIterations, parameters->finetuneMode, parameters->stabilizationMode, parameters->mu, parameters->epsilon, parameters->proportionOfSamplesToUseInAnIteration, numberOfThreadsToUse);

    fastICA.connectBaseWorker(this);

    if (runNumber != -1)
        fastICA.setLabel("ICA" + insertRunStringPrettyPrint);

    while (!succesfullConvergence) {

        succesfullConvergence = fastICA.run();

        if (!succesfullConvergence) {

            fastICA.setNumberOfThreadsToUse(parameters->numberOfThreadsToUse);

            if (runNumber != -1)
                fastICA.setLabel("rerun after failed convergence of ICA" + insertRunStringPrettyPrint);

        }

        if (this->thread()->isInterruptionRequested())
            return QList<double>();

        this->checkIfPauseWasRequested();

    }

    if (parameters->selectedResultItems.contains("ICA: mixing matrix (per run)")) {

        AnnotatedMatrix<double> *result_mixingMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, fastICA.mixingMatrix());

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA" + insertRunStringPrettyPrint + ": mixing matrix", result_mixingMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_mixingMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica" + insertRunStringFileName + "_mixing_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("ICA: separating matrix (per run)")) {

        AnnotatedMatrix<double> *result_seperatingMatrix = new AnnotatedMatrix<double>(componentLabels, parameters->selectedVariableIdentifiers, fastICA.seperatingMatrix());

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA" + insertRunStringPrettyPrint + ": separating matrix", result_seperatingMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_seperatingMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica" + insertRunStringFileName + "_separating_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("ICA: independent components (per run)")) {

        AnnotatedMatrix<double> *result_independentComponents = new AnnotatedMatrix<double>(parameters->selectedItemIdentifiers, componentLabels, MathOperations::transpose(fastICA.independentComponents(), parameters->selectedItemIndexes.count(), numberOfThreadsToUse));

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA" + insertRunStringPrettyPrint + ": independent components", result_independentComponents));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_independentComponents);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica" + insertRunStringFileName + "_independent_components");

        }

    }

    if (parameters->selectedResultItems.contains("ICA: XICOR or distance correlation between independent components (per run)")) {

        AnnotatedMatrix<double> *result_dCorrelationIndependentComponents;

        if (parameters->metricToDetermineIndependence == "XICOR")
            result_dCorrelationIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->xiCorrelationSymmetricalMatrix(fastICA.independentComponents(), parameters->selectedItemIndexes.count()));
        else
            result_dCorrelationIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->distanceCorrelationSymmetricalMatrix(fastICA.independentComponents(), parameters->selectedItemIndexes.count()));

        if (parameters->exportDirectory.isEmpty()) {

            if (parameters->metricToDetermineIndependence == "XICOR")
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA" + insertRunStringPrettyPrint + ": XICOR between independent components", result_dCorrelationIndependentComponents));
            else
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA" + insertRunStringPrettyPrint + ": distance correlation between independent components", result_dCorrelationIndependentComponents));

        } else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_dCorrelationIndependentComponents);

            if (parameters->metricToDetermineIndependence == "XICOR")
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica" + insertRunStringFileName + "_XICOR_independent_components");
            else
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica" + insertRunStringFileName + "_distance_correlation_independent_components");


        }

    }

    return fastICA.independentComponents();

}

QList<unsigned int> IndependentComponentAnalysisWorker::createMatrixWithCountForOverlappingNonNaNs(const QList<double> &matrix, qsizetype leadingDimension)
{

    QList<unsigned int> matrixWithCountForOverlappingNonNaNs;

    matrixWithCountForOverlappingNonNaNs.reserve(matrix.count());

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double> *>(&matrix), leadingDimension);

    qsizetype nRows = matrix.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("whitening step : creating matrix with counts for overlapping non-NaNs", 0, static_cast<qsizetype>( nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor = [this, &uuidProgress, &dispatcher](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<unsigned int> counts(dispatcher.count(), 0);

        this->updateProgressWithOne(uuidProgress);

        for (qsizetype i = index_span.first; i < dispatcher.count(); ++i) {

            counts[i] = MathDescriptives::countIfBothElementsAreNonNaNs(index_span.second, dispatcher.at(i).second);

            if (QThread::currentThread()->isInterruptionRequested())
                return counts;

            this->updateProgressWithOne(uuidProgress);

            this->checkIfPauseWasRequested();

        }

        return counts;

    };

    auto reduceFunctor = [](QList<unsigned int> &result, const QList<unsigned int> &intermediateResult) {

        result.append(intermediateResult);

    };

    return QtConcurrent::blockingMappedReduced<QList<unsigned int>>(&d_threadPool, dispatcher, std::function<QList<unsigned int>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<unsigned int> &, const QList<unsigned int> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}

QList<double> IndependentComponentAnalysisWorker::xiCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension)
{

    QList<double> copyOfVectors = vectors;

    copyOfVectors.detach();

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    this->startProgress("ranking vectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(copyOfVectors, leadingDimension), [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<double>();

    this->checkIfPauseWasRequested();

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(copyOfVectors, leadingDimension);

    qsizetype nRows = copyOfVectors.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("calculating XICOR", 0, static_cast<qsizetype>(nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor = [this, &uuidProgress, &nRows, &dispatcher](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> xiCorrelations(nRows, 0.0);

        this->updateProgressWithOne(uuidProgress);

        for (qsizetype i = index_span.first; i < nRows; ++i) {

            double xiCorrelation_1 = MathDescriptives::xiCorrelation_preRanked(index_span.second, dispatcher.at(i).second);

            double xiCorrelation_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher.at(i).second, index_span.second);

            xiCorrelations[i] = std::max(xiCorrelation_1, xiCorrelation_2);

            if (this->thread()->isInterruptionRequested())
                return xiCorrelations;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return xiCorrelations;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<double> mat = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->stopProgress(uuidProgress);

    return mat;

}

QList<double> IndependentComponentAnalysisWorker::distanceCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension)
{


    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    FastDistanceCorrrelationSymmetricMatrix fastDistanceCorrrelationSymmetricMatrix(vectors, leadingDimension, 1.0, parameters->numberOfThreadsToUse);

    fastDistanceCorrrelationSymmetricMatrix.connectBaseWorker(this);

    fastDistanceCorrrelationSymmetricMatrix.run();

    if (QThread::currentThread()->isInterruptionRequested())
        QList<double>();;

    this->checkIfPauseWasRequested();


    return fastDistanceCorrrelationSymmetricMatrix.symmetricMatrix();

}

QList<double> IndependentComponentAnalysisWorker::pearsonCorrelationSymmetricalMatrix(const QList<double> &matrix, qsizetype leadingDimension)
{

    QList<double> pearsonCorrelationMatrix;

    pearsonCorrelationMatrix.reserve(matrix.count());

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double> *>(&matrix), leadingDimension);

    qsizetype nRows = matrix.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("creating Pearson correlation matrix", 0, static_cast<qsizetype>( nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    qsizetype progress = 0.0;

    QMutex mutex;

    auto mapFunctor = [this, &uuidProgress, &dispatcher, &progress, &mutex](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> counts(dispatcher.count(), 0);

        for (qsizetype i = index_span.first; i < dispatcher.count(); ++i) {

            counts[i] = boost::math::statistics::correlation_coefficient(index_span.second, dispatcher.at(i).second);

        }

        mutex.lock();

        progress += dispatcher.count() - index_span.first;

        mutex.unlock();

        if (QThread::currentThread()->isInterruptionRequested())
            return counts;

        this->updateProgress(uuidProgress, progress);

        this->checkIfPauseWasRequested();

        return counts;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<double> result = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->stopProgress(uuidProgress);

    return result;

}

