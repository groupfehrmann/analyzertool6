#include "descriptivesworker.h"

DescriptivesParameters::DescriptivesParameters(QObject *parent) :
    BaseParameters(parent, "descriptives")
{

}

bool DescriptivesParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if ((annotationIndexDefiningItemSubsets != -1) && selectedItemSubsetIdentifiers.isEmpty()) {

        d_lastError = "annotation label defining item subsets selected, but no item subset identifiers were selected";

        return false;

    }

    if (descriptiveLabels.isEmpty()) {

        d_lastError = "no descriptives selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString DescriptivesParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[calculated descriptives on data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    if (annotationIndexDefiningItemSubsets != -1) {

        stream << Qt::endl << "\t[annotation label defining item subsets = \"" << annotationLabelDefiningItemSubsets << "\"]";

        stream << Qt::endl << "\t[selected item subset identifiers = [\"" << selectedItemSubsetIdentifiers.first() << "\", " << selectedItemSubsetIdentifiers.first() << "]";

        for (qsizetype i = 1; i < selectedItemSubsetIdentifiers.count(); ++i)
            stream << ", [\"" << selectedItemSubsetIdentifiers.at(i) << "\", " << selectedItemSubsetIdentifiers.at(i) << "]";

        stream << "]";

    }

    if (!annotationLabelsForVariables.isEmpty()) {

        stream << Qt::endl << "\t[selected annotation labels for variables = [\"" << annotationLabelsForVariables.first() << "\", " << annotationLabelsForVariables.first() << "]";

        for (qsizetype i = 1; i < annotationLabelsForVariables.count(); ++i)
            stream << ", [\"" << annotationLabelsForVariables.at(i) << "\", " << annotationLabelsForVariables.at(i) << "]";

        stream << "]";

    }

    stream << Qt::endl << "\t[selected descriptives = [\"" << descriptiveLabels.first() << "\"]";

    for (qsizetype i = 1; i < descriptiveLabels.count(); ++i)
        stream << ", [\"" << descriptiveLabels.at(i) << "\"]";

    stream << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

DescriptivesWorker::DescriptivesWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("descriptives"), parameters, "descriptives")
{

}

void DescriptivesWorker::doWork_()
{

    QSharedPointer<DescriptivesParameters> parameters(d_data->d_parameters.dynamicCast<DescriptivesParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("values in data need to be numerical to calculate descriptives");

        QThread::currentThread()->requestInterruption();

    }

}

bool DescriptivesWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<DescriptivesParameters> parameters(d_data->d_parameters.dynamicCast<DescriptivesParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void DescriptivesWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<DescriptivesParameters> parameters(d_data->d_parameters.dynamicCast<DescriptivesParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}

