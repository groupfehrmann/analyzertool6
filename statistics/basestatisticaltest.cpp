#include "basestatisticaltest.h"

BaseStatisticalTest::BaseStatisticalTest(const qsizetype &minimumNumberOfSamples,
                                         const qsizetype &maximumNumberOfSamples,
                                         const qsizetype &minimumNumberOfItemsPerSample,
                                         const QList<QString> &testDescriptiveLabels,
                                         const bool &effectSizeAvailable,
                                         const QString &effectSizeLabel, const std::function<double (const double &)> &backtransformEffectSizeFunction) :
    d_sampleSize(std::numeric_limits<qsizetype>::quiet_NaN()),
    d_statistic(std::numeric_limits<double>::quiet_NaN()),
    d_pValue(std::numeric_limits<double>::quiet_NaN()),
    d_effectSize(std::numeric_limits<double>::quiet_NaN()),
    d_standardErrorOfEffectSize(std::numeric_limits<double>::quiet_NaN()),
    d_minimumNumberOfSamples(minimumNumberOfSamples),
    d_maximumNumberOfSamples(maximumNumberOfSamples),
    d_minimumNumberOfItemsPerSample(minimumNumberOfItemsPerSample),
    d_testDescriptiveLabels(testDescriptiveLabels),
    d_effectSizeAvailable(effectSizeAvailable),
    d_effectSizeLabel(effectSizeLabel),
    d_backtransformEffectSizeFunction(backtransformEffectSizeFunction),
    d_isPValueCalculated(false),
    d_isStatisticCalculated(false),
    d_isTestDescriptiveValuesCalculated(false)
{

}

qsizetype BaseStatisticalTest::sampleSize()
{

    return d_sampleSize;

}

const double &BaseStatisticalTest::statistic()
{

    if (!d_isStatisticCalculated) {

        this->calculateStatistic();

        d_isStatisticCalculated = true;

    }

    return d_statistic;

}

const double &BaseStatisticalTest::pValue()
{

    if (!d_isPValueCalculated) {

        this->calculatePValue();

        d_isPValueCalculated = true;

    }

    return d_pValue;

}

double BaseStatisticalTest::zTransformedPValue()
{

    if (!d_isPValueCalculated) {

        this->calculatePValue();

        d_isPValueCalculated = true;

    }

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    boost::math::normal dist(0.0, 1.0);

    return (d_statistic < 0.0) ? -quantile(complement(dist, d_pValue / 2.0)) : quantile(complement(dist, d_pValue / 2.0));

}

const double &BaseStatisticalTest::effectSize()
{

    if (!d_isEffectSizeCalculated) {

        this->calculateEffectSize();

        d_isEffectSizeCalculated = true;

    }

    return d_effectSize;

}

double &BaseStatisticalTest::standardErrorOfEffectSize()
{

    if (!d_isStandardErrorOfEffectSizeCalculated) {

        this->calculateStandardErrorEffectSize();

        d_isStandardErrorOfEffectSizeCalculated = true;

    }

    return d_standardErrorOfEffectSize;

}

const QList<double> &BaseStatisticalTest::testDescriptiveValues()
{

    if (!d_isTestDescriptiveValuesCalculated) {

        this->calculateTestDescriptiveValues();

        d_isTestDescriptiveValuesCalculated = true;

    }

    return d_testDescriptiveValues;

}
