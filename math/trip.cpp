#include "trip.h"

void TRIP_transpose_algorithm::next(qsizetype &i, qsizetype &count, qsizetype j0, qsizetype j1, qsizetype N, qsizetype p)
{

    if (count == p - 1) {

        count = 0;

        i += (N - j1) + j0 + 1;

    } else {

        count += 1;

        i += 1;

    }

}

void TRIP_transpose_algorithm::prev(qsizetype &i, qsizetype &count, qsizetype j0, qsizetype j1, qsizetype N, qsizetype p)
{

    if (count == 0 ) {

        count = p - 1;

        i -= j0 + (N - j1) + 1;

    } else {

        count -= 1;

        i -= 1;

    }

}
