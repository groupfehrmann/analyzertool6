#include "fastdistancecorrelation.h"

double FastDistanceCorrelation::fastDistanceCorrelation(const QList<double> &x, const QList<double> &y, bool unbiased)
{

    QList<std::tuple<double, qsizetype, qsizetype, double> > xSortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(x);

    QList<std::tuple<double, qsizetype, qsizetype, double> > ySortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y);

    double variance_x = boost::math::statistics::variance(x);

    double variance_y = boost::math::statistics::variance(y);

    qsizetype L = static_cast<qsizetype>(std::ceil(std::log2(static_cast<double>(x.count()))));

    QList<qsizetype> powersOfTwo(L + static_cast<qsizetype>(2.0));

    for (qsizetype i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<int>(std::pow(2.0, static_cast<double>(i)));

    return fastDistanceCorrelation(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, unbiased);

}

double FastDistanceCorrelation::fastDistanceCorrelation(const std::span<double> &x, const std::span<double> &y, bool unbiased)
{

    qsizetype L = static_cast<int>(std::ceil(std::log2(static_cast<double>(x.size()))));

    QList<qsizetype> powersOfTwo(L + 2);

    for (qsizetype i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<int>(std::pow(2.0, static_cast<double>(i)));

    QList<std::tuple<double, qsizetype, qsizetype, double> > xSortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(x);

    QList<std::tuple<double, qsizetype, qsizetype, double> > ySortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y);

    double variance_x = boost::math::statistics::variance(x);

    double variance_y = boost::math::statistics::variance(y);

    return fastDistanceCorrelation(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, unbiased);

}

double FastDistanceCorrelation::fastDistanceCorrelation(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased)
{

    QList<double> parameters = FastDistanceCorrelation::fastDistanceCovariance(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, unbiased, true);

    double xDistanceVariance = parameters.at(1);

    double yDistanceVariance = parameters.at(2);

    return std::sqrt(parameters.at(0) / std::sqrt(xDistanceVariance * yDistanceVariance));

}

double FastDistanceCorrelation::fastDistanceCorrelation(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased)
{

    QList<double> parameters = FastDistanceCorrelation::fastDistanceCovariance(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, unbiased, true);

    double xDistanceVariance = parameters.at(1);

    double yDistanceVariance = parameters.at(2);

    return std::sqrt(parameters.at(0) / std::sqrt(xDistanceVariance * yDistanceVariance));

}

QList<double> FastDistanceCorrelation::fastDistanceCovariance(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased, bool allParameters)
{

    if (x.count() != y.count()) {

        if (allParameters)
            return QList<double>(3, std::numeric_limits<double>::quiet_NaN());
        else
            return QList<double>(1, std::numeric_limits<double>::quiet_NaN());

    }

    QList<double> sums = FastDistanceCorrelation::fastDistanceCovarianceSums(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, allParameters);

    double n = static_cast<double>(x.count());

    double d1;

    double d2;

    double d3;

    if (unbiased) {

        d1 = n * (n - 3.0);

        d2 = d1 * (n - 2.0);

        d3 = d2 * (n - 1.0);

    } else {

        d1 = n * n;

        d2 = n * n * n;

        d3 = n * n * n * n;

    }

    if (allParameters) {

        QList<double> results(3);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        results[1] = (sums.at(3) / d1) - (2.0 * sums.at(5) / d2) + (sums.at(7) / d3);

        results[2] = (sums.at(4) / d1) - (2.0 * sums.at(6) / d2) + (sums.at(8) / d3);

        return results;

    } else {

        QList<double> results(1);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        return results;

    }
}

QList<double> FastDistanceCorrelation::fastDistanceCovariance(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased, bool allParameters)
{

    if (x.size() != y.size()) {

        if (allParameters)
            return QList<double>(3, std::numeric_limits<double>::quiet_NaN());
        else
            return QList<double>(1, std::numeric_limits<double>::quiet_NaN());

    }

    QList<double> sums = FastDistanceCorrelation::fastDistanceCovarianceSums(x, y, xSortedOrderedRanked, ySortedOrderedRanked, variance_x, variance_y, powersOfTwo, L, allParameters);

    double n = static_cast<double>(x.size());

    double d1;

    double d2;

    double d3;

    if (unbiased) {

        d1 = n * (n - 3.0);

        d2 = d1 * (n - 2.0);

        d3 = d2 * (n - 1.0);

    } else {

        d1 = n * n;

        d2 = n * n * n;

        d3 = n * n * n *n;

    }

    if (allParameters) {

        QList<double> results(3);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        results[1] = (sums.at(3) / d1) - (2.0 * sums.at(5) / d2) + (sums.at(7) / d3);

        results[2] = (sums.at(4) / d1) - (2.0 * sums.at(6) / d2) + (sums.at(8) / d3);

        return results;

    } else {

        QList<double> results(1);

        results[0] = (sums.at(0) / d1) - (2.0 * sums.at(1) / d2) + (sums.at(2) / d3);

        return results;

    }
}

QList<double> FastDistanceCorrelation::fastDistanceCovarianceSums(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool allSums)
{

    if (x.count() != y.count()) {

        if (allSums)
            return QList<double>(9, std::numeric_limits<double>::quiet_NaN());
        else
            return QList<double>(3, std::numeric_limits<double>::quiet_NaN());

    }

    QList<double> x1(x.count());

    QList<double> y1(y.count());

    x1[0] = std::get<0>(xSortedOrderedRanked.at(0));

    y1[0] = y.at(std::get<1>(xSortedOrderedRanked.at(0)));

    for (qsizetype i = 1; i < x.count(); ++i) {

        const std::tuple<double, qsizetype, qsizetype, double> &tuple(xSortedOrderedRanked.at(i));

        x1[i] = std::get<0>(tuple);

        y1[i] = y.at(std::get<1>(tuple));

    }

    QList<double> adot(x.count());

    QList<double> bdot(x.count());

    double adotdot = 0.0;

    double bdotdot = 0.0;

    double sum2 = 0.0;

    double sizeX = static_cast<double>(x.count());

    double sizeY = static_cast<double>(y.count());

    double cumSumX = std::get<3>(xSortedOrderedRanked.back());

    double cumSumY = std::get<3>(ySortedOrderedRanked.back());

    QList<double> x1y1(x.count());

    for (qsizetype i = 0; i < x.count(); ++i) {

        qsizetype xalphai = std::get<2>(xSortedOrderedRanked.at(i)) - 1;

        qsizetype yalphai = std::get<2>(ySortedOrderedRanked.at(i)) - 1;


        const std::tuple<double, qsizetype, qsizetype, double> &xTuple(xSortedOrderedRanked.at(xalphai));

        const std::tuple<double, qsizetype, qsizetype, double> &yTuple(ySortedOrderedRanked.at(yalphai));


        double adoti = cumSumX + (2.0 * static_cast<double>(xalphai) - sizeX) * x.at(i) - 2.0 * (std::get<3>(xTuple) - std::get<0>(xTuple));

        double bdoti = cumSumY + (2.0 * static_cast<double>(yalphai) - sizeY) * y.at(i) - 2.0 * (std::get<3>(yTuple) - std::get<0>(yTuple));

        adot[i] = adoti;

        bdot[i] = bdoti;

        sum2 += adoti * bdoti;

        adotdot += adoti;

        bdotdot += bdoti;

        x1y1[i] = x1.at(i) * y1.at(i);

    }

    double sum3 = adotdot * bdotdot;

    QList<std::tuple<double, qsizetype, qsizetype, double> > y1SortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y1);

    QList<double> gamma_1 = FastDistanceCorrelation::partialSum2D(x1, y1, QList<double>(x.count(), 1.0), xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_x = FastDistanceCorrelation::partialSum2D(x1, y1, x1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_y = FastDistanceCorrelation::partialSum2D(x1, y1, y1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_xy = FastDistanceCorrelation::partialSum2D(x1, y1, x1y1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    double sum1 = 0.0;

    for (qsizetype i = 0; i < x.size(); ++i) {

        const double &xValue(x.at(i));

        const double &yValue(y.at(i));

        sum1 += (xValue * yValue * gamma_1.at(i)) + gamma_xy.at(i) - (xValue * gamma_y.at(i)) - (yValue * gamma_x.at(i));

    }

    QList<double> sums(9, std::numeric_limits<double>::quiet_NaN());

    sums[0] = sum1;

    sums[1] = sum2;

    sums[2] = sum3;

    if (allSums) {

        double n = static_cast<double>(x.size());

        sums[3] = 2.0 * n * (n - 1.0) * variance_x;

        sums[4] = 2.0 * n * (n - 1.0) * variance_y;


        double sum1 = cblas_ddot(adot.count(), adot.data(), 1, adot.data(), 1);

        double sum2 = cblas_ddot(bdot.count(), bdot.data(), 1, bdot.data(), 1);

        sums[5] = sum1;

        sums[6] = sum2;

        sums[7] = adotdot * adotdot;

        sums[8] = bdotdot * bdotdot;

    }

    return sums;

}

QList<double> FastDistanceCorrelation::fastDistanceCovarianceSums(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool allSums)
{

    if (x.size() != y.size()) {

        if (allSums)
            return QList<double>(9, std::numeric_limits<double>::quiet_NaN());
        else
            return QList<double>(3, std::numeric_limits<double>::quiet_NaN());

    }

    QList<double> x1(x.size());

    QList<double> y1(y.size());

    x1[0] = std::get<0>(xSortedOrderedRanked.at(0));

    y1[0] = y[std::get<1>(xSortedOrderedRanked.at(0))];

    for (unsigned long i = 1; i < x.size(); ++i) {

        const std::tuple<double, qsizetype, qsizetype, double> &tuple(xSortedOrderedRanked.at(i));

        x1[i] = std::get<0>(tuple);

        y1[i] = y[std::get<1>(tuple)];

    }

    QList<double> adot(x.size());

    QList<double> bdot(x.size());

    double adotdot = 0.0;

    double bdotdot = 0.0;

    double sum2 = 0.0;

    double sizeX = static_cast<double>(x.size());

    double sizeY = static_cast<double>(y.size());

    double cumSumX = std::get<3>(xSortedOrderedRanked.back());

    double cumSumY = std::get<3>(ySortedOrderedRanked.back());

    QList<double> x1y1(x.size());

    for (unsigned long i = 0; i < x.size(); ++i) {

        qsizetype xalphai = std::get<2>(xSortedOrderedRanked.at(i)) - 1;

        qsizetype yalphai = std::get<2>(ySortedOrderedRanked.at(i)) - 1;

        const std::tuple<double, qsizetype, qsizetype, double> &xTuple(xSortedOrderedRanked.at(xalphai));

        const std::tuple<double, qsizetype, qsizetype, double> &yTuple(ySortedOrderedRanked.at(yalphai));

        double adoti = cumSumX + (2.0 * static_cast<double>(xalphai) - sizeX) * x[i] - 2.0 * (std::get<3>(xTuple) - std::get<0>(xTuple));

        double bdoti = cumSumY + (2.0 * static_cast<double>(yalphai) - sizeY) * y[i] - 2.0 * (std::get<3>(yTuple) - std::get<0>(yTuple));

        adot[i] = adoti;

        bdot[i] = bdoti;

        sum2 += adoti * bdoti;

        adotdot += adoti;

        bdotdot += bdoti;

        x1y1[i] = x1.at(i) * y1.at(i);

    }

    double sum3 = adotdot * bdotdot;

    QList<std::tuple<double, qsizetype, qsizetype, double> > y1SortedOrderedRanked = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(y1);

    QList<double> gamma_1 = FastDistanceCorrelation::partialSum2D(x1, y1, QList<double>(x.size(), 1.0), xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_x = FastDistanceCorrelation::partialSum2D(x1, y1, x1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_y = FastDistanceCorrelation::partialSum2D(x1, y1, y1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    QList<double> gamma_xy = FastDistanceCorrelation::partialSum2D(x1, y1, x1y1, xSortedOrderedRanked, y1SortedOrderedRanked, powersOfTwo, L);

    double sum1 = 0.0;

    for (unsigned long i = 0; i < x.size(); ++i) {

        double &xValue(x[i]);

        double &yValue(y[i]);

        sum1 += (xValue * yValue * gamma_1.at(i)) + gamma_xy.at(i) - (xValue * gamma_y.at(i)) - (yValue * gamma_x.at(i));

    }

    QList<double> sums(9, std::numeric_limits<double>::quiet_NaN());

    sums[0] = sum1;

    sums[1] = sum2;

    sums[2] = sum3;

    if (allSums) {

        double n = static_cast<double>(x.size());

        sums[3] = 2.0 * n * (n - 1.0) * variance_x;

        sums[4] = 2.0 * n * (n - 1.0) * variance_y;

        double sum1 = cblas_ddot(adot.count(), adot.data(), 1, adot.data(), 1);

        double sum2 = cblas_ddot(bdot.count(), bdot.data(), 1, bdot.data(), 1);

        sums[5] = sum1;

        sums[6] = sum2;

        sums[7] = adotdot * adotdot;

        sums[8] = bdotdot * bdotdot;

    }

    return sums;

}

QList<double> FastDistanceCorrelation::partialSum2D(const QList<double> &x, const QList<double> &y, const QList<double> &c, const QList<std::tuple<double, qsizetype, qsizetype, double> > &x_orderStatistics_orderIndices_rank_cummulativeSum, const QList<std::tuple<double, qsizetype, qsizetype, double> > &y_orderStatistics_orderIndices_rank_cummulativeSum, QList<qsizetype> &powersOfTwo, qsizetype L)
{

    QList<double> xPartialSums(x.size(), 0.0);

    QList<double> yPartialSums(y.size(), 0.0);

    QList<double> yRanked(y.size());

    yRanked[0] = std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(0));

    double cdot = c.at(0);

    for (qsizetype i = 1; i < c.size(); ++i) {

        cdot += c.at(i);

        xPartialSums[i] = xPartialSums.at(i - 1) + c.at(i - 1);

        yPartialSums[i] = yPartialSums.at(i - 1) + c.at( std::get<1>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(i - 1)));

        yRanked[i] = std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(i));

    }

    QList<double> binaryTreeSums = FastDistanceCorrelation::binaryTreeSums(yRanked, c, powersOfTwo, L);

    QList<double> gamma(x.size());

    for (qsizetype i = 0; i < x.size(); ++i) {

        qsizetype xI = std::get<2>(x_orderStatistics_orderIndices_rank_cummulativeSum.at(i)) - 1;

        gamma[i] = cdot - c.at(xI) - 2.0 * yPartialSums.at(std::get<2>(y_orderStatistics_orderIndices_rank_cummulativeSum.at(xI)) - 1) - 2.0 * xPartialSums.at(xI) + 4.0 * binaryTreeSums.at(xI);

    }

    return gamma;

}

QList<double> FastDistanceCorrelation::binaryTreeSums(const QList<double> &y, const QList<double> &c, QList<qsizetype> &powersOfTwo, qsizetype L)
{

    qsizetype n = y.size();

    QList<double> s(powersOfTwo.at(L + 1), 0.0);

    QList<double> binaryTreeSums(n, 0.0);

    for (qsizetype i = 1; i < n; ++i) {

        double y_iminusone = y.at(i - 1);

        double c_iminusone = c.at(i - 1);

        qsizetype pos = ceil_(y_iminusone);

        s[pos - 1] += c_iminusone;

        for (qsizetype l = 1; l < L; ++l) {

            qsizetype pos = ceil_(y_iminusone / powersOfTwo.at(l));

            qsizetype scale = l;

            while (scale != 0) {

                --scale;

                pos += powersOfTwo.at(L - scale);

            }

            s[pos - 1] += c_iminusone;

        }

        double &binaryTreeSums_i(binaryTreeSums[i]);

        double y_i_minusone = y.at(i) - 1.0;

        pos = static_cast<int>(y_i_minusone);

        if ((static_cast<double>(pos) / 2.0) > static_cast<int>(pos / 2))
            binaryTreeSums_i += s.at(pos - 1);

        for (qsizetype l = 1; l < L; ++l) {

            pos = static_cast<int>(y_i_minusone / powersOfTwo.at(l));

            if ((static_cast<double>(pos) / 2.0) > static_cast<int>(pos / 2)) {

                qsizetype scale = l;

                while (scale != 0) {

                    --scale;

                    pos += powersOfTwo.at(L - scale);

                }

                binaryTreeSums_i += s.at(pos - 1);

           }

        }

    }

    return binaryTreeSums;

}

qsizetype FastDistanceCorrelation::ceil_(const double &x)
{

    return (qsizetype) x + (x > (qsizetype) x);

}

bool FastDistanceCorrelation::sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple(const std::tuple<double, qsizetype, qsizetype, double> &element1, const std::tuple<double, qsizetype, qsizetype, double> &element2)
{

    if (std::get<0>(element1) < std::get<0>(element2))
        return true;

    if (std::get<0>(element1) == std::get<0>(element2))
        return std::get<1>(element1) < std::get<1>(element2);
    else
        return false;

}

QList<std::tuple<double, qsizetype, qsizetype, double> > FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(const QList<double> vector)
{

    QList<std::tuple<double, qsizetype, qsizetype, double> > vector_orderStatistics_orderIndices_rank_cummulativeSum;

    vector_orderStatistics_orderIndices_rank_cummulativeSum.reserve(vector.size());

    for (qsizetype i = 0; i < vector.count(); ++i) {

        const double &value(vector.at(i));

        vector_orderStatistics_orderIndices_rank_cummulativeSum.push_back({value, i, 1, value});

    }

    std::sort(vector_orderStatistics_orderIndices_rank_cummulativeSum.begin(), vector_orderStatistics_orderIndices_rank_cummulativeSum.end(), FastDistanceCorrelation::sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple);

    std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(vector_orderStatistics_orderIndices_rank_cummulativeSum.first())]) = 1;

    for (qsizetype i = 1; i < vector_orderStatistics_orderIndices_rank_cummulativeSum.size(); ++i) {

        std::tuple<double, qsizetype, qsizetype, double> &tuple(vector_orderStatistics_orderIndices_rank_cummulativeSum[i]);

        std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(tuple)]) = i + static_cast<qsizetype>(1);

        std::get<3>(tuple) = std::get<3>(vector_orderStatistics_orderIndices_rank_cummulativeSum.at(i - 1)) + std::get<0>(tuple);

    }

    return vector_orderStatistics_orderIndices_rank_cummulativeSum;

}

QList<std::tuple<double, qsizetype, qsizetype, double>> FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(const std::span<double> vector)
{

    QList<std::tuple<double, qsizetype, qsizetype, double> > vector_orderStatistics_orderIndices_rank_cummulativeSum;

    vector_orderStatistics_orderIndices_rank_cummulativeSum.reserve(vector.size());

    for (unsigned long i = 0; i < vector.size(); ++i) {

        const double &value(vector[i]);

        vector_orderStatistics_orderIndices_rank_cummulativeSum.push_back({value, i, 1, value});

    }

    std::sort(vector_orderStatistics_orderIndices_rank_cummulativeSum.begin(), vector_orderStatistics_orderIndices_rank_cummulativeSum.end(), FastDistanceCorrelation::sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple);

    std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(vector_orderStatistics_orderIndices_rank_cummulativeSum.first())]) = 1;

    for (qsizetype i = 1; i < vector_orderStatistics_orderIndices_rank_cummulativeSum.size(); ++i) {

        std::tuple<double, qsizetype, qsizetype, double> &tuple(vector_orderStatistics_orderIndices_rank_cummulativeSum[i]);

        std::get<2>(vector_orderStatistics_orderIndices_rank_cummulativeSum[std::get<1>(tuple)]) = i + static_cast<qsizetype>(1);

        std::get<3>(tuple) = std::get<3>(vector_orderStatistics_orderIndices_rank_cummulativeSum.at(i - 1)) + std::get<0>(tuple);

    }

    return vector_orderStatistics_orderIndices_rank_cummulativeSum;

}

FastDistanceCorrrelationSymmetricMatrix::FastDistanceCorrrelationSymmetricMatrix(const QList<double> &matrix, qsizetype leadingDimension, double proportionOfItemsToUse, int numberOfThreadsToUse, QObject *parent) :
    QObject(parent),
    d_label("fast distance correlation matrix: "),
    d_leadingDimension(leadingDimension),
    d_numberOfThreadsToUse(numberOfThreadsToUse)
{

    if (proportionOfItemsToUse == 1.0)
        d_matrix = matrix;
    else {

        d_matrix = MathOperations::sampleColumns(matrix, leadingDimension, proportionOfItemsToUse, d_numberOfThreadsToUse);

        qsizetype numberOfRows = matrix.count() / leadingDimension;

        d_leadingDimension = d_matrix.count() / numberOfRows;

    }

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

}

bool FastDistanceCorrrelationSymmetricMatrix::run()
{

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    d_previousNumberOfThreadsToUse = MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines();

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

    auto mapFunctor1 = [](const std::span<double> &span) {

        return FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(span);

    };

    QList<std::span<double>> spans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<double> *>(&d_matrix), d_leadingDimension);

    QFuture<QList<std::tuple<double, qsizetype, qsizetype, double>>> future_vector_orderStatistics_orderIndices_rank_cummulativeSum = QtConcurrent::mapped(&d_threadPool, spans, std::function<QList<std::tuple<double, qsizetype, qsizetype, double>>(const std::span<double> &span)>(mapFunctor1));

    if (d_connectedBaseworker) {

        d_connectedBaseworker->startProgress(d_label + "creating auxillary data", future_vector_orderStatistics_orderIndices_rank_cummulativeSum);

        if (d_connectedBaseworker->thread()->isInterruptionRequested()) {

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }
        d_connectedBaseworker->checkIfPauseWasRequested();

    } else if (QThread::currentThread()->isInterruptionRequested()) {

        d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

        MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

        return false;
    }

    future_vector_orderStatistics_orderIndices_rank_cummulativeSum.waitForFinished();

    auto mapFunctor2 = [](const std::span<double> &span) {

        return boost::math::statistics::sample_variance(span);

    };

    QFuture<double> future_variance = QtConcurrent::mapped(&d_threadPool, spans, std::function<double (const std::span<double> &span)>(mapFunctor2));

    if (d_connectedBaseworker) {

        d_connectedBaseworker->startProgress(d_label + "calculating variance", future_variance);

        if (d_connectedBaseworker->thread()->isInterruptionRequested()) {

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }

        d_connectedBaseworker->checkIfPauseWasRequested();

    } else if (QThread::currentThread()->isInterruptionRequested()) {

        d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

        MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

        return false;
    }

    future_variance.waitForFinished();

    qsizetype L = static_cast<int>(std::ceil(std::log2(static_cast<double>(d_leadingDimension))));

    QList<qsizetype> powersOfTwo(L + 2);

    for (qsizetype i = 0; i <= L + 1; ++i)
        powersOfTwo[i] = static_cast<qsizetype>(std::pow(2.0, static_cast<double>(i)));

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double> *>(&d_matrix), d_leadingDimension);

    QUuid uuidProgress;

    qsizetype nRows = d_matrix.count() / d_leadingDimension;

    if (d_connectedBaseworker)
        uuidProgress = d_connectedBaseworker->startProgress(d_label + "calculating distance correlations", 0, static_cast<qsizetype>( nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor3 = [this, &uuidProgress, &future_vector_orderStatistics_orderIndices_rank_cummulativeSum, &future_variance, &dispatcher, &L, &powersOfTwo](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> distanceCorrelations(dispatcher.count(), 0.0);

        if (d_connectedBaseworker)
            d_connectedBaseworker->updateProgressWithOne(uuidProgress);

        distanceCorrelations[index_span.first] = 1.0;

        for (qsizetype i = index_span.first + 1; i < dispatcher.count(); ++i) {

            distanceCorrelations[i] = FastDistanceCorrelation::fastDistanceCorrelation(index_span.second, dispatcher.at(i).second, future_vector_orderStatistics_orderIndices_rank_cummulativeSum.resultAt(index_span.first), future_vector_orderStatistics_orderIndices_rank_cummulativeSum.resultAt(i), future_variance.resultAt(index_span.first), future_variance.resultAt(i), powersOfTwo, L);

            if (d_connectedBaseworker) {

                if (d_connectedBaseworker->thread()->isInterruptionRequested()) {

                    d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

                    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

                    return distanceCorrelations;

                }
                d_connectedBaseworker->checkIfPauseWasRequested();

                d_connectedBaseworker->updateProgressWithOne(uuidProgress);

            } else if (QThread::currentThread()->isInterruptionRequested()) {

                d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

                MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

                return distanceCorrelations;

            }

        }

        return distanceCorrelations;

    };

    auto reduceFunctor3 = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    d_symmetricMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor3), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor3), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested()) {

        d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

        MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

        return false;

    }
    if (d_connectedBaseworker) {

        d_connectedBaseworker->stopProgress(uuidProgress);

        d_connectedBaseworker->checkIfPauseWasRequested();

    }

    d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

    return true;

}

const QList<double> &FastDistanceCorrrelationSymmetricMatrix::symmetricMatrix() const
{

    return d_symmetricMatrix;

}

void FastDistanceCorrrelationSymmetricMatrix::connectBaseWorker(BaseWorker *baseWorker)
{

    d_connectedBaseworker = QPointer<BaseWorker>(baseWorker);

}

void FastDistanceCorrrelationSymmetricMatrix::setLabel(const QString &label)
{

    d_label = label;

}
