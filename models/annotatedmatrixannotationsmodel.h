#ifndef ANNOTATEDMATRIXANNOTATIONSMODEL_H
#define ANNOTATEDMATRIXANNOTATIONSMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>
#include <QColor>
#include <QSortFilterProxyModel>

#include "containers/annotatedmatrix.h"

class AnnotatedMatrixAnnotationsSortFilterProxyModel : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    explicit AnnotatedMatrixAnnotationsSortFilterProxyModel(QObject *parent = nullptr, Qt::Orientation orientation = Qt::Vertical);

    bool showSelectedOnly();

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

    bool filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const;

private:

    Qt::Orientation d_orientation;

    bool d_showSelectedOnly;

public slots:

    void setShowSelectedOnly(bool showSelectedOnly);

};

class AnnotatedMatrixAnnotationsModel : public QAbstractTableModel
{

    Q_OBJECT

public:

    explicit AnnotatedMatrixAnnotationsModel(QObject *parent = nullptr, QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = nullptr, Qt::Orientation orientation = Qt::Vertical);

    const BaseAnnotatedMatrix *baseAnnotatedMatrix() const;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

private:

    QSharedPointer<BaseAnnotatedMatrix> d_baseAnnotatedMatrix;

    Qt::Orientation d_orientation;

};

#endif // ANNOTATEDMATRIXANNOTATIONSMODEL_H
