#include "datarepositorymodel.h"

DataRepositoryModel::DataRepositoryModel(QObject *parent, const QSharedPointer<DataRepository> &dataRepository) :
    QAbstractItemModel(parent),
    d_dataRepository(dataRepository)
{

    this->connect(d_dataRepository.data(), &DataRepository::currentUuidChanged, this, &DataRepositoryModel::currentUuidChanged);

}

int DataRepositoryModel::columnCount(const QModelIndex &parent) const
{

    Q_UNUSED(parent);

    return 1;

}

const QUuid &DataRepositoryModel::currentUuid() const
{

    return d_dataRepository->currentUuid();

}

QVariant DataRepositoryModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    QUuid uuid = *static_cast<const QUuid *>(index.internalPointer());

    if (d_dataRepository->isUuidProject(uuid)) {

        if (role == Qt::DisplayRole)
            return "project: \"" + d_dataRepository->projectLabel(index.row()) + "\"";
        else if (role == Qt::ForegroundRole)
            return QBrush(Qt::black);

    } else if (d_dataRepository->isUuidAnnotatedMatrix(uuid)) {

        if (role == Qt::DisplayRole)
            return "[data]: \"" + d_dataRepository->objectPointer(uuid)->property("label").toString() + "\"";
        else if (role == Qt::EditRole)
            return d_dataRepository->objectPointer(uuid)->property("label").toString();
        else if (role == Qt::ForegroundRole)
            return QBrush(Qt::black);

    } else if (d_dataRepository->isUuidResult(uuid)) {

        if (role == Qt::DisplayRole) {

            if (d_dataRepository->objectPointer(uuid)->property("resultType").toString() == "table")
                return "[result-table]: \"" + d_dataRepository->objectPointer(uuid)->property("label").toString() + "\"";
            else
                return "ERROR";

        } else if (role == Qt::EditRole)
            return d_dataRepository->objectPointer(uuid)->property("label");
        else if (role == Qt::ForegroundRole)
            return QBrush(Qt::black);

    }

    if (role == Qt::UserRole)
        return uuid;

    return QVariant();

}

Qt::ItemFlags DataRepositoryModel::flags(const QModelIndex &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;

   return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;

}

QModelIndex DataRepositoryModel::index(const QUuid &uuid) const
{

    if (!d_dataRepository->isValidUuid(uuid))
        return QModelIndex();

    if (d_dataRepository->isUuidProject(uuid))
        return this->index(d_dataRepository->indexOfUuidProject(uuid), 0);

    QUuid uuidProject = d_dataRepository->uuidProject(uuid);

    QModelIndex parent = this->index(d_dataRepository->indexOfUuidProject(uuidProject), 0);

    if (d_dataRepository->isUuidAnnotatedMatrix(uuid))
        return this->index(d_dataRepository->indexOfUuidAnnotatedMatrix(uuid), 0, parent);
    else
        return this->index(d_dataRepository->indexOfUuidResult(uuid) + d_dataRepository->numberOfAnnotatedMatrices(uuidProject), 0, parent);

}

QModelIndex DataRepositoryModel::index(int row, int column, const QModelIndex &parent) const
{

    if (!this->hasIndex(row, column, parent))
        return QModelIndex();

    if (!parent.isValid())
        return this->createIndex(row, column, &d_dataRepository->uuidsProject().at(row));

    QUuid uuid = *static_cast<const QUuid *>(parent.internalPointer());

    const QList<QUuid> &uuidsAnnotatedMatrix(d_dataRepository->uuidsAnnotatedMatrix(uuid));

    if (row < uuidsAnnotatedMatrix.count())
        return this->createIndex(row, column, &uuidsAnnotatedMatrix.at(row));
    else
        return this->createIndex(row, column, &d_dataRepository->uuidsResult(uuid).at(row - uuidsAnnotatedMatrix.count()));

}

QModelIndex DataRepositoryModel::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    QUuid uuid = *static_cast<const QUuid *>(index.internalPointer());

    if (d_dataRepository->isUuidProject(uuid))
        return QModelIndex();

    if (d_dataRepository->isUuidAnnotatedMatrix(uuid) || d_dataRepository->isUuidResult(uuid)) {

        qsizetype indexOfUuidProject = d_dataRepository->indexOfUuidProject(d_dataRepository->uuidProject(uuid));

        return this->createIndex(indexOfUuidProject, 0, &d_dataRepository->uuidsProject().at(indexOfUuidProject));

    }

    return QModelIndex();

}

int DataRepositoryModel::rowCount(const QModelIndex &parent) const
{

    if (!parent.isValid())
        return d_dataRepository->uuidsProject().count();

    QUuid uuid = *static_cast<const QUuid *>(parent.internalPointer());

    if (d_dataRepository->isUuidProject(uuid))
        return d_dataRepository->uuidsAnnotatedMatrix(uuid).count() + d_dataRepository->uuidsResult(uuid).count();
    else
        return 0;

}

bool DataRepositoryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    if (data(index, role) != value) {

        QUuid uuid = *static_cast<const QUuid *>(index.internalPointer());

        if (d_dataRepository->isUuidProject(uuid)) {

            if (role == Qt::EditRole)
                d_dataRepository->setProjectLabel(index.row(), value.toString());

        } else {

            if (role == Qt::EditRole)
                d_dataRepository->objectPointer(uuid)->setProperty("label", value.toString());

        }

        emit dataChanged(index, index, QVector<int>() << role);

        return true;

    }

    return false;

}

QUuid DataRepositoryModel::addObject(const QUuid &uuidProject, QObject *object)
{

    QUuid uuid;

    if (d_dataRepository->isUuidProject(uuidProject))
        uuid = uuidProject;
    else {

        if (uuidProject == QUuid())
            uuid = d_dataRepository->currentUuidProject();
        else if(d_dataRepository->isUuidAnnotatedMatrix(uuidProject) || d_dataRepository->isUuidResult(uuidProject))
            uuid = d_dataRepository->uuidProject(uuidProject);
        else
            uuid = uuidProject;

    }

    QString objectType = object->property("objectType").toString();

    if (objectType == "annotated matrix") {

        if (d_dataRepository->isUuidProject(uuid)) {

            qsizetype row = d_dataRepository->uuidsAnnotatedMatrix(uuid).count();

            this->beginInsertRows(this->index(d_dataRepository->indexOfUuidProject(uuid), 0, QModelIndex()), row, row);

        } else {

            qsizetype row = d_dataRepository->uuidsProject().count();

            this->beginInsertRows(QModelIndex(), row, row);

        }

    } else if (objectType == "result") {

        if (d_dataRepository->isUuidProject(uuid)) {

            qsizetype row = d_dataRepository->uuidsAnnotatedMatrix(uuid).count() + d_dataRepository->uuidsResult(uuid).count();

            this->beginInsertRows(this->index(d_dataRepository->indexOfUuidProject(uuid), 0, QModelIndex()), row, row);

        } else {

            qsizetype row = d_dataRepository->uuidsProject().count();

            this->beginInsertRows(QModelIndex(), row, row);

        }

    }

    QUuid uuidObject = d_dataRepository->addObject(uuid, object);

    this->endInsertRows();

    emit objectAdded(uuidObject, d_dataRepository->objectPointer(uuidObject));

    return uuidObject;

}

QUuid DataRepositoryModel::addSharedPointerToObject(const QUuid &uuidProject, QSharedPointer<QObject> object)
{

    QUuid uuid;

    if (d_dataRepository->isUuidProject(uuidProject))
        uuid = uuidProject;
    else {

        if (uuidProject == QUuid())
            uuid = d_dataRepository->currentUuidProject();
        else if(d_dataRepository->isUuidAnnotatedMatrix(uuidProject) || d_dataRepository->isUuidResult(uuidProject))
            uuid = d_dataRepository->uuidProject(uuidProject);
        else
            uuid = uuidProject;

    }

    QString objectType = object->property("objectType").toString();

    if (objectType == "annotated matrix") {

        if (d_dataRepository->isUuidProject(uuid)) {

            qsizetype row = d_dataRepository->uuidsAnnotatedMatrix(uuid).count();

            this->beginInsertRows(this->index(d_dataRepository->indexOfUuidProject(uuid), 0, QModelIndex()), row, row);

        } else {

            qsizetype row = d_dataRepository->uuidsProject().count();

            this->beginInsertRows(QModelIndex(), row, row);

        }

    } else if (objectType == "result") {

        if (d_dataRepository->isUuidProject(uuid)) {

            qsizetype row = d_dataRepository->uuidsAnnotatedMatrix(uuid).count() + d_dataRepository->uuidsResult(uuid).count();

            this->beginInsertRows(this->index(d_dataRepository->indexOfUuidProject(uuid), 0, QModelIndex()), row, row);

        } else {

            qsizetype row = d_dataRepository->uuidsProject().count();

            this->beginInsertRows(QModelIndex(), row, row);

        }

    }

    QUuid uuidObject = d_dataRepository->addSharedPointerToObject(uuid, object);

    this->endInsertRows();

    emit objectAdded(uuidObject, d_dataRepository->objectPointer(uuidObject));

    return uuidObject;

}

QUuid DataRepositoryModel::addProject()
{

    qsizetype row = d_dataRepository->uuidsProject().count();

    this->beginInsertRows(QModelIndex(), row, row);

    QUuid uuidProject = d_dataRepository->addProject();

    this->endInsertRows();

    return uuidProject;

}

void DataRepositoryModel::removeAll()
{

    this->beginResetModel();

    d_dataRepository->removeAll();

    this->endResetModel();

}

void DataRepositoryModel::removeAllDataAssociatedWithUuid(const QUuid &uuid)
{

    if (d_dataRepository->isUuidProject(uuid)) {

        qsizetype row = d_dataRepository->indexOfUuidProject(uuid);

        this->beginRemoveRows(QModelIndex(), row, row);

        d_dataRepository->removeAllDataAssociatedWithUuid(uuid);

        this->endRemoveRows();

    } else if (d_dataRepository->isUuidAnnotatedMatrix(uuid)) {

        qsizetype row = d_dataRepository->indexOfUuidAnnotatedMatrix(uuid);

        QModelIndex parent = this->index(d_dataRepository->uuidProject(uuid));

        this->beginRemoveRows(parent, row, row);

        d_dataRepository->removeAllDataAssociatedWithUuid(uuid);

        this->endRemoveRows();

        emit this->objectRemoved(uuid);

    }  else if (d_dataRepository->isUuidResult(uuid)) {

        qsizetype row = d_dataRepository->indexOfUuidResult(uuid);

        QUuid uuidProject = d_dataRepository->uuidProject(uuid);

        row += d_dataRepository->numberOfAnnotatedMatrices(uuidProject);

        QModelIndex parent = this->index(uuidProject);

        this->beginRemoveRows(parent, row, row);

        d_dataRepository->removeAllDataAssociatedWithUuid(uuid);

        this->endRemoveRows();

        emit this->objectRemoved(uuid);

    }

}

void DataRepositoryModel::convertToData(const QUuid &uuid)
{

    QSharedPointer<QObject> object = d_dataRepository->objectPointer(uuid);

    if (object->property("resultType").toString() != "table")
        return;

    QUuid uuidProject = object->property("uuidProject").toUuid();

    QSharedPointer<QObject> objectInternal = qSharedPointerDynamicCast<Result>(object)->internalObject();

    objectInternal->setProperty("objectType", "annotated matrix");

    objectInternal->setProperty("resultType", QVariant());

    objectInternal->setProperty("label", object->property("label"));

    this->removeAllDataAssociatedWithUuid(uuid);

    this->addSharedPointerToObject(uuidProject, objectInternal);

}

void DataRepositoryModel::setCurrentUuid(const QUuid &uuid)
{

    d_dataRepository->setCurrentUuid(uuid);

}
