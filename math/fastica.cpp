#include "fastica.h"

FastICA::FastICA(const QList<double> &whitenedMatrix, const QList<double> &whiteningMatrix, const QList<double> &dewhiteningMatrix, qsizetype nSamples, qsizetype nItems, qsizetype nComponents, int contrastFunction, int maximumNumberOfIterations, bool finetuneMode, bool stabilizationMode, double mu, double epsilon, double proportionOfItemsToUseInAnIteration, int numberOfThreadsToUse, QObject *parent) :
    QObject(parent),
    d_label("fastICA"),
    d_whitenedMatrix(whitenedMatrix),
    d_whiteningMatrix(whiteningMatrix),
    d_dewhiteningMatrix(dewhiteningMatrix),
    d_nSamples(nSamples),
    d_nItems(nItems),
    d_nSubItems(static_cast<int>(std::floor(static_cast<double>(nItems) * proportionOfItemsToUseInAnIteration))),
    d_nComponents(nComponents),
    d_contrastFunction(contrastFunction),
    d_maximumNumberOfIterations(maximumNumberOfIterations),
    d_finetuneMode(finetuneMode),
    d_stabilizationMode(stabilizationMode),
    d_mu(mu),
    d_epsilon(epsilon),
    d_proportionOfItemsToUseInAnIteration(proportionOfItemsToUseInAnIteration),
    d_numberOfThreadsToUse(numberOfThreadsToUse)
{

}

bool FastICA::run()
{

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    d_previousNumberOfThreadsToUse = MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines();

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

    int gOrig = d_contrastFunction;

    int gFine = d_contrastFunction + 1;

    double myyOrig = d_mu;

    double myyK = 0.01;

    int usedNlinearity = 0;

    double stroke = 0.0;

    int notFine = 1;

    int loong = 0;

    double a1 = 1.0;

    double a2 = 1.0;

    int initialStateMode = FICA_INIT_RAND; // error if 1, need to fix!

    double minAbsCos = 0.0;

    double minAbsCos2 = 0.0;

    QUuid uuidProgress;



    if (static_cast<qsizetype>(std::floor(static_cast<double>(d_nItems) * d_proportionOfItemsToUseInAnIteration)) < 1000)
        d_proportionOfItemsToUseInAnIteration = (1000.0 / static_cast<double>(d_nItems) < 1.0) ? (1000.0 / static_cast<double>(d_nItems)) : 1.0;

    if (d_proportionOfItemsToUseInAnIteration != 1.0)
        gOrig += 2;

    if ((d_mu != 1.0) || (d_stabilizationMode))
        gOrig += 1;

    int fineTuningEnabled = 1;

    if (!d_finetuneMode) {

        if ((d_mu != 1.0) || (d_stabilizationMode))
            gFine = gOrig;
        else
            gFine = gOrig + 1;

        fineTuningEnabled = 0;

    }

    int stabilizationEnabled = d_stabilizationMode;

    if (!d_stabilizationMode && d_mu != 1.0)
        stabilizationEnabled = true;

    usedNlinearity = gOrig;

    d_B.resize(d_nComponents * d_nComponents);

    if (initialStateMode == 0) {

        if (d_connectedBaseworker)
            uuidProgress = d_connectedBaseworker->startProgress("Initializing", 0, 0);

        MathOperations::fillWithRandomNumbersFromUniformRealDistribution(d_B, -0.5, 0.5);

        int info;

        MathOperations::orthogonalizeMatrixInplace(d_B, d_nComponents, d_numberOfThreadsToUse, &info);

        if (d_connectedBaseworker) {

            if (info < 0)
                d_connectedBaseworker->reportError(d_label + ": DGESDD: " + QString::number(info) + "th parameter had illegal value");
            else if (info > 0)
                d_connectedBaseworker->reportError(d_label + ": DGESDD: Error code -> " + QString::number(info));

            d_connectedBaseworker->stopProgress(uuidProgress);

        }

        if (info != 0)
            return false;

    } else {

        QList<double> guess(d_nComponents * d_nComponents);

        MathOperations::fillWithRandomNumbersFromUniformRealDistribution(guess, -0.5, 0.5);
// need to fix, multiplication dimension not okay!
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSamples, 1.0, d_whiteningMatrix.data(), d_nSamples, guess.data(), d_nComponents, 0.0, d_B.data(), d_nComponents);

    }

    QList<double> BOld(d_B.count(), 0.0);

    QList<double> BOld2(d_B.count(), 0.0);

    if (d_connectedBaseworker)
        uuidProgress = d_connectedBaseworker->startProgress(d_label + ": convergences to epsilon", 0, -10000000 * std::log10(d_epsilon));

    for (int round = 0; round < d_maximumNumberOfIterations; round++) {

        if (round != 0) {

            if (stabilizationEnabled)
                d_connectedBaseworker->updateProgressLabel(uuidProgress, d_label + ": converge - iteration " + QString::number(round + 1) + " - mu " + QString::number(d_mu) + " - convergence change: " + QString::number(1.0 - minAbsCos));
            else
                d_connectedBaseworker->updateProgressLabel(uuidProgress, d_label + ": converge - iteration " + QString::number(round + 1) + " - convergence change: " + QString::number(1.0 - minAbsCos));

        }


        if (round == d_maximumNumberOfIterations - 1) {

            if (d_connectedBaseworker)
                d_connectedBaseworker->reportWarning(d_label + ": no convergence reached in FastICA");

            BOld.clear();

            BOld2.clear();

            d_mixingMatrix.resize(d_nSamples * d_nComponents, 0.0);

            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nSamples, d_nComponents, d_nComponents, 1.0, d_dewhiteningMatrix.data(), d_nComponents, d_B.data(), d_nComponents, 0.0, d_mixingMatrix.data(), d_nComponents);

            d_seperatingMatrix.resize(d_nComponents * d_nSamples, 0.0);

            cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nSamples, d_nComponents, 1.0, d_B.data(), d_nComponents, d_whiteningMatrix.data(), d_nSamples, 0.0, d_seperatingMatrix.data(), d_nSamples);

            if (d_connectedBaseworker)
                d_connectedBaseworker->stopProgress(uuidProgress);

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }

        QList<double> BTransposed = MathOperations::transpose(d_B, d_nComponents, d_numberOfThreadsToUse);

        MathOperations::createSymmetricMatrixInPlace(BTransposed, d_nComponents, d_numberOfThreadsToUse);

        int info;

        MathOperations::mPowerSymmetricMatrixInplace(BTransposed, d_nComponents, -0.5, d_numberOfThreadsToUse, &info);

        if (d_connectedBaseworker) {

            if (info < 0)
                d_connectedBaseworker->reportError(d_label + ": mPower:DSYEVR: " + QString::number(info) + "th parameter had illegal value");
            else if (info > 0)
                d_connectedBaseworker->reportError(d_label + ": mPower:DSYEVR: Error code -> " + QString::number(info));

            if (info != 0)
                d_connectedBaseworker->stopProgress(uuidProgress);

        }

        if (info != 0)
            return false;

        QList<double> mat(d_B.count(), 0.0);

        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, 1.0, d_B.data(), d_nComponents, BTransposed.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

        d_B = mat;

        BTransposed = MathOperations::transpose(d_B, d_nComponents, d_numberOfThreadsToUse);

        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, 1.0, BTransposed.data(), d_nComponents, BOld.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

        minAbsCos = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquaredMatrix(mat, d_nComponents);

        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, 1.0, BTransposed.data(), d_nComponents, BOld2.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

        minAbsCos2 = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquaredMatrix(mat, d_nComponents);

        BTransposed.clear();

        BTransposed.squeeze();

        if (std::fabs(1.0 - minAbsCos) < d_epsilon) {

            if (fineTuningEnabled && notFine) {

                notFine = 0;

                usedNlinearity = gFine;

                d_mu = myyK * myyOrig;

                BOld.fill(0.0);

                BOld2.fill(0.0);

            } else {

                if (d_connectedBaseworker)
                    d_connectedBaseworker->reportMessage(d_label + ": convergence after " + QString::number(round) + " iterations");

                BOld.clear();

                BOld2.clear();

                mat.clear();

                d_mixingMatrix.resize(d_nSamples * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nSamples, d_nComponents, d_nComponents, 1.0, d_dewhiteningMatrix.data(), d_nComponents, d_B.data(), d_nComponents, 0.0, d_mixingMatrix.data(), d_nComponents);

                break;

            }

        } else if (stabilizationEnabled) {

            if (!static_cast<bool>(stroke) && (std::fabs(1.0 - minAbsCos2) < d_epsilon)) {

                if (d_connectedBaseworker)
                    d_connectedBaseworker->reportWarning(d_label + ": stroke! Reducing step size by a factor of two");

                stroke = d_mu;

                d_mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            } else if (static_cast<bool>(stroke)) {

                d_mu = stroke;

                stroke = 0;

                if ((std::fabs(d_mu - 1.0) < std::numeric_limits<double>::epsilon()) && ((usedNlinearity % 2) != 0))
                    usedNlinearity -= 1;

            } else if (!loong && (round > d_maximumNumberOfIterations / 2)) {

                if (d_connectedBaseworker)
                    d_connectedBaseworker->reportWarning(d_label + ": convergence is taking to long, reducing step size by a factor of two");

                loong = 1;

                d_mu /= double(2);

                if ((usedNlinearity % 2) == 0)
                    usedNlinearity += 1;

            }

        }

        BOld2 = BOld;

        BOld = d_B;

        switch (usedNlinearity) {

            case FICA_NONLIN_POW3 : {

                QList<double> pow3(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, pow3.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(pow3, [](double &value){value = value * value * value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0 / d_nItems, d_whitenedMatrix.data(), d_nItems, pow3.data(), d_nComponents, -3.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_POW3 + 1) : {

                QList<double> Y(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> pow3 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value * value;}, d_numberOfThreadsToUse);

                QList<double> Beta1 = MathDescriptives::sumOfColumns(MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value * value * value;}, d_numberOfThreadsToUse), d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = Beta1;

                MathOperations::applyFunctor_elementWise_inplace(D, [this](double &value){value = 1.0 / (value - 3.0 * d_nItems);}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0, Y.data(), d_nComponents, pow3.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                pow3.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case(FICA_NONLIN_POW3 + 2) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> pow3(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, pow3.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(pow3, [](double &value){value = value * value * value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0 / d_nSubItems, subWhitenedMatrix.data(), d_nSubItems, pow3.data(), d_nComponents, -3.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_POW3 + 3) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> Y(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                subWhitenedMatrix.clear();

                QList<double> pow3 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value * value;}, d_numberOfThreadsToUse);

                QList<double> Beta1 = MathDescriptives::sumOfColumns(MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value * value * value;}, d_numberOfThreadsToUse), d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = Beta1;

                MathOperations::applyFunctor_elementWise_inplace(D, [this](double &value){value = 1.0 / (value - 3.0 * d_nSubItems);}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0, Y.data(), d_nComponents, pow3.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                pow3.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case FICA_NONLIN_TANH : {

                QList<double> hypTan(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, a1, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, hypTan.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(hypTan, [](double &value){value = std::tanh(value);}, d_numberOfThreadsToUse);

                QList<double> Beta(hypTan.count(), 1.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, hypTan.count(), 0, -1.0, hypTan.data(), 1, hypTan.data(), 1, 1.0, Beta.data(), 1);

                Beta = MathDescriptives::sumOfColumns(Beta, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = MathOperations::repeatVector(Beta, d_nComponents);

                Beta.clear();

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, (1.0 / d_nItems * a1), D.data(), 1, d_B.data(), 1, 0.0, mat.data(), 1);

                D.clear();

                d_B = mat;

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0 / d_nItems, d_whitenedMatrix.data(), d_nItems, hypTan.data(), d_nComponents, -1.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_TANH + 1) : {

                QList<double> Y(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, a1, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> hypTan = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return std::tanh(value);}, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, hypTan.data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> Beta2(hypTan.count(), 1.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, hypTan.count(), 0, -1.0, hypTan.data(), 1, hypTan.data(), 1, 1.0, Beta2.data(), 1);

                Beta2 = MathDescriptives::sumOfColumns(Beta2, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = Beta1;

                cblas_daxpy(Beta2.count(), -a1, Beta2.data(), 1, D.data(), 1);

                Beta2.clear();

                MathOperations::applyFunctor_elementWise_inplace(D, [](double &value){value = 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0, Y.data(), d_nComponents, hypTan.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case(FICA_NONLIN_TANH + 2) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> hypTan(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, a1, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, hypTan.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(hypTan, [](double &value){value = std::tanh(value);}, d_numberOfThreadsToUse);

                QList<double> Beta(hypTan.count(), 1.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, hypTan.count(), 0, -1.0, hypTan.data(), 1, hypTan.data(), 1, 1.0, Beta.data(), 1);

                Beta = MathDescriptives::sumOfColumns(Beta, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = MathOperations::repeatVector(Beta, d_nComponents);

                Beta.clear();

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, (1.0 / d_nSubItems * a1), D.data(), 1, d_B.data(), 1, 0.0, mat.data(), 1);

                D.clear();

                d_B = mat;

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0 / d_nSubItems, subWhitenedMatrix.data(), d_nSubItems, hypTan.data(), d_nComponents, -1.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_TANH + 3) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> Y(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, a1, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                subWhitenedMatrix.clear();

                QList<double> hypTan = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return std::tanh(value);}, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, hypTan.data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> Beta2(hypTan.count(), 1.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, hypTan.count(), 0, -1.0, hypTan.data(), 1, hypTan.data(), 1, 1.0, Beta2.data(), 1);

                Beta2 = MathDescriptives::sumOfColumns(Beta2, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = Beta1;

                cblas_daxpy(Beta2.count(), -a1, Beta2.data(), 1, D.data(), 1);

                Beta2.clear();

                MathOperations::applyFunctor_elementWise_inplace(D, [](double &value){value = 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0, Y.data(), d_nComponents, hypTan.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                hypTan.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;


            }

            case FICA_NONLIN_GAUSS : {

                QList<double> Y(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> pow2 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> ex = MathOperations::applyFunctor_elementWise(pow2, [&a2](const double &value){return std::exp(-a2 * value / 2.0);}, d_numberOfThreadsToUse);

                QList<double> gauss = MathOperations::elementWise_multiplication(Y, ex, d_numberOfThreadsToUse);

                Y.clear();

                QList<double> dGauss = ex;

                cblas_dsbmv(CblasRowMajor, CblasLower, pow2.count(), 0, -a2, pow2.data(), 1, ex.data(), 1, 1.0, dGauss.data(), 1);

                pow2.clear();

                ex.clear();

                QList<double> Beta = MathDescriptives::sumOfColumns(dGauss, d_nComponents, false, d_numberOfThreadsToUse);

                dGauss.clear();

                QList<double> D = MathOperations::repeatVector(Beta, d_nComponents);

                Beta.clear();

                QList<double> mat(D.count());

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0 / d_nItems, D.data(), 1, d_B.data(), 1, 0.0, mat.data(), 1);

                D.clear();

                d_B = mat;

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0 / d_nItems, d_whitenedMatrix.data(), d_nItems, gauss.data(), d_nComponents, -1.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_GAUSS + 1) : {

                QList<double> Y(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> pow2 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> ex = MathOperations::applyFunctor_elementWise(pow2, [&a2](const double &value){return std::exp(-a2 * value / 2.0);}, d_numberOfThreadsToUse);

                QList<double> gauss = MathOperations::elementWise_multiplication(Y, ex, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, gauss.data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> dGauss = ex;

                cblas_dsbmv(CblasRowMajor, CblasLower, pow2.count(), 0, -a2, pow2.data(), 1, ex.data(), 1, 1.0, dGauss.data(), 1);

                pow2.clear();

                ex.clear();

                QList<double> Beta2 = MathDescriptives::sumOfColumns(dGauss, d_nComponents, false, d_numberOfThreadsToUse);

                dGauss.clear();

                QList<double> D = Beta1;

                cblas_daxpy(Beta2.count(), -1.0, Beta2.data(), 1, D.data(), 1);

                MathOperations::applyFunctor_elementWise_inplace(D, [](double &value){value = 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0, Y.data(), d_nComponents, gauss.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                gauss.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case(FICA_NONLIN_GAUSS + 2) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> Y(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> pow2 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> ex = MathOperations::applyFunctor_elementWise(pow2, [&a2](const double &value){return std::exp(-a2 * value / 2.0);}, d_numberOfThreadsToUse);

                QList<double> gauss = MathOperations::elementWise_multiplication(Y, ex, d_numberOfThreadsToUse);

                Y.clear();

                QList<double> dGauss = ex;

                cblas_dsbmv(CblasRowMajor, CblasLower, pow2.count(), 0, -a2, pow2.data(), 1, ex.data(), 1, 1.0, dGauss.data(), 1);

                pow2.clear();

                ex.clear();

                QList<double> Beta = MathDescriptives::sumOfColumns(dGauss, d_nComponents, false, d_numberOfThreadsToUse);

                dGauss.clear();

                QList<double> D = MathOperations::repeatVector(Beta, d_nComponents);

                Beta.clear();

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0 / d_nSubItems, D.data(), 1, d_B.data(), 1, 0.0, mat.data(), 1);

                d_B = mat;

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0 / d_nSubItems, subWhitenedMatrix.data(), d_nSubItems, gauss.data(), d_nComponents, -1.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_GAUSS + 3) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> Y(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                subWhitenedMatrix.clear();

                QList<double> pow2 = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> ex = MathOperations::applyFunctor_elementWise(pow2, [&a2](const double &value){return std::exp(-a2 * value / 2.0);}, d_numberOfThreadsToUse);

                QList<double> gauss = MathOperations::elementWise_multiplication(Y, ex, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, gauss.data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> dGauss = ex;

                cblas_dsbmv(CblasRowMajor, CblasLower, pow2.count(), 0, -a2, pow2.data(), 1, ex.data(), 1, 1.0, dGauss.data(), 1);

                pow2.clear();

                ex.clear();

                QList<double> Beta2 = MathDescriptives::sumOfColumns(dGauss, d_nComponents, false, d_numberOfThreadsToUse);

                dGauss.clear();

                QList<double> D = Beta1;

                cblas_daxpy(Beta2.count(), -1.0, Beta2.data(), 1, D.data(), 1);

                Beta2.clear();

                MathOperations::applyFunctor_elementWise_inplace(D, [](double &value){value = 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0, Y.data(), d_nComponents, gauss.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                gauss.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case FICA_NONLIN_SKEW : {

                QList<double> skew(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, skew.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(skew, [](double &value){value = value * value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0 / d_nItems, d_whitenedMatrix.data(), d_nItems, skew.data(), d_nComponents, 0.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_SKEW + 1) : {

                QList<double> Y(d_nItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nItems, d_nComponents, d_nComponents, 1.0, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                QList<double> skew = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, skew.data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = MathOperations::applyFunctor_elementWise(Beta1, [](const double &value){return 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, 1.0, Y.data(), d_nComponents, skew.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                skew.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

            case(FICA_NONLIN_SKEW+2) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> skew(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, skew.data(), d_nComponents);

                MathOperations::applyFunctor_elementWise_inplace(skew, [](double &value){value = value * value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0 / d_nSubItems, subWhitenedMatrix.data(), d_nSubItems, skew.data(), d_nComponents, 0.0, d_B.data(), d_nComponents);

                break;

            }

            case(FICA_NONLIN_SKEW+3) : {

                QList<double> subWhitenedMatrix = MathOperations::sampleColumns(d_whitenedMatrix, d_nItems, d_proportionOfItemsToUseInAnIteration, d_numberOfThreadsToUse);

                QList<double> Y(d_nSubItems * d_nComponents, 0.0);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nSubItems, d_nComponents, d_nComponents, 1.0, subWhitenedMatrix.data(), d_nSubItems, d_B.data(), d_nComponents, 0.0, Y.data(), d_nComponents);

                subWhitenedMatrix.clear();

                QList<double> skew = MathOperations::applyFunctor_elementWise(Y, [](const double &value){return value * value;}, d_numberOfThreadsToUse);

                QList<double> Beta1(Y.count(), 0.0);

                cblas_dsbmv(CblasRowMajor, CblasLower, Y.count(), 0, 1.0, Y.data(), 1, skew .data(), 1, 0.0, Beta1.data(), 1);

                Beta1 = MathDescriptives::sumOfColumns(Beta1, d_nComponents, false, d_numberOfThreadsToUse);

                QList<double> D = MathOperations::applyFunctor_elementWise(Beta1, [](const double &value){return 1.0 / value;}, d_numberOfThreadsToUse);

                cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nSubItems, 1.0, Y.data(), d_nComponents, skew.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                skew.clear();

                Y = mat;

                for (int i = 0; i < Beta1.size(); ++i)
                    Y[i + i * d_nComponents] -= Beta1.at(i);

                Beta1.clear();

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, d_mu, d_B.data(), d_nComponents, Y.data(), d_nComponents, 0.0, mat.data(), d_nComponents);

                Y.clear();

                D = MathOperations::repeatVector(D, d_nComponents);

                cblas_dsbmv(CblasRowMajor, CblasLower, D.count(), 0, 1.0, D.data(), 1, mat.data(), 1, 1.0, d_B.data(), 1);

                break;

            }

        }

        if (d_connectedBaseworker) {

            d_connectedBaseworker->updateProgress(uuidProgress, -10000000 * std::log10(std::fabs(1.0 - minAbsCos)));

            if (d_connectedBaseworker->thread()->isInterruptionRequested()) {

                d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

                MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

                d_connectedBaseworker->stopProgress(uuidProgress);

                return false;
            }

            d_connectedBaseworker->checkIfPauseWasRequested();

        } else if (QThread::currentThread()->isInterruptionRequested()) {

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }

    }

    d_seperatingMatrix.resize(d_nComponents * d_nSamples, 0.0);

    cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nSamples, d_nComponents, 1.0, d_B.data(), d_nComponents, d_whiteningMatrix.data(), d_nSamples, 0.0, d_seperatingMatrix.data(), d_nSamples);

    if (d_connectedBaseworker)
        d_connectedBaseworker->stopProgress(uuidProgress);

    return true;

}

const QList<double> &FastICA::mixingMatrix() const
{

    return d_mixingMatrix;

}

const QList<double> &FastICA::seperatingMatrix() const
{

    return d_seperatingMatrix;

}

const QList<double> &FastICA::independentComponents()
{

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    d_previousNumberOfThreadsToUse = MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines();

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

    d_independentComponents.resize(d_nItems * d_nComponents, 0.0);

    cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nItems, d_nComponents, 1.0, d_B.data(), d_nComponents, d_whitenedMatrix.data(), d_nItems, 0.0, d_independentComponents.data(), d_nItems);

    d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

    return d_independentComponents;

}

void FastICA::connectBaseWorker(BaseWorker *baseWorker)
{

    d_connectedBaseworker = QPointer<BaseWorker>(baseWorker);

}

void FastICA::setLabel(const QString &label)
{

    d_label = label;

}

void FastICA::setNumberOfThreadsToUse(qsizetype numberOfThreadsToUse)
{

    d_numberOfThreadsToUse = numberOfThreadsToUse;

}
