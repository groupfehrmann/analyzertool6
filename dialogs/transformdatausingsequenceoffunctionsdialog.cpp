#include "transformdatausingsequenceoffunctionsdialog.h"

TransformDataUsingSequenceOfFunctionsDialog::TransformDataUsingSequenceOfFunctionsDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "transform data using sequence of functions", "transformdatausingsequenceoffunctionsdialog")
{

    TransformDataUsingSequenceOfFunctionsParameters *transformDataUsingSequenceOfFunctionsParameters = new TransformDataUsingSequenceOfFunctionsParameters;

    transformDataUsingSequenceOfFunctionsParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<TransformDataUsingSequenceOfFunctionsParameters>(transformDataUsingSequenceOfFunctionsParameters);

    BaseDialog::initWidgets();

}

void TransformDataUsingSequenceOfFunctionsDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes;});


    TransformSequenceFunctionsBuilderWidget *transformSequenceFunctionsBuilderWidget = new TransformSequenceFunctionsBuilderWidget(this, "transformSequenceFunctionsBuilderWidget");

    this->connect(transformSequenceFunctionsBuilderWidget, &TransformSequenceFunctionsBuilderWidget::transformSequenceChanged, [this, transformSequenceFunctionsBuilderWidget](){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedTransformFunctionDescriptionsWithParameterReplacement = transformSequenceFunctionsBuilderWidget->selectedTransformFunctionDescriptionsWithParameterReplacement(); static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->selectedTransformFunctions = transformSequenceFunctionsBuilderWidget->selectedTransformFunctions();});

    d_gridLayout->addWidget(transformSequenceFunctionsBuilderWidget, 2, 0, 1, 2);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 3, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 4, 0, 1, 2);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &TransformDataUsingSequenceOfFunctionsDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void TransformDataUsingSequenceOfFunctionsDialog::orientationChanged(Qt::Orientation orientation)
{

    TransformDataUsingSequenceOfFunctionsParameters *parameters = static_cast<TransformDataUsingSequenceOfFunctionsParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), parameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->identifiers(orientation, true), parameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), parameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), parameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}
