#ifndef SELECTDIRECTORYWIDGET_H
#define SELECTDIRECTORYWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QFileDialog>
#include <QDir>

#include "dialogs/basedialog.h"

namespace Ui {

    class SelectDirectoryWidget;

}

class SelectDirectoryWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectDirectoryWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QFileDialog *fileDialog();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~SelectDirectoryWidget();

private:

    Ui::SelectDirectoryWidget *ui;

    QFileDialog *d_fileDialog;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

    void evaluatePath();

private slots:

    void on_lineEdit_path_textEdited(const QString &text);

    void on_pushButton_browse_clicked();

signals:

    void pathChanged(const QString &path);

    void invalidPath();

};

#endif // SELECTDIRECTORYWIDGET_H
