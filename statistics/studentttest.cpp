#include "studentttest.h"

qsizetype StudentTTest::d_minimumNumberOfSamples = 2;

qsizetype StudentTTest::d_maximumNumberOfSamples = 2;

qsizetype StudentTTest::d_minimumNumberOfItemsPerSample = 2;

bool StudentTTest::d_effectSizeAvailable = true;

QString StudentTTest::d_effectSizeLabel = "Hedges's G";

QList<QString> StudentTTest::d_testDescriptiveLabels = {"Mean Difference", "Std. Error Difference", "Lower 95% CI Difference", "Upper 95% CI Difference", "Hedges's Gs", "Std. Error Hedges's Gs", "Student T", "df", "Sig. (2-tailed)"};

std::function<double (const double &)> StudentTTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

StudentTTest::StudentTTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

StudentTTest::StudentTTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_samples.resize(2);

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1)
            d_samples[codedSampleIdentifier].append(data.at(i));

    }

    d_sampleSize = d_samples.at(0).count() + d_samples.at(1).count();

}

void StudentTTest::calculateStatistic()
{

    d_n1 = d_samples.at(0).count();

    d_n2 = d_samples.at(1).count();

    d_meanAndVarianceOfSample1 = boost::math::statistics::mean_and_sample_variance(d_samples.at(0));

    d_meanAndVarianceOfSample2 = boost::math::statistics::mean_and_sample_variance(d_samples.at(1));

    d_pooledVariance = ((d_n1 - 1.0) * d_meanAndVarianceOfSample1.second + (d_n2 - 1) * d_meanAndVarianceOfSample2.second) / (d_n1 + d_n2 - 2.0);

    d_meanDifference = d_meanAndVarianceOfSample1.first - d_meanAndVarianceOfSample2.first;

    d_standardErrorOfMeanDifference = std::sqrt(d_pooledVariance * (1.0 / d_n1 + 1.0 / d_n2));

    d_statistic = d_meanDifference / d_standardErrorOfMeanDifference;

}

void StudentTTest::calculatePValue()
{

    this->calculateStatistic();

    d_degreeOfFreedom = d_sampleSize - 2.0;

    if ((d_degreeOfFreedom < 0.0) || std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::students_t students_t_distribution(d_degreeOfFreedom);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(students_t_distribution, std::abs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void StudentTTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    this->calculateEffectSize();

    this->calculateStandardErrorEffectSize();

    boost::math::normal normal_distribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normal_distribution, 0.025)) * d_standardErrorOfMeanDifference;

    d_testDescriptiveValues << d_meanDifference << d_standardErrorOfMeanDifference << d_meanDifference - w << d_meanDifference + w << d_effectSize << d_standardErrorOfEffectSize  << d_statistic << d_degreeOfFreedom << d_pValue;

}

void StudentTTest::calculateEffectSize()
{

    this->calculateStatistic();

    d_hedgeM = d_degreeOfFreedom;

    double temp = std::tgamma(d_hedgeM / 2.0);

    if (std::isinf(temp))
        d_hedgeCorrectionFactor = 1.0 / std::sqrt(d_hedgeM / 2.0);
    else
        d_hedgeCorrectionFactor = temp / (std::sqrt(d_hedgeM / 2.0) * std::tgamma((d_hedgeM - 1.0) / 2.0));

    d_effectSize = d_hedgeCorrectionFactor * (d_meanDifference / std::sqrt(d_pooledVariance));

}

void StudentTTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = std::sqrt(d_sampleSize * d_hedgeM / d_n1 * d_n2 * (d_hedgeM - 2.0) + d_effectSize * d_effectSize * (d_hedgeM / (d_hedgeM - 2.0) - 1.0 / d_hedgeCorrectionFactor * d_hedgeCorrectionFactor) * d_hedgeCorrectionFactor * d_hedgeCorrectionFactor);

}
