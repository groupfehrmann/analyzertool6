#include "texteditwidget.h"
#include "ui_texteditwidget.h"

TextEditWidget::TextEditWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::TextEditWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

}

QLabel *TextEditWidget::label()
{

    return ui->label;

}

QTextEdit *TextEditWidget::textEdit()
{

    return ui->textEdit;

}

void TextEditWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/textEdit");

    if (settings.contains("text"))
        ui->textEdit->setText(settings.value("text").toString());

    settings.endGroup();

    d_readWidgetSettings = true;

}

void TextEditWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/textEdit");

    settings.setValue("text", ui->textEdit->toPlainText());

    settings.endGroup();

}

TextEditWidget::~TextEditWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
