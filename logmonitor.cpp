#include "logmonitor.h"

LogMonitor::LogMonitor(QObject *parent) :
    QObject(parent),
    d_data(new LogMonitorData)
{

}

void LogMonitor::clear()
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_logItems.clear();

}

qsizetype LogMonitor::count() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_logItems.count();

}

const LogMonitorData::LogItem &LogMonitor::logItemAt(qsizetype index) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_logItems.at(index);

}

void LogMonitor::reportError(const QUuid &uuidWorker, const QString &error)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_logItems.append({uuidWorker, QDateTime::currentDateTime(), error, LogMonitorData::ERROR});

}

void LogMonitor::reportMessage(const QUuid &uuidWorker, const QString &message)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_logItems.append({uuidWorker, QDateTime::currentDateTime(), message, LogMonitorData::MESSAGE});

}

void LogMonitor::reportWarning(const QUuid &uuidWorker, const QString &warning)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_logItems.append({uuidWorker, QDateTime::currentDateTime(), warning, LogMonitorData::WARNING});

}

LogMonitor::~LogMonitor()
{

}
