#ifndef BARTLETTCHISQUAREDTEST_H
#define BARTLETTCHISQUAREDTEST_H

#include <QList>

#include <boost/math/statistics/univariate_statistics.hpp>
#include "boost/math/distributions/chi_squared.hpp"

#include "basestatisticaltest.h"

class BartlettChiSquaredTest : public BaseStatisticalTest
{

public:

    BartlettChiSquaredTest();

    BartlettChiSquaredTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples);

    virtual ~BartlettChiSquaredTest() {}

private:

    double d_degreeOfFreedom;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // BARTLETTCHISQUAREDTEST_H
