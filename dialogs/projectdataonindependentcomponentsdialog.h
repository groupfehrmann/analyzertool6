#ifndef PROJECTDATAONINDEPENDENTCOMPONENTSDIALOG_H
#define PROJECTDATAONINDEPENDENTCOMPONENTSDIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>

#include "basedialog.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "workerclasses/projectdataonindependentcomponentsworker.h"

class ProjectDataOnIndependentComponentsDialog : public BaseDialog
{

    Q_OBJECT

public:

    ProjectDataOnIndependentComponentsDialog(BaseDialog *parent = nullptr, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices = QList<QSharedPointer<BaseAnnotatedMatrix>>());

protected:

    void initWidgets_() override;

private:

    QList<QSharedPointer<BaseAnnotatedMatrix>> d_baseAnnotatedMatrices;

    OrientationWidget *d_orientationWidget_newData;

    ItemSelectorWidget *d_itemSelectorWidget_variablesNewData;

    ItemSelectorWidget *d_itemSelectorWidget_itemsNewData;

    OrientationWidget *d_orientationWidget_independentComponents;

    ItemSelectorWidget *d_itemSelectorWidget_variablesIndependentComponents;

    ItemSelectorWidget *d_itemSelectorWidget_itemsIndependentComponents;

private slots:

    void orientationNewDataChanged(Qt::Orientation orientation);

    void orientationIndependentComponentsChanged(Qt::Orientation orientation);

};

#endif // PROJECTDATAONINDEPENDENTCOMPONENTSDIALOG_H
