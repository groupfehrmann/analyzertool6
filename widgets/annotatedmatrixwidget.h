#ifndef ANNOTATEDMATRIXWIDGET_H
#define ANNOTATEDMATRIXWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include <QLineEdit>
#include <QMenu>
#include <QScrollBar>
#include <QAbstractButton>
#include <QSpinBox>
#include <QWidgetAction>

#include "containers/annotatedmatrix.h"
#include "models/annotatedmatrixdatamodel.h"
#include "models/annotatedmatrixannotationsmodel.h"

namespace Ui {

    class AnnotatedMatrixWidget;

}

class AnnotatedMatrixWidget : public QWidget
{

    Q_OBJECT

public:

    explicit AnnotatedMatrixWidget(QWidget *parent = nullptr, const QSharedPointer<QObject> &object = nullptr);

    ~AnnotatedMatrixWidget();

private:

    Ui::AnnotatedMatrixWidget *ui;

    QSharedPointer<QObject> d_object;

private slots:

    void showContextMenu_tableView_data(const QPoint &pos);

    void showContextMenu_tableView_columnAnnotations(const QPoint &pos);

    void showContextMenu_tableView_rowAnnotations(const QPoint &pos);

    void showContextMenu_tableView_columnAnnotations_horizontalHeader(const QPoint &pos);

    void showContextMenu_tableView_columnAnnotations_verticalHeader(const QPoint &pos);

    void showContextMenu_tableView_data_horizontalHeader(const QPoint &pos);

    void showContextMenu_tableView_data_verticalHeader(const QPoint &pos);

    void showContextMenu_tableView_rowAnnotations_horizontalHeader(const QPoint &pos);

    void showContextMenu_tableView_rowAnnotations_verticalHeader(const QPoint &pos);

    void showLineEditForHeader(int logicalIndex);

    void synchronizeScrollBars(int value);

    void setShowSelectedOnlyForAllModels(bool showSelectedOnly);

    void invalidateSortFilterModel();

};

#endif // ANNOTATEDMATRIXWIDGET_H
