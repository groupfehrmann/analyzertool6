#include "shuffleidentifiersdialog.h"

ShuffleIdentifiersDialog::ShuffleIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "shuffle identifiers", "shuffleidentifiersdialog")
{

    ShuffleIdentifiersParameters *shuffleIdentifiersParameters = new ShuffleIdentifiersParameters;

    shuffleIdentifiersParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<ShuffleIdentifiersParameters>(shuffleIdentifiersParameters);

    BaseDialog::initWidgets();

}

void ShuffleIdentifiersDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<ShuffleIdentifiersParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 1);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ShuffleIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<ShuffleIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &ShuffleIdentifiersDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void ShuffleIdentifiersDialog::orientationChanged(Qt::Orientation orientation)
{

    ShuffleIdentifiersParameters *shuffleIdentifiersParameters = static_cast<ShuffleIdentifiersParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(shuffleIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), shuffleIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(shuffleIdentifiersParameters->baseAnnotatedMatrix->identifiers(orientation, true), shuffleIdentifiersParameters->baseAnnotatedMatrix->indexes(orientation, true));

}
