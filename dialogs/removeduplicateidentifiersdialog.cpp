#include "removeduplicateidentifiersdialog.h"

RemoveDuplicateIdentifiersDialog::RemoveDuplicateIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "remove duplicate identifiers", "removeduplicateidentifiersdialog")
{

    RemoveDuplicateIdentifiersParameters *removeDuplicateIdentifiersParameters = new RemoveDuplicateIdentifiersParameters;

    removeDuplicateIdentifiersParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<RemoveDuplicateIdentifiersParameters>(removeDuplicateIdentifiersParameters);

    BaseDialog::initWidgets();

}

void RemoveDuplicateIdentifiersDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<RemoveDuplicateIdentifiersParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 1);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<RemoveDuplicateIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<RemoveDuplicateIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &RemoveDuplicateIdentifiersDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void RemoveDuplicateIdentifiersDialog::orientationChanged(Qt::Orientation orientation)
{

    RemoveDuplicateIdentifiersParameters *removeDuplicateIdentifiersParameters = static_cast<RemoveDuplicateIdentifiersParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(removeDuplicateIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), removeDuplicateIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(removeDuplicateIdentifiersParameters->baseAnnotatedMatrix->identifiers(orientation, true), removeDuplicateIdentifiersParameters->baseAnnotatedMatrix->indexes(orientation, true));

}
