#ifndef LOGMONITORWIDGET_H
#define LOGMONITORWIDGET_H

#include <QWidget>
#include <QCheckBox>
#include <QComboBox>
#include <QSettings>
#include <QFileDialog>
#include <QApplication>
#include <QFile>
#include <QMessageBox>

#include "models/logmonitormodel.h"

namespace Ui {

    class LogMonitorWidget;

}

class LogMonitorWidget : public QWidget
{

    Q_OBJECT

public:

    explicit LogMonitorWidget(QWidget *parent = nullptr);

    void setLogMonitorModel(LogMonitorModel *logMonitorModel);

    ~LogMonitorWidget();

public slots:

    void exportLogMonitorToFile();

private:

    Ui::LogMonitorWidget *ui;

signals:

    void uuidWorkerChanged(const QUuid &uuidWorker);

};

#endif // LOGMONITORWIDGET_H
