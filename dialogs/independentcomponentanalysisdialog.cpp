#include "independentcomponentanalysisdialog.h"

IndependentComponentAnalysisDialog::IndependentComponentAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "independent component analysis", "independentcomponentanalysisdialog")
{

    IndependentComponentAnalysisParameters *independentComponentAnalysisParameters = new IndependentComponentAnalysisParameters;

    independentComponentAnalysisParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<IndependentComponentAnalysisParameters>(independentComponentAnalysisParameters);

    BaseDialog::initWidgets();

}

void IndependentComponentAnalysisDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &IndependentComponentAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes; this->updateMaximumNumberOfComponents();});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &IndependentComponentAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes; this->updateMaximumNumberOfComponents();});


    DoubleSpinBoxWidget *doubleSpinBoxWidget_thresholdCumulativeExplainedVariance = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_thresholdCumulativeExplainedVariance");

    this->connect(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_cumulativeExplainedVariance = value;});

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->label()->setText("cumulative explained variance in whitening step");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setValue(100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance, 2, 0, 1, 1);


    d_spinBoxWidget_thresholdMaximumNumberOfComponents = new SpinBoxWidget(this, "d_spinBoxWidget_thresholdMaximumNumberOfComponents");

    this->connect(d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_maximumNumberOfComponents = value;});

    d_spinBoxWidget_thresholdMaximumNumberOfComponents->label()->setText("maximum number of components in whitening step");

    d_gridLayout->addWidget(d_spinBoxWidget_thresholdMaximumNumberOfComponents, 2, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_contrastFunction = new ComboBoxWidget(this, "comboBoxWidget_contrastFunction");

    this->connect(comboBoxWidget_contrastFunction->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_contrastFunction](){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->contrastFunction = comboBoxWidget_contrastFunction->comboBox()->currentText();});

    comboBoxWidget_contrastFunction->label()->setText("contrast function");

    comboBoxWidget_contrastFunction->comboBox()->addItems({"Raise to power 3", "Hyperbolic tangent", "Gaussian", "Skewness"});

    comboBoxWidget_contrastFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_contrastFunction, 3, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_epsilon = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_epsilon");

    this->connect(doubleSpinBoxWidget_epsilon->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->epsilon = value;});

    doubleSpinBoxWidget_epsilon->label()->setText("stopping criteria (epsilon)");

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setRange(0.00000001, 1.0);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setValue(0.00001);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setDecimals(8);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_epsilon->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_epsilon, 3, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_maximumNumberOfIterations = new SpinBoxWidget(this, "spinBoxWidget_maximumNumberOfIterations");

    this->connect(spinBoxWidget_maximumNumberOfIterations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->maximumNumberOfIterations = value;});

    spinBoxWidget_maximumNumberOfIterations->label()->setText("maximum number of iterations");

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setRange(0, 10000);

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setValue(2000);

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_maximumNumberOfIterations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_maximumNumberOfIterations, 4, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration");

    this->connect(doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->proportionOfSamplesToUseInAnIteration = value;});

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->label()->setText("proportion of samples to use in an iteration");

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->doubleSpinBox()->setRange(0.000001, 1.0);

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->doubleSpinBox()->setValue(1.0);

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->doubleSpinBox()->setDecimals(6);

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_proportionOfSamplesToUseInAnIteration, 4, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_stabilizationMode = new ComboBoxWidget(this, "comboBoxWidget_stabilizationMode");

    this->connect(comboBoxWidget_stabilizationMode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_stabilizationMode](){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->stabilizationMode = comboBoxWidget_stabilizationMode->comboBox()->currentData().toBool();});

    comboBoxWidget_stabilizationMode->label()->setText("stabilization mode");

    comboBoxWidget_stabilizationMode->comboBox()->addItem("enable", true);

    comboBoxWidget_stabilizationMode->comboBox()->addItem("disable", false);

    comboBoxWidget_stabilizationMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_stabilizationMode, 5, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_mu = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_mu");

    this->connect(doubleSpinBoxWidget_mu->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->mu = value;});

    doubleSpinBoxWidget_mu->label()->setText("step size (mu) in stabilization mode");

    doubleSpinBoxWidget_mu->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setRange(0.01, 1.0);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setValue(0.75);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_mu->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_mu, 5, 1, 1, 1);



    ComboBoxWidget *comboBoxWidget_finetuneMode = new ComboBoxWidget(this, "comboBoxWidget_finetuneMode");

    this->connect(comboBoxWidget_finetuneMode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_finetuneMode](){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->finetuneMode = comboBoxWidget_finetuneMode->comboBox()->currentData().toBool();});

    comboBoxWidget_finetuneMode->label()->setText("finetune mode");

    comboBoxWidget_finetuneMode->comboBox()->addItem("enable", true);

    comboBoxWidget_finetuneMode->comboBox()->addItem("disable", false);

    comboBoxWidget_finetuneMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_finetuneMode, 6, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfRunsToPerform = new SpinBoxWidget(this, "spinBoxWidget_numberOfRunsToPerform");

    this->connect(spinBoxWidget_numberOfRunsToPerform->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfRunsToPerform = value;});

    spinBoxWidget_numberOfRunsToPerform->label()->setText("number of runs to perform for consensus ICA");

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setRange(1, 1000);

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setValue(100);

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfRunsToPerform->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfRunsToPerform, 7, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->pearsonCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->label()->setText("pearson correlation threshold for clustering components from multiple runs");

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.25, 1.0);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.9);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus, 7, 1, 1, 1);


    d_doubleSpinBoxWidget_credibilityIndexThreshold = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_credibilityIndexThreshold");

    this->connect(d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_credibilityIndex = value;});

    d_doubleSpinBoxWidget_credibilityIndexThreshold->label()->setText("threshold for credibility index");

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setDecimals(6);

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setRange(0, 1.0);

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(1.0 / static_cast<double>(spinBoxWidget_numberOfRunsToPerform->spinBox()->value()));

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    d_doubleSpinBoxWidget_credibilityIndexThreshold->readWidgetSettings();

    d_gridLayout->addWidget(d_doubleSpinBoxWidget_credibilityIndexThreshold, 8, 0, 1, 1);

    this->connect(spinBoxWidget_numberOfRunsToPerform->spinBox(), &QSpinBox::valueChanged, [this](int value){

        d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(1.0 / static_cast<double>(value));

    });



    ComboBoxWidget *comboBoxWidget_metricToDetermineIndependence = new ComboBoxWidget(this, "comboBoxWidget_metricToDetermineIndependence");

    this->connect(comboBoxWidget_metricToDetermineIndependence->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_metricToDetermineIndependence](){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->metricToDetermineIndependence = comboBoxWidget_metricToDetermineIndependence->comboBox()->currentText();});

    comboBoxWidget_metricToDetermineIndependence->label()->setText("metric to determine independence");

    comboBoxWidget_metricToDetermineIndependence->comboBox()->addItem("distance correlation");

    comboBoxWidget_metricToDetermineIndependence->comboBox()->addItem("XICOR");

    comboBoxWidget_metricToDetermineIndependence->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_metricToDetermineIndependence, 9, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->distanceCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->label()->setText("distance or XI correlation threshold for filter non-independent consensus components");

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.0, 1.0);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.5);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus, 9, 1, 1, 1);



    ComboBoxWidget *comboBoxWidget_multiThreadMode = new ComboBoxWidget(this, "comboBoxWidget_multiThreadMode");

    this->connect(comboBoxWidget_multiThreadMode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_multiThreadMode](){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->multiThreadMode = comboBoxWidget_multiThreadMode->comboBox()->currentText();});

    comboBoxWidget_multiThreadMode->label()->setText("multithread mode");

    comboBoxWidget_multiThreadMode->comboBox()->addItems({"perform runs simultaneously", "perform runs sequentially"});

    comboBoxWidget_multiThreadMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_multiThreadMode, 10, 0, 1, 2);



    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 11, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 12, 0, 1, 2);


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"whitening: covariance matrix" ,  "whitening: eigenvalues", "whitening: eigenvectors", "whitening: whitening matrix", "whitening: dewhitening matrix", "whitening: whitened matrix", "ICA: independent components (per run)", "ICA: mixing matrix (per run)", "ICA: separating matrix (per run)", "ICA: XICOR or distance correlation between independent components (per run)", "ICA: independent components (consensus)", "ICA: mixing matrix (consensus)", "ICA: flipped independent components (consensus)", "ICA: flipped mixing matrix (consensus)", "ICA: robustness metrics independent components (consensus)", "ICA: XICOR or distance correlation between independent components (consensus)"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 13, 0, 1, 2);


    this->orientationChanged(d_orientationWidget->orientation());

}

void IndependentComponentAnalysisDialog::orientationChanged(Qt::Orientation orientation)
{

    IndependentComponentAnalysisParameters *independentComponentAnalysisParameters = static_cast<IndependentComponentAnalysisParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(independentComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), independentComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(independentComponentAnalysisParameters->baseAnnotatedMatrix->identifiers(orientation, true), independentComponentAnalysisParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(independentComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), independentComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(independentComponentAnalysisParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), independentComponentAnalysisParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void IndependentComponentAnalysisDialog::updateMaximumNumberOfComponents()
{

    qsizetype numberOfVariablesSelected = d_itemSelectorWidget_variables->itemSelectorModel_selected()->itemIndexes().count();

    qsizetype numberOfItemsSelected = d_itemSelectorWidget_items->itemSelectorModel_selected()->itemIndexes().count();

    qsizetype maxNumberOfComponents = (numberOfVariablesSelected < numberOfItemsSelected) ? numberOfVariablesSelected : numberOfItemsSelected;


    d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setRange(1, maxNumberOfComponents);

    d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setValue(maxNumberOfComponents);

}
