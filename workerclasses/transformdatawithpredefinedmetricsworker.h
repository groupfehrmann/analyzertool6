#ifndef TRANSFORMDATAWITHPREDEFINEDMETRICSWORKER_H
#define TRANSFORMDATAWITHPREDEFINEDMETRICSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"

class TransformDataWithPredefinedMetricsParameters : public BaseParameters
{

public:

    TransformDataWithPredefinedMetricsParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix_dataToTranform;

    Qt::Orientation orientation_dataToTransform;

    QList<qsizetype> selectedVariableIndexes_dataToTransform;

    QList<QString> selectedVariableIdentifiers_dataToTransform;

    QList<qsizetype> selectedItemIndexes_dataToTransform;

    QList<QString> selectedItemIdentifiers_dataToTransform;

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix_dataContainingPredefinedMetrics;

    Qt::Orientation orientation_dataContainingPredefinedMetrics;

    QString variableIdentifierPredefinedMetrics;

    qsizetype variableIndexPredefinedMetrics;

    QList<qsizetype> selectedItemIndexes_dataContainingPredefinedMetrics;

    QList<QString> selectedItemIdentifiers_dataContainingPredefinedMetrics;

    QString transformLabel;

    QString exportDirectory;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransformDataWithPredefinedMetricsWorker : public BaseWorker
{

public:

    TransformDataWithPredefinedMetricsWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T, typename S> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix_dataToTranform, QSharedPointer<AnnotatedMatrix<S>> annotatedMatrix_dataContainingPredefinedMetrics);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T, typename S>
void TransformDataWithPredefinedMetricsWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix_dataToTranform, QSharedPointer<AnnotatedMatrix<S>> annotatedMatrix_dataContainingPredefinedMetrics)
{

    QSharedPointer<TransformDataWithPredefinedMetricsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataWithPredefinedMetricsParameters>());

    std::function<void (const QList<std::reference_wrapper<T>> &, const S &)> transformFunction;

    if (parameters->transformLabel == "value = value - metric")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (const std::reference_wrapper<T> &value : vector) value.get() -= static_cast<T>(metric);};
    else if (parameters->transformLabel == "value = metric - value")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (std::reference_wrapper<T> value : vector) value.get() = static_cast<T>(metric) - value.get();};
    else if (parameters->transformLabel == "value = value + metric")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (std::reference_wrapper<T> value : vector) value.get() += static_cast<T>(metric);};
    else if (parameters->transformLabel == "value = value / metric")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (std::reference_wrapper<T> value : vector) value.get() /= static_cast<T>(metric);};
    else if (parameters->transformLabel == "value = metric / value")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (std::reference_wrapper<T> value : vector) value.get() = static_cast<T>(metric) / value.get();};
    else if (parameters->transformLabel == "value = value * metric")
        transformFunction = [](const QList<std::reference_wrapper<T>> &vector, const S &metric) {for (std::reference_wrapper<T> value : vector) value.get() *= static_cast<T>(metric);};


    QUuid uuidProgress = this->startProgress("building variable identifier to metric mapping", 0, parameters->selectedItemIndexes_dataContainingPredefinedMetrics.count());

    QHash<QString, S> variableIdentifierToMetric;

    variableIdentifierToMetric.reserve(parameters->selectedItemIndexes_dataContainingPredefinedMetrics.count());

    for (qsizetype i = 0; i < parameters->selectedItemIndexes_dataContainingPredefinedMetrics.count(); ++i) {

        T value;

        if (parameters->orientation_dataContainingPredefinedMetrics == Qt::Vertical)
            value = annotatedMatrix_dataContainingPredefinedMetrics->Matrix<S>::operator[](parameters->variableIndexPredefinedMetrics)[parameters->selectedItemIndexes_dataContainingPredefinedMetrics.at(i)];
        else
            value = annotatedMatrix_dataContainingPredefinedMetrics->Matrix<S>::operator[](parameters->selectedItemIndexes_dataContainingPredefinedMetrics.at(i))[parameters->variableIndexPredefinedMetrics];

        variableIdentifierToMetric.insert(parameters->selectedItemIdentifiers_dataContainingPredefinedMetrics.at(i), value);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("transforming data", 0, parameters->selectedVariableIndexes_dataToTransform.count());

    QList<std::pair<QString, qsizetype>> selectedVariableIdentifiersAndIndexes;

    for (qsizetype i = 0; i < parameters->selectedVariableIndexes_dataToTransform.count(); ++i)
        selectedVariableIdentifiersAndIndexes.append(std::pair<QString, qsizetype>(parameters->selectedVariableIdentifiers_dataToTransform.at(i), parameters->selectedVariableIndexes_dataToTransform.at(i)));

    std::function<void(std::pair<QString, qsizetype>)> mapFunctor = [this, &uuidProgress, &parameters, &variableIdentifierToMetric, &annotatedMatrix_dataToTranform, &transformFunction](std::pair<QString, qsizetype> identifierAndIndex) {

        S metric = variableIdentifierToMetric.value(identifierAndIndex.first, std::numeric_limits<S>::quiet_NaN());

        if (std::isnan(metric)) {

            this->reportMessage("variable with identifier \"" + identifierAndIndex.first + "\" and index " + QString::number(identifierAndIndex.second) + " was not transformed, no valid metric available");

            return;

        }

        if (parameters->orientation_dataToTransform == Qt::Vertical)
            transformFunction(annotatedMatrix_dataToTranform->sliced_references({identifierAndIndex.second}, parameters->selectedItemIndexes_dataToTransform), metric);
        else
            transformFunction(annotatedMatrix_dataToTranform->sliced_references(parameters->selectedItemIndexes_dataToTransform, {identifierAndIndex.second}), metric);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    };

    QtConcurrent::blockingMap(&d_threadPool, selectedVariableIdentifiersAndIndexes, mapFunctor);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

}

#endif // TRANSFORMDATAWITHPREDEFINEDMETRICSWORKER_H
