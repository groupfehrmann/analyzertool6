#include "principalcomponentanalysisworker.h"

PrincipalComponentAnalysisParameters::PrincipalComponentAnalysisParameters(QObject *parent) :
    BaseParameters(parent, "principalcomponentanalysis")
{

}

bool PrincipalComponentAnalysisParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    return true;

}

QString PrincipalComponentAnalysisParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[principal componenent analysis on data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[threshold maximum cumulative explained variance = " << threshold_cumulativeExplainedVariance << "]";

    stream << Qt::endl << "\t[threshold maximum number of components = " << threshold_maximumNumberOfComponents << "]";

    stream << Qt::endl << "\t[mode = " << mode << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

PrincipalComponentAnalysisWorker::PrincipalComponentAnalysisWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("principal component analysis"), parameters, "principalcomponentanalysis")
{

}

void PrincipalComponentAnalysisWorker::doWork_()
{

    QSharedPointer<PrincipalComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<PrincipalComponentAnalysisParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("to perform principal component analysis values in data need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool PrincipalComponentAnalysisWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<PrincipalComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<PrincipalComponentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void PrincipalComponentAnalysisWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<PrincipalComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<PrincipalComponentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}


QList<unsigned int> PrincipalComponentAnalysisWorker::createMatrixWithCountForOverlappingNonNaNs(const QList<double> &matrix, qsizetype leadingDimension)
{

    QList<unsigned int> matrixWithCountForOverlappingNonNaNs;

    matrixWithCountForOverlappingNonNaNs.reserve(matrix.count());

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double> *>(&matrix), leadingDimension);

    qsizetype nRows = matrix.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("whitening step : creating matrix with counts for overlapping non-NaNs", 0, static_cast<qsizetype>( nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor = [this, &uuidProgress, &dispatcher](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<unsigned int> counts(dispatcher.count(), 0);

        this->updateProgressWithOne(uuidProgress);

        for (qsizetype i = index_span.first; i < dispatcher.count(); ++i) {

            counts[i] = MathDescriptives::countIfBothElementsAreNonNaNs(index_span.second, dispatcher.at(i).second);

            if (QThread::currentThread()->isInterruptionRequested())
                return counts;

            this->updateProgressWithOne(uuidProgress);

            this->checkIfPauseWasRequested();

        }

        return counts;

    };

    auto reduceFunctor = [](QList<unsigned int> &result, const QList<unsigned int> &intermediateResult) {

        result.append(intermediateResult);

    };

    return QtConcurrent::blockingMappedReduced<QList<unsigned int>>(&d_threadPool, dispatcher, std::function<QList<unsigned int>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<unsigned int> &, const QList<unsigned int> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}
