#ifndef INDEPENDENTCOMPONENTANALYSISWORKER_H
#define INDEPENDENTCOMPONENTANALYSISWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include <algorithm>
#include <ranges>

#include <boost/math/statistics/univariate_statistics.hpp>
#include <boost/math/statistics/bivariate_statistics.hpp>
#include "boost/range/numeric.hpp"

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"
#include "containers/result.h"
#include "math/trip.h"
#include "math/mathutilityfunctions.h"
#include "math/mathoperations.h"
#include "math/fastica.h"
#include "math/fastdistancecorrelation.h"

#include <lapacke.h>

class IndependentComponentAnalysisParameters : public BaseParameters
{

public:

    IndependentComponentAnalysisParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString contrastFunction;

    double epsilon;

    QString exportDirectory;

    bool finetuneMode;

    qsizetype maximumNumberOfIterations;

    double mu;

    QString multiThreadMode;

    qsizetype numberOfRunsToPerform;

    Qt::Orientation orientation;

    double proportionOfSamplesToUseInAnIteration;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<QString> selectedResultItems;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    bool stabilizationMode;

    qsizetype threshold_maximumNumberOfComponents;

    double threshold_credibilityIndex;

    double threshold_cumulativeExplainedVariance;

    double pearsonCorrelationThresholdForConsensus;

    QString metricToDetermineIndependence;

    double distanceCorrelationThresholdForConsensus;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class IndependentComponentAnalysisWorker : public BaseWorker
{

public:

    IndependentComponentAnalysisWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    QList<double> performICARun(qsizetype runNumber, const QList<double> &whitenedMatrix, const QList<double> &whiteningMatrix, const QList<double> &dewhiteningMatrix, qsizetype numberOfValidComponents, int contrastFunction, const QList<QString> &componentLabels, int numberOfThreadsToUse);

    QList<unsigned int> createMatrixWithCountForOverlappingNonNaNs(const QList<double> &matrix, qsizetype leadingDimension);

    QList<double> xiCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension);

    QList<double> distanceCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension);

    QList<double> pearsonCorrelationSymmetricalMatrix(const QList<double> &matrix, qsizetype leadingDimension);

};

template<typename T>
void IndependentComponentAnalysisWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<IndependentComponentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<IndependentComponentAnalysisParameters>());

    // Fetching data
    ////////////////

    QUuid uuidProgress = this->startProgress("fetching data", 0, 0);

    QList<double> matrix;

    if (parameters->orientation == Qt::Vertical)
        matrix = annotatedMatrix->template sliced<double>(parameters->selectedVariableIndexes, parameters->selectedItemIndexes);
    else {

        matrix = annotatedMatrix->template sliced<double>(parameters->selectedItemIndexes, parameters->selectedVariableIndexes);

        MathOperations::transposeInplace(matrix, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // Setting up NaN-mode
    ////////////////

    uuidProgress = this->startProgress("whitening step : check data for NaNs", 0, 0);

    bool enableNaNMode = false;

    for (qsizetype i = 0; i < matrix.count(); ++i) {

        if (std::isnan(matrix.at(i))) {

            enableNaNMode = true;

            break;

        }
    }

    this->stopProgress(uuidProgress);

    // Creating covariance matrix
    ////////////////

    uuidProgress = this->startProgress("whitening step : allocating memory for covariance matrix", 0, 0);

    QList<double> covarianceMatrix(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

    this->stopProgress(uuidProgress);


    if (enableNaNMode) {

        this->reportWarning("detect NaN in the matrix, switching to NaN mode (experimental)");

        uuidProgress = this->startProgress("whitening step : flag NaNs in the matrix", 0, 0);

        QList<float> transformedMatrix(matrix.count());

        std::transform(matrix.begin(), matrix.end(), transformedMatrix.begin(), [](const double &value){return std::isnan(value) ? 0.0 : 1.0;});

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        uuidProgress = this->startProgress("whitening step : creating matrix with counts for overlapping non-NaNs", 0, 0);

        QList<float> countOverlappingNonNaNsMatrix(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

        cblas_ssyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), 1.0, transformedMatrix.data(), parameters->selectedItemIndexes.count(), 0.0, countOverlappingNonNaNsMatrix.data(), parameters->selectedVariableIndexes.count());

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        this->startProgress("whitening step : center variables", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count()), [](std::span<double> &span){MathOperations::centerVectorToMeanInplaceIgnoreNaNs(span);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        uuidProgress = this->startProgress("whitening step : set NaNs to zero", 0, 0);

        std::transform(matrix.begin(), matrix.end(), matrix.begin(), [](const double &value){return std::isnan(value) ? 0.0 : value;});

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        uuidProgress = this->startProgress("whitening step : calculate covariance matrix", 0, 0);

        cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), 1.0, matrix.data(), parameters->selectedItemIndexes.count(), 0.0, covarianceMatrix.data(), parameters->selectedVariableIndexes.count());

        for (qsizetype i = 0; i < covarianceMatrix.count(); ++i)
            covarianceMatrix[i] /= (static_cast<double>(countOverlappingNonNaNsMatrix.at(i)) - 1.0);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);

    } else {

        this->startProgress("whitening step : center variables", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count()), [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        uuidProgress = this->startProgress("whitening step : calculate covariance matrix", 0, 0);

        cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), (1.0 / (static_cast<double>(parameters->selectedItemIndexes.count()) - 1.0)), matrix.data(), parameters->selectedItemIndexes.count(), 0.0, covarianceMatrix.data(), parameters->selectedVariableIndexes.count());

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);

    }

    if (parameters->selectedResultItems.contains("whitening: covariance matrix")) {

        AnnotatedMatrix<double> *result_covarianceMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, parameters->selectedVariableIdentifiers, covarianceMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: covariance matrix", result_covarianceMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_covarianceMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_covariance_matrix");

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    ////////////////    

    // Obtaining eigenvalues and eigenvectors
    ////////////////

    uuidProgress = this->startProgress("whitening step : calculate eigenvalues and eigenvectors", 0, 0);

    double totalExplainedVariance = 0.0;

    for (qsizetype i = 0; i < covarianceMatrix.count(); i = i + parameters->selectedVariableIndexes.count() + 1)
        totalExplainedVariance += std::abs(covarianceMatrix.at(i));

    qsizetype m;

    QList<double> w(parameters->selectedVariableIndexes.count(), 0.0);

    QList<double> z(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

    QList<long long> isuppz(parameters->selectedVariableIndexes.count() * 2, 0.0);

    int info = LAPACKE_dsyevr(LAPACK_ROW_MAJOR, 'V', 'I', 'U', parameters->selectedVariableIndexes.count(), covarianceMatrix.data(), parameters->selectedVariableIndexes.count(), 0.0, 1.0, 1, parameters->selectedVariableIndexes.count(), -1.0, &m, w.data(), z.data(), parameters->selectedVariableIndexes.count(), isuppz.data());

    if (info < 0)
        this->reportError("DSYEVR: " + QString::number(info) + "th parameter had illegal value");
    else if (info > 0)
        this->reportError("DSYEVR: Error code -> " + QString::number(info));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    // covariance matrix not needed anymore
    covarianceMatrix.clear();

    std::reverse(w.begin(), w.end());

    this->startProgress("reversing eigenvectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, parameters->selectedVariableIndexes.count()), [](std::span<double> &span){std::reverse(span.begin(), span.end());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("calculate explained variance", 0, 0);

    QList<double> eigenvaluesSummary;

    double sumExplainedVariance = 0.0;

    QList<QString> componentLabels;

    qsizetype numberOfValidComponents = 0;

    double threshold = parameters->threshold_cumulativeExplainedVariance / 100.0;

    for (qsizetype i = 0; i < parameters->threshold_maximumNumberOfComponents; ++i) {

        double eigenvalue = w.at(i);

        if (eigenvalue > std::numeric_limits<double>::epsilon()) {

            double explainedVariance = std::fabs(eigenvalue) / totalExplainedVariance;

            sumExplainedVariance += explainedVariance;

            ++numberOfValidComponents;

            componentLabels.append("component " + QString::number(i + 1));

            eigenvaluesSummary << eigenvalue << explainedVariance * 100.0 << sumExplainedVariance * 100.0;

            if (almost_equal(threshold, sumExplainedVariance, 6) || sumExplainedVariance > threshold)
                break;

        }

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // Filtering eigenvalues and eigenvectors that do not meet the thresholds
    ////////////////

    uuidProgress = this->startProgress("filtering eigenvalues and eigenvectors that do not meet the thresholds", 0, 0);

    w.resize(numberOfValidComponents);

    MathOperations::transposeInplace(z, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    z.resize(numberOfValidComponents * parameters->selectedVariableIndexes.count());

    MathOperations::transposeInplace(z, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("whitening: eigenvalues")) {

        AnnotatedMatrix<double> *result_eigenvalues = new AnnotatedMatrix<double>(componentLabels, {"eigenvalues", "explained variance", "cumulative explained variance"}, std::move(eigenvaluesSummary));

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: eigenvalues", result_eigenvalues));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_eigenvalues);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_eigenvalues");

        }

    }

    if (parameters->selectedResultItems.contains("whitening: eigenvectors")) {

        AnnotatedMatrix<double> *result_eigenvectors = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, z);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: eigenvectors", result_eigenvectors));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_eigenvectors);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_eigenvectors");

        }

    }

    if (numberOfValidComponents < parameters->threshold_maximumNumberOfComponents)
        this->reportMessage(QString::number(numberOfValidComponents) + " components used for whitening after removing components that did not meet the threshold of (eigenvalue > " + QString::number(std::numeric_limits<double>::epsilon()) + " AND cumulative explained variance < " + QString::number(static_cast<double>(parameters->threshold_cumulativeExplainedVariance)) + "%)");
    else
        this->reportMessage(QString::number(parameters->threshold_maximumNumberOfComponents) + " components used for whitening");

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    ////////////////

    // Creating dewhitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::sqrt(value);});

    QList<double> dewhiteningMatrix = z;

    this->startProgress("whitening step : creating dewhitening matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(dewhiteningMatrix, numberOfValidComponents), [&w](std::span<double> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), std::multiplies<double>());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (parameters->selectedResultItems.contains("whitening: dewhitening matrix")) {

        AnnotatedMatrix<double> *result_dewhiteningMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, dewhiteningMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: dewhitening matrix", result_dewhiteningMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_dewhiteningMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_dewhitening_matrix");

        }

    }

    ////////////////

    // Creating whitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::pow(value, -1);});

    QList<double> whiteningMatrix = z;

    // eigenvectors not needed anymore
    z.clear();

    this->startProgress("whitening step : creating whitening matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(whiteningMatrix, numberOfValidComponents), [&w](std::span<double> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), std::multiplies<double>());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    // eigenvalues not needed anymore
    w.clear();

    uuidProgress = this->startProgress("whitening step : transposing whitening matrix", 0, 0);

    MathOperations::transposeInplace(whiteningMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (parameters->selectedResultItems.contains("whitening: whitening matrix")) {

        AnnotatedMatrix<double> *result_whiteningMatrix = new AnnotatedMatrix<double>(componentLabels, parameters->selectedVariableIdentifiers, whiteningMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: whitening matrix", result_whiteningMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_whiteningMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_whitening_matrix");

        }

    }

    ////////////////

    // Creating whitened matrix
    ////////////////

    uuidProgress = this->startProgress("whitening step : creating whitened matrix", 0, 0);

    QList<double> whitenedMatrix(numberOfValidComponents * parameters->selectedItemIndexes.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, numberOfValidComponents, parameters->selectedItemIndexes.count(), parameters->selectedVariableIndexes.count(), 1.0, whiteningMatrix.data(), parameters->selectedVariableIndexes.count(), matrix.data(), parameters->selectedItemIndexes.count(), 0.0, whitenedMatrix.data(), parameters->selectedItemIndexes.count());

    // matrix with centered variables not needed anymore
    matrix.clear();

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (parameters->selectedResultItems.contains("whitening: whitened matrix")) {

        AnnotatedMatrix<double> *result_whitenedMatrix = new AnnotatedMatrix<double>(componentLabels, parameters->selectedItemIdentifiers, whitenedMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening: whitened matrix", result_whitenedMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_whitenedMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_whitened_matrix");

        }

    }

    ////////////////

    int contrastFunction;

    QString contrastFunctionString = parameters->contrastFunction;

    if (contrastFunctionString == "Raise to power 3")
        contrastFunction = FICA_NONLIN_POW3;
    else if (contrastFunctionString == "Hyperbolic tangent")
        contrastFunction = FICA_NONLIN_TANH;
    else if (contrastFunctionString == "Gaussian")
        contrastFunction = FICA_NONLIN_GAUSS;
    else if (contrastFunctionString == "Skewness")
        contrastFunction = FICA_NONLIN_SKEW;
    else
        contrastFunction = FICA_NONLIN_TANH;


    if (parameters->numberOfRunsToPerform == 1) {

        this->performICARun(-1, whitenedMatrix, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, componentLabels, parameters->numberOfThreadsToUse);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    } else {

        QList<double> independentComponentsForAllRuns;

        if (parameters->multiThreadMode == "perform runs simultaneously") {

            QMutex mutex;

            uuidProgress = this->startProgress("performing ICA runs", 1, parameters->numberOfRunsToPerform);

            auto performICARuns = [&](const std::pair<qsizetype, qsizetype> &block, qsizetype numberOfThreadsToUse) {

                for (qsizetype i = block.first; i < block.second; ++i) {

                    if (QThread::currentThread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    QList<double> independentComponents = this->performICARun(i + 1, whitenedMatrix, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, componentLabels, numberOfThreadsToUse);

                    if (this->thread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    mutex.lock();

                    independentComponentsForAllRuns.append(independentComponents);

                    mutex.unlock();

                    this->updateProgressWithOne(uuidProgress);

                }

            };

            QFutureSynchronizer<void> futureSynchronizer;

            for (const std::pair<qsizetype, qsizetype> &block : MathUtilityFunctions::createIndexBlocksForMultiThreading(parameters->numberOfRunsToPerform, parameters->numberOfThreadsToUse))
                futureSynchronizer.addFuture(QtConcurrent::run(performICARuns, block, 1));

            futureSynchronizer.waitForFinished();

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->stopProgress(uuidProgress);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

        } else {

            uuidProgress = this->startProgress("performing ICA runs", 0, parameters->numberOfRunsToPerform);

            for (qsizetype i = 0; i < parameters->numberOfRunsToPerform; ++i) {

                independentComponentsForAllRuns.append(this->performICARun(i + 1, whitenedMatrix, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, componentLabels, parameters->numberOfThreadsToUse));

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress, i + 1);

            }

            this->stopProgress(uuidProgress);

        }

        // Creating correlation matrix
        ////////////////

        QMutex mutex;

        QList<std::pair<qsizetype, std::span<double>>> independentComponentsForAllRuns_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(independentComponentsForAllRuns, parameters->selectedItemIdentifiers.count());


        QList<double> independentComponentsForAllRuns_means;

        independentComponentsForAllRuns_means.reserve(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

        QList<double> independentComponentsForAllRuns_l2Norms;

        independentComponentsForAllRuns_l2Norms.reserve(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

        for (const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan : independentComponentsForAllRuns_rowIndexes_rowSpans) {

            double mean = boost::math::statistics::mean(independentComponentsForAllRuns_rowIndex_rowSpan.second);

            independentComponentsForAllRuns_means << mean;

            for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
                value = value - mean;

            double l2Norm = boost::math::tools::l2_norm(independentComponentsForAllRuns_rowIndex_rowSpan.second.begin(), independentComponentsForAllRuns_rowIndex_rowSpan.second.end());

            independentComponentsForAllRuns_l2Norms << l2Norm;

            for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
                value = value / l2Norm;

        }

        QList<QPair<qsizetype, QList<qsizetype>>> setsOflinkedIndexes(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

        for (qsizetype i = 0; i < setsOflinkedIndexes.count(); ++i) {

            setsOflinkedIndexes[i].first = i;

            setsOflinkedIndexes[i].second.reserve(parameters->numberOfRunsToPerform);

        }

        uuidProgress = this->startProgress("consensus step : calculating pearson matrix", 0, (independentComponentsForAllRuns_rowIndexes_rowSpans.count() * independentComponentsForAllRuns_rowIndexes_rowSpans.count() - independentComponentsForAllRuns_rowIndexes_rowSpans.count()) / 2.0);

        qsizetype progress = 0;

        auto mapFunctor1 = [&](const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan) {

            for (const std::pair<qsizetype, std::span<double>> &temp : std::ranges::views::drop(independentComponentsForAllRuns_rowIndexes_rowSpans, independentComponentsForAllRuns_rowIndex_rowSpan.first + 1)) {

                double r_coeff = 0.0;

                for (unsigned long j = 0; j < temp.second.size(); ++j)
                    r_coeff += independentComponentsForAllRuns_rowIndex_rowSpan.second[j] * temp.second[j];

                if (std::abs(r_coeff) >= parameters->pearsonCorrelationThresholdForConsensus) {

                    mutex.lock();

                    setsOflinkedIndexes[independentComponentsForAllRuns_rowIndex_rowSpan.first].second.append(temp.first);

                    setsOflinkedIndexes[temp.first].second.append(independentComponentsForAllRuns_rowIndex_rowSpan.first);

                    mutex.unlock();

                }

            }

            mutex.lock();

            progress += independentComponentsForAllRuns_rowIndexes_rowSpans.count() - (independentComponentsForAllRuns_rowIndex_rowSpan.first + 1);

            mutex.unlock();

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, progress);

        };

        this->startProgress("consensus step : calculating pearson distance matrix", QtConcurrent::map(&d_threadPool, independentComponentsForAllRuns_rowIndexes_rowSpans, mapFunctor1), false);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        emit this->progressStopped(uuidProgress);


        for (const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan : independentComponentsForAllRuns_rowIndexes_rowSpans) {

            double l2Norm = independentComponentsForAllRuns_l2Norms.at(independentComponentsForAllRuns_rowIndex_rowSpan.first);

            for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
                value = value * l2Norm;

            double mean = independentComponentsForAllRuns_means.at(independentComponentsForAllRuns_rowIndex_rowSpan.first);

            for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
                value = value + mean;

        }

        QSet<qsizetype> availableIndexes;

        for (qsizetype i = 0; i < independentComponentsForAllRuns_rowIndexes_rowSpans.count(); ++i)
            availableIndexes.insert(i);

        QList<QList<qsizetype>> consensusClusterToMemberIndexes;

        uuidProgress = this->startProgress("consensus step : creating consensus clusters", 0, setsOflinkedIndexes.count());

        std::function<void (const QList<qsizetype> &)> updateAvailableIndexes = [&](const QList<qsizetype> &indexes) {

            for (qsizetype index : indexes) {

                if (availableIndexes.contains(index)) {

                    availableIndexes.remove(index);

                    for (const QPair<qsizetype, QList<qsizetype>> &setOfLinkedIndexes : setsOflinkedIndexes) {

                        if (setOfLinkedIndexes.first == index)

                            updateAvailableIndexes(setOfLinkedIndexes.second);

                    }

                }

            }

        };

        while (!setsOflinkedIndexes.isEmpty()) {

            std::sort(setsOflinkedIndexes.begin(), setsOflinkedIndexes.end(), [](const QPair<qsizetype, QList<qsizetype>> &set1, const QPair<qsizetype, QList<qsizetype>> &set2){return set1.second.count() < set2.second.count();});

            if (!setsOflinkedIndexes.last().second.isEmpty()) {

                consensusClusterToMemberIndexes.append(setsOflinkedIndexes.last().second << setsOflinkedIndexes.last().first);

                updateAvailableIndexes(setsOflinkedIndexes.last().second);

            }

            availableIndexes.remove(setsOflinkedIndexes.last().first);

            setsOflinkedIndexes.removeLast();

            for (qsizetype i = setsOflinkedIndexes.count() - 1; i >= 0; --i) {

                if (!availableIndexes.contains(setsOflinkedIndexes.at(i).first))
                    setsOflinkedIndexes.removeAt(i);

            }

            for (qsizetype i = setsOflinkedIndexes.count() - 1; i >= 0; --i) {

                QMutableListIterator<qsizetype> it(setsOflinkedIndexes[i].second);

                while (it.hasNext()) {

                    qsizetype index = it.next();

                    if (!availableIndexes.contains(index))
                        it.remove();

                }

            }

            this->updateProgressWithOne(uuidProgress);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

        }

        emit this->progressStopped(uuidProgress);

        if (consensusClusterToMemberIndexes.isEmpty()) {

            this->reportMessage("no clusters were detected after clustering pearson distance matrix");

            this->requestInterruption();

            return;

        } else
            this->reportMessage("number of cluster detected after clustering pearson distance matrix : " + QString::number(consensusClusterToMemberIndexes.count()));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        uuidProgress = this->startProgress("consensus step : filtering consensus components based on credibility index", 0, 0);

        qsizetype consensus_counter = 0;

        qsizetype index_counter = 0;

        QList<double> independentComponentsForAllRuns_afterFilteringOnCredibilityIndex;

        for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

            double credibilityIndex = static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(parameters->numberOfRunsToPerform);

            if ((consensusClusterToMemberIndexes.at(i).count() > 1) && (credibilityIndex > parameters->threshold_credibilityIndex)) {

                for (qsizetype &index : consensusClusterToMemberIndexes[i]) {

                    const QPair<qsizetype, std::span<double>> &index_span(independentComponentsForAllRuns_rowIndexes_rowSpans.at(index));

                    independentComponentsForAllRuns_afterFilteringOnCredibilityIndex.append(QList<double>(index_span.second.begin(), index_span.second.end()));

                    index = index_counter;

                    ++index_counter;

                }

            ++consensus_counter;

            } else
                break;

        }

        consensusClusterToMemberIndexes.resize(consensus_counter);

        this->reportMessage("number of cluster remaining after filtering based on credibility index : " + QString::number(consensus_counter));

        this->stopProgress(uuidProgress);


        if (consensus_counter == 0) {

            this->reportError("no consensus clusters remaining after filtering based on credibility index");

            this->requestInterruption();

            return;

        }

        independentComponentsForAllRuns_rowIndexes_rowSpans.clear();

        independentComponentsForAllRuns.clear();


        QList<std::pair<qsizetype, std::span<double>>> independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex, parameters->selectedItemIdentifiers.count());

        QList<double> pearsonCorrelationMatrix = this->pearsonCorrelationSymmetricalMatrix(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex, parameters->selectedItemIdentifiers.count());

        MathOperations::mirrorSymmetrixMatrix(pearsonCorrelationMatrix, independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count());

        QList<std::span<double>> pearsonCorrelationMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(pearsonCorrelationMatrix,  independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count());


        auto mapFunctor2 = [&](const QList<qsizetype> &memberIndexes) {

            QSet<qsizetype> set(memberIndexes.begin(), memberIndexes.constEnd());

            QList<double> meanAbsPearsonCorrelationWithAllOthersOutsideCluster;

            meanAbsPearsonCorrelationWithAllOthersOutsideCluster.reserve(memberIndexes.count());

            for (qsizetype index : memberIndexes) {

                const std::span<double> &span(pearsonCorrelationMatrix_rowSpans.at(index));

                double meanAbsPearsonCorrelation = 0.0;

                for (unsigned long i = 0; i < span.size(); ++i) {

                    if (!set.contains(i))
                        meanAbsPearsonCorrelation += std::abs(span[i]);

                }

                meanAbsPearsonCorrelationWithAllOthersOutsideCluster.append(meanAbsPearsonCorrelation / static_cast<double>(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count() - memberIndexes.count()));

            }

            qsizetype indexOfMin = cblas_idamin(meanAbsPearsonCorrelationWithAllOthersOutsideCluster.count(), meanAbsPearsonCorrelationWithAllOthersOutsideCluster.data(), 1);

            const std::span<double> &spanOfWinner(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.at(memberIndexes.at(indexOfMin)).second);

            if (memberIndexes.count() == 1)
                return std::pair<double, QList<double>>(std::numeric_limits<double>::quiet_NaN(), QList<double>(spanOfWinner.begin(), spanOfWinner.end()));
            else
                return std::pair<double, QList<double>>(meanAbsPearsonCorrelationWithAllOthersOutsideCluster.at(indexOfMin), QList<double>(spanOfWinner.begin(), spanOfWinner.end()));

        };


        auto reduceFunctor = [](QList<std::pair<double, QList<double>>> &result, const std::pair<double, QList<double>> &intermediateResult) {

            result.append(intermediateResult);

        };

        QFuture<QList<std::pair<double, QList<double>>>> future = QtConcurrent::mappedReduced(&d_threadPool, consensusClusterToMemberIndexes, std::function<std::pair<double, QList<double>>(const QList<qsizetype> &memberIndexes)>(mapFunctor2), std::function<void( QList<std::pair<double, QList<double>>> &, const std::pair<double, QList<double>> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

        this->startProgress("consensus step : get winner per clusters", future);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        QList<double> consensusIndependentComponents;

        for (auto &item : future.result())
            consensusIndependentComponents.append(item.second);


        if (parameters->metricToDetermineIndependence == "XICOR")
            uuidProgress = this->startProgress("consensus step : filtering consensus components based on XICOR correlation", 0, 0);
        else
            uuidProgress = this->startProgress("consensus step : filtering consensus components based on distance correlation", 0, 0);

        QSet<qsizetype> indexPool;

        for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i)
            indexPool.insert(i);

        QList<double> distanceCorrelationMatrix;

        if (parameters->metricToDetermineIndependence == "XICOR")
            distanceCorrelationMatrix = this->xiCorrelationSymmetricalMatrix(consensusIndependentComponents, parameters->selectedItemIndexes.count());
        else
            distanceCorrelationMatrix = this->distanceCorrelationSymmetricalMatrix(consensusIndependentComponents, parameters->selectedItemIndexes.count());

        distanceCorrelationMatrix.detach();

        for (qsizetype i = 0; i < distanceCorrelationMatrix.count();) {

            distanceCorrelationMatrix[i] = 0.0;

            i += consensusClusterToMemberIndexes.count() + static_cast<qsizetype>(1);

        }

        QList<std::span<double>> rowSpansOfDistanceCorrelationMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(distanceCorrelationMatrix, consensusClusterToMemberIndexes.count());


        consensusIndependentComponents.clear();

        componentLabels.clear();


        QList<double> robustnessMetricsConsensusIndependentComponents;

        consensus_counter = 0;

        for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

            if (indexPool.contains(i)) {

                if (i >= numberOfValidComponents)
                    break;

                robustnessMetricsConsensusIndependentComponents.append(static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(parameters->numberOfRunsToPerform));

                robustnessMetricsConsensusIndependentComponents.append(consensusClusterToMemberIndexes.at(i).count());

                robustnessMetricsConsensusIndependentComponents.append(future.result().at(i).first);

                consensusIndependentComponents.append(future.result().at(i).second);

                componentLabels.append("consensus independent component " + QString::number(consensus_counter + 1));

                ++consensus_counter;

            }

            QMutableSetIterator<qsizetype> it(indexPool);

            while (it.hasNext()) {

                if (rowSpansOfDistanceCorrelationMatrix.at(i)[it.next()] >= parameters->distanceCorrelationThresholdForConsensus)
                    it.remove();

            }

        }

        if (parameters->metricToDetermineIndependence == "XICOR")
            this->reportMessage("number of cluster remaining after filtering based on XICOR : " + QString::number(componentLabels.count()));
        else
            this->reportMessage("number of cluster remaining after filtering based on distance correlation : " + QString::number(componentLabels.count()));

        if (componentLabels.count() == 0) {

            if (parameters->metricToDetermineIndependence == "XICOR")
                this->reportError("no consensus clusters remaining after filtering based on XICOR");
            else
                this->reportError("no consensus clusters remaining after filtering based on distance correlation");

            this->requestInterruption();

            return;

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        if (parameters->selectedResultItems.contains("ICA: robustness metrics independent components (consensus)")) {

            AnnotatedMatrix<double> *result_robustnessMetricsConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, {"credibility index", "number of members in cluster", "average Pearson correlation of with all other clusters"}, robustnessMetricsConsensusIndependentComponents);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: robustness metrics independent components (consensus)", result_robustnessMetricsConsensusIndependentComponents));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_robustnessMetricsConsensusIndependentComponents);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_robustness_metrics_independent_components_consensus");

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        if (parameters->selectedResultItems.contains("ICA: XICOR or distance correlation between independent components (consensus)")) {

            AnnotatedMatrix<double> *result_dCorrelationConsensusIndependentComponents;

            if (parameters->metricToDetermineIndependence == "XICOR")
                result_dCorrelationConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->xiCorrelationSymmetricalMatrix(consensusIndependentComponents, parameters->selectedItemIndexes.count()));
            else
                result_dCorrelationConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->distanceCorrelationSymmetricalMatrix(consensusIndependentComponents, parameters->selectedItemIndexes.count()));

            if (parameters->exportDirectory.isEmpty()) {

                if (parameters->metricToDetermineIndependence == "XICOR")
                    emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: XICOR between independent components (consensus)", result_dCorrelationConsensusIndependentComponents));
                else
                    emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: distance correlation between independent components (consensus)", result_dCorrelationConsensusIndependentComponents));

            } else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_dCorrelationConsensusIndependentComponents);

                if (parameters->metricToDetermineIndependence == "XICOR")
                    this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_XICOR_between_independent_components_consensus");
                else
                    this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_distance_correlation_between_independent_components_consensus");

            }

        }

        QList<double> flippedConsensusMixingMatrix;

        QList<double> flippedConsensusIndependentComponents;

        if (parameters->selectedResultItems.contains("ICA: mixing matrix (consensus)") || parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)")) {

            uuidProgress = this->startProgress("consensus step : calculate consensus mixing matrix", 0, 0);

            QList<double> pseudoInverseConcensusIndependentComponents = MathOperations::pseudoInverse(consensusIndependentComponents, parameters->selectedItemIndexes.count(), parameters->numberOfThreadsToUse, &info);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            if (info < 0)
                this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");
            else if (info > 0)
                this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

            QList<double> temp(componentLabels.count() * numberOfValidComponents, 0.0);

            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, componentLabels.count(), numberOfValidComponents, parameters->selectedItemIndexes.count(), 1.0, pseudoInverseConcensusIndependentComponents.data(), parameters->selectedItemIndexes.count(), whitenedMatrix.data(), parameters->selectedItemIndexes.count(), 0.0, temp.data(), numberOfValidComponents);

            QList<double> consensusMixingMatrix(parameters->selectedVariableIndexes.count() * componentLabels.count(), 0.0);

            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, parameters->selectedVariableIndexes.count(), componentLabels.count(), numberOfValidComponents, 1.0, dewhiteningMatrix.data(), numberOfValidComponents, temp.data(), numberOfValidComponents, 0.0, consensusMixingMatrix.data(), componentLabels.count());

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->stopProgress(uuidProgress);

            if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)"))
                flippedConsensusMixingMatrix = consensusMixingMatrix;

            AnnotatedMatrix<double> *result_consensusMixingMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, consensusMixingMatrix);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: mixing matrix (consensus)", result_consensusMixingMatrix));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_consensusMixingMatrix);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_mixing_matrix_consensus");

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        if (parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)"))
            flippedConsensusIndependentComponents = consensusIndependentComponents;

        if (parameters->selectedResultItems.contains("ICA: independent components (consensus)")) {

            uuidProgress = this->startProgress("consensus step : transposing consensus independent components", 0, 0);

            MathOperations::transposeInplace(consensusIndependentComponents, parameters->selectedItemIndexes.count(), parameters->numberOfThreadsToUse);

            this->stopProgress(uuidProgress);

            AnnotatedMatrix<double> *result_consensusIndependentComponents = new AnnotatedMatrix<double>(parameters->selectedItemIdentifiers, componentLabels, consensusIndependentComponents);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: independent components (consensus)", result_consensusIndependentComponents));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_consensusIndependentComponents);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_independent_components_consensus");

            }

        }

        if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)") || parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)")) {

            uuidProgress = this->startProgress("consensus step : transposing consensus mixing matrix", 0, 0);

            MathOperations::transposeInplace(flippedConsensusMixingMatrix, componentLabels.count(), parameters->numberOfThreadsToUse);

            QList<std::span<double>> flippedConsensusMixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(flippedConsensusMixingMatrix, parameters->selectedVariableIndexes.count());

            QList<std::span<double>> flippedConsensusIndependentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(flippedConsensusIndependentComponents, parameters->selectedItemIndexes.count());

            for (qsizetype i = 0; i < componentLabels.count(); ++i) {

                std::span<double> &flippedConsensusIndependentComponents_rowSpan_i(flippedConsensusIndependentComponents_rowSpans[i]);

                std::span<double> &flippedConsensusMixingMatrix_rowSpan_i(flippedConsensusMixingMatrix_rowSpans[i]);

                if (boost::math::statistics::skewness(flippedConsensusIndependentComponents_rowSpan_i) < 0.0) {

                    std::transform(flippedConsensusIndependentComponents_rowSpan_i.begin(), flippedConsensusIndependentComponents_rowSpan_i.end(), flippedConsensusIndependentComponents_rowSpan_i.begin(), std::negate<double>());

                    std::transform(flippedConsensusMixingMatrix_rowSpan_i.begin(), flippedConsensusMixingMatrix_rowSpan_i.end(), flippedConsensusMixingMatrix_rowSpan_i.begin(), std::negate<double>());

                }

            }

            MathOperations::transposeInplace(flippedConsensusMixingMatrix, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

            MathOperations::transposeInplace(flippedConsensusIndependentComponents, parameters->selectedItemIndexes.count(), parameters->numberOfThreadsToUse);

            if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)")) {

                AnnotatedMatrix<double> *result_flippedConsensusMixingMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, flippedConsensusMixingMatrix);

                if (parameters->exportDirectory.isEmpty())
                    emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: flipped mixing matrix (consensus)", result_flippedConsensusMixingMatrix));
                else {

                    QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_flippedConsensusMixingMatrix);

                    this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_flipped_mixing_matrix_consensus");

                }

            }

            if (parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)")) {

                AnnotatedMatrix<double> *result_flippedConsensusIndependentComponents = new AnnotatedMatrix<double>(parameters->selectedItemIdentifiers, componentLabels, flippedConsensusIndependentComponents);

                if (parameters->exportDirectory.isEmpty())
                    emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("ICA: flipped independent components (consensus)", result_flippedConsensusIndependentComponents));
                else {

                    QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_flippedConsensusIndependentComponents);

                    this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_flipped_independent_components_consensus");

                }

            }

        }

    }

}

#endif // INDEPENDENTCOMPONENTANALYSISWORKER_H
