#include "selectfilewidget.h"
#include "ui_selectfilewidget.h"

SelectFileWidget::SelectFileWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier, QFileDialog::AcceptMode acceptMode) :
    QWidget(parent),
    ui(new Ui::SelectFileWidget),
    d_acceptMode(acceptMode),
    d_fileDialog(new QFileDialog(this, QCoreApplication::applicationName() + " - select file")),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

    this->connect(d_fileDialog, &QFileDialog::filterSelected, this, &SelectFileWidget::filterSelected);

}

QFileDialog *SelectFileWidget::fileDialog()
{

    return d_fileDialog;

}

void SelectFileWidget::evaluatePath()
{

    if (d_acceptMode == QFileDialog::AcceptOpen) {

        if (QFile::exists(ui->lineEdit_path->text())) {

            ui->lineEdit_path->setStyleSheet(QString());

            emit pathChanged(ui->lineEdit_path->text());

        } else {

            ui->lineEdit_path->setStyleSheet(QString("color: rgb(255, 0, 0);"));

            emit invalidPath();

        }

    } else
        emit pathChanged(ui->lineEdit_path->text());

}

QLabel *SelectFileWidget::label()
{

    return ui->label;

}

void SelectFileWidget::on_lineEdit_path_textEdited(const QString &text)
{

    Q_UNUSED(text);

    this->evaluatePath();

}

void SelectFileWidget::on_pushButton_browse_clicked()
{

    d_fileDialog->setAcceptMode(d_acceptMode);

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

    if (settings.contains("state"))
        d_fileDialog->restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        d_fileDialog->restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        d_fileDialog->setDirectory(QDir(settings.value("lastDirectory").toString()));

    if (settings.contains("selectedNameFilter"))
        d_fileDialog->selectNameFilter(settings.value("selectedNameFilter").toString());

    settings.endGroup();

    if (d_fileDialog->exec()) {

        ui->lineEdit_path->setText(d_fileDialog->selectedFiles().first());

        this->evaluatePath();

        QSettings settings;

        settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

        settings.setValue("state", d_fileDialog->saveState());

        settings.setValue("geometry", d_fileDialog->saveGeometry());

        settings.setValue("lastDirectory", d_fileDialog->directory().path());

        settings.setValue("selectedNameFilter", d_fileDialog->selectedNameFilter());

        settings.endGroup();

        emit this->filterSelected(d_fileDialog->selectedNameFilter());

    } else
        ui->lineEdit_path->setText(QString());

}

void SelectFileWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit_path");

    if (settings.contains("text"))
        ui->lineEdit_path->setText(settings.value("text").toString());

    settings.endGroup();

    d_readWidgetSettings = true;

    emit pathChanged(ui->lineEdit_path->text());

}

void SelectFileWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit_path");

    settings.setValue("text", ui->lineEdit_path->text());

    settings.endGroup();

}

SelectFileWidget::~SelectFileWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
