#include "sysmexdataprocessordialog.h"

SysmexDataProcessorDialog::SysmexDataProcessorDialog(BaseDialog *parent, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices) :
    BaseDialog(parent, "sysmex data processor", "SysmexDataProcessorDialog"),
    d_baseAnnotatedMatrices(baseAnnotatedMatrices)
{

    BaseDialog::d_parameters = QSharedPointer<SysmexDataProcessorParameters>(new SysmexDataProcessorParameters);

    BaseDialog::initWidgets();

}

void SysmexDataProcessorDialog::initWidgets_()
{

    QList<QString> baseAnnotatedMatrixLabels;

    for (qsizetype i = 0; i < d_baseAnnotatedMatrices.count(); ++i)
        baseAnnotatedMatrixLabels.append(d_baseAnnotatedMatrices.at(i)->property("label").toString());


    ComboBoxWidget *comboBoxWidget_patientInfo = new ComboBoxWidget(this, "comboBoxWidget_patientInfo");

    this->connect(comboBoxWidget_patientInfo->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->data_patientInfo = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_patientInfo->label()->setText("select data containing patient information");

    comboBoxWidget_patientInfo->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_patientInfo, 0, 0, 1, 2);


    d_orientationWidget_patientInfo = new OrientationWidget(this, "d_orientationWidget_patientInfo");

    d_orientationWidget_patientInfo->label()->setText("select orientation of patient information");

    this->connect(d_orientationWidget_patientInfo, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->orientation_data_patientInfo = orientation;});

    d_orientationWidget_patientInfo->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_patientInfo, 1, 0, 1, 2);


    d_itemSelectorWidget_variables_patientInfo = new ItemSelectorWidget(this, "d_itemSelectorWidget_variables_patientInfo");

    d_itemSelectorWidget_variables_patientInfo->label()->setText("select variables from patient information to include in export");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables_patientInfo, 2, 0, 1, 2);

    this->connect(d_itemSelectorWidget_variables_patientInfo->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers_data_patientInfo = itemLabels; static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes_data_patientInfo = itemIndexes;});


    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId = new ComboBoxWidget(this, "d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId");

    this->connect(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->patientInfo_variableLabelDefiningColumnWithUniquePatientId = text;});

    this->connect(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->patientInfo_variableIndexDefiningColumnWithUniquePatientId = index;});

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId->label()->setText("select variable label defining column with unique patient id");

    d_gridLayout->addWidget(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId, 3, 0, 1, 2);



    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId = new ComboBoxWidget(this, "d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId");

    this->connect(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->patientInfo_variableLabelDefiningColumnWithSysmexId = text;});

    this->connect(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->patientInfo_variableIndexDefiningColumnWithSysmexId = index;});

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId->label()->setText("select variable label defining column with sysmex id");

    d_gridLayout->addWidget(d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId, 4, 0, 1, 2);


    ComboBoxWidget *comboBoxWidget_sysmexInfo = new ComboBoxWidget(this, "comboBoxWidget_sysmexInfo");

    this->connect(comboBoxWidget_sysmexInfo->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->data_sysmexInfo = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_sysmexInfo->label()->setText("select data containing sysmex information");

    comboBoxWidget_sysmexInfo->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_sysmexInfo, 5, 0, 1, 2);


    d_orientationWidget_sysmexInfo = new OrientationWidget(this, "d_orientationWidget_sysmexInfo");

    d_orientationWidget_sysmexInfo->label()->setText("select orientation of sysmex information");

    this->connect(d_orientationWidget_sysmexInfo, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->orientation_data_sysmexInfo = orientation;});

    d_orientationWidget_sysmexInfo->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_sysmexInfo, 6, 0, 1, 2);


    d_itemSelectorWidget_variables_sysmexInfo = new ItemSelectorWidget(this, "d_itemSelectorWidget_variables_sysmexInfo");

    d_itemSelectorWidget_variables_sysmexInfo->label()->setText("select variables from sysmex information to include in expert");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables_sysmexInfo, 7, 0, 1, 2);

    this->connect(d_itemSelectorWidget_variables_sysmexInfo->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers_data_sysmexInfo = itemLabels; static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes_data_sysmexInfo = itemIndexes;});


    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId = new ComboBoxWidget(this, "d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId");

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableLabelDefiningColumnWithSysmexId = text;});

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableIndexDefiningColumnWithSysmexId = index;});

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId->label()->setText("select variable label defining column with sysmex id");

    d_gridLayout->addWidget(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId, 8, 0, 1, 2);


    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate = new ComboBoxWidget(this, "d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate");

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableLabelDefiningColumnWithDate = text;});

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableIndexDefiningColumnWithDate = index;});

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate->label()->setText("select variable label defining column with sampling date");

    d_gridLayout->addWidget(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate, 9, 0, 1, 2);


    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime = new ComboBoxWidget(this, "d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime");

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableLabelDefiningColumnWithTime = text;});

    this->connect(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->sysmexInfo_variableIndexDefiningColumnWithTime = index;});

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime->label()->setText("select variable label defining column with sampling time");

    d_gridLayout->addWidget(d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime, 10, 0, 1, 2);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 11, 0, 1, 2);


    this->connect(d_orientationWidget_patientInfo, &OrientationWidget::orientationChanged, this, &SysmexDataProcessorDialog::orientation_patientInfo_changed);

    this->connect(comboBoxWidget_patientInfo->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientation_patientInfo_changed(d_orientationWidget_patientInfo->orientation());});

    this->orientation_patientInfo_changed(d_orientationWidget_patientInfo->orientation());


    this->connect(d_orientationWidget_sysmexInfo, &OrientationWidget::orientationChanged, this, &SysmexDataProcessorDialog::orientation_sysmexInfo_changed);

    this->connect(comboBoxWidget_sysmexInfo->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientation_sysmexInfo_changed(d_orientationWidget_sysmexInfo->orientation());});

    this->orientation_sysmexInfo_changed(d_orientationWidget_sysmexInfo->orientation());

}

void SysmexDataProcessorDialog::orientation_patientInfo_changed(Qt::Orientation orientation)
{

    SysmexDataProcessorParameters *sysmexDataProcessorParameters = static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables_patientInfo->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(sysmexDataProcessorParameters->data_patientInfo->nonSelectedIdentifiers(orientation), sysmexDataProcessorParameters->data_patientInfo->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables_patientInfo->itemSelectorModel_selected()->setItemLabelsAndIndexes(sysmexDataProcessorParameters->data_patientInfo->identifiers(orientation, true), sysmexDataProcessorParameters->data_patientInfo->indexes(orientation, true));

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId->comboBox()->clear();

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId->comboBox()->addItems(sysmexDataProcessorParameters->data_patientInfo->identifiers(orientation, false));

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId->comboBox()->clear();

    d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId->comboBox()->addItems(sysmexDataProcessorParameters->data_patientInfo->identifiers(orientation, false));

}

void SysmexDataProcessorDialog::orientation_sysmexInfo_changed(Qt::Orientation orientation)
{

    SysmexDataProcessorParameters *sysmexDataProcessorParameters = static_cast<SysmexDataProcessorParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables_sysmexInfo->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(sysmexDataProcessorParameters->data_sysmexInfo->nonSelectedIdentifiers(orientation), sysmexDataProcessorParameters->data_sysmexInfo->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables_sysmexInfo->itemSelectorModel_selected()->setItemLabelsAndIndexes(sysmexDataProcessorParameters->data_sysmexInfo->identifiers(orientation, true), sysmexDataProcessorParameters->data_sysmexInfo->indexes(orientation, true));

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId->comboBox()->clear();

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId->comboBox()->addItems(sysmexDataProcessorParameters->data_sysmexInfo->identifiers(orientation, false));

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate->comboBox()->clear();

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate->comboBox()->addItems(sysmexDataProcessorParameters->data_sysmexInfo->identifiers(orientation, false));

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime->comboBox()->clear();

    d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime->comboBox()->addItems(sysmexDataProcessorParameters->data_sysmexInfo->identifiers(orientation, false));

}
