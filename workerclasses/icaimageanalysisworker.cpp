#include "icaimageanalysisworker.h"

ICAImageAnalysisParameters::ICAImageAnalysisParameters(QObject *parent) :
    BaseParameters(parent, "icaimageanalysis")
{

}

bool ICAImageAnalysisParameters::isValid_()
{

    if (!QDir(directoryToScanForTrainingImages).exists()) {

        d_lastError = "no valid directory selected to scan for images : \"" + directoryToScanForTrainingImages + "\"";

        return false;

    }

    if (exportDirectory.isEmpty() || !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    return true;

}

QString ICAImageAnalysisParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[directory to scan for training images = \"" << directoryToScanForTrainingImages << "\"]";

    if (!directoryToScanForTestingImages.isEmpty())
        stream << Qt::endl << "\t[directory to scan for testing images = \"" << directoryToScanForTestingImages << "\"]";

    stream << Qt::endl << "\t[index of page to read from multi page image = \"" << indexOfPageToReadFromMultiPageImage << "\"]";

    stream << Qt::endl << "\t[window size = " << windowSize << "]";

    stream << Qt::endl << "\t[threshold cumulative explained variance in whitening step = " << threshold_cumulativeExplainedVariance << "]";

    stream << Qt::endl << "\t[threshold maximum number of components in whitening step = " << threshold_maximumNumberOfComponents << "]";

    stream << Qt::endl << "\t[contrast function = " << contrastFunction << "]";

    stream << Qt::endl << "\t[maximum number of iterations = " << maximumNumberOfIterations << "]";

    stream << Qt::endl << "\t[step size (mu) = " << mu << "]";

    stream << Qt::endl << "\t[stopping criteria (epsilon) = " << epsilon << "]";

    stream << Qt::endl << "\t[number of runs to perform = " << numberOfRunsToPerform << "]";

    if (numberOfRunsToPerform > 1) {

        stream << Qt::endl << "\t[pearson correlation threshold for clustering independent components from multiple runs = " << pearsonCorrelationThresholdForConsensus << "]";

        stream << Qt::endl << "\t[threshold for credibility index = " << threshold_credibilityIndex << "]";

        stream << Qt::endl << "\t[xi correlation threshold for filtering consensus independent components = " << xiCorrelationThresholdForConsensus << "]";

    }

    stream << Qt::endl << "\t[maximum size of library = " << maximumLibrarySize << "]";

    stream << Qt::endl << "\t[xi correlation lower threshold for feature matching with library = " << xiCorrelationLowerThresholdForLibrary << "]";

    stream << Qt::endl << "\t[xi correlation upper threshold for feature matching with library = " << xiCorrelationUpperThresholdForLibrary << "]";

    stream << Qt::endl << "\t[number of permutations for null distribution" << numberOfPermutationsForNullDistribution << "]";

    stream << Qt::endl << "\t[reconstruct training images with library = " << ((reconstructTrainingImagesWithLibrary) ? "enabled" : "disabled") << "]";

    stream << Qt::endl << "\t[frequency threshold for library reconstruction" << frequencyThresholdForLibraryReconstruction << "]";

    stream << Qt::endl << "\t[multithreading mode = " << multiThreadMode << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

ICAImageAnalysisWorker::ICAImageAnalysisWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("ICA image analysis"), parameters, "icaimageanalysis")
{

    d_cvMatrixType = {"CV_8UC1", "CV_8SC1", "CV_16UC1", "CV_16SC1", "CV_32SC1", "CV_32FC1", "CV_64FC1", "CV_8UC2", "CV_8SC2", "CV_16UC2", "CV_16SC2", "CV_32SC2", "CV_32FC2", "CV_64FC2", "CV_8UC3", "CV_8SC3", "CV_16UC3", "CV_16SC3", "CV_32SC3", "CV_32FC3", "CV_64FC3", "CV_8UC4", "CV_8SC4", "CV_16UC4", "CV_16SC4", "CV_32SC4", "CV_32FC4", "CV_64FC4"};

    d_cvMatrixDepth = {"CV_8UC", "CV_8SC", "CV_16UC", "CV_16SC", "CV_32SC", "CV_32FC", "CV_64FC"};

    d_maximumAbsoluteValueForDepth << 255.0 << 127 << 65535 << 32767 << 2147483647;

}

void ICAImageAnalysisWorker::doWork_()
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    QUuid uuidProgress = this->startProgress("scanning directory for valid training images", 0, 0);

    QList<QString> pathOfTrainingImages = this->scanDirectoryForValidImages(parameters->directoryToScanForTrainingImages);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (pathOfTrainingImages.isEmpty()) {

        this->reportError("no valid training images detected in directory \"" + parameters->directoryToScanForTrainingImages + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    } else
        this->reportMessage("detected " + QString::number(pathOfTrainingImages.count()) + " valid training images in directory \"" + parameters->directoryToScanForTrainingImages + "\"");


    if (parameters->maximumLibrarySize > (parameters->windowSize * parameters->windowSize * 3)) {

        this->reportWarning("with current window size of " + QString::number(parameters->windowSize) + ", maximum library size can not be bigger than  " + QString::number(parameters->windowSize * parameters->windowSize * 3) + "");

        parameters->maximumLibrarySize = parameters->windowSize * parameters->windowSize * 3;

    }

    uuidProgress = this->startProgress("processing images", 0, pathOfTrainingImages.count());

    this->createPath(parameters->exportDirectory +"/training");

    d_numberOfTrainingImagesUsed = pathOfTrainingImages.count();

    for (qsizetype i = 0; i < pathOfTrainingImages.count(); ++i) {

        d_exportPathForCurrentImage = parameters->exportDirectory +"/training/" + QFileInfo(pathOfTrainingImages.at(i)).baseName();

        d_suffixForCurrentImage = QFileInfo(pathOfTrainingImages.at(i)).suffix();

        this->createPath(d_exportPathForCurrentImage);


        // read image from file
        ////////////////

        cv::Mat currentImage;

        if (!this->readImage(pathOfTrainingImages.at(i), currentImage, parameters->indexOfPageToReadFromMultiPageImage)) {

            QThread::currentThread()->requestInterruption();

            return;

        }

        if (parameters->selectedResultItems.contains("image - original"))
            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/original." + d_suffixForCurrentImage.toStdString(), currentImage);

        d_nChannelsForCurrentImage = currentImage.channels();

        d_depthForCurrentImage = currentImage.depth();

        ////////////////


        // Subdivide image in windows
        ////////////////

        d_currentImageWindows = this->splitImageInWindows(currentImage, parameters->windowSize);

        d_nImageWindowsForCurrentImage = d_currentImageWindows.count();

        ////////////////

        ////////////////

        // Obtaining fourier transform of image blocks
        ////////////////

        d_nFeaturesForCurrentImage = static_cast<qsizetype>(currentImage.channels()) * static_cast<qsizetype>(parameters->windowSize) * static_cast<qsizetype>(parameters->windowSize);

        std::vector<std::complex<double>> fourierTransformedVectorOfImageWindows = this->convertImageWindowsToFourierTransformedVectors(d_currentImageWindows);

        ////////////////

        // Whitening
        ////////////////

        QList<std::complex<double>> whiteningMatrix;

        QList<std::complex<double>> dewhiteningMatrix;

        qsizetype numberOfValidComponents;

        QList<std::complex<double>> whitenedFourierTransformedVectorOfImageWindows = this->whitening(fourierTransformedVectorOfImageWindows, dewhiteningMatrix, whiteningMatrix, numberOfValidComponents);

        if (whitenedFourierTransformedVectorOfImageWindows.isEmpty()) {

            QThread::currentThread()->requestInterruption();

            return;

        }

        ////////////////

        // Non-circular complex fastICA
        ////////////////

        int contrastFunction;

        if (parameters->contrastFunction == "Log")
            contrastFunction = NC_C_FICA_LOG;
        else if (parameters->contrastFunction == "Kurtosis")
            contrastFunction = NC_C_FICA_KURT;
        else if (parameters->contrastFunction == "Sqrt")
            contrastFunction = NC_C_FICA_SQRT;
        else
            contrastFunction = NC_C_FICA_LOG;

        QList<std::complex<double>> consensusIndependentComponents;

        if (parameters->numberOfRunsToPerform == 1) {

            consensusIndependentComponents = this->performICARun(-1, whitenedFourierTransformedVectorOfImageWindows, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, parameters->numberOfThreadsToUse);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

        } else {

            QList<std::complex<double>> independentComponentsForAllRuns;

            if (parameters->multiThreadMode == "perform runs simultaneously") {

                QMutex mutex;

                QUuid uuidProgress_1 = this->startProgress("performing ICA runs", 1, parameters->numberOfRunsToPerform);

                auto performICARuns = [&](const std::pair<qsizetype, qsizetype> &block, qsizetype numberOfThreadsToUse) {

                    for (qsizetype i = block.first; i < block.second; ++i) {

                        if (QThread::currentThread()->isInterruptionRequested())
                            return;

                        this->checkIfPauseWasRequested();

                        QList<std::complex<double>> independentComponents = this->performICARun(i + 1, whitenedFourierTransformedVectorOfImageWindows, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, numberOfThreadsToUse);

                        if (this->thread()->isInterruptionRequested())
                            return;

                        this->checkIfPauseWasRequested();

                        mutex.lock();

                        independentComponentsForAllRuns.append(independentComponents);

                        mutex.unlock();

                        this->updateProgressWithOne(uuidProgress_1);

                    }

                };

                QFutureSynchronizer<void> futureSynchronizer;

                for (const std::pair<qsizetype, qsizetype> &block : MathUtilityFunctions::createIndexBlocksForMultiThreading(parameters->numberOfRunsToPerform, parameters->numberOfThreadsToUse))
                    futureSynchronizer.addFuture(QtConcurrent::run(performICARuns, block, 1));

                futureSynchronizer.waitForFinished();

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->stopProgress(uuidProgress_1);

                MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

            } else {

                QUuid uuidProgress_1 = this->startProgress("performing ICA runs", 0, parameters->numberOfRunsToPerform);

                for (qsizetype i = 0; i < parameters->numberOfRunsToPerform; ++i) {

                    independentComponentsForAllRuns.append(this->performICARun(i + 1, whitenedFourierTransformedVectorOfImageWindows, whiteningMatrix, dewhiteningMatrix, numberOfValidComponents, contrastFunction, parameters->numberOfThreadsToUse));

                    if (QThread::currentThread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    this->updateProgress(uuidProgress_1, i + 1);

                }

                this->stopProgress(uuidProgress_1);

            }

            consensusIndependentComponents = this->calculateConsensusComponents(independentComponentsForAllRuns, numberOfValidComponents);

            if (parameters->selectedResultItems.contains("image - NC cICA: mixing matrix (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

                this->calculateConsensusMixingMatrix(consensusIndependentComponents, numberOfValidComponents, whitenedFourierTransformedVectorOfImageWindows, dewhiteningMatrix);

                if (parameters->numberOfPermutationsForNullDistribution != 0) {

                    QList<std::complex<double>> transposedConsensusIndependentComponents = MathOperations::transpose(consensusIndependentComponents, d_nFeaturesForCurrentImage);

                    QList<std::span<std::complex<double>>> spansOfTransposedConsensusIndependentComponents = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(transposedConsensusIndependentComponents, numberOfValidComponents);

                    std::random_device rd;

                    std::mt19937 g(rd());

                    QUuid uuidProgress_1 = this->startProgress("performing permutations", 0, parameters->numberOfPermutationsForNullDistribution);

                    for (qsizetype permutationRound = 0; permutationRound < parameters->numberOfPermutationsForNullDistribution; ++permutationRound) {

                        std::shuffle(spansOfTransposedConsensusIndependentComponents.begin(), spansOfTransposedConsensusIndependentComponents.end(), g);

                        QList<std::complex<double>> shuffledConsensusIndependentComponents;

                        shuffledConsensusIndependentComponents.reserve(consensusIndependentComponents.count());

                        for (const std::span<std::complex<double>> &span : spansOfTransposedConsensusIndependentComponents) {

                            for (const std::complex<double> &value : span)
                                shuffledConsensusIndependentComponents.append(value);

                        }

                        MathOperations::transposeInplace(shuffledConsensusIndependentComponents, numberOfValidComponents);

                        this->calculateConsensusMixingMatrix_shuffled(permutationRound + 1, shuffledConsensusIndependentComponents, numberOfValidComponents, whitenedFourierTransformedVectorOfImageWindows, dewhiteningMatrix);

                        if (QThread::currentThread()->isInterruptionRequested())
                            return;

                        this->checkIfPauseWasRequested();

                        this->updateProgress(uuidProgress_1, permutationRound + 1);

                    }

                    this->stopProgress(uuidProgress_1);

                }

            }

        }

        this->updateLibrary(consensusIndependentComponents);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    this->exportLibrary();

    if (parameters->selectedResultItems.contains("whitening summary for all training images")) {

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < pathOfTrainingImages.count(); ++j)
            rowLabels << QString("image_") + QString::number(j + 1);

        QList<QString> columnLabels;

        columnLabels << "n components" << "explained variance";

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_explainedVariance_whitening));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory + "/training", "whitening_summary_for_all_training_images");

    }

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < pathOfTrainingImages.count(); ++j)
            rowLabels << QString("image_") + QString::number(j + 1);

        QList<QString> columnLabels;

        columnLabels << "MSSIM_real";

        for (qsizetype j = 0; j < parameters->numberOfPermutationsForNullDistribution; ++j)
            columnLabels << "MSSIM_permutation_" + QString::number(j + 1);

        AnnotatedMatrix<double> *result1 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageMode));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix1(result1);

        this->exportToFile(sharedPointerToAnnotatedMatrix1, parameters->exportDirectory + "/training", "mssim_imageMode");


        AnnotatedMatrix<double> *result2 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageWindowMode));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix2(result2);

        this->exportToFile(sharedPointerToAnnotatedMatrix2, parameters->exportDirectory + "/training", "mssim_imageWindowMode");

    }

    if (parameters->reconstructTrainingImagesWithLibrary) {

        d_ssimMetric_ReconstructedImageWindowMode.clear();

        d_ssimMetric_ReconstructedImageMode.clear();

        this->applyLibraryToImages(pathOfTrainingImages, "libraryAppliedToTrainingImages");

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < pathOfTrainingImages.count(); ++j)
                rowLabels << QString("image_") + QString::number(j + 1);

            QList<QString> columnLabels;

            columnLabels << "MSSIM_real";

            for (qsizetype j = 0; j < parameters->numberOfPermutationsForNullDistribution; ++j)
                columnLabels << "MSSIM_permutation_" + QString::number(j + 1);

            AnnotatedMatrix<double> *result1 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageMode));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix1(result1);

            this->exportToFile(sharedPointerToAnnotatedMatrix1, parameters->exportDirectory + "/libraryAppliedToTrainingImages", "mssim_imageMode");


            AnnotatedMatrix<double> *result2 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageWindowMode));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix2(result2);

            this->exportToFile(sharedPointerToAnnotatedMatrix2, parameters->exportDirectory + "/libraryAppliedToTrainingImages", "mssim_imageWindowMode");

        }

    }

    if (!parameters->directoryToScanForTestingImages.isEmpty()) {

        QUuid uuidProgress = this->startProgress("scanning directory for valid testing images", 0, 0);

        QList<QString> pathOfTestingImages = this->scanDirectoryForValidImages(parameters->directoryToScanForTestingImages);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        if (pathOfTestingImages.isEmpty()) {

            this->reportError("no valid testing images detected in directory \"" + parameters->directoryToScanForTestingImages + "\"");

            QThread::currentThread()->requestInterruption();

            return;

        } else
            this->reportMessage("detected " + QString::number(pathOfTestingImages.count()) + " valid testing images in directory \"" + parameters->directoryToScanForTestingImages + "\"");

        d_ssimMetric_ReconstructedImageWindowMode.clear();

        d_ssimMetric_ReconstructedImageMode.clear();

        this->applyLibraryToImages(pathOfTestingImages, "libraryAppliedToTestingImages");

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < pathOfTestingImages.count(); ++j)
                rowLabels << QString("image_") + QString::number(j + 1);

            QList<QString> columnLabels;

            columnLabels << "MSSIM_real";

            for (qsizetype j = 0; j < parameters->numberOfPermutationsForNullDistribution; ++j)
                columnLabels << "MSSIM_permutation_" + QString::number(j + 1);

            AnnotatedMatrix<double> *result1 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageMode));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix1(result1);

            this->exportToFile(sharedPointerToAnnotatedMatrix1, parameters->exportDirectory + "/libraryAppliedToTestingImages", "mssim_imageMode");


            AnnotatedMatrix<double> *result2 = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(d_ssimMetric_ReconstructedImageWindowMode));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix2(result2);

            this->exportToFile(sharedPointerToAnnotatedMatrix2, parameters->exportDirectory + "/libraryAppliedToTestingImages", "mssim_imageWindowMode");

        }

    }

}

void ICAImageAnalysisWorker::applyLibraryToImages(const QList<QString> &pathOfImages, const QString &outputDir)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    ////////////////

    QUuid uuidProgress = this->startProgress("apply library to images : calculate pseudo inverse of library", 0, 0);

    QList<std::complex<double>> libraryConsensusIndependentComponents;

    qsizetype numberOfValidLibraryComponents = 0;

    for (const std::pair<qsizetype, QList<std::complex<double>>> &feature : d_library) {

        double freq = static_cast<double>(feature.first) / static_cast<double>(d_numberOfTrainingImagesUsed);

        if (freq >= parameters->frequencyThresholdForLibraryReconstruction) {

            ++numberOfValidLibraryComponents;

            libraryConsensusIndependentComponents.append(feature.second);

        }

    }

    this->reportWarning("reconstruction with library is done with top " + QString::number(numberOfValidLibraryComponents) + " features based on frequency threshold " +  QString::number(parameters->frequencyThresholdForLibraryReconstruction));

    int info;

    double error = 0.0;

    QList<std::complex<double>> pseudoInverselibraryConsensusIndependentComponents = MathOperations::pseudoInverse(libraryConsensusIndependentComponents, d_nFeaturesForCurrentImage, true, error, parameters->numberOfThreadsToUse, &info);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (info < 0) {

        this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");

        return;

    } else if (info > 0) {

        this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

        return;

    }

    this->stopProgress(uuidProgress);

    if (1) {

        this->reportMessage("total error pseudo-inverse (apply library \"" + outputDir + "\"): " + QString::number(static_cast<double>(error)));

    }

    uuidProgress = this->startProgress("processing images", 0, pathOfImages.count());

    this->createPath(parameters->exportDirectory +"/" + outputDir);

    for (qsizetype i = 0; i < pathOfImages.count(); ++i) {

//        d_exportPathForCurrentImage = parameters->exportDirectory +"/" + outputDir + QString("/image_") + QString::number(i + 1);

        d_exportPathForCurrentImage = parameters->exportDirectory + "/" + outputDir + "/" + QFileInfo(pathOfImages.at(i)).baseName();

        d_suffixForCurrentImage = QFileInfo(pathOfImages.at(i)).suffix();

        this->createPath(d_exportPathForCurrentImage);


        // read image from file
        ////////////////

        cv::Mat currentImage;

        if (!this->readImage(pathOfImages.at(i), currentImage, parameters->indexOfPageToReadFromMultiPageImage)) {

            QThread::currentThread()->requestInterruption();

            return;

        }

        d_nChannelsForCurrentImage = currentImage.channels();

        d_depthForCurrentImage = currentImage.depth();

        ////////////////


        // Subdivide image in windows
        ////////////////

        d_currentImageWindows = this->splitImageInWindows(currentImage, parameters->windowSize);

        d_nImageWindowsForCurrentImage = d_currentImageWindows.count();

        ////////////////


        // Obtaining fourier transform of image blocks
        ////////////////

        d_nFeaturesForCurrentImage = static_cast<qsizetype>(currentImage.channels()) * static_cast<qsizetype>(parameters->windowSize) * static_cast<qsizetype>(parameters->windowSize);

        std::vector<std::complex<double>> fourierTransformedVectorOfImageWindows = this->convertImageWindowsToFourierTransformedVectors(d_currentImageWindows);

        if (parameters->selectedResultItems.contains("image - NC cICA: mixing matrix (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

            this->calculateConsensusMixingMatrix_applyLibraryMode(pseudoInverselibraryConsensusIndependentComponents, libraryConsensusIndependentComponents, numberOfValidLibraryComponents, fourierTransformedVectorOfImageWindows);

            if (parameters->numberOfPermutationsForNullDistribution != 0) {

                QList<std::complex<double>> transposedLibraryConsensusIndependentComponents = MathOperations::transpose(libraryConsensusIndependentComponents, d_nFeaturesForCurrentImage);

                QList<std::span<std::complex<double>>> spansOfTransposedLibraryConsensusIndependentComponents = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(transposedLibraryConsensusIndependentComponents, numberOfValidLibraryComponents);

                std::random_device rd;

                std::mt19937 g(rd());

                QUuid uuidProgress_1 = this->startProgress("performing permutations", 0, parameters->numberOfPermutationsForNullDistribution);

                for (qsizetype permutationRound = 0; permutationRound < parameters->numberOfPermutationsForNullDistribution; ++permutationRound) {

                    std::shuffle(spansOfTransposedLibraryConsensusIndependentComponents.begin(), spansOfTransposedLibraryConsensusIndependentComponents.end(), g);

                    QList<std::complex<double>> shuffledLibraryConsensusIndependentComponents;

                    shuffledLibraryConsensusIndependentComponents.reserve(libraryConsensusIndependentComponents.count());

                    for (const std::span<std::complex<double>> &span : spansOfTransposedLibraryConsensusIndependentComponents) {

                        for (const std::complex<double> &value : span)
                            shuffledLibraryConsensusIndependentComponents.append(value);

                    }

                    MathOperations::transposeInplace(shuffledLibraryConsensusIndependentComponents, numberOfValidLibraryComponents);

                    this->calculateConsensusMixingMatrix_shuffled_applyLibraryMode(permutationRound + 1, shuffledLibraryConsensusIndependentComponents, numberOfValidLibraryComponents, fourierTransformedVectorOfImageWindows);

                    if (QThread::currentThread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    this->updateProgress(uuidProgress_1, permutationRound + 1);

                }

                this->stopProgress(uuidProgress_1);

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

}

bool ICAImageAnalysisWorker::performReadWriteLockLogistics_()
{

    return true;

}

void ICAImageAnalysisWorker::performReadWriteUnlockLogistics_()
{

}

QList<QString> ICAImageAnalysisWorker::scanDirectoryForValidImages(const QString &directory)
{

    this->reportMessage("scanning directory \"" + directory + "\" for valid images");

    QDir dir(directory);

    QFileInfoList fileInfoList = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDir::Unsorted);

    QStringList pathsToValidImages;

    for (qsizetype i = 0; i < fileInfoList.count(); ++i) {

        if (fileInfoList.at(i).isDir())
            pathsToValidImages << this->scanDirectoryForValidImages(fileInfoList.at(i).absoluteFilePath());
        else {

            const QString &currentFile(fileInfoList.at(i).absoluteFilePath());

            if (cv::haveImageReader(currentFile.toStdString()))
                pathsToValidImages.append(currentFile);

        }

    }

    return pathsToValidImages;

}

bool ICAImageAnalysisWorker::readImage(const QString &pathToImage, cv::Mat &image, int indexOfPageToReadFromMultiPageImage)
{

    int imageCount = cv::imcount(pathToImage.toStdString());

    if (imageCount == 1)
        image = cv::imread(pathToImage.toStdString(), cv::IMREAD_COLOR);
    else {

        if (indexOfPageToReadFromMultiPageImage >= imageCount) {

            this->reportError("could not read page with index " + QString::number(indexOfPageToReadFromMultiPageImage) + " from multipage image \"" + pathToImage + "\"");

            return false;

        }

        std::vector<cv::Mat> mats;

        if (!cv::imreadmulti(pathToImage.toStdString(), mats, indexOfPageToReadFromMultiPageImage, 1, cv::IMREAD_UNCHANGED)) {

            this->reportError("could not read image \"" + pathToImage + "\"");

            return false;

        }

        image = mats.at(0);

    }

    if (image.empty())
        return false;

    if (imageCount == 1)
        this->reportMessage("image = \"" + pathToImage + "\", depth = " + d_cvMatrixDepth.at(image.depth())  + ", channels = " + QString::number(image.channels()) + ", rows = " + QString::number(image.rows) + ", columns = " + QString::number(image.cols));
    else
        this->reportMessage("image = \"" + pathToImage + "\", page index = " + QString::number(indexOfPageToReadFromMultiPageImage) + ", depth = " + d_cvMatrixDepth.at(image.depth()) + ", channels = " + QString::number(image.channels()) + ", rows = " + QString::number(image.rows) + ", columns = " + QString::number(image.cols));

    return true;

}

void ICAImageAnalysisWorker::createPath(const QString &path)
{

    if (!QDir(path).exists())
        QDir().mkpath(path);

}

QList<cv::Mat> ICAImageAnalysisWorker::splitImageInWindows(const cv::Mat &currentImage, int windowSize)
{

    QList<cv::Mat> imageWindows;

    std::div_t divRows = std::div(currentImage.rows, windowSize);

    std::div_t divCols = std::div(currentImage.cols, windowSize);

    double horizontalBorder = 0;

    if (divRows.rem != 0)
        horizontalBorder = (windowSize - divRows.rem) / 2.0;

    double verticalBorder = 0;

    if (divCols.rem != 0)
        verticalBorder = (windowSize - divCols.rem) / 2.0;

    cv::copyMakeBorder(currentImage, d_currentPaddedImage, std::floor(horizontalBorder), std::ceil(horizontalBorder), std::floor(verticalBorder), std::ceil(verticalBorder), cv::BORDER_CONSTANT, cv::Scalar(255,255,255));

    d_numberOfRowWindowsInCurrentImage = std::div(d_currentPaddedImage.rows, windowSize).quot;

    d_numberOfColumnWindowsInCurrentImage = std::div(d_currentPaddedImage.cols, windowSize).quot;

    QUuid uuidProgress = this->startProgress("splitting image in windows", 0, d_numberOfRowWindowsInCurrentImage * d_numberOfColumnWindowsInCurrentImage);

    for (int x = 0; x < d_currentPaddedImage.rows; x += d_currentPaddedImage.rows / d_numberOfRowWindowsInCurrentImage) {

        for (int y = 0; y < d_currentPaddedImage.cols; y += d_currentPaddedImage.cols / d_numberOfColumnWindowsInCurrentImage) {

            imageWindows.push_back(d_currentPaddedImage(cv::Rect(y, x, (d_currentPaddedImage.cols / d_numberOfColumnWindowsInCurrentImage), (d_currentPaddedImage.rows / d_numberOfRowWindowsInCurrentImage))).clone());

            if (QThread::currentThread()->isInterruptionRequested())
                return imageWindows;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

    }

    this->stopProgress(uuidProgress);

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    if (parameters->selectedResultItems.contains("image - original"))
        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/original_padded." + d_suffixForCurrentImage.toStdString(), d_currentPaddedImage);

    if (parameters->selectedResultItems.contains("image - image windows")) {

        QUuid uuidProgress = this->startProgress("exporting image windows", 0, imageWindows.count());

        this->createPath(d_exportPathForCurrentImage + "/imageWindows");

        for (qsizetype i = 0; i < imageWindows.count(); ++i) {

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/imageWindows/window_" + QString::number(i + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), imageWindows.at(i));

            if (QThread::currentThread()->isInterruptionRequested())
                return imageWindows;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->stopProgress(uuidProgress);

        cv::Mat reconstructedImage = this->createOne(imageWindows, d_numberOfColumnWindowsInCurrentImage, 1);

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/dividedImage." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

    }

    return imageWindows;

}

std::vector<std::complex<double> > ICAImageAnalysisWorker::convertImageWindowsToFourierTransformedVectors(const QList<cv::Mat> &imageWindows)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    QList<double> vectorOfImageWindows;

    std::vector<std::complex<double>> fourierTransformedVectorOfImageWindows;

    int depth = imageWindows.first().depth();

    QUuid uuidProgress = this->startProgress("converting image windows to fourier transformed vectors", 0, imageWindows.count());

    for (qsizetype i = 0; i < imageWindows.count(); ++i) {

        std::vector<cv::Mat> channels;

        cv::split(imageWindows.at(i), channels);

        for (unsigned int j = 0; j < channels.size(); ++j) {

            cv::Mat fChannel;

            channels.at(j).convertTo(fChannel, CV_64F, 1.0 / d_maximumAbsoluteValueForDepth.at(depth));

            if (parameters->selectedResultItems.contains("image - vector of image windows")) {

                for (int k = 0; k < fChannel.rows; ++k) {

                    for (int l = 0; l < fChannel.cols; ++l)
                        vectorOfImageWindows.append(fChannel.at<double>(k, l));

                }

            }

            cv::Mat fourierTransformOfChannel;            

            cv::dft(fChannel, fourierTransformOfChannel, cv::DFT_COMPLEX_OUTPUT);

            for (int k = 0; k < fourierTransformOfChannel.rows; ++k) {

                for (int l = 0; l < fourierTransformOfChannel.cols; ++l) {

                    const cv::Vec2d &vec(fourierTransformOfChannel.at<cv::Vec2d>(k, l));

                    fourierTransformedVectorOfImageWindows.push_back(std::complex<double>(vec[0], vec[1]));

                }

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return std::vector<std::complex<double>>();

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - vector of image windows")) {

        MathOperations::transposeInplace(vectorOfImageWindows, d_nFeaturesForCurrentImage, parameters->numberOfThreadsToUse);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nFeaturesForCurrentImage; ++j)
            rowLabels << "feature_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < imageWindows.count(); ++j)
            columnLabels << "imageWindow_" + QString::number(j + 1);

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(vectorOfImageWindows));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "vectorOfImageWindows");

    }

    if (parameters->selectedResultItems.contains("image - fourier transformeded vector of image windows")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(fourierTransformedVectorOfImageWindows.size());

        QUuid uuidProgress = this->startProgress("converting fourier transformed vector of image windows to string representation", 0, fourierTransformedVectorOfImageWindows.size());

        for (const std::complex<double> &complex : fourierTransformedVectorOfImageWindows) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return std::vector<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        MathOperations::transposeInplace(stringRepresentation, d_nFeaturesForCurrentImage, parameters->numberOfThreadsToUse);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nFeaturesForCurrentImage; ++j)
            rowLabels << "feature_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < imageWindows.count(); ++j)
            columnLabels << "imageWindow_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "fourierTransformedVectorOfImageWindows");

    }

    return fourierTransformedVectorOfImageWindows;

}

QList<cv::Mat> ICAImageAnalysisWorker::convertFourierTransformVectorsToImageWindows(const QList<std::complex<double>> &fourierTransformedVectorOfImageWindows, bool normalize)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    qsizetype numberOfImageWindows = fourierTransformedVectorOfImageWindows.count() / (d_nChannelsForCurrentImage * parameters->windowSize * parameters->windowSize);

    QList<cv::Mat> imageBlocks;

    std::complex<double> *it = const_cast<std::complex<double> *>(fourierTransformedVectorOfImageWindows.data());

    for (qsizetype i = 0; i < numberOfImageWindows; ++i) {

        std::vector<cv::Mat> inverseChannelImages;

        for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

            std::span<std::complex<double>> complexChannelVector(it, parameters->windowSize * parameters->windowSize);

            QList<double> data;

            data.reserve(parameters->windowSize * parameters->windowSize * 2);

            for (const std::complex<double> &complex : complexChannelVector)
                data << complex.real() << complex.imag();

            cv::Mat complexChannelImage(parameters->windowSize, parameters->windowSize, CV_64FC2, data.data());

            cv::Mat inverseChannelImage;

            cv::dft(complexChannelImage, inverseChannelImage, cv::DFT_SCALE|cv::DFT_INVERSE|cv::DFT_REAL_OUTPUT);

            inverseChannelImage.convertTo(inverseChannelImage, d_depthForCurrentImage, d_maximumAbsoluteValueForDepth.at(d_depthForCurrentImage));

            if (normalize)
                inverseChannelImage.convertTo(inverseChannelImage, -1, 5, 1);

            inverseChannelImages.push_back(inverseChannelImage);

            std::advance(it, parameters->windowSize * parameters->windowSize);

        }

        cv::Mat imageWindow;

        cv::merge(inverseChannelImages, imageWindow);

        imageBlocks.push_back(imageWindow);

    }

    return imageBlocks;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateCovarianceMatrix(std::vector<std::complex<double>> &matrix)
{

    this->startProgress("whitening step : center variables", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, d_nFeaturesForCurrentImage), [](std::span<std::complex<double>> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    QUuid uuidProgress = this->startProgress("whitening step : calculate covariance matrix", 0, 0);

    QList<std::complex<double>> covarianceMatrix(d_nImageWindowsForCurrentImage * d_nImageWindowsForCurrentImage, 0.0);

    std::complex<double> alpha = 1.0 / (static_cast<double>(d_nFeaturesForCurrentImage) - 1.0);

    std::complex<double> beta = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, d_nImageWindowsForCurrentImage, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, &alpha, matrix.data(), d_nFeaturesForCurrentImage, matrix.data(), d_nFeaturesForCurrentImage, &beta, covarianceMatrix.data(), d_nImageWindowsForCurrentImage);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    return covarianceMatrix;

}

QList<std::complex<double> > ICAImageAnalysisWorker::whitening(std::vector<std::complex<double>> &fourierTransformedVectorOfImageWindows, QList<std::complex<double>> &dewhiteningMatrix, QList<std::complex<double>> &whiteningMatrix, qsizetype &numberOfValidComponents)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    // Obtaining covariance matrix
    ////////////////

    QList<std::complex<double>> covarianceMatrix = this->calculateCovarianceMatrix(fourierTransformedVectorOfImageWindows);

    if (parameters->selectedResultItems.contains("image - whitening: covariance matrix")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(covarianceMatrix.count());

        QUuid uuidProgress = this->startProgress("converting covariance matrix to string representation", 0, covarianceMatrix.count());

        for (const std::complex<double> &complex : covarianceMatrix) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> labels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            labels << "block_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(labels, labels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_covariance_matrix");

    }


    ////////////////

    // Obtaining eigenvalues and eigenvectors
    ////////////////

    QUuid uuidProgress = this->startProgress("whitening step : calculate eigenvalues and eigenvectors", 0, 0);

    qsizetype m;

    QList<double> w(d_nImageWindowsForCurrentImage, 0.0);

    QList<std::complex<double>> z(d_nImageWindowsForCurrentImage * d_nImageWindowsForCurrentImage, 0.0);

    QList<long long> isuppz(d_nImageWindowsForCurrentImage * 2, 0.0);

    int info = LAPACKE_zheevr(LAPACK_ROW_MAJOR, 'V', 'A', 'U', d_nImageWindowsForCurrentImage, covarianceMatrix.data(), d_nImageWindowsForCurrentImage, 0.0, 1.0, 1, d_nImageWindowsForCurrentImage, LAPACKE_dlamch('S'), &m, w.data(), z.data(), d_nImageWindowsForCurrentImage, isuppz.data());

    if (info < 0)
        this->reportError("ZHEEVR: " + QString::number(info) + "th parameter had illegal value");
    else if (info > 0)
        this->reportError("ZHEEVR: Error code -> " + QString::number(info));

    if (QThread::currentThread()->isInterruptionRequested())
        QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    // covariance matrix not needed anymore
    covarianceMatrix.clear();

    std::reverse(w.begin(), w.end());

    this->startProgress("whitening step : reversing eigenvectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, d_nImageWindowsForCurrentImage), [](std::span<std::complex<double>> &span){std::reverse(span.begin(), span.end());}));

    if (QThread::currentThread()->isInterruptionRequested())
        QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    uuidProgress = this->startProgress("whitening step : calculate explained variance", 0, 0);

    long double sumOfEigenValues = 0.0;

    for (long double value : w)
        sumOfEigenValues += std::abs(value);

    QList<double> eigenvaluesSummary;

    long double sumExplainedVariance = 0.0;

    long double totalExplainedVariance = 0.0;

    QList<QString> componentLabels;

    numberOfValidComponents = 0;

    for (qsizetype i = 0; i < d_nImageWindowsForCurrentImage; ++i) {

        const long double eigenvalue = w.at(i);

        long double explainedVariance = std::fabs(eigenvalue) / sumOfEigenValues;

        sumExplainedVariance += explainedVariance;

        if ((eigenvalue > std::numeric_limits<float>::epsilon()) && ((sumExplainedVariance * 100.0) <= parameters->threshold_cumulativeExplainedVariance) && (numberOfValidComponents < parameters->threshold_maximumNumberOfComponents)) {

            ++numberOfValidComponents;

            totalExplainedVariance += explainedVariance;

            componentLabels.append("component " + QString::number(i + 1));

            eigenvaluesSummary << eigenvalue << explainedVariance * 100.0 << sumExplainedVariance * 100.0;

        }

    }

    this->stopProgress(uuidProgress);

    if (numberOfValidComponents == 0) {

        emit this->reportError("the number of components remaining after whitening the data is to low to start ICA");

        QThread::currentThread()->requestInterruption();

        return QList<std::complex<double> >();

    }

    ////////////////


    // Filtering eigenvalues and eigenvectors that do not meet the thresholds
    ////////////////

    uuidProgress = this->startProgress("whitening step : filtering eigenvalues and eigenvectors that do not meet the thresholds", 0, 0);

    w.resize(numberOfValidComponents);

    MathOperations::transposeInplace(z, d_nImageWindowsForCurrentImage, parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    z.resize(numberOfValidComponents * d_nImageWindowsForCurrentImage);

    MathOperations::transposeInplace(z, d_nImageWindowsForCurrentImage, parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - whitening: eigenvalues")) {

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(componentLabels, {"eigenvalues", "explained variance", "cumulative explained variance"}, std::move(eigenvaluesSummary));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_eigenvalues");

    }

    if (parameters->selectedResultItems.contains("whitening summary for all training images")) {

        d_explainedVariance_whitening.append(numberOfValidComponents);

        d_explainedVariance_whitening.append(totalExplainedVariance);

    }

    if (parameters->selectedResultItems.contains("image - whitening: eigenvectors")) {

        QList<QString> stringRepresentationOfEigenvectors;

        stringRepresentationOfEigenvectors.reserve(z.count());

        QUuid uuidProgress = this->startProgress("converting eigenvectors to string representation", 0, z.count());

        for (const std::complex<double> &complex : z) {

            stringRepresentationOfEigenvectors << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            rowLabels << "imageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            columnLabels << "PC_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentationOfEigenvectors));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_eigenvectors");

    }

    if (numberOfValidComponents < parameters->threshold_maximumNumberOfComponents)
        this->reportMessage(QString::number(numberOfValidComponents) + " components used for whitening after removing components that did not meet the threshold of (eigenvalue > " + QString::number(std::numeric_limits<double>::epsilon()) + " AND cumulative explained variance < " + QString::number(static_cast<double>(parameters->threshold_cumulativeExplainedVariance)) + "%)");
    else
        this->reportMessage(QString::number(parameters->threshold_maximumNumberOfComponents) + " components used for whitening");

    ////////////////


    // Creating dewhitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::sqrt(value);});

    dewhiteningMatrix = z;

    this->startProgress("whitening step : creating dewhitening matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(dewhiteningMatrix, numberOfValidComponents), [&w](std::span<std::complex<double>> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), [](const double &a, const std::complex<double> &b){return a * b;});}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    if (parameters->selectedResultItems.contains("image - whitening: dewhitening matrix")) {

        QList<QString> stringRepresentationOfDewhiteningMatrix;

        stringRepresentationOfDewhiteningMatrix.reserve(dewhiteningMatrix.count());

        QUuid uuidProgress = this->startProgress("converting dewhitening matrix to string representation", 0, dewhiteningMatrix.count());

        for (const std::complex<double> &complex : dewhiteningMatrix) {

            stringRepresentationOfDewhiteningMatrix << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            rowLabels << "imageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            columnLabels << "PC_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentationOfDewhiteningMatrix));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_dewhitening_matrix");

    }

    ////////////////


    // Creating whitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::pow(value, -1);});

    whiteningMatrix = z;

    // eigenvectors not needed anymore
    z.clear();

    this->startProgress("whitening step : creating whitening matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(whiteningMatrix, numberOfValidComponents), [&w](std::span<std::complex<double>> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), [](const double &a, const std::complex<double> &b){return a * b;});}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    // eigenvalues not needed anymore
    w.clear();

    uuidProgress = this->startProgress("whitening step : transposing whitening matrix", 0, 0);

    MathOperations::transposeInplace(whiteningMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - whitening: whitening matrix")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(whiteningMatrix.count());

        QUuid uuidProgress = this->startProgress("converting whitening matrix to string representation", 0, whiteningMatrix.count());

        for (const std::complex<double> &complex : whiteningMatrix) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            rowLabels << "PC_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            columnLabels << "imageWindow_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_whitening_matrix");

    }

    ////////////////


    // Creating whitened matrix
    ////////////////

    uuidProgress = this->startProgress("whitening step : creating whitened matrix", 0, 0);

    QList<std::complex<double>> whitenedFourierTransformedVectorOfImageWindows(numberOfValidComponents * d_nFeaturesForCurrentImage, 0.0);

    std::complex<double> alpha = 1.0;

    std::complex<double> beta = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, numberOfValidComponents, d_nFeaturesForCurrentImage, d_nImageWindowsForCurrentImage, &alpha, whiteningMatrix.data(), d_nImageWindowsForCurrentImage, fourierTransformedVectorOfImageWindows.data(), d_nFeaturesForCurrentImage, &beta, whitenedFourierTransformedVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

    // matrix with centered variables not needed anymore
    fourierTransformedVectorOfImageWindows.clear();

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double> >();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - whitening: whitened fourier transformed vector of image windows")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(whitenedFourierTransformedVectorOfImageWindows.count());

        QUuid uuidProgress = this->startProgress("converting whitened fourier transformed vector of image windows to string representation", 0, whitenedFourierTransformedVectorOfImageWindows.count());

        for (const std::complex<double> &complex : whitenedFourierTransformedVectorOfImageWindows) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double> >();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            rowLabels << "whitenedFourierTransformedVectorOfImageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < d_nFeaturesForCurrentImage; ++j)
            columnLabels << "feature_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "image_whitening_whitenedFourierTransformedVectorOfImageWindows");

    }

    ////////////////

    if (parameters->selectedResultItems.contains("image - whitening: whitened image windows")) {

        QList<cv::Mat> whitenedImageWindows = this->convertFourierTransformVectorsToImageWindows(whitenedFourierTransformedVectorOfImageWindows, true);

        this->createPath(d_exportPathForCurrentImage + "/whitenedImageWindows");

        QUuid uuidProgress_1 = this->startProgress("exporting whitened image windows", 0, whitenedImageWindows.count());

        for (qsizetype j = 0; j < whitenedImageWindows.count(); ++j) {

            whitenedImageWindows[j].convertTo(whitenedImageWindows[j], -1, 5, 1);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/whitenedImageWindows/whitenedImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), whitenedImageWindows.at(j));

            if (QThread::currentThread()->isInterruptionRequested())
                return whitenedFourierTransformedVectorOfImageWindows;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress_1, j + 1);

        }

        this->stopProgress(uuidProgress_1);

        qsizetype nColumns = std::ceil(std::sqrt(whitenedImageWindows.count()));

        qsizetype numberToAdd = whitenedImageWindows.count() - (nColumns * nColumns);

        for (qsizetype i = 0; i < numberToAdd; ++i)
            whitenedImageWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, d_depthForCurrentImage, cv::Scalar(0, 0, 0)));

        cv::Mat overviewIndependentImageWindows = this->createOne(whitenedImageWindows, nColumns, 1);

        this->createPath(d_exportPathForCurrentImage + "/whitenedImageWindows");

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/whitenedImageWindows/overviewWhitenedImageWindows." + d_suffixForCurrentImage.toStdString(), overviewIndependentImageWindows);

    }

    // Reconstructing original matrix with whitened fourier transformed vector of image windows (PCA)
    ////////////////

    if (parameters->selectedResultItems.contains("image - whitening: reconstructed image with whitened fourier transformed vector of image windows")) {

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponents, &alpha, dewhiteningMatrix.data(), numberOfValidComponents, whitenedFourierTransformedVectorOfImageWindows.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows);

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        this->createPath(d_exportPathForCurrentImage + "/reconstructedImageWithWhitenedFourierTransformedVectorOfImageWindows");

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithWhitenedFourierTransformedVectorOfImageWindows/reconstructedImage_AllWhitenedImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);


        MathOperations::transposeInplace(dewhiteningMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

        QList<std::span<std::complex<double>>> dewhiteningMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(dewhiteningMatrix, d_nImageWindowsForCurrentImage);

        QList<std::span<std::complex<double>>> whitenedFourierTransformedVectorOfImageWindows_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(whitenedFourierTransformedVectorOfImageWindows, d_nFeaturesForCurrentImage);


        QUuid uuidProgress_1 = this->startProgress("exporting reconstructed images based on whitened image windows", 0, numberOfValidComponents);

        for (qsizetype j = 0; j < numberOfValidComponents; ++j) {

            const std::span<std::complex<double>> &dewhiteningMatrix_rowSpan_j(dewhiteningMatrix_rowSpans.at(j));

            const std::span<std::complex<double>> &whitenedFourierTransformedVectorOfImageWindows_rowSpans_rowSpan_j(whitenedFourierTransformedVectorOfImageWindows_rowSpans.at(j));

            reconstructedFourierTransformVectorOfImageWindows.clear();

            reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

            for (const std::complex<double> &dewhiteningMatrix_rowSpan_j_value : dewhiteningMatrix_rowSpan_j) {

                for (const std::complex<double> &whitenedFourierTransformedVectorOfImageWindows_rowSpan_j_value : whitenedFourierTransformedVectorOfImageWindows_rowSpans_rowSpan_j)
                    reconstructedFourierTransformVectorOfImageWindows.append(dewhiteningMatrix_rowSpan_j_value * whitenedFourierTransformedVectorOfImageWindows_rowSpan_j_value);

            }

            reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows);

            reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithWhitenedFourierTransformedVectorOfImageWindows/reconstructedImage_whitenedImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

            if (QThread::currentThread()->isInterruptionRequested())
                return whitenedFourierTransformedVectorOfImageWindows;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress_1, j + 1);

        }

         this->stopProgress(uuidProgress_1);

        MathOperations::transposeInplace(dewhiteningMatrix, d_nImageWindowsForCurrentImage, parameters->numberOfThreadsToUse);

    }

    ////////////////

    return whitenedFourierTransformedVectorOfImageWindows;

}

cv::Mat ICAImageAnalysisWorker::createOne(QList<cv::Mat> &images, int cols, int min_gap_size, bool normalize)
{

    int max_width = 0;

    int max_height = 0;

    for (qsizetype i = 0; i < images.count(); i++) {

        if (i > 0 && images.at(i).type() != images.at(i - 1).type() ) {

            std::cerr << "WARNING:createOne failed, different types of images";

            return cv::Mat();

        }

        max_height = std::max(max_height, images.at(i).rows);

        max_width = std::max(max_width, images.at(i).cols);

    }

    int rows = std::ceil(static_cast<double>(images.size()) / static_cast<double>(cols));

    cv::Mat result = cv::Mat::zeros(rows * max_height + (rows - 1) * min_gap_size, cols * max_width + (cols - 1) * min_gap_size, images.at(0).type());

    result.setTo(cv::Scalar(255, 255, 255));

    qsizetype i = 0;

    int current_height = 0;

    int current_width = 0;

    for (int y = 0; y < rows; ++y) {

        for (int x = 0; x < cols; ++x) {

            if (i >= images.count())
                return result;

            cv::Mat to(result, cv::Range(current_height, current_height + images.at(i).rows), cv::Range(current_width, current_width + images.at(i).cols));

            images.at(i++).copyTo(to);

            current_width += max_width + min_gap_size;

        }

        current_width = 0;

        current_height += max_height + min_gap_size;

    }

    if (normalize)
        result.convertTo(result, -1, 5, 1);

    return result;

}

QList<std::complex<double>> ICAImageAnalysisWorker::performICARun(qsizetype runNumber, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &whiteningMatrix, const QList<std::complex<double>> &dewhiteningMatrix, qsizetype numberOfValidComponents, int contrastFunction, int numberOfThreadsToUse)
{

    QString insertRunStringPrettyPrint;

    QString insertRunStringFileName;

    if (runNumber != -1)
        insertRunStringPrettyPrint = " run " + QString::number(runNumber) + " ";

    if (runNumber != -1)
        insertRunStringFileName = "_run_" + QString::number(runNumber);

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    ComplexFastICA complexFastICA(whitenedMatrix, whiteningMatrix, dewhiteningMatrix, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponents, contrastFunction, parameters->maximumNumberOfIterations, parameters->mu, parameters->epsilon, numberOfThreadsToUse);

    complexFastICA.connectBaseWorker(this);

    if (runNumber != -1)
        complexFastICA.setLabel("ICA" + insertRunStringPrettyPrint);

    bool succesfullConvergence = false;

    while (!succesfullConvergence) {

        succesfullConvergence = complexFastICA.run();

        if (!succesfullConvergence) {

            complexFastICA.setNumberOfThreadsToUse(parameters->numberOfThreadsToUse);

            if (runNumber != -1)
                complexFastICA.setLabel("rerun after failed convergence of ICA" + insertRunStringPrettyPrint);

        }

        if (this->thread()->isInterruptionRequested())
            return QList<std::complex<double>>();

        this->checkIfPauseWasRequested();

    }

    if (this->thread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    QList<std::complex<double>> independentComponents = complexFastICA.independentComponents();

    ////////////////

    // Exporting independent image windows (per run)
    ////////////////

    if (parameters->selectedResultItems.contains("image - NC cICA: independent image windows (per run)")) {

        QList<cv::Mat> independentImageWindows = this->convertFourierTransformVectorsToImageWindows(independentComponents, true);

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows" + insertRunStringFileName);

        QUuid uuidProgress = this->startProgress("exporting independent image windows", 0, independentImageWindows.count());

        for (qsizetype i = 0; i < independentImageWindows.count(); ++i) {

            independentImageWindows[i].convertTo(independentImageWindows[i], -1, 5, 1);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/independentImageWindows" + insertRunStringFileName.toStdString() + "/independentImageWindow_" + QString::number(i + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), independentImageWindows.at(i));

            if (QThread::currentThread()->isInterruptionRequested())
                return independentComponents;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->stopProgress(uuidProgress);

    }

    ////////////////

    if (parameters->selectedResultItems.contains("image - NC cICA: mixing matrix (per run)")) {

        QList<std::complex<double>> mixingMatrix = complexFastICA.mixingMatrix();

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(mixingMatrix.size());

        QUuid uuidProgress = this->startProgress("converting consensus mixing matrix to string representation", 0, mixingMatrix.size());

        for (const std::complex<double> &complex : mixingMatrix) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return independentComponents;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            rowLabels << "imageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            columnLabels << "independentWindow_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows" + insertRunStringFileName);

        this->exportToFile(sharedPointerToAnnotatedMatrix,d_exportPathForCurrentImage + "/independentImageWindows" + insertRunStringFileName, "mixingMatrix");

    }

    // Reconstructing original matrix with indepenent fourier transformed vector of image windows
    ////////////////

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with independent fourier transformed vector of image windows (per run)")) {

        QList<std::complex<double>> mixingMatrix = complexFastICA.mixingMatrix();

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponents, &alpha, mixingMatrix.data(), numberOfValidComponents, independentComponents.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        this->createPath(d_exportPathForCurrentImage + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows" + insertRunStringFileName);

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows" + insertRunStringFileName.toStdString() + "/reconstructedImage_AllIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);


        MathOperations::transposeInplace(mixingMatrix, numberOfValidComponents, numberOfThreadsToUse);

        QList<std::span<std::complex<double>>> mixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(mixingMatrix, d_nImageWindowsForCurrentImage);

        QList<std::span<std::complex<double>>> independentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(independentComponents, d_nFeaturesForCurrentImage);


        QUuid uuidProgress = this->startProgress("exporting reconstructed images based on independent image windows", 0, numberOfValidComponents);

        for (qsizetype j = 0; j < numberOfValidComponents; ++j) {

            const std::span<std::complex<double>> &mixingMatrix_rowSpans_j(mixingMatrix_rowSpans.at(j));

            const std::span<std::complex<double>> independentComponents_rowSpans_rowSpan_j(independentComponents_rowSpans.at(j));

            reconstructedFourierTransformVectorOfImageWindows.clear();

            reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

            for (const std::complex<double> &mixingMatrix_rowSpans_j_value : mixingMatrix_rowSpans_j) {

                for (const std::complex<double> &independentComponents_rowSpans_rowSpan_j_value : independentComponents_rowSpans_rowSpan_j)
                    reconstructedFourierTransformVectorOfImageWindows.append(mixingMatrix_rowSpans_j_value * independentComponents_rowSpans_rowSpan_j_value);

            }

            reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

            reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows" + insertRunStringFileName.toStdString() + "/reconstructedImage_independentImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

            if (QThread::currentThread()->isInterruptionRequested())
                return independentComponents;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, j + 1);

        }

         this->stopProgress(uuidProgress);

    }

    ////////////////

    return independentComponents;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateConsensusComponents(const QList<std::complex<double>> &independentComponentsForAllRuns, qsizetype numberOfValidComponents)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    QList<std::complex<double>> normalizedIndependentComponentsForAllRuns = independentComponentsForAllRuns;

    normalizedIndependentComponentsForAllRuns.detach();

    this->startProgress("consensus step: center independent components", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(normalizedIndependentComponentsForAllRuns, d_nFeaturesForCurrentImage), [](std::span<std::complex<double>> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->startProgress("consensus step: scaling independent components", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(normalizedIndependentComponentsForAllRuns, d_nFeaturesForCurrentImage), [](std::span<std::complex<double>> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    QUuid uuidProgress = this->startProgress("consensus step : calculate pearson distance matrix", 0, 0);

    qsizetype numberOfIndependentComponentsForAllRuns = independentComponentsForAllRuns.count() / d_nFeaturesForCurrentImage;

    QList<std::complex<double>> pearsonDistanceMatrix(numberOfIndependentComponentsForAllRuns * numberOfIndependentComponentsForAllRuns, 0.0);

    std::complex<double> alpha = 1.0;

    std::complex<double> beta = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, numberOfIndependentComponentsForAllRuns, numberOfIndependentComponentsForAllRuns, d_nFeaturesForCurrentImage, &alpha, normalizedIndependentComponentsForAllRuns.data(), d_nFeaturesForCurrentImage, normalizedIndependentComponentsForAllRuns.data(), d_nFeaturesForCurrentImage, &beta, pearsonDistanceMatrix.data(), numberOfIndependentComponentsForAllRuns);

    normalizedIndependentComponentsForAllRuns.clear();

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    uuidProgress = this->startProgress("consensus step : clustering Pearson distance matrix", 0, 0);

    QList<QList<qsizetype>> consensusClusterToMemberIndexes = this->createConsensusClusterToMemberIndexes(pearsonDistanceMatrix, numberOfIndependentComponentsForAllRuns, parameters->pearsonCorrelationThresholdForConsensus, parameters->numberOfThreadsToUse);

    if (consensusClusterToMemberIndexes.isEmpty()) {

        this->reportMessage("no clusters were detected after clustering pearson distance matrix");

        this->requestInterruption();

        return QList<std::complex<double>>();

    } else
        this->reportMessage("number of cluster detected after clustering pearson distance matrix : " + QString::number(consensusClusterToMemberIndexes.count()));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    auto mapFunctor = [this, &independentComponentsForAllRuns, &numberOfIndependentComponentsForAllRuns, &pearsonDistanceMatrix](const QList<qsizetype> &memberIndexes) {

        QList<qsizetype> sortedMemberIndexes = memberIndexes;

        std::sort(sortedMemberIndexes.begin(), sortedMemberIndexes.end());

        QList<double> sums;

        for (const qsizetype &memberIndex1 : sortedMemberIndexes) {

            double sum = 0.0;

            for (const qsizetype &memberIndex2 : sortedMemberIndexes)
                sum += std::abs(pearsonDistanceMatrix.at(memberIndex1 * numberOfIndependentComponentsForAllRuns + memberIndex2));

            sums.append(sum);

        }

        qsizetype indexOfMax = cblas_idamax(sums.count(), sums.data(), 1);

        return std::pair<double, QList<std::complex<double>>>(sums.at(indexOfMax) / static_cast<double>(sortedMemberIndexes.count()), independentComponentsForAllRuns.sliced(sortedMemberIndexes.at(indexOfMax) * d_nFeaturesForCurrentImage, d_nFeaturesForCurrentImage));

    };

    auto reduceFunctor = [](QList<std::pair<double, QList<std::complex<double>>>> &result, const std::pair<double, QList<std::complex<double>>> &intermediateResult) {

        result.append(intermediateResult);

    };

    QFuture<QList<std::pair<double, QList<std::complex<double>>>>> future = QtConcurrent::mappedReduced(&d_threadPool, consensusClusterToMemberIndexes, std::function<std::pair<double, QList<std::complex<double>>>(const QList<qsizetype> &memberIndexes)>(mapFunctor), std::function<void( QList<std::pair<double, QList<std::complex<double>>>> &, const std::pair<double, QList<std::complex<double>>> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->startProgress("consensus step : get medoid per clusters", future);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    pearsonDistanceMatrix.clear();


    uuidProgress = this->startProgress("consensus step : filtering consensus components based on credibility index", 0, 0);

    qsizetype numberOfValidComponentsAfterFiltering = 0;

    QList<std::complex<double>> consensusIndependentComponents;

    for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

        if (i < numberOfValidComponents) {

            double credibilityIndex = static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(parameters->numberOfRunsToPerform);

            if (credibilityIndex >= parameters->threshold_credibilityIndex) {

                consensusIndependentComponents.append(future.result().at(i).second);

                ++numberOfValidComponentsAfterFiltering;

            } else
                break;

        } else
            break;

    }

    consensusClusterToMemberIndexes.resize(numberOfValidComponentsAfterFiltering);

    this->reportMessage("number of cluster remaining after filtering based on credibility index : " + QString::number(numberOfValidComponentsAfterFiltering));

    this->stopProgress(uuidProgress);


    if (numberOfValidComponentsAfterFiltering == 0) {

        this->reportError("no consensus clusters remaining after filtering based on credibility index");

        this->requestInterruption();

        return QList<std::complex<double>>();

    }

    // Filtering consensus independent image windows based on xi correlation coefficient
    ////////////////

    uuidProgress = this->startProgress("consensus step : filtering consensus components based on dependency (XICOR)", 0, 0);

    QSet<qsizetype> indexPool;

    for (qsizetype i = 0; i < numberOfValidComponentsAfterFiltering; ++i)
        indexPool.insert(i);

    QList<double> xiCorrelationMatrix = this->xiCorrelationSymmetricalMatrix(consensusIndependentComponents, d_nFeaturesForCurrentImage);

    xiCorrelationMatrix.detach();


    if (1) {

        QList<QString> labels;

        for (qsizetype i = 0; i < numberOfValidComponentsAfterFiltering; ++i)
            labels.append("consensus component " + QString::number(i + 1));

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows_consensus");

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(labels, labels, xiCorrelationMatrix);

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/independentImageWindows_consensus", "xiCorrelationMatrixOfConcensusIndenpendentImageWindows");


    }


    for (qsizetype i = 0; i < xiCorrelationMatrix.count();) {

        xiCorrelationMatrix[i] = 0.0;

        i += numberOfValidComponentsAfterFiltering + static_cast<qsizetype>(1);

    }

    QList<std::span<double>> rowSpansOfXiCorrelationMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(xiCorrelationMatrix, numberOfValidComponentsAfterFiltering);


    consensusIndependentComponents.clear();

    QList<QString> consensusIndependentComponentsLabels;


    QList<double> credibilityIndexes;

    numberOfValidComponentsAfterFiltering = 0;

    for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

        if (indexPool.contains(i)) {

            credibilityIndexes.append(static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(parameters->numberOfRunsToPerform));

            consensusIndependentComponents.append(future.result().at(i).second);

            consensusIndependentComponentsLabels.append("cIC_" + QString::number(i + 1));

            ++numberOfValidComponentsAfterFiltering;

        }

        QMutableSetIterator<qsizetype> it(indexPool);

        while (it.hasNext()) {

            if (rowSpansOfXiCorrelationMatrix.at(i)[it.next()] >= parameters->xiCorrelationThresholdForConsensus)
                it.remove();

        }

    }

    this->reportMessage("number of cluster remaining after filtering based on dependency (XICOR) : " + QString::number(numberOfValidComponentsAfterFiltering));

    if (numberOfValidComponentsAfterFiltering == 0) {

        this->reportError("no consensus clusters remaining after filtering based on dependency (XICOR)");

        this->requestInterruption();

        return QList<std::complex<double>>();

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // Exporting consensus independent image windows (consensus)
    ////////////////

    if (parameters->selectedResultItems.contains("image - NC cICA: credibility index of independent image windows (consensus)")) {

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows_consensus");

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(consensusIndependentComponentsLabels, {"Credibility Index"}, std::move(credibilityIndexes));

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/independentImageWindows_consensus", "credibilityIndexOfConcensusIndenpendentImageWindows");

    }

    if (parameters->selectedResultItems.contains("image - NC cICA: independent image windows (consensus)")) {

        QList<cv::Mat> independentImageWindows = this->convertFourierTransformVectorsToImageWindows(consensusIndependentComponents, true);

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows_consensus");

        QUuid uuidProgress = this->startProgress("exporting independent image windows", 0, independentImageWindows.count());

        for (qsizetype i = 0; i < independentImageWindows.count(); ++i) {

            independentImageWindows[i].convertTo(independentImageWindows[i], -1, 5, 1);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/independentImageWindows_consensus/consensusIndependentImageWindow_" + QString::number(i + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), independentImageWindows.at(i));

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double>>();

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->stopProgress(uuidProgress);

        qsizetype nColumns = std::ceil(std::sqrt(independentImageWindows.count()));

        qsizetype numberToAdd = independentImageWindows.count() - (nColumns * nColumns);

        for (qsizetype i = 0; i < numberToAdd; ++i)
            independentImageWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, d_depthForCurrentImage, cv::Scalar(0, 0, 0)));

        cv::Mat overviewIndependentImageWindows = this->createOne(independentImageWindows, nColumns, 1);

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows_consensus");

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/independentImageWindows_consensus/overviewConsensusIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), overviewIndependentImageWindows);

    }


    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    return consensusIndependentComponents;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateConsensusMixingMatrix(const QList<std::complex<double> > &consensusIndependentComponents, qsizetype numberOfValidComponents, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &dewhiteningMatrix)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    // Calculating consensus mixing matrix
    ////////////////

    QUuid uuidProgress = this->startProgress("consensus step : calculate consensus mixing matrix", 0, 0);

    int info;

    double error = 0.0;

    QList<std::complex<double>> pseudoInverseConsensusIndependentComponents = MathOperations::pseudoInverse(consensusIndependentComponents, d_nFeaturesForCurrentImage, true, error, parameters->numberOfThreadsToUse, &info);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    if (info < 0) {

        this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");

        return QList<std::complex<double>>();

    } else if (info > 0) {

        this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

        return QList<std::complex<double>>();

    }

    if (1) {

        this->reportMessage("total error pseudo-inverse (calculate consensus mixing matrix): " + QString::number(static_cast<double>(error)));

    }

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    qsizetype numberOfValidComponentsAfterFiltering = consensusIndependentComponents.count() / d_nFeaturesForCurrentImage;

    QList<std::complex<double>> temp(numberOfValidComponentsAfterFiltering * numberOfValidComponents, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, numberOfValidComponentsAfterFiltering, numberOfValidComponents, d_nFeaturesForCurrentImage, &complex_one, pseudoInverseConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, whitenedMatrix.data(), d_nFeaturesForCurrentImage, &complex_zero, temp.data(), numberOfValidComponents);

    QList<std::complex<double>> consensusMixingMatrix(d_nImageWindowsForCurrentImage * numberOfValidComponentsAfterFiltering, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, d_nImageWindowsForCurrentImage, numberOfValidComponentsAfterFiltering, numberOfValidComponents, &complex_one, dewhiteningMatrix.data(), numberOfValidComponents, temp.data(), numberOfValidComponents, &complex_zero, consensusMixingMatrix.data(), numberOfValidComponentsAfterFiltering);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - NC cICA: mixing matrix (consensus)")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(consensusMixingMatrix.size());

        QUuid uuidProgress_1 = this->startProgress("converting consensus mixing matrix to string representation", 0, consensusMixingMatrix.size());

        for (const std::complex<double> &complex : consensusMixingMatrix) {

            stringRepresentation << "\"" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i\"";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double>>();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        this->stopProgress(uuidProgress_1);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            rowLabels << "imageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < numberOfValidComponentsAfterFiltering; ++j)
            columnLabels << "consensusIndependentWindow_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->createPath(d_exportPathForCurrentImage + "/independentImageWindows_consensus");

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/independentImageWindows_consensus", "consensusMixingMatrix");

    }

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponentsAfterFiltering, &alpha, consensusMixingMatrix.data(), numberOfValidComponentsAfterFiltering, consensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<cv::Mat> reconstructionQualityHeatmapWindows;

            QList<double> ssimMetrics;

            ssimMetrics.reserve(reconstructedImageWindows.count() * (d_nChannelsForCurrentImage + 1));

            QList<double> meanSSIMMetricPerReconstructedImageWindow;

            meanSSIMMetricPerReconstructedImageWindow.reserve(reconstructedImageWindows.count());

            for (qsizetype i = 0; i < reconstructedImageWindows.count(); ++i) {

                cv::Scalar scalar = this->getMSSIM(reconstructedImageWindows.at(i), d_currentImageWindows.at(i));

                double mean = 0.0;

                for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

                    ssimMetrics.append(scalar.val[j]);

                    mean += scalar.val[j];

                }

                mean /= static_cast<double>(d_nChannelsForCurrentImage);

                ssimMetrics.append(mean);

                meanSSIMMetricPerReconstructedImageWindow.append(mean);

                double g = (1.0 - mean) * 255.0;

                double b = (1.0 - mean) * 255.0;

                reconstructionQualityHeatmapWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, CV_8UC3, cv::Scalar(b, g, 255)));

            }

            d_ssimMetric_ReconstructedImageWindowMode.append(boost::math::statistics::median(meanSSIMMetricPerReconstructedImageWindow));

            cv::Mat reconstructionQualityHeatmapImage = this->createOne(reconstructionQualityHeatmapWindows, d_numberOfColumnWindowsInCurrentImage, 0);

            this->createPath(d_exportPathForCurrentImage + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus");

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructionQualityHeatmap." + d_suffixForCurrentImage.toStdString(), reconstructionQualityHeatmapImage);

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < reconstructedImageWindows.count(); ++j)
                rowLabels << "imageWindow_" + QString::number(j + 1);

            QList<QString> columnLabels;

            for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j)
                columnLabels << "channel_" + QString::number(j + 1);

            columnLabels << "mean";

            AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(ssimMetrics));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

            this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus", "reconstructionQualityMetric");

        }

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        cv::Scalar scalar = this->getMSSIM(reconstructedImage, d_currentPaddedImage);

        double mean = 0.0;

        for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

            mean += scalar.val[j];

        }

        mean /= static_cast<double>(d_nChannelsForCurrentImage);

        d_ssimMetric_ReconstructedImageMode.append(mean);

        this->createPath(d_exportPathForCurrentImage + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus");

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructedImage_AllConsensusIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);


        if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

            MathOperations::transposeInplace(consensusMixingMatrix, numberOfValidComponentsAfterFiltering, parameters->numberOfThreadsToUse);

            QList<std::span<std::complex<double>>> mixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(consensusMixingMatrix, d_nImageWindowsForCurrentImage);

            QList<std::span<std::complex<double>>> independentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<std::complex<double>> *>(&consensusIndependentComponents), d_nFeaturesForCurrentImage);

            QUuid uuidProgress_1 = this->startProgress("exporting reconstructed images based on consensus independent image windows", 0, numberOfValidComponentsAfterFiltering);

            for (qsizetype j = 0; j < numberOfValidComponentsAfterFiltering; ++j) {

                const std::span<std::complex<double>> &mixingMatrix_rowSpans_j(mixingMatrix_rowSpans.at(j));

                const std::span<std::complex<double>> independentComponents_rowSpans_rowSpan_j(independentComponents_rowSpans.at(j));

                reconstructedFourierTransformVectorOfImageWindows.clear();

                reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

                for (const std::complex<double> &mixingMatrix_rowSpans_j_value : mixingMatrix_rowSpans_j) {

                    for (const std::complex<double> &independentComponents_rowSpans_rowSpan_j_value : independentComponents_rowSpans_rowSpan_j)
                        reconstructedFourierTransformVectorOfImageWindows.append(mixingMatrix_rowSpans_j_value * independentComponents_rowSpans_rowSpan_j_value);

                }

                reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, true);

                reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

                cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructedImage_consensusIndependentImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

                if (QThread::currentThread()->isInterruptionRequested())
                    return QList<std::complex<double>>();

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress_1, j + 1);

            }

            this->stopProgress(uuidProgress_1);

        }

    }

    return consensusMixingMatrix;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateConsensusMixingMatrix_shuffled(qsizetype shuffleNumber, const QList<std::complex<double> > &consensusIndependentComponents, qsizetype numberOfValidComponents, const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &dewhiteningMatrix)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    // Calculating consensus mixing matrix
    ////////////////

    QUuid uuidProgress = this->startProgress("consensus step : calculate consensus mixing matrix", 0, 0);

    int info;

    double error = 0.0;

    QList<std::complex<double>> pseudoInverseConsensusIndependentComponents = MathOperations::pseudoInverse(consensusIndependentComponents, d_nFeaturesForCurrentImage, true, error, parameters->numberOfThreadsToUse, &info);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    if (info < 0) {

        this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");

        return QList<std::complex<double>>();

    } else if (info > 0) {

        this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

        return QList<std::complex<double>>();

    }

    if (1) {

        this->reportMessage("total error pseudo-inverse (calculate consensus mixing matrix shuffled): " + QString::number(static_cast<double>(error)));

    }

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    qsizetype numberOfValidComponentsAfterFiltering = consensusIndependentComponents.count() / d_nFeaturesForCurrentImage;

    QList<std::complex<double>> temp(numberOfValidComponentsAfterFiltering * numberOfValidComponents, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, numberOfValidComponentsAfterFiltering, numberOfValidComponents, d_nFeaturesForCurrentImage, &complex_one, pseudoInverseConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, whitenedMatrix.data(), d_nFeaturesForCurrentImage, &complex_zero, temp.data(), numberOfValidComponents);

    QList<std::complex<double>> consensusMixingMatrix(d_nImageWindowsForCurrentImage * numberOfValidComponentsAfterFiltering, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, d_nImageWindowsForCurrentImage, numberOfValidComponentsAfterFiltering, numberOfValidComponents, &complex_one, dewhiteningMatrix.data(), numberOfValidComponents, temp.data(), numberOfValidComponents, &complex_zero, consensusMixingMatrix.data(), numberOfValidComponentsAfterFiltering);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponentsAfterFiltering, &alpha, consensusMixingMatrix.data(), numberOfValidComponentsAfterFiltering, consensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<cv::Mat> reconstructionQualityHeatmapWindows;

            QList<double> ssimMetrics;

            ssimMetrics.reserve(reconstructedImageWindows.count() * (d_nChannelsForCurrentImage + 1));

            QList<double> meanSSIMMetricPerReconstructedImageWindow;

            meanSSIMMetricPerReconstructedImageWindow.reserve(reconstructedImageWindows.count());

            for (qsizetype i = 0; i < reconstructedImageWindows.count(); ++i) {

                cv::Scalar scalar = this->getMSSIM(reconstructedImageWindows.at(i), d_currentImageWindows.at(i));

                double mean = 0.0;

                for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

                    ssimMetrics.append(scalar.val[j]);

                    mean += scalar.val[j];

                }

                mean /= static_cast<double>(d_nChannelsForCurrentImage);

                ssimMetrics.append(mean);

                meanSSIMMetricPerReconstructedImageWindow.append(mean);

                double g = (1.0 - mean) * 255.0;

                double b = (1.0 - mean) * 255.0;

                reconstructionQualityHeatmapWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, CV_8UC3, cv::Scalar(b, g, 255)));

            }

            d_ssimMetric_ReconstructedImageWindowMode.append(boost::math::statistics::median(meanSSIMMetricPerReconstructedImageWindow));

            cv::Mat reconstructionQualityHeatmapImage = this->createOne(reconstructionQualityHeatmapWindows, d_numberOfColumnWindowsInCurrentImage, 0);

            this->createPath(d_exportPathForCurrentImage + "/shuffle_" + QString::number(shuffleNumber) + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus");

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructionQualityHeatmap." + d_suffixForCurrentImage.toStdString(), reconstructionQualityHeatmapImage);

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < reconstructedImageWindows.count(); ++j)
                rowLabels << "imageWindow_" + QString::number(j + 1);

            QList<QString> columnLabels;

            for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j)
                columnLabels << "channel_" + QString::number(j + 1);

            columnLabels << "mean";

            AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(ssimMetrics));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

            this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/shuffle_" + QString::number(shuffleNumber) + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus", "reconstructionQualityMetric");

        }

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        cv::Scalar scalar = this->getMSSIM(reconstructedImage, d_currentPaddedImage);

        double mean = 0.0;

        for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

            mean += scalar.val[j];

        }

        mean /= static_cast<double>(d_nChannelsForCurrentImage);

        d_ssimMetric_ReconstructedImageMode.append(mean);

        this->createPath(d_exportPathForCurrentImage + "/shuffle_" + QString::number(shuffleNumber) + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus");

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructedImage_AllConsensusIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

            MathOperations::transposeInplace(consensusMixingMatrix, numberOfValidComponentsAfterFiltering, parameters->numberOfThreadsToUse);

            QList<std::span<std::complex<double>>> mixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(consensusMixingMatrix, d_nImageWindowsForCurrentImage);

            QList<std::span<std::complex<double>>> independentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<std::complex<double>> *>(&consensusIndependentComponents), d_nFeaturesForCurrentImage);

            QUuid uuidProgress_1 = this->startProgress("exporting reconstructed images based on consensus independent image windows", 0, numberOfValidComponentsAfterFiltering);

            for (qsizetype j = 0; j < numberOfValidComponentsAfterFiltering; ++j) {

                const std::span<std::complex<double>> &mixingMatrix_rowSpans_j(mixingMatrix_rowSpans.at(j));

                const std::span<std::complex<double>> independentComponents_rowSpans_rowSpan_j(independentComponents_rowSpans.at(j));

                reconstructedFourierTransformVectorOfImageWindows.clear();

                reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

                for (const std::complex<double> &mixingMatrix_rowSpans_j_value : mixingMatrix_rowSpans_j) {

                    for (const std::complex<double> &independentComponents_rowSpans_rowSpan_j_value : independentComponents_rowSpans_rowSpan_j)
                        reconstructedFourierTransformVectorOfImageWindows.append(mixingMatrix_rowSpans_j_value * independentComponents_rowSpans_rowSpan_j_value);

                }

                reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, true);

                reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

                cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "_reconstructedImageWithIndependentFourierTransformedVectorOfImageWindows_consensus/reconstructedImage_consensusIndependentImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

                if (QThread::currentThread()->isInterruptionRequested())
                    return QList<std::complex<double>>();

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress_1, j + 1);

            }

            this->stopProgress(uuidProgress_1);

        }

    }

    return consensusMixingMatrix;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateConsensusMixingMatrix_applyLibraryMode(const QList<std::complex<double> > &pseudoInverseLibraryConsensusIndependentComponents, const QList<std::complex<double> > &libraryConsensusIndependentComponents, qsizetype numberOfValidComponents, const std::vector<std::complex<double>> &fourierTransformedVectorOfImageWindows)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    // Calculating consensus mixing matrix
    ////////////////

    QUuid uuidProgress = this->startProgress("apply library to images : calculate mixing matrix", 0, 0);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    QList<std::complex<double>> consensusMixingMatrix(d_nImageWindowsForCurrentImage * numberOfValidComponents, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, d_nImageWindowsForCurrentImage, numberOfValidComponents, d_nFeaturesForCurrentImage, &complex_one, fourierTransformedVectorOfImageWindows.data(), d_nFeaturesForCurrentImage, pseudoInverseLibraryConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &complex_zero, consensusMixingMatrix.data(), numberOfValidComponents);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - NC cICA: mixing matrix (consensus)")) {

        QList<QString> stringRepresentation;

        stringRepresentation.reserve(consensusMixingMatrix.size());

        QUuid uuidProgress_1 = this->startProgress("converting mixing matrix to string representation", 0, consensusMixingMatrix.size());

        for (const std::complex<double> &complex : consensusMixingMatrix) {

            stringRepresentation << "(" + QString::number(complex.real()) + " " + QString::number(complex.imag()) + "i)";

            if (QThread::currentThread()->isInterruptionRequested())
                return QList<std::complex<double>>();

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        this->stopProgress(uuidProgress_1);

        QList<QString> rowLabels;

        for (qsizetype j = 0; j < d_nImageWindowsForCurrentImage; ++j)
            rowLabels << "imageWindow_" + QString::number(j + 1);

        QList<QString> columnLabels;

        for (qsizetype j = 0; j < numberOfValidComponents; ++j)
            columnLabels << "libraryConsensusIndependentWindow_" + QString::number(j + 1);

        AnnotatedMatrix<QString> *result = new AnnotatedMatrix<QString>(rowLabels, columnLabels, std::move(stringRepresentation));

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "mixingMatrix");

    }

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponents, &alpha, consensusMixingMatrix.data(), numberOfValidComponents, libraryConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<cv::Mat> reconstructionQualityHeatmapWindows;

            QList<double> ssimMetrics;

            ssimMetrics.reserve(reconstructedImageWindows.count() * (d_nChannelsForCurrentImage + 1));

            QList<double> meanSSIMMetricPerReconstructedImageWindow;

            meanSSIMMetricPerReconstructedImageWindow.reserve(reconstructedImageWindows.count());

            for (qsizetype i = 0; i < reconstructedImageWindows.count(); ++i) {

                cv::Scalar scalar = this->getMSSIM(reconstructedImageWindows.at(i), d_currentImageWindows.at(i));

                double mean = 0.0;

                for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

                    ssimMetrics.append(scalar.val[j]);

                    mean += scalar.val[j];

                }

                mean /= static_cast<double>(d_nChannelsForCurrentImage);

                ssimMetrics.append(mean);

                meanSSIMMetricPerReconstructedImageWindow.append(mean);

                double g = (1.0 - mean) * 255.0;

                double b = (1.0 - mean) * 255.0;

                reconstructionQualityHeatmapWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, CV_8UC3, cv::Scalar(b, g, 255)));

            }

            d_ssimMetric_ReconstructedImageWindowMode.append(boost::math::statistics::median(meanSSIMMetricPerReconstructedImageWindow));

            cv::Mat reconstructionQualityHeatmapImage = this->createOne(reconstructionQualityHeatmapWindows, d_numberOfColumnWindowsInCurrentImage, 0);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructionQualityHeatmap." + d_suffixForCurrentImage.toStdString(), reconstructionQualityHeatmapImage);

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < reconstructedImageWindows.count(); ++j)
                rowLabels << "imageWindow_" + QString::number(j + 1);

            QList<QString> columnLabels;

            for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j)
                columnLabels << "channel_" + QString::number(j + 1);

            columnLabels << "mean";

            AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(ssimMetrics));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

            this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage, "reconstructionQualityMetric");

        }

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        cv::Scalar scalar = this->getMSSIM(reconstructedImage, d_currentPaddedImage);

        double mean = 0.0;

        for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

            mean += scalar.val[j];

        }

        mean /= static_cast<double>(d_nChannelsForCurrentImage);

        d_ssimMetric_ReconstructedImageMode.append(mean);

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImage_AllLibraryConsensusIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);


        if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

            MathOperations::transposeInplace(consensusMixingMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

            QList<std::span<std::complex<double>>> mixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(consensusMixingMatrix, d_nImageWindowsForCurrentImage);

            QList<std::span<std::complex<double>>> independentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<std::complex<double>> *>(&libraryConsensusIndependentComponents), d_nFeaturesForCurrentImage);

            QUuid uuidProgress_1 = this->startProgress("exporting reconstructed images based on library consensus independent image windows", 0, numberOfValidComponents);

            for (qsizetype j = 0; j < numberOfValidComponents; ++j) {

                const std::span<std::complex<double>> &mixingMatrix_rowSpans_j(mixingMatrix_rowSpans.at(j));

                const std::span<std::complex<double>> independentComponents_rowSpans_rowSpan_j(independentComponents_rowSpans.at(j));

                reconstructedFourierTransformVectorOfImageWindows.clear();

                reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

                for (const std::complex<double> &mixingMatrix_rowSpans_j_value : mixingMatrix_rowSpans_j) {

                    for (const std::complex<double> &independentComponents_rowSpans_rowSpan_j_value : independentComponents_rowSpans_rowSpan_j)
                        reconstructedFourierTransformVectorOfImageWindows.append(mixingMatrix_rowSpans_j_value * independentComponents_rowSpans_rowSpan_j_value);

                }

                reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, true);

                reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

                cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/reconstructedImage_libraryConsensusIndependentImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

                if (QThread::currentThread()->isInterruptionRequested())
                    return QList<std::complex<double>>();

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress_1, j + 1);

            }

            this->stopProgress(uuidProgress_1);

        }

    }

    return consensusMixingMatrix;

}

QList<std::complex<double>> ICAImageAnalysisWorker::calculateConsensusMixingMatrix_shuffled_applyLibraryMode(qsizetype shuffleNumber, const QList<std::complex<double> > &libraryConsensusIndependentComponents, qsizetype numberOfValidComponents, const std::vector<std::complex<double>> &fourierTransformedVectorOfImageWindows)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    this->createPath(d_exportPathForCurrentImage + "/shuffle_" + QString::number(shuffleNumber));

    // Calculating consensus mixing matrix
    ////////////////

    QUuid uuidProgress = this->startProgress("apply library to images : calculate consensus mixing matrix", 0, 0);

    int info;

    double error = 0.0;

    QList<std::complex<double>> pseudoInverseLibraryConsensusIndependentComponents = MathOperations::pseudoInverse(libraryConsensusIndependentComponents, d_nFeaturesForCurrentImage, true, error, parameters->numberOfThreadsToUse, &info);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    if (info < 0) {

        this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");

        return QList<std::complex<double>>();

    } else if (info > 0) {

        this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

        return QList<std::complex<double>>();

    }

    if (1) {

        this->reportMessage("total error pseudo-inverse (calculate consensus mixing matrix, shuffled apply library mode) : " + QString::number(static_cast<double>(error)));

    }

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    QList<std::complex<double>> consensusMixingMatrix(d_nImageWindowsForCurrentImage * numberOfValidComponents, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, d_nImageWindowsForCurrentImage, numberOfValidComponents, d_nFeaturesForCurrentImage, &complex_one, fourierTransformedVectorOfImageWindows.data(), d_nFeaturesForCurrentImage, pseudoInverseLibraryConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &complex_zero, consensusMixingMatrix.data(), numberOfValidComponents);

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<std::complex<double>>();

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)") || parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

        std::complex<double> alpha = 1.0;

        std::complex<double> beta = 0.0;

        QList<std::complex<double>> reconstructedFourierTransformVectorOfImageWindows(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nImageWindowsForCurrentImage, d_nFeaturesForCurrentImage, numberOfValidComponents, &alpha, consensusMixingMatrix.data(), numberOfValidComponents, libraryConsensusIndependentComponents.data(), d_nFeaturesForCurrentImage, &beta, reconstructedFourierTransformVectorOfImageWindows.data(), d_nFeaturesForCurrentImage);

        QList<cv::Mat> reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, false);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstruction quality heatmap (consensus)")) {

            QList<cv::Mat> reconstructionQualityHeatmapWindows;

            QList<double> ssimMetrics;

            ssimMetrics.reserve(reconstructedImageWindows.count() * (d_nChannelsForCurrentImage + 1));

            QList<double> meanSSIMMetricPerReconstructedImageWindow;

            meanSSIMMetricPerReconstructedImageWindow.reserve(reconstructedImageWindows.count());

            for (qsizetype i = 0; i < reconstructedImageWindows.count(); ++i) {

                cv::Scalar scalar = this->getMSSIM(reconstructedImageWindows.at(i), d_currentImageWindows.at(i));

                double mean = 0.0;

                for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

                    ssimMetrics.append(scalar.val[j]);

                    mean += scalar.val[j];

                }

                mean /= static_cast<double>(d_nChannelsForCurrentImage);

                ssimMetrics.append(mean);

                meanSSIMMetricPerReconstructedImageWindow.append(mean);

                double g = (1.0 - mean) * 255.0;

                double b = (1.0 - mean) * 255.0;

                reconstructionQualityHeatmapWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, CV_8UC3, cv::Scalar(b, g, 255)));

            }

            d_ssimMetric_ReconstructedImageWindowMode.append(boost::math::statistics::median(meanSSIMMetricPerReconstructedImageWindow));

            cv::Mat reconstructionQualityHeatmapImage = this->createOne(reconstructionQualityHeatmapWindows, d_numberOfColumnWindowsInCurrentImage, 0);

            cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "/reconstructionQualityHeatmap." + d_suffixForCurrentImage.toStdString(), reconstructionQualityHeatmapImage);

            QList<QString> rowLabels;

            for (qsizetype j = 0; j < reconstructedImageWindows.count(); ++j)
                rowLabels << "imageWindow_" + QString::number(j + 1);

            QList<QString> columnLabels;

            for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j)
                columnLabels << "channel_" + QString::number(j + 1);

            columnLabels << "mean";

            AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, columnLabels, std::move(ssimMetrics));

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

            this->exportToFile(sharedPointerToAnnotatedMatrix, d_exportPathForCurrentImage + "/shuffle_" + QString::number(shuffleNumber), "reconstructionQualityMetric");

        }

        cv::Mat reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0);

        cv::Scalar scalar = this->getMSSIM(reconstructedImage, d_currentPaddedImage);

        double mean = 0.0;

        for (qsizetype j = 0; j < d_nChannelsForCurrentImage; ++j) {

            mean += scalar.val[j];

        }

        mean /= static_cast<double>(d_nChannelsForCurrentImage);

        d_ssimMetric_ReconstructedImageMode.append(mean);

        cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "/reconstructedImage_AllLibraryConsensusIndependentImageWindows." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

        if (parameters->selectedResultItems.contains("image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)")) {

            MathOperations::transposeInplace(consensusMixingMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

            QList<std::span<std::complex<double>>> mixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(consensusMixingMatrix, d_nImageWindowsForCurrentImage);

            QList<std::span<std::complex<double>>> independentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<std::complex<double>> *>(&libraryConsensusIndependentComponents), d_nFeaturesForCurrentImage);

            QUuid uuidProgress_1 = this->startProgress("exporting reconstructed images based on library consensus independent image windows", 0, numberOfValidComponents);

            for (qsizetype j = 0; j < numberOfValidComponents; ++j) {

                const std::span<std::complex<double>> &mixingMatrix_rowSpans_j(mixingMatrix_rowSpans.at(j));

                const std::span<std::complex<double>> independentComponents_rowSpans_rowSpan_j(independentComponents_rowSpans.at(j));

                reconstructedFourierTransformVectorOfImageWindows.clear();

                reconstructedFourierTransformVectorOfImageWindows.reserve(d_nImageWindowsForCurrentImage * d_nFeaturesForCurrentImage);

                for (const std::complex<double> &mixingMatrix_rowSpans_j_value : mixingMatrix_rowSpans_j) {

                    for (const std::complex<double> &independentComponents_rowSpans_rowSpan_j_value : independentComponents_rowSpans_rowSpan_j)
                        reconstructedFourierTransformVectorOfImageWindows.append(mixingMatrix_rowSpans_j_value * independentComponents_rowSpans_rowSpan_j_value);

                }

                reconstructedImageWindows = this->convertFourierTransformVectorsToImageWindows(reconstructedFourierTransformVectorOfImageWindows, true);

                reconstructedImage = this->createOne(reconstructedImageWindows, d_numberOfColumnWindowsInCurrentImage, 0, true);

                cv::imwrite(d_exportPathForCurrentImage.toStdString() + "/shuffle_" + QString::number(shuffleNumber).toStdString() + "/reconstructedImage_libraryConsensusIndependentImageWindow_" + QString::number(j + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), reconstructedImage);

                if (QThread::currentThread()->isInterruptionRequested())
                    return QList<std::complex<double>>();

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress_1, j + 1);

            }

            this->stopProgress(uuidProgress_1);

        }

    }

    return consensusMixingMatrix;
}


QList<QList<qsizetype>> ICAImageAnalysisWorker::createConsensusClusterToMemberIndexes(QList<std::complex<double>> &symmetricDistanceMatrix, qsizetype leadingDimension, double threshold, qsizetype numberOfThreadsToUse)
{

    QList<QList<qsizetype>> consensusClusterToMemberIndexes;

    QUuid uuidProgress = this->startProgress("consensus step : converting Pearson distance matrix to link matrix", 0, 0);

    MathOperations::applyFunctor_elementWise_inplace(symmetricDistanceMatrix, [&threshold](std::complex<double> &value){value = (std::abs(value) < threshold) ? std::complex<double>(0) : std::complex<double>(1.0);}, numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return consensusClusterToMemberIndexes;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    QHash<qsizetype, std::span<std::complex<double>>> indexToRowConnectionMatrix = MathUtilityFunctions::rowMajor_matrix1d_hash_rowIndexes_rowSpans(symmetricDistanceMatrix, leadingDimension);

    uuidProgress = this->startProgress("consensus step : clustering link matrix", 0, indexToRowConnectionMatrix.count());

    qsizetype startCountOfIndexToRowConnectionMatrix = indexToRowConnectionMatrix.count();

    QList<double> sums(indexToRowConnectionMatrix.count());

    QList<bool> updateSums(indexToRowConnectionMatrix.count(), true);

    while (!indexToRowConnectionMatrix.isEmpty()) {

        qsizetype indexWithMaximumSum = -1;

        double maximumSum = 0;

        QHashIterator<qsizetype, std::span<std::complex<double>>> it(indexToRowConnectionMatrix);

        std::complex<double> complexZero = 0.0;

        while (it.hasNext()) {

            it.next();

            double sum;

            if (updateSums.at(it.key())) {

                sum = std::accumulate(it.value().begin(), it.value().end(), complexZero).real();

                updateSums[it.key()] = false;

                sums[it.key()] = sum;

            } else
                sum = sums.at(it.key());

            if (sum >= maximumSum) {

                maximumSum = sum;

                indexWithMaximumSum = it.key();

            }

            if (QThread::currentThread()->isInterruptionRequested())
                return consensusClusterToMemberIndexes;

            this->checkIfPauseWasRequested();

        }

        QList<qsizetype> memberIndexes;

        qsizetype index = 0;

        for (std::complex<double> &value : indexToRowConnectionMatrix.value(indexWithMaximumSum)) {

            if ((std::fabs(value.real() - 1.0) <= std::numeric_limits<double>::epsilon()))
                memberIndexes.append(index);

            ++index;

            if (QThread::currentThread()->isInterruptionRequested())
                 return consensusClusterToMemberIndexes;

            this->checkIfPauseWasRequested();

        }

        it.toFront();

        std::complex<double> complexOne = 1.0;

        while (it.hasNext()) {

            it.next();

            const std::span<std::complex<double>> &span(it.value());

            for (qsizetype index : memberIndexes) {

                std::complex<double> &value(span[index]);

                if (value == complexOne) {

                    updateSums[it.key()] = true;

                    value = 0;

                }

            }

        }

        for (qsizetype index : memberIndexes)
            indexToRowConnectionMatrix.remove(index);

        consensusClusterToMemberIndexes.append(memberIndexes);

        if (QThread::currentThread()->isInterruptionRequested())
             return consensusClusterToMemberIndexes;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, startCountOfIndexToRowConnectionMatrix - indexToRowConnectionMatrix.count());

    }

    this->stopProgress(uuidProgress);

    return consensusClusterToMemberIndexes;

}

void ICAImageAnalysisWorker::updateLibrary(const QList<std::complex<double> > &consensusIndependentComponents)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    qsizetype numberOfConsensusIndependentComponents = consensusIndependentComponents.count() / d_nFeaturesForCurrentImage;

    QList<bool> processedConsensusIndependentComponents(numberOfConsensusIndependentComponents, false);

    qsizetype nConsensusIndependentComponentsLeftToProcess = numberOfConsensusIndependentComponents;

    QList<std::complex<double>> normalizedConsensusIndependentComponents = consensusIndependentComponents;

    normalizedConsensusIndependentComponents.detach();

    QList<std::span<std::complex<double>>> rowSpans_normalizedConsensusIndependentComponents = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(normalizedConsensusIndependentComponents, d_nFeaturesForCurrentImage);

    this->startProgress("updating library: center consensus independent components", QtConcurrent::map(&d_threadPool, rowSpans_normalizedConsensusIndependentComponents, [](std::span<std::complex<double>> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->startProgress("updating library: scaling consensusindependent components", QtConcurrent::map(&d_threadPool, rowSpans_normalizedConsensusIndependentComponents, [](std::span<std::complex<double>> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    QUuid uuidProgress = this->startProgress("updating library : identifying matches", 0, d_library.count());

    for (qsizetype i = 0; i < d_library.count(); ++i) {

        if (nConsensusIndependentComponentsLeftToProcess > 0) {

            std::pair<qsizetype, QList<std::complex<double>>> &currentLibraryItem(d_library[i]);

            QList<std::complex<double>> normalizedCurrentLibraryItem = currentLibraryItem.second;

            MathOperations::centerVectorToMeanInplace(normalizedCurrentLibraryItem);

            MathOperations::scaleVectorToL2NormInplace(normalizedCurrentLibraryItem);

            for (std::complex<double> &value : normalizedCurrentLibraryItem)
                value = std::conj(value);

            for (qsizetype j = 0; j < rowSpans_normalizedConsensusIndependentComponents.count(); ++j) {

                if (!processedConsensusIndependentComponents.at(j)) {

                    const std::span<std::complex<double>> &rowSpan_j_normalizedConsensusIndependentComponents(rowSpans_normalizedConsensusIndependentComponents.at(j));

                    std::complex<double> dotProduct = 0.0;

                    for (qsizetype k = 0; k < normalizedCurrentLibraryItem.count(); ++k)
                        dotProduct += rowSpan_j_normalizedConsensusIndependentComponents[k] * normalizedCurrentLibraryItem.at(k);

                    if (std::abs(dotProduct) >= parameters->xiCorrelationUpperThresholdForLibrary) {

                        currentLibraryItem.first += 1;

                        processedConsensusIndependentComponents[j] = true;

                        nConsensusIndependentComponentsLeftToProcess -= 1;

                        break;

                    }

                    if (std::abs(dotProduct) > parameters->xiCorrelationLowerThresholdForLibrary) {

                        processedConsensusIndependentComponents[j] = true;

                        nConsensusIndependentComponentsLeftToProcess -= 1;

                        break;

                    }

                }

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("updating library : adding new", 0, d_library.count());

    qsizetype counter = 0;

    for (qsizetype i = 0; i < processedConsensusIndependentComponents.count(); ++i) {

        if (!processedConsensusIndependentComponents.at(i)) {

            if (d_library.count() < parameters->maximumLibrarySize)
                d_library.append(std::pair<qsizetype, QList<std::complex<double>>>(1, consensusIndependentComponents.sliced(i * d_nFeaturesForCurrentImage, d_nFeaturesForCurrentImage)));
            else
                ++counter;

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    if (counter != 0)
        this->reportWarning("could not add " + QString::number(counter) + " new consensus independent windows to library, limit has been reached");

    uuidProgress = this->startProgress("updating library : sorting library", 0, 0);

    std::sort(d_library.begin(), d_library.end(), [](const std::pair<qsizetype, QList<std::complex<double>>> &element1, const std::pair<qsizetype, QList<std::complex<double>>> &element2){return element1.first > element2.first;});

    this->stopProgress(uuidProgress);

    this->reportMessage("number of consensus independent windows in library:" + QString::number(d_library.count()));

    d_sizeOfLibraryTracker.append(d_library.count());

}

void ICAImageAnalysisWorker::updateLibrary_test(const QList<std::complex<double> > &consensusIndependentComponents)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    qsizetype numberOfConsensusIndependentComponents = consensusIndependentComponents.count() / d_nFeaturesForCurrentImage;

    QList<bool> processedConsensusIndependentComponents(numberOfConsensusIndependentComponents, false);

    qsizetype nConsensusIndependentComponentsLeftToProcess = numberOfConsensusIndependentComponents;

    QList<double> consensusIndependentComponents_real;

    consensusIndependentComponents_real.reserve(consensusIndependentComponents.count());

    QList<double> consensusIndependentComponents_imag;

    consensusIndependentComponents_imag.reserve(consensusIndependentComponents.count());

    for (const std::complex<double> &value : consensusIndependentComponents) {

        consensusIndependentComponents_real.append(value.real());

        consensusIndependentComponents_imag.append(value.imag());

    }

    QList<std::span<double>> dispatcher_consensusIndependentComponents_real = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<double>*>(&consensusIndependentComponents_real), d_nFeaturesForCurrentImage);

    QList<std::span<double>> dispatcher_consensusIndependentComponents_imag = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<double>*>(&consensusIndependentComponents_imag), d_nFeaturesForCurrentImage);

    this->startProgress("ranking consensus independent components (real part)", QtConcurrent::map(&d_threadPool, dispatcher_consensusIndependentComponents_real, [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->startProgress("ranking consensus independent components (imaginary part)", QtConcurrent::map(&d_threadPool, dispatcher_consensusIndependentComponents_imag, [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    QUuid uuidProgress = this->startProgress("updating library : identifying matches", 0, d_library.count());

    for (qsizetype i = 0; i < d_library.count(); ++i) {

        if (nConsensusIndependentComponentsLeftToProcess > 0) {

            std::pair<qsizetype, QList<std::complex<double>>> &currentLibraryItem(d_library[i]);

            QList<double> currentLibraryItem_real;

            currentLibraryItem_real.reserve(currentLibraryItem.second.count());

            QList<double> currentLibraryItem_imag;

            currentLibraryItem_imag.reserve(currentLibraryItem.second.count());

            for (const std::complex<double> &value : currentLibraryItem.second) {

                currentLibraryItem_real.append(value.real());

                currentLibraryItem_imag.append(value.imag());

            }

            MathOperations::rankInplace(currentLibraryItem_real);

            MathOperations::rankInplace(currentLibraryItem_imag);


            for (qsizetype j = 0; j < dispatcher_consensusIndependentComponents_real.count(); ++j) {

                if (!processedConsensusIndependentComponents.at(j)) {

                    double xiCorrelation_real_1 = MathDescriptives::xiCorrelation_preRanked(std::span<double>(currentLibraryItem_real.data(), currentLibraryItem_real.count()), dispatcher_consensusIndependentComponents_real.at(i));

                    double xiCorrelation_real_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher_consensusIndependentComponents_real.at(i), std::span<double>(currentLibraryItem_real.data(), currentLibraryItem_real.count()));

                    double xiCorrelation_imag_1 = MathDescriptives::xiCorrelation_preRanked(std::span<double>(currentLibraryItem_imag.data(), currentLibraryItem_imag.count()), dispatcher_consensusIndependentComponents_imag.at(i));

                    double xiCorrelation_imag_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher_consensusIndependentComponents_imag.at(i), std::span<double>(currentLibraryItem_imag.data(), currentLibraryItem_imag.count()));

                    double xiCorrelation = std::max(std::max(xiCorrelation_real_1, xiCorrelation_real_2), std::max(xiCorrelation_imag_1, xiCorrelation_imag_2));

                    if (std::abs(xiCorrelation) > parameters->xiCorrelationUpperThresholdForLibrary) {

                        currentLibraryItem.first += 1;

                        processedConsensusIndependentComponents[j] = true;

                        nConsensusIndependentComponentsLeftToProcess -= 1;

                        break;

                    }

                }

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("updating library : adding new", 0, d_library.count());

    qsizetype counter = 0;

    for (qsizetype i = 0; i < processedConsensusIndependentComponents.count(); ++i) {

        if (!processedConsensusIndependentComponents.at(i)) {

            if (d_library.count() <= parameters->maximumLibrarySize)
                d_library.append(std::pair<qsizetype, QList<std::complex<double>>>(1, consensusIndependentComponents.sliced(i * d_nFeaturesForCurrentImage, d_nFeaturesForCurrentImage)));
            else
                ++counter;

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    if (counter != 0)
        this->reportWarning("could not add " + QString::number(counter) + " new consensus independent windows to library, limit has been reached");

    uuidProgress = this->startProgress("updating library : sorting library", 0, 0);

    std::sort(d_library.begin(), d_library.end(), [](const std::pair<qsizetype, QList<std::complex<double>>> &element1, const std::pair<qsizetype, QList<std::complex<double>>> &element2){return element1.first > element2.first;});

    this->stopProgress(uuidProgress);

    this->reportMessage("number of consensus independent windows in library:" + QString::number(d_library.count()));

    d_sizeOfLibraryTracker.append(d_library.count());

}

void ICAImageAnalysisWorker::exportLibrary()
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    QList<QString> rowLabels;

    for (qsizetype i = 0; i < d_sizeOfLibraryTracker.count(); ++i)
        rowLabels.append("image_" + QString::number(i + 1));

    if (parameters->selectedResultItems.contains("library - library size tracker")) {

        this->createPath(parameters->exportDirectory + "/library");

        AnnotatedMatrix<qsizetype> *result = new AnnotatedMatrix<qsizetype>(rowLabels, {"Number of features"}, d_sizeOfLibraryTracker);

        QSharedPointer<AnnotatedMatrix<qsizetype>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory + "/library", "sizeOfLibraryTracker");

    }

    rowLabels.clear();

    QList<double> featureFrequencies;

    for (qsizetype i = 0; i < d_library.count(); ++i) {

        rowLabels.append("feature_" + QString::number(i + 1));

        double temp = d_library.at(i).first;

        featureFrequencies.append(temp);

        featureFrequencies.append(temp / static_cast<double>(d_numberOfTrainingImagesUsed));

    }

    if (parameters->selectedResultItems.contains("library - library feature frequencies")) {

        this->createPath(parameters->exportDirectory + "/library");

        AnnotatedMatrix<double> *result = new AnnotatedMatrix<double>(rowLabels, {"N", "Frequency"}, featureFrequencies);

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory + "/library", "featureFrequencies");

    }

    if (parameters->selectedResultItems.contains("library - features")) {

        QList<cv::Mat> independentImageWindows;

        QUuid uuidProgress = this->startProgress("converting library features from frequency domain to image", 0, independentImageWindows.count());

        for (qsizetype i = 0; i < d_library.count(); ++i) {

            independentImageWindows.append(this->convertFourierTransformVectorsToImageWindows(d_library.at(i).second, true));

            if (QThread::currentThread()->isInterruptionRequested())
                return ;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->createPath(parameters->exportDirectory + "/library");

        this->stopProgress(uuidProgress);


        uuidProgress = this->startProgress("exporting library features", 0, independentImageWindows.count());

        for (qsizetype i = 0; i < independentImageWindows.count(); ++i) {

            independentImageWindows[i].convertTo(independentImageWindows[i], -1, 5, 1);

            cv::imwrite(parameters->exportDirectory.toStdString() + "/library/feature" + QString::number(i + 1).toStdString() + "." + d_suffixForCurrentImage.toStdString(), independentImageWindows.at(i));

            if (QThread::currentThread()->isInterruptionRequested())
                return ;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->stopProgress(uuidProgress);

        qsizetype nColumns = std::ceil(std::sqrt(independentImageWindows.count()));

        qsizetype numberToAdd = independentImageWindows.count() - (nColumns * nColumns);

        for (qsizetype i = 0; i < numberToAdd; ++i)
            independentImageWindows.append(cv::Mat(parameters->windowSize, parameters->windowSize, d_depthForCurrentImage, cv::Scalar(0, 0, 0)));

        cv::Mat overviewIndependentImageWindows = this->createOne(independentImageWindows, nColumns, 1);

        this->createPath(parameters->exportDirectory + "/library");

        cv::imwrite(parameters->exportDirectory.toStdString() + "/library/overviewFeatures." + d_suffixForCurrentImage.toStdString(), overviewIndependentImageWindows);

    }

}

QList<double> ICAImageAnalysisWorker::xiCorrelationSymmetricalMatrix(const QList<std::complex<double> > &vectors, qsizetype leadingDimension)
{

    QSharedPointer<ICAImageAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<ICAImageAnalysisParameters>());

    QList<double> vectors_real;

    vectors_real.reserve(vectors.count());

    QList<double> vectors_imag;

    vectors_imag.reserve(vectors.count());

    for (const std::complex<double> &value : vectors) {

        vectors_real.append(value.real());

        vectors_imag.append(value.imag());

    }

    this->startProgress("ranking vectors (real part)", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<double>*>(&vectors_real), leadingDimension), [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<double>();

    this->checkIfPauseWasRequested();

    this->startProgress("ranking vectors (imaginary part)", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<double>*>(&vectors_imag), leadingDimension), [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<double>();

    this->checkIfPauseWasRequested();

    QList<std::pair<qsizetype, std::span<double>>> dispatcher_real = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double>*>(&vectors_real), leadingDimension);

    QList<std::pair<qsizetype, std::span<double>>> dispatcher_imag = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double>*>(&vectors_imag), leadingDimension);

    qsizetype nRows = vectors.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("calculating xi correlations", 0, static_cast<qsizetype>(nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor = [this, &uuidProgress, &nRows, &dispatcher_real, &dispatcher_imag](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> xiCorrelations(nRows, 0.0);

        this->updateProgressWithOne(uuidProgress);

        for (qsizetype i = index_span.first; i < nRows; ++i) {

            double xiCorrelation_real_1 = MathDescriptives::xiCorrelation_preRanked(index_span.second, dispatcher_real.at(i).second);

            double xiCorrelation_real_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher_real.at(i).second, index_span.second);

            double xiCorrelation_imag_1 = MathDescriptives::xiCorrelation_preRanked(dispatcher_imag.at(index_span.first).second, dispatcher_imag.at(i).second);

            double xiCorrelation_imag_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher_imag.at(i).second, dispatcher_imag.at(index_span.first).second);

            xiCorrelations[i] = std::max(std::max(xiCorrelation_real_1, xiCorrelation_real_2), std::max(xiCorrelation_imag_1, xiCorrelation_imag_2));

            if (this->thread()->isInterruptionRequested())
                return xiCorrelations;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return xiCorrelations;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<double> mat = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher_real, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->stopProgress(uuidProgress);

    return mat;

}

double ICAImageAnalysisWorker::xiCorrelation(QList<std::complex<double> > vector1, QList<std::complex<double> > vector2)
{

    QList<double> vector1_real;

    vector1_real.reserve(vector1.count());

    QList<double> vector1_imag;

    vector1_imag.reserve(vector1.count());

    for (const std::complex<double> &value : vector1) {

        vector1_real.append(value.real());

        vector1_imag.append(value.imag());

    }

    QList<double> vector2_real;

    vector2_real.reserve(vector2.count());

    QList<double> vector2_imag;

    vector2_imag.reserve(vector2.count());

    for (const std::complex<double> &value : vector2) {

        vector2_real.append(value.real());

        vector2_imag.append(value.imag());

    }

    double r1 = MathDescriptives::xiCorrelation(vector1_real, vector2_real);

    double r2 = MathDescriptives::xiCorrelation(vector2_real, vector1_real);

    double r3 = MathDescriptives::xiCorrelation(vector1_imag, vector2_imag);

    double r4 = MathDescriptives::xiCorrelation(vector2_imag, vector1_imag);

    return std::max(std::max(r1, r2), std::max(r3, r4));

}

cv::Scalar ICAImageAnalysisWorker::getMSSIM( const cv::Mat& i1, const cv::Mat& i2) const
{
    const double C1 = 6.5025, C2 = 58.5225;

    /***************************** INITS **********************************/

    int d = CV_32F;

    cv::Mat I1, I2;

    i1.convertTo(I1, d);            // cannot calculate on one byte large values

    i2.convertTo(I2, d);

    cv::Mat I2_2   = I2.mul(I2);        // I2^2

    cv::Mat I1_2   = I1.mul(I1);        // I1^2

    cv::Mat I1_I2  = I1.mul(I2);        // I1 * I2

    /*************************** END INITS **********************************/
    cv::Mat mu1, mu2;                   // PRELIMINARY COMPUTING

    cv::GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);

    cv::GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);

    cv::Mat mu1_2   =   mu1.mul(mu1);

    cv::Mat mu2_2   =   mu2.mul(mu2);

    cv::Mat mu1_mu2 =   mu1.mul(mu2);

    cv::Mat sigma1_2, sigma2_2, sigma12;

    cv::GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);

    sigma1_2 -= mu1_2;

    cv::GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);

    sigma2_2 -= mu2_2;

    GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);

    sigma12 -= mu1_mu2;

    cv::Mat t1, t2, t3;

    t1 = 2 * mu1_mu2 + C1;

    t2 = 2 * sigma12 + C2;

    t3 = t1.mul(t2);                 // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

    t1 = mu1_2 + mu2_2 + C1;

    t2 = sigma1_2 + sigma2_2 + C2;

    t1 = t1.mul(t2);                 // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

    cv::Mat ssim_map;

    divide(t3, t1, ssim_map);        // ssim_map =  t3./t1;

    cv::Scalar mssim = mean(ssim_map);   // mssim = average of ssim map

    return mssim;

}
