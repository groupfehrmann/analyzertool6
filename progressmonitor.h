#ifndef PROGRESSMONITOR_H
#define PROGRESSMONITOR_H

#include <QObject>
#include <QSharedDataPointer>
#include <QUuid>
#include <QString>
#include <QDateTime>
#include <QHash>
#include <QList>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

#include "boost/circular_buffer.hpp"

class MovingAverage {

private:

        boost::circular_buffer<double> q;

        double sum;

        double ssq;
public:

    MovingAverage(int n = 3)  {

        sum = 0;

        ssq = 0;

        q = boost::circular_buffer<double>(n);

    }

    void push(double v) {

        if (q.size() == q.capacity()) {

            double t = q.front();

            sum -= t;

            ssq -= t * t;

            q.pop_front();

        }

        q.push_back(v);

        sum += v;

        ssq += v * v;

    }

    double size() {

        return q.size();

    }

    double mean() {

        return sum / size();

    }

};

class ProgressMonitorData : public QSharedData
{

public:

    struct ProgressInfo {

        QString d_label;

        qsizetype d_maximum;

        qsizetype d_minimum;

        QDateTime d_startTime;

        qsizetype d_value;

        qint64 d_etaInSeconds;

        QUuid d_uuidWorker;

        bool d_interrupted;

        bool d_paused;

        qsizetype d_lastupdateValue;

        QDateTime d_lastUpdateTime;

        MovingAverage d_timePerStepMovingAverage;

        bool d_provideETA;

    };

    struct WorkerInfo {

        QString d_label;

        QDateTime d_startTime;

        QList<QUuid> d_uuidsProgress;

        bool d_interrupted;

        bool d_paused;

        int d_delayInSeconds;

    };

    QHash<QUuid, ProgressInfo> d_uuidProgressToProgressInfo;

    QHash<QUuid, WorkerInfo> d_uuidWorkerToWorkerInfo;

    QList<QUuid> d_uuidsWorker;

    QList<QUuid> d_emptyListOfQUuid;

    ProgressInfo d_emptyProgressInfo;

    WorkerInfo d_emptyWorkerInfo;

    QUuid d_emptyUuid;

};

class ProgressMonitor : public QObject
{

    Q_OBJECT

public:

    static QString secondsToTimeString(qint64 s);

    static QString milliSecondsToTimeString(qint64 s);

    explicit ProgressMonitor(QObject *parent = nullptr);

    qsizetype indexOfUuidProgress(const QUuid& uuidProgress) const;

    qsizetype indexOfUuidWorker(const QUuid& uuidWorker) const;

    qsizetype indexOfUuidWorkerOfWhichUuidProgressIsMember(const QUuid& uuidProgress) const;

    bool isUuidWorker(const QUuid& uuid) const;

    bool isUuidProgress(const QUuid& uuid) const;

    const ProgressMonitorData::ProgressInfo &progressInfo(const QUuid& uuidProgress) const;

    const QUuid &uuidWorker(const QUuid& uuidProgress) const;

    const QList<QUuid> &uuidsWorker() const;

    const QList<QUuid> &uuidsProgress(const QUuid& uuidWorker) const;

    const ProgressMonitorData::WorkerInfo &workerInfo(const QUuid& uuidWorker) const;

    ~ProgressMonitor();

protected:

    QSharedDataPointer<ProgressMonitorData> d_data;

    mutable QReadWriteLock d_readWriteLock;

    void timerEvent(QTimerEvent *event);

public slots:

    void putWorkerInQueu(const QUuid &uuidWorker, const QString &workerLabel, int delayInSeconds);

    void startMonitoringWorker(const QUuid &uuidWorker);

    void startProgress(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, qsizetype minimum, qsizetype maximum);

    void stopAllMonitoringAssociatedWithUuid(const QUuid &uuid);

    void updateProgress(const QUuid &uuidProgress, qsizetype value);

    void updateProgressLabel(const QUuid &uuidProgress, const QString &label);

    void updateProgressWithOne(const QUuid &uuidProgress);

    void workerIsInterruption(const QUuid &uuidWorker);

    void workerIsPaused(const QUuid &uuidWorker);

    void workerIsResumed(const QUuid &uuidWorker);

signals:

    void messageReported(const QUuid &uuidWorker, const QString &message) const;

};

#endif // PROGRESSMONITOR_H

