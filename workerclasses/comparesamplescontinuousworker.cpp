#include "comparesamplescontinuousworker.h"

CompareSamplesContinuousParameters::CompareSamplesContinuousParameters(QObject *parent) :
    BaseParameters(parent, "comparesamplescontinuous")
{

}

bool CompareSamplesContinuousParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if (((metaAnalysisApproach == "Generic inverse method with fixed effect model") || (metaAnalysisApproach == "Generic inverse method with random effect model")) && (!baseStatisticalTest->effectSizeAvailable())) {

        d_lastError ="statistical test \"" + statisticalTest + "\" has no effect size, which is needed for \"Generic inverse method\" as meta analysis approach";

        return false;

    }


    if ((selectedSampleIdentifiers.count() < baseStatisticalTest->minimumNumberOfSamples()) || (selectedSampleIdentifiers.count() > baseStatisticalTest->maximumNumberOfSamples())) {

        d_lastError = "number of sample identifiers selected (" + QString::number(selectedSampleIdentifiers.count()) + ") is incompatible with statistical test \"" + statisticalTest + "\"";

        return false;

    }

    if ((annotationIndexDefiningItemSubsets != -1) && selectedItemSubsetIdentifiers.isEmpty()) {

        d_lastError = "annotation label defining item subsets selected, but no item subset identifiers were selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString CompareSamplesContinuousParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[compare samples in data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[annotation label defining samples = \"" << annotationLabelDefiningSamples << "\"]";

    stream << Qt::endl << "\t[selected sample identifiers = [\"" << selectedSampleIdentifiers.first() << "\"]";

    for (qsizetype i = 1; i < selectedSampleIdentifiers.count(); ++i)
        stream << ", [\"" << selectedSampleIdentifiers.at(i) << "\"]";

    stream << "]";

    if (annotationIndexDefiningItemSubsets != -1) {

        stream << Qt::endl << "\t[annotation label defining item subsets = \"" << annotationLabelDefiningItemSubsets << "\"]";

        stream << Qt::endl << "\t[selected item subset identifiers = [\"" << selectedItemSubsetIdentifiers.first() << "\"]";

        for (qsizetype i = 1; i < selectedItemSubsetIdentifiers.count(); ++i)
            stream << ", [\"" << selectedItemSubsetIdentifiers.at(i) << "\"]";

        stream << "]";

    }

    if (annotationIndexDefiningPairs != -1)
        stream << Qt::endl << "\t[annotation label defining pairs = \"" << annotationLabelDefiningPairs << "\"]";

    if (!annotationLabelsForVariables.isEmpty()) {

        stream << Qt::endl << "\t[selected annotation labels for variables = [\"" << annotationLabelsForVariables.first() << "\", " << annotationLabelsForVariables.first() << "]";

        for (qsizetype i = 1; i < annotationLabelsForVariables.count(); ++i)
            stream << ", [\"" << annotationLabelsForVariables.at(i) << "\", " << annotationLabelsForVariables.at(i) << "]";

        stream << "]";

    }

    if (enablePermutationMode) {

        stream << Qt::endl << "\t[permutation mode is enabled with " << numberOfPermutations << " permutations]";

        stream << Qt::endl << "\t[false discovery rate to controle for in permutation mode " << falseDiscoveryRate <<"]";

        stream << Qt::endl << "\t[confidence level for multivariate threshold in permutation mode " << confidenceLevel <<"]";

    }

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

CompareSamplesContinuousWorker::CompareSamplesContinuousWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("compare samples (continuous data)"), parameters, "comparesamplescontinuous")
{

}

void CompareSamplesContinuousWorker::doWork_()
{

    QSharedPointer<CompareSamplesContinuousParameters> parameters(d_data->d_parameters.dynamicCast<CompareSamplesContinuousParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("values in data need to be numerical to compare samples");

        QThread::currentThread()->requestInterruption();

    }

}

bool CompareSamplesContinuousWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<CompareSamplesContinuousParameters> parameters(d_data->d_parameters.dynamicCast<CompareSamplesContinuousParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void CompareSamplesContinuousWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<CompareSamplesContinuousParameters> parameters(d_data->d_parameters.dynamicCast<CompareSamplesContinuousParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}

void CompareSamplesContinuousWorker::apply_permutation(QList<qsizetype> &list, QList<qsizetype> indices)
{

    for (qsizetype i = 0; i < list.count(); i++) {

        auto current = i;

        while (i != indices[current]) {

            auto next = indices[current];

            std::swap(list[current], list[next]);

            indices[current] = current;

            current = next;
        }

        indices[current] = current;

    }

}

