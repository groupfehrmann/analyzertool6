#include "ludcmp.h"

LUdcmp::LUdcmp(QList<QList<double> > &a) :
    n(a.count()),
    lu(a),
    indx(n),
    aref(a)
{

    const double TINY = 1.0e-40;

    QList<double> vv(n);

    d = 1.0;

    for(qsizetype i = 0; i < n; ++i)
    {

        double big = 0.0;

        const QList<double> &lu_i(lu.at(i));

        for(qsizetype j = 0; j < n; ++j) {

            double temp;

            if((temp = std::fabs(lu_i.at(j))) > big)
                big = temp;

        }

        if( big == 0.0 )
            throw("Singular matrix in LUdcmp");

        vv[i] = 1.0 / big;

    }

    for(qsizetype k = 0; k < n; ++k)
    {
        double big = 0.0;

        qsizetype imax = k;

        for(qsizetype i = k; i < n; ++i)
        {

           double temp = vv.at(i) * std::fabs( lu.at(i).at(k));

            if( temp > big ) {

                big = temp;

                imax = i;

            }

        }

        if(k != imax) {

            QList<double> &lu_imax(lu[imax]);

            QList<double> &lu_k(lu[k]);

            for(qsizetype j = 0; j < n; ++j) {

                double temp = lu_imax.at(j);

                lu_imax[j] = lu_k.at(j);

                lu_k[j] = temp;

            }

            d = -d;

            vv[imax] = vv.at(k);

        }

        indx[k] = imax;

        if(lu.at(k).at(k) == 0.0)
            lu[k][k] = TINY;

        const double &lu_k_k(lu.at(k).at(k));

        const QList<double> &lu_k(lu.at(k));

        for(qsizetype i = k + 1; i < n; ++i)
        {

            double temp = lu[i][k] /= lu_k_k;

            QList<double> &lu_i(lu[i]);

            for(qsizetype j = k + 1; j < n; ++j)
                lu_i[j] -= temp * lu_k.at(j);

        }

    }

}

void LUdcmp::solve(QList<double> &b, QList<double> &x)
{

    if((b.count() != n) || (x.count() != n))
        throw( "LUdcmp::solve bad sizes" );

    for(qsizetype i = 0; i < n; ++i)
        x[i] = b.at(i);

    int ii = 0;

    for(qsizetype i = 0; i < n; ++i) {

        qsizetype ip = indx.at(i);

        double sum = x.at(ip);

        x[ip] = x.at(i);

        const QList<double> &lu_i(lu.at(i));

        if( ii != 0 )
            for(qsizetype j = ii - 1; j < i; ++j)
                sum -= lu_i.at(j) * x.at(j);
        else if(sum != 0.0)
            ii = i + 1;

        x[i] = sum;

    }

    for(qsizetype i = n - 1; i >= 0; --i) {

        double sum = x.at(i);

        const QList<double> &lu_i(lu.at(i));

        for(qsizetype j = i + 1; j < n; ++j)
            sum -= lu_i.at(j) * x.at(j);

        x[i] = sum / lu_i.at(i);

    }

}

void LUdcmp::solve(QList<QList<double> > &b, QList<QList<double> > &x)
{
    qsizetype m = b.isEmpty() ? 0 : b.at(0).count();

    if( b.size() != n || x.count() != n || (b.isEmpty() ? 0 : b.at(0).size()) != (x.isEmpty() ? 0 : x.at(0).count()) )
        throw( "LUdcmp::solve bad sizes" );

    QList<double> xx(n);

    for(qsizetype j = 0; j < m; ++j) {

        for(qsizetype i = 0; i < n; ++i)
            xx[i] = b.at(i).at(j);

        LUdcmp::solve(xx, xx);

        for(qsizetype i = 0; i < n; ++i)
            x[i][j] = xx.at(i);

    }

}

void LUdcmp::inverse(QList<QList<double> > &ainv)
{

    ainv = QList<QList<double> >(n, QList<double>(n));

    for(qsizetype i = 0; i < n; ++i) {

        for(qsizetype j = 0; j < n; ++j)
            ainv[i][j] = 0.0;

        ainv[i][i] = 1.0;

    }

    LUdcmp::solve(ainv, ainv);

}
double LUdcmp::det()
{

    double dd = d;

    for(qsizetype i = 0; i < n; ++i)
        dd *= lu.at(i).at(i);

    return dd;

}

void LUdcmp::mprove(QList<double> &b, QList<double> &x)
{

    QList<double> r(n);

    for(qsizetype i = 0; i < n; ++i) {

        long double sdp = -b[i];

        const QList<double> &aref_i(aref.at(i));

        for(qsizetype j = 0; j < n; ++j)
            sdp += static_cast<long double>(aref_i.at(j)) * static_cast<long double>(x.at(j));

        r[i] = sdp;

    }

    LUdcmp::solve(r, r);

    for(qsizetype i = 0; i < n; ++i)
        x[i] -= r.at(i);

}

