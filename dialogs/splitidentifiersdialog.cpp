#include "splitidentifiersdialog.h"

SplitIdentifiersDialog::SplitIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "split identifiers", "splitidentifiersdialog")
{

    SplitIdentifiersParameters *splitIdentifiersParameters = new SplitIdentifiersParameters;

    splitIdentifiersParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<SplitIdentifiersParameters>(splitIdentifiersParameters);

    BaseDialog::initWidgets();

    this->connect(this, &SplitIdentifiersDialog::accepted, this, &SplitIdentifiersDialog::dialogAccepted);

}

void SplitIdentifiersDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 1);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_selectDelimiterWidget = new SelectDelimiterWidget(this, "d_selectDelimiterWidget");

    d_selectDelimiterWidget->label()->setText("select delimiter to split identifiers");

    d_selectDelimiterWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_selectDelimiterWidget, 2, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_tokenToKeep = new SpinBoxWidget(this, "spinBoxWidget_tokenToKeep");

    this->connect(spinBoxWidget_tokenToKeep->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data())->tokenToKeep = value;});

    spinBoxWidget_tokenToKeep->label()->setText("select number of token to keep");

    spinBoxWidget_tokenToKeep->spinBox()->setRange(1, 100);

    spinBoxWidget_tokenToKeep->spinBox()->setValue(1);

    spinBoxWidget_tokenToKeep->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_tokenToKeep, 3, 0, 1, 1);



    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SplitIdentifiersDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void SplitIdentifiersDialog::orientationChanged(Qt::Orientation orientation)
{

    SplitIdentifiersParameters *splitIdentifiersParameters = static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(splitIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), splitIdentifiersParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(splitIdentifiersParameters->baseAnnotatedMatrix->identifiers(orientation, true), splitIdentifiersParameters->baseAnnotatedMatrix->indexes(orientation, true));

}

void SplitIdentifiersDialog::dialogAccepted()
{

    static_cast<SplitIdentifiersParameters *>(BaseDialog::d_parameters.data())->delimiter = d_selectDelimiterWidget->comboBox()->currentData().toString();

}
