#ifndef SYSMEXDATAPROCESSORDIALOG_H
#define SYSMEXDATAPROCESSORDIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>
#include <QInputDialog>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/sysmexdataprocessorworker.h"

class SysmexDataProcessorDialog : public BaseDialog
{

    Q_OBJECT

public:

    SysmexDataProcessorDialog(BaseDialog *parent = nullptr, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices = QList<QSharedPointer<BaseAnnotatedMatrix>>());

protected:

    void initWidgets_() override;

private:

    QList<QSharedPointer<BaseAnnotatedMatrix>> d_baseAnnotatedMatrices;

    OrientationWidget *d_orientationWidget_patientInfo;

    ItemSelectorWidget *d_itemSelectorWidget_variables_patientInfo;

    ComboBoxWidget *d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithUniquePatientId;

    ComboBoxWidget *d_comboBoxWidget_patientInfo_variableLabelDefiningColumnWithSysmexId;

    OrientationWidget *d_orientationWidget_sysmexInfo;

    ItemSelectorWidget *d_itemSelectorWidget_variables_sysmexInfo;

    ComboBoxWidget *d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithSysmexId;

    ComboBoxWidget *d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithDate;

    ComboBoxWidget *d_comboBoxWidget_sysmexInfo_variableLabelDefiningColumnWithTime;

private slots:

    void orientation_patientInfo_changed(Qt::Orientation orientation);

    void orientation_sysmexInfo_changed(Qt::Orientation orientation);

};

#endif // SYSMEXDATAPROCESSORDIALOG_H
