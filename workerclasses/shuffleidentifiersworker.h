#ifndef SHUFFLEIDENTIFIERSWORKER_H
#define SHUFFLEIDENTIFIERSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class ShuffleIdentifiersParameters : public BaseParameters
{

public:

    ShuffleIdentifiersParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ShuffleIdentifiersWorker : public BaseWorker
{

public:

    ShuffleIdentifiersWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void ShuffleIdentifiersWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<ShuffleIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<ShuffleIdentifiersParameters>());

    std::random_device rd;

    std::mt19937 g(rd());

    std::shuffle(parameters->selectedVariableIdentifiers.begin(), parameters->selectedVariableIdentifiers.end(), g);

    QUuid uuidProgress = this->startProgress("shuffeling identifiers", 0, parameters->selectedVariableIndexes.count());

    for (qsizetype i = 0; i< parameters->selectedVariableIndexes.count(); ++i) {

        annotatedMatrix->setIdentifier(parameters->selectedVariableIndexes.at(i), parameters->selectedVariableIdentifiers.at(i), parameters->orientation);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

}

#endif // SHUFFLEIDENTIFIERSWORKER_H
