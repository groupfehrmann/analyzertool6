#ifndef REMOVEDUPLICATEIDENTIFIERSWORKER_H
#define REMOVEDUPLICATEIDENTIFIERSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class RemoveDuplicateIdentifiersParameters : public BaseParameters
{

public:

    RemoveDuplicateIdentifiersParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class RemoveDuplicateIdentifiersWorker : public BaseWorker
{

public:

    RemoveDuplicateIdentifiersWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void RemoveDuplicateIdentifiersWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<RemoveDuplicateIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<RemoveDuplicateIdentifiersParameters>());

    QList<qsizetype> indexesToRemove;

    QHash<QString, QList<qsizetype>> identifierToIndexes;

    for (qsizetype i = 0; i < parameters->selectedVariableIdentifiers.count(); ++i)
        identifierToIndexes[parameters->selectedVariableIdentifiers.at(i)].append(parameters->selectedVariableIndexes.at(i));

    QHashIterator<QString, QList<qsizetype>> it(identifierToIndexes);

    while (it.hasNext()) {

        it.next();

        if (it.value().count() > 1) {

            QList<qsizetype> temp = it.value();

            std::sort(temp.begin(), temp.end());

            this->reportMessage("identifier \"" + it.key() + "\" is present " + QString::number(temp.count()) + " times in the data, keeping data at index " + QString::number(temp.first()));

            for (qsizetype i = 1; i < temp.count(); ++i)
                indexesToRemove.append(temp.at(i));

        }

    }

    if (!indexesToRemove.isEmpty())
        annotatedMatrix->removeLists(indexesToRemove, parameters->orientation);

    this->reportMessage("number of identifiers removed : " + QString::number(indexesToRemove.count()));

    this->reportMessage("number of identifiers remaining : " + QString::number(annotatedMatrix->identifierCount(parameters->orientation)));

}

#endif // REMOVEDUPLICATEIDENTIFIERSWORKER_H
