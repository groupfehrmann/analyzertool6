#include <QList>

#include <limits>

#include "ludcmp.h"

template <class T>
void lnsrch(QList<double> &xold, const double &fold, QList<double> &g, QList<double> &p, QList<double> &x, double &f, const double &stpmax, bool &check, T &func)
{
    const double ALF = 1.0e-4;

    const double TOLX = std::numeric_limits<double>::epsilon();

    qsizetype n = xold.count();

    check = false;

    double sum = 0.0;

    for (qsizetype i = 0; i < n; ++i)
        sum += p.at(i) * p.at(i);

    sum = std::sqrt(sum);

    if (sum > stpmax) {

        for (qsizetype i = 0; i < n; ++i)
            p[i] *= stpmax / sum;

    }

    double slope = 0.0;

    for (qsizetype i = 0; i < n; ++i)
        slope += g[i] * p[i];

    if (slope >= 0.0)
        throw("Roundoff problem in lnsrch.");

    double test = 0.0;

    for (qsizetype i = 0; i < n; ++i) {

        double temp = std::abs(p.at(i)) / std::max(std::abs(xold.at(i)), 1.0);

        if (temp > test)
            test = temp;

	}

    double alamin = TOLX / test;

    double alam = 1.0;

    double alam2 = 0.0;

    double tmplam = 0.0;

    double f2 = 0.0;

	for (;;) {

        for (qsizetype i = 0; i < n; ++i)
            x[i] = xold.at(i) + alam * p.at(i);

        f = func(x);

		if (alam < alamin) {

            for (qsizetype i = 0; i < n; ++i)
                x[i] = xold.at(i);

            check = true;

			return;

        } else if (f <= fold + ALF * alam * slope)
            return;
		else {

            if (alam == 1.0)
                tmplam = -slope / (2.0 * (f - fold - slope));
			else {

                double rhs1 = f - fold - alam * slope;

                double rhs2 = f2 - fold - alam2 * slope;

                double a = (rhs1 / (alam * alam) - rhs2 / (alam2 * alam2)) / (alam - alam2);

                double b = (-alam2 * rhs1 / (alam * alam) + alam * rhs2 / (alam2 * alam2)) / (alam - alam2);

                if (a == 0.0)
                    tmplam = -slope / (2.0 * b);
				else {

                    double disc = b * b - 3.0 * a * slope;

                    if (disc < 0.0)
                        tmplam = 0.5 * alam;
                    else if (b <= 0.0)
                        tmplam = (-b + std::sqrt(disc)) / (3.0 * a);
                    else tmplam =- slope / (b + std::sqrt(disc));

				}

                if (tmplam > 0.5 * alam)
                    tmplam = 0.5 * alam;

			}

		}

        alam2 = alam;

		f2 = f;

        alam = std::max(tmplam, 0.1 * alam);

	}

}

template <class T>
struct NRfdjac {

    const double EPS;

    T &func;

    NRfdjac(T &funcc) : EPS(1.0e-8), func(funcc) {}

    QList<QList<double>> operator() (QList<double> &x, QList<double> &fvec) {

        qsizetype n = x.count();

        QList<QList<double>> df(n, QList<double>(n, 0.0));

        QList<double> xh = x;

        for (qsizetype j = 0; j < n; ++j) {

            double temp = xh.at(j);

            double h = EPS * std::abs(temp);

            if (h == 0.0)
                h = EPS;

            xh[j] = temp + h;

            h = xh.at(j) - temp;

            QList<double> f = func(xh);

            xh[j] = temp;

            for (qsizetype i = 0; i < n; ++i)
                df[i][j] = (f[i] - fvec[i]) / h;

		}

		return df;

    }

};

template <class T>
struct NRfmin
{

    QList<double> fvec;

    T &func;

	NRfmin(T &funcc) : func(funcc){}

    double operator() (QList<double> &x) {

        double sum = 0;

        fvec = func(x);

        for (qsizetype i = 0; i < x.count(); ++i)
            sum += std::sqrt(fvec.at(i));

        return 0.5 * sum;

	}

};

template <class T>
void newt(QList<double> &x, bool &check, T &vecfunc)
{

    const qsizetype MAXITS = 200;

    const double TOLF = 1.0e-8;

    const double TOLMIN = 1.0e-12;

    const double STPMX = 100.0;

    const double TOLX = std::numeric_limits<double>::epsilon();

    qsizetype n = x.count();

    QList<double> g(n), p(n), xold(n);

    QList<QList<double>> fjac(n, QList<double>(n, 0.0));

    NRfmin<T> fmin(vecfunc);

    NRfdjac<T> fdjac(vecfunc);

    QList<double> &fvec = fmin.fvec;

    double f = fmin(x);

    double test = 0.0;

    for (qsizetype i = 0; i < n; ++i) {

        if (std::abs(fvec.at(i)) > test)
            test = std::abs(fvec.at(i));

    }

    if (test < 0.01 * TOLF) {

        check = false;

        return;

	}

    double sum = 0.0;

    for (qsizetype i = 0; i < n; ++i)
        sum += std::sqrt(x.at(i));

    double stpmax = STPMX * std::max(std::sqrt(sum), static_cast<double>(n));

    for (qsizetype its = 0; its < MAXITS; ++its) {

        fjac = fdjac(x, fvec);

        for (qsizetype i = 0; i < n; ++i) {

            sum = 0.0;

            for (qsizetype j = 0; j < n; ++j)
                sum += fjac.at(j).at(i) * fvec.at(j);

            g[i] = sum;

        }

        for (qsizetype i = 0; i < n; ++i)
            xold[i] = x.at(i);

        double fold = f;

        for (qsizetype i = 0; i < n; ++i)
            p[i] = -fvec.at(i);

        LUdcmp alu(fjac);

        alu.solve(p, p);

        lnsrch(xold, fold, g, p, x, f, stpmax, check, fmin);

        test = 0.0;

        for (qsizetype i = 0; i < n; ++i) {

            if (std::abs(fvec.at(i)) > test)
                test = std::abs(fvec.at(i));

        }

		if (test < TOLF) {

            check = false;

            return;

		}

		if (check) {

            test = 0.0;

            double den = std::max(f, 0.5 * n);

            for (qsizetype i = 0; i < n; ++i) {

                double temp = std::abs(g.at(i)) * std::max(std::abs(x.at(i)), 1.0) / den;

                if (temp > test)
                    test = temp;

            }

            check = (test < TOLMIN);

            return;

		}

        test = 0.0;

        for (qsizetype i = 0; i < n; ++i) {

            double temp= (std::abs(x.at(i) - xold.at(i))) / std::max(std::abs(x.at(i)), 1.0);

            if (temp > test)
                test = temp;

		}

		if (test < TOLX)
			return;

	}

	throw("MAXITS exceeded in newt");

}

