#include "saveprojectdataresultworker.h"

SaveProjectDataResultParameters::SaveProjectDataResultParameters(QObject *parent) :
    BaseParameters(parent, "saveProjectDataResult")
{

}

bool SaveProjectDataResultParameters::isValid_()
{

    return true;

}

QString SaveProjectDataResultParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[path to file = \"" << pathToFile << "\"]";

    return prettyPrintString;

}

SaveProjectDataResultWorker::SaveProjectDataResultWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("save project/data/result"), parameters, "saveProjectDataResult")
{

}

void SaveProjectDataResultWorker::doWork_()
{

    QSharedPointer<SaveProjectDataResultParameters> parameters(d_data->d_parameters.dynamicCast<SaveProjectDataResultParameters>());

    QFile fileOut(parameters->pathToFile);

    if (!fileOut.open(QIODevice::WriteOnly)) {

        this->reportError("could not open file \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    QDataStream out(&fileOut);

    out << QString("_ATool_begin#");

    out << (qint32)1;

    out << parameters->savingProject;

    out << parameters->projectLabel;

    QUuid uuidProgress = this->startProgress("saving to ATool file \"" + parameters->pathToFile + "\"", 0, parameters->objectPointers.count());

    for (qsizetype i = 0; i < parameters->objectPointers.count(); ++i) {

        this->reportMessage("adding " + parameters->objectPointers.at(i)->property("objectType").toString() + " with label \"" + parameters->objectPointers.at(i)->property("label").toString() + "\" and uuid \"" + parameters->objectPointers.at(i)->property("uuidObject").toString() + "\" to file \"" + parameters->pathToFile + "\"");

        if (parameters->objectPointers.at(i)->property("objectType").toString() == "annotated matrix") {

            QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointers.at(i));

            out << parameters->objectPointers.at(i)->property("objectType").toString();

            out << baseAnnotatedMatrix->typeName();

            out << *baseAnnotatedMatrix.data();

            if (!parameters->savingProject)
                emit this->addToRecentlyOpenedFiles("data", baseAnnotatedMatrix->property("label").toString(), parameters->pathToFile);

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->updateProgress(uuidProgress, i);

    }

    out << QString("_ATool_end#");

    if (parameters->savingProject)
        emit this->addToRecentlyOpenedFiles("project", parameters->projectLabel, parameters->pathToFile);

}

bool SaveProjectDataResultWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<SaveProjectDataResultParameters> parameters(d_data->d_parameters.dynamicCast<SaveProjectDataResultParameters>());

    for (qsizetype i = 0; i < parameters->objectPointers.count(); ++i) {

        QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointers.at(i));

        if (parameters->objectPointers.at(i)->property("objectType").toString() == "annotated matrix")
            baseAnnotatedMatrix->lockForRead();

    }

    return true;

}

void SaveProjectDataResultWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<SaveProjectDataResultParameters> parameters(d_data->d_parameters.dynamicCast<SaveProjectDataResultParameters>());

    for (qsizetype i = 0; i < parameters->objectPointers.count(); ++i) {

        QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointers.at(i));

        if (parameters->objectPointers.at(i)->property("objectType").toString() == "annotated matrix")
            baseAnnotatedMatrix->unlock();

    }

}
