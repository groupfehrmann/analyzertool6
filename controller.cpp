#include "controller.h"

Controller::Controller(QObject *parent) :
    QObject(parent),
    d_data(new ControllerData)
{

    d_data->d_runWorkersSequentially = true;

}

void Controller::setDataRepository(QSharedPointer<DataRepository> dataRepository)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_dataRepository = dataRepository;

}

void Controller::setLogMonitor(QSharedPointer<LogMonitor> logMonitor)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_logMonitor = logMonitor;

}

void Controller::setProgressMonitor(QSharedPointer<ProgressMonitor> progressMonitor)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_progressMonitor = progressMonitor;

}

void Controller::pauseWorker(const QUuid &uuidWorker)
{

    QReadLocker readLocker(&d_readWriteLock);

    if (!d_data->d_uuidWorkerToWorker.contains(uuidWorker))
        return;

    d_data->d_uuidWorkerToWorker.value(uuidWorker)->pauseWorker();

}

void Controller::startWorker(BaseWorker *worker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    worker->setParent(nullptr);

    if (d_data->d_logMonitor) {

        this->connect(worker, &BaseWorker::errorReported, d_data->d_logMonitor.data(), &LogMonitor::reportError);

        this->connect(worker, &BaseWorker::messageReported, d_data->d_logMonitor.data(), &LogMonitor::reportMessage);

        this->connect(worker, &BaseWorker::warningReported, d_data->d_logMonitor.data(), &LogMonitor::reportWarning);

    } else {

        this->connect(worker, &BaseWorker::errorReported, this, &Controller::errorReported);

        this->connect(worker, &BaseWorker::messageReported, this, &Controller::messageReported);

        this->connect(worker, &BaseWorker::warningReported, this, &Controller::warningReported);

    }

    if (d_data->d_progressMonitor) {

        this->connect(worker, &BaseWorker::workerStarted, d_data->d_progressMonitor.data(), &ProgressMonitor::startMonitoringWorker);

        this->connect(worker, &BaseWorker::workerInterrupted, d_data->d_progressMonitor.data(), &ProgressMonitor::stopAllMonitoringAssociatedWithUuid);

        this->connect(worker, &BaseWorker::workerFinished, d_data->d_progressMonitor.data(), &ProgressMonitor::stopAllMonitoringAssociatedWithUuid);

        this->connect(worker, &BaseWorker::progressStarted, d_data->d_progressMonitor.data(), &ProgressMonitor::startProgress);

        this->connect(worker, &BaseWorker::progressUpdated, d_data->d_progressMonitor.data(), &ProgressMonitor::updateProgress);

        this->connect(worker, &BaseWorker::progressLabelUpdated, d_data->d_progressMonitor.data(), &ProgressMonitor::updateProgressLabel);

        this->connect(worker, &BaseWorker::progressUpdatedWithOne, d_data->d_progressMonitor.data(), &ProgressMonitor::updateProgressWithOne);

        this->connect(worker, &BaseWorker::progressStopped, d_data->d_progressMonitor.data(), &ProgressMonitor::stopAllMonitoringAssociatedWithUuid);

    } else {

        this->connect(worker, &BaseWorker::workerStarted, this, &Controller::workerStarted);

        this->connect(worker, &BaseWorker::workerInterrupted, this, &Controller::workerInterrupted);

        this->connect(worker, &BaseWorker::workerFinished, this, &Controller::workerFinished);

        this->connect(worker, &BaseWorker::progressStarted, this, &Controller::progressStarted);

        this->connect(worker, &BaseWorker::progressUpdated, this, &Controller::progressUpdated);

        this->connect(worker, &BaseWorker::progressLabelUpdated, this, &Controller::progressLabelUpdated);

        this->connect(worker, &BaseWorker::progressUpdatedWithOne, this, &Controller::progressUpdatedWithOne);

        this->connect(worker, &BaseWorker::progressStopped, this, &Controller::progressStopped);

    }

    if (d_data->d_dataRepository)
        this->connect(worker, &BaseWorker::objectAvailable, d_data->d_dataRepository.data(), &DataRepository::addObject);
    else
        this->connect(worker, &BaseWorker::objectAvailable, this, &Controller::objectAvailable);

    int delayInSeconds = 0;

    if (d_data->d_enableDelayMode) {

        QSettings settings;

        settings.beginGroup("controller");

        if (settings.contains("delayInSeconds"))
           delayInSeconds = settings.value("delayInSeconds").toInt();

        settings.endGroup();

        delayInSeconds = QInputDialog::getInt(nullptr, QCoreApplication::applicationName() + " - input dialog", "start worker with delay (seconds)", delayInSeconds, 0);

        settings.beginGroup("controller");

        settings.setValue("delayInSeconds", delayInSeconds);

        settings.endGroup();

        worker->setDelay(delayInSeconds);

    }

    QThread *workerThread = new QThread;

    QUuid uuidWorker = worker->uuid();

    d_data->d_uuidWorkerToWorkerThread.insert(uuidWorker, workerThread);

    d_data->d_uuidWorkerToWorker.insert(uuidWorker, worker);

    d_data->d_uuidWorkers.append(uuidWorker);


    this->connect(workerThread, &QThread::finished, [this, uuidWorker]() {this->removeFromController(uuidWorker);});

    this->connect(workerThread, &QThread::finished, worker, &QObject::deleteLater);

    this->connect(workerThread, &QThread::finished, workerThread, &QThread::deleteLater);

    this->connect(workerThread, &QThread::started, worker, &BaseWorker::doWork);

    worker->moveToThread(workerThread);

    emit this->workerQueued(worker->uuid(), worker->label(), delayInSeconds);

    if (d_data->d_runWorkersSequentially && (d_data->d_uuidWorkerToWorkerThread.count() > 1))
        this->connect(d_data->d_uuidWorkerToWorkerThread[d_data->d_uuidWorkers.at(d_data->d_uuidWorkers.indexOf(uuidWorker) - 1)], &QThread::finished, [workerThread](){workerThread->start();});
    else
        workerThread->start();

}

void Controller::removeFromController(const QUuid &uuidWorker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidWorkerToWorkerThread.remove(uuidWorker);

    d_data->d_uuidWorkerToWorker.remove(uuidWorker);

    d_data->d_uuidWorkers.removeOne(uuidWorker);

}

void Controller::resumeWorker(const QUuid &uuidWorker)
{

    QReadLocker readLocker(&d_readWriteLock);

    if (!d_data->d_uuidWorkerToWorker.contains(uuidWorker))
        return;

    d_data->d_uuidWorkerToWorker.value(uuidWorker)->resumeWorker();

}

void Controller::requestInterruption(const QUuid &uuidWorker)
{

    QReadLocker readLocker(&d_readWriteLock);

    if (!d_data->d_uuidWorkerToWorkerThread.contains(uuidWorker))
        return;

    d_data->d_uuidWorkerToWorkerThread.value(uuidWorker)->requestInterruption();

    if (!d_data->d_uuidWorkerToWorker.contains(uuidWorker))
        return;

    d_data->d_uuidWorkerToWorker.value(uuidWorker)->requestInterruption();

}

void Controller::runWorkersSequentially(bool value)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_runWorkersSequentially = value;

    if (value == false) {

        for (QThread *workerTread : d_data->d_uuidWorkerToWorkerThread.values())
            workerTread->start();

    }

}

void Controller::enableDelayMode(bool value)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_enableDelayMode = value;

}

Controller::~Controller()
{

}
