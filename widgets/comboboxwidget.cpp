#include "comboboxwidget.h"
#include "ui_comboboxwidget.h"

ComboBoxWidget::ComboBoxWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::ComboBoxWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

}

QComboBox *ComboBoxWidget::comboBox()
{

    return ui->comboBox;

}

QLabel *ComboBoxWidget::label()
{

    return ui->label;

}

void ComboBoxWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    if (settings.contains("currentIndex"))
        ui->comboBox->setCurrentIndex(settings.value("currentIndex").toInt());

    settings.endGroup();

    d_readWidgetSettings = true;

}

void ComboBoxWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    settings.setValue("currentIndex", ui->comboBox->currentIndex());

    settings.endGroup();

}

ComboBoxWidget::~ComboBoxWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
