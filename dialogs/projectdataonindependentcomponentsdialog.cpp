#include "projectdataonindependentcomponentsdialog.h"

ProjectDataOnIndependentComponentsDialog::ProjectDataOnIndependentComponentsDialog(BaseDialog *parent, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices) :
    BaseDialog(parent, "project independent components on new data", "projectdataonindependentcomponentsdialog"),
    d_baseAnnotatedMatrices(baseAnnotatedMatrices)
{

    BaseDialog::d_parameters = QSharedPointer<ProjectDataOnIndependentComponentsParameters>(new ProjectDataOnIndependentComponentsParameters);

    BaseDialog::initWidgets();

}

void ProjectDataOnIndependentComponentsDialog::initWidgets_()
{

    QList<QString> baseAnnotatedMatrixLabels;

    for (qsizetype i = 0; i < d_baseAnnotatedMatrices.count(); ++i)
        baseAnnotatedMatrixLabels.append(d_baseAnnotatedMatrices.at(i)->property("label").toString());


    ComboBoxWidget *comboBoxWidget_newData = new ComboBoxWidget(this, "comboBoxWidget_newData");

    this->connect(comboBoxWidget_newData->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->data = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_newData->label()->setText("select data");

    comboBoxWidget_newData->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_newData, 0, 0, 1, 6);


    d_orientationWidget_newData = new OrientationWidget(this, "d_orientationWidget_newData");

    d_orientationWidget_newData->label()->setText("select orientation of data");

    this->connect(d_orientationWidget_newData, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->orientationData = orientation;});

    d_orientationWidget_newData->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_newData, 1, 0, 1, 6);


    d_itemSelectorWidget_variablesNewData = new ItemSelectorWidget(this, "d_itemSelectorWidget_variablesNewData");

    d_itemSelectorWidget_variablesNewData->label()->setText("select variables in data");

    d_gridLayout->addWidget(d_itemSelectorWidget_variablesNewData, 2, 0, 1, 3);

    this->connect(d_itemSelectorWidget_variablesNewData->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiersData = itemLabels; static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexesData = itemIndexes;});


    d_itemSelectorWidget_itemsNewData = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemsNewData");

    d_itemSelectorWidget_itemsNewData->label()->setText("select items in data");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemsNewData, 2, 3, 1, 3);

    this->connect(d_itemSelectorWidget_itemsNewData->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiersData = itemLabels; static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexesData = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_independentComponents = new ComboBoxWidget(this, "comboBoxWidget_independentComponents");

    this->connect(comboBoxWidget_independentComponents->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->independentComponents = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_independentComponents->label()->setText("select independent components");

    comboBoxWidget_independentComponents->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_independentComponents, 3, 0, 1, 6);


    d_orientationWidget_independentComponents = new OrientationWidget(this, "d_orientationWidget_independentComponents");

    d_orientationWidget_independentComponents->label()->setText("select orientation of independent components");

    this->connect(d_orientationWidget_independentComponents, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->orientationIndependentComponents = orientation;});

    d_orientationWidget_independentComponents->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_independentComponents, 4, 0, 1, 6);


    d_itemSelectorWidget_variablesIndependentComponents = new ItemSelectorWidget(this, "d_itemSelectorWidget_variablesIndependentComponents");

    d_itemSelectorWidget_variablesIndependentComponents->label()->setText("select labels of independent components");

    d_gridLayout->addWidget(d_itemSelectorWidget_variablesIndependentComponents, 5, 0, 1, 3);

    this->connect(d_itemSelectorWidget_variablesIndependentComponents->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiersIndependentComponents = itemLabels; static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexesIndependentComponents = itemIndexes;});


    d_itemSelectorWidget_itemsIndependentComponents = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemsIndependentComponents");

    d_itemSelectorWidget_itemsIndependentComponents->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemsIndependentComponents, 5, 3, 1, 3);

    this->connect(d_itemSelectorWidget_itemsIndependentComponents->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiersIndependentComponents = itemLabels; static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexesIndependentComponents = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_flipICsOnSkewness = new ComboBoxWidget(this, "comboBoxWidget_flipICsOnSkewness");

    this->connect(comboBoxWidget_flipICsOnSkewness->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_flipICsOnSkewness](){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->flipICs = comboBoxWidget_flipICsOnSkewness->comboBox()->currentData().toBool();});

    comboBoxWidget_flipICsOnSkewness->label()->setText("flip independent components on skewness");

    comboBoxWidget_flipICsOnSkewness->comboBox()->addItem("enable", true);

    comboBoxWidget_flipICsOnSkewness->comboBox()->addItem("disable", false);

    comboBoxWidget_flipICsOnSkewness->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_flipICsOnSkewness, 6, 0, 1, 3);


    SpinBoxWidget *spinBoxWidget_blocksize = new SpinBoxWidget(this, "spinBoxWidget_blocksize");

    this->connect(spinBoxWidget_blocksize->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->blockSize = value;});

    spinBoxWidget_blocksize->label()->setText("blocksize");

    spinBoxWidget_blocksize->spinBox()->setRange(1, 1000000);

    spinBoxWidget_blocksize->spinBox()->setValue(1000);

    spinBoxWidget_blocksize->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_blocksize->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_blocksize, 6, 3, 1, 3);


    SpinBoxWidget *spinBoxWidget_numberOfPermutations = new SpinBoxWidget(this, "spinBoxWidget_numberOfPermutations");

    this->connect(spinBoxWidget_numberOfPermutations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->numberOfPermutations = value;});

    spinBoxWidget_numberOfPermutations->label()->setText("number of permutations");

    spinBoxWidget_numberOfPermutations->spinBox()->setRange(1, 1000000);

    spinBoxWidget_numberOfPermutations->spinBox()->setValue(1000);

    spinBoxWidget_numberOfPermutations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfPermutations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfPermutations, 7, 0, 1, 3);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_pValue_threshold = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_pValue_threshold");

    this->connect(doubleSpinBoxWidget_pValue_threshold->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->pValue_threshold = value;});

    doubleSpinBoxWidget_pValue_threshold->label()->setText("p-value threshold");

    doubleSpinBoxWidget_pValue_threshold->doubleSpinBox()->setRange(0.0, 1.0);

    doubleSpinBoxWidget_pValue_threshold->doubleSpinBox()->setValue(1.0);

    doubleSpinBoxWidget_pValue_threshold->doubleSpinBox()->setDecimals(10);

    doubleSpinBoxWidget_pValue_threshold->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_pValue_threshold->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_pValue_threshold, 7, 3, 1, 3);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 9, 0, 1, 6);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 10, 0, 1, 6);


    this->connect(d_orientationWidget_newData, &OrientationWidget::orientationChanged, this, &ProjectDataOnIndependentComponentsDialog::orientationNewDataChanged);

    this->connect(comboBoxWidget_newData->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientationNewDataChanged(d_orientationWidget_newData->orientation());});

    this->orientationNewDataChanged(d_orientationWidget_newData->orientation());


    this->connect(d_orientationWidget_independentComponents, &OrientationWidget::orientationChanged, this, &ProjectDataOnIndependentComponentsDialog::orientationIndependentComponentsChanged);

    this->connect(comboBoxWidget_independentComponents->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientationIndependentComponentsChanged(d_orientationWidget_independentComponents->orientation());});

    this->orientationIndependentComponentsChanged(d_orientationWidget_independentComponents->orientation());

}

void ProjectDataOnIndependentComponentsDialog::orientationNewDataChanged(Qt::Orientation orientation)
{

    ProjectDataOnIndependentComponentsParameters *principalComponentAnalysisParameters = static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variablesNewData->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->data->nonSelectedIdentifiers(orientation), principalComponentAnalysisParameters->data->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variablesNewData->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->data->identifiers(orientation, true), principalComponentAnalysisParameters->data->indexes(orientation, true));


    d_itemSelectorWidget_itemsNewData->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->data->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), principalComponentAnalysisParameters->data->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_itemsNewData->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->data->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), principalComponentAnalysisParameters->data->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void ProjectDataOnIndependentComponentsDialog::orientationIndependentComponentsChanged(Qt::Orientation orientation)
{

    ProjectDataOnIndependentComponentsParameters *principalComponentAnalysisParameters = static_cast<ProjectDataOnIndependentComponentsParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variablesIndependentComponents->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->independentComponents->nonSelectedIdentifiers(orientation), principalComponentAnalysisParameters->independentComponents->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variablesIndependentComponents->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->independentComponents->identifiers(orientation, true), principalComponentAnalysisParameters->independentComponents->indexes(orientation, true));


    d_itemSelectorWidget_itemsIndependentComponents->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->independentComponents->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), principalComponentAnalysisParameters->independentComponents->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_itemsIndependentComponents->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->independentComponents->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), principalComponentAnalysisParameters->independentComponents->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}
