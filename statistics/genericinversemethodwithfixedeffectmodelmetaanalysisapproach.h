#ifndef GENERICINVERSEMETHODWITHFIXEDEFFECTMODELMETAANALYSISAPPROACH_H
#define GENERICINVERSEMETHODWITHFIXEDEFFECTMODELMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/normal.hpp"
#include "boost/math/distributions/chi_squared.hpp"

#include "basemetaanalysisapproach.h"

class GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach();

    GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach(const QList<double> &effectSizes, const QList<double> &standardErrorOfEffectSizes, const std::function<double (const double &)> &backtransformEffectSizeFunction);

    virtual ~GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach() {}

private:

    QList<double> d_effectSizes;

    QList<double> d_standardErrorOfEffectSizes;

    double d_sumOfInverseVarianceWeights;

    double d_sumOfInverseVarianceWeightedEffectSizes;

    double d_sumOfInverseVarianceWeightedSquaredEffectSizes;

    double d_meanEffectSize;

    double d_standardErrorEffectSize;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // GENERICINVERSEMETHODWITHFIXEDEFFECTMODELMETAANALYSISAPPROACH_H
