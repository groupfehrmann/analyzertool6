#ifndef SETENRICHMENTANALYSISWORKER_H
#define SETENRICHMENTANALYSISWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>
#include <QRegularExpression>
#include <QSet>
#include <QMutex>

#include <tuple>
#include <random>
#include <algorithm>

#include "boost/math/distributions/normal.hpp"

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "statistics/studentttest.h"
#include "statistics/welchttest.h"
#include "statistics/mannwhitneyutest.h"
#include "statistics/kolmogorovsmirnovtest.h"
#include "statistics/fisherexact2x2test.h"
#include "statistics/fittingdistributions.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"

class SetEnrichmentAnalysisParameters : public BaseParameters
{

public:

    SetEnrichmentAnalysisParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString exportDirectory;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<QString> selectedResultItems;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QList<QString> pathToSetCollections;

    QString labelForMatching;

    qsizetype indexForMatching;

    QString testToDetermineEnrichment;

    double thresholdForFisherExact;

    qsizetype minimumSizeOfSet;

    qsizetype maximumSizeOfSet;

    bool enablePermutationMode;

    qsizetype numberOfPermutations;

    int numberOfThreadsToUse;

    QList<QString> annotationLabelsForItems;

    double falseDiscoveryRate;

    double confidenceLevel;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class SetEnrichmentAnalysisWorker : public BaseWorker
{

public:

    SetEnrichmentAnalysisWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    bool importSetCollections(const QString &path, const QHash<QString, qsizetype> &itemIdentifierToIndex, QList<QList<std::tuple<QString, QString, QList<QString>>>> &setCollectionToSetInfo, qsizetype minimumSizeOfSet, qsizetype maximumSizeOfSet);

};

template<typename T>
void SetEnrichmentAnalysisWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<SetEnrichmentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<SetEnrichmentAnalysisParameters>());

    // creating item identifier to index mapping
    ////////////////

    QUuid uuidProgress = this->startProgress("creating item identifier to index mapping", 0, parameters->selectedItemIndexes.count());

    QHash<QString, qsizetype> itemIdentifierToIndex;

    if (parameters->indexForMatching == -1) {

        for (qsizetype i = 0; i < parameters->selectedItemIndexes.count(); ++i) {

            if (itemIdentifierToIndex.contains(parameters->selectedItemIdentifiers.at(i))) {

                this->reportError("duplicate item identifier \"" + parameters->selectedItemIdentifiers.at(i) + "\" detected");

                this->requestInterruption();

                return;

            } else
                itemIdentifierToIndex.insert(parameters->selectedItemIdentifiers.at(i), i);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

    } else {

        for (qsizetype i = 0; i < parameters->selectedItemIndexes.count(); ++i) {

            QString itemIdentifier = annotatedMatrix->BaseAnnotatedMatrix::annotationValue(parameters->selectedItemIndexes.at(i), parameters->indexForMatching, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString();

            if (itemIdentifierToIndex.contains(itemIdentifier)) {

                this->reportError("duplicate item identifier \"" + itemIdentifier + "\" detected when annotations with label \"" + parameters->labelForMatching + "\" is used for matching");

                this->requestInterruption();

                return;

            } else
                itemIdentifierToIndex.insert(itemIdentifier, i);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

    }

    this->stopProgress(uuidProgress);


    if (itemIdentifierToIndex.count() <= 3) {

        this->reportError("not enough items remaining for analysis after item identifier to index mapping (n = " + QString::number(itemIdentifierToIndex.count()) + ")");

        this->requestInterruption();

        return;

    }

    ////////////////

    // importing set collections
    ////////////////

    QList<QString> setCollectionToPrettyPrintLabels;

    QList<QList<QString>> setCollectionToSetLabels;

    QList<QList<std::tuple<QString, QString, QList<QString>>>> setCollectionToSetInfo;

    QList<QList<QList<qsizetype>>> collectionToSetToCodedSetMembers;

    uuidProgress = this->startProgress("importing set collections", 0, parameters->pathToSetCollections.count());

    for (qsizetype i = 0; i < parameters->pathToSetCollections.count(); ++i) {

        setCollectionToPrettyPrintLabels.append(QFileInfo(parameters->pathToSetCollections.at(i)).baseName());

        if (!this->importSetCollections(parameters->pathToSetCollections.at(i), itemIdentifierToIndex, setCollectionToSetInfo, parameters->minimumSizeOfSet, parameters->maximumSizeOfSet)) {

            this->requestInterruption();

            return;

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        QUuid uuidProgress_1 = this->startProgress("matching items to set members for \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", 0, setCollectionToSetInfo.at(i).count());

        QList<QList<qsizetype>> setToCodedSetMembers;

        for (const std::tuple<QString, QString, QList<QString>> &setInfo : setCollectionToSetInfo.at(i)) {

            QList<qsizetype> codedSetMembers(parameters->selectedItemIndexes.count(), 1);

            for (const QString &member : std::get<2>(setInfo))
                codedSetMembers[itemIdentifierToIndex.value(member)] = 0;

            setToCodedSetMembers.append(codedSetMembers);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        collectionToSetToCodedSetMembers.append(setToCodedSetMembers);

        this->stopProgress(uuidProgress_1);


        uuidProgress_1 = this->startProgress("constructing pretty print set labels for \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", 0, setCollectionToSetInfo.last().count());

        QList<QString> setLabels;

        for (const std::tuple<QString, QString, QList<QString>> &set : setCollectionToSetInfo.at(i)) {

            setLabels.append(std::get<0>(set) + " -- " + std::get<1>(set));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        setCollectionToSetLabels.append(setLabels);

        this->stopProgress(uuidProgress_1);


        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // fetching data
    ////////////////

    QList<QList<double>> lists;

    uuidProgress = this->startProgress("fetching lists", 0, parameters->selectedVariableIndexes.count());

    for (qsizetype i = 0; i < parameters->selectedVariableIndexes.count(); ++i) {

        if (parameters->orientation == Qt::Vertical)
            lists.append(annotatedMatrix->template rowAt<double>(parameters->selectedVariableIndexes.at(i), parameters->selectedItemIndexes));
        else
            lists.append(annotatedMatrix->template columnAt<double>(parameters->selectedVariableIndexes.at(i), parameters->selectedItemIndexes));

        if (parameters->testToDetermineEnrichment == "Fisher exact")
            MathOperations::applyFunctor_elementWise_inplace(lists.last(), [&parameters](double &value){if (value >= parameters->thresholdForFisherExact) value = 1; else if (std::fabs(value) >= parameters->thresholdForFisherExact) value = 2; else value = 0;}, parameters->numberOfThreadsToUse);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // setting up enrichment
    ////////////////

    std::function<double (const QList<double> &, const QList<qsizetype> &)> enrichmentFunction;

    if (parameters->testToDetermineEnrichment == "Student T")
        enrichmentFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {StudentTTest studentTTest(list, codedSetMembers); return studentTTest.zTransformedPValue();};
    else if (parameters->testToDetermineEnrichment == "Welch T")
        enrichmentFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {WelchTTest welchT(list, codedSetMembers); return welchT.zTransformedPValue();};
    else if (parameters->testToDetermineEnrichment == "Mann-Whitney U")
        enrichmentFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {MannWhitneyUTest mannWhitneyUTest(list, codedSetMembers); return mannWhitneyUTest.zTransformedPValue();};
    else if (parameters->testToDetermineEnrichment == "Kolmogorov-Smirnov Z")
        enrichmentFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {KolmogorovSmirnovTest kolmogorovsmirnovZ(list, codedSetMembers); return kolmogorovsmirnovZ.zTransformedPValue();};
    else if (parameters->testToDetermineEnrichment == "Fisher exact")
        enrichmentFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {FisherExact2x2Test<double> fisherExact2x2Test1(list, codedSetMembers, 0.0, 1.0); FisherExact2x2Test<double> fisherExact2x2Test2(list, codedSetMembers, 0.0, 2.0); return ((fisherExact2x2Test1.zTransformedPValue() > fisherExact2x2Test2.zTransformedPValue()) ? fisherExact2x2Test1.zTransformedPValue() : -fisherExact2x2Test2.zTransformedPValue());};

    ////////////////

    // calculating enrichment
    ////////////////

    QList<double> enrichementSummary;

    uuidProgress = this->startProgress("processing set collections", 0, parameters->pathToSetCollections.count());

    for (qsizetype i = 0; i < parameters->pathToSetCollections.count(); ++i) {

        this->updateProgressLabel(uuidProgress, "processing set collection \"" + setCollectionToPrettyPrintLabels.at(i) + "\"");

        const QList<QList<qsizetype>> &setToCodedSetMembers(collectionToSetToCodedSetMembers.at(i));

        QUuid uuidProgress_1 = this->startProgress("calculating Z-score enrichment matrix", 0, setToCodedSetMembers.count() * lists.count());

        std::function<QList<double>(const QList<qsizetype> &)> mapFunctor = [this, &lists, &enrichmentFunction, &uuidProgress_1](const QList<qsizetype> &codedSetMembers) {

            QList<double> enrichments(lists.count());

            for (qsizetype j = 0; j < lists.count(); ++j) {

                enrichments[j] = enrichmentFunction(lists.at(j), codedSetMembers);

                if (this->thread()->isInterruptionRequested())
                    return enrichments;

                this->checkIfPauseWasRequested();

                this->updateProgressWithOne(uuidProgress_1);

            }

            return enrichments;

        };

        auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

            result.append(intermediateResult);

        };

        QList<double> enrichmentMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, setToCodedSetMembers, mapFunctor, std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress_1);


        if (parameters->selectedResultItems.contains("enrichment matrix per set collection")) {

            AnnotatedMatrix<double> *result_enrichmentMatrixPerSetCollection = new AnnotatedMatrix<double>(setCollectionToSetLabels.at(i), parameters->selectedVariableIdentifiers, enrichmentMatrix);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("enrichment matrix \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", result_enrichmentMatrixPerSetCollection));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_enrichmentMatrixPerSetCollection);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "enrichment_matrix_" + setCollectionToPrettyPrintLabels.at(i));

            }

        }

        QList<double> listToMultivariatePermutationThreshold;

        if (parameters->enablePermutationMode) {

            uuidProgress_1 = this->startProgress("transposing Z-score enrichment matrix", 0, 0);

            MathOperations::transposeInplace(enrichmentMatrix, lists.count(), parameters->numberOfThreadsToUse);

            this->stopProgress(uuidProgress_1);


            QUuid uuidProgress_1 = this->startProgress("performing permutations", 0, parameters->numberOfPermutations);

            std::random_device rd;

            std::mt19937 g(rd());

            QList<QList<double>> listToMultivariatePermutationDistribution(lists.count());

            QList<QList<double>> shuffledLists = lists;

            for (qsizetype j = 0; j < parameters->numberOfPermutations; ++j) {

                this->updateProgressLabel(uuidProgress_1, "performing permutation round " + QString::number(j + 1));

                QUuid uuidProgress_2 = this->startProgress("shuffling lists", 0, shuffledLists.count());

                for (qsizetype k = 0; k < shuffledLists.count(); ++k) {

                    QList<double> &shuffledList(shuffledLists[k]);

                    std::shuffle(shuffledList.begin(), shuffledList.end(), g);

                    while (shuffledList == lists.at(k))
                        std::shuffle(shuffledList.begin(), shuffledList.end(), g);

                    if (QThread::currentThread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    this->updateProgressWithOne(uuidProgress_2);

                }

                this->stopProgress(uuidProgress_2);


                uuidProgress_2 = this->startProgress("calculating permutation Z-score enrichment matrix", 0, setToCodedSetMembers.count() * lists.count());

                mapFunctor = [this, &shuffledLists, &enrichmentFunction, &uuidProgress_2](const QList<qsizetype> &codedSetMembers) {

                    QList<double> enrichments(shuffledLists.count());

                    for (qsizetype k = 0; k < shuffledLists.count(); ++k) {

                        enrichments[k] = std::abs(enrichmentFunction(shuffledLists.at(k), codedSetMembers));

                        if (this->thread()->isInterruptionRequested())
                            return enrichments;

                        this->checkIfPauseWasRequested();

                        this->updateProgressWithOne(uuidProgress_2);

                    }

                    return enrichments;

                };

                QList<double> permutationMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, setToCodedSetMembers, mapFunctor, std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->stopProgress(uuidProgress_2);



                uuidProgress_2 = this->startProgress("transposing permutation Z-score enrichment matrix", 0, 0);

                MathOperations::transposeInplace(permutationMatrix, lists.count(), parameters->numberOfThreadsToUse);

                this->stopProgress(uuidProgress_2);


                uuidProgress_2 = this->startProgress("obtaining mvp threshold", 0, 0);

                for (qsizetype k = 0; k < shuffledLists.count(); ++k) {

                    QList<std::pair<double, bool>> combinedZScores;

                    combinedZScores.reserve(2 * setToCodedSetMembers.count());

                    for (qsizetype l = 0; l < setToCodedSetMembers.count(); ++l) {

                        combinedZScores.append(std::pair<double, bool>(std::abs(enrichmentMatrix.at(k * setToCodedSetMembers.count() + l)), true));

                        combinedZScores.append(std::pair<double, bool>(permutationMatrix.at(k * setToCodedSetMembers.count() + l), false));

                    }

                    std::sort(combinedZScores.begin(), combinedZScores.end(), [](const std::pair<double, bool> &value1, const std::pair<double, bool> & value2){return value1.first > value2.first;});

                    qsizetype countEnrichement = 0;

                    qsizetype countPermutation = 0;

                    for (const std::pair<double, bool> &value : combinedZScores) {

                        if (value.second)
                            ++countEnrichement;
                        else
                            ++countPermutation;

                        if ((static_cast<double>(countPermutation) / static_cast<double>(countEnrichement)) > parameters->falseDiscoveryRate) {

                            listToMultivariatePermutationDistribution[k].append(value.first);

                            break;

                        }

                    }

                    if (QThread::currentThread()->isInterruptionRequested())
                        return;

                    this->checkIfPauseWasRequested();

                    this->updateProgress(uuidProgress_2, k + 1);

                }

                this->stopProgress(uuidProgress_2);


                this->updateProgress(uuidProgress_1, j + 1);

            }

            this->stopProgress(uuidProgress_1);

            for (QList<double> &multivariatePermutationDistribution : listToMultivariatePermutationDistribution) {

                std::sort(multivariatePermutationDistribution.begin(), multivariatePermutationDistribution.end(), std::less<double>());

                listToMultivariatePermutationThreshold.append(MathDescriptives::percentileOfPresortedList(multivariatePermutationDistribution, parameters->confidenceLevel));

            }

        }


        if (parameters->selectedResultItems.contains("enrichment summary")) {

            uuidProgress_1 = this->startProgress("determining enrichment summary per list", 0, 0);

            if (!parameters->enablePermutationMode)
                MathOperations::transposeInplace(enrichmentMatrix, lists.count(), parameters->numberOfThreadsToUse);

            QList<double> maximumEnrichment(lists.count());

            QList<double> mvpThresholds(lists.count());

            QList<double> nAboveMVPThreshold(lists.count());

            for (const std::pair<qsizetype, std::span<double>> &rowIndex_rowSpan : MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(enrichmentMatrix, setCollectionToSetLabels.at(i).count())) {

                maximumEnrichment[rowIndex_rowSpan.first] = *std::max_element(rowIndex_rowSpan.second.begin(), rowIndex_rowSpan.second.end(), [](const double &a, const double &b){return std::abs(a) < std::abs(b);});

                if (parameters->enablePermutationMode) {

                    double mvpThreshold = listToMultivariatePermutationThreshold.at(rowIndex_rowSpan.first);

                    mvpThresholds[rowIndex_rowSpan.first] = mvpThreshold;

                    nAboveMVPThreshold[rowIndex_rowSpan.first] = std::count_if(rowIndex_rowSpan.second.begin(), rowIndex_rowSpan.second.end(), [&mvpThreshold](double &value){return (std::abs(value) >= mvpThreshold);});

                }

            }

            enrichementSummary.append(maximumEnrichment);

            if (parameters->enablePermutationMode) {

                enrichementSummary.append(mvpThresholds);

                enrichementSummary.append(nAboveMVPThreshold);

            }

            this->stopProgress(uuidProgress_1);

        }


        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("enrichment summary")) {

        MathOperations::transposeInplace(enrichementSummary, lists.count(), parameters->numberOfThreadsToUse);

        QList<QString> labels;

        for (const QString &setCollectionLabel : setCollectionToPrettyPrintLabels) {

            labels.append(setCollectionLabel);

            if (parameters->enablePermutationMode) {

                labels.append("absolute mvp threshold (FDR = " + QString::number(parameters->falseDiscoveryRate) + ", CL = " + QString::number(parameters->confidenceLevel) + ")");

                labels.append("N above mvp threshold");

            }

        }

        AnnotatedMatrix<double> *result_enrichementSummary = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, labels, enrichementSummary);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("enrichment summary", result_enrichementSummary));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_enrichementSummary);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "enrichment_summary");

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

}

#endif // SETENRICHMENTANALYSISWORKER_H
