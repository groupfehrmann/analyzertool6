#ifndef DATAREPOSITORYMODEL_H
#define DATAREPOSITORYMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>
#include <QUuid>
#include <QBrush>

#include "datarepository.h"
#include "containers/result.h"

class DataRepositoryModel : public QAbstractItemModel
{

    Q_OBJECT

public:

    explicit DataRepositoryModel(QObject *parent = nullptr, const QSharedPointer<DataRepository> &dataRepository = nullptr);

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;

    QModelIndex index(const QUuid &uuid) const;

    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    const QUuid &currentUuid() const;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

public slots:

    QUuid addObject(const QUuid &uuidProject, QObject *object);

    QUuid addSharedPointerToObject(const QUuid &uuidProject, QSharedPointer<QObject> object);

    QUuid addProject();

    void removeAll();

    void removeAllDataAssociatedWithUuid(const QUuid &uuid);

    void convertToData(const QUuid &uuid);

    void setCurrentUuid(const QUuid &uuid);

private:

    QSharedPointer<DataRepository> d_dataRepository;

signals:

    void currentUuidChanged(const QUuid &uuid);

    void objectAdded(const QUuid &uuidObject, const QSharedPointer<QObject> &object);

    void objectRemoved(const QUuid &uuidObject);

};

#endif // DATAREPOSITORYMODEL_H
