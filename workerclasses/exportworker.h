#ifndef EXPORTWORKER_H
#define EXPORTWORKER_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QApplication>
#include <QSortFilterProxyModel>

//#include <utility>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "models/annotatedmatrixdatamodel.h"
#include "models/annotatedmatrixannotationsmodel.h"

class ExportParameters : public BaseParameters
{

public:

    ExportParameters(QObject *parent = nullptr);

    QSharedPointer<QObject> objectPointer;

    QString pathToFile;

    QString annotatedMatrixTabToExport;

    bool exportSelectedOnly;

    QString selectedFilter;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ExportWorker : public BaseWorker
{

public:

    ExportWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template <typename T> void doWork_annotatedMatrix(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    void doWork_result();

    void doWork_result_table();

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template <typename T>
void ExportWorker::doWork_annotatedMatrix(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    QSortFilterProxyModel *sortFilterProxyModel = nullptr;

    if (parameters->annotatedMatrixTabToExport == "data") {

        AnnotatedMatrixDataSortFilterProxyModel *annotatedMatrixDataSortFilterProxyModel = new AnnotatedMatrixDataSortFilterProxyModel(this);

        annotatedMatrixDataSortFilterProxyModel->setShowSelectedOnly(parameters->exportSelectedOnly);

        annotatedMatrixDataSortFilterProxyModel->setSourceModel(new AnnotatedMatrixDataModel(this, annotatedMatrix));

        sortFilterProxyModel = annotatedMatrixDataSortFilterProxyModel;

    } else if (parameters->annotatedMatrixTabToExport == "row annotations") {

        AnnotatedMatrixAnnotationsSortFilterProxyModel *annotatedMatrixAnnotationsSortFilterProxyModel = new AnnotatedMatrixAnnotationsSortFilterProxyModel(this, Qt::Vertical);

        annotatedMatrixAnnotationsSortFilterProxyModel->setShowSelectedOnly(parameters->exportSelectedOnly);

        annotatedMatrixAnnotationsSortFilterProxyModel->setSourceModel(new AnnotatedMatrixAnnotationsModel(this, annotatedMatrix, Qt::Vertical));

        sortFilterProxyModel = annotatedMatrixAnnotationsSortFilterProxyModel;

    } else if (parameters->annotatedMatrixTabToExport == "column annotations") {

        AnnotatedMatrixAnnotationsSortFilterProxyModel *annotatedMatrixAnnotationsSortFilterProxyModel = new AnnotatedMatrixAnnotationsSortFilterProxyModel(this, Qt::Horizontal);

        annotatedMatrixAnnotationsSortFilterProxyModel->setShowSelectedOnly(parameters->exportSelectedOnly);

        annotatedMatrixAnnotationsSortFilterProxyModel->setSourceModel(new AnnotatedMatrixAnnotationsModel(this, annotatedMatrix, Qt::Horizontal));

        sortFilterProxyModel = annotatedMatrixAnnotationsSortFilterProxyModel;

    }

    QFile fileOut(parameters->pathToFile);

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    QTextStream out(&fileOut);

    QString delimiter;

    QString startOfLine;

    QString endOfLine;

    if (parameters->selectedFilter == "tab-delimited (*.tsv)")
        delimiter = "\t";
    else if (parameters->selectedFilter == "comma-seperate values (*.csv)") {

        delimiter = "\",\"";

        startOfLine = "\"";

        endOfLine = "\"";

    }

    QUuid uuidProgress = this->startProgress("exporting to file \"" + parameters->pathToFile + "\"", 0, sortFilterProxyModel->rowCount());

    out << startOfLine;

    for (int i = 0; i < sortFilterProxyModel->columnCount(); ++i)
        out << delimiter << sortFilterProxyModel->headerData(i, Qt::Horizontal).toString();

    out << endOfLine << Qt::endl;

    for (int i = 0; i < sortFilterProxyModel->rowCount(); ++i) {

        out << startOfLine << sortFilterProxyModel->headerData(i, Qt::Vertical).toString();

        for (int j = 0; j < sortFilterProxyModel->columnCount(); ++j)
            out << delimiter << sortFilterProxyModel->data(sortFilterProxyModel->index(i, j)).toString();

        out << endOfLine << Qt::endl;

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

}

#endif // EXPORTWORKER_H
