#include "exportdialog.h"

ExportDialog::ExportDialog(QWidget *parent, QSharedPointer<QObject> objectPointer) :
    BaseDialog(parent, QString(), "exportdialog"),
    d_objectPointer(objectPointer)
{

    if (objectPointer->property("objectType").toString() == "annotated matrix")
        this->setWindowTitle(QCoreApplication::applicationName() + " - export data");
    else if (objectPointer->property("objectType").toString() == "result")
        this->setWindowTitle(QCoreApplication::applicationName() + " - " + "export result");
    else
        this->setWindowTitle(QCoreApplication::applicationName() + " - " + "FIX ME!!");

    ExportParameters *exportParameters = new ExportParameters;

    exportParameters->objectPointer = d_objectPointer;

    BaseDialog::d_parameters = QSharedPointer<ExportParameters>(exportParameters);

    BaseDialog::initWidgets();

}

void ExportDialog::initWidgets_()
{

    if (d_objectPointer->property("objectType").toString() == "annotated matrix") {

        SelectFileWidget *selectFileWidget = new SelectFileWidget(this, "selectFileWidgetAnnotatedMatrix", QFileDialog::AcceptSave);

        this->connect(selectFileWidget, &SelectFileWidget::pathChanged, [this](const QString &path){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->pathToFile = path;});

        this->connect(selectFileWidget, &SelectFileWidget::filterSelected, [this](const QString &filter){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->selectedFilter = filter;});

        selectFileWidget->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

        selectFileWidget->fileDialog()->setNameFilters({"tab-delimited (*.tsv)", "comma-seperate values (*.csv)"});

        selectFileWidget->fileDialog()->selectFile(d_objectPointer->property("label").toString());


        // set also file in filewidget itself with current directory

        selectFileWidget->fileDialog()->selectNameFilter("tab-delimited (*.tsv)");

        d_gridLayout->addWidget(selectFileWidget, 0, 0, 1, 2);


        ComboBoxWidget *comboBoxWidget_dataType = new ComboBoxWidget(this, "comboBoxWidget_annotatedMatrixTabToExport");

        this->connect(comboBoxWidget_dataType->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_dataType](){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->annotatedMatrixTabToExport = comboBoxWidget_dataType->comboBox()->currentText();});

        comboBoxWidget_dataType->label()->setText("select tab to export");

        comboBoxWidget_dataType->comboBox()->addItems({"data", "row annotations", "column annotations"});

        comboBoxWidget_dataType->readWidgetSettings();

        d_gridLayout->addWidget(comboBoxWidget_dataType, 1, 0, 1, 1);


        ComboBoxWidget *comboBoxWidget_exportSelectedOnly = new ComboBoxWidget(this, "comboBoxWidget_exportSelectedOnly");

        this->connect(comboBoxWidget_exportSelectedOnly->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_exportSelectedOnly](){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->exportSelectedOnly = comboBoxWidget_exportSelectedOnly->comboBox()->currentData().toBool();});

        comboBoxWidget_exportSelectedOnly->label()->setText("select export selection mode");

        comboBoxWidget_exportSelectedOnly->comboBox()->addItem("export selected only", true);

        comboBoxWidget_exportSelectedOnly->comboBox()->addItem("export all", false);

        comboBoxWidget_exportSelectedOnly->readWidgetSettings();

        d_gridLayout->addWidget(comboBoxWidget_exportSelectedOnly, 1, 1, 1, 1);

    }

    if ((d_objectPointer->property("objectType").toString() == "result") && (d_objectPointer->property("resultType").toString() == "table")) {

        SelectFileWidget *selectFileWidget = new SelectFileWidget(this, "selectFileWidgetResultTable", QFileDialog::AcceptSave);

        this->connect(selectFileWidget, &SelectFileWidget::pathChanged, [this](const QString &path){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->pathToFile = path;});

        this->connect(selectFileWidget, &SelectFileWidget::filterSelected, [this](const QString &filter){static_cast<ExportParameters *>(BaseDialog::d_parameters.data())->selectedFilter = filter;});

        selectFileWidget->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

        selectFileWidget->fileDialog()->setNameFilters({"tab-delimited (*.tsv)", "comma-seperate values (*.csv)"});

        selectFileWidget->fileDialog()->selectFile(d_objectPointer->property("label").toString());

        selectFileWidget->fileDialog()->selectNameFilter("tab-delimited (*.tsv)");

        d_gridLayout->addWidget(selectFileWidget, 0, 0, 1, 2);

    }

}
