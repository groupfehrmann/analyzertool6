#ifndef SHUFFLEDATAWORKER_H
#define SHUFFLEDATAWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class ShuffleDataParameters : public BaseParameters
{

public:

    ShuffleDataParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ShuffleDataWorker : public BaseWorker
{

public:

    ShuffleDataWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void ShuffleDataWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<ShuffleDataParameters> parameters(d_data->d_parameters.dynamicCast<ShuffleDataParameters>());

    QUuid uuidProgress = this->startProgress("shuffling data", 0, 0);

    QList<std::reference_wrapper<T>> vectorWithReferences;

    if (parameters->orientation == Qt::Vertical)
        vectorWithReferences = annotatedMatrix->sliced_references(parameters->selectedVariableIndexes, parameters->selectedItemIndexes);
    else
        vectorWithReferences = annotatedMatrix->sliced_references(parameters->selectedItemIndexes, parameters->selectedVariableIndexes);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    QList<T> vectorWithValues(vectorWithReferences.count());

    for (qsizetype i = 0; i < vectorWithReferences.count(); ++i)
        vectorWithValues[i] = vectorWithReferences.at(i);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    std::random_device rd;

    std::mt19937 g(rd());

    std::shuffle(vectorWithValues.begin(), vectorWithValues.end(), g);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    for (qsizetype i = 0; i < vectorWithReferences.count(); ++i)
        vectorWithReferences.at(i).get() = vectorWithValues.at(i);

    this->stopProgress(uuidProgress);

}

#endif // SHUFFLEDATAWORKER_H
