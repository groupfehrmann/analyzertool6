#ifndef PRINCIPALCOMPONENTANALYSISDIALOG_H
#define PRINCIPALCOMPONENTANALYSISDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/principalcomponentanalysisworker.h"

class PrincipalComponentAnalysisDialog : public BaseDialog
{

public:

    PrincipalComponentAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

    SpinBoxWidget *d_spinBoxWidget_thresholdMaximumNumberOfComponents;


private slots:

    void orientationChanged(Qt::Orientation orientation);

    void updateMaximumNumberOfComponents();

};

#endif // PRINCIPALCOMPONENTANALYSISDIALOG_H
