#ifndef DATAREPOSITORYWIDGET_H
#define DATAREPOSITORYWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QAction>
#include <QMenu>
#include <QItemSelectionModel>
#include <QTimer>

#include "models/datarepositorymodel.h"

namespace Ui {

    class DataRepositoryWidget;

}

class DataRepositoryWidget : public QWidget
{

    Q_OBJECT

public:

    explicit DataRepositoryWidget(QWidget *parent = nullptr);

    void setDataRepositoryModel(DataRepositoryModel *dataRepositoryModel);

    ~DataRepositoryWidget();

private:

    Ui::DataRepositoryWidget *ui;

};

#endif // DATAREPOSITORYWIDGET_H
