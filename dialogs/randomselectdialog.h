#ifndef RANDOMSELECTDIALOG_H
#define RANDOMSELECTDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/randomselectworker.h"

class RandomSelectDialog : public BaseDialog
{

public:

    RandomSelectDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};

#endif // RANDOMSELECTDIALOG_H
