#include "lancastersmetaanalysisapproach.h"

QList<QString> LancastersMetaAnalysisApproach::d_testDescriptiveLabels = {"Chi-squared", "df", "Sig. (2-tailed)"};

LancastersMetaAnalysisApproach::LancastersMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

LancastersMetaAnalysisApproach::LancastersMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr),
    d_pValues(pValues),
    d_sampleSizes(sampleSizes)
{

}

void LancastersMetaAnalysisApproach::calculateStatistic()
{

    d_statistic = 0;

    d_degreeOfFreedom = 0.0;

    for (qsizetype i = 0; i < d_pValues.count(); ++i) {

        double sampleSize = static_cast<double>(d_sampleSizes.at(i));

        boost::math::chi_squared dist(sampleSize);

        d_statistic += boost::math::quantile(complement(dist, d_pValues.at(i)));

        d_degreeOfFreedom += sampleSize;


    }

}

void LancastersMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_degreeOfFreedom);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void LancastersMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_degreeOfFreedom << d_pValue;

}
