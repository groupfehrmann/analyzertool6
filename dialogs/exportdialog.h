#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>

#include "basedialog.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectfilewidget.h"
#include "widgets/selectdelimiterwidget.h"
#include "workerclasses/exportworker.h"

class ExportDialog : public BaseDialog
{

    Q_OBJECT

public:

    ExportDialog(QWidget *parent = nullptr, QSharedPointer<QObject> objectPointer = nullptr);

protected:

    void initWidgets_() override;

private:

    QSharedPointer<QObject> d_objectPointer;
};

#endif // EXPORTDIALOG_H
