#ifndef TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSDIALOG_H
#define TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/transformsequencefunctionsbuilderwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/transformdatausingsequenceoffunctionsworker.h"

class TransformDataUsingSequenceOfFunctionsDialog : public BaseDialog
{

public:

    TransformDataUsingSequenceOfFunctionsDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};

#endif // TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSDIALOG_H
