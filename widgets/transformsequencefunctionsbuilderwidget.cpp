#include "transformsequencefunctionsbuilderwidget.h"
#include "math/johnsontransformation.h"
#include "ui_transformsequencefunctionsbuilderwidget.h"

TransformSequenceFunctionsBuilderWidget::TransformSequenceFunctionsBuilderWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::TransformSequenceFunctionsBuilderWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

    d_transformFunctionDescriptions << "shift: X = X + parameter1"; // 0

    d_transformFunctionDescriptions << "shift: X = X - parameter1"; // 1

    d_transformFunctionDescriptions << "shift: X = parameter1 - X"; // 2

    d_transformFunctionDescriptions << "scale: X = X * parameter1"; // 3

    d_transformFunctionDescriptions << "scale: X = X / parameter1"; // 4

    d_transformFunctionDescriptions << "scale: X = parameter1 / X"; // 5

    d_transformFunctionDescriptions << "standardize : X = (X - mean(X)) / (variance(X))"; // 6

    d_transformFunctionDescriptions << "shift : X = X - mean(X)"; // 7

    d_transformFunctionDescriptions << "scale: X = X / variance(X)"; // 8

    d_transformFunctionDescriptions << "standardize : X = (X - mean(X)) / (stddev(X))"; // 9

    d_transformFunctionDescriptions << "normalize : rank normalize"; // 10

    d_transformFunctionDescriptions << "transform : X = Log(X) with base parameter1"; // 11

    d_transformFunctionDescriptions << "transform : if (X <= parameter1) X = X else X = parameter2"; // 12

    d_transformFunctionDescriptions << "transform : if (X >= parameter1) X = X else X = parameter2"; // 13

    d_transformFunctionDescriptions << "transform : if ((X <= parameter1) OR (X >= parameter2)) X = X else X = parameter3"; // 14

    d_transformFunctionDescriptions << "transform : if ((X >= parameter1) AND (X <= parameter2)) X = X else X = parameter3"; // 15

    d_transformFunctionDescriptions << "transform: X = abs(X)"; // 16

    d_transformFunctionDescriptions << "standardize: X = X^2 / sum(vector^2)"; // 17

    d_transformFunctionDescriptions << "transform: X = Cox-box transformation"; // 18

    d_transformFunctionDescriptions << "standardize: X = X / mean(abs(vector))"; // 19

    d_transformFunctionDescriptions << "transform: X = ForcedSymmetrical(X - median(vector)))"; // 20

    d_transformFunctionDescriptions << "transform: X = JTransformation"; // 21

    QCompleter *completer = new QCompleter(d_transformFunctionDescriptions, this);

    completer->setCaseSensitivity(Qt::CaseInsensitive);

    completer->setFilterMode(Qt::MatchContains);

    this->connect(ui->comboBox_transformFunctions, &QComboBox::currentIndexChanged, this, &TransformSequenceFunctionsBuilderWidget::comboBox_transformFunctions_currentIndexChanged);

    ui->comboBox_transformFunctions->addItems(d_transformFunctionDescriptions);

    ui->comboBox_transformFunctions->setCompleter(completer);


    this->connect(ui->pushButton_add, &QPushButton::clicked, this, &TransformSequenceFunctionsBuilderWidget::pushButton_add_clicked);

    this->connect(ui->pushButton_removeSelected, &QPushButton::clicked, this, &TransformSequenceFunctionsBuilderWidget::pushButton_removeSelected_clicked);

}

void TransformSequenceFunctionsBuilderWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    if (settings.contains("currentIndex"))
        ui->comboBox_transformFunctions->setCurrentIndex(settings.value("currentIndex").toInt());

    settings.endGroup();

    d_readWidgetSettings = true;

}

void TransformSequenceFunctionsBuilderWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    settings.setValue("currentIndex", ui->comboBox_transformFunctions->currentIndex());

    settings.endGroup();

}

void TransformSequenceFunctionsBuilderWidget::pushButton_add_clicked()
{

    bool ok = false;

    qsizetype index = ui->comboBox_transformFunctions->currentIndex();

    QString transformFunctionDescriptions = d_transformFunctionDescriptions.at(index);

    QList<double> parameters;

    for (qsizetype i = 0; i < ui->tableWidget_parameters->rowCount(); ++i) {

        QLineEdit *lineEdit = static_cast<QLineEdit *>(ui->tableWidget_parameters->cellWidget(i, 1));

        parameters.append(lineEdit->text().toDouble(&ok));

        if (!ok)
            return;

        transformFunctionDescriptions = transformFunctionDescriptions.replace("parameter" + QString::number(i + 1), static_cast<QLineEdit *>(ui->tableWidget_parameters->cellWidget(i, 1))->text());

    }

    int newRowIndex = ui->tableWidget_transformFunctions->rowCount();

    ui->tableWidget_transformFunctions->setRowCount(ui->tableWidget_transformFunctions->rowCount() + 1);

    ui->tableWidget_transformFunctions->setItem(newRowIndex, 0, new QTableWidgetItem((newRowIndex == 0) ? "First apply" : "Then apply"));

    ui->tableWidget_transformFunctions->setItem(newRowIndex, 1, new QTableWidgetItem(transformFunctionDescriptions));

    ui->tableWidget_transformFunctions->resizeColumnToContents(0);

    ui->tableWidget_transformFunctions->resizeColumnToContents(1);


    d_selectedTransformFunctionDescriptionsWithParameterReplacement.append(transformFunctionDescriptions);

    switch (index) {

        case 0: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value += parameter1;});

            break;

        }

        case 1: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value -= parameter1;});

            break;

        }

        case 2: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value = parameter1 - value;});

            break;

        }

        case 3: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value *= parameter1;});

            break;

        }

        case 4: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value /= parameter1;});

            break;

        }

        case 5: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value = parameter1 / value;});

            break;

        }

        case 6: {

            d_selectedTransformFunctions.append([](QList<double> &vector){std::pair<double, double> meanAndVariance = boost::math::statistics::mean_and_sample_variance(vector); for (double &value : vector) value = (value - meanAndVariance.first) / meanAndVariance.second;});

            break;

        }

        case 7: {

            d_selectedTransformFunctions.append([](QList<double> &vector){double mean = boost::math::statistics::mean(vector); for (double &value : vector) value = value - mean;});

            break;

        }

        case 8: {

            d_selectedTransformFunctions.append([](QList<double> &vector){double sample_variance = boost::math::statistics::sample_variance(vector); for (double &value : vector) value = value / sample_variance;});

            break;

        }

        case 9: {

            d_selectedTransformFunctions.append([](QList<double> &vector){std::pair<double, double> meanAndVariance = boost::math::statistics::mean_and_sample_variance(vector); for (double &value : vector) value = (value - meanAndVariance.first) / std::sqrt(meanAndVariance.second);});

            break;

        }

        case 10: {

            d_selectedTransformFunctions.append([](QList<double> &vector){MathOperations::crankInplace(vector);});

            break;

        }

        case 11: {

            double parameter1 = parameters.at(0);

            d_selectedTransformFunctions.append([parameter1](QList<double> &vector){for (double &value : vector) value = std::log(value) / std::log(parameter1);});

            break;

        }

        case 12: {

            double parameter1 = parameters.at(0);

            double parameter2 = parameters.at(1);

            d_selectedTransformFunctions.append([parameter1, parameter2](QList<double> &vector){for (double &value : vector) if (value > parameter1) value = parameter2;});

            break;

        }

        case 13: {

            double parameter1 = parameters.at(0);

            double parameter2 = parameters.at(1);

            d_selectedTransformFunctions.append([parameter1, parameter2](QList<double> &vector){for (double &value : vector) if (value < parameter1) value = parameter2;});

            break;

        }

        case 14: {

            double parameter1 = parameters.at(0);

            double parameter2 = parameters.at(1);

            double parameter3 = parameters.at(2);

            d_selectedTransformFunctions.append([parameter1, parameter2, parameter3](QList<double> &vector){for (double &value : vector) if ((value > parameter1) && (value < parameter2)) value = parameter3;});

            break;

        }

        case 15: {

            double parameter1 = parameters.at(0);

            double parameter2 = parameters.at(1);

            double parameter3 = parameters.at(2);

            d_selectedTransformFunctions.append([parameter1, parameter2, parameter3](QList<double> &vector){for (double &value : vector) if ((value < parameter1) || (value > parameter2)) value = parameter3;});

            break;

        }

        case 16: {

            d_selectedTransformFunctions.append([](QList<double> &vector){for (double &value : vector) value = std::abs(value);});

            break;

        }

        case 17: {

            d_selectedTransformFunctions.append([](QList<double> &vector){double sum_squared = 0.0; for (const double &value : vector) sum_squared += value * value; for (double &value : vector) value = (value * value) / sum_squared;});

            break;

        }


        case 18: {

            d_selectedTransformFunctions.append([](QList<double> &vector){

                QList<double *> vectorPtr;

                vectorPtr.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    vectorPtr << &vector[i];

                std::sort(vectorPtr.begin(), vectorPtr.end(), [](double *value1, double *value2){ return *value1 < *value2;});

                QList<double> sortedVector;

                sortedVector.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    sortedVector << *vectorPtr.at(i);

                bool succes;

                MathOperations::cox_box_transformation(sortedVector, succes);

                for (qsizetype i = 0; i < vector.count(); ++i)
                    *vectorPtr[i] = sortedVector.at(i);

            });

            break;
        }

        case 19: {

            d_selectedTransformFunctions.append([](QList<double> &vector){double mean = 0.0; for (const double &value : vector) mean += std::abs(value); mean /= static_cast<double>(vector.count()); for (double &value : vector) value = value / mean;});

            break;

        }

        case 20: {

            d_selectedTransformFunctions.append([](QList<double> &vector){

                QList<double *> vectorPtr;

                vectorPtr.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    vectorPtr << &vector[i];

                std::sort(vectorPtr.begin(), vectorPtr.end(), [](double *value1, double *value2){ return *value1 < *value2;});

                QList<double> sortedVector;

                sortedVector.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    sortedVector << *vectorPtr.at(i);

                double dummy = 1.0;

                MathOperations::medianCenteredForcedSymmetric(sortedVector, dummy);

                for (qsizetype i = 0; i < vector.count(); ++i)
                    *vectorPtr[i] = sortedVector.at(i);


            });

            break;

        }

        case 21: {

            d_selectedTransformFunctions.append([](QList<double> &vector){

                QList<double *> vectorPtr;

                vectorPtr.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    vectorPtr << &vector[i];

                std::sort(vectorPtr.begin(), vectorPtr.end(), [](double *value1, double *value2){ return *value1 < *value2;});

                QList<double> sortedVector;

                sortedVector.reserve(vector.count());

                for (qsizetype i = 0; i < vector.count(); ++i)
                    sortedVector << *vectorPtr.at(i);

                std::span<double> span(sortedVector.data(), sortedVector.count());

                JohnsonTransformation::Parameters p = JohnsonTransformation::getTransformParameters(span, sortedVector.last(), false, std::pair<double, double>(0.0001, 12), 10001);

                JohnsonTransformation::transform(span, p);

                for (qsizetype i = 0; i < vector.count(); ++i)
                    *vectorPtr[i] = sortedVector.at(i);

            });

            break;
        }

        default: return;

    }

    emit this->transformSequenceChanged();

}

void TransformSequenceFunctionsBuilderWidget::pushButton_removeSelected_clicked()
{

    QModelIndexList list = ui->tableWidget_transformFunctions->selectionModel()->selectedRows();

    for (int i = list.size() - 1; i >= 0; --i)
        ui->tableWidget_transformFunctions->removeRow(list.at(i).row());

    if (ui->tableWidget_transformFunctions->rowCount() >= 1)
        ui->tableWidget_transformFunctions->item(0, 0)->setText("First apply");

    emit this->transformSequenceChanged();

}

void TransformSequenceFunctionsBuilderWidget::comboBox_transformFunctions_currentIndexChanged(qsizetype index)
{

    for (int i = ui->tableWidget_parameters->rowCount() - 1; i >= 0; --i)
        ui->tableWidget_parameters->removeRow(i);

    switch (index) {

        case 0: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 1: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 2: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 3: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 4: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 5: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 11: {

            ui->tableWidget_parameters->setRowCount(1);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit = new QLineEdit;

            lineEdit->setValidator(new QDoubleValidator(std::numeric_limits<double>::min(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max_digits10, lineEdit));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit);

            break;

        }

        case 12: {

            ui->tableWidget_parameters->setRowCount(2);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit1 = new QLineEdit;

            lineEdit1->setValidator(new QDoubleValidator(lineEdit1));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit1);

            ui->tableWidget_parameters->setItem(1, 0, new QTableWidgetItem("parameter2"));

            QLineEdit *lineEdit2 = new QLineEdit;

            lineEdit2->setValidator(new QDoubleValidator(lineEdit2));

            ui->tableWidget_parameters->setCellWidget(1, 1, lineEdit2);

            break;

        }

        case 13: {

            ui->tableWidget_parameters->setRowCount(2);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit1 = new QLineEdit;

            lineEdit1->setValidator(new QDoubleValidator(lineEdit1));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit1);

            ui->tableWidget_parameters->setItem(1, 0, new QTableWidgetItem("parameter2"));

            QLineEdit *lineEdit2 = new QLineEdit;

            lineEdit2->setValidator(new QDoubleValidator(lineEdit2));

            ui->tableWidget_parameters->setCellWidget(1, 1, lineEdit2);

            break;

        }

        case 14: {

            ui->tableWidget_parameters->setRowCount(3);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit1 = new QLineEdit;

            lineEdit1->setValidator(new QDoubleValidator(lineEdit1));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit1);

            ui->tableWidget_parameters->setItem(1, 0, new QTableWidgetItem("parameter2"));

            QLineEdit *lineEdit2 = new QLineEdit;

            lineEdit2->setValidator(new QDoubleValidator(lineEdit2));

            ui->tableWidget_parameters->setCellWidget(1, 1, lineEdit2);

            ui->tableWidget_parameters->setItem(2, 0, new QTableWidgetItem("parameter3"));

            QLineEdit *lineEdit3 = new QLineEdit;

            lineEdit3->setValidator(new QDoubleValidator(lineEdit3));

            ui->tableWidget_parameters->setCellWidget(2, 1, lineEdit3);

            break;

        }

        case 15: {

            ui->tableWidget_parameters->setRowCount(3);

            ui->tableWidget_parameters->setItem(0, 0, new QTableWidgetItem("parameter1"));

            QLineEdit *lineEdit1 = new QLineEdit;

            lineEdit1->setValidator(new QDoubleValidator(lineEdit1));

            ui->tableWidget_parameters->setCellWidget(0, 1, lineEdit1);

            ui->tableWidget_parameters->setItem(1, 0, new QTableWidgetItem("parameter2"));

            QLineEdit *lineEdit2 = new QLineEdit;

            lineEdit2->setValidator(new QDoubleValidator(lineEdit2));

            ui->tableWidget_parameters->setCellWidget(1, 1, lineEdit2);

            ui->tableWidget_parameters->setItem(2, 0, new QTableWidgetItem("parameter3"));

            QLineEdit *lineEdit3 = new QLineEdit;

            lineEdit3->setValidator(new QDoubleValidator(lineEdit3));

            ui->tableWidget_parameters->setCellWidget(2, 1, lineEdit3);

            break;

        }

        default: return;

    }

    ui->tableWidget_parameters->resizeColumnToContents(0);

    ui->tableWidget_parameters->resizeColumnToContents(1);

}

const QList<QString> &TransformSequenceFunctionsBuilderWidget::selectedTransformFunctionDescriptionsWithParameterReplacement()
{

    return d_selectedTransformFunctionDescriptionsWithParameterReplacement;

}

const QList<std::function<void (QList<double> &)>> &TransformSequenceFunctionsBuilderWidget::selectedTransformFunctions()
{

    return d_selectedTransformFunctions;

}

TransformSequenceFunctionsBuilderWidget::~TransformSequenceFunctionsBuilderWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
