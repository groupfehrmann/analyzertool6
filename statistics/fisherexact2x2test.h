#ifndef FISHEREXACT2X2TEST_H
#define FISHEREXACT2X2TEST_H

#include <QList>

#include "basestatisticaltest.h"

#include "boost/math/distributions/hypergeometric.hpp"

template <typename T>
class FisherExact2x2Test : public BaseStatisticalTest
{

public:

    FisherExact2x2Test();

    FisherExact2x2Test(const QList<T> &data, const QList<qsizetype> &codedSampleIdentifiers, const T &categoryLabel1, const T &categoryLabel2);

    virtual ~FisherExact2x2Test() {}

    QList<double> pointProbabilityDistribution();

private:

    QList<QList<unsigned int>> d_samples;

    unsigned int d_sampleSize;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

template <typename T>
qsizetype FisherExact2x2Test<T>::d_minimumNumberOfSamples = 2;

template <typename T>
qsizetype FisherExact2x2Test<T>::d_maximumNumberOfSamples = 2;

template <typename T>
qsizetype FisherExact2x2Test<T>::d_minimumNumberOfItemsPerSample = 0;

template <typename T>
bool FisherExact2x2Test<T>::d_effectSizeAvailable = false;

template <typename T>
QString FisherExact2x2Test<T>::d_effectSizeLabel = QString();

template <typename T>
QList<QString> FisherExact2x2Test<T>::d_testDescriptiveLabels = {"Fisher's Exact Threshold", "Sig. (2-tailed)"};

template <typename T>
std::function<double (const double &)> FisherExact2x2Test<T>::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

template <typename T>
FisherExact2x2Test<T>::FisherExact2x2Test() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

template <typename T>
FisherExact2x2Test<T>::FisherExact2x2Test(const QList<T> &data, const QList<qsizetype> &codedSampleIdentifiers, const T &categoryLabel1, const T &categoryLabel2) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_sampleSize = 0;

    d_samples = QList<QList<unsigned int>>(2, QList<unsigned int>(2, 0));

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1) {

            const T &value(data.at(i));

            if (value == categoryLabel1) {

                d_samples[codedSampleIdentifier][0]++;

                ++d_sampleSize;

            } else if(value == categoryLabel2) {

                d_samples[codedSampleIdentifier][1]++;

                ++d_sampleSize;

            }

        }

    }

}

template <typename T>
void FisherExact2x2Test<T>::calculateStatistic()
{

    QList<unsigned int> sample0 = d_samples.at(0);

    QList<unsigned int> sample1 = d_samples.at(1);

    boost::math::hypergeometric_distribution<double> distribution(sample0.at(0) + sample0.at(1), sample0.at(0) + sample1.at(0), d_sampleSize);

    d_statistic = boost::math::pdf<double>(distribution, sample0.at(0));

}

template <typename T>
void FisherExact2x2Test<T>::calculatePValue()
{

    this->calculateStatistic();

    QList<unsigned int> sample0 = d_samples.at(0);

    QList<unsigned int> sample1 = d_samples.at(1);

    boost::math::hypergeometric_distribution<double> distribution(sample0.at(0) + sample0.at(1), sample0.at(0) + sample1.at(0), d_sampleSize);

    double sumPValue = d_statistic;

    while ((sample0.at(0) > 0) && (sample1.at(1) > 1)) {

        sample0[0]--;

        sample0[1]++;

        sample1[0]++;

        sample1[1]--;

        double temp = boost::math::pdf<double>(distribution, sample0.at(0));

        if (temp <= d_statistic)
            sumPValue += temp;

    }

    sample0 = d_samples.at(0);

    sample1 = d_samples.at(1);

    while ((sample1.at(0) > 0) && (sample0.at(1) > 0)) {

        sample0[0]++;

        sample0[1]--;

        sample1[0]--;

        sample1[1]++;

        double temp = boost::math::pdf<double>(distribution, sample0.at(0));

        if (temp <= d_statistic)
            sumPValue += temp;
    }

    d_pValue = sumPValue;

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::min();

}

template <typename T>
void FisherExact2x2Test<T>::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_pValue;

}

template <typename T>
QList<double> FisherExact2x2Test<T>::pointProbabilityDistribution()
{

    QList<double> pointProbabilityDistribution;

    QList<unsigned int> sample0 = d_samples.at(0);

    QList<unsigned int> sample1 = d_samples.at(1);

    boost::math::hypergeometric_distribution<double> distribution(sample0.at(0) + sample0.at(1), sample0.at(0) + sample1.at(0), d_sampleSize);

    pointProbabilityDistribution.append(boost::math::pdf<double>(distribution, sample0.at(0)));

    while ((sample0.at(0) > 0) && (sample1.at(1) > 1)) {

        sample0[0]--;

        sample0[1]++;

        sample1[0]++;

        sample1[1]--;

        pointProbabilityDistribution.append(boost::math::pdf<double>(distribution, sample0.at(0)));

    }

    sample0 = d_samples.at(0);

    sample1 = d_samples.at(1);

    while ((sample1.at(0) > 0) && (sample0.at(1) > 0)) {

        sample0[0]++;

        sample0[1]--;

        sample1[0]--;

        sample1[1]++;

        pointProbabilityDistribution.append(boost::math::pdf<double>(distribution, sample0.at(0)));

    }

    std::sort(pointProbabilityDistribution.begin(), pointProbabilityDistribution.end(), std::less<double>());

    return pointProbabilityDistribution;

}

#endif // FISHEREXACT2X2TEST_H
