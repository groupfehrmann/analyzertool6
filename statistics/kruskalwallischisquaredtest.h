#ifndef KRUSKALWALLISCHISQUAREDTEST_H
#define KRUSKALWALLISCHISQUAREDTEST_H

#include <QList>

#include <boost/math/statistics/univariate_statistics.hpp>
#include "boost/math/distributions/normal.hpp"
#include "boost/math/distributions/chi_squared.hpp"
#include "boost/range/numeric.hpp"

#include "basestatisticaltest.h"
#include "math/mathoperations.h"

class KruskalWallisChiSquaredTest : public BaseStatisticalTest
{

public:

    KruskalWallisChiSquaredTest();

    KruskalWallisChiSquaredTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples);

    virtual ~KruskalWallisChiSquaredTest() {}

private:

    double d_degreeOfFreedom;

    double d_tiesCorrectionFactor;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // KRUSKALWALLISCHISQUAREDTEST_H
