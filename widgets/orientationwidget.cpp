#include "orientationwidget.h"
#include "ui_orientationwidget.h"

OrientationWidget::OrientationWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::OrientationWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

    d_buttonGroup.addButton(ui->checkBox_vertical, 0);

    d_buttonGroup.addButton(ui->checkBox_horizontal, 1);

    d_buttonGroup.setExclusive(true);

    this->connect(ui->checkBox_horizontal, &QCheckBox::toggled, [this](bool checked){if (checked) emit this->orientationChanged(Qt::Horizontal); else emit this->orientationChanged(Qt::Vertical);});

}

QLabel *OrientationWidget::label()
{

    return ui->label;

}

Qt::Orientation OrientationWidget::orientation() const
{

    return ui->checkBox_horizontal->isChecked() ? Qt::Horizontal : Qt::Vertical;

}

void OrientationWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/buttonGroup");

    if (settings.contains("checkedId"))
        d_buttonGroup.button(settings.value("checkedId").toInt())->setChecked(true);

    settings.endGroup();

    d_readWidgetSettings = true;

    emit this->orientationChanged(ui->checkBox_horizontal->isChecked() ? Qt::Horizontal : Qt::Vertical);

}

void OrientationWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/buttonGroup");

    settings.setValue("checkedId", d_buttonGroup.checkedId());

    settings.endGroup();

}

OrientationWidget::~OrientationWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
