#include "conversionfunctions.h"

bool ConversionFunctions::convert(const QString &string, short &value)
{

    bool ok;

    value = string.toShort(&ok);

    if (!ok)
        value = std::numeric_limits<short>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, unsigned short &value)
{

    bool ok;

    value = string.toUShort(&ok);

    if (!ok)
        value = std::numeric_limits<unsigned short>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, int &value)
{

    bool ok;

    value = string.toInt(&ok);

    if (!ok)
        value = std::numeric_limits<int>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, unsigned int &value)
{

    bool ok;

    value = string.toUInt(&ok);

    if (!ok)
        value = std::numeric_limits<unsigned int>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, long &value)
{

    bool ok;

    value = string.toLong(&ok);

    if (!ok)
        value = std::numeric_limits<long long>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, unsigned long &value)
{

    bool ok;

    value = string.toULong(&ok);

    if (!ok)
        value = std::numeric_limits<long long>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, long long &value)
{

    bool ok;

    value = string.toLongLong(&ok);

    if (!ok)
        value = std::numeric_limits<long long>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, unsigned long long &value)
{

    bool ok;

    value = string.toLongLong(&ok);

    if (!ok)
        value = std::numeric_limits<unsigned long long>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, float &value)
{

    bool ok;

    value = string.toFloat(&ok);

    if (!ok)
        value = std::numeric_limits<float>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, double &value)
{

    bool ok;

    value = string.toDouble(&ok);

    if (!ok)
        value = std::numeric_limits<double>::quiet_NaN();

    return ok;

}

bool ConversionFunctions::convert(const QString &string, long double &value)
{

    bool ok = true;

    try {

        value = std::stold(string.toStdString());

    }  catch (...) {

        ok = false;

        value = std::numeric_limits<double>::quiet_NaN();

    }


    return ok;

}

bool ConversionFunctions::convert(const QString &string, QString &value)
{

    value = string;

    return true;

}

bool ConversionFunctions::convert(const QString &string, unsigned char &value)
{

    if (string.isEmpty())
        value = char();
    else if (string.length() == 1)
        value = static_cast<unsigned char>(string.at(0).toLatin1());
    else
        return false;

    return true;

}

bool ConversionFunctions::convert(const QString &string, bool &value)
{

    value = QVariant(string).toBool();

    return true;

}

template <>
short ConversionFunctions::invalidValue(short temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<short>::quiet_NaN();

}

template <>
unsigned short ConversionFunctions::invalidValue(unsigned short temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<unsigned short>::quiet_NaN();

}

template <>
int ConversionFunctions::invalidValue(int temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<int>::quiet_NaN();

}

template <>
unsigned int ConversionFunctions::invalidValue(unsigned int temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<unsigned int>::quiet_NaN();

}

template <>
long ConversionFunctions::invalidValue(long temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<long>::quiet_NaN();

}

template <>
unsigned long ConversionFunctions::invalidValue(unsigned long temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<unsigned long>::quiet_NaN();

}

template <>
long long ConversionFunctions::invalidValue(long long temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<long long>::quiet_NaN();

}

template <>
unsigned long long ConversionFunctions::invalidValue(unsigned long long temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<unsigned long long>::quiet_NaN();

}

template <>
float ConversionFunctions::invalidValue(float temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<float>::quiet_NaN();

}

template <>
double ConversionFunctions::invalidValue(double temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<double>::quiet_NaN();

}

template <>
long double ConversionFunctions::invalidValue(long double temp)
{

    Q_UNUSED(temp);

    return std::numeric_limits<long double>::quiet_NaN();

}

template <>
QString ConversionFunctions::invalidValue(QString temp)
{

    Q_UNUSED(temp);

    return QString();

}

template <>
unsigned char ConversionFunctions::invalidValue(unsigned char temp)
{

    Q_UNUSED(temp);

    return uchar();

}

template <>
bool ConversionFunctions::invalidValue(bool temp)
{

    Q_UNUSED(temp);

    return false;

}
