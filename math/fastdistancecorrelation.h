#ifndef FASTDISTANCECORRELATION_H
#define FASTDISTANCECORRELATION_H

#include <QList>
#include <QObject>
#include <QThreadPool>
#include <QPointer>
#include <QtConcurrent>

#include <tuple>
#include <cmath>
#include <algorithm>
#include <vector>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include <boost/math/statistics/univariate_statistics.hpp>

//#include "mathdescriptives.h"
#include "mathoperations.h"
#include "mathutilityfunctions.h"
#include "workerclasses/baseworker.h"


namespace FastDistanceCorrelation {

    double fastDistanceCorrelation(const QList<double> &x, const QList<double> &y, bool unbiased = false);

    double fastDistanceCorrelation(const std::span<double> &x, const std::span<double> &y, bool unbiased = false);

    double fastDistanceCorrelation(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased = false);

    double fastDistanceCorrelation(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased = false);

    QList<double> fastDistanceCovariance(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased = false, bool allParameters = false);

    QList<double> fastDistanceCovariance(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool unbiased = false, bool allParameters = false);

    QList<double> fastDistanceCovarianceSums(const QList<double> &x, const QList<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool allSums = false);

    QList<double> fastDistanceCovarianceSums(const std::span<double> &x, const std::span<double> &y, const QList<std::tuple<double, qsizetype, qsizetype, double> > &xSortedOrderedRanked, const QList<std::tuple<double, qsizetype, qsizetype, double> > &ySortedOrderedRanked, const double &variance_x, const double &variance_y, QList<qsizetype> &powersOfTwo, qsizetype L, bool allSums = false);

    QList<double> partialSum2D(const QList<double> &x, const QList<double> &y, const QList<double> &c, const QList<std::tuple<double, qsizetype, qsizetype, double> > &x_orderStatistics_orderIndices_rank_cummulativeSum, const QList<std::tuple<double, qsizetype, qsizetype, double> > &y_orderStatistics_orderIndices_rank_cummulativeSum, QList<qsizetype> &powersOfTwo, qsizetype L);

    QList<double> binaryTreeSums(const QList<double> &y, const QList<double> &c, QList<qsizetype> &powersOfTwo, qsizetype L);

    bool sortAscendingOnFirstElementOfTupleAndThenOnSecondElementOfTuple(const std::tuple<double, qsizetype, qsizetype, double> &element1, const std::tuple<double, qsizetype, qsizetype, double> &element2);

    qsizetype ceil_(const double &x);

    QList<std::tuple<double, qsizetype, qsizetype, double> > createVector_orderStatistics_orderIndices_rank_cummulativeSum(const QList<double> vector);

    QList<std::tuple<double, qsizetype, qsizetype, double> > createVector_orderStatistics_orderIndices_rank_cummulativeSum(const std::span<double> vector);

}

class FastDistanceCorrrelationSymmetricMatrix : public QObject
{
    Q_OBJECT

public:

    explicit FastDistanceCorrrelationSymmetricMatrix(const QList<double> &matrix, qsizetype leadingDimension, double proportionOfItemsToUse = 1.0, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), QObject *parent = nullptr);

    const QList<double> &symmetricMatrix() const;

    void connectBaseWorker(BaseWorker *baseWorker);

    void setLabel(const QString &label);

private:

    QString d_label;

    QList<double> d_matrix;

    qsizetype d_leadingDimension;

    QList<double> d_symmetricMatrix;

    int d_numberOfThreadsToUse;

    int d_previousNumberOfThreadsToUse;

    QThreadPool d_threadPool;

    QPointer<BaseWorker> d_connectedBaseworker;

public slots:

    bool run();

signals:

    void errorReported(const QUuid &uuidWorker, const QString &error) const;

    void messageReported(const QUuid &uuidWorker, const QString &message) const;

    void progressStarted(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, std::size_t minimum, std::size_t maximum) const;

    void progressStopped(const QUuid &uuidProgress) const;

    void progressUpdated(const QUuid &uuidProgress, qsizetype value) const;

    void progressUpdatedWithOne(const QUuid &uuidProgress) const;

    void warningReported(const QUuid &uuidWorker, const QString &warning) const;

};


#endif // FASTDISTANCECORRELATION_H
