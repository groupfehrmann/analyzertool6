#ifndef PROGRESSMONITORWIDGET_H
#define PROGRESSMONITORWIDGET_H

#include <QWidget>
#include <QAbstractItemModel>
#include <QTreeView>
#include <QAbstractItemDelegate>
#include <QSettings>

#include "progressmonitor.h"
#include "models/progressmonitormodel.h"

namespace Ui {

    class ProgressMonitorWidget;

}

class ProgressMonitorWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ProgressMonitorWidget(QWidget *parent = nullptr);

    void setProgressMonitorModel(ProgressMonitorModel *progressMonitorModel);

    ~ProgressMonitorWidget();

private:

    Ui::ProgressMonitorWidget *ui;

};

#endif // PROGRESSMONITORWIDGET_H
