#ifndef ITEMSELECTORWIDGET_H
#define ITEMSELECTORWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QAbstractListModel>
#include <QVariant>
#include <QMimeData>
#include <QSortFilterProxyModel>
#include <QSettings>
#include <QUuid>
#include <QLineEdit>

#include <numeric>

#include "dialogs/basedialog.h"

class ItemSelectorModel : public QAbstractListModel
{

    Q_OBJECT

public:

    explicit ItemSelectorModel(QObject *parent = nullptr);

    void clear();

    QVariant data(const QModelIndex &index, int role) const;

    const QString &dragDropIdentifier() const;

    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    const QList<QString> &itemLabels() const;

    const QList<qsizetype> &itemIndexes() const;

    QMimeData *mimeData(const QModelIndexList &indexes) const;

    QStringList mimeTypes() const;

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    void setDragDropIdentifier(const QString &dragDropIdentifier);

    void setItemLabelsAndIndexes(const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes);

    void setItemLabelsAndIndexes(const QList<QString> &itemLabels);

    Qt::DropActions supportedDropActions() const;

private:

    QList<QString> d_itemLabels;

    QList<qsizetype> d_itemIndexes;

    QString d_dragDropIdentifier;

signals:

    void selectionChanged(const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes) const;

};


namespace Ui {

    class ItemSelectorWidget;

}

class ItemSelectorWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ItemSelectorWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    ItemSelectorModel *itemSelectorModel_notSelected();

    ItemSelectorModel *itemSelectorModel_selected();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~ItemSelectorWidget();

private:

    Ui::ItemSelectorWidget *ui;

    ItemSelectorModel *d_itemSelectorModel_notSelected;

    ItemSelectorModel *d_itemSelectorModel_selected;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

public slots:

    void clear();

};

#endif // ITEMSELECTORWIDGET_H
