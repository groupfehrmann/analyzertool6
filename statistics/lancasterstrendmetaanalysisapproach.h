#ifndef LANCASTERSTRENDMETAANALYSISAPPROACH_H
#define LANCASTERSTRENDMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/chi_squared.hpp"

#include "basemetaanalysisapproach.h"

class LancastersTrendMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    LancastersTrendMetaAnalysisApproach();

    LancastersTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~LancastersTrendMetaAnalysisApproach() {}

private:

    QList<double> d_statistics;

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    double d_degreeOfFreedom;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // LANCASTERSTRENDMETAANALYSISAPPROACH_H
