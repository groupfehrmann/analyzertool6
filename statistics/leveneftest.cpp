#include "leveneftest.h"

qsizetype LeveneFTest::d_minimumNumberOfSamples = 2;

qsizetype LeveneFTest::d_maximumNumberOfSamples = std::numeric_limits<qsizetype>::max();

qsizetype LeveneFTest::d_minimumNumberOfItemsPerSample = 2;

bool LeveneFTest::d_effectSizeAvailable = false;

QString LeveneFTest::d_effectSizeLabel = QString();

QList<QString> LeveneFTest::d_testDescriptiveLabels = {"Levene F", "df1", "df2", "Sig. (2-tailed)"};

std::function<double (const double &)> LeveneFTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

LeveneFTest::LeveneFTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

LeveneFTest::LeveneFTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_samples.resize(numberOfSamples);

    d_sampleSize = 0;

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1) {

            d_samples[codedSampleIdentifier].append(data.at(i));

            ++d_sampleSize;

        }

    }

}

void LeveneFTest::calculateStatistic()
{

    double w1 = 0.0;

    double z_ = 0.0;

    QVector<double> mean_i;

    for (qsizetype i = 0; i < d_samples.count(); ++i) {

        QList<double> &currentSample(d_samples[i]);

        double mean = boost::math::statistics::mean(currentSample);

        for (double &value : currentSample)
            value = std::abs(value - mean);

        double z_i = std::accumulate(currentSample.begin(), currentSample.end(), 0.0);

        z_ += z_i;

        z_i = z_i / static_cast<double>(currentSample.count());

        mean_i << z_i;

        for (const double &value : currentSample)
            w1 += ((value - z_i) * (value - z_i));
    }

    z_ = z_ / double(d_sampleSize);

    double w2 = 0.0;

    for (int i = 0; i < d_samples.size(); ++i)
        w2 += static_cast<double>(d_samples.value(i).count()) * ((mean_i.at(i) - z_) * (mean_i.at(i) - z_));

    d_statistic = ((double(d_sampleSize) - double(d_samples.size())) / double(d_samples.size() - 1.0)) * (w2 / w1);

}

void LeveneFTest::calculatePValue()
{

    this->calculateStatistic();

    d_df1 = d_samples.count() - 1.0;

    d_df2 = d_sampleSize - d_samples.size();

    if ((d_df1 < 0.0) || (d_df2 < 0.0) || (d_statistic < 0.0) || std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::fisher_f dist(d_df1, d_df2);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void LeveneFTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_df1 << d_df2 << d_pValue;

}

void LeveneFTest::calculateEffectSize()
{

    this->calculateStatistic();

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

void LeveneFTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}
