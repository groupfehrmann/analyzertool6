#ifndef ICAIMAGEANALYSISDIALOG_H
#define ICAIMAGEANALYSISDIALOG_H

#include "basedialog.h"
#include "workerclasses/icaimageanalysisworker.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/comboboxwidget.h"

class ICAImageAnalysisDialog : public BaseDialog
{

public:

    ICAImageAnalysisDialog(QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

};

#endif // ICAIMAGEANALYSISDIALOG_H
