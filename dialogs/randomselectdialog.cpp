#include "randomselectdialog.h"


RandomSelectDialog::RandomSelectDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "replace identifier with annotation value", "RandomSelectDialog")
{

    RandomSelectParameters *randomSelectParameters = new RandomSelectParameters;

    randomSelectParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<RandomSelectParameters>(randomSelectParameters);

    BaseDialog::initWidgets();

}

void RandomSelectDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 1);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    DoubleSpinBoxWidget *doubleSpinBoxWidget_proportionToSelect = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_proportionToSelect");

    this->connect(doubleSpinBoxWidget_proportionToSelect->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data())->proportionToSelect = value;});

    doubleSpinBoxWidget_proportionToSelect->label()->setText("proportion to select");

    doubleSpinBoxWidget_proportionToSelect->doubleSpinBox()->setRange(0.000001, 1.0);

    doubleSpinBoxWidget_proportionToSelect->doubleSpinBox()->setValue(0.5);

    doubleSpinBoxWidget_proportionToSelect->doubleSpinBox()->setDecimals(6);

    doubleSpinBoxWidget_proportionToSelect->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_proportionToSelect->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_proportionToSelect, 2, 0, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 3, 0, 1, 1);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &RandomSelectDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void RandomSelectDialog::orientationChanged(Qt::Orientation orientation)
{

    RandomSelectParameters *randomSelectParameters = static_cast<RandomSelectParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(randomSelectParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), randomSelectParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(randomSelectParameters->baseAnnotatedMatrix->identifiers(orientation, true), randomSelectParameters->baseAnnotatedMatrix->indexes(orientation, true));

}
