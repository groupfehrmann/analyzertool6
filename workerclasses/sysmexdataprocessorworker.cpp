#include "sysmexdataprocessorworker.h"

SysmexDataProcessorParameters::SysmexDataProcessorParameters(QObject *parent) :
    BaseParameters(parent, "sysmexdataprocessor")
{

}

bool SysmexDataProcessorParameters::isValid_()
{

    const char *typeName_data_patientInfo = data_patientInfo->typeName();

    const char *typeName_data_sysmexInfo = data_sysmexInfo->typeName();

    if (*typeName_data_patientInfo != *typeid(QString).name()) {

        d_lastError = "data containing patient information should be in string format";

        return false;

    }

    if (*typeName_data_sysmexInfo != *typeid(QString).name()) {

        d_lastError = "data containing sysmex information should be in string format";

    }

    if (selectedVariableIdentifiers_data_patientInfo.isEmpty()) {

        d_lastError = "no variables selected from patient information to include in export";

        return false;

    }

    if (selectedVariableIdentifiers_data_sysmexInfo.isEmpty()) {

        d_lastError = "no variables selected from sysmex information to include in export";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString SysmexDataProcessorParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[data containing patient information = \"" + data_patientInfo->property("label").toString() + "\" and uuid = \"" + data_patientInfo->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation containing patient information = " << ((orientation_data_patientInfo == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in patient information to include in export = [\"" << selectedVariableIdentifiers_data_patientInfo.first() << "\", " << selectedVariableIndexes_data_patientInfo.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers_data_patientInfo.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers_data_patientInfo.at(i) << "\", " << selectedVariableIndexes_data_patientInfo.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[variable label defining column with unique patient id in patient information = \"" << patientInfo_variableLabelDefiningColumnWithUniquePatientId << "\"]";

    stream << Qt::endl << "\t[variable label defining column with sysmex id in patient information = \"" << patientInfo_variableLabelDefiningColumnWithSysmexId << "\"]";

    stream << Qt::endl << "\t[data containing sysmex information = \"" + data_sysmexInfo->property("label").toString() + "\" and uuid = \"" + data_sysmexInfo->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation containing sysmex information = " << ((orientation_data_sysmexInfo == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in sysmex information to include in export = [\"" << selectedVariableIdentifiers_data_sysmexInfo.first() << "\", " << selectedVariableIndexes_data_sysmexInfo.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers_data_sysmexInfo.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers_data_sysmexInfo.at(i) << "\", " << selectedVariableIndexes_data_sysmexInfo.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[variable label defining column with sysmex id in sysmex information = \"" << sysmexInfo_variableLabelDefiningColumnWithSysmexId << "\"]";

    stream << Qt::endl << "\t[variable label defining column with sampling date in sysmex information = \"" << sysmexInfo_variableLabelDefiningColumnWithDate << "\"]";

    stream << Qt::endl << "\t[variable label defining column with sampling time in sysmex information = \"" << sysmexInfo_variableLabelDefiningColumnWithTime << "\"]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    return prettyPrintString;

}

SysmexDataProcessorWorker::SysmexDataProcessorWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("sysmex data processor"), parameters, "sysmexdataprocessor")
{

}

void SysmexDataProcessorWorker::doWork_()
{

    QSharedPointer<SysmexDataProcessorParameters> parameters(d_data->d_parameters.dynamicCast<SysmexDataProcessorParameters>());


    QSharedPointer<AnnotatedMatrix<QString>> data_sysmexInfo = qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(parameters->data_sysmexInfo);

    QSharedPointer<AnnotatedMatrix<QString>> data_patientInfo = qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(parameters->data_patientInfo);


    QList<qsizetype> patientInfo_indexes = data_patientInfo->indexes(BaseAnnotatedMatrix::switchOrientation(parameters->orientation_data_patientInfo));

    QList<qsizetype> sysmexInfo_indexes = data_sysmexInfo->indexes(BaseAnnotatedMatrix::switchOrientation(parameters->orientation_data_sysmexInfo));


    QHash<QString, QString> sysmexId_to_patientId;

    QUuid uuidProgress = this->startProgress("obtaining sysmexID to patientID mapping", 0, patientInfo_indexes.count());

    for (qsizetype i = 0; i < patientInfo_indexes.count(); ++i) {

        QList<QString> patientInfo = (parameters->orientation_data_patientInfo == Qt::Horizontal) ?
                    data_patientInfo->template sliced<QString>({patientInfo_indexes.at(i)}, {parameters->patientInfo_variableIndexDefiningColumnWithUniquePatientId, parameters->patientInfo_variableIndexDefiningColumnWithSysmexId}) :
                    data_patientInfo->template sliced<QString>({parameters->patientInfo_variableIndexDefiningColumnWithUniquePatientId, parameters->patientInfo_variableIndexDefiningColumnWithSysmexId}, {patientInfo_indexes.at(i)});

        sysmexId_to_patientId.insert(patientInfo.at(1), patientInfo.at(0));

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    QHash<QString, qsizetype> sysmexId_to_index;

    QHash<QString, QList<QPair<QString, QDateTime>>> patientId_to_listOfPair_sysmexId_dateTime;

    uuidProgress = this->startProgress("obtaining patientID to sysmexIDs mapping", 0, sysmexInfo_indexes.count());

    for (qsizetype i = 0; i < sysmexInfo_indexes.count(); ++i) {

        QList<QString> sysmexInfo = (parameters->orientation_data_sysmexInfo == Qt::Horizontal) ?
                    data_sysmexInfo->template sliced<QString>({sysmexInfo_indexes.at(i)}, {parameters->sysmexInfo_variableIndexDefiningColumnWithSysmexId, parameters->sysmexInfo_variableIndexDefiningColumnWithDate, parameters->sysmexInfo_variableIndexDefiningColumnWithTime}) :
                    data_sysmexInfo->template sliced<QString>({parameters->sysmexInfo_variableIndexDefiningColumnWithSysmexId, parameters->sysmexInfo_variableIndexDefiningColumnWithDate, parameters->sysmexInfo_variableIndexDefiningColumnWithTime}, {sysmexInfo_indexes.at(i)});

        sysmexId_to_index.insert(sysmexInfo.at(0), i);

        QDate date = QDate::fromString(sysmexInfo.at(1).trimmed(), "dd/MM/yyyy");

        if (!date.isValid())
            this->reportWarning("invalid data \"" + sysmexInfo.at(1) + "\" for sysmex sample \"" + sysmexInfo.at(0) + "\"");

        QTime time = QTime::fromString(sysmexInfo.at(2).trimmed(), "HH:mm:ss");

        if (!time.isValid())
            this->reportWarning("invalid time \"" + sysmexInfo.at(2) + "\" for sysmex sample \"" + sysmexInfo.at(0) + "\"");

        patientId_to_listOfPair_sysmexId_dateTime[sysmexId_to_patientId.value(sysmexInfo.at(0))].append(QPair<QString, QDateTime>(sysmexInfo.at(0), QDateTime(date, time)));

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    QHash<QString, qsizetype> sysmexId_to_rankInTime;

    uuidProgress = this->startProgress("obtaining sysmex sampling date and time", 0, patientId_to_listOfPair_sysmexId_dateTime.count());

    for (const QString &key : patientId_to_listOfPair_sysmexId_dateTime.keys()) {

        QList<QPair<QString, QDateTime>> &listOfPair_sysmexId_dateTime(patientId_to_listOfPair_sysmexId_dateTime[key]);

        std::sort(listOfPair_sysmexId_dateTime.begin(), listOfPair_sysmexId_dateTime.end(), [](const QPair<QString, QDateTime> &value1, const QPair<QString, QDateTime> &value2){return value1.second < value2.second;});

        for (qsizetype j = 0; j < listOfPair_sysmexId_dateTime.count(); ++j)
            sysmexId_to_rankInTime.insert(listOfPair_sysmexId_dateTime.at(j).first, j + 1);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    }

    this->stopProgress(uuidProgress);


    QList<QString> rowLabels_matrixForm;

    QList<QString> processedData_matrixForm;

    QList<QString> rowLabels_rowForm;

    QList<QString> processedData_rowForm;

    uuidProgress = this->startProgress("connecting patient and sysmex information", 0, patientId_to_listOfPair_sysmexId_dateTime.count());

    for (qsizetype i = 0; i < patientInfo_indexes.count(); ++i) {

        QList<QString> patientInfo = (parameters->orientation_data_patientInfo == Qt::Horizontal) ?
                    data_patientInfo->template sliced<QString>({patientInfo_indexes.at(i)}, {parameters->selectedVariableIndexes_data_patientInfo}) :
                    data_patientInfo->template sliced<QString>({parameters->selectedVariableIndexes_data_patientInfo}, {patientInfo_indexes.at(i)});

        QString patientInfo_sysmexId = (parameters->orientation_data_patientInfo == Qt::Horizontal) ?
                    data_patientInfo->rowAt(patientInfo_indexes.at(i)).at(parameters->patientInfo_variableIndexDefiningColumnWithSysmexId) :
                    data_patientInfo->rowAt(parameters->patientInfo_variableIndexDefiningColumnWithSysmexId).at(patientInfo_indexes.at(i));

        if (sysmexId_to_index.contains(patientInfo_sysmexId)) {

            QList<QString> sysmexInfo = (parameters->orientation_data_sysmexInfo == Qt::Horizontal) ?
                        data_sysmexInfo->template sliced<QString>({sysmexId_to_index.value(patientInfo_sysmexId)}, {parameters->selectedVariableIndexes_data_sysmexInfo}) :
                        data_sysmexInfo->template sliced<QString>({parameters->selectedVariableIndexes_data_sysmexInfo}, {patientInfo_indexes.at(i)});

            processedData_matrixForm << patientInfo << QString::number(sysmexId_to_rankInTime.value(patientInfo_sysmexId)) << sysmexInfo;

            rowLabels_matrixForm << "row_" + QString::number(rowLabels_matrixForm.count() + 1);

            QString sysmexInfo_date = (parameters->orientation_data_sysmexInfo == Qt::Horizontal) ?
                        data_sysmexInfo->rowAt(sysmexId_to_index.value(patientInfo_sysmexId)).at(parameters->sysmexInfo_variableIndexDefiningColumnWithDate) :
                        data_sysmexInfo->rowAt(parameters->sysmexInfo_variableIndexDefiningColumnWithDate).at(sysmexId_to_index.value(patientInfo_sysmexId));

            QString sysmexInfo_time = (parameters->orientation_data_sysmexInfo == Qt::Horizontal) ?
                        data_sysmexInfo->rowAt(sysmexId_to_index.value(patientInfo_sysmexId)).at(parameters->sysmexInfo_variableIndexDefiningColumnWithTime) :
                        data_sysmexInfo->rowAt(parameters->sysmexInfo_variableIndexDefiningColumnWithTime).at(sysmexId_to_index.value(patientInfo_sysmexId));

            for (qsizetype j = 0; j < sysmexInfo.count(); j++) {

                processedData_rowForm << patientInfo << sysmexInfo_date << sysmexInfo_time << QString::number(sysmexId_to_rankInTime.value(patientInfo_sysmexId)) << parameters->selectedVariableIdentifiers_data_sysmexInfo.at(j) << sysmexInfo.at(j);

                rowLabels_rowForm << "row_" + QString::number(rowLabels_rowForm.count() + 1);

            }

            if (this->thread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        } else
            this->reportWarning("could not find sysmex measurements for id \"" + patientInfo_sysmexId + "\"");


    }

    this->stopProgress(uuidProgress);


    QList<QString> headerLabels_matrixForm;

    headerLabels_matrixForm << parameters->selectedVariableIdentifiers_data_patientInfo;

    headerLabels_matrixForm << QString("sampling_rank");

    headerLabels_matrixForm << parameters->selectedVariableIdentifiers_data_sysmexInfo;

    AnnotatedMatrix<QString> *result_matrixForm = new AnnotatedMatrix<QString>(rowLabels_matrixForm, headerLabels_matrixForm, processedData_matrixForm);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(QUuid(), new Result("processedSysmexData_matrixForm", result_matrixForm));
    else {

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_matrixForm);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "processedSysmexData_matrixForm");

    }

    QList<QString> headerLabels_rowForm;

    headerLabels_rowForm << parameters->selectedVariableIdentifiers_data_patientInfo;

    headerLabels_rowForm << QString("sampling_date");

    headerLabels_rowForm << QString("sampling_time");

    headerLabels_rowForm << QString("sampling_rank");

    headerLabels_rowForm << QString("sysmex_label");

    headerLabels_rowForm << QString("sysmex_value");

    AnnotatedMatrix<QString> *result_rowForm = new AnnotatedMatrix<QString>(rowLabels_rowForm, headerLabels_rowForm, processedData_rowForm);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(QUuid(), new Result("processedSysmexData_rowForm", result_rowForm));
    else {

        QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_rowForm);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "processedSysmexData_rowForm");

    }

}

bool SysmexDataProcessorWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<SysmexDataProcessorParameters> parameters(d_data->d_parameters.dynamicCast<SysmexDataProcessorParameters>());

    parameters->data_patientInfo->lockForRead();

    parameters->data_sysmexInfo->lockForRead();

    return true;

}

void SysmexDataProcessorWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<SysmexDataProcessorParameters> parameters(d_data->d_parameters.dynamicCast<SysmexDataProcessorParameters>());

    parameters->data_patientInfo->unlock();

    parameters->data_sysmexInfo->unlock();

}

