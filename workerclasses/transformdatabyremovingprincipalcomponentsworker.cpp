#include "transformdatabyremovingprincipalcomponentsworker.h"

TransformDataByRemovingPrincipalComponentsParameters::TransformDataByRemovingPrincipalComponentsParameters(QObject *parent) :
    BaseParameters(parent, "transformdatabyremovingprinciplcomponents")
{

}

bool TransformDataByRemovingPrincipalComponentsParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString TransformDataByRemovingPrincipalComponentsParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[transform data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[threshold maximum cumulative explained variance = " << threshold_cumulativeExplainedVariance << "]";

    stream << Qt::endl << "\t[threshold maximum number of components = " << threshold_maximumNumberOfComponents << "]";

    stream << Qt::endl << "\t[mode = " << mode << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    if (!selectedResultItems.isEmpty()) {

        stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

        for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
            stream << ", [" << selectedResultItems.at(i) << "\"]";

        stream << "]";

    }

    return prettyPrintString;

}

TransformDataByRemovingPrincipalComponentsWorker::TransformDataByRemovingPrincipalComponentsWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("transform data by removing principal components"), parameters, "transformdatabyremovingprinciplcomponents")
{

}

void TransformDataByRemovingPrincipalComponentsWorker::doWork_()
{

    QSharedPointer<TransformDataByRemovingPrincipalComponentsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataByRemovingPrincipalComponentsParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("for transforming data, values in data need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool TransformDataByRemovingPrincipalComponentsWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<TransformDataByRemovingPrincipalComponentsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataByRemovingPrincipalComponentsParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    return true;

}

void TransformDataByRemovingPrincipalComponentsWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<TransformDataByRemovingPrincipalComponentsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataByRemovingPrincipalComponentsParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}
