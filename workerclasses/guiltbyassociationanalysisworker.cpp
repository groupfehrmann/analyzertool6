#include "guiltbyassociationanalysisworker.h"

GuiltByAssociationAnalysisParameters::GuiltByAssociationAnalysisParameters(QObject *parent) :
    BaseParameters(parent, "guiltbyassociationanalysis")
{

}

bool GuiltByAssociationAnalysisParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items to include in the barcode selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    if (pathToSetCollections.isEmpty()) {

        d_lastError = "no files containing sets with variables used for generating barcodes selected";

        return false;

    }

    if (annotationLabelsForVariables.isEmpty()) {

        d_lastError = "at least one annotation label for variables needs to be selected";

        return false;

    }

    return true;

}

QString GuiltByAssociationAnalysisParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[guilt-by-association analysis on data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[select items to include in the barcode = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[files containing sets with variables used for generating barcodes = [\"" << pathToSetCollections.first() << "\"]";

    for (qsizetype i = 1; i < pathToSetCollections.count(); ++i)
        stream << ", [\"" << pathToSetCollections.at(i) << "\"]";

    stream << "]";

    stream << Qt::endl << "\t[label for matching between variables and members of sets = \"" << labelForMatching << "\"]";

    stream << Qt::endl << "\t[barcode function = \"" << barcodeFunction << "\"]";

    if (barcodeFunction == "Fisher exact")
        stream << Qt::endl << "\t[threshold used for Fisher Exact = \"" << thresholdForFisherExact << "\"]";

    stream << Qt::endl << "\t[minimum size of set = " << minimumSizeOfSet << "]";

    stream << Qt::endl << "\t[maximum size of set = " << maximumSizeOfSet << "]";

    stream << Qt::endl << "\t[function to determining association between variable and barcode = \"" << associationFunction << "\"]";

    if (associationFunction == "rank-biased overlap") {

        stream << Qt::endl << "\t[ranked biased overlap : p = " << p_rankBiasedOverlap << "]";

        stream << Qt::endl << "\t[ranked biased overlap : maximum depth = " << maxDepth_rankBiasedOverlap << "]";

    }

    if (enablePermutationMode) {

        stream << Qt::endl << "\t[permutation mode is enabled with " << numberOfPermutations << " permutations]";

        stream << Qt::endl << "\t[false discovery rate to controle for in permutation mode " << falseDiscoveryRate <<"]";

        stream << Qt::endl << "\t[confidence level for multivariate threshold in permutation mode " << confidenceLevel <<"]";

    }

    stream << Qt::endl << "\t[annotation labels for items = [\"" << annotationLabelsForVariables.first() << "\"]";

    for (qsizetype i = 1; i < annotationLabelsForVariables.count(); ++i)
        stream << ", [\"" << annotationLabelsForVariables.at(i) << "\"]";


    stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

GuiltByAssociationAnalysisWorker::GuiltByAssociationAnalysisWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("guilt-by-association analysis"), parameters, "guiltbyassociationanalysis")
{

}

void GuiltByAssociationAnalysisWorker::doWork_()
{

    QSharedPointer<GuiltByAssociationAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<GuiltByAssociationAnalysisParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("values in data need to be numerical to perform guilt-by-association analysis");

        QThread::currentThread()->requestInterruption();

    }

}

bool GuiltByAssociationAnalysisWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<GuiltByAssociationAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<GuiltByAssociationAnalysisParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void GuiltByAssociationAnalysisWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<GuiltByAssociationAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<GuiltByAssociationAnalysisParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}

bool GuiltByAssociationAnalysisWorker::importSetCollections(const QString &path, const QHash<QString, qsizetype> &variableIdentifierToIndex, QList<QList<std::tuple<QString, QString, QSet<QString> > > > &setCollections_sets_identifier_description_members, qsizetype minimumSizeOfSet, qsizetype maximumSizeOfSet)
{

    QFile fileIn(path);

    if (!fileIn.exists() || !fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + path + "\"");

        QThread::currentThread()->requestInterruption();

        return false;

    }

    QUuid uuidProgress = this->startProgress("importing set collection \"" + path + "\"", 0, fileIn.size());

    qsizetype numberOfLinesRead = 0;

    QList<std::tuple<QString, QString, QSet<QString>>> sets_identifier_description_members;

    QSet<QString> setIdentifiers;

    while (!fileIn.atEnd()) {

        QList<QString> tokens = QString(fileIn.readLine()).remove(QRegularExpression("[\r\n]")).split(QRegularExpression("\t"), Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            this->reportError("in set collection file \"" + path + "\" at line number " + QString::number(numberOfLinesRead) + " not enough tokens could be read");

            fileIn.close();

            return false;

        }

        QSet<QString> setMembers;

        for (qsizetype i = 2; i < tokens.count(); ++i) {

            const QString &token(tokens.at(i));

            if (variableIdentifierToIndex.contains(token)) {

                if (setMembers.contains(token))
                    this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", set with identifier \"" + tokens.at(0) + "\" conatains duplicate members with identifier \"" + token + "\"");
                else
                    setMembers.insert(token);

            }

        }

        if ((setMembers.count() <= maximumSizeOfSet) && (setMembers.count() >= minimumSizeOfSet)) {

            if (setIdentifiers.contains(tokens.at(0))) {

                this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", duplicate set identifier \"" + tokens.at(0) + "\" was detected");

                fileIn.close();

                return false;

            } else {

                setIdentifiers.insert(tokens.at(0));

                sets_identifier_description_members.append({tokens.at(0), tokens.at(1), setMembers});

            }

        } else
            this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", set identifier \"" + tokens.at(0) + "\" with description \"" + tokens.at(1) + " was excluded as it did contain a valid number of members (" + QString::number(setMembers.count()) + ")");


        if (QThread::currentThread()->isInterruptionRequested())
            return false;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    this->stopProgress(uuidProgress);

    if (sets_identifier_description_members.isEmpty()) {

        this->reportError("remove \"" + path + "\" from set collections as there were no set definitions that met the criteria of a valid set");

        return false;

    }

    this->reportMessage("imported " + QString::number(sets_identifier_description_members.count()) + " set definitions from set collection file \"" + path + "\"");

    setCollections_sets_identifier_description_members.append(sets_identifier_description_members);

    return true;

}

double GuiltByAssociationAnalysisWorker::rankBiasedOverlap(const std::span<double> &span1, const std::span<double> &span2, qsizetype maxDepth, long double p)
{

    qsizetype k = std::max(span1.size(), span2.size());

    if (maxDepth < k)
        k = maxDepth;

    QSet<double> set1;

    if (k <= static_cast<qsizetype>(span1.size()))
        set1 = QSet<double>(span1.begin(), span1.begin() + k);
    else
        set1 = QSet<double>(span1.begin(), span1.end());

    QSet<double> set2;

    if (k <= static_cast<qsizetype>(span2.size()))
        set2 = QSet<double>(span2.begin(), span2.begin() + k);
    else
        set2 = QSet<double>(span2.begin(), span2.end());

    qsizetype x_k = set1.intersect(set2).count();

    long double summation = 0.0;

    qsizetype i = 1;

    while (k != i) {

        if (i <= static_cast<qsizetype>(span1.size()))
            set1 = QSet<double>(span1.begin(), span1.begin() + i);
        else
            set1 = QSet<double>(span1.begin(), span1.end());

        if (i <= static_cast<qsizetype>(span2.size()))
            set2 = QSet<double>(span2.begin(), span2.begin() + i);
        else
            set2 = QSet<double>(span2.begin(), span2.end());

        long double a_d = static_cast<double>(set1.intersect(set2).count()) / static_cast<double>(i);

        summation += std::pow(p, static_cast<long double>(i)) * a_d;

        ++i;

    }

    return ((static_cast<long double>(x_k) / static_cast<long double>(k)) * std::pow(p, static_cast<long double>(k))) + ( (static_cast<long double>(1) - p) / p * summation);

}
