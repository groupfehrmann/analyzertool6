#include "result.h"

class ResultData : public QSharedData
{

public:

    QSharedPointer<QObject> d_internalResultObject;

};

Result::Result(QObject *parent) :
    QObject(parent),
    d_data(new ResultData)
{

    this->setProperty("objectType", "result");

}

Result::Result(const QString &label, BaseAnnotatedMatrix *table, QObject *parent) :
    QObject(parent),
    d_data(new ResultData)
{

    this->setProperty("objectType", "result");

    this->setTable(label, table);

}

Result::Result(const Result &result) :
    QObject(result.parent()),
    d_data(result.d_data)
{

}

QSharedPointer<QObject> Result::internalObject()
{

    return d_data->d_internalResultObject;

}

Result &Result::operator=(const Result &result)
{

    if (this != &result)
        d_data.operator=(result.d_data);

    return *this;

}

QString Result::resultType() const
{

    if (d_data->d_internalResultObject)
        return d_data->d_internalResultObject->property("resultType").toString();

    return QString();

}

void Result::setTable(const QString &label, BaseAnnotatedMatrix *table)
{

    table->setProperty("objectType", "result");

    table->setProperty("resultType", "table");

    table->setProperty("label", label);

    this->setProperty("label", label);

    this->setProperty("resultType", "table");

    d_data->d_internalResultObject = QSharedPointer<QObject>(table);

}

Result::~Result()
{

}

QList<QString> Result::createIdentifiersWithSequentialNumbers(qsizetype numberOfIdentifiers, qsizetype startNumber, const QString &prefix, const QString &suffix)
{

    QList<QString> identifiers;

    identifiers.reserve(numberOfIdentifiers);

    for (qsizetype i = 0; i < numberOfIdentifiers; ++i)
        identifiers.append(prefix + QString::number(startNumber + i) + suffix);

    return identifiers;

}
