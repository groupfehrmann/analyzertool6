#ifndef FASTICA_H
#define FASTICA_H

#include <QObject>
#include <QThreadPool>
#include <QPointer>

#include "workerclasses/baseworker.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"

#define FICA_APPROACH_DEFL 2
#define FICA_APPROACH_SYMM 1
#define FICA_NONLIN_POW3 10
#define FICA_NONLIN_TANH 20
#define FICA_NONLIN_GAUSS 30
#define FICA_NONLIN_SKEW 40
#define FICA_INIT_RAND  0
#define FICA_INIT_GUESS 1
#define FICA_TOL 1e-9

class FastICA : public QObject
{
    Q_OBJECT

public:

    explicit FastICA(const QList<double> &whitenedMatrix, const QList<double> &whiteningMatrix, const QList<double> &dewhiteningMatrix, qsizetype nSamples, qsizetype nItems, qsizetype nComponents, int contrastFunction = FICA_NONLIN_POW3, int maximumNumberOfIterations = 1000, bool finetuneMode = true, bool stabilizationMode = true, double mu = 1.0, double epsilon = 0.00001, double proportionOfItemsToUseInAnIteration = 1.0, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), QObject *parent = nullptr);

    const QList<double> &mixingMatrix() const;

    const QList<double> &seperatingMatrix() const;

    const QList<double> &independentComponents();

    void connectBaseWorker(BaseWorker *baseWorker);

    void setLabel(const QString &label);

private:

    QString d_label;

    const QList<double> &d_whitenedMatrix;

    const QList<double> &d_whiteningMatrix;

    const QList<double> &d_dewhiteningMatrix;

    qsizetype d_nSamples;

    qsizetype d_nItems;

    qsizetype d_nSubItems;

    qsizetype d_nComponents;

    QList<double> d_mixingMatrix;

    QList<double> d_seperatingMatrix;

    int d_contrastFunction;

    int d_maximumNumberOfIterations;

    bool d_finetuneMode;

    bool d_stabilizationMode;

    double d_mu;

    double d_epsilon;

    double d_proportionOfItemsToUseInAnIteration;

    int d_numberOfThreadsToUse;

    int d_previousNumberOfThreadsToUse;

    QThreadPool d_threadPool;

    QList<double> d_independentComponents;

    QList<double> d_B;

    QPointer<BaseWorker> d_connectedBaseworker;

public slots:

    bool run();

    void setNumberOfThreadsToUse(qsizetype numberOfThreadsToUse);

};

#endif // FASTICA_H
