#ifndef RESULTTABLEWIDGET_H
#define RESULTTABLEWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include <QObject>
#include <QTableView>
#include <QHeaderView>
#include <QAbstractButton>
#include <QMenu>
#include <QSpinBox>
#include <QWidgetAction>
#include <QLabel>
#include <QHBoxLayout>

#include "containers/result.h"
#include "containers/annotatedmatrix.h"
#include "models/annotatedmatrixdatamodel.h"

namespace Ui {

    class ResultTableWidget;

}

class ResultTableWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ResultTableWidget(QWidget *parent = nullptr, const QSharedPointer<QObject> &object = nullptr);

    ~ResultTableWidget();

private:

    Ui::ResultTableWidget *ui;

    QSharedPointer<QObject> d_object;

};

#endif // RESULTTABLEWIDGET_H
