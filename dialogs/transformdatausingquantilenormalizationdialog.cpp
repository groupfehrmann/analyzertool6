#include "transformdatausingquantilenormalizationdialog.h"

TransformDataUsingQuantileNormalizationDialog::TransformDataUsingQuantileNormalizationDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "transform data using quantile normalization", "transformdatausingquantilenormalizationdialog")
{

    TransformDataUsingQuantileNormalizationParameters *transformDataUsingQuantileNormalizationParameters = new TransformDataUsingQuantileNormalizationParameters;

    transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<TransformDataUsingQuantileNormalizationParameters>(transformDataUsingQuantileNormalizationParameters);

    BaseDialog::initWidgets();

}

void TransformDataUsingQuantileNormalizationDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &TransformDataUsingQuantileNormalizationDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &TransformDataUsingQuantileNormalizationDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes;});



    ComboBoxWidget *comboBoxWidget_descriptiveFunction = new ComboBoxWidget(this, "comboBoxWidget_descriptiveFunction");

    this->connect(comboBoxWidget_descriptiveFunction->comboBox(), &QComboBox::currentTextChanged, this, &TransformDataUsingQuantileNormalizationDialog::comboBoxWidget_descriptiveFunctionCurrentTextChanged);

    comboBoxWidget_descriptiveFunction->comboBox()->addItems({"mean", "median"});

    comboBoxWidget_descriptiveFunction->comboBox()->setCurrentIndex(0);

    comboBoxWidget_descriptiveFunction->label()->setText("select meta-analysis approach");

    comboBoxWidget_descriptiveFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_descriptiveFunction, 2, 0, 1, 1);



    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 3, 0, 1, 2);

     this->orientationChanged(d_orientationWidget->orientation());

}

void TransformDataUsingQuantileNormalizationDialog::orientationChanged(Qt::Orientation orientation)
{

    TransformDataUsingQuantileNormalizationParameters *transformDataUsingQuantileNormalizationParameters = static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->identifiers(orientation, true), transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), transformDataUsingQuantileNormalizationParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void TransformDataUsingQuantileNormalizationDialog::comboBoxWidget_descriptiveFunctionCurrentTextChanged(const QString &text)
{

    TransformDataUsingQuantileNormalizationParameters *parameters = static_cast<TransformDataUsingQuantileNormalizationParameters *>(BaseDialog::d_parameters.data());

    parameters->labelOfDescriptiveFunction = text;

    if (text == "mean")
        parameters->descriptiveFunction = [](const QList<double> &vector){return boost::math::statistics::mean(vector);};
    else if (text == "median")
        parameters->descriptiveFunction = [](const QList<double> &vector){QList<double> temp = vector; return boost::math::statistics::median(temp);};

}
