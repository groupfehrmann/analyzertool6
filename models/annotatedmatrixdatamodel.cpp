#include "annotatedmatrixdatamodel.h"

AnnotatedMatrixDataSortFilterProxyModel::AnnotatedMatrixDataSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent), d_showSelectedOnly(false)
{

}

bool AnnotatedMatrixDataSortFilterProxyModel::showSelectedOnly() const
{

    return d_showSelectedOnly;

}

bool AnnotatedMatrixDataSortFilterProxyModel::filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    if (!d_showSelectedOnly)
        return true;

    return (static_cast<Qt::CheckState>(this->sourceModel()->headerData(sourceColumn, Qt::Horizontal, Qt::CheckStateRole).toInt()) == Qt::Checked);

}

bool AnnotatedMatrixDataSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    if (!d_showSelectedOnly)
        return true;

    return (static_cast<Qt::CheckState>(this->sourceModel()->headerData(sourceRow, Qt::Vertical, Qt::CheckStateRole).toInt()) == Qt::Checked);

}

void AnnotatedMatrixDataSortFilterProxyModel::setShowSelectedOnly(bool showSelectedOnly)
{

    d_showSelectedOnly = showSelectedOnly;

    this->invalidateFilter();

}

AnnotatedMatrixDataModel::AnnotatedMatrixDataModel(QObject *parent, QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix) :
    QAbstractTableModel(parent),
    d_baseAnnotatedMatrix(baseAnnotatedMatrix)
{

    this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::resetDataAboutToBegin, this, &AnnotatedMatrixDataModel::beginResetModel);

    this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::dataIsReset, this, &AnnotatedMatrixDataModel::endResetModel);

}

const BaseAnnotatedMatrix *AnnotatedMatrixDataModel::baseAnnotatedMatrix() const
{

    return d_baseAnnotatedMatrix.data();

}

int AnnotatedMatrixDataModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return d_baseAnnotatedMatrix->identifierCount(Qt::Horizontal);

}

QVariant AnnotatedMatrixDataModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return d_baseAnnotatedMatrix->valueAsVariant(index.row(), index.column());

        case Qt::EditRole:
            return d_baseAnnotatedMatrix->valueAsVariant(index.row(), index.column()).toString();

        case Qt::ForegroundRole:
            return (!d_baseAnnotatedMatrix->identifierSelectionStatus(index.row(), Qt::Vertical) || !d_baseAnnotatedMatrix->identifierSelectionStatus(index.column(), Qt::Horizontal)) ? QColor(Qt::gray) : QColor(Qt::black);

        case Qt::StatusTipRole:
            return "row index " + QString::number(index.row()) + ", column index " + QString::number(index.column())  + " - \"" + d_baseAnnotatedMatrix->identifier(index.row(), Qt::Vertical) + "\", \"" + d_baseAnnotatedMatrix->identifier(index.column(), Qt::Horizontal) + "\"";

        default:
            return QVariant();

    }

    return QVariant();

}

Qt::ItemFlags AnnotatedMatrixDataModel::flags(const QModelIndex &index) const
{

    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;

}

QVariant AnnotatedMatrixDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if ((section < 0) || section >= d_baseAnnotatedMatrix->identifierCount(orientation))
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return d_baseAnnotatedMatrix->identifier(section, orientation);

        case Qt::EditRole:
            return d_baseAnnotatedMatrix->identifier(section, orientation);

        case Qt::ForegroundRole:
            return d_baseAnnotatedMatrix->identifierSelectionStatus(section, orientation) ? QColor(Qt::black) : QColor(Qt::white);

        case Qt::BackgroundRole:
            return d_baseAnnotatedMatrix->identifierSelectionStatus(section, orientation) ? QVariant() : QColor(128, 0, 0);

        case Qt::StatusTipRole:
            return (orientation == Qt::Vertical) ? "row index " + QString::number(section) : "column index " + QString::number(section);

        case Qt::CheckStateRole:
            return d_baseAnnotatedMatrix->identifierSelectionStatus(section, orientation) ? Qt::Checked : Qt::Unchecked;

        default:
            return QVariant();

    }

}

bool AnnotatedMatrixDataModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);

    d_baseAnnotatedMatrix->insertLists(column, count, Qt::Horizontal);

    endInsertColumns();

    return true;

}

bool AnnotatedMatrixDataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    d_baseAnnotatedMatrix->insertLists(row, count, Qt::Vertical);

    endInsertRows();

    return true;

}

bool AnnotatedMatrixDataModel::removeColumns(int column, int count, const QModelIndex &parent)
{

    beginRemoveColumns(parent, column, column + count - 1);

    QList<qsizetype> indexes(count);

    std::iota(std::begin(indexes), std::end(indexes), column);

    d_baseAnnotatedMatrix->removeLists(indexes, Qt::Horizontal);

    endRemoveColumns();

    return true;

}

bool AnnotatedMatrixDataModel::removeRows(int row, int count, const QModelIndex &parent)
{

    beginRemoveRows(parent, row, row + count - 1);

    QList<qsizetype> indexes(count);

    std::iota(std::begin(indexes), std::end(indexes), row);

    d_baseAnnotatedMatrix->removeLists(indexes, Qt::Vertical);

    endRemoveRows();

    return true;

}

int AnnotatedMatrixDataModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;

    return d_baseAnnotatedMatrix->identifierCount(Qt::Vertical);

}

bool AnnotatedMatrixDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!d_baseAnnotatedMatrix->tryLockForWrite())
        return false;

    if (data(index, role) != value) {

        switch (role) {

            case Qt::EditRole:
                d_baseAnnotatedMatrix->setValueAsVariant(index.row(), index.column(), value);
            break;

            default: {

                d_baseAnnotatedMatrix->unlock();

                return false;

            }

        }

        emit dataChanged(index, index, QVector<int>() << role);

        d_baseAnnotatedMatrix->unlock();

        return true;

    }

    d_baseAnnotatedMatrix->unlock();

    return false;
}

bool AnnotatedMatrixDataModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{

    if (value != headerData(section, orientation, role)) {

        switch (role) {

            case Qt::EditRole:
                d_baseAnnotatedMatrix->setIdentifier(section, value.toString(), orientation);
            break;

            case Qt::CheckStateRole:
                d_baseAnnotatedMatrix->setIdentifierSelectionStatus(section, (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked) ? true : false, orientation);
            break;

            default:
                return false;

        }

        emit headerDataChanged(orientation, section, section);

        return true;

    }

    return false;

}
