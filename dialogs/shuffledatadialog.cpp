#include "shuffledatadialog.h"

ShuffleDataDialog::ShuffleDataDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "principal component analysis", "ShuffleDataDialog")
{

    ShuffleDataParameters *shuffleDataParameters = new ShuffleDataParameters;

    shuffleDataParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<ShuffleDataParameters>(shuffleDataParameters);

    BaseDialog::initWidgets();

}

void ShuffleDataDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes;});


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &ShuffleDataDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void ShuffleDataDialog::orientationChanged(Qt::Orientation orientation)
{

    ShuffleDataParameters *shuffleDataParameters = static_cast<ShuffleDataParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(shuffleDataParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), shuffleDataParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(shuffleDataParameters->baseAnnotatedMatrix->identifiers(orientation, true), shuffleDataParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(shuffleDataParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), shuffleDataParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(shuffleDataParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), shuffleDataParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}
