#include "selectdatawithtokensdialog.h"

SelectDataWithTokensDialog::SelectDataWithTokensDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, QString(), "selectdatawithtokensdialog")
{

    SelectDataWithTokensParameters *selectDataWithTokensParameters = new SelectDataWithTokensParameters;

    selectDataWithTokensParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<SelectDataWithTokensParameters>(selectDataWithTokensParameters);

    BaseDialog::initWidgets();

    this->connect(this, &SelectDataWithTokensDialog::accepted, this, &SelectDataWithTokensDialog::dialogAccepted);

}


void SelectDataWithTokensDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_identifiers = new ItemSelectorWidget(this, "itemSelectorWidget_identifiers");

    if (d_orientationWidget->orientation() == Qt::Vertical)
        d_itemSelectorWidget_identifiers->label()->setText("select row identifiers");
    else
        d_itemSelectorWidget_identifiers->label()->setText("select column identifiers");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SelectDataWithTokensDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_identifiers, 1, 0, 1, 2);

    this->connect(d_itemSelectorWidget_identifiers->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectedIdentifiersLabels = itemLabels; static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectedIdentifiersIndexes = itemIndexes;});


    d_itemSelectorWidget_labelToApplyOn = new ItemSelectorWidget(this, "itemSelectorWidget_labelToApplyOn");

    if (d_orientationWidget->orientation() == Qt::Vertical)
        d_itemSelectorWidget_labelToApplyOn->label()->setText("select row identifier column, data columns, or row annotations to perform matching in");
    else
        d_itemSelectorWidget_labelToApplyOn->label()->setText("select column identifier row, data rows, or column annotations to perform matching in");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SelectDataWithTokensDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_labelToApplyOn, 2, 0, 1, 2);

    this->connect(d_itemSelectorWidget_labelToApplyOn->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectedLabelsToApplyOn = itemLabels; static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectedIndexesToApplyOn = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_selectionMode1 = new ComboBoxWidget(this, "comboBoxWidget_selectionMode1");

    this->connect(comboBoxWidget_selectionMode1->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_selectionMode1](){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectionMode1 = comboBoxWidget_selectionMode1->comboBox()->currentText();});

    comboBoxWidget_selectionMode1->label()->setText("selection mode");

    comboBoxWidget_selectionMode1->comboBox()->addItems({"match with at least one token in at least one field", "match with all tokens in at least one field", "match with at least one token in all fields", "match with all tokens in all fields"});

    comboBoxWidget_selectionMode1->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_selectionMode1, 3, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_selectionMode2 = new ComboBoxWidget(this, "comboBoxWidget_selectionMode2");

    this->connect(comboBoxWidget_selectionMode2->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_selectionMode2](){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->selectionMode2 = comboBoxWidget_selectionMode2->comboBox()->currentText();});

    comboBoxWidget_selectionMode2->label()->setText("selection mode");

    comboBoxWidget_selectionMode2->comboBox()->addItems({"if match then select", "if match then deselect"});

    comboBoxWidget_selectionMode2->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_selectionMode2, 3, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_matchType = new ComboBoxWidget(this, "comboBoxWidget_matchType");

    this->connect(comboBoxWidget_matchType->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_matchType](){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->matchType = comboBoxWidget_matchType->comboBox()->currentText();});

    comboBoxWidget_matchType->label()->setText("select match type");

    comboBoxWidget_matchType->comboBox()->addItems({"exact match", "contains"});

    comboBoxWidget_matchType->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_matchType, 4, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_caseSensitivity = new ComboBoxWidget(this, "comboBoxWidget_caseSensitivity");

    this->connect(comboBoxWidget_caseSensitivity->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_caseSensitivity](){static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->caseSensitivity = static_cast<Qt::CaseSensitivity>(comboBoxWidget_caseSensitivity->comboBox()->currentData().toInt());});

    comboBoxWidget_caseSensitivity->label()->setText("select case sensitivity");

    comboBoxWidget_caseSensitivity->comboBox()->addItem("case insensitive", Qt::CaseInsensitive);

    comboBoxWidget_caseSensitivity->comboBox()->addItem("case sensitive", Qt::CaseSensitive);

    comboBoxWidget_caseSensitivity->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_caseSensitivity, 4, 1, 1, 1);


    d_textEditWidget_tokens = new TextEditWidget(this, "d_textEditWidget_tokens");

    d_textEditWidget_tokens->label()->setText("text field to enter or paste tokens");

    d_gridLayout->addWidget(d_textEditWidget_tokens, 5, 0, 1, 2);


    d_selectDelimiterWidget = new SelectDelimiterWidget(this, "d_selectDelimiterWidget");

    d_selectDelimiterWidget->label()->setText("select delimiter to apply on text field above in which the tokens are entered or pasted");

    d_selectDelimiterWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_selectDelimiterWidget, 6, 0, 1, 2);


    this->orientationChanged(d_orientationWidget->orientation());

}

void SelectDataWithTokensDialog::orientationChanged(Qt::Orientation orientation)
{

    SelectDataWithTokensParameters *selectDataWithTokensParameters = static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_identifiers->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(selectDataWithTokensParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), selectDataWithTokensParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_identifiers->itemSelectorModel_selected()->setItemLabelsAndIndexes(selectDataWithTokensParameters->baseAnnotatedMatrix->identifiers(orientation, true), selectDataWithTokensParameters->baseAnnotatedMatrix->indexes(orientation, true));

    QList<QString> itemLabels;

    itemLabels.append((orientation == Qt::Vertical) ? "ROW IDENTIFIER" : "COLUMN IDENTIFIER");

    for (const QString &identifier : selectDataWithTokensParameters->baseAnnotatedMatrix->annotationLabels(orientation, false))
        itemLabels.append((orientation == Qt::Vertical ? "[ROW ANNOTATION] " : "[COLUMN ANNOTATION] ") + identifier);

    for (const QString &identifier : selectDataWithTokensParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation)))
        itemLabels.append((orientation == Qt::Vertical ? "[DATA COLUMN] " : "[DATA ROW] ") + identifier);

    d_itemSelectorWidget_labelToApplyOn->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(itemLabels, QList<qsizetype>{-1} << selectDataWithTokensParameters->baseAnnotatedMatrix->annotationLabelIndexes(orientation, false) << selectDataWithTokensParameters->baseAnnotatedMatrix->indexes(orientation, false));

    d_itemSelectorWidget_labelToApplyOn->itemSelectorModel_selected()->clear();

}

void SelectDataWithTokensDialog::dialogAccepted()
{

    static_cast<SelectDataWithTokensParameters *>(BaseDialog::d_parameters.data())->tokensToMatch = d_textEditWidget_tokens->textEdit()->toPlainText().split(QRegularExpression(d_selectDelimiterWidget->comboBox()->currentData().toString()), Qt::SkipEmptyParts);

}
