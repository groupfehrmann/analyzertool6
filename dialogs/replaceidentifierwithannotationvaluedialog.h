#ifndef REPLACEIDENTIFIERWITHANNOTATIONVALUEDIALOG_H
#define REPLACEIDENTIFIERWITHANNOTATIONVALUEDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/comboboxwidget.h"
#include "workerclasses/replaceidentifierwithannotationvalueworker.h"

class ReplaceIdentifierWithAnnotationValueDialog : public BaseDialog
{

public:

    ReplaceIdentifierWithAnnotationValueDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ComboBoxWidget *d_comboBoxWidget_annotationLabelUsedForReplacements;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};



#endif // REPLACEIDENTIFIERWITHANNOTATIONVALUEDIALOG_H
