#include "transformdatausingquantilenormalizationworker.h"

TransformDataUsingQuantileNormalizationParameters::TransformDataUsingQuantileNormalizationParameters(QObject *parent) :
    BaseParameters(parent, "transformdatausingquantilenormalization")
{

}

bool TransformDataUsingQuantileNormalizationParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    return true;

}

QString TransformDataUsingQuantileNormalizationParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[transform data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected descriptive function = \"" << labelOfDescriptiveFunction << "]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

TransformDataUsingQuantileNormalizationWorker::TransformDataUsingQuantileNormalizationWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("transform data using quantile normalization"), parameters, "transformdatausingquantilenormalization")
{

}

void TransformDataUsingQuantileNormalizationWorker::doWork_()
{

    QSharedPointer<TransformDataUsingQuantileNormalizationParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingQuantileNormalizationParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("for transforming data, values in data need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool TransformDataUsingQuantileNormalizationWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<TransformDataUsingQuantileNormalizationParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingQuantileNormalizationParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    return true;

}

void TransformDataUsingQuantileNormalizationWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<TransformDataUsingQuantileNormalizationParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingQuantileNormalizationParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}
