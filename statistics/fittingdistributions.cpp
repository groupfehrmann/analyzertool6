#include "fittingdistributions.h"

std::function<double (const double &)> FittingDistributions::cdf_generalizedParetoTypeDistribution(const QList<double> &sortedPermutationDistributionAscending, bool &succes, qsizetype nExceedances, qsizetype nPermutationsInBootstrap)
{

    succes = false;

    while (nExceedances > 0) {

        double exceedancesThreshold = (sortedPermutationDistributionAscending.at(sortedPermutationDistributionAscending.count() - nExceedances) + sortedPermutationDistributionAscending.at(sortedPermutationDistributionAscending.count() - nExceedances - 1)) / static_cast<double>(2.0);

        QList<double> Zexc;

        Zexc.reserve(nExceedances);

        for (qsizetype i = sortedPermutationDistributionAscending.count() - nExceedances; i < sortedPermutationDistributionAscending.count(); ++i)
            Zexc.append(sortedPermutationDistributionAscending.at(i) - exceedancesThreshold);

        auto functionK = [&Zexc](const double &sigma) {

            double sum = 0.0;

            for (const double &value : Zexc)
                sum += std::log(static_cast<double>(1.0) - value / sigma);

            return -(sum / static_cast<double>(Zexc.count()));

        };

        auto functionP = [&Zexc, &functionK](const double &sigma) {

            return 1.0 / (static_cast<double>(Zexc.count()) * (-std::log(functionK(sigma) * sigma) + functionK(sigma) - static_cast<double>(1.0)));

        };

        Brent brent1;

        brent1.bracket(std::numeric_limits<double>::epsilon(), 1000.0 * Zexc.last(), functionP);

        double optimalSigma = brent1.minimize(functionP, succes);

        if (succes) {

            double k = functionK(optimalSigma);

            double a = functionK(optimalSigma) * optimalSigma;

            auto generalizedParetoCDF = [&a, &k](const double &x) {

                double base = static_cast<double>(1.0) + k * x / a;

                return static_cast<double>(1.0) - std::pow(base, static_cast<double>(-1.0) / k);

            };

            auto andersonDarlingStatistic = [&generalizedParetoCDF](const QList<double> &y) {

                double n = static_cast<double>(y.count());

                double s = 0.0;

                for (qsizetype i = 0; i < y.count(); ++i) {

                    double cdf_yi = generalizedParetoCDF(y.at(i));

                    if (cdf_yi == 0.0)
                        cdf_yi = std::numeric_limits<double>::min();

                    double cdf_y_rev_i = static_cast<double>(1.0) - generalizedParetoCDF(y.at(y.count() - i - 1 ));

                    if (cdf_y_rev_i == 0.0)
                        cdf_y_rev_i = std::numeric_limits<double>::min();

                    s += ((2.0 * static_cast<double>(i) - static_cast<double>(1.0)) / static_cast<double>(y.count())) * std::log(cdf_yi) + std::log(cdf_y_rev_i);

                }

                return -n - s;

            };

            double adStat = andersonDarlingStatistic(Zexc);

            if (std::isnormal(adStat)) {

                boost::mt19937 randGen(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));

                boost::random::uniform_real_distribution<> uniformReal(0.0, 1.0);

                boost::variate_generator<boost::mt19937&, boost::random::uniform_real_distribution<> > generator(randGen, uniformReal);

                QList<double> bootstrapDistribution(nPermutationsInBootstrap);

                for (qsizetype i = 0; i < nPermutationsInBootstrap; ++i) {

                    QList<double> sample;

                    sample.reserve(nExceedances);

                    while(sample.count() < static_cast<qsizetype>(nExceedances)) {

                        double generatedValue = generator();

                        if (generatedValue > 0) {

                            double value = a * (std::pow(generatedValue, -k) - static_cast<double>(1.0)) / k;

                            if (!std::isinf(value))
                                sample.append(value);

                        }

                    }


                    std::sort(sample.begin(), sample.end(), std::less<double>());

                    bootstrapDistribution[i] = andersonDarlingStatistic(sample);

                }

                std::sort(bootstrapDistribution.begin(), bootstrapDistribution.end());

                auto ecdf = boost::math::empirical_cumulative_distribution_function(std::move(bootstrapDistribution), true);

                if ((static_cast<double>(1.0) - ecdf(adStat)) > 0.05) {

                    succes = true;

                    return [a, k, exceedancesThreshold](const double &x) {

                        double base = static_cast<double>(1.0) + k * (x - exceedancesThreshold) / a;

                        return static_cast<double>(1.0) - std::pow(base, static_cast<double>(-1.0) / k);

                    };

                } else
                    succes = false;

            } else
                succes = false;

        } else
            succes = false;

        nExceedances -= 10;

    }

    return [](const double &x) {Q_UNUSED(x); return std::numeric_limits<double>::quiet_NaN();};

}

double FittingDistributions::fitTDistribution(const QList<double> &sortedEmpericalDistributionAscending)
{

    auto functionP = [&sortedEmpericalDistributionAscending](const double &degreeOfFreedom) {

        boost::math::students_t students_t_distribution(degreeOfFreedom);

        double n = static_cast<double>(sortedEmpericalDistributionAscending.count());

        double s = 0.0;

        for (qsizetype i = 0; i < sortedEmpericalDistributionAscending.count(); ++i) {

            double cdf_yi = boost::math::cdf(students_t_distribution, sortedEmpericalDistributionAscending.at(i));

            if (cdf_yi == 0.0)
                cdf_yi = std::numeric_limits<double>::min();

            double cdf_y_rev_i = boost::math::cdf(boost::math::complement(students_t_distribution, sortedEmpericalDistributionAscending.at(sortedEmpericalDistributionAscending.count() - i - 1 )));

            if (cdf_y_rev_i == 0.0)
                cdf_y_rev_i = std::numeric_limits<double>::min();

            s += ((2.0 * static_cast<double>(i) - static_cast<double>(1.0)) / static_cast<double>(sortedEmpericalDistributionAscending.count())) * std::log(cdf_yi) + std::log(cdf_y_rev_i);

        }

        return -n - s;

    };

    bool succes;

    Brent brent;

    brent.bracket(0, 1.0, functionP);

    double optimalDegreeOfFreedom = brent.minimize(functionP, succes);

    if (succes)
        return optimalDegreeOfFreedom;
    else
        return std::numeric_limits<double>::quiet_NaN();

}

double FittingDistributions::fitTDistribution(const std::span<double> &sortedEmpericalDistributionAscending)
{

    auto functionP = [&sortedEmpericalDistributionAscending](const double &degreeOfFreedom) {

        boost::math::students_t students_t_distribution(degreeOfFreedom);

        double n = static_cast<double>(sortedEmpericalDistributionAscending.size());

        double s = 0.0;

        for (unsigned long i = 0; i < sortedEmpericalDistributionAscending.size(); ++i) {

            double cdf_yi = boost::math::cdf(students_t_distribution, sortedEmpericalDistributionAscending[i]);

            if (cdf_yi == 0.0)
                cdf_yi = std::numeric_limits<double>::min();

            double cdf_y_rev_i = boost::math::cdf(boost::math::complement(students_t_distribution, sortedEmpericalDistributionAscending[sortedEmpericalDistributionAscending.size() - i - 1 ]));

            if (cdf_y_rev_i == 0.0)
                cdf_y_rev_i = std::numeric_limits<double>::min();

            s += ((2.0 * static_cast<double>(i) - static_cast<double>(1.0)) / static_cast<double>(sortedEmpericalDistributionAscending.size())) * std::log(cdf_yi) + std::log(cdf_y_rev_i);

        }

        return -n - s;

    };

    bool succes;

    Brent brent;

    brent.bracket(0.001, 0.005, functionP);

    double optimalDegreeOfFreedom = brent.minimize(functionP, succes);

    if (succes)
        return optimalDegreeOfFreedom;
    else
        return std::numeric_limits<double>::quiet_NaN();

}

QList<double> FittingDistributions::fitSkewNormal(const std::span<double> &distribution)
{
 /*
    auto skew_normal_neg_log_likelihood = [&distribution](const QList<double> &parameters) {

        boost::math::skew_normal_distribution<> dist(0, scale, shape);

        double ll = 0.0;

        for (const double &value : distribution)
                ll += std::log(skew_normal_pdf(data[i], location, scale, shape));
            }
            return ll;

    };


            const int N = 2;  // number of dimensions
               int nfunc = 0;    // number of function evaluations
               const float ftol = 1.0e-6;  // tolerance for stopping criterion
               Vec_DP p(N, 0.0);  // initial starting point
               Vec_DP step(N, 1.0);  // initial step size for each dimension

               // Initialize starting point and step size
               p[0] = 0.0;
               p[1] = 0.0;
               step[0] = 0.1;
               step[1] = 0.1;

               // Call the downhill simplex optimization routine
               DP ans = amoeba(p, step, ftol, func, nfunc);

               // Print the result
               std::cout << "Optimized value: " << ans << std::endl;
               std::cout << "Number of function evaluations: " << nfunc << std::endl;

   */
}
