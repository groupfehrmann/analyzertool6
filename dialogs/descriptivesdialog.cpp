#include "descriptivesdialog.h"

DescriptivesDialog::DescriptivesDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "descriptives", "descriptivesdialog")
{

    DescriptivesParameters *descriptivesParameters = new DescriptivesParameters;

    descriptivesParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<DescriptivesParameters>(descriptivesParameters);

    BaseDialog::initWidgets();

    this->connect(this, &DescriptivesDialog::accepted, this, &DescriptivesDialog::dialogAccepted);

}

void DescriptivesDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes; this->comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->currentIndex());});


    d_comboBoxWidget_annotationLabelDefiningItemSubsets = new ComboBoxWidget(this, "d_comboBoxWidget_annotationLabelDefiningItemSubsets");

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->annotationLabelDefiningItemSubsets = text;});

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->annotationIndexDefiningItemSubsets = (index == 0) ? -1 : index - 1;});

    d_comboBoxWidget_annotationLabelDefiningItemSubsets->label()->setText("select annotation label defining item subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxWidget_annotationLabelDefiningItemSubsets, 2, 0, 1, 1);


    d_itemSelectorWidget_itemSubsetIdentifiers = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemSubsetIdentifiers");

    d_itemSelectorWidget_itemSubsetIdentifiers->label()->setText("select item subset identifiers");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemSubsetIdentifiers, 2, 1, 1, 1);

    this->connect(d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->selectedItemSubsetIdentifiers = itemLabels;});


    d_itemSelectorWidget_annotationLabelsForVariables = new ItemSelectorWidget(this, "d_itemSelectorWidget_annotationLabelsForVariables");

    d_itemSelectorWidget_annotationLabelsForVariables->label()->setText("select annotation labels for variables (optional)");

    d_gridLayout->addWidget(d_itemSelectorWidget_annotationLabelsForVariables, 3, 0, 1, 1);

    this->connect(d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->annotationLabelsForVariables = itemLabels;});


    d_itemSelectorWidget_descriptives = new ItemSelectorWidget(this, "d_itemSelectorWidget_descriptives");

    d_itemSelectorWidget_descriptives->label()->setText("select descriptives");

    d_itemSelectorWidget_descriptives->itemSelectorModel_selected()->setItemLabelsAndIndexes({"sum", "mean", "population variance", "sample variance", "standard deviation", "skewness", "kurtosis", "excess kurtosis", "median", "median absolute deviation", "minimum", "maximum", "absolute minimum", "absolute maximum", "top ranked", "bottom ranked", "top ranked absolute", "absolute sum", "mode"});

    d_itemSelectorWidget_descriptives->readWidgetSettings();

    d_gridLayout->addWidget(d_itemSelectorWidget_descriptives, 3, 1, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 4, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 5, 0, 1, 2);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &DescriptivesDialog::orientationChanged);

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentIndexChanged, this, &DescriptivesDialog::comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void DescriptivesDialog::orientationChanged(Qt::Orientation orientation)
{

    DescriptivesParameters *descriptivesParameters = static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(descriptivesParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), descriptivesParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(descriptivesParameters->baseAnnotatedMatrix->identifiers(orientation, true), descriptivesParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(descriptivesParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), descriptivesParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(descriptivesParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), descriptivesParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));


    QList<QString> itemLabels;

    itemLabels.append(QString());

    for (const QString &identifier : descriptivesParameters->baseAnnotatedMatrix->annotationLabels(BaseAnnotatedMatrix::switchOrientation(orientation), false))
        itemLabels.append(identifier);

    d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->clear();

    d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->addItems(itemLabels);


    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(descriptivesParameters->baseAnnotatedMatrix->annotationLabels(orientation));

    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected()->clear();

}

void DescriptivesDialog::comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(qsizetype index)
{

    if (index <= 0) {

        d_itemSelectorWidget_itemSubsetIdentifiers->clear();

        d_itemSelectorWidget_itemSubsetIdentifiers->setEnabled(false);

        return;

    }

    d_itemSelectorWidget_itemSubsetIdentifiers->setEnabled(true);

    DescriptivesParameters *descriptivesParameters = static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data());

    QSet<QString> itemSubsetIdentifiers;

    for (qsizetype indexOfSelectedItem : descriptivesParameters->selectedItemIndexes)
        itemSubsetIdentifiers.insert(descriptivesParameters->baseAnnotatedMatrix->annotationValue(indexOfSelectedItem, index - 1, BaseAnnotatedMatrix::switchOrientation(d_orientationWidget->orientation())).toString());

    QList<QString> temp(itemSubsetIdentifiers.begin(), itemSubsetIdentifiers.end());

    std::sort(temp.begin(), temp.end());

    d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_notSelected()->clear();

    d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_selected()->setItemLabelsAndIndexes(temp);

}

void DescriptivesDialog::dialogAccepted()
{

    DescriptivesParameters *descriptivesParameters = static_cast<DescriptivesParameters *>(BaseDialog::d_parameters.data());

    descriptivesParameters->descriptiveFunctions.clear();

    descriptivesParameters->descriptiveLabels.clear();

    for (const QString &descriptiveLabel : d_itemSelectorWidget_descriptives->itemSelectorModel_selected()->itemLabels()) {

        if (descriptiveLabel == "mean") {

            descriptivesParameters->descriptiveLabels << "mean";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::mean(vector);});

        } else if (descriptiveLabel == "population variance") {

            descriptivesParameters->descriptiveLabels << "population variance";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::variance(vector);});

        } else if (descriptiveLabel == "sample variance") {

            descriptivesParameters->descriptiveLabels << "sample variance";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::sample_variance(vector);});

        } else if (descriptiveLabel == "standard deviation") {

            descriptivesParameters->descriptiveLabels << "standard deviation";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << std::sqrt(boost::math::statistics::sample_variance(vector));});

        } else if (descriptiveLabel == "skewness") {

            descriptivesParameters->descriptiveLabels << "skewness";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::skewness(vector);});

        } else if (descriptiveLabel == "kurtosis") {

            descriptivesParameters->descriptiveLabels << "kurtosis";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::kurtosis(vector);});

        } else if (descriptiveLabel == "excess kurtosis") {

            descriptivesParameters->descriptiveLabels << "excess kurtosis";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << boost::math::statistics::excess_kurtosis(vector);});

        } else if (descriptiveLabel == "median") {

            descriptivesParameters->descriptiveLabels << "median";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); QList<double> temp = vector; return QList<QVariant>() << boost::math::statistics::median(temp);});

        } else if (descriptiveLabel == "median absolute deviation") {

            descriptivesParameters->descriptiveLabels << "median absolute deviation";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); QList<double> temp = vector; return QList<QVariant>() << boost::math::statistics::median_absolute_deviation(temp);});

        } else if (descriptiveLabel == "minimum") {

            descriptivesParameters->descriptiveLabels << "minimum (identifier)" << "minimum (value)";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){QList<double>::const_iterator min_element = std::min_element(vector.begin(), vector.end()); return QList<QVariant>() << identifiers.at(std::distance(vector.begin(), min_element)) << *min_element;});

        } else if (descriptiveLabel == "maximum") {

            descriptivesParameters->descriptiveLabels << "maximum (identifier)" << "maximum (value)";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){QList<double>::const_iterator max_element = std::max_element(vector.begin(), vector.end()); return QList<QVariant>() << identifiers.at(std::distance(vector.begin(), max_element)) << *max_element;});

        } else if (descriptiveLabel == "absolute minimum") {

            descriptivesParameters->descriptiveLabels << "absolute minimum (identifier)" << "absolute minimum (value)";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){QList<double>::const_iterator min_element = std::min_element(vector.begin(), vector.end(), [](const double &a, const double &b) {return (std::abs(a) < std::abs(b));}); return QList<QVariant>() << identifiers.at(std::distance(vector.begin(), min_element)) << *min_element;});

        } else if (descriptiveLabel == "absolute maximum") {

            descriptivesParameters->descriptiveLabels << "absolute maximum (identifier)" << "absolute maximum (value)";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){QList<double>::const_iterator max_element = std::max_element(vector.begin(), vector.end(), [](const double &a, const double &b) {return (std::abs(a) < std::abs(b));}); return QList<QVariant>() << identifiers.at(std::distance(vector.begin(), max_element)) << *max_element;});

        } else if (descriptiveLabel == "sum") {

            descriptivesParameters->descriptiveLabels << "sum";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << std::accumulate(vector.begin(), vector.end(), 0.0);});

        } else if (descriptiveLabel == "absolute sum") {

            descriptivesParameters->descriptiveLabels << "absolute sum";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); return QList<QVariant>() << std::transform_reduce(vector.begin(), vector.end(), 0.0, std::plus<>{}, static_cast<double (*)(double)>(std::fabs));});

        } else if (descriptiveLabel == "top ranked") {

            QSettings settings;

            int nTopRanked = 3;

            settings.beginGroup(d_dialogSettingsIdentifier);

            if (settings.contains("nTopRanked"))
               nTopRanked = settings.value("nTopRanked").toInt();

            settings.endGroup();

            nTopRanked = QInputDialog::getInt(this, QCoreApplication::applicationName() + " - input dialog", "select number of top ranks", nTopRanked, 1);

            settings.beginGroup(d_dialogSettingsIdentifier);

            settings.setValue("nTopRanked", nTopRanked);

            settings.endGroup();

            for (qsizetype i = 0; i < nTopRanked; ++i)
                descriptivesParameters->descriptiveLabels << "rank " + QString::number(i + 1) + " (identifier)" << "rank " + QString::number(i + 1) + " (value)";

            descriptivesParameters->descriptiveFunctions.append([nTopRanked](const QList<QString> &identifiers, const QList<double> &vector) {

                QList<QPair<QString, double>> temp;

                for (qsizetype i = 0; i < vector.count(); ++i)
                    temp.append(QPair<QString, double>(identifiers.at(i), vector.at(i)));

                std::sort(temp.begin(), temp.end(), [](const QPair<QString, double> &pair1, const QPair<QString, double> &pair2){return pair1.second > pair2.second;});

                QList<QVariant> result;

                for (qsizetype i = 0; i < nTopRanked; ++i) {

                    if (i < temp.count())
                        result << temp.at(i).first << temp.at(i).second;
                    else
                        result << "" << std::numeric_limits<double>::quiet_NaN();

                }

                return result;

            });

        } else if (descriptiveLabel == "bottom ranked") {

            QSettings settings;

            int nBottomRanked = 3;

            settings.beginGroup(d_dialogSettingsIdentifier);

            if (settings.contains("nBottomRanked"))
               nBottomRanked = settings.value("nBottomRanked").toInt();

            settings.endGroup();

            nBottomRanked = QInputDialog::getInt(this, QCoreApplication::applicationName() + " - input dialog", "select number of bottom ranks", nBottomRanked, 1);

            settings.beginGroup(d_dialogSettingsIdentifier);

            settings.setValue("nTopRanked", nBottomRanked);

            settings.endGroup();

            for (qsizetype i = 0; i < nBottomRanked; ++i)
                descriptivesParameters->descriptiveLabels << "rank " + QString::number(i + 1) + " (identifier)" << "rank " + QString::number(i + 1) + " (value)";

            descriptivesParameters->descriptiveFunctions.append([nBottomRanked](const QList<QString> &identifiers, const QList<double> &vector) {

                QList<QPair<QString, double>> temp;

                for (qsizetype i = 0; i < vector.count(); ++i)
                    temp.append(QPair<QString, double>(identifiers.at(i), vector.at(i)));

                std::sort(temp.begin(), temp.end(), [](const QPair<QString, double> &pair1, const QPair<QString, double> &pair2){return pair1.second < pair2.second;});

                QList<QVariant> result;

                for (qsizetype i = 0; i < nBottomRanked; ++i) {

                    if (i < temp.count())
                        result << temp.at(i).first << temp.at(i).second;
                    else
                        result << "" << std::numeric_limits<double>::quiet_NaN();

                }

                return result;

            });

        } else if (descriptiveLabel == "top ranked absolute") {

            QSettings settings;

            int nTopRankedAbsolute = 3;

            settings.beginGroup(d_dialogSettingsIdentifier);

            if (settings.contains("nTopRanked"))
               nTopRankedAbsolute = settings.value("nTopRanked").toInt();

            settings.endGroup();

            nTopRankedAbsolute = QInputDialog::getInt(this, QCoreApplication::applicationName() + " - input dialog", "select number of top ranks absolute", nTopRankedAbsolute, 1);

            settings.beginGroup(d_dialogSettingsIdentifier);

            settings.setValue("nTopRanked", nTopRankedAbsolute);

            settings.endGroup();

            for (qsizetype i = 0; i < nTopRankedAbsolute; ++i)
                descriptivesParameters->descriptiveLabels << "rank " + QString::number(i + 1) + " (identifier)" << "rank " + QString::number(i + 1) + " (value)";

            descriptivesParameters->descriptiveFunctions.append([nTopRankedAbsolute](const QList<QString> &identifiers, const QList<double> &vector) {

                QList<QPair<QString, double>> temp;

                for (qsizetype i = 0; i < vector.count(); ++i)
                    temp.append(QPair<QString, double>(identifiers.at(i), vector.at(i)));

                std::sort(temp.begin(), temp.end(), [](const QPair<QString, double> &pair1, const QPair<QString, double> &pair2){return std::abs(pair1.second) > std::abs(pair2.second);});

                QList<QVariant> result;

                for (qsizetype i = 0; i < nTopRankedAbsolute; ++i) {

                    if (i < temp.count())
                        result << temp.at(i).first << temp.at(i).second;
                    else
                        result << "" << std::numeric_limits<double>::quiet_NaN();

                }

                return result;

            });

        } else if (descriptiveLabel == "mode") {

            descriptivesParameters->descriptiveLabels << "mode";

            descriptivesParameters->descriptiveFunctions.append([](const QList<QString> &identifiers, const QList<double> &vector){Q_UNUSED(identifiers); QList<double> modes; boost::math::statistics::mode(vector, std::back_inserter(modes));  return QList<QVariant>() << modes.first();});

        }

    }

}
