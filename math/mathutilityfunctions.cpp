#include "mathutilityfunctions.h"


void MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(int numberOfThreads)
{

    openblas_set_num_threads(numberOfThreads);

}

int MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines()
{

    return openblas_get_num_threads();

}

QList<std::pair<qsizetype, qsizetype> > MathUtilityFunctions::createIndexBlocksForMultiThreading(qsizetype sizeOfSequence, qsizetype numberOfThreads)
{

    QList<std::pair<qsizetype, qsizetype> > blocks;

    std::lldiv_t divResult = std::div(sizeOfSequence, numberOfThreads);

    if (divResult.quot == 0) {

        for (qsizetype i = 0; i < divResult.rem; ++i)
            blocks << std::pair<qsizetype, qsizetype>(i, i + 1);

    } else {

        qsizetype counter = 0;

        qsizetype remainder = divResult.rem;

        for (qsizetype i = 0; i < numberOfThreads; ++i) {

            std::pair<qsizetype, qsizetype> block;

            block.first = counter;

            for (qsizetype j = 0; j < divResult.quot - 1; ++j)
                ++counter;

            if (remainder != 0) {

                ++counter;

                --remainder;

            }

            block.second = counter + 1;

            ++counter;

            blocks << block;

        }

    }

    return blocks;

}
