#include "transformdatawithpredefinedmetricsdialog.h"


TransformDataWithPredefinedMetricsDialog::TransformDataWithPredefinedMetricsDialog(BaseDialog *parent, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices) :
    BaseDialog(parent, "transform data with predefined metrics", "transformdatawithpredefinedmetricsdialog"),
    d_baseAnnotatedMatrices(baseAnnotatedMatrices)
{

    BaseDialog::d_parameters = QSharedPointer<TransformDataWithPredefinedMetricsParameters>(new TransformDataWithPredefinedMetricsParameters);

    BaseDialog::initWidgets();

}

void TransformDataWithPredefinedMetricsDialog::initWidgets_()
{

    QList<QString> baseAnnotatedMatrixLabels;

    for (qsizetype i = 0; i < d_baseAnnotatedMatrices.count(); ++i)
        baseAnnotatedMatrixLabels.append(d_baseAnnotatedMatrices.at(i)->property("label").toString());


    ComboBoxWidget *comboBoxWidget_dataToTransform = new ComboBoxWidget(this, "comboBoxWidget_dataToTransform");

    this->connect(comboBoxWidget_dataToTransform->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->baseAnnotatedMatrix_dataToTranform = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_dataToTransform->label()->setText("select data to transform");

    comboBoxWidget_dataToTransform->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_dataToTransform, 0, 0, 1, 2);


    d_orientationWidget_dataToTransform = new OrientationWidget(this, "d_orientationWidget_dataToTransform");

    d_orientationWidget_dataToTransform->label()->setText("select orientation of data to transform");

    this->connect(d_orientationWidget_dataToTransform, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->orientation_dataToTransform = orientation;});

    d_orientationWidget_dataToTransform->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_dataToTransform, 1, 0, 1, 2);


    d_itemSelectorWidget_variablesDataToTransform = new ItemSelectorWidget(this, "d_itemSelectorWidget_variablesDataToTransform");

    d_itemSelectorWidget_variablesDataToTransform->label()->setText("select variables in data to transform");

    d_gridLayout->addWidget(d_itemSelectorWidget_variablesDataToTransform, 2, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variablesDataToTransform->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers_dataToTransform = itemLabels; static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes_dataToTransform = itemIndexes;});


    d_itemSelectorWidget_itemsDataToTransform = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemsDataToTransform");

    d_itemSelectorWidget_itemsDataToTransform->label()->setText("select items in data to transform");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemsDataToTransform, 2, 1, 1, 1);

    this->connect(d_itemSelectorWidget_itemsDataToTransform->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers_dataToTransform = itemLabels; static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes_dataToTransform = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_dataContainingPredefinedMetrics = new ComboBoxWidget(this, "comboBoxWidget_dataContainingPredefinedMetrics");

    this->connect(comboBoxWidget_dataContainingPredefinedMetrics->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->baseAnnotatedMatrix_dataContainingPredefinedMetrics = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_dataContainingPredefinedMetrics->label()->setText("select data containing predefined metrics");

    comboBoxWidget_dataContainingPredefinedMetrics->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_dataContainingPredefinedMetrics, 3, 0, 1, 2);


    d_orientationWidget_dataContainingPredefinedMetrics = new OrientationWidget(this, "d_orientationWidget_dataContainingPredefinedMetrics");

    d_orientationWidget_dataContainingPredefinedMetrics->label()->setText("select orientation of data containg predefined metrics");

    this->connect(d_orientationWidget_dataContainingPredefinedMetrics, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->orientation_dataContainingPredefinedMetrics = orientation;});

    d_orientationWidget_dataContainingPredefinedMetrics->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_dataContainingPredefinedMetrics, 4, 0, 1, 2);


    d_comboBoxWidget_variableIdentifierPredefinedMetrics = new ComboBoxWidget(this, "d_comboBoxWidget_variableIdentifierPredefinedMetrics");

    this->connect(d_comboBoxWidget_variableIdentifierPredefinedMetrics->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->variableIdentifierPredefinedMetrics = text;});

    this->connect(d_comboBoxWidget_variableIdentifierPredefinedMetrics->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->variableIndexPredefinedMetrics = index;});

    d_comboBoxWidget_variableIdentifierPredefinedMetrics->label()->setText("select variable containing predefined metrics");

    d_gridLayout->addWidget(d_comboBoxWidget_variableIdentifierPredefinedMetrics, 5, 0, 1, 1);


    d_itemSelectorWidget_itemsDataContainingPredefinedMetrics = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemsDataContainingPredefinedMetrics");

    d_itemSelectorWidget_itemsDataContainingPredefinedMetrics->label()->setText("select items in data containing predefined metrics");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemsDataContainingPredefinedMetrics, 5, 1, 1, 1);

    this->connect(d_itemSelectorWidget_itemsDataContainingPredefinedMetrics->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers_dataContainingPredefinedMetrics = itemLabels; static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes_dataContainingPredefinedMetrics = itemIndexes;});


    ComboBoxWidget * comboBoxWidget_transformFunction = new ComboBoxWidget(this, "comboBoxWidget_transformFunction");

    this->connect(comboBoxWidget_transformFunction->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->transformLabel = text;});

    comboBoxWidget_transformFunction->label()->setText("select transform function");

    comboBoxWidget_transformFunction->comboBox()->addItems({"value = value - metric", "value = metric - value", "value = value + metric", "value = value / metric", "value = metric / value", "value = value * metric"});

    comboBoxWidget_transformFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_transformFunction, 6, 0, 1, 2);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 7, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 8, 0, 1, 2);


    this->connect(d_orientationWidget_dataToTransform, &OrientationWidget::orientationChanged, this, &TransformDataWithPredefinedMetricsDialog::orientationDataToTransformChanged);

    this->connect(comboBoxWidget_dataToTransform->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientationDataToTransformChanged(d_orientationWidget_dataToTransform->orientation());});

    this->orientationDataToTransformChanged(d_orientationWidget_dataToTransform->orientation());


    this->connect(d_orientationWidget_dataContainingPredefinedMetrics, &OrientationWidget::orientationChanged, this, &TransformDataWithPredefinedMetricsDialog::orientationDataContainingPredefinedMetricsChanged);

    this->connect(comboBoxWidget_dataContainingPredefinedMetrics->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientationDataContainingPredefinedMetricsChanged(d_orientationWidget_dataContainingPredefinedMetrics->orientation());});

    this->orientationDataContainingPredefinedMetricsChanged(d_orientationWidget_dataContainingPredefinedMetrics->orientation());

}

void TransformDataWithPredefinedMetricsDialog::orientationDataToTransformChanged(Qt::Orientation orientation)
{

    TransformDataWithPredefinedMetricsParameters *transformDataWithPredefinedMetricsParameters = static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variablesDataToTransform->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->nonSelectedIdentifiers(orientation), transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variablesDataToTransform->itemSelectorModel_selected()->setItemLabelsAndIndexes(transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->identifiers(orientation, true), transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->indexes(orientation, true));


    d_itemSelectorWidget_itemsDataToTransform->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_itemsDataToTransform->itemSelectorModel_selected()->setItemLabelsAndIndexes(transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), transformDataWithPredefinedMetricsParameters->baseAnnotatedMatrix_dataToTranform->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void TransformDataWithPredefinedMetricsDialog::orientationDataContainingPredefinedMetricsChanged(Qt::Orientation orientation)
{

    TransformDataWithPredefinedMetricsParameters *parameters = static_cast<TransformDataWithPredefinedMetricsParameters *>(BaseDialog::d_parameters.data());

    d_comboBoxWidget_variableIdentifierPredefinedMetrics->comboBox()->clear();

    d_comboBoxWidget_variableIdentifierPredefinedMetrics->comboBox()->addItems(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->identifiers(orientation));

    d_itemSelectorWidget_itemsDataContainingPredefinedMetrics->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_itemsDataContainingPredefinedMetrics->itemSelectorModel_selected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}
