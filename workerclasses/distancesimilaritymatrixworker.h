#ifndef DISTANCESIMILARITYMATRIXWORKER_H
#define DISTANCESIMILARITYMATRIXWORKER_H

#include <QObject>
#include <QList>
#include <QString>
#include <QUuid>
#include <QApplication>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include <algorithm>
#include <cmath>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "math/mathoperations.h"
#include "math/fastdistancecorrelation.h"
#include "math/mathdescriptives.h"

class DistanceSimilarityMatrixParameters : public BaseParameters
{

public:

    DistanceSimilarityMatrixParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> data1;

    Qt::Orientation orientation_data1;

    QList<qsizetype> selectedItemIndexes_data1;

    QList<QString> selectedItemIdentifiers_data1;

    QList<qsizetype> selectedVariableIndexes_data1;

    QList<QString> selectedVariableIdentifiers_data1;

    QSharedPointer<BaseAnnotatedMatrix> data2;

    Qt::Orientation orientation_data2;

    QList<qsizetype> selectedItemIndexes_data2;

    QList<QString> selectedItemIdentifiers_data2;

    QList<qsizetype> selectedVariableIndexes_data2;

    QList<QString> selectedVariableIdentifiers_data2;

    QString distanceFunction;

    QString exportDirectory;

    qsizetype numberOfThreadsToUse;

    QList<QString> selectedResultItems;

    double thresholdForOverlapBasedComparisions;

    double p_rankBiasedOverlap;

    qsizetype maxDepth_rankBiasedOverlap;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class DistanceSimilarityMatrixWorker : public BaseWorker
{

public:

    DistanceSimilarityMatrixWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    QHash<QString, qsizetype> d_itemIdentifierToIndex_data1;

    QHash<QString, qsizetype> d_itemIdentifierToIndex_data2;

    QList<qsizetype> d_alignedItemIndexes_data1;

    QList<qsizetype> d_alignedItemIndexes_data2;

    QList<QString> d_alignedItemIdentifiers;

    QList<double> d_data1_aligned;

    QList<double> d_data2_aligned;

    QList<std::span<double>> d_data1_aligned_rowSpans;

    QList<std::span<double>> d_data2_aligned_rowSpans;

    QList<std::pair<qsizetype, std::span<double>>> d_data1_aligned_rowIndexes_rowSpans;

    QList<std::pair<qsizetype, std::span<double>>> d_data2_aligned_rowIndexes_rowSpans;

    void doWork_() override;

    template<typename T, typename S> void doWork_(QSharedPointer<AnnotatedMatrix<T>> data1, QSharedPointer<AnnotatedMatrix<S>> data2);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    void processForCovarianceFunction();

    void processForPearsonCorrelationFunction();

    void processForSpearmanCorrelationFunction();

    void processForDistanceCovarianceFunction();

    void processForDistanceCorrelationFunction();

    void processForChatterjeeCorrelation();

    void processForDotProduct();

    void processForCountOverlapForAbsoluteValueAboveThreshold();

    void processForPearsonCorrelationForUnionAbsoluteValueAboveThreshold();

    template<typename T, typename S> void processForRankBiasedOverlap(QSharedPointer<AnnotatedMatrix<T>> data1, QSharedPointer<AnnotatedMatrix<S>> data2);

    double rankBiasedOverlap(const std::span<qsizetype> &span1, const std::span<qsizetype> &span2, qsizetype maxDepth, long double p = 0.9);

};

template<typename T, typename S>
void DistanceSimilarityMatrixWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> data1, QSharedPointer<AnnotatedMatrix<S>> data2)
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    if (parameters->distanceFunction == "rank-biased overlap")
        this->processForRankBiasedOverlap(data1, data2);

    // Matching items between new data and independent components
    ////////////////

    QUuid uuidProgress = this->startProgress("matching items between data and independent components", 0, 0);

    for (qsizetype i = 0; i < parameters->selectedItemIndexes_data1.count(); ++i)
        d_itemIdentifierToIndex_data1.insert(parameters->selectedItemIdentifiers_data1.at(i), parameters->selectedItemIndexes_data1.at(i));

    for (qsizetype i = 0; i < parameters->selectedItemIndexes_data2.count(); ++i)
        d_itemIdentifierToIndex_data2.insert(parameters->selectedItemIdentifiers_data2.at(i), parameters->selectedItemIndexes_data2.at(i));

    QHashIterator<QString, qsizetype> it1(d_itemIdentifierToIndex_data1);

    while (it1.hasNext()) {

        it1.next();

        qsizetype matchedItemIndex = d_itemIdentifierToIndex_data2.value(it1.key(), -1);

        if (matchedItemIndex != -1) {

            d_alignedItemIndexes_data1.append(it1.value());

            d_alignedItemIndexes_data2.append(matchedItemIndex);

            d_alignedItemIdentifiers.append(it1.key());

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (d_alignedItemIndexes_data1.isEmpty()) {

        this->reportError("no items could be matched between data 1 and data 2");

        this->requestInterruption();

        return;

    }

    this->reportMessage("number of overlapping items : " + QString::number(d_alignedItemIdentifiers.count()));

    ////////////////

    // Fetching data 1
    ////////////////

    uuidProgress = this->startProgress("fetching data 1", 0, 0);

    if (parameters->orientation_data1 == Qt::Vertical)
        d_data1_aligned = data1->template sliced<double>(parameters->selectedVariableIndexes_data1, d_alignedItemIndexes_data1);
    else {

        d_data1_aligned = data1->template sliced<double>(d_alignedItemIndexes_data1, parameters->selectedVariableIndexes_data1);

        MathOperations::transposeInplace(d_data1_aligned, parameters->selectedVariableIndexes_data1.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // Fetching data 2
    ////////////////

    uuidProgress = this->startProgress("fetching data 2", 0, 0);

    if (parameters->orientation_data2 == Qt::Vertical)
        d_data2_aligned = data2->template sliced<double>(parameters->selectedVariableIndexes_data2, d_alignedItemIndexes_data2);
    else {

        d_data2_aligned = data2->template sliced<double>(d_alignedItemIndexes_data2, parameters->selectedVariableIndexes_data2);

        MathOperations::transposeInplace(d_data2_aligned, parameters->selectedVariableIndexes_data2.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////


    // Preprocessing data 1 and data 2
    ////////////////

    d_data1_aligned_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(d_data1_aligned, d_alignedItemIndexes_data1.count());

    d_data2_aligned_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(d_data2_aligned, d_alignedItemIndexes_data2.count());

    d_data1_aligned_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data1_aligned, d_alignedItemIndexes_data1.count());

    d_data2_aligned_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(d_data2_aligned, d_alignedItemIndexes_data2.count());

    if (parameters->distanceFunction == "covariance")
        this->processForCovarianceFunction();
    else if (parameters->distanceFunction == "Pearson R")
        this->processForPearsonCorrelationFunction();
    else if (parameters->distanceFunction == "Spearman R")
        this->processForSpearmanCorrelationFunction();
    else if (parameters->distanceFunction == "distance covariance")
        this->processForDistanceCovarianceFunction();
    else if (parameters->distanceFunction == "distance correlation")
        this->processForDistanceCorrelationFunction();
    else if (parameters->distanceFunction == "dot product")
        this->processForDotProduct();
    else if (parameters->distanceFunction == "count overlap for ABS(value) > threshold")
        this->processForCountOverlapForAbsoluteValueAboveThreshold();
    else if (parameters->distanceFunction == "Pearson R for union ABS(value) > threshold")
        this->processForPearsonCorrelationForUnionAbsoluteValueAboveThreshold();
    else if (parameters->distanceFunction == "Chatterjee correlation")
        this->processForChatterjeeCorrelation();

}

template<typename T, typename S>
void DistanceSimilarityMatrixWorker::processForRankBiasedOverlap(QSharedPointer<AnnotatedMatrix<T>> data1, QSharedPointer<AnnotatedMatrix<S>> data2)
{

    QSharedPointer<DistanceSimilarityMatrixParameters> parameters(d_data->d_parameters.dynamicCast<DistanceSimilarityMatrixParameters>());

    // Mapping item identifiers to coded item identifiers
    ////////////////

    QHash<QString, qsizetype> itemIdentifierToCodedItemIdentifier;

    for (qsizetype i = 0; i < parameters->selectedItemIndexes_data1.count(); ++i) {

        if (!itemIdentifierToCodedItemIdentifier.contains(parameters->selectedItemIdentifiers_data1.at(i)))
            itemIdentifierToCodedItemIdentifier.insert(parameters->selectedItemIdentifiers_data1.at(i), itemIdentifierToCodedItemIdentifier.count());

    }

    for (qsizetype i = 0; i < parameters->selectedItemIndexes_data2.count(); ++i) {

        if (!itemIdentifierToCodedItemIdentifier.contains(parameters->selectedItemIdentifiers_data2.at(i)))
            itemIdentifierToCodedItemIdentifier.insert(parameters->selectedItemIdentifiers_data2.at(i), itemIdentifierToCodedItemIdentifier.count());

    }

    QList<qsizetype> codedData1;

    codedData1.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedItemIndexes_data1.count());

    QList<qsizetype> codedData2;

    codedData2.reserve(parameters->selectedVariableIndexes_data2.count() * parameters->selectedItemIndexes_data2.count());

    ////////////////

    // Fetching data 1
    ////////////////

    QUuid uuidProgress = this->startProgress("fetching data 1", 0, parameters->selectedVariableIndexes_data1.count());

    for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data1.count(); ++i) {

        QList<T> list;

        if (parameters->orientation_data1 == Qt::Vertical)
            list = data1->sliced({parameters->selectedVariableIndexes_data1.at(i)}, parameters->selectedItemIndexes_data1);
        else
            list = data1->sliced(parameters->selectedItemIndexes_data1, {parameters->selectedVariableIndexes_data1.at(i)});

        QList<std::pair<qsizetype, T>> pair_codedItemIdentifier_value;

        pair_codedItemIdentifier_value.reserve(parameters->selectedItemIndexes_data1.count());

        for (qsizetype j = 0; j < parameters->selectedItemIndexes_data1.count(); ++j)
            pair_codedItemIdentifier_value.append(std::pair<qsizetype, T>(itemIdentifierToCodedItemIdentifier.value(parameters->selectedItemIdentifiers_data1.at(j)), list.at(j)));

        std::sort(pair_codedItemIdentifier_value.begin(), pair_codedItemIdentifier_value.end(), [](const std::pair<qsizetype, T> &pair1, const std::pair<qsizetype, T> &pair2){ return pair1.second > pair2.second;});

        for (qsizetype j = 0; j < pair_codedItemIdentifier_value.count(); ++j)
            codedData1.append(pair_codedItemIdentifier_value.at(j).first);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // Fetching data 2
    ////////////////

    uuidProgress = this->startProgress("fetching data 2", 0, parameters->selectedVariableIndexes_data2.count());

    for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

        QList<T> list;

        if (parameters->orientation_data2 == Qt::Vertical)
            list = data2->sliced({parameters->selectedVariableIndexes_data2.at(i)}, parameters->selectedItemIndexes_data2);
        else
            list = data2->sliced(parameters->selectedItemIndexes_data2, {parameters->selectedVariableIndexes_data2.at(i)});

        QList<std::pair<qsizetype, T>> pair_codedItemIdentifier_value;

        pair_codedItemIdentifier_value.reserve(parameters->selectedItemIndexes_data2.count());

        for (qsizetype j = 0; j < parameters->selectedItemIndexes_data2.count(); ++j)
            pair_codedItemIdentifier_value.append(std::pair<qsizetype, T>(itemIdentifierToCodedItemIdentifier.value(parameters->selectedItemIdentifiers_data2.at(j)), list.at(j)));

        std::sort(pair_codedItemIdentifier_value.begin(), pair_codedItemIdentifier_value.end(), [](const std::pair<qsizetype, T> &pair1, const std::pair<qsizetype, T> &pair2){ return pair1.second > pair2.second;});

        for (qsizetype j = 0; j < pair_codedItemIdentifier_value.count(); ++j)
            codedData2.append(pair_codedItemIdentifier_value.at(j).first);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    QList<std::span<qsizetype>> codedData1_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(codedData1, parameters->selectedItemIndexes_data1.count());

    QList<std::span<qsizetype>> codedData2_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(codedData2, parameters->selectedItemIndexes_data2.count());


    QList<double> rankBiasedOverlapMatrix;

    rankBiasedOverlapMatrix.reserve(parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());


    uuidProgress = this->startProgress("calculating rank-biased overlap matrix", 0, parameters->selectedVariableIndexes_data1.count() * parameters->selectedVariableIndexes_data2.count());

    auto mapFunctor = [this, &uuidProgress, &parameters, &codedData2_rowSpans](const std::span<qsizetype> &codedData1_span) {

        QList<double> intermediateResults(parameters->selectedVariableIndexes_data2.count(), 0);

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes_data2.count(); ++i) {

            intermediateResults[i] = this->rankBiasedOverlap(codedData1_span, codedData2_rowSpans.at(i), parameters->maxDepth_rankBiasedOverlap, parameters->p_rankBiasedOverlap);

            if (this->thread()->isInterruptionRequested())
                return intermediateResults;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return intermediateResults;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    rankBiasedOverlapMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, codedData1_rowSpans, std::function<QList<double>(const std::span<qsizetype> &codedData1_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);


    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("distance/similarity matrix")) {

        AnnotatedMatrix<double> *result_rankBiasedOverlapMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers_data1, parameters->selectedVariableIdentifiers_data2, rankBiasedOverlapMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("rank-biased overlap matrix", result_rankBiasedOverlapMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_rankBiasedOverlapMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "rank_biased_overlap_matrix");

        }

    }

    if (parameters->selectedResultItems.contains("distance/similarity diagonal")) {

        QList<QString> diagonal;

        QList<QString> rowIdentifiers;

        diagonal.reserve(parameters->selectedVariableIndexes_data1.count() * 3);

        qsizetype dim = std::min(parameters->selectedVariableIndexes_data1.count(), parameters->selectedVariableIndexes_data2.count());

        for (qsizetype i = 0; i < dim; ++i) {

            rowIdentifiers.append(QString::number(i + 1));

            diagonal.append(parameters->selectedVariableIdentifiers_data1.at(i));

            diagonal.append(parameters->selectedVariableIdentifiers_data2.at(i));

            diagonal.append(QString::number(rankBiasedOverlapMatrix.at(i * parameters->selectedVariableIndexes_data2.count() + i)));

        }

        AnnotatedMatrix<QString> *result_diagonal = new AnnotatedMatrix<QString>(rowIdentifiers, {"from", "to", "rank-biased overlap"}, diagonal);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data1->property("uuidProject").toUuid(), new Result("diagonal of rank-biased overlap matrix", result_diagonal));
        else {

            QSharedPointer<AnnotatedMatrix<QString>> sharedPointerToAnnotatedMatrix(result_diagonal);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "diagonal_of_rank_biased_overlap_matrix");

        }

    }

}

#endif // DISTANCESIMILARITYMATRIXWORKER_H
