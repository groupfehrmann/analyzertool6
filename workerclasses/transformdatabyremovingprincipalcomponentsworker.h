#ifndef TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSWORKER_H
#define TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include <algorithm>

#include <boost/math/statistics/univariate_statistics.hpp>
#include "boost/range/numeric.hpp"

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"
#include "containers/result.h"
#include "math/trip.h"
#include "math/mathutilityfunctions.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

class TransformDataByRemovingPrincipalComponentsParameters : public BaseParameters
{

public:

    TransformDataByRemovingPrincipalComponentsParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString exportDirectory;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QList<qsizetype> selectedComponentToRemoveIndexes;

    QList<QString> selectedComponentToRemoveIdentifiers;

    QList<QString> selectedResultItems;

    qsizetype threshold_maximumNumberOfComponents;

    double threshold_cumulativeExplainedVariance;

    QString mode;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransformDataByRemovingPrincipalComponentsWorker : public BaseWorker
{

public:

    TransformDataByRemovingPrincipalComponentsWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void TransformDataByRemovingPrincipalComponentsWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<TransformDataByRemovingPrincipalComponentsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataByRemovingPrincipalComponentsParameters>());

    // Fetching data
    ////////////////

    QUuid uuidProgress = this->startProgress("fetching data", 0, 0);

    QList<double> matrix;

    QList<double> means;

    QList<double> l2Norms;

    if (parameters->orientation == Qt::Vertical)
        matrix = annotatedMatrix->template sliced<double>(parameters->selectedVariableIndexes, parameters->selectedItemIndexes);
    else {

        matrix = annotatedMatrix->template sliced<double>(parameters->selectedItemIndexes, parameters->selectedVariableIndexes);

        MathOperations::transposeInplace(matrix, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // Setting up NaN-mode
    ////////////////

    uuidProgress = this->startProgress("check data for NaNs", 0, 0);

    bool enableNaNMode = false;

    for (qsizetype i = 0; i < matrix.count(); ++i) {

        if (std::isnan(matrix.at(i))) {

            enableNaNMode = true;

            break;

        }
    }

    this->stopProgress(uuidProgress);

    // Creating covariance/correlation matrix
    ////////////////

    if (parameters->mode == "covariance")
        uuidProgress = this->startProgress("allocating memory for covariance matrix", 0, 0);
    else
        uuidProgress = this->startProgress("allocating memory for correlation matrix", 0, 0);

    QList<double> covarianceOrCorrelationMatrix(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

    this->stopProgress(uuidProgress);


    if (enableNaNMode) {

        this->reportWarning("detected NaN, switching to NaN mode (experimental)");

        uuidProgress = this->startProgress("flagging NaNs", 0, 0);

        QList<float> transformedMatrix(matrix.count());

        std::transform(matrix.begin(), matrix.end(), transformedMatrix.begin(), [](const double &value){return std::isnan(value) ? 0.0 : 1.0;});

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        QList<float> countOverlappingNonNaNsMatrix;

        if (parameters->mode == "covariance") {

            uuidProgress = this->startProgress("creating matrix with counts for overlapping non-NaNs", 0, 0);

            countOverlappingNonNaNsMatrix.resize(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

            cblas_ssyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), 1.0, transformedMatrix.data(), parameters->selectedItemIndexes.count(), 0.0, countOverlappingNonNaNsMatrix.data(), parameters->selectedVariableIndexes.count());

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->stopProgress(uuidProgress);

        }

        QList<std::span<double>> rowSpansMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count());

        uuidProgress = this->startProgress("center variables", 0, rowSpansMatrix.size());

        means.reserve(rowSpansMatrix.count());

        for (const std::span<double> &span : rowSpansMatrix) {

            double mean = MathDescriptives::meanIgnoreNans(span);

            for (double &value : span)
                value -= mean;

            means.append(mean);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        if (parameters->mode == "correlation") {

            uuidProgress = this->startProgress("scale variables", 0, rowSpansMatrix.size());

            l2Norms.reserve(rowSpansMatrix.count());

            for (const std::span<double> &span : rowSpansMatrix) {

                double l2Norm = MathDescriptives::l2NormIgnoreNans(span);

                for (double &value : span)
                    value /= l2Norm;

                l2Norms.append(l2Norm);

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgressWithOne(uuidProgress);

            }

            this->stopProgress(uuidProgress);

        }

        uuidProgress = this->startProgress("set NaNs to zero", 0, 0);

        std::transform(matrix.begin(), matrix.end(), matrix.begin(), [](const double &value){return std::isnan(value) ? 0.0 : value;});

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);

        if (parameters->mode == "covariance")
            uuidProgress = this->startProgress("calculate covariance matrix", 0, 0);
        else
            uuidProgress = this->startProgress("calculate correlation matrix", 0, 0);

        cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), 1.0, matrix.data(), parameters->selectedItemIndexes.count(), 0.0, covarianceOrCorrelationMatrix.data(), parameters->selectedVariableIndexes.count());

        if (parameters->mode == "covariance") {

            for (qsizetype i = 0; i < covarianceOrCorrelationMatrix.count(); ++i)
                covarianceOrCorrelationMatrix[i] /= (static_cast<double>(countOverlappingNonNaNsMatrix.at(i)) - 1.0);

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);

    } else {

        QList<std::span<double>> rowSpansMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count());

        uuidProgress = this->startProgress("center variables", 0, rowSpansMatrix.size());

        means.reserve(rowSpansMatrix.count());

        for (const std::span<double> &span : rowSpansMatrix) {

            double mean = boost::math::statistics::mean(span);

            for (double &value : span)
                value -= mean;

            means.append(mean);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        uuidProgress = this->startProgress("center variables", 0, rowSpansMatrix.size());

        if (parameters->mode == "correlation") {

            l2Norms.reserve(rowSpansMatrix.count());

            for (const std::span<double> &span : rowSpansMatrix) {

                double l2Norm = boost::math::tools::l2_norm(span.begin(), span.end());

                for (double &value : span)
                    value /= l2Norm;

                l2Norms.append(l2Norm);

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgressWithOne(uuidProgress);

            }

            this->stopProgress(uuidProgress);

        }

        if (parameters->mode == "covariance") {

            uuidProgress = this->startProgress("calculate covariance matrix", 0, 0);

            cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), (1.0 / (static_cast<double>(parameters->selectedItemIndexes.count()) - 1.0)), matrix.data(), parameters->selectedItemIndexes.count(), 0.0, covarianceOrCorrelationMatrix.data(), parameters->selectedVariableIndexes.count());

        } else {

            uuidProgress = this->startProgress("calculate correlation matrix", 0, 0);

           cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), 1.0, matrix.data(), parameters->selectedItemIndexes.count(), 0.0, covarianceOrCorrelationMatrix.data(), parameters->selectedVariableIndexes.count());

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);

    }

    if (parameters->selectedResultItems.contains("covariance or correlation matrix")) {

        AnnotatedMatrix<double> *result_covarianceOrCorrelationMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, parameters->selectedVariableIdentifiers, covarianceOrCorrelationMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result((parameters->mode == "covariance") ? "covariance matrix" : "correlation matrix", result_covarianceOrCorrelationMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_covarianceOrCorrelationMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, (parameters->mode == "covariance") ? "covariance_matrix" : "correlation_matrix");

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    ////////////////

    // Obtaining eigenvalues and eigenvectors
    ////////////////

    uuidProgress = this->startProgress("calculate eigenvalues and eigenvectors", 0, 0);

    double totalExplainedVariance = 0.0;

    for (qsizetype i = 0; i < covarianceOrCorrelationMatrix.count(); i = i + parameters->selectedVariableIndexes.count() + 1)
        totalExplainedVariance += std::abs(covarianceOrCorrelationMatrix.at(i));

    qsizetype m;

    QList<double> w(parameters->selectedVariableIndexes.count(), 0.0);

    QList<double> z(parameters->selectedVariableIndexes.count() * parameters->selectedVariableIndexes.count(), 0.0);

    QList<long long> isuppz(parameters->selectedVariableIndexes.count() * 2, 0.0);

    int info = LAPACKE_dsyevr(LAPACK_ROW_MAJOR, 'V', 'I', 'U', parameters->selectedVariableIndexes.count(), covarianceOrCorrelationMatrix.data(), parameters->selectedVariableIndexes.count(), 0.0, 1.0, 1, parameters->selectedVariableIndexes.count(), -1.0, &m, w.data(), z.data(), parameters->selectedVariableIndexes.count(), isuppz.data());

    if (info < 0)
        this->reportError("DSYEVR: " + QString::number(info) + "th parameter had illegal value");
    else if (info > 0)
        this->reportError("DSYEVR: Error code -> " + QString::number(info));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    // covariance matrix not needed anymore
    covarianceOrCorrelationMatrix.clear();

    std::reverse(w.begin(), w.end());

    this->startProgress("reversing eigenvectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, parameters->selectedVariableIndexes.count()), [](std::span<double> &span){std::reverse(span.begin(), span.end());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("calculate explained variance", 0, 0);

    QList<double> eigenvaluesSummary;

    double sumExplainedVariance = 0.0;

    QList<QString> componentLabels;

    qsizetype numberOfValidComponents = 0;

    double threshold = parameters->threshold_cumulativeExplainedVariance / 100.0;

    for (qsizetype i = 0; i < parameters->threshold_maximumNumberOfComponents; ++i) {

        double eigenvalue = w.at(i);

        if (eigenvalue > std::numeric_limits<double>::epsilon()) {

            double explainedVariance = std::fabs(eigenvalue) / totalExplainedVariance;

            sumExplainedVariance += explainedVariance;

            ++numberOfValidComponents;

            componentLabels.append("component " + QString::number(i + 1));

            eigenvaluesSummary << eigenvalue << explainedVariance * 100.0 << sumExplainedVariance * 100.0;

            if (almost_equal(threshold, sumExplainedVariance, 6) || sumExplainedVariance > threshold)
                break;

        }

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // Filtering eigenvalues and eigenvectors that do not meet the thresholds
    ////////////////

    uuidProgress = this->startProgress("filtering eigenvalues and eigenvectors that do not meet the thresholds", 0, 0);

    w.resize(numberOfValidComponents);

    MathOperations::transposeInplace(z, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    z.resize(numberOfValidComponents * parameters->selectedVariableIndexes.count());

    MathOperations::transposeInplace(z, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    if (parameters->selectedResultItems.contains("eigenvalues")) {

        AnnotatedMatrix<double> *result_eigenvalues = new AnnotatedMatrix<double>(componentLabels, {"eigenvalues", "explained variance", "cumulative explained variance"}, std::move(eigenvaluesSummary));

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("eigenvalues", result_eigenvalues));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_eigenvalues);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "eigenvalues");

        }

    }

    if (parameters->selectedResultItems.contains("eigenvectors")) {

        AnnotatedMatrix<double> *result_eigenvectors = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, z);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("eigenvectors", result_eigenvectors));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_eigenvectors);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "eigenvectors");

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    ////////////////

    // Creating dewhitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::sqrt(value);});

    QList<double> dewhiteningFactorloadingsMatrix = z;

    this->startProgress((parameters->mode == "covariance") ? "creating dewhitening matrix" : "creating dewhitening/factorloadings matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(dewhiteningFactorloadingsMatrix, numberOfValidComponents), [&w](std::span<double> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), std::multiplies<double>());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (parameters->selectedResultItems.contains("dewhitening/factorloadings matrix")) {

        AnnotatedMatrix<double> *result_dewhiteningFactorloadingsMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiers, componentLabels, dewhiteningFactorloadingsMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result((parameters->mode == "covariance") ? "dewhitening matrix" : "factorloadings", result_dewhiteningFactorloadingsMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_dewhiteningFactorloadingsMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, (parameters->mode == "covariance") ? "dewhitening_matrix" : "factorloadings");

        }

    }

    ////////////////

    // Creating whitening matrix
    ////////////////

    std::for_each(w.begin(), w.end(), [](double &value){value = std::pow(value, -1);});

    QList<double> whiteningMatrix = z;

    // eigenvectors not needed anymore
    z.clear();

    this->startProgress("creating whitening matrix", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(whiteningMatrix, numberOfValidComponents), [&w](std::span<double> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), std::multiplies<double>());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    // eigenvalues not needed anymore
    w.clear();

    uuidProgress = this->startProgress("transposing whitening matrix", 0, 0);

    MathOperations::transposeInplace(whiteningMatrix, numberOfValidComponents, parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (parameters->selectedResultItems.contains("whitening matrix")) {

        AnnotatedMatrix<double> *result_whiteningMatrix = new AnnotatedMatrix<double>(componentLabels, parameters->selectedVariableIdentifiers, whiteningMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("whitening matrix", result_whiteningMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_whiteningMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "whitening_matrix");

        }

    }

    ////////////////

    // calculating principal component scores
    ////////////////

    uuidProgress = this->startProgress("calculating principal component scores", 0, 0);

    QList<double> principalComponentScores(numberOfValidComponents * parameters->selectedItemIndexes.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, numberOfValidComponents, parameters->selectedItemIndexes.count(), parameters->selectedVariableIndexes.count(), 1.0, whiteningMatrix.data(), parameters->selectedVariableIndexes.count(), matrix.data(), parameters->selectedItemIndexes.count(), 0.0, principalComponentScores.data(), parameters->selectedItemIndexes.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    whiteningMatrix.clear();

    if (parameters->selectedResultItems.contains("principal component scores")) {

        AnnotatedMatrix<double> *result_principalComponentScores = new AnnotatedMatrix<double>(componentLabels, parameters->selectedItemIdentifiers, principalComponentScores);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("principal component scores", result_principalComponentScores));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_principalComponentScores);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "principal_component_scores");

        }

    }

    ////////////////

    // removing principal components
    ////////////////

    uuidProgress = this->startProgress("removing principal components", 0, 0);

    MathOperations::transposeInplace(dewhiteningFactorloadingsMatrix, componentLabels.count(), parameters->numberOfThreadsToUse);

    QList<std::span<double>> rowSpans_dewhiteningFactorloadingsMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(dewhiteningFactorloadingsMatrix, parameters->selectedVariableIdentifiers.count());

    for (qsizetype index : parameters->selectedComponentToRemoveIndexes) {

        if (index < rowSpans_dewhiteningFactorloadingsMatrix.size()) {

            for (double &value : rowSpans_dewhiteningFactorloadingsMatrix[index])
                value = 0.0;

        }

    }

    MathOperations::transposeInplace(dewhiteningFactorloadingsMatrix, parameters->selectedVariableIdentifiers.count(), parameters->numberOfThreadsToUse);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, parameters->selectedVariableIndexes.count(), parameters->selectedItemIndexes.count(), numberOfValidComponents, 1.0, dewhiteningFactorloadingsMatrix.data(), numberOfValidComponents, principalComponentScores.data(), parameters->selectedItemIndexes.count(), 0.0, matrix.data(), parameters->selectedItemIndexes.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    dewhiteningFactorloadingsMatrix.clear();

    principalComponentScores.clear();

    /*
    // Correct for mean and optional L2norm
    ////////////////

    QList<std::span<double>> rowSpans_matrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count());

    if (parameters->mode == "covariance") {

        for (qsizetype i = 0; i < rowSpans_matrix.count(); ++i) {

            const double &mean(means.at(i));

            for (double &value : rowSpans_matrix[i])
                value += mean;

        }

    } else {

        for (qsizetype i = 0; i < rowSpans_matrix.count(); ++i) {

            const double &mean(means.at(i));

            const double &l2Norm(l2Norms.at(i));

            for (double &value : rowSpans_matrix[i])
                value = (value * l2Norm) + mean;

        }

    }

    ////////////////
*/
    // Fetching data
    ////////////////

    QList<std::reference_wrapper<T>> matrixReferences;

    if (parameters->orientation == Qt::Vertical)
        matrixReferences = annotatedMatrix->sliced_references(parameters->selectedVariableIndexes, parameters->selectedItemIndexes);
    else {

        matrixReferences = annotatedMatrix->sliced_references(parameters->selectedItemIndexes, parameters->selectedVariableIndexes);

        MathOperations::transposeInplace(matrixReferences, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    }

    for (qsizetype i = 0; i < matrixReferences.count(); ++i)
        matrixReferences[i].get() = matrix.at(i);

    ////////////////


    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

}

#endif // TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSWORKER_H
