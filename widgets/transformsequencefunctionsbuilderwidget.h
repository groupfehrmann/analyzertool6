#ifndef TRANSFORMSEQUENCEFUNCTIONSBUILDERWIDGET_H
#define TRANSFORMSEQUENCEFUNCTIONSBUILDERWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QComboBox>
#include <QTableWidget>
#include <QList>
#include <QString>
#include <QCompleter>
#include <QLineEdit>
#include <QDoubleValidator>

#include <functional>

#include <boost/math/statistics/univariate_statistics.hpp>

#include "dialogs/basedialog.h"
#include "math/mathoperations.h"
#include "math/johnsontransformation.h"

namespace Ui {

    class TransformSequenceFunctionsBuilderWidget;

}

class TransformSequenceFunctionsBuilderWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TransformSequenceFunctionsBuilderWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    void readWidgetSettings();

    void writeWidgetSettings();

    ~TransformSequenceFunctionsBuilderWidget();

    const QList<QString> &selectedTransformFunctionDescriptionsWithParameterReplacement();

    const QList<std::function<void (QList<double> &)>> &selectedTransformFunctions();

private:

    Ui::TransformSequenceFunctionsBuilderWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

    QList<QString> d_transformFunctionDescriptions;

    QList<QString> d_selectedTransformFunctionDescriptionsWithParameterReplacement;

    QList<std::function<void (QList<double> &)>> d_selectedTransformFunctions;

private slots:

    void pushButton_add_clicked();

    void pushButton_removeSelected_clicked();

    void comboBox_transformFunctions_currentIndexChanged(qsizetype index);

signals:

    void transformSequenceChanged();

};

#endif // TRANSFORMSEQUENCEFUNCTIONSBUILDERWIDGET_H
