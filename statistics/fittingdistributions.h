#ifndef FITTINGDISTRIBUTIONS_H
#define FITTINGDISTRIBUTIONS_H

#include <QList>

#include <algorithm>
#include <functional>
#include <span>
#include <cmath>

#include "boost/math/statistics/univariate_statistics.hpp"
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/math/distributions/empirical_cumulative_distribution_function.hpp>
#include "boost/math/distributions/students_t.hpp"
#include <boost/math/distributions/skew_normal.hpp>

#include "math/mins.h"
#include "statistics/kolmogorovsmirnovtest.h"
#include "math/amoeba.h"

namespace FittingDistributions {

    std::function<double (const double &)> cdf_generalizedParetoTypeDistribution(const QList<double> &sortedPermutationDistributionAscending, bool &succes, qsizetype nExceedances = 250, qsizetype nPermutationsInBootstrap = 1000);

    double fitTDistribution(const QList<double> &sortedEmpericalDistributionAscending);

    double fitTDistribution(const std::span<double> &sortedEmpericalDistributionAscending);

    QList<double> fitSkewNormal(const std::span<double> &distribution);

}

#endif // FITTINGDISTRIBUTIONS_H
