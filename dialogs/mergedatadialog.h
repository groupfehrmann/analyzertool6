#ifndef MERGEDATADIALOG_H
#define MERGEDATADIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>

#include "basedialog.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/mergedataworker.h"

class MergeDataDialog : public BaseDialog
{

    Q_OBJECT

public:

    MergeDataDialog(BaseDialog *parent = nullptr, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices = QList<QSharedPointer<BaseAnnotatedMatrix>>());

protected:

    void initWidgets_() override;

private:

    QList<QSharedPointer<BaseAnnotatedMatrix>> d_baseAnnotatedMatrices;

    ItemSelectorWidget *d_itemSelectorWidget_baseAnnotedMatrices;

    OrientationWidget *d_orientationWidget;

};

#endif // MERGEDATADIALOG_H
