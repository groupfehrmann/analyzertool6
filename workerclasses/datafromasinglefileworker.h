#ifndef DATAFROMASINGLEFILEWORKER_H
#define DATAFROMASINGLEFILEWORKER_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QUuid>
#include <QRegularExpression>
#include <QApplication>

#include <utility>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "utils/conversionfunctions.h"

class DataFromASingleFileParameters : public BaseParameters
{

public:

    DataFromASingleFileParameters(QObject *parent = nullptr);

    QString delimiter;

    bool firstTokenInHeaderDefinesColumnWithRowIdentifiers;

    qsizetype indexOfColumnDefiningFirstDataColumn;

    qsizetype indexOfColumnDefiningRowIdentifiers;

    qsizetype indexOfColumnDefiningSecondDataColumn;

    qsizetype lineNumberOfFirstDataLine;

    qsizetype lineNumberOfHeader;

    qsizetype lineNumberOfLastDataLine;

    QString pathToFile;

    QString typeName;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class DataFromASingleFileWorker : public BaseWorker
{

public:

    DataFromASingleFileWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(AnnotatedMatrix<T> *annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void DataFromASingleFileWorker::doWork_(AnnotatedMatrix<T> *annotatedMatrix)
{

    QSharedPointer<DataFromASingleFileParameters> parameters(d_data->d_parameters.dynamicCast<DataFromASingleFileParameters>());

    QFile fileIn(parameters->pathToFile);

    if (!fileIn.exists() || !fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        delete annotatedMatrix;

        return;

    }

    QTextStream textStreamIn(&fileIn);

    textStreamIn.setEncoding(QStringConverter::Latin1);

    annotatedMatrix->setProperty("label", QFileInfo(parameters->pathToFile).baseName());

    QUuid uuidProgress = this->startProgress("importing file \"" + parameters->pathToFile + "\"", 0, fileIn.size());

    qsizetype numberOfLinesRead = 0;

    while (!textStreamIn.atEnd() && (numberOfLinesRead < (parameters->lineNumberOfHeader - 1))) {

        textStreamIn.readLine();

        ++numberOfLinesRead;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    QList<QString> tokens;

    qsizetype numberOfTokensInReferenceLine = 0;

    if (parameters->lineNumberOfHeader != -1) {

        if (!parameters->firstTokenInHeaderDefinesColumnWithRowIdentifiers)
            tokens << QString();

        if (!textStreamIn.atEnd()) {

            tokens << textStreamIn.readLine().split(QRegularExpression(parameters->delimiter), Qt::KeepEmptyParts);

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

        if (numberOfLinesRead != (parameters->lineNumberOfHeader)) {

            this->reportError("line number of header (" + QString::number(parameters->lineNumberOfHeader) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + parameters->pathToFile + "\"");

            QThread::currentThread()->requestInterruption();

            delete annotatedMatrix;

            return;

        }

        numberOfTokensInReferenceLine = tokens.count();

    }

    qsizetype deltaIndex = (parameters->indexOfColumnDefiningSecondDataColumn != -1) ? parameters->indexOfColumnDefiningSecondDataColumn - parameters->indexOfColumnDefiningFirstDataColumn : tokens.count();

    QList<QString> columnIdentifiers;

    if (parameters->lineNumberOfHeader != -1) {

        for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex)
            columnIdentifiers.append(tokens.at(i).trimmed());

    }

    if (parameters->lineNumberOfHeader != -1) {

        while (!textStreamIn.atEnd() && (numberOfLinesRead < parameters->lineNumberOfFirstDataLine - parameters->lineNumberOfHeader)) {

            textStreamIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    } else {

        while (!textStreamIn.atEnd() && (numberOfLinesRead < parameters->lineNumberOfFirstDataLine - 1)) {

            textStreamIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    }

    QList<T> values;

    if (parameters->lineNumberOfLastDataLine != -1)
        values.reserve(columnIdentifiers.count() * (parameters->lineNumberOfLastDataLine - parameters->lineNumberOfFirstDataLine + 1));

    QList<QString> rowIdentifiers;

    bool setNumberOfTokensInReferenceLine = (parameters->lineNumberOfHeader == -1) ? true : false;

    while (!textStreamIn.atEnd() && ((parameters->lineNumberOfLastDataLine == -1) || (numberOfLinesRead != parameters->lineNumberOfLastDataLine))) {

        tokens = textStreamIn.readLine().split(QRegularExpression(parameters->delimiter), Qt::KeepEmptyParts);

        if (setNumberOfTokensInReferenceLine) {

            for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex)
                columnIdentifiers.append("column_" + QString::number(columnIdentifiers.count()));

            numberOfTokensInReferenceLine = tokens.count();

            setNumberOfTokensInReferenceLine = false;

        }

        ++numberOfLinesRead;

        if (tokens.count() != numberOfTokensInReferenceLine) {

            if (parameters->lineNumberOfHeader != -1)
                this->reportError("the number of tokens read (" + QString::number(tokens.count()) + ") at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the header line");
            else
                this->reportError("the number of tokens read (" + QString::number(tokens.count()) + ") at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the first data line");

            QThread::currentThread()->requestInterruption();

            delete annotatedMatrix;

            return;

        }

        if (parameters->indexOfColumnDefiningRowIdentifiers != -1)
            rowIdentifiers.append(tokens.at(parameters->indexOfColumnDefiningRowIdentifiers).trimmed());
        else
            rowIdentifiers.append("row_" + QString::number(rowIdentifiers.count()));

        T value;

        for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex) {

            if (!ConversionFunctions::convert(tokens.at(i).trimmed(), value))
                this->reportWarning("could not convert \"" + tokens.at(i).trimmed() + "\" at line " + QString::number(numberOfLinesRead) + " to type \"" + parameters->typeName + "\"");

            values.append(value);

        }

        if (QThread::currentThread()->isInterruptionRequested()) {

            delete annotatedMatrix;

            return;

        }

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    if ((parameters->lineNumberOfLastDataLine != -1) && (parameters->lineNumberOfLastDataLine != numberOfLinesRead)) {

        this->reportError("line number of last data line (" + QString::number(parameters->lineNumberOfLastDataLine) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        delete annotatedMatrix;

        return;

    }

    annotatedMatrix->setData(rowIdentifiers, columnIdentifiers, std::move(values));

    this->reportMessage("number or rows imported : " + QString::number(annotatedMatrix->rowCount()));

    this->reportMessage("number or columns imported : " + QString::number(annotatedMatrix->columnCount()));

    annotatedMatrix->moveToThread(QApplication::instance()->thread());

    emit objectAvailable(QUuid(), annotatedMatrix);

}

#endif // DATAFROMASINGLEFILEWORKER_H
