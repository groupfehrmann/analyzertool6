#include "complexfastica.h"

ComplexFastICA::ComplexFastICA(const QList<std::complex<double> > &whitenedMatrix, const QList<std::complex<double> > &whiteningMatrix, const QList<std::complex<double> > &dewhiteningMatrix, qsizetype nSamples, qsizetype nItems, qsizetype nComponents, int contrastFunction, int maximumNumberOfIterations, double mu, double epsilon, int numberOfThreadsToUse, QObject *parent) :
    QObject(parent),
    d_label("fastICA"),
    d_whitenedMatrix(whitenedMatrix),
    d_whiteningMatrix(whiteningMatrix),
    d_dewhiteningMatrix(dewhiteningMatrix),
    d_nSamples(nSamples),
    d_nItems(nItems),
    d_nComponents(nComponents),
    d_contrastFunction(contrastFunction),
    d_maximumNumberOfIterations(maximumNumberOfIterations),
    d_mu(mu),
    d_epsilon(epsilon),
    d_numberOfThreadsToUse(numberOfThreadsToUse)
{

}

bool ComplexFastICA::run()
{

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    d_previousNumberOfThreadsToUse = MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines();

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    QUuid uuidProgress;

    d_B.resize(d_nComponents * d_nComponents);

    if (d_connectedBaseworker)
        uuidProgress = d_connectedBaseworker->startProgress("Initializing", 0, 0);

    MathOperations::fillWithRandomComplexNumbersFromUniformRealDistribution(d_B, -0.5, 0.5);

    int info = 0;

    MathOperations::orthogonalizeMatrixInplace(d_B, d_nComponents, d_numberOfThreadsToUse, &info);

    if (d_connectedBaseworker) {

        if (info < 0)
            d_connectedBaseworker->reportError(d_label + ": DGESDD: " + QString::number(info) + "th parameter had illegal value");
        else if (info > 0)
            d_connectedBaseworker->reportError(d_label + ": DGESDD: Error code -> " + QString::number(info));

        d_connectedBaseworker->stopProgress(uuidProgress);

    }

    if (info != 0)
        return false;

    QList<std::complex<double>> BOld(d_B.count(), 0.0);

    if (d_connectedBaseworker)
        uuidProgress = d_connectedBaseworker->startProgress("calculate pseudo-covariance", 0, 0);

    QList<std::complex<double>> pseudoCovariance(d_nComponents * d_nComponents, 0.0);

    std::complex<double> alpha = 1.0 / static_cast<double>(d_nItems);

    std::complex<double> beta = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasTrans, d_nComponents, d_nComponents, d_nItems, &alpha, d_whitenedMatrix.data(), d_nItems, d_whitenedMatrix.data(), d_nItems, &beta, pseudoCovariance.data(), d_nComponents);

    if (d_connectedBaseworker)
        d_connectedBaseworker->stopProgress(uuidProgress);

    if (d_connectedBaseworker)
        uuidProgress = d_connectedBaseworker->startProgress(d_label + ": convergences to epsilon", 0, -10000000 * std::log10(d_epsilon));

    double minAbsCos = 0.0;

    for (int round = 0; round < d_maximumNumberOfIterations; round++) {

        if (round != 0)
            d_connectedBaseworker->updateProgressLabel(uuidProgress, d_label + ": converge - iteration " + QString::number(round + 1) + " - mu " + QString::number(d_mu) + " - convergence change: " + QString::number(1.0 - minAbsCos));

        if (round == d_maximumNumberOfIterations - 1) {

            if (d_connectedBaseworker)
                d_connectedBaseworker->reportWarning(d_label + ": no convergence reached in FastICA");

            d_mixingMatrix.resize(d_nSamples * d_nComponents, 0.0);

            cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nSamples, d_nComponents, d_nComponents, &complex_one, d_dewhiteningMatrix.data(), d_nComponents, d_B.data(), d_nComponents, &complex_zero, d_mixingMatrix.data(), d_nComponents);

            d_seperatingMatrix.resize(d_nComponents * d_nSamples, 0.0);

            cblas_zgemm(CblasRowMajor, CblasTrans, CblasNoTrans, d_nComponents, d_nSamples, d_nComponents, &complex_one, d_B.data(), d_nComponents, d_whiteningMatrix.data(), d_nSamples, &complex_zero, d_seperatingMatrix.data(), d_nSamples);

            if (d_connectedBaseworker)
                d_connectedBaseworker->stopProgress(uuidProgress);

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }

        QList<std::complex<double>> tmp(d_B.count(), 0.0);

        cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, &complex_one, BOld.data(), d_nComponents, d_B.data(), d_nComponents, &complex_zero, tmp.data(), d_nComponents);

        minAbsCos = MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquaredMatrix(tmp, d_nComponents);

        if (std::fabs(1.0 - minAbsCos) < d_epsilon) {

            if (d_connectedBaseworker)
                d_connectedBaseworker->reportMessage(d_label + ": convergence after " + QString::number(round) + " iterations");

            d_mixingMatrix.resize(d_nSamples * d_nComponents, 0.0);

            cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nSamples, d_nComponents, d_nComponents, &complex_one, d_dewhiteningMatrix.data(), d_nComponents, d_B.data(), d_nComponents, &complex_zero, d_mixingMatrix.data(), d_nComponents);

            break;

        }

        BOld = d_B;

        QList<std::complex<double>> Y(d_nItems * d_nComponents, 0.0);

        cblas_zgemm(CblasRowMajor, CblasTrans, CblasConjNoTrans, d_nItems, d_nComponents, d_nComponents, &complex_one, d_whitenedMatrix.data(), d_nItems, d_B.data(), d_nComponents, &complex_zero, Y.data(), d_nComponents);

        QList<double> G(Y.count());

        QList<std::complex<double>> temp(Y.count());

        QList<std::complex<double>> Beta1(Y.count());

        switch (d_contrastFunction) {

            case NC_C_FICA_LOG : {

                for (qsizetype i = 0; i < Y.count(); ++i) {

                    const std::complex<double> &Y_i(Y.at(i));

                    double Y_abs_i = std::abs(Y_i);

                    double Y_abs_pow2_i = Y_abs_i * Y_abs_i;

                    double &G_i(G[i]);

                    double mu_plus_Y_abs_pow2_i = d_mu + Y_abs_pow2_i;

                    G_i = 1.0 / mu_plus_Y_abs_pow2_i;

                    double GP_i = -1.0 / (mu_plus_Y_abs_pow2_i * mu_plus_Y_abs_pow2_i);

                    std::complex<double> Y_conj_i = std::conj(Y_i);

                    Beta1[i] = G_i * Y_conj_i;

                    G_i += GP_i * Y_abs_pow2_i;

                    temp[i] = GP_i * Y_conj_i * Y_conj_i;

                }

                break;

            }

            case NC_C_FICA_SQRT : {

                for (qsizetype i = 0; i < Y.count(); ++i) {

                    const std::complex<double> &Y_i(Y.at(i));

                    double Y_abs_i = std::abs(Y_i);

                    double Y_abs_pow2_i = Y_abs_i * Y_abs_i;

                    double &G_i(G[i]);

                    G_i = 1.0 / (2.0 * std::sqrt(d_mu + Y_abs_pow2_i));

                    double GP_i = -1.0 / (4.0 * std::pow(d_mu + Y_abs_pow2_i, 1.5));

                    std::complex<double> Y_conj_i = std::conj(Y_i);

                    Beta1[i] = G_i * Y_conj_i;

                    G_i += GP_i * Y_abs_pow2_i;

                    temp[i] = GP_i * Y_conj_i * Y_conj_i;

                }

                break;

            }

            case NC_C_FICA_KURT : {

                for (qsizetype i = 0; i < Y.count(); ++i) {

                    const std::complex<double> &Y_i(Y.at(i));

                    double Y_abs_i = std::abs(Y_i);

                    double Y_abs_pow2_i = Y_abs_i * Y_abs_i;

                    double &G_i(G[i]);

                    G_i = Y_abs_pow2_i;

                    std::complex<double> Y_conj_i = std::conj(Y_i);

                    Beta1[i] = G_i * Y_conj_i;

                    G_i += Y_abs_pow2_i;

                    temp[i] = Y_conj_i * Y_conj_i;

                }

                break;

            }

        }

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nItems, &alpha, d_whitenedMatrix.data(), d_nItems, Beta1.data(), d_nComponents, &complex_zero, d_B.data(), d_nComponents);

        QList<double> ggg = MathOperations::repeatVector(MathDescriptives::meanOfColumns(G, d_nComponents, false, d_numberOfThreadsToUse), d_nComponents);

        QList<std::complex<double>> Beta2 = MathOperations::repeatVector(MathDescriptives::meanOfColumns(temp, d_nComponents, false, d_numberOfThreadsToUse), d_nComponents);

        QList<std::complex<double>> BOld2 = BOld;

        for (qsizetype i = 0; i < d_B.count(); ++i) {

            const std::complex<double> &BOld_i(BOld.at(i));

            d_B[i] = BOld_i * ggg.at(i) - d_B.at(i);

            BOld2[i] = BOld_i * Beta2.at(i);

        }

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjNoTrans, d_nComponents, d_nComponents, d_nComponents, &complex_one, pseudoCovariance.data(), d_nComponents, BOld2.data(), d_nComponents, &complex_one, d_B.data(), d_nComponents);

        //Orthonormalize
        QList<std::complex<double>> symmetricalMatrix_B(d_B.count());

        cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, &complex_one, d_B.data(), d_nComponents, d_B.data(), d_nComponents, &complex_zero, symmetricalMatrix_B.data(), d_nComponents);

        int info;

        MathOperations::mPowerSymmetricMatrixInplace(symmetricalMatrix_B, d_nComponents, -0.5, d_numberOfThreadsToUse, &info);

        if (d_connectedBaseworker) {

            if (info < 0)
                d_connectedBaseworker->reportError(d_label + ": mPower:DSYEVR: " + QString::number(info) + "th parameter had illegal value");
            else if (info > 0)
                d_connectedBaseworker->reportError(d_label + ": mPower:DSYEVR: Error code -> " + QString::number(info));

            if (info != 0)
                d_connectedBaseworker->stopProgress(uuidProgress);

        }

        if (info != 0)
            return false;

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, d_nComponents, d_nComponents, d_nComponents, &complex_one, d_B.data(), d_nComponents, symmetricalMatrix_B.data(), d_nComponents, &complex_zero, tmp.data(), d_nComponents);

        d_B = tmp;

        if (d_connectedBaseworker) {

            d_connectedBaseworker->updateProgress(uuidProgress, -10000000 * std::log10(std::fabs(1.0 - minAbsCos)));

            if (d_connectedBaseworker->thread()->isInterruptionRequested()) {

                d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

                MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

                d_connectedBaseworker->stopProgress(uuidProgress);

                return false;
            }

            d_connectedBaseworker->checkIfPauseWasRequested();

        } else if (QThread::currentThread()->isInterruptionRequested()) {

            d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

            MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

            return false;

        }

    }

    d_seperatingMatrix.resize(d_nComponents * d_nSamples, 0.0);

    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, d_nComponents, d_nSamples, d_nComponents, &complex_one, d_B.data(), d_nComponents, d_whiteningMatrix.data(), d_nSamples, &complex_zero, d_seperatingMatrix.data(), d_nSamples);

    if (d_connectedBaseworker)
        d_connectedBaseworker->stopProgress(uuidProgress);

    return true;

}

const QList<std::complex<double>> &ComplexFastICA::mixingMatrix() const
{

    return d_mixingMatrix;

}

const QList<std::complex<double>> &ComplexFastICA::seperatingMatrix() const
{

    return d_seperatingMatrix;

}

const QList<std::complex<double>> &ComplexFastICA::independentComponents()
{

    d_threadPool.setMaxThreadCount(d_numberOfThreadsToUse);

    d_previousNumberOfThreadsToUse = MathUtilityFunctions::getNumberOfThreadsUsedForBLASandLAPACKroutines();

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_numberOfThreadsToUse);

    d_independentComponents.resize(d_nItems * d_nComponents, 0.0);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, d_nComponents, d_nItems, d_nComponents, &complex_one, d_B.data(), d_nComponents, d_whitenedMatrix.data(), d_nItems, &complex_zero, d_independentComponents.data(), d_nItems);

    d_threadPool.setMaxThreadCount(d_previousNumberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(d_previousNumberOfThreadsToUse);

    return d_independentComponents;

}

void ComplexFastICA::connectBaseWorker(BaseWorker *baseWorker)
{

    d_connectedBaseworker = QPointer<BaseWorker>(baseWorker);

}

void ComplexFastICA::setLabel(const QString &label)
{

    d_label = label;

}

void ComplexFastICA::setNumberOfThreadsToUse(qsizetype numberOfThreadsToUse)
{

    d_numberOfThreadsToUse = numberOfThreadsToUse;

}
