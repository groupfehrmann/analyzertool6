#ifndef MATHDESCRIPTIVES_H
#define MATHDESCRIPTIVES_H

#include <QList>
#include <QThreadPool>

#include <span>
#include <algorithm>
#include <limits>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include "mathutilityfunctions.h"
#include "mathoperations.h"

namespace MathDescriptives {

    double sumIgnoreNaNs(const std::span<double> &vector);

    double meanIgnoreNans(const std::span<double> &vector);

    double l2NormIgnoreNans(const std::span<double> &vector);

    double pearsonRCorrelation(QList<double> vector1, QList<double> vector2);

    double spearmanRCorrelation(QList<double> vector1, QList<double> vector2);

    double xiCorrelation(const QList<double> &vector1, QList<double> vector2);

    double xiCorrelation_preRanked(const QList<double> &vector1, const QList<double> &vector2_preRanked);

    double xiCorrelation_preRanked(const std::span<double> &vector1, const std::span<double> &vector2_preRanked);

    template <typename T> double minimumAbsoluteValueOnDiagonalOfSquaredMatrix(const QList<T> &squaredMatrix, qsizetype dimension);

    template <typename T> double frobeniusNorm(const QList<T> &matrix);

    template <typename T> QList<T> sumOfColumns(const QList<T> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix = false, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <typename T> QList<T> meanOfColumns(const QList<T> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix = false, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <> QList<std::complex<double>> meanOfColumns(const QList<std::complex<double>> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix, int numberOfThreadsToUse);

    template <typename T> qsizetype countIfBothElementsAreNonNaNs(const QList<T> &vector1, const QList<T> &vector2);

    template <typename T> qsizetype countIfBothElementsAreNonNaNs(const std::span<T> &vector1, const std::span<T> &vector2);

    template <typename T> double percentileOfPresortedList(const QList<T> &sortedAscendingList, const double &percentile);

    template <typename T> double percentileOfPresortedList(const std::span<T> &sortedAscendingList, const double &percentile);

}

template <typename T>
double MathDescriptives::minimumAbsoluteValueOnDiagonalOfSquaredMatrix(const QList<T> &squaredMatrix, qsizetype dimension)
{

    if (squaredMatrix.empty())
        return std::numeric_limits<double>::quiet_NaN();

    double minimumAbsoluteValue = std::abs(squaredMatrix.first());

    for (qsizetype i = 1; i < dimension; ++i) {

        double absoluteValue = std::abs(squaredMatrix.at(i + i * dimension));

        if (absoluteValue < minimumAbsoluteValue)
            minimumAbsoluteValue = absoluteValue;

    }

    return minimumAbsoluteValue;

}

template <typename T>
double MathDescriptives::frobeniusNorm(const QList<T> &matrix)
{

    double norm = 0.0;

    for (const T &value : matrix) {

        double absValue = std::abs(value);

        norm += absValue * absValue;

    }

    return std::sqrt(norm);

}

template <typename T>
QList<T> MathDescriptives::sumOfColumns(const QList<T> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    qsizetype m = (firstTransposeMatrix ? leadingDimension : numberOfRows);

    qsizetype n = (firstTransposeMatrix ? numberOfRows : leadingDimension);

    QList<double> sums(n, 0.0);

    cblas_dgemv(CblasRowMajor, (firstTransposeMatrix ? CblasNoTrans : CblasTrans), m, n, 1.0, matrix.data(), n, QList<double>(m, 1.0).data(), 1, 0.0, sums.data(), 1);

    return sums;

}

template <typename T>
QList<T> MathDescriptives::meanOfColumns(const QList<T> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    qsizetype m = (firstTransposeMatrix ? leadingDimension : numberOfRows);

    qsizetype n = (firstTransposeMatrix ? numberOfRows : leadingDimension);

    QList<double> sums(n, 0.0);

    cblas_dgemv(CblasRowMajor, (firstTransposeMatrix ? CblasNoTrans : CblasTrans), m, n, 1.0, matrix.data(), n, QList<double>(m, 1.0).data(), 1, 0.0, sums.data(), 1);

    for (double &sum : sums)
        sum /= m;

    return sums;

}

template <typename T>
qsizetype MathDescriptives::countIfBothElementsAreNonNaNs(const QList<T> &vector1, const QList<T> &vector2)
{

    qsizetype count = 0;

    for (qsizetype i = 0; i < vector1.count(); ++i) {

        if ((vector1.at(i) != std::numeric_limits<T>::quiet_NaN()) && (vector2.at(i) != std::numeric_limits<T>::quiet_NaN()))
            ++count;

    }

    return count;

}

template <typename T>
qsizetype MathDescriptives::countIfBothElementsAreNonNaNs(const std::span<T> &vector1, const std::span<T> &vector2)
{

    qsizetype count = 0;

    for (unsigned long i = 0; i < vector1.size(); ++i) {

        if (!std::isnan(vector1[i]) && !std::isnan(vector2[i]))
            ++count;

    }

    return count;

}

template <typename T>
double MathDescriptives::percentileOfPresortedList(const QList<T> &sortedAscendingList, const double &percentile) {

    if (sortedAscendingList.isEmpty())
        return std::numeric_limits<double>::quiet_NaN();

    if (sortedAscendingList.size() == 1)
        return sortedAscendingList.first();

    long double integerPart;

    long double fractionalPart = std::modf(static_cast<double>(sortedAscendingList.size() - 1) * percentile, &integerPart);

    return static_cast<double>(sortedAscendingList.at(int(integerPart))) + fractionalPart * (sortedAscendingList.at(int(integerPart) + 1) - sortedAscendingList.at(int(integerPart)));

}

template <typename T>
double MathDescriptives::percentileOfPresortedList(const std::span<T> &sortedAscendingList, const double &percentile) {

    if (sortedAscendingList.empty())
        return std::numeric_limits<double>::quiet_NaN();

    if (sortedAscendingList.size() == 1)
        return sortedAscendingList[0];

    long double integerPart;

    long double fractionalPart = std::modf(static_cast<double>(sortedAscendingList.size() - 1) * percentile, &integerPart);

    return static_cast<double>(sortedAscendingList[int(integerPart)]) + fractionalPart * (sortedAscendingList[int(integerPart) + 1] - sortedAscendingList[int(integerPart)]);

}

#endif // MATHDESCRIPTIVES_H
