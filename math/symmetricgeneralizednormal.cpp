#include "symmetricgeneralizednormal.h"

SymmetricGeneralizedNormal::SymmetricGeneralizedNormal(const double &location, const double &scale, const double &shape) :
    d_location(location), d_scale(scale), d_shape(shape)
{

}

SymmetricGeneralizedNormal::SymmetricGeneralizedNormal(const std::span<double> &data)
{

    long double m1 = 0.0;

    for (const double &value : data)
        m1 += std::abs(static_cast<long double>(value));

    m1 /= static_cast<long double>(data.size());

    long double m2 = boost::math::statistics::variance(data);

    long double beta_0 = m1 / std::sqrt(m2);

    auto func1 = [&](const long double &beta) {

        long double s1 = 0.0;

        long double s2 = 0.0;

        long double s3 = 0.0;

        for (const double &value : data) {

            long double absDiff = std::abs(static_cast<long double>(value) - static_cast<long double>(m1));

            long double absDiff_to_power_beta = std::pow(absDiff, beta);

            if (absDiff == 0.0)
                absDiff += std::numeric_limits<long double>::min();

            long double log_absDiff = std::log(absDiff);

            s1 += absDiff_to_power_beta * log_absDiff;

            s2 += absDiff_to_power_beta;

            s3 += absDiff_to_power_beta * log_absDiff * log_absDiff;

        }

        if (s2 == 0.0)
            s2 += std::numeric_limits<long double>::min();

        long double t1 = beta / static_cast<long double>(data.size()) * s2;

        if (t1 == 0.0)
            t1 += std::numeric_limits<long double>::min();

        double g = static_cast<long double>(1.0) + boost::math::digamma(static_cast<long double>(1.0) / beta) / beta
                   - s1 / s2
                   + std::log(t1) / beta;

        double g_df = - boost::math::digamma(static_cast<long double>(1.0) / beta) / (beta * beta)
                      - boost::math::trigamma(static_cast<long double>(1.0) / beta) / (beta * beta * beta)
                      + static_cast<long double>(1.0) / (beta * beta)
                      - s3 / s2
                      + (s1 * s1) / (s2 * s2)
                      + s1 / (beta * s2)
                      - std::log(beta / static_cast<double>(data.size()) * s2) / (beta * beta);


        return std::pair<double, double>(g, g_df);

    };

    int digits = std::numeric_limits<double>::digits;

    std::uintmax_t iter = 250;

    d_shape = boost::math::tools::newton_raphson_iterate(func1, beta_0, static_cast<long double>(1.0), std::numeric_limits<long double>::max(), digits, iter);

    auto func2 = [&](const long double &location) {

        long double s = 0.0;

        for (const double &value : data)
            s += std::pow(std::abs(static_cast<long double>(value) - location), static_cast<long double>(d_shape));

        return s;

    };

    iter = 250;

    d_location = boost::math::tools::brent_find_minima(func2, static_cast<long double>(-10.0), static_cast<long double>(10.0), digits, iter).first;

    long double s = 0.0;

    for (const double &value : data)
        s += std::pow(std::abs(static_cast<long double>(value) - static_cast<long double>(d_location)), static_cast<long double>(d_shape));

    d_scale = std::pow((static_cast<long double>(d_shape) / static_cast<long double>(data.size())) * s, static_cast<long double>(1.0) / static_cast<long double>(d_shape));

}

double SymmetricGeneralizedNormal::pdf(const double &x)
{

    return (static_cast<long double>(d_shape) / (static_cast<long double>(2.0) * static_cast<long double>(d_scale) * boost::math::tgamma(static_cast<long double>(1.0) / static_cast<long double>(d_shape)))) * std::exp(-std::pow(std::abs(static_cast<long double>(x) - static_cast<long double>(d_location)) / static_cast<long double>(d_scale), static_cast<long double>(d_shape)));

}

double SymmetricGeneralizedNormal::cdf(const double &x)
{

    long double gamma_q = boost::math::gamma_q(static_cast<long double>(1.0) / static_cast<long double>(d_shape), std::pow(std::abs((static_cast<long double>(x) - static_cast<long double>(d_location)) / static_cast<long double>(d_scale)), static_cast<long double>(d_shape)));

    long double gamma_p = boost::math::gamma_p(static_cast<long double>(1.0) / static_cast<long double>(d_shape), std::pow(std::abs((static_cast<long double>(x) - static_cast<long double>(d_location)) / static_cast<long double>(d_scale)), static_cast<long double>(d_shape)));

    double p;

    if (gamma_q < 0.5) {

        if ((x - d_location) > 0.0)
            p = static_cast<long double>(1.0) - static_cast<long double>(0.5) * gamma_q;
        else
            p = static_cast<long double>(0.5) * gamma_q;

    } else {

        if ((x - d_location) > 0.0)
            p = static_cast<long double>(0.5) + static_cast<long double>(0.5) * gamma_p;
        else
            p = static_cast<long double>(0.5) - static_cast<long double>(0.5) * gamma_p;

    }

    if (p == 0)
        return std::numeric_limits<double>::min();
    else if (p == 1.0)
        return p - std::numeric_limits<double>::epsilon();

    return p;

}
