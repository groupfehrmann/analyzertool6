#ifndef DESCRIPTIVESWORKER_H
#define DESCRIPTIVESWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"

class DescriptivesParameters : public BaseParameters
{

public:

    DescriptivesParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString exportDirectory;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QString annotationLabelDefiningItemSubsets;

    qsizetype annotationIndexDefiningItemSubsets;

    QList<QString> selectedItemSubsetIdentifiers;

    QList<QString> annotationLabelsForVariables;

    QList<QString> descriptiveLabels;

    QList<std::function<QList<QVariant> (const QList<QString> &, const QList<double> &)>> descriptiveFunctions;

    int numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class DescriptivesWorker : public BaseWorker
{

public:

    DescriptivesWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void DescriptivesWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<DescriptivesParameters> parameters(d_data->d_parameters.dynamicCast<DescriptivesParameters>());


    QUuid uuidProgress = this->startProgress("mapping indexes to subsets", 0, 0);

    QList<QString> resultColumnLabels;

    if (!parameters->annotationLabelsForVariables.isEmpty())
        resultColumnLabels.append(parameters->annotationLabelsForVariables);

    QList<QList<qsizetype>> itemSubsetToIndexes(parameters->selectedItemSubsetIdentifiers.count());

    QList<QList<QString>> itemSubsetToIdentifiers(parameters->selectedItemSubsetIdentifiers.count());

    if (parameters->annotationIndexDefiningItemSubsets == -1) {

        resultColumnLabels.append(parameters->descriptiveLabels);

        itemSubsetToIndexes.append(parameters->selectedItemIndexes);

        itemSubsetToIdentifiers.append(parameters->selectedItemIdentifiers);

    } else {

        QHash<QString, qsizetype> itemSubsetIdentifierToIndex;

        for (const QString &itemSubsetIdentifier : parameters->selectedItemSubsetIdentifiers) {

            itemSubsetIdentifierToIndex.insert(itemSubsetIdentifier, itemSubsetIdentifierToIndex.count());

            for (const QString &descriptiveLabel : parameters->descriptiveLabels)
                resultColumnLabels.append(descriptiveLabel + "_" + itemSubsetIdentifier);

        }

        for (qsizetype i = 0; i < parameters->selectedItemIndexes.count(); ++i) {

            const qsizetype &itemIndex(parameters->selectedItemIndexes.at(i));

            qsizetype indexToAppend = itemSubsetIdentifierToIndex.value(annotatedMatrix->annotationValue(itemIndex, parameters->annotationIndexDefiningItemSubsets, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString(), -1);

            if (indexToAppend != -1) {

                itemSubsetToIndexes[indexToAppend].append(itemIndex);

                itemSubsetToIdentifiers[indexToAppend].append(parameters->selectedItemIdentifiers.at(i));

            }

        }

    }


    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("calculating descriptives", 0, parameters->selectedVariableIndexes.count());

    std::function<QList<QVariant>(qsizetype)> mapFunctor = [this, &uuidProgress, &annotatedMatrix, &parameters, &itemSubsetToIdentifiers, &itemSubsetToIndexes](qsizetype variableIndex) {

        QList<QVariant> descriptiveValues;

        if (parameters->annotationLabelsForVariables.isEmpty())
            descriptiveValues.reserve(itemSubsetToIndexes.count() * parameters->descriptiveLabels.count());
        else {

            descriptiveValues.reserve(itemSubsetToIndexes.count() * (parameters->descriptiveLabels.count() + parameters->annotationLabelsForVariables.count()));

            for (const QString &annotationLabel : parameters->annotationLabelsForVariables)
                descriptiveValues.append(annotatedMatrix->annotationValue(annotatedMatrix->identifier(variableIndex, parameters->orientation), annotationLabel, parameters->orientation));

        }

        for (qsizetype j = 0; j < itemSubsetToIndexes.count(); ++j) {

            QList<double> vector;

            if (parameters->orientation == Qt::Vertical)
                vector = annotatedMatrix->template rowAt<double>(variableIndex, itemSubsetToIndexes.at(j));
            else
                vector = annotatedMatrix->template columnAt<double>(variableIndex, itemSubsetToIndexes.at(j));

            for (qsizetype k = 0; k < parameters->descriptiveFunctions.count(); ++k)
                descriptiveValues.append(parameters->descriptiveFunctions.at(k)(itemSubsetToIdentifiers.at(j), vector));

            if (this->thread()->isInterruptionRequested())
                return descriptiveValues;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return descriptiveValues;

    };

    auto reduceFunctor = [](QList<QVariant> &result, const QList<QVariant> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<QVariant> descriptiveMatrix = QtConcurrent::blockingMappedReduced<QList<QVariant>>(&d_threadPool, parameters->selectedVariableIndexes, mapFunctor, std::function<void(QList<QVariant> &, const QList<QVariant> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    AnnotatedMatrix<QVariant> *result_descriptiveMatrix = new AnnotatedMatrix<QVariant>(parameters->selectedVariableIdentifiers, resultColumnLabels, descriptiveMatrix);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("descriptives", result_descriptiveMatrix));
    else {

        QSharedPointer<AnnotatedMatrix<QVariant>> sharedPointerToAnnotatedMatrix(result_descriptiveMatrix);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "descriptives");

    }

}


#endif // DESCRIPTIVESWORKER_H
