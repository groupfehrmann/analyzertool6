#include "selectdelimiterwidget.h"
#include "ui_selectdelimiterwidget.h"

SelectDelimiterWidget::SelectDelimiterWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::SelectDelimiterWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

    this->connect(ui->comboBox, &QComboBox::currentIndexChanged, [this](int index){ui->lineEdit->setEnabled((index == 0) ? true : false); emit this->delimiterChanged();});

    ui->comboBox->addItem("custum", QString());

    ui->comboBox->addItem("tab", QString("\\t"));

    ui->comboBox->addItem("space", QString( " "));

    ui->comboBox->addItem("line feed", QString("\\n"));

    ui->comboBox->addItem("carriage return", QString("\\r"));

    ui->comboBox->addItem("whitespace", QString("\\s+"));

    this->connect(ui->lineEdit, &QLineEdit::textEdited, [this](const QString &text){ui->comboBox->setItemData(0, text); emit this->delimiterChanged();});

}

QComboBox *SelectDelimiterWidget::comboBox()
{

    return ui->comboBox;

}

QLabel *SelectDelimiterWidget::label()
{

    return ui->label;

}

QLineEdit *SelectDelimiterWidget::lineEdit()
{

    return ui->lineEdit;

}

void SelectDelimiterWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    if (settings.contains("currentIndex"))
        ui->comboBox->setCurrentIndex(settings.value("currentIndex").toInt());
    else
        ui->comboBox->setCurrentIndex(1);

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit");

    if (settings.contains("text")) {

        ui->lineEdit->setText(settings.value("text").toString());

        ui->comboBox->setItemData(0, ui->lineEdit->text());

    }

    settings.endGroup();

    d_readWidgetSettings = true;

}

void SelectDelimiterWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/comboBox");

    settings.setValue("currentIndex", ui->comboBox->currentIndex());

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit");

    settings.setValue("text", ui->lineEdit->text());

    settings.endGroup();

}

SelectDelimiterWidget::~SelectDelimiterWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
