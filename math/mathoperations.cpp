#include "mathoperations.h"

#include "mathdescriptives.h"

QList<double> &MathOperations::fillWithRandomNumbersFromUniformRealDistribution(QList<double> &sequence, const double &minimumValue, const double &maximumValue)
{

    std::default_random_engine generator(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));

    std::uniform_real_distribution<double> distribution(minimumValue, maximumValue);

    for (double &value : sequence)
            value = distribution(generator);

    return sequence;

}

QList<std::complex<double>> &MathOperations::fillWithRandomComplexNumbersFromUniformRealDistribution(QList<std::complex<double>> &sequence, const double &minimumValue, const double &maximumValue)
{

    std::default_random_engine generator(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));

    std::uniform_real_distribution<double> distribution(minimumValue, maximumValue);

    for (std::complex<double> &value : sequence) {

        value.real(distribution(generator));

        value.imag(distribution(generator));

    }

    return sequence;

}

QList<double> &MathOperations::orthogonalizeMatrixInplace(QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse, int *info)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> s(leadingDimension, 0.0);

    QList<double> u(numberOfRows * numberOfRows, 0.0);

    QList<double> vt(leadingDimension * leadingDimension, 0.0);

    *info = LAPACKE_dgesdd(LAPACK_ROW_MAJOR, 'S', numberOfRows, leadingDimension, matrix.data(), leadingDimension, s.data(), u.data(), numberOfRows, vt.data(), leadingDimension);

    double tol = s.size() * 2.2e-16 * s.at(cblas_idamax(s.count(), s.data(), 1));

    qsizetype r = 0;

    for (qsizetype i = 0; i < s.count(); ++i)
        if (s.at(i) > tol) r++;

    MathOperations::transposeInplace(u, leadingDimension, numberOfThreadsToUse);

    u.resize(r * leadingDimension);

    MathOperations::transposeInplace(u, leadingDimension, numberOfThreadsToUse);

    matrix = u;

    return matrix;

}

QList<std::complex<double>> &MathOperations::orthogonalizeMatrixInplace(QList<std::complex<double>> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse, int *info)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> s(leadingDimension, 0.0);

    QList<std::complex<double>> u(numberOfRows * numberOfRows, 0.0);

    QList<std::complex<double>> vt(leadingDimension * leadingDimension, 0.0);

    *info = LAPACKE_zgesdd(LAPACK_ROW_MAJOR, 'S', numberOfRows, leadingDimension, matrix.data(), leadingDimension, s.data(), u.data(), numberOfRows, vt.data(), leadingDimension);

    double tol = s.size() * 2.2e-16 * s.at(cblas_idamax(s.count(), s.data(), 1));

    qsizetype r = 0;

    for (qsizetype i = 0; i < s.count(); ++i)
        if (s.at(i) > tol) r++;

    MathOperations::transposeInplace(u, leadingDimension, numberOfThreadsToUse);

    u.resize(r * leadingDimension);

    MathOperations::transposeInplace(u, leadingDimension, numberOfThreadsToUse);

    matrix = u;

    return matrix;

}

QList<double> &MathOperations::mPowerSymmetricMatrixInplace(QList<double> &symmetricMatrixUpperTriangle, qsizetype leadingDimension, double power, int numberOfThreadsToUse, int *info)
{

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype m;

    QList<double> w(leadingDimension, 0.0);

    QList<double> z(leadingDimension * leadingDimension, 0.0);

    QList<long long> isuppz(leadingDimension * 2, 0.0);

    *info = LAPACKE_dsyevr(LAPACK_ROW_MAJOR, 'V', 'A', 'U', leadingDimension, symmetricMatrixUpperTriangle.data(), leadingDimension, 0.0, 1.0, 1, leadingDimension, -1.0, &m, w.data(), z.data(), leadingDimension, isuppz.data());

    std::reverse(w.begin(), w.end());

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, leadingDimension), [](std::span<double> &span){std::reverse(span.begin(), span.end());});

    std::for_each(w.begin(), w.end(), [&power](double &value){value = std::pow(value, power);});

    QList<double> z_ = z;

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, leadingDimension), [&w](std::span<double> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), std::multiplies<double>());});

    QList<double> mPowerSymmetricMatrix(leadingDimension * leadingDimension, 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, leadingDimension, leadingDimension, leadingDimension, 1.0, z.data(), leadingDimension, z_.data(), leadingDimension, 0.0, mPowerSymmetricMatrix.data(), leadingDimension);

    symmetricMatrixUpperTriangle = mPowerSymmetricMatrix;

    return symmetricMatrixUpperTriangle;

}

QList<std::complex<double>> &MathOperations::mPowerSymmetricMatrixInplace(QList<std::complex<double>> &symmetricMatrixUpperTriangle, qsizetype leadingDimension, double power, int numberOfThreadsToUse, int *info)
{

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype m;

    QList<double> w(leadingDimension, 0.0);

    QList<std::complex<double>> z(leadingDimension * leadingDimension, 0.0);

    QList<long long> isuppz(leadingDimension * 2, 0.0);

    *info = LAPACKE_zheevr(LAPACK_ROW_MAJOR, 'V', 'A', 'U', leadingDimension, symmetricMatrixUpperTriangle.data(), leadingDimension, 0.0, 1.0, 1, leadingDimension, -1.0, &m, w.data(), z.data(), leadingDimension, isuppz.data());

    std::reverse(w.begin(), w.end());

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, leadingDimension), [](std::span<std::complex<double>> &span){std::reverse(span.begin(), span.end());});

    std::for_each(w.begin(), w.end(), [&power](double &value){value = std::pow(value, power);});

    QList<std::complex<double>> z_ = z;

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(z, leadingDimension), [&w](std::span<std::complex<double>> &span){std::transform(w.begin(), w.end(), span.begin(), span.begin(), [](const double &a, const std::complex<double> &b){return a * b;});});

    QList<std::complex<double>> mPowerSymmetricMatrix(leadingDimension * leadingDimension, 0.0);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, leadingDimension, leadingDimension, leadingDimension, &complex_one, z.data(), leadingDimension, z_.data(), leadingDimension, &complex_zero, mPowerSymmetricMatrix.data(), leadingDimension);

    symmetricMatrixUpperTriangle = mPowerSymmetricMatrix;

    return symmetricMatrixUpperTriangle;

}

QList<double> &MathOperations::createSymmetricMatrixInPlace(QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> symmetricMatrix(numberOfRows * numberOfRows, 0.0);

    cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, numberOfRows, leadingDimension, 1.0, matrix.data(), leadingDimension, 0.0, symmetricMatrix.data(), numberOfRows);

    matrix = symmetricMatrix;

    return matrix;

}

QList<double> MathOperations::elementWise_multiplication(const QList<double> &vector1, const QList<double> &vector2, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    QList<double> result(vector1.count());

    cblas_dsbmv(CblasRowMajor, CblasLower, vector1.count(), 0, 1.0, vector1.data(), 1, vector2.data(), 1, 0.0, result.data(), 1);

    return result;

}

QList<double> MathOperations::elementWise_multiplication_inplace(QList<double> &vector1, const QList<double> &vector2, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    QList<double> result(vector1.count());

    cblas_dsbmv(CblasRowMajor, CblasLower, vector1.count(), 0, 1.0, vector1.data(), 1, vector2.data(), 1, 0.0, result.data(), 1);

    vector1 = result;

    return vector1;

}

QList<double> &MathOperations::centerVectorToMeanInplace(QList<double> &vector)
{

    cblas_daxpy(vector.count(), -1.0, QList<double>(vector.count(), boost::math::statistics::mean(vector)).data(), 1, vector.data(), 1);

    return vector;

}

std::span<double> &MathOperations::centerVectorToMeanInplace(std::span<double> &vector)
{

    cblas_daxpy(vector.size(), -1.0, QList<double>(vector.size(), boost::math::statistics::mean(vector)).data(), 1, vector.data(), 1);

    return vector;

}

QList<std::complex<double>> &MathOperations::centerVectorToMeanInplace(QList<std::complex<double>> &vector)
{

    std::complex<double> mean = 0.0;

    for (const std::complex<double> &complex : vector)
        mean += complex;

    mean /= static_cast<double>(vector.size());

    for (std::complex<double> &complex : vector)
        complex -= mean;

    return vector;

}

std::span<std::complex<double>> &MathOperations::centerVectorToMeanInplace(std::span<std::complex<double>> &vector)
{

    std::complex<double> mean = 0.0;

    for (const std::complex<double> &complex : vector)
        mean += complex;

    mean /= static_cast<double>(vector.size());

    for (std::complex<double> &complex : vector)
        complex -= mean;

    return vector;

}

std::span<double> &MathOperations::centerVectorToMeanInplaceIgnoreNaNs(std::span<double> &vector)
{

    double mean = MathDescriptives::meanIgnoreNans(vector);

    std::transform(vector.begin(), vector.end(), vector.begin(), [&mean](const double &value){ return value - mean;});

    return vector;

}

std::span<double> &MathOperations::standardize(std::span<double> &vector)
{

    std::pair<double, double> meanAndVariance = boost::math::statistics::mean_and_sample_variance(vector);

    for (double &value : vector)
        value = (value - meanAndVariance.first) / meanAndVariance.second;

    return vector;

}

QList<double> &MathOperations::scaleVectorToL2NormInplace(QList<double> &vector)
{

    cblas_dscal(vector.count(), 1.0 / boost::math::tools::l2_norm(vector), vector.data(), 1);

    return vector;

}

std::span<double> &MathOperations::scaleVectorToL2NormInplace(std::span<double> &vector)
{

    cblas_dscal(vector.size(), 1.0 / boost::math::tools::l2_norm(vector.begin(), vector.end()), vector.data(), 1);

    return vector;

}

QList<std::complex<double>> &MathOperations::scaleVectorToL2NormInplace(QList<std::complex<double>> &vector)
{

    cblas_zdscal(vector.size(), 1.0 / boost::math::tools::l2_norm(vector.begin(), vector.end()), vector.data(), 1);

    return vector;

}

std::span<std::complex<double>> &MathOperations::scaleVectorToL2NormInplace(std::span<std::complex<double>> &vector)
{

    cblas_zdscal(vector.size(), 1.0 / boost::math::tools::l2_norm(vector.begin(), vector.end()), vector.data(), 1);

    return vector;

}

std::span<double> &MathOperations::scaleVectorToL2NormInplaceIgnoreNaNs(std::span<double> &vector)
{

    double l2Norm = MathDescriptives::l2NormIgnoreNans(vector);

    cblas_dscal(vector.size(), 1.0 / l2Norm, vector.data(), 1);

    return vector;

}

QList<double> MathOperations::inverse(QList<double> matrix, qsizetype leadingDimension, int numberOfThreadsToUse , int *info)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<qsizetype> ipiv((numberOfRows < leadingDimension) ? numberOfRows : leadingDimension, 0.0);

    *info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, numberOfRows, leadingDimension, matrix.data(), leadingDimension, ipiv.data());

    if (*info != 0)
        return QList<double>();

    *info = LAPACKE_dgetri(LAPACK_ROW_MAJOR, leadingDimension, matrix.data(), leadingDimension, ipiv.data());

    return matrix;

}

QList<double> MathOperations::pseudoInverse(const QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse, int *info)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> symmetricMatrix(numberOfRows * numberOfRows, 0.0);

    cblas_dsyrk(CblasRowMajor, CblasUpper, CblasNoTrans, numberOfRows, leadingDimension, 1.0, matrix.data(), leadingDimension, 0.0, symmetricMatrix.data(), numberOfRows);

    MathOperations::mPowerSymmetricMatrixInplace(symmetricMatrix, numberOfRows, -1.0, numberOfThreadsToUse, info);

    QList<double> inverse(numberOfRows * leadingDimension, 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, numberOfRows, leadingDimension, numberOfRows, 1.0, symmetricMatrix.data(), numberOfRows, matrix.data(), leadingDimension, 0.0, inverse.data(), leadingDimension);

    return inverse;

}

QList<std::complex<double>> MathOperations::pseudoInverse(const QList<std::complex<double>> &matrix, qsizetype leadingDimension, bool checkError, double &error, int numberOfThreadsToUse, int *info)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<std::complex<double>> symmetricMatrix(numberOfRows * numberOfRows, 0.0);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 0.0;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, numberOfRows, numberOfRows, leadingDimension, &complex_one, matrix.data(), leadingDimension, matrix.data(), leadingDimension, &complex_zero, symmetricMatrix.data(), numberOfRows);

    MathOperations::mPowerSymmetricMatrixInplace(symmetricMatrix, numberOfRows, -1.0, numberOfThreadsToUse, info);

    QList<std::complex<double>> inverse(numberOfRows * leadingDimension, 0.0);

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, numberOfRows, leadingDimension, numberOfRows, &complex_one, symmetricMatrix.data(), numberOfRows, matrix.data(), leadingDimension, &complex_zero, inverse.data(), leadingDimension);

    if (checkError) {

        QList<std::complex<double>> symmetricMatrix(numberOfRows * numberOfRows, 0.0);

        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, numberOfRows, numberOfRows, leadingDimension, &complex_one, matrix.data(), leadingDimension, inverse.data(), leadingDimension, &complex_zero, symmetricMatrix.data(), numberOfRows);

        long double sum = 0;

        for (qsizetype i = 0; i < numberOfRows; ++i) {

            sum += std::abs(1.0 - static_cast<double>(std::abs(symmetricMatrix.at(i + i * numberOfRows))));

            for (qsizetype j = i + 1; j < numberOfRows; ++j)
                sum += std::abs(symmetricMatrix.at(i * numberOfRows + j));

        }

        error = sum;

    }

    return inverse;

}

QList<double> MathOperations::pseudoInverseSVD(QList<double> matrix, qsizetype leadingDimension, int numberOfThreadsToUse, int *info)
{

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> s(numberOfRows, 0.0);

    QList<double> u(numberOfRows * numberOfRows, 0.0);

    QList<double> vt(numberOfRows * leadingDimension, 0.0);

    *info = LAPACKE_dgesdd(LAPACK_ROW_MAJOR, 'S', numberOfRows, leadingDimension, matrix.data(), leadingDimension, s.data(), u.data(), numberOfRows, vt.data(), leadingDimension);

    double tol = s.size() * 2.2e-16 * s.at(cblas_idamax(s.count(), s.data(), 1));

    qsizetype r = 0;

    for (qsizetype i = 0; i < s.count(); ++i)
        if (s.at(i) > tol) r++;

    s.resize(r);

    MathOperations::transposeInplace(u, numberOfRows, numberOfThreadsToUse);

    u.resize(r * numberOfRows);

    vt.resize(r * leadingDimension);

    std::for_each(s.begin(), s.end(), [](double &value){value = 1.0 / value;});

    MathOperations::transposeInplace(vt, leadingDimension, numberOfThreadsToUse);

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(vt, numberOfRows), [&s](std::span<double> &span){std::transform(s.begin(), s.end(), span.begin(), span.begin(), std::multiplies<double>());});

    QList<double> inverse(numberOfRows * leadingDimension, 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, leadingDimension, numberOfRows, numberOfRows, 1.0, vt.data(), numberOfRows, u.data(), numberOfRows, 0.0, inverse.data(), numberOfRows);

    return inverse;

}

QList<std::complex<double>> MathOperations::pseudoInverseSVD(QList<std::complex<double>> matrix, qsizetype leadingDimension, int numberOfThreadsToUse, int *info)
{

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<double> s(numberOfRows, 0.0);

    QList<std::complex<double>> u(numberOfRows * numberOfRows, 0.0);

    QList<std::complex<double>> vt(numberOfRows * leadingDimension, 0.0);

    *info = LAPACKE_zgesdd(LAPACK_ROW_MAJOR, 'S', numberOfRows, leadingDimension, matrix.data(), leadingDimension, s.data(), u.data(), numberOfRows, vt.data(), leadingDimension);

    double tol = s.size() * 2.2e-16 * s.at(cblas_idamax(s.count(), s.data(), 1));

    qsizetype r = 0;

    for (qsizetype i = 0; i < s.count(); ++i)
        if (s.at(i) > tol) r++;

    s.resize(r);

    MathOperations::transposeInplace(u, numberOfRows, numberOfThreadsToUse);

    u.resize(r * numberOfRows);

    vt.resize(r * leadingDimension);

    std::for_each(s.begin(), s.end(), [](double &value){value = 1.0 / value;});

    MathOperations::transposeInplace(vt, leadingDimension, numberOfThreadsToUse);

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(vt, numberOfRows), [&s](std::span<std::complex<double>> &span){std::transform(s.begin(), s.end(), span.begin(), span.begin(), [](const double &value1, const std::complex<double> &value2){return std::complex<double>(value1) * value2;});});

    QList<std::complex<double>> inverse(numberOfRows * leadingDimension, 1.0);

    std::complex<double> complex_one;

    std::complex<double> complex_zero;

    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, leadingDimension, numberOfRows, numberOfRows, &complex_one, vt.data(), numberOfRows, u.data(), numberOfRows, &complex_zero, inverse.data(), numberOfRows);

    return inverse;

}
