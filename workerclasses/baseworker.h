#ifndef BASEWORKER_H
#define BASEWORKER_H

#include <QObject>
#include <QThread>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QUuid>
#include <QString>
#include <QSharedPointer>
#include <QPointer>
#include <QReadWriteLock>
#include <QWaitCondition>
#include <QThreadPool>
#include <QFutureWatcherBase>
#include <QTimer>
#include <QEventLoop>

#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "models/annotatedmatrixdatamodel.h"

class BaseWorkerData : public QSharedData
{

public:

    BaseWorkerData(const QString &label = QString(), const QSharedPointer<BaseParameters> &parameters = nullptr, const QString &parametersWorkerLink = QString());

    QString d_label;

    QString d_parametersWorkerLink;

    QUuid d_uuid;

    QSharedPointer<BaseParameters> d_parameters;

    int d_delayInSeconds;

};

class BaseWorker : public QObject
{

    Q_OBJECT

public:

    explicit BaseWorker(QObject *parent = nullptr, const QString &label = QString(), const QSharedPointer<BaseParameters> &parameters = nullptr, const QString &parametersWorkerLink = QString());

    virtual ~BaseWorker();

    const QString &label() const;

    bool isPaused() const;

    const QUuid &uuid() const;

    void checkIfPauseWasRequested();

    template <typename T> void startProgress(const QString &label, QFuture<T> future, bool showProgress = true);

public slots:

    void doWork();

    void pauseWorker();

    bool performReadWriteLockLogistics();

    void performReadWriteUnlockLogistics();

    void reportError(const QString &error) const;

    void reportMessage(const QString &message) const;

    void reportWarning(const QString &warning) const;

    void requestInterruption();

    void resumeWorker();

    QUuid startProgress(const QString &label, qsizetype minimum, qsizetype maximum) const;

    void stopProgress(const QUuid &uuidProgress) const;

    void updateProgress(const QUuid &uuidProgress, qsizetype value) const;

    void updateProgressLabel(const QUuid &uuidProgress, const QString &label);

    void updateProgressWithOne(const QUuid &uuidProgress) const;

    void setDelay(int seconds);

protected:

    QSharedDataPointer<BaseWorkerData> d_data;

    mutable QReadWriteLock d_readWriteLockForPauseAndResumeWorker;

    QWaitCondition d_pauseWaitCondition;

    bool d_pauseWorker;

    QThreadPool d_threadPool;

    virtual void doWork_() = 0;

    virtual bool performReadWriteLockLogistics_() = 0;

    virtual void performReadWriteUnlockLogistics_() = 0;

    template <typename T> bool exportToFile(const QSharedPointer<AnnotatedMatrix<T>> &sharedPointerToAnnotatedMatrix, const QString &exportDirectory, const QString &fileName);

signals:

    void errorReported(const QUuid &uuidWorker, const QString &error) const;

    void messageReported(const QUuid &uuidWorker, const QString &message) const;

    void objectAvailable(const QUuid &uuidProject, QObject *object);

    void progressStarted(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, std::size_t minimum, std::size_t maximum) const;

    void progressStopped(const QUuid &uuidProgress) const;

    void progressUpdated(const QUuid &uuidProgress, qsizetype value) const;

    void progressLabelUpdated(const QUuid &uuidProgress, const QString &label);

    void progressUpdatedWithOne(const QUuid &uuidProgress) const;

    void warningReported(const QUuid &uuidWorker, const QString &warning) const;

    void workerFinished(const QUuid &uuidWorker) const;

    void workerInterrupted(const QUuid &uuidWorker) const;

    void workerQueued(const QUuid &uuidWorker, const QString &workerLabel) const;

    void workerStarted(const QUuid &uuidWorker) const;

    void workerPaused() const;

    void workerResumed() const;

    void cancelFutureWatchers() const;

};

template <typename T>
void BaseWorker::startProgress(const QString &label, QFuture<T> future, bool showProgress)
{

    QThread *thread = new QThread;

    this->connect(thread, &QThread::finished, thread, &QThread::deleteLater);


    QFutureWatcher<T> futureWatcher;

    this->connect(this, &BaseWorker::workerPaused, &futureWatcher, &QFutureWatcher<T>::suspend);

    this->connect(this, &BaseWorker::workerResumed, &futureWatcher, &QFutureWatcher<T>::resume);

    this->connect(this, &BaseWorker::cancelFutureWatchers, &futureWatcher, &QFutureWatcher<T>::cancel);

    futureWatcher.moveToThread(thread);


    futureWatcher.setFuture(future);

    QUuid uuidProgress;

    if (showProgress) {

        uuidProgress = QUuid::createUuid();

        emit this->progressStarted(d_data->d_uuid, uuidProgress, label, futureWatcher.progressMinimum(), futureWatcher.progressMaximum());

        this->connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, [this, &uuidProgress](int value){this->updateProgress(uuidProgress, value);});

    }

    thread->start();

    futureWatcher.waitForFinished();

    thread->quit();

    thread->wait();

    if (showProgress)
        this->stopProgress(uuidProgress);

}

template <typename T>
bool BaseWorker::exportToFile(const QSharedPointer<AnnotatedMatrix<T>> &sharedPointerToAnnotatedMatrix, const QString &exportDirectory, const QString &fileName)
{

    QString selectedFilter = "tab-delimited (*.tsv)";

    QString fileExtension = ".tsv";

    AnnotatedMatrixDataSortFilterProxyModel sortFilterProxyModel;

    AnnotatedMatrixDataModel annotatedMatrixDataModel(nullptr, sharedPointerToAnnotatedMatrix);

    sortFilterProxyModel.setShowSelectedOnly(false);

    sortFilterProxyModel.setSourceModel(&annotatedMatrixDataModel);

    QFile fileOut(exportDirectory + "/" + fileName + fileExtension);

    if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + exportDirectory + "/" + fileName + fileExtension + "\"");

        QThread::currentThread()->requestInterruption();

        return false;

    }

    QTextStream out(&fileOut);

    QString delimiter;

    QString startOfLine;

    QString endOfLine;


    if (selectedFilter == "tab-delimited (*.tsv)")
        delimiter = "\t";
    else if (selectedFilter == "comma-seperate values (*.csv)") {

        delimiter = "\",\"";

        startOfLine = "\"";

        endOfLine = "\"";

    }

    QUuid uuidProgress = this->startProgress("exporting file \"" + fileName + fileExtension + "\"", 0, sortFilterProxyModel.rowCount());

    out << startOfLine;

    for (int i = 0; i < sortFilterProxyModel.columnCount(); ++i)
        out << delimiter << sortFilterProxyModel.headerData(i, Qt::Horizontal).toString();

    out << endOfLine << Qt::endl;

    for (int i = 0; i < sortFilterProxyModel.rowCount(); ++i) {

        out << startOfLine << sortFilterProxyModel.headerData(i, Qt::Vertical).toString();

        for (int j = 0; j < sortFilterProxyModel.columnCount(); ++j)
            out << delimiter << sortFilterProxyModel.data(sortFilterProxyModel.index(i, j)).toString();//value<T>();

        out << endOfLine << Qt::endl;

        if (QThread::currentThread()->isInterruptionRequested())
            return false;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    return true;

}

#endif // BASEWORKER_H
