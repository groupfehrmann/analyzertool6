#include "baseworker.h"

BaseWorkerData::BaseWorkerData(const QString &label, const QSharedPointer<BaseParameters> &parameters, const QString &parametersWorkerLink) :
    d_label(label),
    d_parametersWorkerLink(parametersWorkerLink),
    d_uuid(QUuid::createUuid()),
    d_parameters(parameters),
    d_delayInSeconds(0)
{

}

BaseWorker::BaseWorker(QObject *parent, const QString &label, const QSharedPointer<BaseParameters> &parameters, const QString &parametersWorkerLink) :
    QObject(parent),
    d_data(new BaseWorkerData(label, parameters, parametersWorkerLink)),
    d_pauseWorker(false)
{

    d_threadPool.setMaxThreadCount(QThreadPool::globalInstance()->maxThreadCount());

}

const QString &BaseWorker::label() const
{

    return d_data->d_label;

}

bool BaseWorker::isPaused() const
{

    d_readWriteLockForPauseAndResumeWorker.lockForRead();

    return d_pauseWorker;

    d_readWriteLockForPauseAndResumeWorker.unlock();

}

const QUuid &BaseWorker::uuid() const
{

    return d_data->d_uuid;

}

void BaseWorker::checkIfPauseWasRequested()
{

    d_readWriteLockForPauseAndResumeWorker.lockForRead();

    if(d_pauseWorker)
        d_pauseWaitCondition.wait(&d_readWriteLockForPauseAndResumeWorker);

    d_readWriteLockForPauseAndResumeWorker.unlock();

}

void BaseWorker::doWork()
{

    QThread::currentThread()->sleep(d_data->d_delayInSeconds);

    this->reportMessage("worker \"" + d_data->d_label + "\" has started");

    if (!d_data->d_parameters->isValid(d_data->d_parametersWorkerLink)) {

        this->reportError(d_data->d_parameters->lastError());

        emit this->workerInterrupted(d_data->d_uuid);

        QThread::currentThread()->quit();

        return;

    }

    if (this->performReadWriteLockLogistics()) {

        this->reportMessage(d_data->d_parameters->prettyPrint());

        emit workerStarted(d_data->d_uuid);

        this->doWork_();

    }

    if (QThread::currentThread()->isInterruptionRequested())
        emit this->workerInterrupted(d_data->d_uuid);
    else
        emit this->workerFinished(d_data->d_uuid);

    this->performReadWriteUnlockLogistics();

    QThread::currentThread()->quit();

}

bool BaseWorker::performReadWriteLockLogistics()
{

    while(!this->performReadWriteLockLogistics_()) {

        QThread::currentThread()->sleep(1);

        if (QThread::currentThread()->isInterruptionRequested())
            return false;

    }

    return true;

}

void BaseWorker::pauseWorker()
{

    d_readWriteLockForPauseAndResumeWorker.lockForWrite();

    if (d_pauseWorker == false)
        d_pauseWorker = true;

    d_readWriteLockForPauseAndResumeWorker.unlock();

    emit workerPaused();

}

void BaseWorker::performReadWriteUnlockLogistics()
{

    this->performReadWriteUnlockLogistics_();

}

void BaseWorker::reportError(const QString &error) const
{

    emit this->errorReported(d_data->d_uuid, error);

}

void BaseWorker::reportMessage(const QString &message) const
{

    emit this->messageReported(d_data->d_uuid, message);

}

void BaseWorker::reportWarning(const QString &warning) const
{

    emit this->warningReported(d_data->d_uuid, warning);

}

void BaseWorker::requestInterruption()
{

    emit this->cancelFutureWatchers();

}

void BaseWorker::resumeWorker()
{

    d_readWriteLockForPauseAndResumeWorker.lockForWrite();

    if (d_pauseWorker == true) {

        d_pauseWorker = false;

        d_pauseWaitCondition.wakeAll();

    }

    d_readWriteLockForPauseAndResumeWorker.unlock();

    emit workerResumed();

}

QUuid BaseWorker::startProgress(const QString &label, qsizetype minimum, qsizetype maximum) const
{

    QUuid uuidProgress = QUuid::createUuid();

    emit this->progressStarted(d_data->d_uuid, uuidProgress, label, minimum, maximum);

    return uuidProgress;

}

void BaseWorker::stopProgress(const QUuid &uuidProgress) const
{

    emit this->progressStopped(uuidProgress);

}

void BaseWorker::updateProgress(const QUuid &uuidProgress, qsizetype value) const
{

    emit this->progressUpdated(uuidProgress, value);

}

void BaseWorker::updateProgressLabel(const QUuid &uuidProgress, const QString &label)
{

    emit this->progressLabelUpdated(uuidProgress, label);

}

void BaseWorker::updateProgressWithOne(const QUuid &uuidProgress) const
{

    emit this->progressUpdatedWithOne(uuidProgress);

}

void BaseWorker::setDelay(int seconds)
{

    d_data->d_delayInSeconds = seconds;

}

BaseWorker::~BaseWorker()
{

}
