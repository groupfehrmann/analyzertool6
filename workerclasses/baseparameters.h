#ifndef BASEPARAMETERS_H
#define BASEPARAMETERS_H

#include <QObject>
#include <QString>
#include <QThreadPool>

class BaseParameters : public QObject
{

    Q_OBJECT

public:

    explicit BaseParameters(QObject *parent = nullptr, const QString &parametersWorkerLink = QString());

    BaseParameters(const BaseParameters &baseParameters);

    const QString &lastError() const;

    bool isValid(const QString &parametersWorkerLink);

    QString prettyPrint() const;

    BaseParameters &operator=(const BaseParameters &baseParameters);

    ~BaseParameters();

protected:

    QString d_parametersWorkerLink;

    QString d_lastError;

    QThreadPool d_threadPool;

private:

    virtual bool isValid_() = 0;

    virtual QString prettyPrint_() const = 0;

};

#endif // BASEPARAMETERS_H
