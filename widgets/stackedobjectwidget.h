#ifndef STACKEDOBJECTWIDGET_H
#define STACKEDOBJECTWIDGET_H

#include <QStackedWidget>
#include <QWidget>
#include <QObject>
#include <QUuid>
#include <QSharedPointer>
#include <QHash>
#include <QVariant>

#include "widgets/annotatedmatrixwidget.h"
#include "widgets/resulttablewidget.h"

class StackedObjectWidget : public QStackedWidget
{

    Q_OBJECT

public:

    StackedObjectWidget(QWidget *parent = nullptr);

public slots:

    void addObject(const QUuid &uuidObject, const QSharedPointer<QObject> &object);

    void removeObject(const QUuid &uuidObject);

    void removeAll();

    void showObject(const QUuid &uuidObject);

private:

    QHash<QUuid, QWidget *> d_uuidObjectToWidget;

};

#endif // STACKEDOBJECTWIDGET_H
