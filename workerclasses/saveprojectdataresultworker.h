#ifndef SAVEPROJECTDATARESULTWORKER_H
#define SAVEPROJECTDATARESULTWORKER_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QApplication>
#include <QDataStream>

#include <utility>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class SaveProjectDataResultParameters : public BaseParameters
{

public:

    SaveProjectDataResultParameters(QObject *parent = nullptr);

    QList<QSharedPointer<QObject>> objectPointers;

    QString pathToFile;

    QString projectLabel;

    bool savingProject;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class SaveProjectDataResultWorker : public BaseWorker
{

    Q_OBJECT

public:

    SaveProjectDataResultWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

signals:

    void addToRecentlyOpenedFiles(const QString objectType, const QString &label, const QString &pathToFile);

};

#endif // SAVEPROJECTDATARESULTWORKER_H
