#ifndef RESULT_H
#define RESULT_H

#include <QObject>
#include <QSharedDataPointer>
#include <QVariant>
#include <QSharedPointer>

#include <containers/annotatedmatrix.h>

class ResultData;

class Result : public QObject
{

public:

    Result(QObject *parent = nullptr);

    Result(const QString &label, BaseAnnotatedMatrix *table, QObject *parent = nullptr);

    Result(const Result &result);

    QSharedPointer<QObject> internalObject();

    Result &operator=(const Result &result);

    QString resultType() const;

    void setTable(const QString &label, BaseAnnotatedMatrix *table);

    ~Result();

    static QList<QString> createIdentifiersWithSequentialNumbers(qsizetype numberOfIdentifiers, qsizetype startNumber = 1, const QString &prefix = QString(), const QString &suffix = QString());

private:

    QSharedDataPointer<ResultData> d_data;

};

#endif // RESULT_H
