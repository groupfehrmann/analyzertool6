#ifndef BASESTATISTICALTEST_H
#define BASESTATISTICALTEST_H

#include <QList>

#include <limits>

#include "boost/math/distributions/normal.hpp"

class BaseStatisticalTest
{

public:

    BaseStatisticalTest(const qsizetype &minimumNumberOfSamples,
                        const qsizetype &maximumNumberOfSamples,
                        const qsizetype &minimumNumberOfItemsPerSample,
                        const QList<QString> &testDescriptiveLabels,
                        const bool &effectSizeAvailable,
                        const QString &effectSizeLabel,
                        const std::function<double (const double &)> &backtransformEffectSizeFunction);

    qsizetype sampleSize();

    const double &statistic();

    const double &pValue();

    double zTransformedPValue();

    const double &effectSize();

    double &standardErrorOfEffectSize();

    const QList<double> &testDescriptiveValues();

    inline qsizetype minimumNumberOfSamples() {return d_minimumNumberOfSamples;}

    inline qsizetype maximumNumberOfSamples() {return d_maximumNumberOfSamples;}

    inline qsizetype minimumNumberOfItemsPerSample() {return d_minimumNumberOfItemsPerSample;}

    inline const QList<QString> &testDescriptiveLabels() {return d_testDescriptiveLabels;}

    inline bool effectSizeAvailable() {return d_effectSizeAvailable;}

    inline const QString &effectSizeLabel() {return d_effectSizeLabel;}

    inline const std::function<double (const double &)> &backtransformEffectSizeFunction() {return d_backtransformEffectSizeFunction;}

protected:

    qsizetype d_sampleSize;

    double d_statistic;

    double d_pValue;

    double d_effectSize;

    double d_standardErrorOfEffectSize;

    QList<double> d_testDescriptiveValues;

    QList<QList<double>> d_samples;

    virtual void calculateStatistic() = 0;

    virtual void calculatePValue() = 0;

    virtual void calculateEffectSize() {}

    virtual void calculateStandardErrorEffectSize() {};

    virtual void calculateTestDescriptiveValues() = 0;

private :

    const qsizetype &d_minimumNumberOfSamples;

    const qsizetype &d_maximumNumberOfSamples;

    const qsizetype &d_minimumNumberOfItemsPerSample;

    const QList<QString> &d_testDescriptiveLabels;

    const bool &d_effectSizeAvailable;

    const QString &d_effectSizeLabel;

    const std::function<double (const double &)> &d_backtransformEffectSizeFunction;

    bool d_isPValueCalculated;

    bool d_isStatisticCalculated;

    bool d_isEffectSizeCalculated;

    bool d_isStandardErrorOfEffectSizeCalculated;

    bool d_isTestDescriptiveValuesCalculated;

};

#endif // BASESTATISTICALTEST_H
