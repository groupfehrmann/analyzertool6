#ifndef DEPENDENTTTEST_H
#define DEPENDENTTTEST_H

#include <QList>

#include "boost/math/distributions/students_t.hpp"
#include <boost/math/statistics/univariate_statistics.hpp>
#include <boost/math/statistics/bivariate_statistics.hpp>

#include "basestatisticaltest.h"

class DependentTTest : public BaseStatisticalTest
{

public:

    DependentTTest();

    DependentTTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers);

    virtual ~DependentTTest() {}

private:

    double d_degreeOfFreedom;

    double d_meanDifference;

    double d_pooledVariance;

    double d_standardErrorOfMeanDifference;

    double d_hedgeCorrectionFactor;

    double d_hedgeM;

    double d_hedgeCorrelation;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // DEPENDENTTTEST_H
