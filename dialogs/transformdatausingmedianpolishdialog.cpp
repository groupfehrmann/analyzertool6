#include "transformdatausingmedianpolishdialog.h"

TransformDataUsingMedianPolishDialog::TransformDataUsingMedianPolishDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "transform data using median polish", "transformdatausingmedianpolishdialog")
{

    TransformDataUsingMedianPolishParameters *transformDataUsingMedianPolishParameters = new TransformDataUsingMedianPolishParameters;

    transformDataUsingMedianPolishParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<TransformDataUsingMedianPolishParameters>(transformDataUsingMedianPolishParameters);

    BaseDialog::initWidgets();

}

void TransformDataUsingMedianPolishDialog::initWidgets_()
{

    TransformDataUsingMedianPolishParameters *parameters = static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data());


    d_itemSelectorWidget_rowVariables = new ItemSelectorWidget(this, "itemSelectorWidget_rowVariables");

    d_itemSelectorWidget_rowVariables->label()->setText("select row variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_rowVariables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_rowVariables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->selectedRowVariableIdentifiers = itemLabels; static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->selectedRowVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_rowVariables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->nonSelectedIdentifiers(Qt::Vertical), parameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(Qt::Vertical));

    d_itemSelectorWidget_rowVariables->itemSelectorModel_selected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->identifiers(Qt::Vertical, true), parameters->baseAnnotatedMatrix->indexes(Qt::Vertical, true));


    d_itemSelectorWidget_columnVariables = new ItemSelectorWidget(this, "itemSelectorWidget_columnVariables");

    d_itemSelectorWidget_columnVariables->label()->setText("select column variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_columnVariables, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_columnVariables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->selectedColumnVariableIdentifiers = itemLabels; static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->selectedColumnVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_columnVariables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->nonSelectedIdentifiers(Qt::Horizontal), parameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(Qt::Vertical));

    d_itemSelectorWidget_columnVariables->itemSelectorModel_selected()->setItemLabelsAndIndexes(parameters->baseAnnotatedMatrix->identifiers(Qt::Horizontal, true), parameters->baseAnnotatedMatrix->indexes(Qt::Horizontal, true));


    SpinBoxWidget *spinBoxWidget_maximumIterations = new SpinBoxWidget(this, "spinBoxWidget_maximumIterations");

    this->connect(spinBoxWidget_maximumIterations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->maximumIterations = value;});

    spinBoxWidget_maximumIterations->label()->setText("maximum number of iterations");

    spinBoxWidget_maximumIterations->spinBox()->setRange(0, 10000);

    spinBoxWidget_maximumIterations->spinBox()->setValue(2000);

    spinBoxWidget_maximumIterations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_maximumIterations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_maximumIterations, 2, 0, 1, 2);



    DoubleSpinBoxWidget *doubleSpinBoxWidget_epsilon = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_epsilon");

    this->connect(doubleSpinBoxWidget_epsilon->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->epsilon = value;});

    doubleSpinBoxWidget_epsilon->label()->setText("stopping criteria (epsilon)");

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setRange(0.00000001, 1.0);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setValue(0.00001);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setDecimals(8);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_epsilon->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_epsilon, 3, 0, 1, 2);



    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<TransformDataUsingMedianPolishParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 4, 0, 1, 2);

}
