#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QSharedDataPointer>
#include <QSharedPointer>
#include <QThread>
#include <QUuid>
#include <QPointer>
#include <QHash>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>
#include <QInputDialog>

#include "workerclasses/baseworker.h"
#include "progressmonitor.h"
#include "logmonitor.h"
#include "datarepository.h"

class ControllerData : public QSharedData
{

public:

    QSharedPointer<DataRepository> d_dataRepository;

    QSharedPointer<LogMonitor> d_logMonitor;

    QSharedPointer<ProgressMonitor> d_progressMonitor;

    bool d_runWorkersSequentially;

    bool d_enableDelayMode;

    QHash<QUuid, QThread *> d_uuidWorkerToWorkerThread;

    QHash<QUuid, BaseWorker *> d_uuidWorkerToWorker;

    QList<QUuid> d_uuidWorkers;

};

class Controller : public QObject
{

    Q_OBJECT

public:

    explicit Controller(QObject *parent = nullptr);

    void setDataRepository(QSharedPointer<DataRepository> dataRepository);

    void setLogMonitor(QSharedPointer<LogMonitor> logMonitor);

    void setProgressMonitor(QSharedPointer<ProgressMonitor> progressMonitor);

    void startWorker(BaseWorker *worker);

    ~Controller();

public slots:

    void pauseWorker(const QUuid &uuidWorker);

    void removeFromController(const QUuid &uuidWorker);

    void resumeWorker(const QUuid &uuidWorker);

    void requestInterruption(const QUuid &uuidWorker);

    void runWorkersSequentially(bool value);

    void enableDelayMode(bool value);

protected:

    QSharedDataPointer<ControllerData> d_data;

    mutable QReadWriteLock d_readWriteLock;

signals:

    void errorReported(const QUuid &uuidWorker, const QString &error) const;

    void messageReported(const QUuid &uuidWorker, const QString &message) const;

    void objectAvailable(const QUuid &uuidProject, QObject *object);

    void progressStarted(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, std::size_t minimum, std::size_t maximum) const;

    void progressStopped(const QUuid &uuidProgress) const;

    void progressUpdated(const QUuid &uuidProgress, qsizetype value) const;

    void progressLabelUpdated(const QUuid &uuidProgress, const QString &label);

    void progressUpdatedWithOne(const QUuid &uuidProgress) const;

    void warningReported(const QUuid &uuidWorker, const QString &warning) const;

    void workerFinished(const QUuid &uuidWorker) const;

    void workerInterrupted(const QUuid &uuidWorker) const;

    void workerQueued(const QUuid &uuidWorker, const QString &workerLabel, int delayInSeconds) const;

    void workerStarted(const QUuid &uuidWorker) const;

};

#endif // CONTROLLER_H
