#include "mathdescriptives.h"

double MathDescriptives::sumIgnoreNaNs(const std::span<double> &vector)
{

    double init = 0.0;

    return std::accumulate(vector.begin(), vector.end(), init, [](const double &a, const double &b){return std::isnan(b) ? a : (a + b);});

}

double MathDescriptives::meanIgnoreNans(const std::span<double> &vector)
{

    double sum = 0.0;

    qsizetype nValidValues = 0;

    for (unsigned long i = 0; i < vector.size(); ++i) {

        const double &value(vector[i]);

        if (!std::isnan(value)) {

            sum += value;

            ++nValidValues;

        }

    }

    return sum / static_cast<double>(nValidValues);

}

double MathDescriptives::l2NormIgnoreNans(const std::span<double> &vector)
{

    double sum = 0.0;

    for (unsigned long i = 0; i < vector.size(); ++i) {

        const double &value(vector[i]);

        if (!std::isnan(value))
            sum += value * value;

    }

    return std::sqrt(sum);

}

double MathDescriptives::pearsonRCorrelation(QList<double> vector1, QList<double> vector2)
{

    MathOperations::centerVectorToMeanInplace(vector1);

    MathOperations::centerVectorToMeanInplace(vector2);

    MathOperations::scaleVectorToL2NormInplace(vector1);

    MathOperations::scaleVectorToL2NormInplace(vector2);

    return cblas_ddot(vector1.count(), vector1.data(), 1.0, vector2.data(), 1.0);

}

double MathDescriptives::spearmanRCorrelation(QList<double> vector1, QList<double> vector2)
{

    double tiesCorrectionFactor1 = MathOperations::crankInplace(vector1);

    double tiesCorrectionFactor2 = MathOperations::crankInplace(vector2);

    double n = vector1.count();

    double d = 0.0;

    for (unsigned int j = 0; j < vector1.count(); ++j)
        d += (vector1.at(j) - vector2.at(j)) * (vector1.at(j) - vector2.at(j));

    double en3n = n * n * n - n;

    double fac = (1.0 - tiesCorrectionFactor2 / en3n) * (1.0 - tiesCorrectionFactor1 / en3n);

    return (1.0 - (6.0 / en3n) * (d + (tiesCorrectionFactor2 + tiesCorrectionFactor1) / 12.0)) / std::sqrt(fac);

}

double MathDescriptives::xiCorrelation(const QList<double> &vector1, QList<double> vector2)
{

    MathOperations::rankInplace(vector2);

    QList<std::pair<double, double>> pairedData;

    pairedData.reserve(vector2.count());

    for (qsizetype i = 0; i < vector2.size(); ++i)
        pairedData.append(std::pair<double, double>(vector1.at(i), vector2.at(i)));

    std::sort(pairedData.begin(), pairedData.end(), [](const std::pair<double, double> &pair1, const std::pair<double, double> &pair2){return pair1.first < pair2.first;});

    double sum1 = 0.0;

    for (qsizetype i = 0; i < pairedData.size() - 1; ++i)
        sum1 += std::abs(pairedData.at(i + 1).second - pairedData.at(i).second);

    double sum2 = 0.0;

    double n = pairedData.count();

    for (unsigned int i = 0; i < vector2.size() ; ++i) {

        double l = n - vector2.at(i);

        sum2 += l * (n - l);

    }

    return 1.0 - ((n * sum1) / (2.0 * sum2));

}

double MathDescriptives::xiCorrelation_preRanked(const QList<double> &vector1, const QList<double> &vector2_preRanked)
{

    QList<std::pair<double, double>> pairedData;

    pairedData.reserve(vector2_preRanked.count());

    for (qsizetype i = 0; i < vector2_preRanked.size(); ++i)
        pairedData.append(std::pair<double, double>(vector1.at(i), vector2_preRanked.at(i)));

    std::sort(pairedData.begin(), pairedData.end(), [](const std::pair<double, double> &pair1, const std::pair<double, double> &pair2){return pair1.first < pair2.first;});

    double sum1 = 0.0;

    for (qsizetype i = 0; i < pairedData.size() - 1; ++i)
        sum1 += std::abs(pairedData.at(i + 1).second - pairedData.at(i).second);

    double sum2 = 0.0;

    double n = pairedData.count();

    for (unsigned int i = 0; i < vector2_preRanked.size() ; ++i) {

        double l = n - vector2_preRanked.at(i);

        sum2 += l * (n - l);

    }

    return 1.0 - ((n * sum1) / (2.0 * sum2));

}

double MathDescriptives::xiCorrelation_preRanked(const std::span<double> &vector1, const std::span<double> &vector2_preRanked)
{

    QList<std::pair<double, double>> pairedData;

    pairedData.reserve(vector2_preRanked.size());

    for (unsigned long i = 0; i < vector2_preRanked.size(); ++i)
        pairedData.append(std::pair<double, double>(vector1[i], vector2_preRanked[i]));

    std::sort(pairedData.begin(), pairedData.end(), [](const std::pair<double, double> &pair1, const std::pair<double, double> &pair2){return pair1.first < pair2.first;});

    double sum1 = 0.0;

    for (qsizetype i = 0; i < pairedData.size() - 1; ++i)
        sum1 += std::abs(pairedData.at(i + 1).second - pairedData.at(i).second);

    double sum2 = 0.0;

    double n = pairedData.count();

    for (unsigned int i = 0; i < vector2_preRanked.size() ; ++i) {

        double l = n - vector2_preRanked[i];

        sum2 += l * (n - l);

    }

    return 1.0 - ((n * sum1) / (2.0 * sum2));

}

template <>
QList<std::complex<double>> MathDescriptives::sumOfColumns(const QList<std::complex<double>> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    qsizetype m = (firstTransposeMatrix ? leadingDimension : numberOfRows);

    qsizetype n = (firstTransposeMatrix ? numberOfRows : leadingDimension);

    QList<std::complex<double>> sums(n, 0.0);

    std::complex<double> one = 1;

    std::complex<double> zero = 0;

    cblas_zgemv(CblasRowMajor, (firstTransposeMatrix ? CblasNoTrans : CblasTrans), m, n, &one, matrix.data(), n, QList<double>(m, 1.0).data(), 1, &zero, sums.data(), 1);

    return sums;

}

template <>
QList<std::complex<double>> MathDescriptives::meanOfColumns(const QList<std::complex<double>> &matrix, qsizetype leadingDimension, bool firstTransposeMatrix, int numberOfThreadsToUse)
{

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(numberOfThreadsToUse);

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    qsizetype m = (firstTransposeMatrix ? leadingDimension : numberOfRows);

    qsizetype n = (firstTransposeMatrix ? numberOfRows : leadingDimension);

    QList<std::complex<double>> sums(n, 0.0);

    std::complex<double> complex_one = 1.0;

    std::complex<double> complex_zero = 1.0;

    cblas_zgemv(CblasRowMajor, (firstTransposeMatrix ? CblasNoTrans : CblasTrans), m, n, &complex_one, matrix.data(), n, QList<std::complex<double>>(m, 1.0).data(), 1, &complex_zero, sums.data(), 1);

    for (std::complex<double> &sum : sums)
        sum /= m;

    return sums;

}
