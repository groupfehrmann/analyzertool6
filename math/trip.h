    #ifndef TRIP_H
#define TRIP_H

#include <QtConcurrent>
#include <QFuture>
#include <QThreadPool>
#include <QFutureSynchronizer>

#include <algorithm>
#include <stddef.h>

#define REVERSE_VOODOO 1

namespace TRIP_transpose_algorithm {

    void next(qsizetype &i, qsizetype &count, qsizetype j0, qsizetype j1, qsizetype N, qsizetype p);

    void prev(qsizetype &i, qsizetype &count, qsizetype j0, qsizetype j1, qsizetype N, qsizetype p);

    template <typename T> void reverse(T *a, qsizetype m0, qsizetype m1, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void reverse_recursive(T *a, qsizetype m0, qsizetype m1, qsizetype l, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void reverse_offset(T *a, qsizetype m0 , qsizetype m1, qsizetype l, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N);

    template <typename T> void merge(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void merger(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype m0, qsizetype m1, qsizetype k, qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void split(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1 , qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void splitr(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype s0, qsizetype s1, qsizetype k, qsizetype N, int numberOfThreadsAvailable);

    template <typename T> void transpose(T *a, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable = QThreadPool::globalInstance()->maxThreadCount());

    template <typename T> void transpose4(T *a, qsizetype I0, qsizetype J0, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable);

}

template <typename T>
void TRIP_transpose_algorithm::reverse(T *a, qsizetype m0, qsizetype m1, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    reverse_recursive(a, m0, m1, (m1 - m0 ) / 2, i0, j0, j1, N, numberOfThreadsAvailable);

}

template <typename T>
void TRIP_transpose_algorithm::reverse_recursive(T *a, qsizetype m0, qsizetype m1, qsizetype l, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    if (l > REVERSE_VOODOO) {

        qsizetype lm = l / 2;

        if (numberOfThreadsAvailable > 0) {

            QThreadPool threadPool;

            threadPool.setMaxThreadCount(2);

            numberOfThreadsAvailable /= 2;

            QFutureSynchronizer<void> futureSynchronizer;

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse_recursive<T>, a, m0, m1, lm, i0, j0, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse_recursive<T>, a, m0 + lm, m1 - lm, l - lm, i0, j0, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.waitForFinished();

        } else {

            reverse_recursive(a, m0, m1, lm, i0, j0, j1, N, numberOfThreadsAvailable);

            reverse_recursive(a, m0 + lm, m1 - lm, l - lm, i0, j0, j1, N, numberOfThreadsAvailable);

        }

    } else
        reverse_offset(a, m0, m1, l, i0, j0, j1, N);

}

template <typename T>
void TRIP_transpose_algorithm::reverse_offset(T *a, qsizetype m0 , qsizetype m1, qsizetype l, qsizetype i0, qsizetype j0, qsizetype j1, qsizetype N)
{

    qsizetype i, next_count;

    qsizetype j, prev_count;

    qsizetype m, mm;

    qsizetype p;


    p = j1 - j0;


    i = i0 * N + j0 + (m0 / p) * N + (m0 % p);

    next_count = m0 % p;


    j = i0 * N + j0 + ((m1 - 1) / p) * N + ((m1 - 1) % p);

    prev_count = (m1 - 1) % p;


    mm = m0 + l ;

    for (m = m0; m < mm; next(i, next_count, j0, j1, N, p), prev(j, prev_count, j0, j1, N, p), m++)
        std::swap(a[j], a[i]);

}

template <typename T>
void TRIP_transpose_algorithm::merge(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    qsizetype m0, m1;

    m0 = 0 ;

    m1 = (j1 - j0) * (i1 - i0);

    merger(a, p, q, i0, i1, j0, j1, m0, m1, j1 - j0, N, numberOfThreadsAvailable);

}

template <typename T>
void TRIP_transpose_algorithm::merger(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype m0, qsizetype m1, qsizetype k, qsizetype N, int numberOfThreadsAvailable)
{

    qsizetype k2 = k / 2;

    qsizetype rm, r0, r1;

    qsizetype mm;

    if (k == 1)
        return;


    r0 = m0 + k2 * p;

    r1 = m0 + k * p + k2 * q;


    reverse(a, r0, r1, i0, j0, j1, N, numberOfThreadsAvailable);


    rm = r0 + k2 * q ;

    if (numberOfThreadsAvailable > 0) {

        QThreadPool threadPool;

        threadPool.setMaxThreadCount(2);

        numberOfThreadsAvailable /= 2;

        QFutureSynchronizer<void> futureSynchronizer;

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse<T>, a, r0, rm, i0, j0, j1, N, numberOfThreadsAvailable));

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse<T>, a, rm, r1, i0, j0, j1, N, numberOfThreadsAvailable));

        futureSynchronizer.waitForFinished();

        numberOfThreadsAvailable *= 2;

    } else {

        reverse(a, r0, rm, i0, j0, j1, N, numberOfThreadsAvailable);

        reverse(a, rm, r1, i0, j0, j1, N, numberOfThreadsAvailable);

    }

    mm = m0 + k2 * (p + q);

    if (numberOfThreadsAvailable > 0) {

        QThreadPool threadPool;

        threadPool.setMaxThreadCount(2);

        numberOfThreadsAvailable /= 2;

        QFutureSynchronizer<void> futureSynchronizer;

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, merger<T>, a, p, q, i0, i1, j0, j1, m0, mm, k2, N, numberOfThreadsAvailable));

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, merger<T>, a, p, q, i0, i1, j0, j1, mm, m1, k - k2, N, numberOfThreadsAvailable));

        futureSynchronizer.waitForFinished();

    } else {

        merger(a, p, q, i0, i1, j0, j1, m0, mm, k2, N, numberOfThreadsAvailable);

        merger(a, p, q, i0, i1, j0, j1, mm, m1, k - k2, N, numberOfThreadsAvailable);

    }

}

template <typename T>
void TRIP_transpose_algorithm::split(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    qsizetype s0, s1;

    s0 = 0;

    s1 = (j1 - j0 ) * (i1 - i0);

    splitr(a, p, q, i0, i1, j0, j1, s0, s1, i1 - i0, N, numberOfThreadsAvailable);

}

template <typename T>
void TRIP_transpose_algorithm::splitr(T *a, qsizetype p, qsizetype q, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype s0, qsizetype s1, qsizetype k, qsizetype N, int numberOfThreadsAvailable)
{

    qsizetype k2 = k / 2;

    qsizetype rm, r0, r1;

    qsizetype sm ;


    if (k == 1)
        return;


    sm = s0 + k2 * (p + q);

    if (numberOfThreadsAvailable > 0) {

        QThreadPool threadPool;

        threadPool.setMaxThreadCount(2);

        numberOfThreadsAvailable /= 2;

        QFutureSynchronizer<void> futureSynchronizer;

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, splitr<T>, a, p, q, i0, i1, j0, j1, s0, sm, k2, N, numberOfThreadsAvailable));

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, splitr<T>, a, p, q, i0, i1, j0, j1, sm, s1, k - k2, N, numberOfThreadsAvailable));

        futureSynchronizer.waitForFinished();

        numberOfThreadsAvailable *= 2;

    } else {

        splitr(a, p, q, i0, i1, j0, j1, s0, sm, k2, N, numberOfThreadsAvailable);

        splitr(a, p, q, i0, i1, j0, j1, sm, s1, k - k2, N, numberOfThreadsAvailable);

    }

    r0 = s0 + k2 * p;

    r1 = s0 + k * p + k2 * q;

    reverse(a, r0, r1, i0, j0, j1, N, numberOfThreadsAvailable);

    rm = s0 + k * p;

    if (numberOfThreadsAvailable > 0) {

        QThreadPool threadPool;

        threadPool.setMaxThreadCount(2);

        numberOfThreadsAvailable /= 2;

        QFutureSynchronizer<void> futureSynchronizer;

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse<T>, a, r0, rm, i0, j0, j1 ,N, numberOfThreadsAvailable));

        futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, reverse<T>, a, rm, r1, i0, j0, j1, N, numberOfThreadsAvailable));

        futureSynchronizer.waitForFinished();

    } else {

        reverse(a, r0, rm, i0, j0, j1 ,N, numberOfThreadsAvailable);

        reverse(a, rm, r1, i0, j0, j1, N, numberOfThreadsAvailable);

    }

}

template <typename T>
void TRIP_transpose_algorithm::transpose(T *a, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    qsizetype m, n;


    m = i1 - i0 ;

    n = j1 - j0 ;


    if ((m == 1) || (n == 1))
        return;


    if (m == n) {

        transpose4(a, i0, j0, 0, m, 0, n, N, numberOfThreadsAvailable);

        return;

    } else if (m > n) {

        qsizetype im;

        if (m < 2 * n)
            im = i0 + n ;
        else
            im = (i1 + i0) / 2;

        if (numberOfThreadsAvailable > 0) {

            QThreadPool threadPool;

            threadPool.setMaxThreadCount(2);

            numberOfThreadsAvailable /= 2;

            QFutureSynchronizer<void> futureSynchronizer;

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose<T>, a, i0, im, j0, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose<T>, a, im, i1, j0, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.waitForFinished();

            numberOfThreadsAvailable *= 2;

        } else {

            transpose(a, i0, im, j0, j1, N, numberOfThreadsAvailable);

            transpose(a, im, i1, j0, j1, N, numberOfThreadsAvailable);

        }

        merge(a, im - i0 , i1 - im, i0, i1, j0, j1 , N, numberOfThreadsAvailable);

        return;

    } else {

        qsizetype jm;

        if (2 * m > n)
            jm = j0 + m;
        else
            jm = (j1 + j0) / 2;

        if (numberOfThreadsAvailable > 0) {

            QThreadPool threadPool;

            threadPool.setMaxThreadCount(2);

            numberOfThreadsAvailable /= 2;

            QFutureSynchronizer<void> futureSynchronizer;

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose<T>, a, i0, i1, j0, jm, N, numberOfThreadsAvailable));

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose<T>, a, i0, i1, jm, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.waitForFinished();

            numberOfThreadsAvailable *= 2;

        } else {

            transpose(a, i0, i1, j0, jm, N, numberOfThreadsAvailable);

            transpose(a, i0, i1, jm, j1, N, numberOfThreadsAvailable);
        }

        split(a, jm - j0, j1 - jm, i0, i1, j0, j1, N, numberOfThreadsAvailable);

        return;
    }

}

template <typename T>
void TRIP_transpose_algorithm::transpose4(T *a, qsizetype I0, qsizetype J0, qsizetype i0, qsizetype i1, qsizetype j0, qsizetype j1, qsizetype N, int numberOfThreadsAvailable)
{

    if (i1 - i0 > 1) {

        qsizetype im = (i0 + i1) / 2, jm = (j0 + j1) / 2;

        if (numberOfThreadsAvailable > 0) {

            QThreadPool threadPool;

            if (i1 <= j0) {

                threadPool.setMaxThreadCount(4);

                numberOfThreadsAvailable /= 4;

             } else {

                threadPool.setMaxThreadCount(3);

                numberOfThreadsAvailable /= 3;

            }

            QFutureSynchronizer<void> futureSynchronizer;

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose4<T>, a, I0, J0, i0, im, j0, jm, N, numberOfThreadsAvailable));

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose4<T>, a, I0, J0, i0, im, jm, j1, N, numberOfThreadsAvailable));

            futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose4<T>, a, I0, J0, im, i1, jm, j1, N, numberOfThreadsAvailable));

            if (i1 <= j0)
                futureSynchronizer.addFuture(QtConcurrent::run(&threadPool, transpose4<T>, a, I0, J0, im, i1, j0, jm, N, numberOfThreadsAvailable));

            futureSynchronizer.waitForFinished();

        } else {

            transpose4(a, I0, J0, i0, im, j0, jm, N, numberOfThreadsAvailable);

            transpose4(a, I0, J0, i0, im, jm, j1, N, numberOfThreadsAvailable);

            transpose4(a, I0, J0, im, i1, jm, j1, N, numberOfThreadsAvailable);

            if (i1 <= j0)
                transpose4(a, I0, J0, im, i1, j0, jm, N, numberOfThreadsAvailable);

        }

    } else {

        for (qsizetype j = j0; j < j1; j++)
            std::swap(a[(I0 + j) * N + J0 + i0], a[(I0 + i0) * N + J0 + j]);

    }

}

#endif // TRIP_H
