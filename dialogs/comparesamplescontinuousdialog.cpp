#include "comparesamplescontinuousdialog.h"

CompareSamplesContinuousDialog::CompareSamplesContinuousDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "compare samples (continuous data)", "comparesamplescontinuousdialog")
{

    CompareSamplesContinuousParameters *compareSamplesContinuousParameters = new CompareSamplesContinuousParameters;

    compareSamplesContinuousParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<CompareSamplesContinuousParameters>(compareSamplesContinuousParameters);

    BaseDialog::initWidgets();

}

void CompareSamplesContinuousDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes; this->comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->currentIndex());});


    d_comboBoxWidget_annotationLabelDefiningSamples = new ComboBoxWidget(this, "d_comboBoxWidget_annotationLabelDefiningSamples");

    this->connect(d_comboBoxWidget_annotationLabelDefiningSamples->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationLabelDefiningSamples = text;});

    this->connect(d_comboBoxWidget_annotationLabelDefiningSamples->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationIndexDefiningSamples = index;});

    d_comboBoxWidget_annotationLabelDefiningSamples->label()->setText("select annotation label defining samples");

    d_gridLayout->addWidget(d_comboBoxWidget_annotationLabelDefiningSamples, 2, 0, 1, 1);


    d_itemSelectorWidget_sampleIdentifiers = new ItemSelectorWidget(this, "d_itemSelectorWidget_sampleIdentifiers");

    d_itemSelectorWidget_sampleIdentifiers->label()->setText("select sample identifiers");

    d_gridLayout->addWidget(d_itemSelectorWidget_sampleIdentifiers, 3, 0, 1, 1);

    this->connect(d_itemSelectorWidget_sampleIdentifiers->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedSampleIdentifiers = itemLabels;});


    d_comboBoxWidget_annotationLabelDefiningItemSubsets = new ComboBoxWidget(this, "d_comboBoxWidget_annotationLabelDefiningItemSubsets");

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationLabelDefiningItemSubsets = text;});

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationIndexDefiningItemSubsets = (index == 0) ? -1 : index - 1;});

    d_comboBoxWidget_annotationLabelDefiningItemSubsets->label()->setText("select annotation label defining item subsets (optional)");

    d_gridLayout->addWidget(d_comboBoxWidget_annotationLabelDefiningItemSubsets, 2, 1, 1, 1);


    d_itemSelectorWidget_itemSubsetIdentifiers = new ItemSelectorWidget(this, "d_itemSelectorWidget_itemSubsetIdentifiers");

    d_itemSelectorWidget_itemSubsetIdentifiers->label()->setText("select item subset identifiers");

    d_gridLayout->addWidget(d_itemSelectorWidget_itemSubsetIdentifiers, 3, 1, 1, 1);

    this->connect(d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->selectedItemSubsetIdentifiers = itemLabels;});


    d_comboBoxWidget_annotationLabelDefiningPairs = new ComboBoxWidget(this, "d_comboBoxWidget_annotationLabelDefiningPairs");

    this->connect(d_comboBoxWidget_annotationLabelDefiningPairs->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationLabelDefiningPairs = text;});

    this->connect(d_comboBoxWidget_annotationLabelDefiningPairs->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationIndexDefiningItemSubsets = (index == 0) ? -1 : index - 1;});


    d_comboBoxWidget_annotationLabelDefiningPairs->label()->setText("select annotation label defining pairs");

    d_gridLayout->addWidget(d_comboBoxWidget_annotationLabelDefiningPairs, 4, 0, 1, 1);


    d_comboBoxWidget_statisticalTest = new ComboBoxWidget(this, "comboBoxWidget_statisticalTest");

    this->connect(d_comboBoxWidget_statisticalTest->comboBox(), &QComboBox::currentTextChanged, this, &CompareSamplesContinuousDialog::comboBoxWidget_statisticalTestCurrentTextChanged);

    d_comboBoxWidget_statisticalTest->comboBox()->addItems({"Student T", "Welch T", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Levene F", "Kruskal-Wallis Chi-squared", "Bartlett Chi-squared", "Dependent T"});

    d_comboBoxWidget_statisticalTest->comboBox()->setCurrentIndex(0);

    d_comboBoxWidget_statisticalTest->label()->setText("select statistical test");

    //comboBoxWidget_statisticalTest->readWidgetSettings();

    d_gridLayout->addWidget(d_comboBoxWidget_statisticalTest, 5, 0, 1, 1);


    d_comboBoxWidget_metaAnalysisApproach = new ComboBoxWidget(this, "d_comboBoxWidget_metaAnalysisApproach");

    this->connect(d_comboBoxWidget_metaAnalysisApproach->comboBox(), &QComboBox::currentTextChanged, this, &CompareSamplesContinuousDialog::comboBoxWidget_metaAnalysisApproachCurrentTextChanged);

    d_comboBoxWidget_metaAnalysisApproach->comboBox()->addItems({"Fisher's method", "Fisher's trend method", "Stouffer's method", "Stouffer's trend method", "Liptak's method", "Liptak's trend method", "Lancaster's method", "Lancaster's trend method", "Generic inverse method with fixed effect model", "Generic inverse method with random effect model"});

    d_comboBoxWidget_metaAnalysisApproach->comboBox()->setCurrentIndex(0);

    d_comboBoxWidget_metaAnalysisApproach->label()->setText("select meta-analysis approach");

    //comboBoxWidget_metaAnalysisApproach->readWidgetSettings();

    d_gridLayout->addWidget(d_comboBoxWidget_metaAnalysisApproach, 5, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_permutationTestMode = new ComboBoxWidget(this, "comboBoxWidget_permutationTestMode");

    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->enablePermutationMode = (text == "enable") ? true : false;});

    comboBoxWidget_permutationTestMode->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_permutationTestMode->label()->setText("permutation test mode");

    comboBoxWidget_permutationTestMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_permutationTestMode, 6, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfPermutations = new SpinBoxWidget(this, "spinBoxWidget_numberOfPermutations");

    this->connect(spinBoxWidget_numberOfPermutations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->numberOfPermutations = value;});

    spinBoxWidget_numberOfPermutations->label()->setText("number of permutations");

    spinBoxWidget_numberOfPermutations->spinBox()->setRange(100, 10000000);

    spinBoxWidget_numberOfPermutations->spinBox()->setValue(1000);

    spinBoxWidget_numberOfPermutations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfPermutations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfPermutations, 6, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_falseDiscoveryRate = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_falseDiscoveryRate");

    this->connect(doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->falseDiscoveryRate = value / 100.0;});

    doubleSpinBoxWidget_falseDiscoveryRate->label()->setText("false discovery rate to control for in permutation mode");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setValue(5);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_falseDiscoveryRate->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_falseDiscoveryRate, 7, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_confidenceLevel = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_confidenceLevel");

    this->connect(doubleSpinBoxWidget_confidenceLevel->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->confidenceLevel = value / 100.0;});

    doubleSpinBoxWidget_confidenceLevel->label()->setText("confidence level for multivariate threshold in permutation mode");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setValue(80);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_confidenceLevel->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_confidenceLevel, 7, 1, 1, 1);


    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [spinBoxWidget_numberOfPermutations, doubleSpinBoxWidget_confidenceLevel, doubleSpinBoxWidget_falseDiscoveryRate](const QString &text) {

        if (text == "enable") {

            spinBoxWidget_numberOfPermutations->setEnabled(true);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(true);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(true);

        } else {

            spinBoxWidget_numberOfPermutations->setEnabled(false);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(false);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(false);

        };
    });

    spinBoxWidget_numberOfPermutations->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_confidenceLevel->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");


    d_itemSelectorWidget_annotationLabelsForVariables = new ItemSelectorWidget(this, "d_itemSelectorWidget_annotationLabelsForVariables");

    d_itemSelectorWidget_annotationLabelsForVariables->label()->setText("select annotation labels for variables (optional)");

    d_gridLayout->addWidget(d_itemSelectorWidget_annotationLabelsForVariables, 8, 0, 1, 1);

    this->connect(d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->annotationLabelsForVariables = itemLabels;});


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 9, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 10, 0, 1, 2);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &CompareSamplesContinuousDialog::orientationChanged);

    this->connect(d_comboBoxWidget_annotationLabelDefiningSamples->comboBox(), &QComboBox::currentIndexChanged, this, &CompareSamplesContinuousDialog::comboBoxWidget_annotationLabelDefiningSamplesCurrentIndexChanged);

    this->connect(d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox(), &QComboBox::currentIndexChanged, this, &CompareSamplesContinuousDialog::comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void CompareSamplesContinuousDialog::orientationChanged(Qt::Orientation orientation)
{

    CompareSamplesContinuousParameters *compareSamplesContinuousParameters = static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(compareSamplesContinuousParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), compareSamplesContinuousParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(compareSamplesContinuousParameters->baseAnnotatedMatrix->identifiers(orientation, true), compareSamplesContinuousParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(compareSamplesContinuousParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), compareSamplesContinuousParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(compareSamplesContinuousParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), compareSamplesContinuousParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));


    d_comboBoxWidget_annotationLabelDefiningSamples->comboBox()->clear();

    d_comboBoxWidget_annotationLabelDefiningSamples->comboBox()->addItems(compareSamplesContinuousParameters->baseAnnotatedMatrix->annotationLabels(BaseAnnotatedMatrix::switchOrientation(orientation), false));


    QList<QString> itemLabels;

    itemLabels.append(QString());

    for (const QString &identifier : compareSamplesContinuousParameters->baseAnnotatedMatrix->annotationLabels(BaseAnnotatedMatrix::switchOrientation(orientation), false))
        itemLabels.append(identifier);


    d_comboBoxWidget_annotationLabelDefiningPairs->comboBox()->clear();

    d_comboBoxWidget_annotationLabelDefiningPairs->comboBox()->addItems(itemLabels);


    d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->clear();

    d_comboBoxWidget_annotationLabelDefiningItemSubsets->comboBox()->addItems(itemLabels);


    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(compareSamplesContinuousParameters->baseAnnotatedMatrix->annotationLabels(orientation));

    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected()->clear();

}

void CompareSamplesContinuousDialog::comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(qsizetype index)
{

    if (index <= 0) {

        d_itemSelectorWidget_itemSubsetIdentifiers->clear();

        d_itemSelectorWidget_itemSubsetIdentifiers->setEnabled(false);

        d_comboBoxWidget_metaAnalysisApproach->setEnabled(false);

        return;

    }

    d_comboBoxWidget_metaAnalysisApproach->setEnabled(true);

    d_itemSelectorWidget_itemSubsetIdentifiers->setEnabled(true);

    CompareSamplesContinuousParameters *compareSamplesContinuousParameters = static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data());

    QSet<QString> itemSubsetIdentifiers;

    for (qsizetype indexOfSelectedItem : compareSamplesContinuousParameters->selectedItemIndexes)
        itemSubsetIdentifiers.insert(compareSamplesContinuousParameters->baseAnnotatedMatrix->annotationValue(indexOfSelectedItem, index - 1, BaseAnnotatedMatrix::switchOrientation(d_orientationWidget->orientation())).toString());

    QList<QString> temp(itemSubsetIdentifiers.begin(), itemSubsetIdentifiers.end());

    std::sort(temp.begin(), temp.end());

    d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(temp);

    d_itemSelectorWidget_itemSubsetIdentifiers->itemSelectorModel_selected()->clear();

}

void CompareSamplesContinuousDialog::comboBoxWidget_annotationLabelDefiningSamplesCurrentIndexChanged(qsizetype index)
{

    if (index == -1)
        return;

    CompareSamplesContinuousParameters *compareSamplesContinuousParameters = static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data());

    QSet<QString> sampleIdentifiers;

    for (qsizetype indexOfSelectedItem : compareSamplesContinuousParameters->selectedItemIndexes)
        sampleIdentifiers.insert(compareSamplesContinuousParameters->baseAnnotatedMatrix->annotationValue(indexOfSelectedItem, index, BaseAnnotatedMatrix::switchOrientation(d_orientationWidget->orientation())).toString());

    QList<QString> temp(sampleIdentifiers.begin(), sampleIdentifiers.end());

    std::sort(temp.begin(), temp.end());

    d_itemSelectorWidget_sampleIdentifiers->itemSelectorModel_selected()->clear();

    d_itemSelectorWidget_sampleIdentifiers->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(temp);

}

void CompareSamplesContinuousDialog::comboBoxWidget_statisticalTestCurrentTextChanged(const QString &text)
{

    CompareSamplesContinuousParameters *parameters = static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data());

    parameters->statisticalTest = text;

    if (text == "Student T") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new StudentTTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Welch T") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new WelchTTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Mann-Whitney U") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new MannWhitneyUTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Kolmogorov-Smirnov Z") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new KolmogorovSmirnovTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Levene F") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new LeveneFTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Bartlett Chi-squared") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new BartlettChiSquaredTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Kruskal-Wallis Chi-squared") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new KruskalWallisChiSquaredTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(false);

        parameters->annotationIndexDefiningPairs = -1;

    } else if (text == "Dependent T") {

        parameters->baseStatisticalTest = QSharedPointer<BaseStatisticalTest>(new DependentTTest());

        d_comboBoxWidget_annotationLabelDefiningPairs->setEnabled(true);

        parameters->annotationIndexDefiningPairs = (d_comboBoxWidget_annotationLabelDefiningPairs->comboBox()->currentIndex() == 0) ? -1 : d_comboBoxWidget_annotationLabelDefiningPairs->comboBox()->currentIndex() - 1;

    }

}

void CompareSamplesContinuousDialog::comboBoxWidget_metaAnalysisApproachCurrentTextChanged(const QString &text)
{

    CompareSamplesContinuousParameters *parameters = static_cast<CompareSamplesContinuousParameters *>(BaseDialog::d_parameters.data());

    parameters->metaAnalysisApproach = text;

    if (text == "Fisher's method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new FishersMetaAnalysisApproach());
    else if (text == "Fisher's trend method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new FishersTrendMetaAnalysisApproach());
    else if (text == "Stouffer's method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new StouffersMetaAnalysisApproach());
    else if (text == "Stouffer's trend method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new StouffersTrendMetaAnalysisApproach());
    else if (text == "Liptak's method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new LiptaksMetaAnalysisApproach());
    else if (text == "Liptak's trend method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new LiptaksTrendMetaAnalysisApproach());
    else if (text == "Lancaster's method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new LancastersMetaAnalysisApproach());
    else if (text == "Lancaster's trend method")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new LancastersTrendMetaAnalysisApproach());
    else if (text == "Generic inverse method with fixed effect model")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach());
    else if (text == "Generic inverse method with random effect model")
        parameters->baseMetaAnalysisApproach = QSharedPointer<BaseMetaAnalysisApproach>(new GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach());


}
