#ifndef REPLACEIDENTIFIERWITHANNOTATIONVALUEWORKER_H
#define REPLACEIDENTIFIERWITHANNOTATIONVALUEWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class ReplaceIdentifierWithAnnotationValueParameters : public BaseParameters
{

public:

    ReplaceIdentifierWithAnnotationValueParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QString annotationLabelUsedForReplacement;

    qsizetype annotationIndexUsedForReplacement;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ReplaceIdentifierWithAnnotationValueWorker : public BaseWorker
{

public:

    ReplaceIdentifierWithAnnotationValueWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void ReplaceIdentifierWithAnnotationValueWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<ReplaceIdentifierWithAnnotationValueParameters> parameters(d_data->d_parameters.dynamicCast<ReplaceIdentifierWithAnnotationValueParameters>());

    QUuid uuidProgress = this->startProgress("replacing identifiers", 0, parameters->selectedVariableIndexes.count());

    for (qsizetype selectedVariableIndex : parameters->selectedVariableIndexes) {

        annotatedMatrix->setIdentifier(selectedVariableIndex, annotatedMatrix->annotationValue(selectedVariableIndex, parameters->annotationIndexUsedForReplacement, parameters->orientation).toString(), parameters->orientation);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    }

    this->stopProgress(uuidProgress);

}

#endif // REPLACEIDENTIFIERWITHANNOTATIONVALUEWORKER_H
