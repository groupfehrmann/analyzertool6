#include "shuffleidentifiersworker.h"

ShuffleIdentifiersParameters::ShuffleIdentifiersParameters(QObject *parent) :
    BaseParameters(parent, "shuffleidentifiers")
{

}

bool ShuffleIdentifiersParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    return true;

}

QString ShuffleIdentifiersParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[shuffle identifiers in data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    return prettyPrintString;

}

ShuffleIdentifiersWorker::ShuffleIdentifiersWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("shuffle identifiers"), parameters, "shuffleidentifiers")
{

}

void ShuffleIdentifiersWorker::doWork_()
{

    QSharedPointer<ShuffleIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<ShuffleIdentifiersParameters>());

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(QString).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned char).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned char>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(bool).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<bool>>(parameters->baseAnnotatedMatrix));

}

bool ShuffleIdentifiersWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<ShuffleIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<ShuffleIdentifiersParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    return true;

}

void ShuffleIdentifiersWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<ShuffleIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<ShuffleIdentifiersParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}
