#include "progressmonitormodel.h"
#include <QProgressBar>

ProgressBarDelegate::ProgressBarDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

}

void ProgressBarDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if (!index.isValid() || !index.parent().isValid() || (index.column() != 3)) {

        QStyledItemDelegate::paint(painter, option, index);

        return;

    }

    QList<QVariant> data = index.data().toList();

    QStyleOptionProgressBar progressBarOption;

    progressBarOption.rect = option.rect;

    qsizetype minimum = data.at(0).toLongLong();

    qsizetype maximum = data.at(1).toLongLong();

    progressBarOption.minimum = 0;

    progressBarOption.maximum = 1000;

    progressBarOption.progress = (static_cast<double>(data.at(2).toLongLong() - minimum) / static_cast<double>(maximum)) * 1000;

    progressBarOption.state = QStyle::State_Horizontal | QStyle::State_Item | QStyle::State_Open;

    #ifdef Q_OS_MAC
    painter->translate(progressBarOption.rect.left(), progressBarOption.rect.top());
    #endif

    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);

    painter->resetTransform();

}

InterruptWorkerButtonDelegate::InterruptWorkerButtonDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

    d_rowOfButtonClicked = -1;

}

void InterruptWorkerButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if (!index.isValid())
        return;

    QApplication::style()->drawPrimitive(QStyle::PE_PanelItemViewItem, &option, painter, 0);

    if (index.parent().isValid())
        return;

    QStyleOptionButton button;

    button.rect = option.rect;

    button.text = "interrupt";

    if (d_rowOfButtonClicked == index.row())
        button.state = QStyle::State_Sunken | QStyle::State_Enabled;
    else
        button.state = QStyle::State_Raised | QStyle::State_Enabled;

    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);


}

bool InterruptWorkerButtonDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{

    Q_UNUSED(model);

    Q_UNUSED(option);

    if (event->type() == QEvent::MouseButtonPress)
        d_rowOfButtonClicked = index.row();

    if (event->type() == QEvent::MouseButtonRelease) {

        d_rowOfButtonClicked = -1;

        emit interruptionRequested(index.data().toUuid());

    }

    return true;

}

PauseWorkerButtonDelegate::PauseWorkerButtonDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

    d_rowOfButtonClicked = -1;

}

void PauseWorkerButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if (!index.isValid())
        return;

    QApplication::style()->drawPrimitive(QStyle::PE_PanelItemViewItem, &option, painter, 0);

    if (index.parent().isValid())
        return;

    QStyleOptionButton button;

    button.rect = option.rect;

    button.text = "pause";

    if (d_rowOfButtonClicked == index.row())
        button.state = QStyle::State_Sunken | QStyle::State_Enabled;
    else
        button.state = QStyle::State_Raised | QStyle::State_Enabled;

    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);


}

bool PauseWorkerButtonDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{

    Q_UNUSED(model);

    Q_UNUSED(option);

    if (event->type() == QEvent::MouseButtonPress)
        d_rowOfButtonClicked = index.row();

    if (event->type() == QEvent::MouseButtonRelease) {

        d_rowOfButtonClicked = -1;

        emit pauseRequested(index.data().toUuid());

    }

    return true;

}

ResumeWorkerButtonDelegate::ResumeWorkerButtonDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

    d_rowOfButtonClicked = -1;

}

void ResumeWorkerButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    if (!index.isValid())
        return;

    QApplication::style()->drawPrimitive(QStyle::PE_PanelItemViewItem, &option, painter, 0);

    if (index.parent().isValid())
        return;

    QStyleOptionButton button;

    button.rect = option.rect;

    button.text = "resume";

    if (d_rowOfButtonClicked == index.row())
        button.state = QStyle::State_Sunken | QStyle::State_Enabled;
    else
        button.state = QStyle::State_Raised | QStyle::State_Enabled;

    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);


}

bool ResumeWorkerButtonDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{

    Q_UNUSED(model);

    Q_UNUSED(option);

    if (event->type() == QEvent::MouseButtonPress)
        d_rowOfButtonClicked = index.row();

    if (event->type() == QEvent::MouseButtonRelease) {

        d_rowOfButtonClicked = -1;

        emit resumeRequested(index.data().toUuid());

    }

    return true;

}

ProgressMonitorModel::ProgressMonitorModel(QObject *parent, const QSharedPointer<ProgressMonitor> &progressMonitor)
    : QAbstractItemModel(parent),
      d_progressMonitor(progressMonitor)
{

    this->connect(progressMonitor.data(), &ProgressMonitor::messageReported, this, &ProgressMonitorModel::messageReported);

    this->startTimer(100);

}

QVariant ProgressMonitorModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if (orientation == Qt::Vertical)
        return QVariant();

    if (role == Qt::DisplayRole) {

        switch (section) {

        case 0 : return QString("worker/progress label");

        case 1 : return QString("status");

        case 2 : return QString("duration");

        case 3 : return QString("progress bar");

        case 4 : return QString("ETA");

        default : return QVariant();

        }

     }

    return QVariant();

}

QModelIndex ProgressMonitorModel::index(int row, int column, const QModelIndex &parent) const
{

    if (!this->hasIndex(row, column, parent))
        return QModelIndex();

    if (!parent.isValid())
        return this->createIndex(row, column, &d_progressMonitor->uuidsWorker().at(row));

    return this->createIndex(row, column, &d_progressMonitor->uuidsProgress(*static_cast<const QUuid *>(parent.internalPointer())).at(row));

}

QModelIndex ProgressMonitorModel::parent(const QModelIndex &index) const
{

    if (!index.isValid())
        return QModelIndex();

    QUuid uuid = *static_cast<const QUuid *>(index.internalPointer());

    if (d_progressMonitor->isUuidWorker(uuid))
        return QModelIndex();

    qsizetype indexOfUuidWorker = d_progressMonitor->indexOfUuidWorker(d_progressMonitor->uuidWorker(uuid));

    return this->createIndex(indexOfUuidWorker, 0, &d_progressMonitor->uuidsWorker().at(indexOfUuidWorker));

}


int ProgressMonitorModel::rowCount(const QModelIndex &parent) const
{

    if (!parent.isValid())
        return d_progressMonitor->uuidsWorker().count();

    if (d_progressMonitor->isUuidWorker(*static_cast<const QUuid *>(parent.internalPointer())))
        return d_progressMonitor->workerInfo(*static_cast<const QUuid *>(parent.internalPointer())).d_uuidsProgress.count();

    return 0;

}

int ProgressMonitorModel::columnCount(const QModelIndex &parent) const
{

    Q_UNUSED(parent);

    return 8;

}

QVariant ProgressMonitorModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    QUuid uuid = *static_cast<const QUuid *>(index.internalPointer());

    if (d_progressMonitor->isUuidWorker(uuid)) {

        const ProgressMonitorData::WorkerInfo &workerInfo(d_progressMonitor->workerInfo(uuid));

        if (role == Qt::DisplayRole) {

            switch (index.column()) {

            case 0 : return workerInfo.d_label;

            case 1 : return (workerInfo.d_interrupted) ? "interruption requested" : ((!workerInfo.d_paused) ? ((workerInfo.d_startTime == QDateTime()) ? ((workerInfo.d_delayInSeconds == 0) ? "queued" : "queued & delayed") : "running") : "paused");

            case 2 : return (workerInfo.d_startTime == QDateTime()) ? QVariant() : ProgressMonitor::secondsToTimeString(workerInfo.d_startTime.secsTo(QDateTime::currentDateTime()));

            case 5 : return uuid;

            case 6 : return uuid;

            case 7 : return uuid;

            default: return QVariant();

            }

        } else if (role == Qt::SizeHintRole) {

            QSize size;

            size.setHeight(25);

            return size;

        } else if (role == Qt::StatusTipRole) {

            switch (index.column()) {

            case 0 : return workerInfo.d_label;

            default: return QVariant();

            }

        }

    } else {

        const ProgressMonitorData::ProgressInfo &progressInfo(d_progressMonitor->progressInfo(uuid));

        if (role == Qt::DisplayRole) {

            switch (index.column()) {

            case 0 : return progressInfo.d_label;

            case 1 : return (progressInfo.d_interrupted) ? "interruption requested" : (progressInfo.d_paused ? "paused" : "running");

            case 2 : return ProgressMonitor::secondsToTimeString(progressInfo.d_startTime.secsTo(QDateTime::currentDateTime()));

            case 3 : return QList<QVariant> {progressInfo.d_minimum, progressInfo.d_maximum, progressInfo.d_value};

            case 4 : return (progressInfo.d_etaInSeconds == -1) ? "?" : ProgressMonitor::milliSecondsToTimeString(progressInfo.d_etaInSeconds);

            default : return QVariant();

            }

        } else if (role == Qt::ForegroundRole)
            return QBrush(Qt::darkGray);
        else if (role == Qt::SizeHintRole) {

            QSize size;

            size.setHeight(25);

            return size;

        } else if (role == Qt::StatusTipRole) {

            switch (index.column()) {

            case 0 : return progressInfo.d_label;

            default: return QVariant();

            }

        }

    }

    return QVariant();
}

void ProgressMonitorModel::putWorkerInQueu(const QUuid &uuidWorker, const QString &workerLabel, int delayInSeconds)
{

    qsizetype row = d_progressMonitor->uuidsWorker().count();

    this->beginInsertRows(QModelIndex(), row, row);

    d_progressMonitor->putWorkerInQueu(uuidWorker, workerLabel, delayInSeconds);

    this->endInsertRows();

}

void ProgressMonitorModel::startMonitoringWorker(const QUuid &uuidWorker)
{

    d_progressMonitor->startMonitoringWorker(uuidWorker);

}

void ProgressMonitorModel::startProgress(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, qsizetype minimum, qsizetype maximum)
{

    qsizetype row = d_progressMonitor->uuidsProgress(uuidWorker).count();

    this->beginInsertRows(this->index(d_progressMonitor->indexOfUuidWorker(uuidWorker), 0, QModelIndex()), row, row);

    d_progressMonitor->startProgress(uuidWorker, uuidProgress, label, minimum, maximum);

    this->endInsertRows();

}

void ProgressMonitorModel::stopAllMonitoringAssociatedWithUuid(const QUuid &uuid)
{

    if (d_progressMonitor->isUuidWorker(uuid)) {

        qsizetype row = d_progressMonitor->indexOfUuidWorker(uuid);

        this->beginRemoveRows(QModelIndex(), row, row);

        d_progressMonitor->stopAllMonitoringAssociatedWithUuid(uuid);

        this->endRemoveRows();

    } else {

        qsizetype row = d_progressMonitor->indexOfUuidProgress(uuid);

        this->beginRemoveRows(this->index(d_progressMonitor->indexOfUuidWorker(d_progressMonitor->uuidWorker(uuid)), 0, QModelIndex()), row, row);

        d_progressMonitor->stopAllMonitoringAssociatedWithUuid(uuid);

        this->endRemoveRows();

    }

}

void ProgressMonitorModel::updateProgress(const QUuid &uuidProgress, qsizetype value)
{

    d_progressMonitor->updateProgress(uuidProgress, value);

    QModelIndex parent = this->index(d_progressMonitor->indexOfUuidWorkerOfWhichUuidProgressIsMember(uuidProgress), 0, QModelIndex());

    QModelIndex index = this->index(d_progressMonitor->indexOfUuidProgress(uuidProgress), 3, parent);

    emit this->dataChanged(index, index, {Qt::DisplayRole});

}

void ProgressMonitorModel::updateProgressLabel(const QUuid &uuidProgress, const QString &label)
{

    d_progressMonitor->updateProgressLabel(uuidProgress, label);

    QModelIndex parent = this->index(d_progressMonitor->indexOfUuidWorkerOfWhichUuidProgressIsMember(uuidProgress), 0, QModelIndex());

    QModelIndex index = this->index(d_progressMonitor->indexOfUuidProgress(uuidProgress), 0, parent);

    emit this->dataChanged(index, index, {Qt::DisplayRole});

}

void ProgressMonitorModel::updateProgressWithOne(const QUuid &uuidProgress)
{

    d_progressMonitor->updateProgressWithOne(uuidProgress);

    QModelIndex parent = this->index(d_progressMonitor->indexOfUuidWorkerOfWhichUuidProgressIsMember(uuidProgress), 0, QModelIndex());

    QModelIndex index = this->index(d_progressMonitor->indexOfUuidProgress(uuidProgress), 3, parent);

    emit this->dataChanged(index, index, {Qt::DisplayRole});

}

void ProgressMonitorModel::timerEvent(QTimerEvent *event)
{

    Q_UNUSED(event)

    emit this->dataChanged(this->index(0, 2), this->index(this->rowCount(), 2));

    for (qsizetype i = 0; i < this->rowCount(); ++i) {

        QModelIndex parentIndex = this->index(i, 0, QModelIndex());

        emit this->dataChanged(this->index(0, 1, parentIndex), this->index(this->rowCount(parentIndex), 4, parentIndex));

    }

}
