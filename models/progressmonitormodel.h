#ifndef PROGRESSMONITORMODEL_H
#define PROGRESSMONITORMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>
#include <QStyledItemDelegate>
#include <QStyleOptionProgressBar>
#include <QStyleOptionButton>
#include <QApplication>
#include <QPainter>
#include <QSize>
#include <QDateTime>

#include "progressmonitor.h"

class ProgressBarDelegate : public QStyledItemDelegate
{

    Q_OBJECT

public:

    ProgressBarDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

};

class InterruptWorkerButtonDelegate : public QStyledItemDelegate
{

    Q_OBJECT

public:

    InterruptWorkerButtonDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

signals:

    void interruptionRequested(const QUuid &uuidWorker);

private:

    qsizetype d_rowOfButtonClicked;

};

class PauseWorkerButtonDelegate : public QStyledItemDelegate
{

    Q_OBJECT

public:

    PauseWorkerButtonDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

signals:

    void pauseRequested(const QUuid &uuidWorker);

private:

    qsizetype d_rowOfButtonClicked;

};

class ResumeWorkerButtonDelegate : public QStyledItemDelegate
{

    Q_OBJECT

public:

    ResumeWorkerButtonDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;

signals:

    void resumeRequested(const QUuid &uuidWorker);

private:

    qsizetype d_rowOfButtonClicked;

};

class ProgressMonitorModel : public QAbstractItemModel
{
    Q_OBJECT

public:

    explicit ProgressMonitorModel(QObject *parent = nullptr, const QSharedPointer<ProgressMonitor> &progressMonitor = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;

    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:

    void putWorkerInQueu(const QUuid &uuidWorker, const QString &workerLabel, int delayInSeconds);

    void startMonitoringWorker(const QUuid &uuidWorker);

    void startProgress(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, qsizetype minimum, qsizetype maximum);

    void stopAllMonitoringAssociatedWithUuid(const QUuid &uuid);

    void updateProgress(const QUuid &uuidProgress, qsizetype value);

    void updateProgressLabel(const QUuid &uuidProgress, const QString &label);

    void updateProgressWithOne(const QUuid &uuidProgress);

protected:

    void timerEvent(QTimerEvent *event) override;

private:

    QSharedPointer<ProgressMonitor> d_progressMonitor;

signals:

    void interruptionRequested(const QUuid &uuidWorker);

    void messageReported(const QUuid &uuidWorker, const QString &message) const;

    void pauseRequested(const QUuid &uuidWorker);

    void resumeRequested(const QUuid &uuidWorker);

};

#endif // PROGRESSMONITORMODEL_H
