#ifndef TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSWORKER_H
#define TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"

class TransformDataUsingSequenceOfFunctionsParameters : public BaseParameters
{

public:

    TransformDataUsingSequenceOfFunctionsParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<QString> selectedTransformFunctionDescriptionsWithParameterReplacement;

    QList<std::function<void (QList<double> &)>> selectedTransformFunctions;

    QString exportDirectory;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransformDataUsingSequenceOfFunctionsWorker : public BaseWorker
{

public:

    TransformDataUsingSequenceOfFunctionsWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void TransformDataUsingSequenceOfFunctionsWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<TransformDataUsingSequenceOfFunctionsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingSequenceOfFunctionsParameters>());

    QUuid uuidProgress = this->startProgress("transforming data", 0, parameters->selectedVariableIndexes.count());

    std::function<void(qsizetype)> mapFunctor = [this, &uuidProgress, &parameters, &annotatedMatrix](qsizetype selectedVariableIndex) {

        QList<std::reference_wrapper<T>> vectorWithReferences;

        if (parameters->orientation == Qt::Vertical)
            vectorWithReferences = annotatedMatrix->sliced_references({selectedVariableIndex}, parameters->selectedItemIndexes);
        else
            vectorWithReferences = annotatedMatrix->sliced_references(parameters->selectedItemIndexes, {selectedVariableIndex});

        QList<double> vectorOfValues;

        vectorOfValues.reserve(vectorWithReferences.count());

        for (const std::reference_wrapper<T> &value : vectorWithReferences)
            vectorOfValues.append(value);

        for (std::function<void (QList<double> &)> func : parameters->selectedTransformFunctions)
            func(vectorOfValues);

        for (qsizetype i = 0; i < vectorWithReferences.count(); ++i)
            vectorWithReferences[i].get() = vectorOfValues.at(i);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    };

    QtConcurrent::blockingMap(&d_threadPool, parameters->selectedVariableIndexes, mapFunctor);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

}

#endif // TRANSFORMDATAUSINGSEQUENCEOFFUNCTIONSWORKER_H
