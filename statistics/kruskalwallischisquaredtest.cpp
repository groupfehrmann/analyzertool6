#include "kruskalwallischisquaredtest.h"

qsizetype KruskalWallisChiSquaredTest::d_minimumNumberOfSamples = 2;

qsizetype KruskalWallisChiSquaredTest::d_maximumNumberOfSamples = std::numeric_limits<qsizetype>::max();

qsizetype KruskalWallisChiSquaredTest::d_minimumNumberOfItemsPerSample = 2;

bool KruskalWallisChiSquaredTest::d_effectSizeAvailable = true;

QString KruskalWallisChiSquaredTest::d_effectSizeLabel = "Eta-squared";

QList<QString> KruskalWallisChiSquaredTest::d_testDescriptiveLabels = {"Eta-squared", "Std. Error Eta-squared", "Kruskal-Wallis Chi-squared", "df", "Sig. (2-tailed)"};

std::function<double (const double &)> KruskalWallisChiSquaredTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return std::tanh(effectSize);};

KruskalWallisChiSquaredTest::KruskalWallisChiSquaredTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

KruskalWallisChiSquaredTest::KruskalWallisChiSquaredTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    QList<double> rankedData;

    for (qsizetype i = 0; i < data.count(); ++i) {

        if (codedSampleIdentifiers.at(i) != -1)
            rankedData.append(data.at(i));

    }

    d_tiesCorrectionFactor = MathOperations::crankInplace(rankedData);

    d_samples.resize(numberOfSamples);

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1)
            d_samples[codedSampleIdentifier].append(rankedData.at(i));

    }

    d_sampleSize = data.count();

}

void KruskalWallisChiSquaredTest::calculateStatistic()
{

    double k = 0.0;

    for (const QList<double> & sample : d_samples) {

        double temp = std::accumulate(sample.begin(), sample.end(), 0.0);

        k += temp * temp / sample.count();

    }

    double n = d_sampleSize;

    k *= (12.0 / (n * (n + 1.0)));

    k -= (3.0 * (n + 1.0));

    k /= (1.0 - d_tiesCorrectionFactor / (n * n * n - n));

    d_statistic = k;

}

void KruskalWallisChiSquaredTest::calculatePValue()
{

    this->calculateStatistic();

    d_degreeOfFreedom = d_samples.count() - 1.0;

    if ((d_degreeOfFreedom <= 0.0) || d_statistic < 0.0 || std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_degreeOfFreedom);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void KruskalWallisChiSquaredTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    this->calculateEffectSize();

    this->calculateStandardErrorEffectSize();

    d_testDescriptiveValues << d_effectSize << d_standardErrorOfEffectSize << d_statistic << d_degreeOfFreedom << d_pValue;

}

void KruskalWallisChiSquaredTest::calculateEffectSize()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_effectSize = (d_statistic - d_samples.count() + 1) / (d_sampleSize - d_samples.count());

}

void KruskalWallisChiSquaredTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = std::sqrt(2.0 * d_effectSize / d_degreeOfFreedom);

}
