#ifndef LIPTAKSTRENDMETAANALYSISAPPROACH_H
#define LIPTAKSTRENDMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/normal.hpp"

#include "basemetaanalysisapproach.h"

class LiptaksTrendMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    LiptaksTrendMetaAnalysisApproach();

    LiptaksTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~LiptaksTrendMetaAnalysisApproach() {}

private:

    QList<double> d_statistics;

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};


#endif // LIPTAKSTRENDMETAANALYSISAPPROACH_H
