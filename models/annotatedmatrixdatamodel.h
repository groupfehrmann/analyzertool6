#ifndef ANNOTATEDMATRIXDATAMODEL_H
#define ANNOTATEDMATRIXDATAMODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include <QColor>
#include <QSortFilterProxyModel>

#include "containers/annotatedmatrix.h"

class AnnotatedMatrixDataSortFilterProxyModel : public QSortFilterProxyModel
{

    Q_OBJECT

public:

    explicit AnnotatedMatrixDataSortFilterProxyModel(QObject *parent = nullptr);

    bool showSelectedOnly() const;

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

    bool filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const;

private:

    bool d_showSelectedOnly;

public slots:

    void setShowSelectedOnly(bool showSelectedOnly);

};

class AnnotatedMatrixDataModel : public QAbstractTableModel
{

    Q_OBJECT

public:

    explicit AnnotatedMatrixDataModel(QObject *parent = nullptr, QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = nullptr);

    const BaseAnnotatedMatrix *baseAnnotatedMatrix() const;

    void detachSharedPointerToBaseAnnotatedMatrix();

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

private:

    QSharedPointer<BaseAnnotatedMatrix> d_baseAnnotatedMatrix;

};

#endif // ANNOTATEDMATRIXDATAMODEL_H
