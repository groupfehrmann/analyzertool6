#ifndef LOGMONITOR_H
#define LOGMONITOR_H

#include <QObject>
#include <QSharedDataPointer>
#include <QUuid>
#include <QString>
#include <QDateTime>
#include <QList>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

class LogMonitorData : public QSharedData
{

public:

    enum LogType {

        ERROR = 1,

        MESSAGE = 2,

        WARNING = 3

    };

    struct LogItem {

        QUuid d_uuidWorker;

        QDateTime d_dataTimeStamp;

        QString d_string;

        LogType d_logType;

    };

    QList<LogItem> d_logItems;

};

class LogMonitor : public QObject
{

    Q_OBJECT

public:

    explicit LogMonitor(QObject *parent = nullptr);

    ~LogMonitor();

    qsizetype count() const;

    const LogMonitorData::LogItem &logItemAt(qsizetype index) const;

public slots:

    void clear();

    void reportError(const QUuid &uuidWorker, const QString &error);

    void reportMessage(const QUuid &uuidWorker, const QString &message);

    void reportWarning(const QUuid &uuidWorker, const QString &warning);

private:

    QSharedDataPointer<LogMonitorData> d_data;

    mutable QReadWriteLock d_readWriteLock;

};

#endif // LOGMONITOR_H
