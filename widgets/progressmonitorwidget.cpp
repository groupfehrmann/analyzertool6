#include "progressmonitorwidget.h"
#include "ui_progressmonitorwidget.h"

#include "models/progressmonitormodel.h"

ProgressMonitorWidget::ProgressMonitorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProgressMonitorWidget)
{

    ui->setupUi(this);

}

void ProgressMonitorWidget::setProgressMonitorModel(ProgressMonitorModel *progressMonitorModel)
{

    ui->treeView->setModel(progressMonitorModel);

    InterruptWorkerButtonDelegate *interruptWorkerButtonDelegate = new InterruptWorkerButtonDelegate(ui->treeView);

    PauseWorkerButtonDelegate *pauseWorkerButtonDelegate = new PauseWorkerButtonDelegate(ui->treeView);

    ResumeWorkerButtonDelegate *resumeWorkerButtonDelegate = new ResumeWorkerButtonDelegate(ui->treeView);

    this->connect(interruptWorkerButtonDelegate, &InterruptWorkerButtonDelegate::interruptionRequested, progressMonitorModel, &ProgressMonitorModel::interruptionRequested);

    this->connect(pauseWorkerButtonDelegate, &PauseWorkerButtonDelegate::pauseRequested, progressMonitorModel, &ProgressMonitorModel::pauseRequested);

    this->connect(resumeWorkerButtonDelegate, &ResumeWorkerButtonDelegate::resumeRequested, progressMonitorModel, &ProgressMonitorModel::resumeRequested);

    ui->treeView->setItemDelegateForColumn(3, new ProgressBarDelegate(ui->treeView));

    ui->treeView->setItemDelegateForColumn(5, interruptWorkerButtonDelegate);

    ui->treeView->setItemDelegateForColumn(6, pauseWorkerButtonDelegate);

    ui->treeView->setItemDelegateForColumn(7, resumeWorkerButtonDelegate);

    ui->treeView->header()->setSectionResizeMode(0, QHeaderView::Stretch);

    ui->treeView->header()->setSectionResizeMode(1, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(2, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(3, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(4, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(5, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(6, QHeaderView::Fixed);

    ui->treeView->header()->setSectionResizeMode(7, QHeaderView::Fixed);

    ui->treeView->header()->resizeSection(1, 125);

    ui->treeView->header()->resizeSection(2, 125);

    ui->treeView->header()->resizeSection(3, 250);

    ui->treeView->header()->resizeSection(4, 125);

    ui->treeView->header()->resizeSection(5, 100);

    ui->treeView->header()->resizeSection(6, 100);

    ui->treeView->header()->resizeSection(7, 100);

    this->connect(progressMonitorModel, &QAbstractItemModel::rowsInserted, [this](const QModelIndex &parent, int first, int last){Q_UNUSED(first); Q_UNUSED(last); if (!ui->treeView->isExpanded(parent)) {ui->treeView->expand(parent);}});

}

ProgressMonitorWidget::~ProgressMonitorWidget()
{

    delete ui;

}
