#ifndef ANNOTATIONSFROMASINGLEFILEWORKER_H
#define ANNOTATIONSFROMASINGLEFILEWORKER_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QUuid>
#include <QRegularExpression>
#include <QApplication>

#include <utility>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class AnnotationsFromASingleFileParameters : public BaseParameters
{

public:

    AnnotationsFromASingleFileParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString delimiter;

    bool firstTokenInHeaderDefinesColumnWithRowIdentifiers;

    qsizetype indexOfColumnDefiningFirstDataColumn;

    qsizetype indexOfColumnDefiningRowIdentifiers;

    qsizetype indexOfColumnDefiningSecondDataColumn;

    qsizetype lineNumberOfFirstDataLine;

    qsizetype lineNumberOfHeader;

    qsizetype lineNumberOfLastDataLine;

    Qt::Orientation orientation;

    QString pathToFile;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class AnnotationsFromASingleFileWorker : public BaseWorker
{

public:

    AnnotationsFromASingleFileWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

#endif // ANNOTATIONSFROMASINGLEFILEWORKER_H
