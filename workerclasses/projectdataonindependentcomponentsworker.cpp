#include "projectdataonindependentcomponentsworker.h"

ProjectDataOnIndependentComponentsParameters::ProjectDataOnIndependentComponentsParameters(QObject *parent) :
    BaseParameters(parent, "projectdataonindependentcomponents")
{

}

bool ProjectDataOnIndependentComponentsParameters::isValid_()
{

    if (selectedVariableIdentifiersData.isEmpty()) {

        d_lastError = "no variables selected in new data";

        return false;

    }

    if (selectedItemIdentifiersData.isEmpty()) {

        d_lastError = "no items selected in new data";

        return false;

    }

    if (selectedVariableIdentifiersIndependentComponents.isEmpty()) {

        d_lastError = "no independent components selected";

        return false;

    }

    if (selectedVariableIdentifiersIndependentComponents.isEmpty()) {

        d_lastError = "no items selected in independent components";

        return false;

    }

    if (selectedItemIdentifiersData.count() != QSet<QString>(selectedItemIdentifiersData.begin(), selectedItemIdentifiersData.end()).count()) {

        d_lastError = "cannot project -> data \"" + data->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (selectedItemIdentifiersIndependentComponents.count() != QSet<QString>(selectedItemIdentifiersIndependentComponents.begin(), selectedItemIdentifiersIndependentComponents.end()).count()) {

        d_lastError = "cannot project -> independent components \"" + data->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString ProjectDataOnIndependentComponentsParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[data with label = \"" + data->property("label").toString() + "\" and uuid = \"" + data->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in data containing variables = " << ((orientationData == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in data = [\"" << selectedVariableIdentifiersData.first() << "\", " << selectedVariableIndexesData.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiersData.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiersData.at(i) << "\", " << selectedVariableIndexesData.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items in data = [\"" << selectedItemIdentifiersData.first() << "\", " << selectedItemIndexesData.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiersData.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiersData.at(i) << "\", " << selectedItemIndexesData.at(i) << "]";

    stream << "]";



    stream << Qt::endl << "\t[independent components with label = \"" + independentComponents->property("label").toString() + "\" and uuid = \"" + independentComponents->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in independent components containing items to match = " << ((orientationIndependentComponents == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in independent components = [\"" << selectedVariableIdentifiersIndependentComponents.first() << "\", " << selectedVariableIndexesIndependentComponents.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiersIndependentComponents.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiersIndependentComponents.at(i) << "\", " << selectedVariableIndexesIndependentComponents.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items in new data = [\"" << selectedItemIdentifiersIndependentComponents.first() << "\", " << selectedItemIndexesIndependentComponents.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiersIndependentComponents.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiersIndependentComponents.at(i) << "\", " << selectedItemIndexesIndependentComponents.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[number of permutations = \"" + QString::number(numberOfPermutations) + "\"]";

    stream << Qt::endl << "\t[block size in permutations = \"" + QString::number(blockSize) + "\"]";

    stream << Qt::endl << "\t[p-value treshold = \"" + QString::number(pValue_threshold) + "\"]";

    stream << Qt::endl << "\t[flip independent components based on skewness = " + (flipICs ? QString("enable") : QString("disable")) + "\"]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

ProjectDataOnIndependentComponentsWorker::ProjectDataOnIndependentComponentsWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("project independent components on new data"), parameters, "projectdataonindependentcomponents")
{

}

void ProjectDataOnIndependentComponentsWorker::doWork_()
{

    QSharedPointer<ProjectDataOnIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<ProjectDataOnIndependentComponentsParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeNameData = parameters->data->typeName();

    const char *typeNameIndependentComponents = parameters->data->typeName();

    if (*typeNameData != *typeNameIndependentComponents) {

        this->reportError("for projecting independent components on new data, new data and indepependent components need to be the same numerical type");

        QThread::currentThread()->requestInterruption();

    }

    if (*typeNameData == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->independentComponents));
    else if (*typeNameData == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->data), qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->independentComponents));
    else {

        this->reportError("for projecting independent components on  data, data and indepependent components need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool ProjectDataOnIndependentComponentsWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<ProjectDataOnIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<ProjectDataOnIndependentComponentsParameters>());

    parameters->data->lockForRead();

    parameters->independentComponents->lockForRead();

    return true;

}

void ProjectDataOnIndependentComponentsWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<ProjectDataOnIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<ProjectDataOnIndependentComponentsParameters>());

    parameters->data->unlock();

    parameters->independentComponents->unlock();

}

void ProjectDataOnIndependentComponentsWorker::apply_permutation(std::span<double> &span, QList<unsigned long> indices) const
{

    for (unsigned long i = 0; i < span.size(); i++) {

        auto current = i;

        while (i != indices[current]) {

            auto next = indices[current];

            std::swap(span[current], span[next]);

            indices[current] = current;

            current = next;
        }

        indices[current] = current;

    }

}
