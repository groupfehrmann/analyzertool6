#ifndef TRANSFORMDATAUSINGQUANTILENORMALIZATIONDIALOG_H
#define TRANSFORMDATAUSINGQUANTILENORMALIZATIONDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/spinboxwidget.h"
#include "workerclasses/transformdatausingquantilenormalizationworker.h"

class TransformDataUsingQuantileNormalizationDialog : public BaseDialog
{

public:

    TransformDataUsingQuantileNormalizationDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void comboBoxWidget_descriptiveFunctionCurrentTextChanged(const QString &text);

};

#endif // TRANSFORMDATAUSINGQUANTILENORMALIZATIONDIALOG_H
