#include "annotatedmatrixwidget.h"
#include "ui_annotatedmatrixwidget.h"

AnnotatedMatrixWidget::AnnotatedMatrixWidget(QWidget *parent, const QSharedPointer<QObject> &object) :
    QWidget(parent),
    ui(new Ui::AnnotatedMatrixWidget),
    d_object(object)
{

    ui->setupUi(this);

    AnnotatedMatrixDataSortFilterProxyModel *annotatedMatrixDataSortFilterProxyModel = new AnnotatedMatrixDataSortFilterProxyModel(ui->tableView_data);

    annotatedMatrixDataSortFilterProxyModel->setDynamicSortFilter(false);

    annotatedMatrixDataSortFilterProxyModel->setSourceModel(new AnnotatedMatrixDataModel(ui->tableView_data, qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)));

    ui->tableView_data->setModel(annotatedMatrixDataSortFilterProxyModel);


    ui->tableView_data->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_data->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(ui->tableView_data->horizontalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_data->verticalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_data, &QTableView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_data);

    this->connect(ui->tableView_data->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_data_horizontalHeader);

    this->connect(ui->tableView_data->verticalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_data_verticalHeader);

    if(auto button = ui->tableView_data->findChild<QAbstractButton*>(QString(), Qt::FindDirectChildrenOnly)) {

        disconnect(button, Q_NULLPTR, ui->tableView_data, Q_NULLPTR);

        this->connect(button, &QAbstractButton::clicked, [this]() {

            QMenu menu;

            QSpinBox *spinBox = new QSpinBox;

            spinBox->setRange(19, 1000);

            spinBox->setValue(ui->tableView_data->verticalHeader()->width());

            spinBox->setPrefix("resize vertical header : ");

            this->connect(spinBox, &QSpinBox::valueChanged, [this](int value){ui->tableView_data->verticalHeader()->setFixedWidth(value); QMetaObject::invokeMethod(ui->tableView_data, "updateGeometries");});

            QWidgetAction *widgetAction = new QWidgetAction(&menu);

            widgetAction->setDefaultWidget(spinBox);

            menu.addActions({widgetAction});

            menu.exec(QCursor::pos());

        ;});

    }


    AnnotatedMatrixAnnotationsSortFilterProxyModel *annotatedMatrixColumnAnnotationsSortFilterProxyModel = new AnnotatedMatrixAnnotationsSortFilterProxyModel(ui->tableView_columnAnnotations);

    annotatedMatrixColumnAnnotationsSortFilterProxyModel->setDynamicSortFilter(false);

    annotatedMatrixColumnAnnotationsSortFilterProxyModel->setSourceModel(new AnnotatedMatrixAnnotationsModel(ui->tableView_columnAnnotations, qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object), Qt::Horizontal));

    ui->tableView_columnAnnotations->setModel(annotatedMatrixColumnAnnotationsSortFilterProxyModel);

    ui->tableView_columnAnnotations->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_columnAnnotations->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(ui->tableView_columnAnnotations->horizontalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_columnAnnotations->verticalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_columnAnnotations, &QTableView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations);

    this->connect(ui->tableView_columnAnnotations->verticalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations_verticalHeader);

    this->connect(ui->tableView_columnAnnotations->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations_horizontalHeader);

    if(auto button = ui->tableView_columnAnnotations->findChild<QAbstractButton*>(QString(), Qt::FindDirectChildrenOnly)) {

        disconnect(button, Q_NULLPTR, ui->tableView_columnAnnotations, Q_NULLPTR);

        this->connect(button, &QAbstractButton::clicked, [this]() {

            QMenu menu;

            QSpinBox *spinBox = new QSpinBox;

            spinBox->setRange(19, 1000);

            spinBox->setValue(ui->tableView_columnAnnotations->verticalHeader()->width());

            spinBox->setPrefix("resize vertical header : ");

            this->connect(spinBox, &QSpinBox::valueChanged, [this](int value){ui->tableView_columnAnnotations->verticalHeader()->setFixedWidth(value); QMetaObject::invokeMethod(ui->tableView_columnAnnotations, "updateGeometries");});

            QWidgetAction *widgetAction = new QWidgetAction(&menu);

            widgetAction->setDefaultWidget(spinBox);

            menu.addActions({widgetAction});

            menu.exec(QCursor::pos());

        ;});

    }

    AnnotatedMatrixAnnotationsSortFilterProxyModel *annotatedMatrixRowAnnotationsSortFilterProxyModel = new AnnotatedMatrixAnnotationsSortFilterProxyModel(ui->tableView_rowAnnotations);

    annotatedMatrixRowAnnotationsSortFilterProxyModel->setDynamicSortFilter(false);

    annotatedMatrixRowAnnotationsSortFilterProxyModel->setSourceModel(new AnnotatedMatrixAnnotationsModel(ui->tableView_rowAnnotations, qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object), Qt::Vertical));

    ui->tableView_rowAnnotations->setModel(annotatedMatrixRowAnnotationsSortFilterProxyModel);

    ui->tableView_rowAnnotations->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    ui->tableView_rowAnnotations->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(ui->tableView_rowAnnotations->horizontalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_rowAnnotations->verticalHeader(), &QHeaderView::sectionDoubleClicked, this, &AnnotatedMatrixWidget::showLineEditForHeader);

    this->connect(ui->tableView_rowAnnotations, &QTableView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations);

    this->connect(ui->tableView_rowAnnotations->verticalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations_verticalHeader);

    this->connect(ui->tableView_rowAnnotations->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations_horizontalHeader);

    if(auto button = ui->tableView_rowAnnotations->findChild<QAbstractButton*>(QString(), Qt::FindDirectChildrenOnly)) {

        disconnect(button, Q_NULLPTR, ui->tableView_rowAnnotations, Q_NULLPTR);

        this->connect(button, &QAbstractButton::clicked, [this]() {

            QMenu menu;

            QSpinBox *spinBox = new QSpinBox;

            spinBox->setRange(19, 1000);

            spinBox->setValue(ui->tableView_rowAnnotations->verticalHeader()->width());

            spinBox->setPrefix("resize vertical header : ");

            this->connect(spinBox, &QSpinBox::valueChanged, [this](int value){ui->tableView_rowAnnotations->verticalHeader()->setFixedWidth(value); QMetaObject::invokeMethod(ui->tableView_rowAnnotations, "updateGeometries");});

            QWidgetAction *widgetAction = new QWidgetAction(&menu);

            widgetAction->setDefaultWidget(spinBox);

            menu.addActions({widgetAction});

            menu.exec(QCursor::pos());

        ;});

    }


    this->connect(ui->tableView_data->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    this->connect(ui->tableView_data->horizontalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    this->connect(ui->tableView_rowAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    this->connect(ui->tableView_columnAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

}

void AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations_horizontalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isColumnSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_columnAnnotations->selectColumn(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedColumns())
        selectedSections << modelIndex.column();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertColumns(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeColumns(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations_verticalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isRowSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_columnAnnotations->selectRow(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedRows())
        selectedSections << modelIndex.row();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertRows(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeRows(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_data(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    if (static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(static_cast<QTableView *>(QObject::sender())->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(ui->tableView_data->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_columnAnnotations(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(static_cast<QTableView *>(QObject::sender())->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(ui->tableView_columnAnnotations->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(static_cast<QTableView *>(QObject::sender())->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(ui->tableView_rowAnnotations->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_data_horizontalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isColumnSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_data->selectColumn(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedColumns())
        selectedSections << modelIndex.column();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertColumns(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeColumns(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_data_verticalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isRowSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_data->selectRow(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedRows())
        selectedSections << modelIndex.row();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertRows(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeRows(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations_horizontalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isColumnSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_rowAnnotations->selectColumn(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedColumns())
        selectedSections << modelIndex.column();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertColumns(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeColumns(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Horizontal, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showContextMenu_tableView_rowAnnotations_verticalHeader(const QPoint &pos)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = static_cast<QHeaderView *>(QObject::sender());

    int index = headerView->logicalIndexAt(pos);

    if (index < 0)
        return;

    if (!headerView->selectionModel()->isRowSelected(index, QModelIndex())) {

        headerView->selectionModel()->clear();

        ui->tableView_rowAnnotations->selectRow(index);

    }

    QList<int> selectedSections;

    for (const QModelIndex &modelIndex : headerView->selectionModel()->selectedRows())
        selectedSections << modelIndex.row();

    if (selectedSections.isEmpty())
        selectedSections << index;

    QMenu *menu = new QMenu(this);

    this->connect(menu, &QMenu::triggered, menu, &QMenu::deleteLater);

    menu->addAction("insert", [selectedSections, headerView](){headerView->model()->insertRows(selectedSections.first(), selectedSections.count());});

    menu->addAction("remove", [selectedSections, headerView](){headerView->model()->removeRows(selectedSections.first(), selectedSections.count());});

    menu->addSeparator();

    menu->addAction("select", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Checked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addAction("deselect", [this, selectedSections, headerView](){for (int selectedSection : selectedSections) {headerView->model()->setHeaderData(selectedSection, Qt::Vertical, Qt::Unchecked, Qt::CheckStateRole);} this->invalidateSortFilterModel();});

    menu->addSeparator();

    if (static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(headerView->model())->showSelectedOnly())
        menu->addAction("show all", [this](){this->setShowSelectedOnlyForAllModels(false);});
    else
        menu->addAction("show selected only", [this](){this->setShowSelectedOnlyForAllModels(true);});

    menu->popup(headerView->viewport()->mapToGlobal(pos));

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::showLineEditForHeader(int logicalIndex)
{

    if (!qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->tryLockForWrite())
        return;

    QHeaderView *headerView = qobject_cast<QHeaderView *>(this->sender());

    QLineEdit *lineEdit = new QLineEdit(headerView->viewport());

    QAbstractItemModel *model = headerView->model();

    if (headerView->orientation() == Qt::Horizontal) {

        lineEdit->move(headerView->sectionViewportPosition(logicalIndex), 0);

        lineEdit->resize(headerView->sectionSize(logicalIndex), headerView->height());

    } else {

        lineEdit->move(0, headerView->sectionViewportPosition(logicalIndex));

        lineEdit->resize(headerView->width(), headerView->sectionSize(logicalIndex));

    }

    this->connect(lineEdit, &QLineEdit::editingFinished, [model, logicalIndex, headerView, lineEdit](){model->setHeaderData(logicalIndex, headerView->orientation(), lineEdit->text()); lineEdit->deleteLater();});

    lineEdit->setText(headerView->model()->headerData(logicalIndex, headerView->orientation()).toString());

    lineEdit->setFocus();

    lineEdit->show();

    qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_object)->unlock();

}

void AnnotatedMatrixWidget::synchronizeScrollBars(int value)
{

    QScrollBar *scrollBar = static_cast<QScrollBar *>(QObject::sender());

    double fraction = static_cast<double>(value) / static_cast<double>(scrollBar->maximum());

    if (scrollBar == ui->tableView_data->verticalScrollBar()) {

        this->disconnect(ui->tableView_rowAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

        ui->tableView_rowAnnotations->verticalScrollBar()->setValue(ui->tableView_rowAnnotations->verticalScrollBar()->maximum() * fraction);

        this->connect(ui->tableView_rowAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    } else if (scrollBar == ui->tableView_data->horizontalScrollBar()) {

        this->disconnect(ui->tableView_columnAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

        ui->tableView_columnAnnotations->verticalScrollBar()->setValue(ui->tableView_columnAnnotations->verticalScrollBar()->maximum() * fraction);

        this->connect(ui->tableView_columnAnnotations->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    } else if (scrollBar == ui->tableView_rowAnnotations->verticalScrollBar()) {

        this->disconnect(ui->tableView_data->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

        ui->tableView_data->verticalScrollBar()->setValue(ui->tableView_data->verticalScrollBar()->maximum() * fraction);

        this->connect(ui->tableView_data->verticalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    } else if (scrollBar == ui->tableView_columnAnnotations->verticalScrollBar()) {

        this->disconnect(ui->tableView_data->horizontalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

        ui->tableView_data->horizontalScrollBar()->setValue(ui->tableView_data->horizontalScrollBar()->maximum() * fraction);

        this->connect(ui->tableView_data->horizontalScrollBar(), &QScrollBar::valueChanged, this, &AnnotatedMatrixWidget::synchronizeScrollBars);

    }

}

void AnnotatedMatrixWidget::setShowSelectedOnlyForAllModels(bool showSelectedOnly)
{

    if (static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(ui->tableView_data->model())->showSelectedOnly() == showSelectedOnly)
        return;

    static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(ui->tableView_data->model())->setShowSelectedOnly(showSelectedOnly);

    static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->setShowSelectedOnly(showSelectedOnly);

    static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->setShowSelectedOnly(showSelectedOnly);

}

void AnnotatedMatrixWidget::invalidateSortFilterModel()
{

    ui->tableView_data->selectionModel()->clear();

    static_cast<AnnotatedMatrixDataSortFilterProxyModel *>(ui->tableView_data->model())->invalidate();

    ui->tableView_rowAnnotations->selectionModel()->clear();

    static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(ui->tableView_rowAnnotations->model())->invalidate();

    ui->tableView_columnAnnotations->selectionModel()->clear();

    static_cast<AnnotatedMatrixAnnotationsSortFilterProxyModel *>(ui->tableView_columnAnnotations->model())->invalidate();

}

AnnotatedMatrixWidget::~AnnotatedMatrixWidget()
{

    delete ui;

}
