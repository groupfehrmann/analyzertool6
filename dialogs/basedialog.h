#ifndef BASEDIALOG_H
#define BASEDIALOG_H

#include <QDialog>
#include <QString>
#include <QGridLayout>
#include <QApplication>
#include <QSettings>

#include "workerclasses/baseparameters.h"

namespace Ui {

    class BaseDialog;

}

class BaseDialog : public QDialog
{

    Q_OBJECT

public:

    explicit BaseDialog(QWidget *parent = nullptr, const QString &dialogTitle = QString(), const QString &dialogSettingsIdentifier = QString());

    const QString &dialogSettingsIdentifier() const;

    const QSharedPointer<BaseParameters> &parameters() const;

    ~BaseDialog();

protected:

    Ui::BaseDialog *ui;

    QGridLayout *d_gridLayout;

    QString d_dialogSettingsIdentifier;

    QSharedPointer<BaseParameters> d_parameters;

    void initWidgets();

    virtual void initWidgets_();

};

#endif // BASEDIALOG_H
