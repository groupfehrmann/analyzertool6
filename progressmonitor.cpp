#include "progressmonitor.h"

QString ProgressMonitor::secondsToTimeString(qint64 s)
{
    qint64 days = s / 86400;

    qint64 seconds = s - days * 86400;

    qint64 hours = seconds / 3600;

    seconds -= hours * 3600;

    qint64 minutes = seconds / 60;

    seconds -= minutes * 60;

    QString str;

    if (days)
        str += QString("%1d ").arg(days, 1);

    if (hours >= 1)
        str += QString("%1h ").arg(hours, 2);

    if (minutes >= 1)
        str += QString("%1m ").arg(minutes, 2);

    if (seconds >= 1)
        str += QString("%1s ").arg(seconds, 2);

    if (str.isEmpty())
        str = "< 1s";

    return str;

}

QString ProgressMonitor::milliSecondsToTimeString(qint64 s)
{
    s /= 1000;

    qint64 days = s / 86400;

    qint64 seconds = s - days * 86400;

    qint64 hours = seconds / 3600;

    seconds -= hours * 3600;

    qint64 minutes = seconds / 60;

    seconds -= minutes * 60;

    QString str;

    if (days)
        str += QString("%1d ").arg(days, 1);

    if (hours >= 1)
        str += QString("%1h ").arg(hours, 2);

    if (minutes >= 1)
        str += QString("%1m ").arg(minutes, 2);

    if (seconds >= 1)
        str += QString("%1s ").arg(seconds, 2);

    if (str.isEmpty())
        str = "< 1s";

    return str;

}

ProgressMonitor::ProgressMonitor(QObject *parent) :
    QObject(parent),
    d_data(new ProgressMonitorData)
{

    this->startTimer(500);

}

qsizetype ProgressMonitor::indexOfUuidProgress(const QUuid& uuidProgress) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QUuid uuidWorker = d_data->d_uuidProgressToProgressInfo.value(uuidProgress).d_uuidWorker;

    return d_data->d_uuidWorkerToWorkerInfo.value(uuidWorker).d_uuidsProgress.indexOf(uuidProgress);

}

qsizetype ProgressMonitor::indexOfUuidWorker(const QUuid& uuidWorker) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidsWorker.indexOf(uuidWorker);

}

qsizetype ProgressMonitor::indexOfUuidWorkerOfWhichUuidProgressIsMember(const QUuid& uuidProgress) const
{

    return this->indexOfUuidWorker(this->uuidWorker(uuidProgress));

}

bool ProgressMonitor::isUuidWorker(const QUuid& uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidsWorker.contains(uuid);

}

bool ProgressMonitor::isUuidProgress(const QUuid& uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidProgressToProgressInfo.contains(uuid);

}

const ProgressMonitorData::ProgressInfo &ProgressMonitor::progressInfo(const QUuid& uuidProgress) const
{

    QReadLocker readLocker(&d_readWriteLock);

    const QHash<QUuid, ProgressMonitorData::ProgressInfo>::const_iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

    if (it == d_data->d_uuidProgressToProgressInfo.end())
        return d_data->d_emptyProgressInfo;
    else
        return it.value();

}

const QUuid &ProgressMonitor::uuidWorker(const QUuid& uuidProgress) const
{

    QReadLocker readLocker(&d_readWriteLock);

    const QHash<QUuid, ProgressMonitorData::ProgressInfo>::const_iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

    if (it == d_data->d_uuidProgressToProgressInfo.end())
        return d_data->d_emptyUuid;
    else
        return it->d_uuidWorker;


}


const QList<QUuid> &ProgressMonitor::uuidsWorker() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidsWorker;

}

const QList<QUuid> &ProgressMonitor::uuidsProgress(const QUuid& uuidWorker) const
{

    QReadLocker readLocker(&d_readWriteLock);

    const QHash<QUuid, ProgressMonitorData::WorkerInfo>::const_iterator it = d_data->d_uuidWorkerToWorkerInfo.find(uuidWorker);

    if (it == d_data->d_uuidWorkerToWorkerInfo.end())
        return d_data->d_emptyListOfQUuid;
    else
        return it->d_uuidsProgress;

}

const ProgressMonitorData::WorkerInfo &ProgressMonitor::workerInfo(const QUuid& uuidWorker) const
{

    QReadLocker readLocker(&d_readWriteLock);

    const QHash<QUuid, ProgressMonitorData::WorkerInfo>::const_iterator it = d_data->d_uuidWorkerToWorkerInfo.find(uuidWorker);

    if (it == d_data->d_uuidWorkerToWorkerInfo.end())
        return d_data->d_emptyWorkerInfo;
    else
        return it.value();

}

ProgressMonitor::~ProgressMonitor()
{

}

void ProgressMonitor::putWorkerInQueu(const QUuid &uuidWorker, const QString &workerLabel, int delayInSeconds)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidWorkerToWorkerInfo.insert(uuidWorker, {workerLabel, QDateTime(), QList<QUuid>(), false, false, delayInSeconds});

    d_data->d_uuidsWorker.append(uuidWorker);

}

void ProgressMonitor::startMonitoringWorker(const QUuid &uuidWorker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_uuidWorkerToWorkerInfo[uuidWorker].d_startTime = QDateTime::currentDateTime();

}

void ProgressMonitor::startProgress(const QUuid &uuidWorker, const QUuid &uuidProgress, const QString &label, qsizetype minimum, qsizetype maximum)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    ProgressMonitorData::ProgressInfo progressInfo;

    progressInfo.d_label = label;

    progressInfo.d_maximum = maximum;

    progressInfo.d_minimum = minimum;

    progressInfo.d_startTime = QDateTime::currentDateTime();

    progressInfo.d_value = minimum;

    progressInfo.d_etaInSeconds = -1;

    progressInfo.d_uuidWorker = uuidWorker;

    progressInfo.d_interrupted = false;

    progressInfo.d_paused = false;

    progressInfo.d_lastupdateValue = minimum;

    progressInfo.d_lastUpdateTime = progressInfo.d_startTime;

    progressInfo.d_provideETA = true;

    progressInfo.d_timePerStepMovingAverage = MovingAverage(10);

    d_data->d_uuidProgressToProgressInfo.insert(uuidProgress, progressInfo);

    d_data->d_uuidWorkerToWorkerInfo[uuidWorker].d_uuidsProgress.append(uuidProgress);

}

void ProgressMonitor::stopAllMonitoringAssociatedWithUuid(const QUuid &uuid)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (d_data->d_uuidWorkerToWorkerInfo.contains(uuid))
        emit this->messageReported(uuid, "worker \"" + d_data->d_uuidWorkerToWorkerInfo.value(uuid).d_label + "\" has stopped, total time elapsed " + this->secondsToTimeString(d_data->d_uuidWorkerToWorkerInfo.value(uuid).d_startTime.secsTo(QDateTime::currentDateTime())));

    if (d_data->d_uuidProgressToProgressInfo.contains(uuid))
        d_data->d_uuidWorkerToWorkerInfo[d_data->d_uuidProgressToProgressInfo.value(uuid).d_uuidWorker].d_uuidsProgress.removeOne(uuid);

    d_data->d_uuidProgressToProgressInfo.remove(uuid);

    d_data->d_uuidWorkerToWorkerInfo.remove(uuid);

    d_data->d_uuidsWorker.removeOne(uuid);

}

void ProgressMonitor::updateProgress(const QUuid &uuidProgress, qsizetype value)
{

    QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

    if (it != d_data->d_uuidProgressToProgressInfo.end())
        it->d_value = value;

}

void ProgressMonitor::updateProgressLabel(const QUuid &uuidProgress, const QString &label)
{

    QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

    if (it != d_data->d_uuidProgressToProgressInfo.end())
        it->d_label = label;

}

void ProgressMonitor::updateProgressWithOne(const QUuid &uuidProgress)
{

    QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

    if (it != d_data->d_uuidProgressToProgressInfo.end())
        it->d_value += static_cast<qsizetype>(1);

}

void ProgressMonitor::workerIsInterruption(const QUuid &uuidWorker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QHash<QUuid, ProgressMonitorData::WorkerInfo>::iterator it = d_data->d_uuidWorkerToWorkerInfo.find(uuidWorker);

    if (it == d_data->d_uuidWorkerToWorkerInfo.end())
        return;

    it->d_interrupted = true;

    for (QUuid &uuidProgress : it->d_uuidsProgress) {

        QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

        if (it != d_data->d_uuidProgressToProgressInfo.end())
            it->d_interrupted = true;

    }

}

void ProgressMonitor::workerIsPaused(const QUuid &uuidWorker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QHash<QUuid, ProgressMonitorData::WorkerInfo>::iterator it = d_data->d_uuidWorkerToWorkerInfo.find(uuidWorker);

    if (it == d_data->d_uuidWorkerToWorkerInfo.end())
        return;

    it->d_paused = true;

    for (QUuid &uuidProgress : it->d_uuidsProgress) {

        QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

        if (it != d_data->d_uuidProgressToProgressInfo.end())
            it->d_paused = true;

    }

}

void ProgressMonitor::workerIsResumed(const QUuid &uuidWorker)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QHash<QUuid, ProgressMonitorData::WorkerInfo>::iterator it = d_data->d_uuidWorkerToWorkerInfo.find(uuidWorker);

    if (it == d_data->d_uuidWorkerToWorkerInfo.end())
        return;

    it->d_paused = false;

    for (QUuid &uuidProgress : it->d_uuidsProgress) {

        QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it = d_data->d_uuidProgressToProgressInfo.find(uuidProgress);

        if (it != d_data->d_uuidProgressToProgressInfo.end())
            it->d_paused = false;

    }

}

void ProgressMonitor::timerEvent(QTimerEvent *event)
{

    Q_UNUSED(event)

    QWriteLocker writeLocker(&d_readWriteLock);

    QHash<QUuid, ProgressMonitorData::ProgressInfo>::iterator it;

    for (it = d_data->d_uuidProgressToProgressInfo.begin(); it != d_data->d_uuidProgressToProgressInfo.end(); ++it) {

        ProgressMonitorData::ProgressInfo &progressInfo(it.value());

        if ((progressInfo.d_startTime.msecsTo(QDateTime::currentDateTime()) > 5000) && (progressInfo.d_minimum != progressInfo.d_maximum) && (progressInfo.d_value > progressInfo.d_minimum) && progressInfo.d_provideETA) {

            qsizetype differenceInValue = progressInfo.d_value - progressInfo.d_lastupdateValue;

            qsizetype differenceInTime = progressInfo.d_lastUpdateTime.msecsTo(QDateTime::currentDateTime());

            if (!progressInfo.d_paused) {

                if (differenceInValue > 0) {

                    progressInfo.d_lastUpdateTime = QDateTime::currentDateTime();

                    progressInfo.d_lastupdateValue = progressInfo.d_value;

                    progressInfo.d_timePerStepMovingAverage.push((static_cast<double>(differenceInTime) / static_cast<double>(differenceInValue)));

                    progressInfo.d_etaInSeconds = progressInfo.d_timePerStepMovingAverage.mean() * static_cast<double>(progressInfo.d_maximum - progressInfo.d_value);

                } else if (differenceInValue < 0)
                    progressInfo.d_provideETA = false;

            }

        } else
            progressInfo.d_etaInSeconds = -1;

    }

}
