#include "kolmogorovsmirnovtest.h"

qsizetype KolmogorovSmirnovTest::d_minimumNumberOfSamples = 2;

qsizetype KolmogorovSmirnovTest::d_maximumNumberOfSamples = 2;

qsizetype KolmogorovSmirnovTest::d_minimumNumberOfItemsPerSample = 2;

bool KolmogorovSmirnovTest::d_effectSizeAvailable = false;

QString KolmogorovSmirnovTest::d_effectSizeLabel = QString();

QList<QString> KolmogorovSmirnovTest::d_testDescriptiveLabels = {"D", "Kolmogorov-Smirnov Z", "Sig. (2-tailed)"};

std::function<double (const double &)> KolmogorovSmirnovTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

KolmogorovSmirnovTest::KolmogorovSmirnovTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

KolmogorovSmirnovTest::KolmogorovSmirnovTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_samples.resize(2);

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1)
            d_samples[codedSampleIdentifier].append(data.at(i));

    }

    d_sampleSize = d_samples.at(0).count() + d_samples.at(1).count();

}

void KolmogorovSmirnovTest::calculateStatistic()
{

    QList<double> &sample0(d_samples[0]);

    QList<double> &sample1(d_samples[1]);

    std::sort(sample0.begin(), sample0.end());

    std::sort(sample1.begin(), sample1.end());

    int j1 = 0;

    int j2 = 0;

    d_d = 0.0;

    double d1;

    double d2;

    double dt;

    double fn1 = 0.0;

    double fn2 = 0.0;

    double en1 = d_samples.at(0).count();

    double en2 = sample1.size();

    while (j1 < en1 && j2 < en2) {

        if ((d1 = sample0.at(j1)) <= (d2 = sample1.at(j2)))
            fn1 = ++j1 / en1;

        if (d2 <= d1)
            fn2 = ++j2 / en2;

        if ((dt = std::fabs(fn2 - fn1)) > d_d)
            d_d = dt;
    }

    d_statistic = std::sqrt(en1 * en2 / (en1 + en2)) * d_d;

}

void KolmogorovSmirnovTest::calculatePValue()
{

    this->calculateStatistic();

    d_pValue = 1.0;

    const double EPS1 = 1.0e-6;

    const double EPS2 = 1.0e-16;

    double fac = 2.0;

    double sum = 0.0;

    double termbf = 0.0;

    double a2 = -2.0 * d_d * d_d;

    for (int j = 1; j <= 100; ++j) {

        double term = fac * std::exp(a2 * j * j);

        sum += term;

        if (std::fabs(term) <= EPS1 * termbf || std::fabs(term) <= EPS2 * sum) {

            d_pValue = sum;

            break;

        }

        fac = -fac;

        termbf = std::fabs(term);

    }

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void KolmogorovSmirnovTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_d << d_statistic << d_pValue;

}
