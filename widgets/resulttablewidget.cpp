#include "resulttablewidget.h"
#include "ui_resulttablewidget.h"

ResultTableWidget::ResultTableWidget(QWidget *parent, const QSharedPointer<QObject> &object) :
    QWidget(parent),
    ui(new Ui::ResultTableWidget),
    d_object(object)
{

    ui->setupUi(this);

    AnnotatedMatrixDataSortFilterProxyModel *annotatedMatrixDataSortFilterProxyModel = new AnnotatedMatrixDataSortFilterProxyModel(ui->tableView);

    annotatedMatrixDataSortFilterProxyModel->setSourceModel(new AnnotatedMatrixDataModel(ui->tableView, qSharedPointerDynamicCast<BaseAnnotatedMatrix>(qSharedPointerDynamicCast<Result>(d_object)->internalObject())));

    ui->tableView->setModel(annotatedMatrixDataSortFilterProxyModel);

    if(auto button = ui->tableView->findChild<QAbstractButton*>(QString(), Qt::FindDirectChildrenOnly)) {

        button->setToolTip("Reset sorting");

        disconnect(button, Q_NULLPTR, ui->tableView, Q_NULLPTR);

        this->connect(button, &QAbstractButton::clicked, [this, annotatedMatrixDataSortFilterProxyModel]() {

            QMenu menu;

            menu.addAction("reset sorting", [annotatedMatrixDataSortFilterProxyModel]() {annotatedMatrixDataSortFilterProxyModel->sort(-1);});

            QSpinBox *spinBox = new QSpinBox;

            spinBox->setRange(19, ui->tableView->verticalHeader()->width());

            spinBox->setValue(ui->tableView->verticalHeader()->width());

            spinBox->setPrefix("resize vertical header : ");

            this->connect(spinBox, &QSpinBox::valueChanged, [this](int value){ui->tableView->verticalHeader()->setFixedWidth(value); QMetaObject::invokeMethod(ui->tableView, "updateGeometries");});

            QWidgetAction *widgetAction = new QWidgetAction(&menu);

            widgetAction->setDefaultWidget(spinBox);

            menu.addSeparator();

            menu.addActions({widgetAction});

            menu.exec(QCursor::pos());

            ;});

    }

}

ResultTableWidget::~ResultTableWidget()
{
    delete ui;
}
