#ifndef COMBOBOXWIDGET_H
#define COMBOBOXWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QComboBox>

#include "dialogs/basedialog.h"

namespace Ui {

    class ComboBoxWidget;

}

class ComboBoxWidget : public QWidget
{

    Q_OBJECT

public:

    explicit ComboBoxWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QComboBox *comboBox();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~ComboBoxWidget();

private:

    Ui::ComboBoxWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

};

#endif // COMBOBOXWIDGET_H
