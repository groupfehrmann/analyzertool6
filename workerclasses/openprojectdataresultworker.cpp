#include "openprojectdataresultworker.h"

OpenProjectDataResultParameters::OpenProjectDataResultParameters(QObject *parent) :
    BaseParameters(parent, "openProjectDataResult")
{

}

bool OpenProjectDataResultParameters::isValid_()
{

    if (!QFile::exists(pathToFile)) {

        d_lastError = "could not find file \"" + pathToFile + "\"";

        return false;

    }

    return true;

}

QString OpenProjectDataResultParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[path to file = \"" << pathToFile << "\"]";

    return prettyPrintString;

}

OpenProjectDataResultWorker::OpenProjectDataResultWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("open project/data/result"), parameters, "openProjectDataResult")
{

}

void OpenProjectDataResultWorker::doWork_()
{

    QSharedPointer<OpenProjectDataResultParameters> parameters(d_data->d_parameters.dynamicCast<OpenProjectDataResultParameters>());

    QFile *fileIn = new QFile(parameters->pathToFile);

    if (!fileIn->exists() || !fileIn->open(QIODevice::ReadOnly)) {

        this->reportError("could not open file \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    QUuid uuidProgress = this->startProgress("importing file \"" + parameters->pathToFile + "\"" , 0, fileIn->size());


    QTimer *timer = new QTimer();

    QThread *timerThread = new QThread();

    timer->setInterval(100);

    this->connect(timerThread, SIGNAL(started()), timer, SLOT(start()));

    this->connect(timerThread, &QThread::finished, timer, &QTimer::stop);

    this->connect(timerThread, &QThread::finished, timer, &QThread::deleteLater);

    this->connect(timerThread, &QThread::finished, timerThread, &QThread::deleteLater);

    this->connect(timerThread, &QThread::finished, fileIn, &QFile::deleteLater);

    timer->moveToThread(timerThread);

    this->connect(timer, &QTimer::timeout, [this, uuidProgress, fileIn](){this->updateProgress(uuidProgress, fileIn->pos());});

    timerThread->start();


    QDataStream in(fileIn);

    in.setStatus(QDataStream::Ok);

    QString label;

    in >> label;

    if (label != QString("_ATool_begin#")) {

        this->reportError("file \"" + parameters->pathToFile + "\" is not a valid ATool file");

        QThread::currentThread()->requestInterruption();

        timerThread->quit();

        return;

    }

    qint32 version;

    in >> version;

    bool openingProject;

    in >> openingProject;

    QString projectLabel;

    in >> projectLabel;

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("annotated matrix")) {

            char *typeName;

            in >> typeName;

            BaseAnnotatedMatrix *baseAnnotatedMatrix;

            if (*typeName == *typeid(short).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<short>;
            else if (*typeName == *typeid(unsigned short).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<unsigned short>;
            else if (*typeName == *typeid(int).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<int>;
            else if (*typeName == *typeid(unsigned int).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<unsigned int>;
            else if (*typeName == *typeid(long long).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<long long>;
            else if (*typeName == *typeid(unsigned long long).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<unsigned long long>;
            else if (*typeName == *typeid(float).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<float>;
            else if (*typeName == *typeid(double).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<double>;
            else if (*typeName == *typeid(QString).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<QString>;
            else if (*typeName == *typeid(unsigned char).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<unsigned char>;
            else if (*typeName == *typeid(bool).name())
                baseAnnotatedMatrix = new AnnotatedMatrix<bool>;
            else
                baseAnnotatedMatrix = nullptr;

            in >> *baseAnnotatedMatrix;

            delete typeName;

            if (in.status() == QDataStream::ReadCorruptData) {

                this->reportError("file \"" + parameters->pathToFile + "\" is not a valid ATool file or may be corrupt");

                QThread::currentThread()->requestInterruption();

                timerThread->quit();

                return;

            }

            if ((baseAnnotatedMatrix->rowCount() * baseAnnotatedMatrix->columnCount()) != baseAnnotatedMatrix->valueCount()) {

                this->reportError("file \"" + parameters->pathToFile + "\" is corrupt, row count " + QString::number(baseAnnotatedMatrix->rowCount()) + " and column count " + QString::number(baseAnnotatedMatrix->columnCount()) + " is not compatible with value count " + QString::number(baseAnnotatedMatrix->valueCount()));

                QThread::currentThread()->requestInterruption();

                timerThread->quit();

                return;

            }

            this->reportMessage("detected " + baseAnnotatedMatrix->property("objectType").toString() + " with label \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\" in file \"" + parameters->pathToFile + "\"");

            baseAnnotatedMatrix->moveToThread(QApplication::instance()->thread());

            if (openingProject)
                emit this->objectAvailable(baseAnnotatedMatrix->property("uuidProject").toUuid(), baseAnnotatedMatrix);
            else {

                emit this->objectAvailable(QUuid(), baseAnnotatedMatrix);

                emit this->addToRecentlyOpenedFiles("data", baseAnnotatedMatrix->property("label").toString(), parameters->pathToFile);

            }

        } else if (label == QString("_ATool_end#")) {

            if (openingProject)
                emit this->addToRecentlyOpenedFiles("project", projectLabel, parameters->pathToFile);

            timerThread->quit();

            return;

        }

    }

    this->reportError("file \"" + parameters->pathToFile + "\" is not a valid ATool file or may be corrupt");

}

bool OpenProjectDataResultWorker::performReadWriteLockLogistics_()
{

    return true;

}

void OpenProjectDataResultWorker::performReadWriteUnlockLogistics_()
{

}
