#ifndef BASEMETAANALYSISAPPROACH_H
#define BASEMETAANALYSISAPPROACH_H

#include <QList>

#include <limits>

#include "boost/math/distributions/normal.hpp"

class BaseMetaAnalysisApproach
{

public:

    BaseMetaAnalysisApproach(const QList<QString> &testDescriptiveLabels, const std::function<double (const double &)> &backtransformEffectSizeFunction);

    double zTransformedPValue();

    const double &pValue();

    const double &statistic();

    const QList<double> &testDescriptiveValues();

    inline const QList<QString> &testDescriptiveLabels() {return d_testDescriptiveLabels;}

    inline const std::function<double (const double &)> &backtransformEffectSizeFunction() {return d_backtransformEffectSizeFunction;}

protected:

    double d_statistic;

    double d_pValue;

    QList<double> d_testDescriptiveValues;

    virtual void calculateStatistic() = 0;

    virtual void calculatePValue() = 0;

    virtual void calculateTestDescriptiveValues() = 0;

private:

    const QList<QString> &d_testDescriptiveLabels;

    const std::function<double (const double &)> &d_backtransformEffectSizeFunction;

    bool d_isPValueCalculated;

    bool d_isStatisticCalculated;

    bool d_isTestDescriptiveValuesCalculated;

};


#endif // BASEMETAANALYSISAPPROACH_H
