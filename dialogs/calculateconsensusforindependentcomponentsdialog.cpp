#include "calculateconsensusforindependentcomponentsdialog.h"

CalculateConsensusForIndependentComponentsDialog::CalculateConsensusForIndependentComponentsDialog(QWidget *parent) :
    BaseDialog(parent, "calculate consensus for independent components", "calculateconsensusforindependentcomponentsdialog")
{

    CalculateConsensusForIndependentComponentsParameters *calculateConsensusForIndependentComponentsParameters = new CalculateConsensusForIndependentComponentsParameters;

    BaseDialog::d_parameters = QSharedPointer<CalculateConsensusForIndependentComponentsParameters>(calculateConsensusForIndependentComponentsParameters);

    BaseDialog::initWidgets();

}

void CalculateConsensusForIndependentComponentsDialog::initWidgets_()
{

    SelectFileWidget *selectFileWidget = new SelectFileWidget(this, "selectFileWidget");

    selectFileWidget->label()->setText("select file containing original data");

    selectFileWidget->fileDialog()->setLabelText(QFileDialog::Accept, "Import");

    this->connect(selectFileWidget, &SelectFileWidget::pathChanged, [this](const QString &path){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->pathToOriginalData = path;});

    selectFileWidget->readWidgetSettings();

    d_gridLayout->addWidget(selectFileWidget, 0, 0, 1, 2);


    OrientationWidget *orientationWidget_orientationOfVariablesInOriginalData = new OrientationWidget(this, "orientationWidget_orientationOfVariablesInOriginalData");

    orientationWidget_orientationOfVariablesInOriginalData->label()->setText("select orientation containing variables in original data");

    this->connect(orientationWidget_orientationOfVariablesInOriginalData, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->orientationOfVariablesInOriginalData = orientation;});

    orientationWidget_orientationOfVariablesInOriginalData->readWidgetSettings();

    d_gridLayout->addWidget(orientationWidget_orientationOfVariablesInOriginalData, 1, 0, 1, 1);



    ComboBoxWidget *comboBoxWidget_rMode_originalData = new ComboBoxWidget(this, "comboBoxWidget_rMode_originalData");

    this->connect(comboBoxWidget_rMode_originalData->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_rMode_originalData](){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->rMode_originalData = comboBoxWidget_rMode_originalData->comboBox()->currentData().toBool();});

    comboBoxWidget_rMode_originalData->label()->setText("first token in header in original data file defines column with row identifiers");

    comboBoxWidget_rMode_originalData->comboBox()->addItem("true", true);

    comboBoxWidget_rMode_originalData->comboBox()->addItem("false", false);

    comboBoxWidget_rMode_originalData->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_rMode_originalData, 1, 1, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_directoryContainingIndependentComponents = new SelectDirectoryWidget(this, "selectDirectoryWidget_directoryContainingIndependentComponents");

    selectDirectoryWidget_directoryContainingIndependentComponents->label()->setText("select directory containing independent components");

    this->connect(selectDirectoryWidget_directoryContainingIndependentComponents, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->directoryContainingIndependentComponents = path;});

    selectDirectoryWidget_directoryContainingIndependentComponents->fileDialog()->setLabelText(QFileDialog::Accept, "Select");

    selectDirectoryWidget_directoryContainingIndependentComponents->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_directoryContainingIndependentComponents, 2, 0, 1, 2);


    OrientationWidget *orientationWidget_orientationOfIndependentComponentsInFile = new OrientationWidget(this, "orientationWidget_orientationOfIndependentComponentsInFile");

    orientationWidget_orientationOfIndependentComponentsInFile->label()->setText("select orientation containing independent components");

    this->connect(orientationWidget_orientationOfIndependentComponentsInFile, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->orientationOfIndependentComponentsInFile = orientation;});

    orientationWidget_orientationOfIndependentComponentsInFile->readWidgetSettings();

    d_gridLayout->addWidget(orientationWidget_orientationOfIndependentComponentsInFile, 3, 0, 1, 1);



    ComboBoxWidget *comboBoxWidget_rMode_independentComponents = new ComboBoxWidget(this, "comboBoxWidget_rMode_independentComponents");

    this->connect(comboBoxWidget_rMode_independentComponents->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_rMode_independentComponents](){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->rMode_independentComponents = comboBoxWidget_rMode_independentComponents->comboBox()->currentData().toBool();});

    comboBoxWidget_rMode_independentComponents->label()->setText("first token in header in independent component files defines column with row identifiers");

    comboBoxWidget_rMode_independentComponents->comboBox()->addItem("true", true);

    comboBoxWidget_rMode_independentComponents->comboBox()->addItem("false", false);

    comboBoxWidget_rMode_independentComponents->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_rMode_independentComponents, 3, 1, 1, 1);



    DoubleSpinBoxWidget *doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->pearsonCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->label()->setText("pearson correlation threshold for clustering components from multiple runs");

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.25, 1.0);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.9);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus, 4, 0, 1, 1);


    d_doubleSpinBoxWidget_credibilityIndexThreshold = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_credibilityIndexThreshold");

    this->connect(d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->threshold_credibilityIndex = value;});

    d_doubleSpinBoxWidget_credibilityIndexThreshold->label()->setText("threshold for credibility index");

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setDecimals(6);

    QDir dir = selectDirectoryWidget_directoryContainingIndependentComponents->fileDialog()->directory();

    if (dir.exists()) {

        dir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );

        d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(1.0 / static_cast<double>(dir.count()));

        d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setRange(1.0 / static_cast<double>(dir.count()), 1.0);

    } else {

        d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(0.0);

        d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setRange(0.0, 1.0);

    }

    d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    d_doubleSpinBoxWidget_credibilityIndexThreshold->readWidgetSettings();

    d_gridLayout->addWidget(d_doubleSpinBoxWidget_credibilityIndexThreshold, 4, 1, 1, 1);

    this->connect(selectDirectoryWidget_directoryContainingIndependentComponents, &SelectDirectoryWidget::pathChanged, [this](const QString &path){

        QDir dir = QDir(path);

        if (dir.exists()) {

            dir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );

            d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(1.0 / static_cast<double>(dir.count()));

        } else
            d_doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(0.0);

    });


    ComboBoxWidget *comboBoxWidget_metricToDetermineIndependence = new ComboBoxWidget(this, "comboBoxWidget_metricToDetermineIndependence");

    this->connect(comboBoxWidget_metricToDetermineIndependence->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_metricToDetermineIndependence](){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->metricToDetermineIndependence = comboBoxWidget_metricToDetermineIndependence->comboBox()->currentText();});

    comboBoxWidget_metricToDetermineIndependence->label()->setText("metric to determine independence");

    comboBoxWidget_metricToDetermineIndependence->comboBox()->addItem("distance correlation");

    comboBoxWidget_metricToDetermineIndependence->comboBox()->addItem("XICOR");

    comboBoxWidget_metricToDetermineIndependence->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_metricToDetermineIndependence, 5, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->distanceCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->label()->setText("distance or XI correlation threshold for filter non-independent consensus components");

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.0, 1.0);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.5);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_distanceCorrelationThresholdForConsensus, 5, 1, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 6, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 7, 0, 1, 2);


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<CalculateConsensusForIndependentComponentsParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"ICA: independent components (consensus)", "ICA: mixing matrix (consensus)", "ICA: flipped independent components (consensus)", "ICA: flipped mixing matrix (consensus)", "ICA: robustness metrics independent components (consensus)", "ICA: XICOR between independent components (consensus)"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 8, 0, 1, 2);

}
