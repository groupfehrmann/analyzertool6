QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#CONFIG += c++2a

CONFIG += c++2b

#CONFIG += sdk_no_version_check

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_MACOSX_DEPLOYMENT_TARGET = 13.5

SOURCES += \
    containers/annotatedmatrix.cpp \
    containers/matrix.cpp \
    containers/result.cpp \
    controller.cpp \
    datarepository.cpp \
    dialogs/annotationsfromasinglefiledialog.cpp \
    dialogs/basedialog.cpp \
    dialogs/calculateconsensusforindependentcomponentsdialog.cpp \
    dialogs/comparesamplescontinuousdialog.cpp \
    dialogs/datafromasinglefiledialog.cpp \
    dialogs/descriptivesdialog.cpp \
    dialogs/distancesimilaritymatrixdialog.cpp \
    dialogs/exportdialog.cpp \
    dialogs/guiltbyassociationanalysisdialog.cpp \
    dialogs/icaimageanalysisdialog.cpp \
    dialogs/independentcomponentanalysisdialog.cpp \
    dialogs/mergedatadialog.cpp \
    dialogs/principalcomponentanalysisdialog.cpp \
    dialogs/projectdataonindependentcomponentsdialog.cpp \
    dialogs/randomselectdialog.cpp \
    dialogs/removeduplicateidentifiersdialog.cpp \
    dialogs/replaceidentifierwithannotationvaluedialog.cpp \
    dialogs/selectdatawithtokensdialog.cpp \
    dialogs/setenrichmentanalysisdialog.cpp \
    dialogs/shuffledatadialog.cpp \
    dialogs/shuffleidentifiersdialog.cpp \
    dialogs/splitidentifiersdialog.cpp \
    dialogs/sysmexdataprocessordialog.cpp \
    dialogs/transformdatabyremovingprincipalcomponentsdialog.cpp \
    dialogs/transformdatausingmedianpolishdialog.cpp \
    dialogs/transformdatausingquantilenormalizationdialog.cpp \
    dialogs/transformdatausingsequenceoffunctionsdialog.cpp \
    dialogs/transformdatawithpredefinedmetricsdialog.cpp \
    logmonitor.cpp \
    main.cpp \
    mainwindow.cpp \
    math/complexfastica.cpp \
    math/fastdistancecorrelation.cpp \
    math/fastica.cpp \
    math/interp_1d.cpp \
    math/ludcmp.cpp \
    math/mathdescriptives.cpp \
    math/mathoperations.cpp \
    math/mathutilityfunctions.cpp \
    math/symmetricgeneralizednormal.cpp \
    math/trip.cpp \
    models/annotatedmatrixannotationsmodel.cpp \
    models/annotatedmatrixdatamodel.cpp \
    models/datarepositorymodel.cpp \
    models/logmonitormodel.cpp \
    models/progressmonitormodel.cpp \
    progressmonitor.cpp \
    statistics/bartlettchisquaredtest.cpp \
    statistics/basemetaanalysisapproach.cpp \
    statistics/basestatisticaltest.cpp \
    statistics/dependentttest.cpp \
    statistics/fishersmetaanalysisapproach.cpp \
    statistics/fisherstrendmetaanalysisapproach.cpp \
    statistics/fittingdistributions.cpp \
    statistics/genericinversemethodwithfixedeffectmodelmetaanalysisapproach.cpp \
    statistics/genericinversemethodwithrandomeffectmodelmetaanalysisapproach.cpp \
    statistics/kolmogorovsmirnovtest.cpp \
    statistics/kruskalwallischisquaredtest.cpp \
    statistics/lancastersmetaanalysisapproach.cpp \
    statistics/lancasterstrendmetaanalysisapproach.cpp \
    statistics/leveneftest.cpp \
    statistics/liptaksmetaanalysisapproach.cpp \
    statistics/liptakstrendmetaanalysisapproach.cpp \
    statistics/mannwhitneyutest.cpp \
    statistics/stouffersmetaanalysisapproach.cpp \
    statistics/stoufferstrendmetaanalysisapproach.cpp \
    statistics/studentttest.cpp \
    statistics/welchttest.cpp \
    utils/conversionfunctions.cpp \
    widgets/annotatedmatrixwidget.cpp \
    widgets/comboboxwidget.cpp \
    widgets/datarepositorywidget.cpp \
    widgets/doublespinboxwidget.cpp \
    widgets/itemselectorwidget.cpp \
    widgets/logmonitorwidget.cpp \
    widgets/orientationwidget.cpp \
    widgets/progressmonitorwidget.cpp \
    widgets/resulttablewidget.cpp \
    widgets/selectdelimiterwidget.cpp \
    widgets/selectdirectorywidget.cpp \
    widgets/selectfilewidget.cpp \
    widgets/selectmultiplefileswidget.cpp \
    widgets/spinboxwidget.cpp \
    widgets/stackedobjectwidget.cpp \
    widgets/texteditwidget.cpp \
    widgets/transformsequencefunctionsbuilderwidget.cpp \
    workerclasses/annotationsfromasinglefileworker.cpp \
    workerclasses/baseparameters.cpp \
    workerclasses/baseworker.cpp \
    workerclasses/calculateconsensusforindependentcomponentsworker.cpp \
    workerclasses/comparesamplescontinuousworker.cpp \
    workerclasses/datafromasinglefileworker.cpp \
    workerclasses/descriptivesworker.cpp \
    workerclasses/distancesimilaritymatrixworker.cpp \
    workerclasses/exportworker.cpp \
    workerclasses/guiltbyassociationanalysisworker.cpp \
    workerclasses/icaimageanalysisworker.cpp \
    workerclasses/independentcomponentanalysisworker.cpp \
    workerclasses/mergedataworker.cpp \
    workerclasses/openprojectdataresultworker.cpp \
    workerclasses/principalcomponentanalysisworker.cpp \
    workerclasses/projectdataonindependentcomponentsworker.cpp \
    workerclasses/randomselectworker.cpp \
    workerclasses/removeduplicateidentifiersworker.cpp \
    workerclasses/replaceidentifierwithannotationvalueworker.cpp \
    workerclasses/saveprojectdataresultworker.cpp \
    workerclasses/selectdatawithtokensworker.cpp \
    workerclasses/setenrichmentanalysisworker.cpp \
    workerclasses/shuffledataworker.cpp \
    workerclasses/shuffleidentifiersworker.cpp \
    workerclasses/splitidentifiersworker.cpp \
    workerclasses/sysmexdataprocessorworker.cpp \
    workerclasses/transformdatabyremovingprincipalcomponentsworker.cpp \
    workerclasses/transformdatausingmedianpolishworker.cpp \
    workerclasses/transformdatausingquantilenormalizationworker.cpp \
    workerclasses/transformdatausingsequenceoffunctionsworker.cpp \
    workerclasses/transformdatawithpredefinedmetricsworker.cpp \
    workerclasses/transposedataworker.cpp

HEADERS += \
    containers/annotatedmatrix.h \
    containers/matrix.h \
    containers/result.h \
    controller.h \
    datarepository.h \
    dialogs/annotationsfromasinglefiledialog.h \
    dialogs/basedialog.h \
    dialogs/calculateconsensusforindependentcomponentsdialog.h \
    dialogs/comparesamplescontinuousdialog.h \
    dialogs/datafromasinglefiledialog.h \
    dialogs/descriptivesdialog.h \
    dialogs/distancesimilaritymatrixdialog.h \
    dialogs/exportdialog.h \
    dialogs/guiltbyassociationanalysisdialog.h \
    dialogs/icaimageanalysisdialog.h \
    dialogs/independentcomponentanalysisdialog.h \
    dialogs/mergedatadialog.h \
    dialogs/principalcomponentanalysisdialog.h \
    dialogs/projectdataonindependentcomponentsdialog.h \
    dialogs/randomselectdialog.h \
    dialogs/removeduplicateidentifiersdialog.h \
    dialogs/replaceidentifierwithannotationvaluedialog.h \
    dialogs/selectdatawithtokensdialog.h \
    dialogs/setenrichmentanalysisdialog.h \
    dialogs/shuffledatadialog.h \
    dialogs/shuffleidentifiersdialog.h \
    dialogs/splitidentifiersdialog.h \
    dialogs/sysmexdataprocessordialog.h \
    dialogs/transformdatabyremovingprincipalcomponentsdialog.h \
    dialogs/transformdatausingmedianpolishdialog.h \
    dialogs/transformdatausingquantilenormalizationdialog.h \
    dialogs/transformdatausingsequenceoffunctionsdialog.h \
    dialogs/transformdatawithpredefinedmetricsdialog.h \
    logmonitor.h \
    mainwindow.h \
    math/XoshiroCpp.hpp \
    math/amoeba.h \
    math/complexfastica.h \
    math/fastdistancecorrelation.h \
    math/fastica.h \
    math/interp_1d.h \
    math/johnsontransformation.h \
    math/ludcmp.h \
    math/mathdescriptives.h \
    math/mathoperations.h \
    math/mathutilityfunctions.h \
    math/mins.h \
    math/roots_multidim.h \
    math/symmetricgeneralizednormal.h \
    math/trip.h \
    models/annotatedmatrixannotationsmodel.h \
    models/annotatedmatrixdatamodel.h \
    models/datarepositorymodel.h \
    models/logmonitormodel.h \
    models/progressmonitormodel.h \
    progressmonitor.h \
    statistics/bartlettchisquaredtest.h \
    statistics/basemetaanalysisapproach.h \
    statistics/basestatisticaltest.h \
    statistics/dependentttest.h \
    statistics/fisherexact2x2test.h \
    statistics/fishersmetaanalysisapproach.h \
    statistics/fisherstrendmetaanalysisapproach.h \
    statistics/fittingdistributions.h \
    statistics/genericinversemethodwithfixedeffectmodelmetaanalysisapproach.h \
    statistics/genericinversemethodwithrandomeffectmodelmetaanalysisapproach.h \
    statistics/kolmogorovsmirnovtest.h \
    statistics/kruskalwallischisquaredtest.h \
    statistics/lancastersmetaanalysisapproach.h \
    statistics/lancasterstrendmetaanalysisapproach.h \
    statistics/leveneftest.h \
    statistics/liptaksmetaanalysisapproach.h \
    statistics/liptakstrendmetaanalysisapproach.h \
    statistics/mannwhitneyutest.h \
    statistics/stouffersmetaanalysisapproach.h \
    statistics/stoufferstrendmetaanalysisapproach.h \
    statistics/studentttest.h \
    statistics/welchttest.h \
    utils/conversionfunctions.h \
    widgets/annotatedmatrixwidget.h \
    widgets/comboboxwidget.h \
    widgets/datarepositorywidget.h \
    widgets/doublespinboxwidget.h \
    widgets/itemselectorwidget.h \
    widgets/logmonitorwidget.h \
    widgets/orientationwidget.h \
    widgets/progressmonitorwidget.h \
    widgets/resulttablewidget.h \
    widgets/selectdelimiterwidget.h \
    widgets/selectdirectorywidget.h \
    widgets/selectfilewidget.h \
    widgets/selectmultiplefileswidget.h \
    widgets/spinboxwidget.h \
    widgets/stackedobjectwidget.h \
    widgets/texteditwidget.h \
    widgets/transformsequencefunctionsbuilderwidget.h \
    workerclasses/annotationsfromasinglefileworker.h \
    workerclasses/baseparameters.h \
    workerclasses/baseworker.h \
    workerclasses/calculateconsensusforindependentcomponentsworker.h \
    workerclasses/comparesamplescontinuousworker.h \
    workerclasses/datafromasinglefileworker.h \
    workerclasses/descriptivesworker.h \
    workerclasses/distancesimilaritymatrixworker.h \
    workerclasses/exportworker.h \
    workerclasses/guiltbyassociationanalysisworker.h \
    workerclasses/icaimageanalysisworker.h \
    workerclasses/independentcomponentanalysisworker.h \
    workerclasses/mergedataworker.h \
    workerclasses/openprojectdataresultworker.h \
    workerclasses/principalcomponentanalysisworker.h \
    workerclasses/projectdataonindependentcomponentsworker.h \
    workerclasses/randomselectworker.h \
    workerclasses/removeduplicateidentifiersworker.h \
    workerclasses/replaceidentifierwithannotationvalueworker.h \
    workerclasses/saveprojectdataresultworker.h \
    workerclasses/selectdatawithtokensworker.h \
    workerclasses/setenrichmentanalysisworker.h \
    workerclasses/shuffledataworker.h \
    workerclasses/shuffleidentifiersworker.h \
    workerclasses/splitidentifiersworker.h \
    workerclasses/sysmexdataprocessorworker.h \
    workerclasses/transformdatabyremovingprincipalcomponentsworker.h \
    workerclasses/transformdatausingmedianpolishworker.h \
    workerclasses/transformdatausingquantilenormalizationworker.h \
    workerclasses/transformdatausingsequenceoffunctionsworker.h \
    workerclasses/transformdatawithpredefinedmetricsworker.h \
    workerclasses/transposedataworker.h

FORMS += \
    dialogs/basedialog.ui \
    mainwindow.ui \
    widgets/annotatedmatrixwidget.ui \
    widgets/comboboxwidget.ui \
    widgets/datarepositorywidget.ui \
    widgets/doublespinboxwidget.ui \
    widgets/itemselectorwidget.ui \
    widgets/logmonitorwidget.ui \
    widgets/orientationwidget.ui \
    widgets/progressmonitorwidget.ui \
    widgets/resulttablewidget.ui \
    widgets/selectdelimiterwidget.ui \
    widgets/selectdirectorywidget.ui \
    widgets/selectfilewidget.ui \
    widgets/selectmultiplefileswidget.ui \
    widgets/spinboxwidget.ui \
    widgets/texteditwidget.ui \
    widgets/transformsequencefunctionsbuilderwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#unix:!macx: INCLUDEPATH += /usr/local/include
u#nix:!macx: LIBS += -L/usr/local/lib -lopenblas
unix:!macx: INCLUDEPATH += /usr/local/include/opencv4
unix:!macx: LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_highgui


macx: INCLUDEPATH += /usr/local/include/opencv4
macx: LIBS += -L/usr/local/lib -lopencv_world
#-lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_highgui

macx: INCLUDEPATH += /opt/OpenBLAS_64/include #/opt/OpenBLAS/include/openblas64
macx: LIBS += /opt/OpenBLAS_64/lib/libopenblas.a /usr/local/gfortran/lib/libgfortran.dylib#/opt/OpenBLAS/lib/libopenblas_64.a -L/usr/local/gfortran/lib -lgfortran


#macx: LIBS += -L$$PWD/opencv/install/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_highgui
#macx: INCLUDEPATH += / $$PWD/opencv/install/include/opencv4


# In OpenBLAS directory
# make clean
# make USE_THREAD=1 USE_OPENMP=0 INTERFACE64=1 BINARY=64 LAPACK_COMPLEX_CPP=1 HAVE_LAPACK_CONFIG_H=1  -> on mac CC=clang
# make PREFIX=$PWD install
# add line in lapack.h -> #define HAVE_LAPACK_CONFIG_H
# add lines #define LAPACK_COMPLEX_CPP and #define LAPACK_ILP64 to file lapacke_config.h
