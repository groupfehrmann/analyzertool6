#ifndef COMPARESAMPLESCONTINUOUSDIALOG_H
#define COMPARESAMPLESCONTINUOUSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/comparesamplescontinuousworker.h"
#include "statistics/basestatisticaltest.h"
#include "statistics/studentttest.h"
#include "statistics/welchttest.h"
#include "statistics/mannwhitneyutest.h"
#include "statistics/kolmogorovsmirnovtest.h"
#include "statistics/leveneftest.h"
#include "statistics/kruskalwallischisquaredtest.h"
#include "statistics/bartlettchisquaredtest.h"
#include "statistics/dependentttest.h"
#include "statistics/fishersmetaanalysisapproach.h"
#include "statistics/fisherstrendmetaanalysisapproach.h"
#include "statistics/stouffersmetaanalysisapproach.h"
#include "statistics/stoufferstrendmetaanalysisapproach.h"
#include "statistics/liptaksmetaanalysisapproach.h"
#include "statistics/liptakstrendmetaanalysisapproach.h"
#include "statistics/lancastersmetaanalysisapproach.h"
#include "statistics/lancasterstrendmetaanalysisapproach.h"
#include "statistics/genericinversemethodwithfixedeffectmodelmetaanalysisapproach.h"
#include "statistics/genericinversemethodwithrandomeffectmodelmetaanalysisapproach.h"

class CompareSamplesContinuousDialog : public BaseDialog
{

public:

    CompareSamplesContinuousDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

    ComboBoxWidget *d_comboBoxWidget_annotationLabelDefiningSamples;

    ItemSelectorWidget *d_itemSelectorWidget_sampleIdentifiers;

    ComboBoxWidget *d_comboBoxWidget_annotationLabelDefiningItemSubsets;

    ItemSelectorWidget *d_itemSelectorWidget_itemSubsetIdentifiers;

    ComboBoxWidget *d_comboBoxWidget_annotationLabelDefiningPairs;

    ItemSelectorWidget *d_itemSelectorWidget_annotationLabelsForVariables;

    ComboBoxWidget *d_comboBoxWidget_metaAnalysisApproach;

     ComboBoxWidget *d_comboBoxWidget_statisticalTest;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(qsizetype index);

    void comboBoxWidget_annotationLabelDefiningSamplesCurrentIndexChanged(qsizetype index);

    void comboBoxWidget_statisticalTestCurrentTextChanged(const QString &text);

    void comboBoxWidget_metaAnalysisApproachCurrentTextChanged(const QString &text);

};

#endif // COMPARESAMPLESCONTINUOUSDIALOG_H
