#ifndef OPENPROJECTDATARESULTWORKER_H
#define OPENPROJECTDATARESULTWORKER_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QApplication>
#include <QDataStream>
#include <QTimer>
#include <QThread>

#include <utility>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class OpenProjectDataResultParameters : public BaseParameters
{

public:

    OpenProjectDataResultParameters(QObject *parent = nullptr);

    QString pathToFile;


private:

    bool isValid_();

    QString prettyPrint_() const;

};

class OpenProjectDataResultWorker : public BaseWorker
{

    Q_OBJECT

public:

    OpenProjectDataResultWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

signals:

    void addToRecentlyOpenedFiles(const QString objectType, const QString &label, const QString &pathToFile);

};

#endif // OPENPROJECTDATARESULTWORKER_H
