#ifndef REMOVEDUPLICATEIDENTIFIERSDIALOG_H
#define REMOVEDUPLICATEIDENTIFIERSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/comboboxwidget.h"
#include "workerclasses/removeduplicateidentifiersworker.h"

class RemoveDuplicateIdentifiersDialog : public BaseDialog
{

public:

    RemoveDuplicateIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};

#endif // REMOVEDUPLICATEIDENTIFIERSDIALOG_H
