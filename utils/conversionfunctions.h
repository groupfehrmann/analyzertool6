#ifndef CONVERSIONFUNCTIONS_H
#define CONVERSIONFUNCTIONS_H

#include <QString>
#include <QVariant>

#include <string>
#include <limits>

namespace ConversionFunctions {

    bool convert(const QString &string, short &value);

    bool convert(const QString &string, unsigned short &value);

    bool convert(const QString &string, int &value);

    bool convert(const QString &string, unsigned int &value);

    bool convert(const QString &string, long &value);

    bool convert(const QString &string, unsigned long &value);

    bool convert(const QString &string, long long &value);

    bool convert(const QString &string, unsigned long long &value);

    bool convert(const QString &string, float &value);

    bool convert(const QString &string, double &value);

    bool convert(const QString &string, long double &value);

    bool convert(const QString &string, QString &value);

    bool convert(const QString &string, unsigned char &value);

    bool convert(const QString &string, bool &value);

    template <typename T> T invalidValue(T temp = T());

    template <> short invalidValue(short temp);

    template <> unsigned short invalidValue(unsigned short temp);

    template <> int invalidValue(int temp);

    template <> unsigned int invalidValue(unsigned int temp);

    template <> long invalidValue(long temp);

    template <> unsigned long invalidValue(unsigned long temp);

    template <> long long invalidValue(long long temp);

    template <> unsigned long long invalidValue(unsigned long long temp);

    template <> float invalidValue(float temp);

    template <> double invalidValue(double temp);

    template <> long double invalidValue(long double temp);

    template <> QString invalidValue(QString temp);

    template <> unsigned char invalidValue(unsigned char temp);

    template <> bool invalidValue(bool temp);

}

template <typename T> T ConversionFunctions::invalidValue(T temp)
{

    Q_UNUSED(temp);

    return T();

}

#endif // CONVERSIONFUNCTIONS_H
