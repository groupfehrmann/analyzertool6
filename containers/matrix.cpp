#include "matrix.h"

BaseMatrix::BaseMatrix() :
    d_data(new BaseMatrixData)
{

    d_data->d_columnCount = 0;

    d_data->d_rowCount = 0;

}

BaseMatrix::BaseMatrix(BaseMatrix &&baseMatrix) :
    d_data(std::move(baseMatrix.d_data))
{

}

BaseMatrix::BaseMatrix(const BaseMatrix &baseMatrix) :
    d_data(baseMatrix.d_data)
{

}

BaseMatrix::BaseMatrix(qsizetype rowCount, qsizetype columnCount, const char *typeName) :
    d_data(new BaseMatrixData)
{

    d_data->d_columnCount = columnCount;

    d_data->d_rowCount = rowCount;

    d_data->d_typeName = typeName;

}

qsizetype BaseMatrix::columnCount() const
{

    return d_data->d_columnCount;

}

qsizetype BaseMatrix::rowCount() const
{

    return d_data->d_rowCount;

}

const char* BaseMatrix::typeName() const
{

    return d_data->d_typeName;

}

BaseMatrix &BaseMatrix::operator=(const BaseMatrix &baseMatrix)
{

    if (this != &baseMatrix)
        d_data.operator=(baseMatrix.d_data);

    return *this;

}

BaseMatrix &BaseMatrix::operator=(BaseMatrix &&baseMatrix)
{

    if (this != &baseMatrix) {

        d_data.operator=(baseMatrix.d_data);

        baseMatrix.d_data->d_columnCount = 0;

        baseMatrix.d_data->d_rowCount = 0;

    }

    return *this;

}

BaseMatrix::~BaseMatrix()
{

}

QDataStream& operator<<(QDataStream& out, BaseMatrix &baseMatrix)
{

    return baseMatrix.writeToDataStream(out);

}

QDataStream& operator>>(QDataStream& in, BaseMatrix &baseMatrix)
{

    return baseMatrix.readFromDataStream(in);

}
