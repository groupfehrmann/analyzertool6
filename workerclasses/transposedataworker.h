#ifndef TRANSPOSEDATAWORKER_H
#define TRANSPOSEDATAWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class TransposeDataParameters : public BaseParameters
{

public:

    TransposeDataParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransposeDataWorker : public BaseWorker
{

public:

    TransposeDataWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

#endif // TRANSPOSEDATAWORKER_H
