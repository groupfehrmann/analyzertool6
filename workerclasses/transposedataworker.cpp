#include "transposedataworker.h"

TransposeDataParameters::TransposeDataParameters(QObject *parent) :
    BaseParameters(parent, "transposedata")
{

}

bool TransposeDataParameters::isValid_()
{

    return true;

}

QString TransposeDataParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[transpose data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    return prettyPrintString;

}

TransposeDataWorker::TransposeDataWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("transpose data"), parameters, "transposedata")
{

}

void TransposeDataWorker::doWork_()
{

    QSharedPointer<TransposeDataParameters> parameters(d_data->d_parameters.dynamicCast<TransposeDataParameters>());

    QUuid uuidProgress = this->startProgress("transposing", 0, 0);

    parameters->baseAnnotatedMatrix->transpose();

    this->stopProgress(uuidProgress);

}

bool TransposeDataWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<TransposeDataParameters> parameters(d_data->d_parameters.dynamicCast<TransposeDataParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    parameters->baseAnnotatedMatrix->beginResetData();

    parameters->baseAnnotatedMatrix->beginResetColumnAnnotations();

    parameters->baseAnnotatedMatrix->beginResetRowAnnotations();

    return true;

}

void TransposeDataWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<TransposeDataParameters> parameters(d_data->d_parameters.dynamicCast<TransposeDataParameters>());

    parameters->baseAnnotatedMatrix->endResetData();

    parameters->baseAnnotatedMatrix->endResetColumnAnnotations();

    parameters->baseAnnotatedMatrix->endResetRowAnnotations();

    parameters->baseAnnotatedMatrix->unlock();

}
