#include "replaceidentifierwithannotationvaluedialog.h"

ReplaceIdentifierWithAnnotationValueDialog::ReplaceIdentifierWithAnnotationValueDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "replace identifier with annotation value", "replaceidentifierwithannotationvaluedialog")
{

    ReplaceIdentifierWithAnnotationValueParameters *replaceIdentifierWithAnnotationValueParameters = new ReplaceIdentifierWithAnnotationValueParameters;

    replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<ReplaceIdentifierWithAnnotationValueParameters>(replaceIdentifierWithAnnotationValueParameters);

    BaseDialog::initWidgets();

}

void ReplaceIdentifierWithAnnotationValueDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 1);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_comboBoxWidget_annotationLabelUsedForReplacements = new ComboBoxWidget(this, "d_comboBoxWidget_annotationLabelUsedForReplacements");

    this->connect(d_comboBoxWidget_annotationLabelUsedForReplacements->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data())->annotationLabelUsedForReplacement = text;});

    this->connect(d_comboBoxWidget_annotationLabelUsedForReplacements->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data())->annotationIndexUsedForReplacement = index;});

    d_comboBoxWidget_annotationLabelUsedForReplacements->label()->setText("select annotation label containing replacements for current identifiers");

    d_gridLayout->addWidget(d_comboBoxWidget_annotationLabelUsedForReplacements, 2, 0, 1, 1);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &ReplaceIdentifierWithAnnotationValueDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void ReplaceIdentifierWithAnnotationValueDialog::orientationChanged(Qt::Orientation orientation)
{

    ReplaceIdentifierWithAnnotationValueParameters *replaceIdentifierWithAnnotationValueParameters = static_cast<ReplaceIdentifierWithAnnotationValueParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix->identifiers(orientation, true), replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix->indexes(orientation, true));

    d_comboBoxWidget_annotationLabelUsedForReplacements->comboBox()->clear();

    d_comboBoxWidget_annotationLabelUsedForReplacements->comboBox()->addItems(replaceIdentifierWithAnnotationValueParameters->baseAnnotatedMatrix->annotationLabels(orientation, false));

}
