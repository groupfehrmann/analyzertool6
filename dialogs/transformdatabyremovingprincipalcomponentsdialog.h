#ifndef TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSDIALOG_H
#define TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/transformdatabyremovingprincipalcomponentsworker.h"

class TransformDataByRemovingPrincipalComponentsDialog : public BaseDialog
{

public:

    TransformDataByRemovingPrincipalComponentsDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

    ItemSelectorWidget *d_itemSelectorWidget_componentsToRemove;

    SpinBoxWidget *d_spinBoxWidget_thresholdMaximumNumberOfComponents;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void updateMaximumNumberOfComponents();

};

#endif // TRANSFORMDATABYREMOVINGPRINCIPALCOMPONENTSDIALOG_H
