#ifndef SPINBOXWIDGET_H
#define SPINBOXWIDGET_H

#include <QWidget>
#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QSpinBox>

#include "dialogs/basedialog.h"

namespace Ui {

    class SpinBoxWidget;

}

class SpinBoxWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SpinBoxWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QSpinBox *spinBox();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~SpinBoxWidget();

private:

    Ui::SpinBoxWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

};

#endif // SPINBOXWIDGET_H
