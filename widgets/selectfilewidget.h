#ifndef SELECTFILEWIDGET_H
#define SELECTFILEWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QFile>
#include <QFileDialog>

#include "dialogs/basedialog.h"

namespace Ui {

    class SelectFileWidget;

}

class SelectFileWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectFileWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString(), QFileDialog::AcceptMode acceptMode = QFileDialog::AcceptOpen);

    QFileDialog *fileDialog();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~SelectFileWidget();

private:

    Ui::SelectFileWidget *ui;

    QFileDialog::AcceptMode d_acceptMode;

    QFileDialog *d_fileDialog;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

    void evaluatePath();

private slots:

    void on_lineEdit_path_textEdited(const QString &text);

    void on_pushButton_browse_clicked();

signals:

    void pathChanged(const QString &path);

    void filterSelected(const QString &filter);

    void invalidPath();

};

#endif // SELECTFILEWIDGET_H
