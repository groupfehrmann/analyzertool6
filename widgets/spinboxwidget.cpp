#include "spinboxwidget.h"
#include "ui_spinboxwidget.h"

SpinBoxWidget::SpinBoxWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::SpinBoxWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

}

QSpinBox *SpinBoxWidget::spinBox()
{

    return ui->spinBox;

}

QLabel *SpinBoxWidget::label()
{

    return ui->label;

}

void SpinBoxWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/spinBox");

    if (settings.contains("value"))
        ui->spinBox->setValue(settings.value("value").toInt());

    if (settings.contains("displayIntegerBase"))
        ui->spinBox->setDisplayIntegerBase(settings.value("displayIntegerBase").toInt());

    if (settings.contains("maximum"))
        ui->spinBox->setMaximum(settings.value("maximum").toInt());

    if (settings.contains("minimum"))
        ui->spinBox->setMinimum(settings.value("minimum").toInt());

    if (settings.contains("prefix"))
        ui->spinBox->setPrefix(settings.value("prefix").toString());

    if (settings.contains("singleStep"))
        ui->spinBox->setSingleStep(settings.value("singleStep").toInt());

    if (settings.contains("stepType"))
        ui->spinBox->setStepType(static_cast<QAbstractSpinBox::StepType>(settings.value("stepType").toInt()));

    if (settings.contains("suffix"))
        ui->spinBox->setSuffix(settings.value("suffix").toString());

    settings.endGroup();

    d_readWidgetSettings = true;

}

void SpinBoxWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/spinBox");

    settings.setValue("value", ui->spinBox->value());

    settings.setValue("displayIntegerBase", ui->spinBox->displayIntegerBase());

    settings.setValue("maximum", ui->spinBox->maximum());

    settings.setValue("minimum", ui->spinBox->minimum());

    settings.setValue("prefix", ui->spinBox->prefix());

    settings.setValue("singleStep", ui->spinBox->singleStep());

    settings.setValue("stepType", ui->spinBox->stepType());

    settings.setValue("suffix", ui->spinBox->suffix());

    settings.endGroup();

}

SpinBoxWidget::~SpinBoxWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
