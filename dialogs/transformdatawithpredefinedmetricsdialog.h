#ifndef TRANSFORMDATAWITHPREDEFINEDMETRICSDIALOG_H
#define TRANSFORMDATAWITHPREDEFINEDMETRICSDIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>

#include "basedialog.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "widgets/spinboxwidget.h"
#include "workerclasses/transformdatawithpredefinedmetricsworker.h"

class TransformDataWithPredefinedMetricsDialog : public BaseDialog
{

    Q_OBJECT

public:

    TransformDataWithPredefinedMetricsDialog(BaseDialog *parent = nullptr, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices = QList<QSharedPointer<BaseAnnotatedMatrix>>());

protected:

    void initWidgets_() override;

private:

    QList<QSharedPointer<BaseAnnotatedMatrix>> d_baseAnnotatedMatrices;

    OrientationWidget *d_orientationWidget_dataToTransform;

    ItemSelectorWidget *d_itemSelectorWidget_variablesDataToTransform;

    ItemSelectorWidget *d_itemSelectorWidget_itemsDataToTransform;

    OrientationWidget *d_orientationWidget_dataContainingPredefinedMetrics;

    ItemSelectorWidget *d_itemSelectorWidget_itemsDataContainingPredefinedMetrics;

    ComboBoxWidget *d_comboBoxWidget_variableIdentifierPredefinedMetrics;

private slots:

    void orientationDataToTransformChanged(Qt::Orientation orientation);

    void orientationDataContainingPredefinedMetricsChanged(Qt::Orientation orientation);

};

#endif // TRANSFORMDATAWITHPREDEFINEDMETRICSDIALOG_H
