#ifndef TRANSFORMDATAUSINGMEDIANPOLISHWORKER_H
#define TRANSFORMDATAUSINGMEDIANPOLISHWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "math/mathoperations.h"
#include "math/mathutilityfunctions.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"

class TransformDataUsingMedianPolishParameters : public BaseParameters
{

public:

    TransformDataUsingMedianPolishParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedRowVariableIndexes;

    QList<QString> selectedRowVariableIdentifiers;

    QList<qsizetype> selectedColumnVariableIndexes;

    QList<QString> selectedColumnVariableIdentifiers;

    double epsilon;

    qsizetype maximumIterations;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransformDataUsingMedianPolishWorker : public BaseWorker
{

public:

    TransformDataUsingMedianPolishWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void TransformDataUsingMedianPolishWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<TransformDataUsingMedianPolishParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingMedianPolishParameters>());

    QUuid uuidProgress = this->startProgress("performing median polish", 0, parameters->maximumIterations);

    QList<std::reference_wrapper<T>> vectorWithReferencesRowMajor = annotatedMatrix->sliced_references(parameters->selectedRowVariableIndexes, parameters->selectedColumnVariableIndexes);

    QList<std::reference_wrapper<T>> vectorWithReferencesColumnMajor = MathOperations::transpose(vectorWithReferencesRowMajor, parameters->selectedColumnVariableIndexes.count(), parameters->numberOfThreadsToUse);

    QList<std::pair<qsizetype, std::span<std::reference_wrapper<T>>>> rowMajor_indexes_spans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(vectorWithReferencesRowMajor,parameters->selectedColumnVariableIndexes.count());

    QList<std::pair<qsizetype, std::span<std::reference_wrapper<T>>>> columnMajor_indexes_spans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(vectorWithReferencesColumnMajor,parameters->selectedRowVariableIndexes.count());

    double grandEffect = 0.0;

    QList<double> rowEffects(parameters->selectedRowVariableIdentifiers.count(), 0.0);

    QList<double> columnEffects(parameters->selectedColumnVariableIdentifiers.count(), 0.0);

    qsizetype iteration = 1;

    while (1) {

        this->startProgress("update rows", QtConcurrent::map(&d_threadPool, rowMajor_indexes_spans, [&rowEffects](std::pair<qsizetype, std::span<std::reference_wrapper<T>>> &index_span){

                                QList<double> vectorOfValues;

                                vectorOfValues.reserve(index_span.second.size());

                                for (const std::reference_wrapper<T> &value : index_span.second)
                                    vectorOfValues.append(value);

                                double median = boost::math::statistics::median(vectorOfValues.begin(), vectorOfValues.end());

                                rowEffects[index_span.first] += median;

                                for (std::reference_wrapper<T> &value : index_span.second)
                                    value.get() = value - median;

                                ;}));

        this->startProgress("update columns", QtConcurrent::map(&d_threadPool, columnMajor_indexes_spans, [&columnEffects](std::pair<qsizetype, std::span<std::reference_wrapper<T>>> &index_span){

                                QList<double> vectorOfValues;

                                vectorOfValues.reserve(index_span.second.size());

                                for (const std::reference_wrapper<T> &value : index_span.second)
                                    vectorOfValues.append(value);

                                double median = boost::math::statistics::median(vectorOfValues.begin(), vectorOfValues.end());

                                columnEffects[index_span.first] += median;

                                for (std::reference_wrapper<T> &value : index_span.second)
                                    value.get() = value - median;

                                ;}));


        double median_row_effects = boost::math::statistics::median(rowEffects.begin(), rowEffects.end());

        for (double &rowEffect : rowEffects)
            rowEffect -= median_row_effects;

        double median_column_effects = boost::math::statistics::median(columnEffects.begin(), columnEffects.end());

        for (double &columnEffect : columnEffects)
            columnEffect -= median_column_effects;

        double grandEffectNew = grandEffect  + median_row_effects + median_column_effects;

        if (std::abs(grandEffectNew - grandEffect) < parameters->epsilon) {

            this->reportMessage("median polish converged at iteration " + QString::number(iteration) + " with grand effect delta " + QString::number(std::abs(grandEffectNew - grandEffect)));

            break;

        }

        grandEffect += median_row_effects + median_column_effects;

        if (iteration > parameters->maximumIterations) {

            this->reportMessage("median polish reached maximum number of iterations, current change value is " + QString::number(std::abs(grandEffect)));

            break;

        }

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, iteration);

        ++iteration;

    }

    this->stopProgress(uuidProgress);

}

#endif // TRANSFORMDATAUSINGMEDIANPOLISHWORKER_H
