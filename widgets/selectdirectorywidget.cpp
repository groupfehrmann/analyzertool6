#include "selectdirectorywidget.h"
#include "ui_selectdirectorywidget.h"

SelectDirectoryWidget::SelectDirectoryWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::SelectDirectoryWidget),
    d_fileDialog(new QFileDialog(this, QCoreApplication::applicationName() + " - select directory")),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)

{

    ui->setupUi(this);

}

void SelectDirectoryWidget::evaluatePath()
{

    if (QDir(ui->lineEdit_path->text()).exists()) {

        ui->lineEdit_path->setStyleSheet(QString());

        emit pathChanged(ui->lineEdit_path->text());

    } else {

        ui->lineEdit_path->setStyleSheet(QString("color: rgb(255, 0, 0);"));

        emit invalidPath();

    }

}

QFileDialog *SelectDirectoryWidget::fileDialog()
{

    return d_fileDialog;

}

QLabel *SelectDirectoryWidget::label()
{

    return ui->label;

}

void SelectDirectoryWidget::on_lineEdit_path_textEdited(const QString &text)
{

    Q_UNUSED(text);

    this->evaluatePath();

}

void SelectDirectoryWidget::on_pushButton_browse_clicked()
{

    d_fileDialog->setFileMode(QFileDialog::Directory);

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

    if (settings.contains("state"))
        d_fileDialog->restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        d_fileDialog->restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        d_fileDialog->setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (d_fileDialog->exec()) {

        ui->lineEdit_path->setText(d_fileDialog->selectedFiles().first());

        this->evaluatePath();

        QSettings settings;

        settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

        settings.setValue("state", d_fileDialog->saveState());

        settings.setValue("geometry", d_fileDialog->saveGeometry());

        settings.setValue("lastDirectory", d_fileDialog->directory().path());

        settings.endGroup();

    } else
        ui->lineEdit_path->setText(QString());

}

void SelectDirectoryWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit_path");

    if (settings.contains("text"))
        ui->lineEdit_path->setText(settings.value("text").toString());

    settings.endGroup();

    d_readWidgetSettings = true;

    emit pathChanged(ui->lineEdit_path->text());

}

void SelectDirectoryWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/lineEdit_path");

    settings.setValue("text", ui->lineEdit_path->text());

    settings.endGroup();

}

SelectDirectoryWidget::~SelectDirectoryWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
