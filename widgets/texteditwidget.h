#ifndef TEXTEDITWIDGET_H
#define TEXTEDITWIDGET_H

#include <QWidget>
#include <QString>
#include <QTextEdit>
#include <QLabel>

#include "dialogs/basedialog.h"

namespace Ui {

    class TextEditWidget;

}

class TextEditWidget : public QWidget
{

    Q_OBJECT

public:

    explicit TextEditWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QLabel *label();

    QTextEdit *textEdit();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~TextEditWidget();

private:

    Ui::TextEditWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

};

#endif // TEXTEDITWIDGET_H
