#ifndef DATAREPOSITORY_H
#define DATAREPOSITORY_H

#include <QObject>
#include <QSharedDataPointer>
#include <QHash>
#include <QUuid>
#include <QSharedPointer>
#include <QVariant>
#include <QString>
#include <QReadWriteLock>
#include <QReadLocker>
#include <QWriteLocker>

class DataRepositoryData;

class DataRepository : public QObject
{

    Q_OBJECT

public:

    explicit DataRepository(QObject *parent = nullptr);

    const QUuid &currentUuid() const;

    QUuid currentUuidProject() const;

    qsizetype indexOfUuidAnnotatedMatrix(const QUuid &uuidAnnotatedMatrix) const;

    qsizetype indexOfUuidProject(const QUuid &uuidProject) const;

    qsizetype indexOfProjectOfWhichUuidObjectIsMember(const QUuid &uuidObject) const;

    qsizetype indexOfUuidResult(const QUuid &uuidResult) const;

    bool isUuidAnnotatedMatrix(const QUuid &uuid) const;

    bool isUuidProject(const QUuid &uuid) const;

    bool isUuidResult(const QUuid &uuid) const;

    bool isValidUuid(const QUuid &uuid) const;

    QString label(const QUuid &uuid) const;

    qsizetype numberOfAnnotatedMatrices(const QUuid& uuidProject) const;

    qsizetype numberOfResults(const QUuid& uuidProject) const;

    QSharedPointer<QObject> objectPointer(const QUuid &uuidObject);

    QList<QSharedPointer<QObject>> objectPointers(const QUuid &uuidProject);

    const QString &projectLabel(qsizetype index) const;

    QReadWriteLock &readWriteLock();

    QUuid uuidProject(const QUuid &uuidObject) const;

    const QList<QUuid> &uuidsAnnotatedMatrix(const QUuid& uuidProject) const;

    const QList<QUuid> &uuidsProject() const;

    const QList<QUuid> &uuidsResult(const QUuid& uuidProject) const;

    ~DataRepository();

public slots:

    QUuid addObject(const QUuid &uuidProject, QObject *object);

    QUuid addSharedPointerToObject(const QUuid &uuidProject, QSharedPointer<QObject> object);

    QUuid addProject();

    void removeAll();

    void removeAllDataAssociatedWithUuid(const QUuid &uuid);

    void setProjectLabel(qsizetype index, const QString &label);

    void setCurrentUuid(const QUuid &uuid);

private:

    QSharedDataPointer<DataRepositoryData> d_data;

    mutable QReadWriteLock d_readWriteLock;

signals:

    void currentUuidChanged(const QUuid &uuid);

    void objectAdded(const QUuid &uuidObject, const QSharedPointer<QObject> &object);

    void objectRemoved(const QUuid &uuidObject);

};

#endif // DATAREPOSITORY_H
