#ifndef ORIENTATIONWIDGET_H
#define ORIENTATIONWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <QButtonGroup>

#include "dialogs/basedialog.h"

namespace Ui {

    class OrientationWidget;

}

class OrientationWidget : public QWidget
{

    Q_OBJECT

public:

    explicit OrientationWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QLabel *label();

    Qt::Orientation orientation() const;

    void readWidgetSettings();

    void writeWidgetSettings();

    ~OrientationWidget();

private:

    Ui::OrientationWidget *ui;

    QButtonGroup d_buttonGroup;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

signals:

    void orientationChanged(Qt::Orientation orientation) const;

};

#endif // ORIENTATIONWIDGET_H
