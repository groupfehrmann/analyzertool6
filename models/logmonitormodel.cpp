#include "logmonitormodel.h"

LogMonitorSortFilterProxyModel::LogMonitorSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}

bool LogMonitorSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    LogMonitorModel *logMonitorModel = static_cast<LogMonitorModel *>(this->sourceModel());

    return logMonitorModel->show(sourceRow);

}


LogMonitorModel::LogMonitorModel(QObject *parent, const QSharedPointer<LogMonitor> &logMonitor) :
    QAbstractTableModel(parent),
    d_logMonitor(logMonitor)
{

    d_showError = true;

    d_showMessage = true;

    d_showWarning = true;

    d_characterLimit = 1000;

}

void LogMonitorModel::addUuidWorkerToSet(const QUuid &uuidWorker)
{

    if (!d_setOfUuidWorker.contains(uuidWorker)) {

        d_setOfUuidWorker.insert(uuidWorker);

        emit uniqueUuidWorkerAdded(uuidWorker);

    }

}

int LogMonitorModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 4;

}

QVariant LogMonitorModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    const LogMonitorData::LogItem &logMonitorData(d_logMonitor->logItemAt(index.row()));

    if (role == Qt::DisplayRole) {

        switch (index.column()) {

        case 0 : return logMonitorData.d_uuidWorker.toString();

        case 1 : switch (logMonitorData.d_logType) {

                case LogMonitorData::ERROR : return "error";

                case LogMonitorData::MESSAGE : return "message";

                case LogMonitorData::WARNING : return "warning";

                default: return QVariant();

                }

        case 2 : return logMonitorData.d_dataTimeStamp.toString("d MMM yyyy - hh:mm:ss");

        case 3 : return ((d_characterLimit == -1) || logMonitorData.d_string.length() <= d_characterLimit) ? logMonitorData.d_string : (logMonitorData.d_string.sliced(0, d_characterLimit) + "\t...TRUNCATED");

        default: return QVariant();

        }

    } else if (role == Qt::ToolTipRole) {

        if (index.column() == 3)
            return ((d_characterLimit == -1) || logMonitorData.d_string.length() <= d_characterLimit) ? logMonitorData.d_string : (logMonitorData.d_string.sliced(0, d_characterLimit) + "\t...TRUNCATED");

    } else if (role == Qt::ForegroundRole) {

        if (index.column() == 1) {

            switch (logMonitorData.d_logType) {

            case LogMonitorData::ERROR : return QColor(204, 0, 0);

            case LogMonitorData::MESSAGE : return QColor(Qt::black);

            case LogMonitorData::WARNING : return QColor(255, 153, 0);

            default: QVariant();

            }

        }

    }

    return QVariant();

}

QVariant LogMonitorModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if (orientation == Qt::Vertical)
        return QVariant();

    if (role == Qt::DisplayRole) {

        switch (section) {

        case 0 : return QString("worker uuid");

        case 1 : return QString("log type");

        case 2 : return QString("date/time stamp");

        case 3 : return QString("message");

        default : return QVariant();

        }

     }

    return QVariant();

}

int LogMonitorModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;

    return d_logMonitor->count();

}

void LogMonitorModel::clear()
{

    this->beginResetModel();

    d_logMonitor->clear();

    this->endResetModel();

}

void LogMonitorModel::reportError(const QUuid &uuidWorker, const QString &error)
{

    this->beginInsertRows(QModelIndex(), d_logMonitor->count(), d_logMonitor->count());

    this->addUuidWorkerToSet(uuidWorker);

    d_logMonitor->reportError(uuidWorker, error);

    this->endInsertRows();

}

void LogMonitorModel::reportMessage(const QUuid &uuidWorker, const QString &message)
{

    this->beginInsertRows(QModelIndex(), d_logMonitor->count(), d_logMonitor->count());

    this->addUuidWorkerToSet(uuidWorker);

    d_logMonitor->reportMessage(uuidWorker, message);

    this->endInsertRows();

}


void LogMonitorModel::reportWarning(const QUuid &uuidWorker, const QString &warning)
{

    this->beginInsertRows(QModelIndex(), d_logMonitor->count(), d_logMonitor->count());

    this->addUuidWorkerToSet(uuidWorker);

    d_logMonitor->reportWarning(uuidWorker, warning);

    this->endInsertRows();

}

void LogMonitorModel::setShowError(bool show)
{

    this->beginResetModel();

    d_showError = show;

    this->endResetModel();

}

void LogMonitorModel::setShowMessage(bool show)
{

    this->beginResetModel();

    d_showMessage = show;

    this->endResetModel();

}

void LogMonitorModel::setShowWarning(bool show)
{

    this->beginResetModel();

    d_showWarning = show;

    this->endResetModel();

}

void LogMonitorModel::setUuidWorkerToShow(const QUuid &uuidWorker)
{

    this->beginResetModel();

    d_uuidWorkerToShow = uuidWorker;

    this->endResetModel();

}

void LogMonitorModel::setCharacterLimit(qsizetype limit)
{

    this->beginResetModel();

    d_characterLimit = limit;

    this->endResetModel();

}

bool LogMonitorModel::show(qsizetype index) const
{

    const LogMonitorData::LogItem &logMonitorData(d_logMonitor->logItemAt(index));

    bool show_ = true;

    switch (logMonitorData.d_logType) {

    case LogMonitorData::ERROR : show_ = d_showError; break;

    case LogMonitorData::MESSAGE : show_ = d_showMessage; break;

    case LogMonitorData::WARNING : show_ = d_showWarning; break;

    default : show_ = true;

    }

    if (d_uuidWorkerToShow == QUuid() || (d_uuidWorkerToShow == logMonitorData.d_uuidWorker))
        return show_;
    else
        return false;

}
