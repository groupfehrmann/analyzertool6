#ifndef MERGEDATAWORKER_H
#define MERGEDATAWORKER_H

#include <QObject>
#include <QList>
#include <QString>
#include <QUuid>
#include <QApplication>
#include <QFile>
#include <QVariant>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "utils/conversionfunctions.h"

class MergeDataParameters : public BaseParameters
{

public:

    MergeDataParameters(QObject *parent = nullptr);

    bool addDataSpecificSuffixtoAnnotationLabels;

    bool addDataSpecificSuffixtoIdentifiers;

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    QString exportDirectory;

    bool mergeColumnAnnotations;

    QString mergeMode;

    bool mergeRowAnnotations;

    Qt::Orientation orientation;

    bool selectedOnly;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class MergeDataWorker : public BaseWorker
{

public:

    MergeDataWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(AnnotatedMatrix<T> *annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void MergeDataWorker::doWork_(AnnotatedMatrix<T> *annotatedMatrix)
{

    QSharedPointer<MergeDataParameters> parameters(d_data->d_parameters.dynamicCast<MergeDataParameters>());

    bool intersectionMode = (parameters->mergeMode == "intersection") ? true : false;


    QSet<QString> setOfMergedIdentifiers_mergeOrientation;

    QList<QList<QString>> originalIdentifiers_oppositeMergeOrientation_perData;

    QList<QList<QString>> modifiedIdentifiers_oppositeMergeOrientation_perData;

    QList<QList<qsizetype>> indexes_oppositeMergeOrientation_perData;

    QList<QList<qsizetype>> indexes_mergeOrientation_perData;

    QList<QString> mergedIdentifiers_oppositeMergeOrientation;

    QList<QVariant> dataOriginLabel_oppositeMergeOrientation;

    QList<QString> originalIdentifiers_oppositeMergeOrientation;


    QUuid uuidProgress = this->startProgress("create " + QString(intersectionMode ? "intersected set" : "union set") + " of " + QString(parameters->orientation == Qt::Horizontal ? "horizontal" : "vertical") + " identifiers", 0, parameters->baseAnnotatedMatrices.count());


    for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i) {

        originalIdentifiers_oppositeMergeOrientation_perData.append(parameters->baseAnnotatedMatrices.at(i)->identifiers(BaseAnnotatedMatrix::switchOrientation(parameters->orientation), parameters->selectedOnly));

        modifiedIdentifiers_oppositeMergeOrientation_perData.append(originalIdentifiers_oppositeMergeOrientation_perData.last());

        originalIdentifiers_oppositeMergeOrientation.append(originalIdentifiers_oppositeMergeOrientation_perData.last());

        if (parameters->addDataSpecificSuffixtoIdentifiers) {

            for (QString &identifier : modifiedIdentifiers_oppositeMergeOrientation_perData.last())
                identifier += "_" + QString::number(i + 1);

        }

        mergedIdentifiers_oppositeMergeOrientation.append(modifiedIdentifiers_oppositeMergeOrientation_perData.last());

        dataOriginLabel_oppositeMergeOrientation.append(QList<QVariant>(modifiedIdentifiers_oppositeMergeOrientation_perData.last().count(), parameters->baseAnnotatedMatrices.at(i)->property("label")));

        indexes_oppositeMergeOrientation_perData.append(parameters->baseAnnotatedMatrices.at(i)->indexes(BaseAnnotatedMatrix::switchOrientation(parameters->orientation), parameters->selectedOnly));

        QList<QString> identifiers_mergeOrientation = parameters->baseAnnotatedMatrices.at(i)->identifiers(parameters->orientation, parameters->selectedOnly);

        if (intersectionMode && (i != 0))
            setOfMergedIdentifiers_mergeOrientation.intersect(QSet<QString>(identifiers_mergeOrientation.begin(), identifiers_mergeOrientation.end()));
        else
            setOfMergedIdentifiers_mergeOrientation.unite(QSet<QString>(identifiers_mergeOrientation.begin(), identifiers_mergeOrientation.end()));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    if (setOfMergedIdentifiers_mergeOrientation.isEmpty()) {

        this->reportError(QString(intersectionMode ? "intersected set" : "union set") + " of " + QString(parameters->orientation == Qt::Horizontal ? "horizontal" : "vertical") + " identifiers resulted in an empty set");

        QThread::currentThread()->requestInterruption();

        delete annotatedMatrix;

        return;

    }


    AnnotatedMatrix<QVariant> *mappingTable1 = new AnnotatedMatrix<QVariant>;

    mappingTable1->appendList(QString(parameters->orientation == Qt::Horizontal ? "vertical" : "horizontal") + " identifiers", mergedIdentifiers_oppositeMergeOrientation, Qt::Horizontal);

    if (parameters->addDataSpecificSuffixtoIdentifiers)
        mappingTable1->appendList("original " + QString(parameters->orientation == Qt::Horizontal ? "vertical" : "horizontal") + " identifiers", originalIdentifiers_oppositeMergeOrientation, Qt::Horizontal);

    mappingTable1->appendList("label of data origin", dataOriginLabel_oppositeMergeOrientation, Qt::Horizontal);

    emit objectAvailable(parameters->baseAnnotatedMatrices.first()->property("uuidProject").toUuid(), new Result("merged set of " + QString(parameters->orientation == Qt::Horizontal ? "vertical" : "horizontal") + " identifiers", mappingTable1));


    QList<QString> mergedIdentifiers_mergeOrientation = setOfMergedIdentifiers_mergeOrientation.values();

    std::sort(mergedIdentifiers_mergeOrientation.begin(), mergedIdentifiers_mergeOrientation.end());


    uuidProgress = this->startProgress("obtained indexes for merged " + QString(parameters->orientation == Qt::Horizontal ? "horizontal" : "vertical") + " identifiers", 0, parameters->baseAnnotatedMatrices.count() * mergedIdentifiers_mergeOrientation.count());

    qsizetype progressCounter = 0;

    AnnotatedMatrix<QVariant> *mappingTable2 = new AnnotatedMatrix<QVariant>;

    mappingTable2->appendList(QString(parameters->orientation == Qt::Horizontal ? "horizontal" : "vertical") + " identifiers", mergedIdentifiers_mergeOrientation, Qt::Horizontal);

    for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i) {

        BaseAnnotatedMatrix *baseAnnotatedMatrix = parameters->baseAnnotatedMatrices.at(i).data();

        QList<qsizetype> indexes;

        for (const QString &identifier : mergedIdentifiers_mergeOrientation) {

            QList<qsizetype> temp = baseAnnotatedMatrix->indexes(identifier, parameters->orientation, parameters->selectedOnly);

            indexes.append(temp.isEmpty() ? -1 : temp.first());

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, ++progressCounter);

        }

        indexes_mergeOrientation_perData.append(indexes);

        mappingTable2->appendList("mapped index in \"" + baseAnnotatedMatrix->property("label").toString() + "\"", indexes, Qt::Horizontal);

    }

    emit objectAvailable(parameters->baseAnnotatedMatrices.first()->property("uuidProject").toUuid(), new Result("merged set of " + QString(parameters->orientation == Qt::Horizontal ? "horizontal" : "vertical") + " identifiers", mappingTable2));

    this->stopProgress(uuidProgress);

    QList<QSharedPointer<AnnotatedMatrix<T>>> annotatedMatrices;

    for (const QSharedPointer<BaseAnnotatedMatrix> &baseAnnotatedMatrix : parameters->baseAnnotatedMatrices)
        annotatedMatrices.append(qSharedPointerDynamicCast<AnnotatedMatrix<T>>(baseAnnotatedMatrix));

    QList<T> values;

    if (parameters->exportDirectory.isEmpty())
        values.reserve(mergedIdentifiers_mergeOrientation.count() * mergedIdentifiers_oppositeMergeOrientation.count());

    QFile fileOut;

    QTextStream out;

    if (!parameters->exportDirectory.isEmpty()) {

        fileOut.setFileName(parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedData.tsv");

        if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

            this->reportError("could not open file \"" + parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedData.tsv" + "\"");

            QThread::currentThread()->requestInterruption();

            return;

        }

        out.setDevice(&fileOut);

    }


    if (parameters->orientation == Qt::Vertical) {

        if (!parameters->exportDirectory.isEmpty()) {

            for (const QString &mergedIdentifier_oppositeMergeOrientation : mergedIdentifiers_oppositeMergeOrientation)
                out << "\t" << mergedIdentifier_oppositeMergeOrientation;

            out << Qt::endl;

        }

        uuidProgress = this->startProgress("merging data in vertical orientation", 0, mergedIdentifiers_mergeOrientation.count());

        for (qsizetype i = 0; i < mergedIdentifiers_mergeOrientation.count(); ++i) {

            QList<T> valuesToAdd;

            for (qsizetype j = 0; j < annotatedMatrices.count(); ++j) {

                AnnotatedMatrix<T> *annotatedMatrix = annotatedMatrices.at(j).data();

                const QList<qsizetype> &indexes_oppositeMergeOrientation(indexes_oppositeMergeOrientation_perData.at(j));

                qsizetype index = indexes_mergeOrientation_perData.at(j).at(i);

                if (index == -1)
                    valuesToAdd << QList<T>(indexes_oppositeMergeOrientation.count(), ConversionFunctions::invalidValue<T>());
                else
                    valuesToAdd << annotatedMatrix->sliced({index}, indexes_oppositeMergeOrientation);

            }

            if (parameters->exportDirectory.isEmpty())
                values.append(std::move(valuesToAdd));
            else {

                out << mergedIdentifiers_mergeOrientation.at(i);

                for (const T &valueToAdd : valuesToAdd)
                    out << "\t" << valueToAdd;

                out << Qt::endl;

            }

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        this->stopProgress(uuidProgress);

        values.detach();

        if (parameters->exportDirectory.isEmpty())
            annotatedMatrix->setData(mergedIdentifiers_mergeOrientation, mergedIdentifiers_oppositeMergeOrientation, std::move(values));
        else
            fileOut.close();

    } else {

        if (!parameters->exportDirectory.isEmpty()) {

            for (const QString &mergedIdentifier_mergeOrientation : mergedIdentifiers_mergeOrientation)
                out << "\t" << mergedIdentifier_mergeOrientation;

            out << Qt::endl;

        }

        uuidProgress = this->startProgress("merging data in horizontal orientation", 0, mergedIdentifiers_oppositeMergeOrientation.count());

        qsizetype progressCounter = 0;

        for (qsizetype i = 0; i < annotatedMatrices.count(); ++i) {

            AnnotatedMatrix<T> *annotatedMatrix = annotatedMatrices.at(i).data();

            const QList<qsizetype> &indexes_mergeOrientation(indexes_mergeOrientation_perData.at(i));

            const QList<qsizetype> &indexes_oppositeMergeOrientation(indexes_oppositeMergeOrientation_perData.at(i));

            const QList<QString> &modifiedIdentifiers_oppositeMergeOrientation(modifiedIdentifiers_oppositeMergeOrientation_perData.at(i));

            for (qsizetype j = 0; j < indexes_oppositeMergeOrientation.count(); ++j) {

                std::span<T> rowInAnnotatedMatrixAtIndex = annotatedMatrix->operator[](indexes_oppositeMergeOrientation.at(j));

                QList<T> valuesToAdd;

                for (qsizetype index : indexes_mergeOrientation) {

                    if (index == -1)
                        valuesToAdd << ConversionFunctions::invalidValue<T>();
                    else
                        valuesToAdd << rowInAnnotatedMatrixAtIndex[index];

                }

                values.detach();

                if (parameters->exportDirectory.isEmpty())
                    values.append(std::move(valuesToAdd));
                else {

                    out << modifiedIdentifiers_oppositeMergeOrientation.at(j);

                    for (const T &valueToAdd : valuesToAdd)
                        out << "\t" << valueToAdd;

                    out << Qt::endl;

                }

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress, ++progressCounter);

            }
        }

        this->stopProgress(uuidProgress);

        if (parameters->exportDirectory.isEmpty())
            annotatedMatrix->setData(mergedIdentifiers_oppositeMergeOrientation, mergedIdentifiers_mergeOrientation, std::move(values));
        else
            fileOut.close();

    }

    if ((annotatedMatrix->rowCount() * annotatedMatrix->columnCount()) != annotatedMatrix->valueCount()) {

        this->reportError("error merging, row count " + QString::number(annotatedMatrix->rowCount()) + " and column count " + QString::number(annotatedMatrix->columnCount()) + " is not compatible with value count " + QString::number(annotatedMatrix->valueCount()));

        QThread::currentThread()->requestInterruption();

        return;

    }

    if (parameters->mergeColumnAnnotations) {

        if (!parameters->exportDirectory.isEmpty()) {

            fileOut.setFileName(parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedColumnAnnotations.tsv");

            if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

                this->reportError("could not open file \"" + parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedColumnAnnotations.tsv" + "\"");

                QThread::currentThread()->requestInterruption();

                return;

            }

            out.setDevice(&fileOut);

        }

        QList<QList<QString>> originalAnnotationLabelsPerData;

        QList<QList<QString>> modifiedAnnotationLabelsPerData;

        QList<QString> uniqueAnnotationLabels;

        for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i) {

            originalAnnotationLabelsPerData.append(parameters->baseAnnotatedMatrices.at(i)->annotationLabels(Qt::Horizontal, parameters->selectedOnly));

            modifiedAnnotationLabelsPerData.append(originalAnnotationLabelsPerData.last());

            if (parameters->addDataSpecificSuffixtoAnnotationLabels) {

                for (QString &annotationLabel : modifiedAnnotationLabelsPerData.last())
                    annotationLabel += "_" + QString::number(i + 1);

            }

            for (const QString &annotationLabel : modifiedAnnotationLabelsPerData.last()) {

                if (!uniqueAnnotationLabels.contains(annotationLabel))
                    uniqueAnnotationLabels.append(annotationLabel);

            }

        }

        if (!parameters->exportDirectory.isEmpty()) {

            for (const QString &uniqueAnnotationLabel : uniqueAnnotationLabels)
                out << "\t" << uniqueAnnotationLabel;

            out << Qt::endl;

        } else
            annotatedMatrix->appendAnnotationLabels(uniqueAnnotationLabels, Qt::Horizontal);


        QList<QString> identifiersToQueryAnnotationValues = (parameters->orientation == Qt::Horizontal) ? mergedIdentifiers_mergeOrientation : originalIdentifiers_oppositeMergeOrientation;

        QList<QString> identifiersToSetAnnotationValues = (parameters->orientation == Qt::Horizontal) ? mergedIdentifiers_mergeOrientation : mergedIdentifiers_oppositeMergeOrientation;

        uuidProgress = this->startProgress("merging column annotations", 0, identifiersToQueryAnnotationValues.count());

        for (qsizetype i = 0; i < identifiersToQueryAnnotationValues.count(); ++i) {

            const QString &identifierToQueryAnnotationValue(identifiersToQueryAnnotationValues.at(i));

            const QString &identifierToSetAnnotationValue(identifiersToSetAnnotationValues.at(i));

            if (!parameters->exportDirectory.isEmpty())
                out << identifierToSetAnnotationValue;

            QHash<QString, QVariant> annotationLabelToValue;

            for (qsizetype j = 0; j < parameters->baseAnnotatedMatrices.count(); ++j) {

                BaseAnnotatedMatrix *baseAnnotatedMatrix = parameters->baseAnnotatedMatrices.at(j).data();

                QList<QString> originalAnnotationLabels = originalAnnotationLabelsPerData.at(j);

                QList<QString> modifiedAnnotationLabels = modifiedAnnotationLabelsPerData.at(j);

                for (qsizetype k = 0; k < originalAnnotationLabels.count(); ++k) {

                    QVariant annotationValue;

                    if (baseAnnotatedMatrix->containsIdentifier(identifierToQueryAnnotationValue, Qt::Horizontal, parameters->selectedOnly))
                        annotationValue = baseAnnotatedMatrix->annotationValue(identifierToQueryAnnotationValue, originalAnnotationLabels.at(k), Qt::Horizontal);

                    QVariant annotationValueCurrent = annotatedMatrix->annotationValue(identifierToSetAnnotationValue, modifiedAnnotationLabels.at(k), Qt::Horizontal);

                    if (parameters->exportDirectory.isEmpty()) {

                        if (annotationValue.isValid()) {

                            if (!annotatedMatrix->setAnnotationValue(identifierToSetAnnotationValue, modifiedAnnotationLabels.at(k), annotationValue, Qt::Horizontal))
                                this->reportWarning("annotation value \"" + annotationValue.toString() + "\" for column annotation label \"" + modifiedAnnotationLabels.at(k) + "\" and identifier \"" + identifierToSetAnnotationValue + "\" overwrites current annotation value \"" + annotationValueCurrent.toString() + "\"");

                        }

                    } else {

                        if (annotationValueCurrent.isValid() && (annotationValueCurrent != annotationValue))
                            this->reportWarning("annotation value \"" + annotationValue.toString() + "\" for column annotation label \"" + modifiedAnnotationLabels.at(k) + "\" and identifier \"" + identifierToSetAnnotationValue + "\" overwrites current annotation value \"" + annotationValueCurrent.toString() + "\"");

                        annotationLabelToValue[modifiedAnnotationLabels.at(k)] = annotationValue;

                    }

                }

            }

            if (!parameters->exportDirectory.isEmpty()) {

                for (const QString &uniqueAnnotationLabel : uniqueAnnotationLabels)
                    out << "\t" << annotationLabelToValue.value(uniqueAnnotationLabel).toString();

                out << Qt::endl;
            }

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        if (!parameters->exportDirectory.isEmpty())
            fileOut.close();

        this->stopProgress(uuidProgress);

    }


    if (parameters->mergeRowAnnotations) {

        if (!parameters->exportDirectory.isEmpty()) {

            fileOut.setFileName(parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedRowAnnotations.tsv");

            if (!fileOut.open(QIODevice::WriteOnly | QIODevice::Text)) {

                this->reportError("could not open file \"" + parameters->exportDirectory + "/project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedRowAnnotations.tsv" + "\"");

                QThread::currentThread()->requestInterruption();

                return;

            }

            out.setDevice(&fileOut);

        }

        QList<QList<QString>> originalAnnotationLabelsPerData;

        QList<QList<QString>> modifiedAnnotationLabelsPerData;

        QList<QString> uniqueAnnotationLabels;

        for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i) {

            originalAnnotationLabelsPerData.append(parameters->baseAnnotatedMatrices.at(i)->annotationLabels(Qt::Vertical, parameters->selectedOnly));

            modifiedAnnotationLabelsPerData.append(originalAnnotationLabelsPerData.last());

            if (parameters->addDataSpecificSuffixtoAnnotationLabels) {

                for (QString &annotationLabel : modifiedAnnotationLabelsPerData.last())
                    annotationLabel += "_" + QString::number(i + 1);

            }

            for (const QString &annotationLabel : modifiedAnnotationLabelsPerData.last()) {

                if (!uniqueAnnotationLabels.contains(annotationLabel))
                    uniqueAnnotationLabels.append(annotationLabel);

            }

        }

        if (!parameters->exportDirectory.isEmpty()) {

            for (const QString &uniqueAnnotationLabel : uniqueAnnotationLabels)
                out << "\t" << uniqueAnnotationLabel;

            out << Qt::endl;

        } else
            annotatedMatrix->appendAnnotationLabels(uniqueAnnotationLabels, Qt::Vertical);


        QList<QString> identifiersToQueryAnnotationValues = (parameters->orientation == Qt::Vertical) ? mergedIdentifiers_mergeOrientation : originalIdentifiers_oppositeMergeOrientation;

        QList<QString> identifiersToSetAnnotationValues = (parameters->orientation == Qt::Vertical) ? mergedIdentifiers_mergeOrientation : mergedIdentifiers_oppositeMergeOrientation;

        uuidProgress = this->startProgress("merging row annotations", 0, identifiersToQueryAnnotationValues.count());

        for (qsizetype i = 0; i < identifiersToQueryAnnotationValues.count(); ++i) {

            const QString &identifierToQueryAnnotationValue(identifiersToQueryAnnotationValues.at(i));

            const QString &identifierToSetAnnotationValue(identifiersToSetAnnotationValues.at(i));

            if (!parameters->exportDirectory.isEmpty())
                out << identifierToSetAnnotationValue;

            QHash<QString, QVariant> annotationLabelToValue;

            for (qsizetype j = 0; j < parameters->baseAnnotatedMatrices.count(); ++j) {

                BaseAnnotatedMatrix *baseAnnotatedMatrix = parameters->baseAnnotatedMatrices.at(j).data();

                QList<QString> originalAnnotationLabels = originalAnnotationLabelsPerData.at(j);

                QList<QString> modifiedAnnotationLabels = modifiedAnnotationLabelsPerData.at(j);

                for (qsizetype k = 0; k < originalAnnotationLabels.count(); ++k) {

                    QVariant annotationValue;

                    if (baseAnnotatedMatrix->containsIdentifier(identifierToQueryAnnotationValue, Qt::Vertical, parameters->selectedOnly))
                        annotationValue = baseAnnotatedMatrix->annotationValue(identifierToQueryAnnotationValue, originalAnnotationLabels.at(k), Qt::Vertical);

                    QVariant annotationValueCurrent = annotatedMatrix->annotationValue(identifierToSetAnnotationValue, modifiedAnnotationLabels.at(k), Qt::Vertical);

                    if (parameters->exportDirectory.isEmpty()) {

                        if (annotationValue.isValid()) {

                            if (!annotatedMatrix->setAnnotationValue(identifierToSetAnnotationValue, modifiedAnnotationLabels.at(k), annotationValue, Qt::Vertical))
                                this->reportWarning("annotation value \"" + annotationValue.toString() + "\" for row annotation label \"" + modifiedAnnotationLabels.at(k) + "\" and identifier \"" + identifierToSetAnnotationValue + "\" overwrites current annotation value \"" + annotationValueCurrent.toString() + "\"");

                        }

                    } else {

                        if (annotationValueCurrent.isValid() && (annotationValueCurrent != annotationValue))
                            this->reportWarning("annotation value \"" + annotationValue.toString() + "\" for row annotation label \"" + modifiedAnnotationLabels.at(k) + "\" and identifier \"" + identifierToSetAnnotationValue + "\" overwrites current annotation value \"" + annotationValueCurrent.toString() + "\"");

                        annotationLabelToValue[modifiedAnnotationLabels.at(k)] = annotationValue;

                    }

                }

            }

            if (!parameters->exportDirectory.isEmpty()) {

                for (const QString &uniqueAnnotationLabel : uniqueAnnotationLabels)
                    out << "\t" << annotationLabelToValue.value(uniqueAnnotationLabel).toString();

                out << Qt::endl;
            }

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

        if (!parameters->exportDirectory.isEmpty())
            fileOut.close();

        this->stopProgress(uuidProgress);

    }


    if (parameters->exportDirectory.isEmpty()) {

        annotatedMatrix->setProperty("label", "project_" + parameters->baseAnnotatedMatrices.first()->property("projectLabel").toString() + "_mergedData");

        annotatedMatrix->moveToThread(QApplication::instance()->thread());

        emit objectAvailable(QUuid(), annotatedMatrix);

    }

    this->reportMessage("merged data contains " + QString::number(mergedIdentifiers_mergeOrientation.count()) + (parameters->orientation == Qt::Horizontal ? " horizontal" : " vertical") + " identifiers");

    this->reportMessage("merged data contains " + QString::number(mergedIdentifiers_oppositeMergeOrientation.count()) + (parameters->orientation == Qt::Horizontal ? " vertical" : " horizontal") + " identifiers");

}

#endif // MERGEDATAWORKER_H
