#include "fishersmetaanalysisapproach.h"

QList<QString> FishersMetaAnalysisApproach::d_testDescriptiveLabels = {"Chi-squared", "df", "Sig. (2-tailed)"};

FishersMetaAnalysisApproach::FishersMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

FishersMetaAnalysisApproach::FishersMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr),
    d_pValues(pValues),
    d_sampleSizes(sampleSizes)
{

}

void FishersMetaAnalysisApproach::calculateStatistic()
{

    d_statistic = 0;

    double chiSquared = 0.0;

    for (const double &pValue : d_pValues)
        chiSquared += std::log(pValue);

    d_statistic = -2.0 * chiSquared;

}

void FishersMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_pValues.count() * 2.0);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void FishersMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_pValues.count() * 2.0 << d_pValue;

}
