#ifndef SETENRICHMENTANALYSISDIALOG_H
#define SETENRICHMENTANALYSISDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>
#include <QInputDialog>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/selectmultiplefileswidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "widgets/doublespinboxwidget.h"
#include "workerclasses/setenrichmentanalysisworker.h"

class SetEnrichmentAnalysisDialog : public BaseDialog
{

public:

    SetEnrichmentAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

    ComboBoxWidget *d_comboBoxWidget_labelForMatching;

    ItemSelectorWidget *d_itemSelectorWidget_annotationLabelsForItems;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void dialogAccepted();

};

#endif // SETENRICHMENTANALYSISDIALOG_H
