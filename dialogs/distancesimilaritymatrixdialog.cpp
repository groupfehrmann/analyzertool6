#include "distancesimilaritymatrixdialog.h"

DistanceSimilarityMatrixDialog::DistanceSimilarityMatrixDialog(BaseDialog *parent, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices) :
    BaseDialog(parent, "distance matrix", "DistanceSimilarityMatrixDialog"),
    d_baseAnnotatedMatrices(baseAnnotatedMatrices)
{

    BaseDialog::d_parameters = QSharedPointer<DistanceSimilarityMatrixParameters>(new DistanceSimilarityMatrixParameters);

    BaseDialog::initWidgets();

    this->connect(this, &DistanceSimilarityMatrixDialog::accepted, this, &DistanceSimilarityMatrixDialog::dialogAccepted);

}

void DistanceSimilarityMatrixDialog::initWidgets_()
{

    QList<QString> baseAnnotatedMatrixLabels;

    for (qsizetype i = 0; i < d_baseAnnotatedMatrices.count(); ++i)
        baseAnnotatedMatrixLabels.append(d_baseAnnotatedMatrices.at(i)->property("label").toString());


    ComboBoxWidget *comboBoxWidget_data1 = new ComboBoxWidget(this, "comboBoxWidget_data1");

    this->connect(comboBoxWidget_data1->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->data1 = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_data1->label()->setText("select data 1");

    comboBoxWidget_data1->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_data1, 0, 0, 1, 2);


    d_orientationWidget_data1 = new OrientationWidget(this, "d_orientationWidget_data1");

    d_orientationWidget_data1->label()->setText("select orientation of data 1");

    this->connect(d_orientationWidget_data1, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->orientation_data1 = orientation;});

    d_orientationWidget_data1->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_data1, 1, 0, 1, 2);


    d_itemSelectorWidget_variables_data1 = new ItemSelectorWidget(this, "d_itemSelectorWidget_variables_data1");

    d_itemSelectorWidget_variables_data1->label()->setText("select variables in data 1");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables_data1, 2, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables_data1->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers_data1 = itemLabels; static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes_data1 = itemIndexes;});


    d_itemSelectorWidget_items_data1 = new ItemSelectorWidget(this, "d_itemSelectorWidget_items_data1");

    d_itemSelectorWidget_items_data1->label()->setText("select items in data 1");

    d_gridLayout->addWidget(d_itemSelectorWidget_items_data1, 2, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items_data1->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers_data1 = itemLabels; static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes_data1 = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_data2 = new ComboBoxWidget(this, "comboBoxWidget_data2");

    this->connect(comboBoxWidget_data2->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->data2 = d_baseAnnotatedMatrices.at(index);});

    comboBoxWidget_data2->label()->setText("select data 2");

    comboBoxWidget_data2->comboBox()->addItems(baseAnnotatedMatrixLabels);

    d_gridLayout->addWidget(comboBoxWidget_data2, 3, 0, 1, 2);


    d_orientationWidget_data2 = new OrientationWidget(this, "d_orientationWidget_data2");

    d_orientationWidget_data2->label()->setText("select orientation of data 2");

    this->connect(d_orientationWidget_data2, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->orientation_data2 = orientation;});

    d_orientationWidget_data2->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget_data2, 4, 0, 1, 2);


    d_itemSelectorWidget_variables_data2 = new ItemSelectorWidget(this, "d_itemSelectorWidget_variables_data2");

    d_itemSelectorWidget_variables_data2->label()->setText("select variables in data 2");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables_data2, 5, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables_data2->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers_data2 = itemLabels; static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes_data2 = itemIndexes;});


    d_itemSelectorWidget_items_data2 = new ItemSelectorWidget(this, "d_itemSelectorWidget_items_data2");

    d_itemSelectorWidget_items_data2->label()->setText("select items in data 2");

    d_gridLayout->addWidget(d_itemSelectorWidget_items_data2, 5, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items_data2->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers_data2 = itemLabels; static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes_data2 = itemIndexes;});


    ComboBoxWidget *comboBoxWidget_distanceFunction = new ComboBoxWidget(this, "comboBoxWidget_distanceFunction");

    this->connect(comboBoxWidget_distanceFunction->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_distanceFunction](){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->distanceFunction = comboBoxWidget_distanceFunction->comboBox()->currentText();});

    comboBoxWidget_distanceFunction->label()->setText("distance function");

    comboBoxWidget_distanceFunction->comboBox()->addItems({"covariance", "Pearson R", "Spearman R", "distance covariance", "distance correlation", "Chatterjee correlation", "dot product", "count overlap for ABS(value) > threshold", "Pearson R for union ABS(value) > threshold", "rank-biased overlap"});

    comboBoxWidget_distanceFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_distanceFunction, 6, 0, 1, 2);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 7, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 8, 0, 1, 2);


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"distance/similarity matrix",  "distance/similarity diagonal"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 12, 0, 1, 2);


    this->connect(d_orientationWidget_data1, &OrientationWidget::orientationChanged, this, &DistanceSimilarityMatrixDialog::orientation_data1_changed);

    this->connect(comboBoxWidget_data1->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientation_data1_changed(d_orientationWidget_data1->orientation());});

    this->orientation_data1_changed(d_orientationWidget_data1->orientation());


    this->connect(d_orientationWidget_data2, &OrientationWidget::orientationChanged, this, &DistanceSimilarityMatrixDialog::orientation_data2_changed);

    this->connect(comboBoxWidget_data2->comboBox(), &QComboBox::currentIndexChanged, [this](int index){Q_UNUSED(index); this->orientation_data2_changed(d_orientationWidget_data2->orientation());});

    this->orientation_data2_changed(d_orientationWidget_data2->orientation());

}

void DistanceSimilarityMatrixDialog::orientation_data1_changed(Qt::Orientation orientation)
{

    DistanceSimilarityMatrixParameters *distanceSimilarityMatrixParameters = static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables_data1->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data1->nonSelectedIdentifiers(orientation), distanceSimilarityMatrixParameters->data1->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables_data1->itemSelectorModel_selected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data1->identifiers(orientation, true), distanceSimilarityMatrixParameters->data1->indexes(orientation, true));


    d_itemSelectorWidget_items_data1->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data1->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), distanceSimilarityMatrixParameters->data1->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items_data1->itemSelectorModel_selected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data1->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), distanceSimilarityMatrixParameters->data1->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void DistanceSimilarityMatrixDialog::orientation_data2_changed(Qt::Orientation orientation)
{

    DistanceSimilarityMatrixParameters *distanceSimilarityMatrixParameters = static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables_data2->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data2->nonSelectedIdentifiers(orientation), distanceSimilarityMatrixParameters->data2->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables_data2->itemSelectorModel_selected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data2->identifiers(orientation, true), distanceSimilarityMatrixParameters->data2->indexes(orientation, true));


    d_itemSelectorWidget_items_data2->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data2->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), distanceSimilarityMatrixParameters->data2->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items_data2->itemSelectorModel_selected()->setItemLabelsAndIndexes(distanceSimilarityMatrixParameters->data2->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), distanceSimilarityMatrixParameters->data2->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void DistanceSimilarityMatrixDialog::dialogAccepted()
{

    DistanceSimilarityMatrixParameters *distanceSimilarityMatrixParameters = static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data());

    if ((distanceSimilarityMatrixParameters->distanceFunction == "count overlap for ABS(value) > threshold") || (distanceSimilarityMatrixParameters->distanceFunction == "Pearson R for union ABS(value) > threshold")) {

        double threshold = 3.0;

        QSettings settings;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("thresholdForOverlapBasedComparisions"))
           threshold = settings.value("thresholdForOverlapBasedComparisions").toDouble();

        settings.endGroup();

        threshold = QInputDialog::getDouble(this, QCoreApplication::applicationName() + " - input dialog", "select threshold for use for function \"" + distanceSimilarityMatrixParameters->distanceFunction + "\"", threshold, 0.0);

        static_cast<DistanceSimilarityMatrixParameters *>(BaseDialog::d_parameters.data())->thresholdForOverlapBasedComparisions = threshold;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("thresholdForOverlapBasedComparisions", threshold);

        settings.endGroup();

    }

    if (distanceSimilarityMatrixParameters->distanceFunction == "rank-biased overlap") {

        double p_rankBiasedOverlap = 0.9;

        QSettings settings;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("p_rankBiasedOverlap"))
           p_rankBiasedOverlap = settings.value("p_rankBiasedOverlap").toDouble();

        settings.endGroup();

        p_rankBiasedOverlap = QInputDialog::getDouble(this, QCoreApplication::applicationName() + " - input dialog", "select p parameter for rank-biased overlap", p_rankBiasedOverlap, 0.0, 1.0, 2);

        distanceSimilarityMatrixParameters->p_rankBiasedOverlap = p_rankBiasedOverlap;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("p_rankBiasedOverlap", p_rankBiasedOverlap);

        settings.endGroup();


        int maxDepth_rankBiasedOverlap = 250;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("maxDepth_rankBiasedOverlap"))
           maxDepth_rankBiasedOverlap = settings.value("maxDepth_rankBiasedOverlap").toInt();

        settings.endGroup();

        maxDepth_rankBiasedOverlap = QInputDialog::getInt(this, QCoreApplication::applicationName() + " - input dialog", "select maximum depth for rank-biased overlap", maxDepth_rankBiasedOverlap, 1);

        distanceSimilarityMatrixParameters->maxDepth_rankBiasedOverlap = maxDepth_rankBiasedOverlap;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("maxDepth_rankBiasedOverlap", maxDepth_rankBiasedOverlap);

        settings.endGroup();

    }

}
