#include "basedialog.h"
#include "ui_basedialog.h"

BaseDialog::BaseDialog(QWidget *parent, const QString &dialogTitle, const QString &dialogSettingsIdentifier) :
    QDialog(parent),
    ui(new Ui::BaseDialog),
    d_dialogSettingsIdentifier(dialogSettingsIdentifier)
{

    ui->setupUi(this);

    this->setWindowTitle(QCoreApplication::applicationName() + " - " + dialogTitle);

    QSettings settings;

    settings.beginGroup(d_dialogSettingsIdentifier);

    if (settings.contains("geometry"))
        this->restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();

}

const QString &BaseDialog::dialogSettingsIdentifier() const
{

    return d_dialogSettingsIdentifier;

}

void BaseDialog::initWidgets_()
{

}

void BaseDialog::initWidgets()
{

    d_gridLayout = new QGridLayout;

    this->initWidgets_();

    d_gridLayout->addWidget(ui->buttonBox, d_gridLayout->rowCount(), 0, -1, -1, Qt::AlignRight | Qt::AlignBottom);

    this->setLayout(d_gridLayout);

}

const QSharedPointer<BaseParameters> &BaseDialog::parameters() const
{

    return d_parameters;

}

BaseDialog::~BaseDialog()
{

    QSettings settings;

    settings.beginGroup(d_dialogSettingsIdentifier);

    settings.setValue("geometry", this->saveGeometry());

    settings.endGroup();

    delete ui;

}
