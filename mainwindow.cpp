#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    this->setupMainWindow();

    this->readApplicationSettings();

    this->setWindowTitle(QCoreApplication::applicationName());

    ui->statusbar->setStyleSheet("QStatusBar{color:red}");

    ui->statusbar->showMessage(QCoreApplication::applicationName() + " - Last modified on " + QCoreApplication::applicationVersion() + " - Developed by Rudolf S.N. Fehrmann", 10000);

}

void MainWindow::readApplicationSettings()
{

    QSettings settings;

    settings.beginGroup("mainWindow");

    if (settings.contains("state"))
        this->restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        this->restoreGeometry(settings.value("geometry").toByteArray());
    else
        this->showMaximized();

    if (settings.contains("splitter_horizontal/state"))
        ui->splitter_horizontal->restoreState(settings.value("splitter_horizontal/state").toByteArray());
    else
        ui->splitter_horizontal->setSizes({80, 20});

    if (settings.contains("splitter_vertical/state"))
        ui->splitter_vertical->restoreState(settings.value("splitter_vertical/state").toByteArray());

    if (settings.contains("tabWidget_bottom/currentIndex"))
        ui->tabWidget_bottom->setCurrentIndex(settings.value("tabWidget_bottom/currentIndex").toInt());

    if (settings.contains("d_recentlyOpenedLabel") && settings.contains("d_recentlyOpenedPathToFile") && settings.contains("d_recentlyOpenedObjectType")) {

        d_recentlyOpenedLabel = settings.value("d_recentlyOpenedLabel").toStringList();

        d_recentlyOpenedPathToFile = settings.value("d_recentlyOpenedPathToFile").toStringList();

        d_recentlyOpenedObjectType = settings.value("d_recentlyOpenedObjectType").toStringList();

        this->updateRecentlyOpenedActions();

    }

    if (settings.contains("actionrun_workers_sequentially/checked"))
        ui->actionrun_workers_sequentially->setChecked(settings.value("actionrun_workers_sequentially/checked").toBool());

    if (settings.contains("actionenable_delay_mode/checked"))
        ui->actionenable_delay_mode->setChecked(settings.value("actionenable_delay_mode/checked").toBool());


}

QString MainWindow::selectFile(QFileDialog::AcceptMode acceptMode, const QString &directory, const QString &suggestedFileName, const QStringList &filters, const QString &defaultSuffix) const
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - select file");

    fileDialog.setAcceptMode(acceptMode);

    if (!defaultSuffix.isEmpty())
        fileDialog.setDefaultSuffix(defaultSuffix);

    QSettings settings;

    settings.beginGroup("MainWindow/fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (!directory.isEmpty())
        fileDialog.setDirectory(directory);
    else if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    if (!suggestedFileName.isEmpty())
        fileDialog.selectFile(suggestedFileName);

    if (!filters.isEmpty())
        fileDialog.setNameFilters(filters);

    settings.endGroup();

    if (fileDialog.exec()) {

        QSettings settings;

        settings.beginGroup("MainWindow/fileDialog");

        settings.setValue("state", fileDialog.saveState());

        settings.setValue("geometry", fileDialog.saveGeometry());

        settings.setValue("lastDirectory", fileDialog.directory().path());

        settings.endGroup();

        return fileDialog.selectedFiles().first();
    }

    return QString();

}

void MainWindow::setupDataRepositoryWidget()
{

    d_dataRepository = QSharedPointer<DataRepository>(new DataRepository);

    d_dataRepositoryModel = QSharedPointer<DataRepositoryModel>(new DataRepositoryModel(nullptr, d_dataRepository));

    ui->widget_dataRepository->setDataRepositoryModel(d_dataRepositoryModel.data());

    this->connect(d_controller.data(), &Controller::objectAvailable, d_dataRepositoryModel.data(), &DataRepositoryModel::addObject);

    this->connect(d_dataRepositoryModel.data(), &DataRepositoryModel::objectAdded, ui->stackedWidget_objectViewer, &StackedObjectWidget::addObject);

    this->connect(d_dataRepositoryModel.data(), &DataRepositoryModel::objectRemoved, ui->stackedWidget_objectViewer, &StackedObjectWidget::removeObject);

    this->connect(d_dataRepositoryModel.data(), &DataRepositoryModel::currentUuidChanged, [this](const QUuid &uuid) {

        ui->stackedWidget_objectViewer->showObject(uuid);

        QString label = d_dataRepository->label(uuid);

        if (label.isEmpty()) {

            ui->actionclose->setText("close");

            ui->actionclose->setEnabled(false);

            ui->actionconvert_to_data->setText("convert to data");

            ui->actionconvert_to_data->setEnabled(false);

            return;

        }

        ui->actionclose->setText("close \"" + label + "\"");

        ui->actionclose->setEnabled(true);

        QSharedPointer<QObject> object = d_dataRepository->objectPointer(uuid);

        if (object) {

            if (object->property("resultType").toString() == "table") {

                ui->actionconvert_to_data->setText("convert \"" + label + "\" to data");

                ui->actionconvert_to_data->setEnabled(true);

            } else {

                ui->actionconvert_to_data->setText("convert to data");

                ui->actionconvert_to_data->setEnabled(false);

            };

            return;

        }

    });

    this->connect(d_dataRepositoryModel.data(), &DataRepositoryModel::dataChanged, [this](const QModelIndex &topLeft, const QModelIndex &bottomRight, const QList<int> &roles) {

        Q_UNUSED(bottomRight);

        Q_UNUSED(roles);

        if (!topLeft.isValid()) {

            ui->actionclose->setText("close");

            ui->actionclose->setEnabled(false);

            ui->actionconvert_to_data->setText("convert to data");

            ui->actionconvert_to_data->setEnabled(false);

            return;

        }

        QString label = d_dataRepositoryModel->data(topLeft).toString();

        ui->actionclose->setText("close \"" + label + "\"");

        QUuid uuid = *static_cast<const QUuid *>(topLeft.internalPointer());

        if (d_dataRepository->isUuidProject(uuid))
            return;

        if (d_dataRepository->objectPointer(uuid)->property("resultType").toString() == "table") {

            ui->actionconvert_to_data->setText("convert \"" + label + "\" to data");

            ui->actionconvert_to_data->setEnabled(true);

        } else {

            ui->actionconvert_to_data->setText("convert to data");

            ui->actionconvert_to_data->setEnabled(false);

        };

    });

}

void MainWindow::setupLogMonitorWidget()
{

    d_logMonitor = QSharedPointer<LogMonitor>(new LogMonitor);

    d_logMonitorModel = QSharedPointer<LogMonitorModel>(new LogMonitorModel(nullptr, d_logMonitor));

    this->connect(d_controller.data(), &Controller::errorReported, d_logMonitorModel.data(), &LogMonitorModel::reportError);

    this->connect(d_controller.data(), &Controller::messageReported, d_logMonitorModel.data(), &LogMonitorModel::reportMessage);

    this->connect(d_controller.data(), &Controller::warningReported, d_logMonitorModel.data(), &LogMonitorModel::reportWarning);

    ui->tab_logMonitor->setLogMonitorModel(d_logMonitorModel.data());

}

void MainWindow::setupMainWindow()
{

    d_controller = QSharedPointer<Controller>(new Controller());

    this->connect(ui->actionrun_workers_sequentially, &QAction::toggled, d_controller.data(), &Controller::runWorkersSequentially);

    this->connect(ui->actionenable_delay_mode, &QAction::toggled, d_controller.data(), &Controller::enableDelayMode);

    this->setupDataRepositoryWidget();

    this->setupLogMonitorWidget();

    this->setupProgressMonitorWidget();

    this->setupSplashScreen();

    this->setupRecentlyOpenedActions();

}

void MainWindow::setupProgressMonitorWidget()
{

    d_progressMonitor = QSharedPointer<ProgressMonitor>(new ProgressMonitor);

    d_progressMonitorModel = QSharedPointer<ProgressMonitorModel>(new ProgressMonitorModel(nullptr, d_progressMonitor));

    this->connect(d_controller.data(), &Controller::workerQueued, d_progressMonitorModel.data(), &ProgressMonitorModel::putWorkerInQueu);

    this->connect(d_controller.data(), &Controller::workerStarted, d_progressMonitorModel.data(), &ProgressMonitorModel::startMonitoringWorker);

    this->connect(d_controller.data(), &Controller::workerInterrupted, d_progressMonitorModel.data(), &ProgressMonitorModel::stopAllMonitoringAssociatedWithUuid);

    this->connect(d_controller.data(), &Controller::workerFinished, d_progressMonitorModel.data(), &ProgressMonitorModel::stopAllMonitoringAssociatedWithUuid);

    this->connect(d_controller.data(), &Controller::progressStarted, d_progressMonitorModel.data(), &ProgressMonitorModel::startProgress);

    this->connect(d_controller.data(), &Controller::progressUpdated, d_progressMonitorModel.data(), &ProgressMonitorModel::updateProgress);

    this->connect(d_controller.data(), &Controller::progressLabelUpdated, d_progressMonitorModel.data(), &ProgressMonitorModel::updateProgressLabel);

    this->connect(d_controller.data(), &Controller::progressUpdatedWithOne, d_progressMonitorModel.data(), &ProgressMonitorModel::updateProgressWithOne);

    this->connect(d_controller.data(), &Controller::progressStopped, d_progressMonitorModel.data(), &ProgressMonitorModel::stopAllMonitoringAssociatedWithUuid);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::interruptionRequested, d_controller.data(), &Controller::requestInterruption);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::pauseRequested, d_controller.data(), &Controller::pauseWorker);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::resumeRequested, d_controller.data(), &Controller::resumeWorker);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::interruptionRequested, d_progressMonitor.data(), &ProgressMonitor::workerIsInterruption);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::pauseRequested, d_progressMonitor.data(), &ProgressMonitor::workerIsPaused);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::resumeRequested, d_progressMonitor.data(), &ProgressMonitor::workerIsResumed);

    this->connect(d_progressMonitorModel.data(), &ProgressMonitorModel::messageReported, d_logMonitorModel.data(), &LogMonitorModel::reportMessage);

    ui->tab_progressMonitor->setProgressMonitorModel(d_progressMonitorModel.data());

}

void MainWindow::setupRecentlyOpenedActions()
{

    for (int i = 0; i < 10; ++i) {

        d_recentlyOpenedActions.append(new QAction(this));

        d_recentlyOpenedActions.last()->setVisible(false);

       this->connect(d_recentlyOpenedActions.last(), &QAction::triggered, this, &MainWindow::openRecentlyOpened_project_data_result);

        ui->menurecently_opened->addAction(d_recentlyOpenedActions.last());

    }

    ui->menurecently_opened->addSeparator();

    d_clearRecentlyOpenedFilesAction = new QAction("Clear recently opened", this);

    this->connect(d_clearRecentlyOpenedFilesAction, &QAction::triggered, [this](){d_recentlyOpenedLabel.clear(); d_recentlyOpenedPathToFile.clear(); d_recentlyOpenedObjectType.clear(); this->updateRecentlyOpenedActions();});

    ui->menurecently_opened->addAction(d_clearRecentlyOpenedFilesAction);

}

void MainWindow::setupSplashScreen()
{

    QWidget *widget = ui->stackedWidget_splashScreen;

    QVBoxLayout *layout = new QVBoxLayout(widget);

    QLabel *label1 = new QLabel(widget);

    label1->setText("AnalyzerTool");

    label1->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    label1->setStyleSheet("font: 25 144pt \"Myriad Pro\";");

    QLabel *label2 = new QLabel(widget);

    label2->setText("Developed by R.S.N. Fehrmann");

    label2->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    label2->setStyleSheet("font: 36pt \"Myriad Pro\"; color: rgb(128, 0, 0);");

    QLabel *label3 = new QLabel(widget);

    label3->setText("Last modified on " + QCoreApplication::applicationVersion());

    label3->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    label3->setStyleSheet("font: 24pt \"Myriad Pro\";");

    widget->setLayout(layout);

    layout->addWidget(label1);

    layout->addWidget(label2);

    layout->addWidget(label3);

}

void MainWindow::updateRecentlyOpenedActions()
{

    QMutableListIterator<QString> it(d_recentlyOpenedPathToFile);

    while (it.hasNext()) {

        it.next();

        if (!QFile::exists(it.value()))
            it.remove();

    }

    for (int i = 0; i < 10; ++i) {

        if (i < d_recentlyOpenedPathToFile.count()) {

            d_recentlyOpenedActions[i]->setText(d_recentlyOpenedObjectType.at(i) + " - \"" + d_recentlyOpenedLabel.at(i) + "\" - \"" + d_recentlyOpenedPathToFile.at(i) + "\"");

            d_recentlyOpenedActions[i]->setProperty("pathToFile", d_recentlyOpenedPathToFile.at(i));

            d_recentlyOpenedActions[i]->setVisible(true);

        } else
            d_recentlyOpenedActions[i]->setVisible(false);

    }

}

void MainWindow::writeApplicationSettings()
{

    QSettings settings;

    if (ui->actionreset_settings_at_next_startup->isChecked()) {

        settings.clear();

        return;

    }

    settings.beginGroup("mainWindow");

    settings.setValue("state", this->saveState());

    settings.setValue("geometry", this->saveGeometry());

    settings.setValue("splitter_horizontal/state", ui->splitter_horizontal->saveState());

    settings.setValue("splitter_vertical/state", ui->splitter_vertical->saveState());

    settings.setValue("tabWidget_bottom/currentIndex", ui->tabWidget_bottom->currentIndex());

    settings.setValue("d_recentlyOpenedLabel", d_recentlyOpenedLabel);

    settings.setValue("d_recentlyOpenedPathToFile", d_recentlyOpenedPathToFile);

    settings.setValue("d_recentlyOpenedObjectType", d_recentlyOpenedObjectType);

    settings.setValue("actionrun_workers_sequentially/checked", ui->actionrun_workers_sequentially->isChecked());

    settings.setValue("actionenable_delay_mode/checked", ui->actionenable_delay_mode->isChecked());

}

void MainWindow::addToRecentlyOpenedFiles(const QString &objectType, const QString &label, const QString &pathToFile)
{

    for (int i = d_recentlyOpenedPathToFile.size() - 1; i >= 0; --i) {

        if (d_recentlyOpenedPathToFile.at(i) == pathToFile) {

            d_recentlyOpenedLabel.removeAt(i);

            d_recentlyOpenedPathToFile.removeAt(i);

            d_recentlyOpenedObjectType.removeAt(i);

        }
    }

    d_recentlyOpenedLabel.prepend(label);

    d_recentlyOpenedPathToFile.prepend(pathToFile);

    d_recentlyOpenedObjectType.prepend(objectType);

    this->updateRecentlyOpenedActions();

}

void MainWindow::on_actionclose_all_triggered()
{

    d_dataRepositoryModel->removeAll();

    ui->stackedWidget_objectViewer->removeAll();

}

void MainWindow::on_actionclose_triggered()
{

    QUuid uuid = d_dataRepositoryModel->currentUuid();

    d_dataRepositoryModel->removeAllDataAssociatedWithUuid(uuid);

}

void MainWindow::on_actionconvert_to_data_triggered()
{

    QUuid uuid = d_dataRepositoryModel->currentUuid();

    d_dataRepositoryModel->convertToData(uuid);

}

void MainWindow::on_actioncolumn_annotations_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not import row annotations, no data selected", 10000);

        return;

    }

    AnnotationsFromASingleFileDialog dataFromASingleFileDialog(Qt::Horizontal, baseAnnotatedMatrix);

    if (!dataFromASingleFileDialog.exec())
        return;

    d_controller->startWorker(new AnnotationsFromASingleFileWorker(nullptr, dataFromASingleFileDialog.parameters()));

}

void MainWindow::on_actiondata_from_a_single_file_triggered()
{

    DataFromASingleFileDialog dataFromASingleFileDialog;

    if (!dataFromASingleFileDialog.exec())
        return;

    d_controller->startWorker(new DataFromASingleFileWorker(nullptr, dataFromASingleFileDialog.parameters()));

}

void MainWindow::on_actionexport_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (currentUuid == QUuid()) {

        ui->statusbar->showMessage("can not export data or result, nothing selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    } else if (d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("can not export data or result, project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    QSharedPointer<QObject> objectPointer = d_dataRepository->objectPointer(currentUuid);

    d_dataRepository->readWriteLock().unlock();

    ExportDialog exportDialog(nullptr, objectPointer);

    if (!exportDialog.exec())
        return;

    d_controller->startWorker(new ExportWorker(nullptr, exportDialog.parameters()));

}

void MainWindow::on_actionindependent_component_analysis_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not perform independent component analysis, no data selected", 10000);

        return;

    }

    IndependentComponentAnalysisDialog independentComponentAnalysisDialog(baseAnnotatedMatrix);

    if (!independentComponentAnalysisDialog.exec())
        return;

    d_controller->startWorker(new IndependentComponentAnalysisWorker(nullptr, independentComponentAnalysisDialog.parameters()));

}

void MainWindow::on_actionproject_data_on_independent_components_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (!d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("can not project data on independent components, no project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    d_dataRepository->readWriteLock().unlock();

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    for (const QUuid &uuid : d_dataRepository->uuidsAnnotatedMatrix(currentUuid))
        baseAnnotatedMatrices.append(qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(uuid)));

    d_dataRepository->readWriteLock().unlock();

    ProjectDataOnIndependentComponentsDialog projectDataOnIndependentComponentsDialog(nullptr, baseAnnotatedMatrices);

    if (!projectDataOnIndependentComponentsDialog.exec())
        return;

    d_controller->startWorker(new ProjectDataOnIndependentComponentsWorker(nullptr, projectDataOnIndependentComponentsDialog.parameters()));

}

void MainWindow::on_actionprincipal_component_analysis_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not perform principal component analysis, no data selected", 10000);

        return;

    }

    PrincipalComponentAnalysisDialog principalComponentAnalysisDialog(baseAnnotatedMatrix);

    if (!principalComponentAnalysisDialog.exec())
        return;

    d_controller->startWorker(new PrincipalComponentAnalysisWorker(nullptr, principalComponentAnalysisDialog.parameters()));

}

void MainWindow::on_actionmerge_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (!d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("can not merge data, no project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    d_dataRepository->readWriteLock().unlock();

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    for (const QUuid &uuid : d_dataRepository->uuidsAnnotatedMatrix(currentUuid))
        baseAnnotatedMatrices.append(qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(uuid)));

    d_dataRepository->readWriteLock().unlock();

    MergeDataDialog mergeDataDialog(nullptr, baseAnnotatedMatrices);

    if (!mergeDataDialog.exec())
        return;

    d_controller->startWorker(new MergeDataWorker(nullptr, mergeDataDialog.parameters()));

}

void MainWindow::on_actionnew_project_triggered()
{

    d_dataRepositoryModel->addProject();

}

void MainWindow::on_actionopen_project_data_result_triggered()
{

    OpenProjectDataResultParameters *openProjectDataResultParameters = new OpenProjectDataResultParameters;

    openProjectDataResultParameters->pathToFile = this->selectFile(QFileDialog::AcceptOpen, QString(), QString(), {"AnalyzerTool (*.Atool)"}, "Atool");

    if (openProjectDataResultParameters->pathToFile.isEmpty()) {

        delete openProjectDataResultParameters;

        return;

    }

    OpenProjectDataResultWorker *openProjectDataResultWorker = new OpenProjectDataResultWorker(nullptr, QSharedPointer<BaseParameters>(openProjectDataResultParameters));

    this->connect(openProjectDataResultWorker, &OpenProjectDataResultWorker::addToRecentlyOpenedFiles, this, &MainWindow::addToRecentlyOpenedFiles);

    d_controller->startWorker(openProjectDataResultWorker);

}

void MainWindow::openRecentlyOpened_project_data_result()
{

    QString pathToFile = qobject_cast<QAction *>(sender())->property("pathToFile").toString();

    OpenProjectDataResultParameters *openProjectDataResultParameters = new OpenProjectDataResultParameters;

    openProjectDataResultParameters->pathToFile = pathToFile;

    if (openProjectDataResultParameters->pathToFile.isEmpty()) {

        delete openProjectDataResultParameters;

        return;

    }

    d_controller->startWorker(new OpenProjectDataResultWorker(nullptr, QSharedPointer<BaseParameters>(openProjectDataResultParameters)));

}

void MainWindow::on_actionrow_annotations_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not import row annotations, no data selected", 10000);

        return;

    }

    AnnotationsFromASingleFileDialog dataFromASingleFileDialog(Qt::Vertical, baseAnnotatedMatrix);

    if (!dataFromASingleFileDialog.exec())
        return;

    d_controller->startWorker(new AnnotationsFromASingleFileWorker(nullptr, dataFromASingleFileDialog.parameters()));

}

void MainWindow::on_actionsave_project_data_result_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (currentUuid == QUuid()) {

        ui->statusbar->showMessage("could not save project/data/result, nothing selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    QList<QSharedPointer<QObject>> objectPointers;

    QString suggestedFileName;

    SaveProjectDataResultParameters *saveProjectDataResultParameters = new SaveProjectDataResultParameters;

    if (d_dataRepository->isUuidProject(currentUuid)) {

        saveProjectDataResultParameters->savingProject = true;

        saveProjectDataResultParameters->projectLabel = d_dataRepository->projectLabel(d_dataRepository->indexOfUuidProject(currentUuid));

        objectPointers = d_dataRepository->objectPointers(currentUuid);

        suggestedFileName = d_dataRepository->projectLabel(d_dataRepository->indexOfUuidProject(currentUuid));

        if (objectPointers.isEmpty()) {

            ui->statusbar->showMessage("could not save project/data/result, project is empty", 10000);

            delete saveProjectDataResultParameters;

            return;

        }

    } else {

        saveProjectDataResultParameters->savingProject = false;

        objectPointers.append(d_dataRepository->objectPointer(currentUuid));

        suggestedFileName = objectPointers.first()->property("label").toString();

    }

    d_dataRepository->readWriteLock().unlock();

    saveProjectDataResultParameters->pathToFile = this->selectFile(QFileDialog::AcceptSave, QString(), suggestedFileName, {"AnalyzerTool (*.Atool)"}, "Atool");

    if (saveProjectDataResultParameters->pathToFile.isEmpty()) {

        delete saveProjectDataResultParameters;

        return;

    }

    saveProjectDataResultParameters->objectPointers = objectPointers;

    SaveProjectDataResultWorker *saveProjectDataResultWorker = new SaveProjectDataResultWorker(nullptr, QSharedPointer<BaseParameters>(saveProjectDataResultParameters));

    this->connect(saveProjectDataResultWorker, &SaveProjectDataResultWorker::addToRecentlyOpenedFiles, this, &MainWindow::addToRecentlyOpenedFiles);

    d_controller->startWorker(saveProjectDataResultWorker);

}

void MainWindow::on_actionset_enrichment_analysis_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not perform set enrichment analysis, no data selected", 10000);

        return;

    }

    SetEnrichmentAnalysisDialog setEnrichmentAnalysisDialog(baseAnnotatedMatrix);

    if (!setEnrichmentAnalysisDialog.exec())
        return;

    d_controller->startWorker(new SetEnrichmentAnalysisWorker(nullptr, setEnrichmentAnalysisDialog.parameters()));

}

void MainWindow::on_actionguilt_by_association_analysis_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not perform guilt-by-association analysis, no data selected", 10000);

        return;

    }

    GuiltByAssociationAnalysisDialog guiltByAssociationAnalysisDialog(baseAnnotatedMatrix);

    if (!guiltByAssociationAnalysisDialog.exec())
        return;

    d_controller->startWorker(new GuiltByAssociationAnalysisWorker(nullptr, guiltByAssociationAnalysisDialog.parameters()));

}

void MainWindow::on_actiontranspose_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not transpose, no data selected", 10000);

        return;

    }

    TransposeDataParameters *transposeDataParameters = new TransposeDataParameters;

    transposeDataParameters->baseAnnotatedMatrix = baseAnnotatedMatrix;

    d_controller->startWorker(new TransposeDataWorker(nullptr, QSharedPointer<BaseParameters>(transposeDataParameters)));

}

void MainWindow::on_actionwith_tokens_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not select data with tokens, no data selected", 10000);

        return;

    }

    SelectDataWithTokensDialog selectDataWithTokensDialog(baseAnnotatedMatrix);

    if (!selectDataWithTokensDialog.exec())
        return;

    d_controller->startWorker(new SelectDataWithTokensWorker(nullptr, selectDataWithTokensDialog.parameters()));

}

void MainWindow::on_actionusing_predefined_metrics_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (!d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("can not transform data with predefined metrics, no project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    d_dataRepository->readWriteLock().unlock();

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    for (const QUuid &uuid : d_dataRepository->uuidsAnnotatedMatrix(currentUuid))
        baseAnnotatedMatrices.append(qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(uuid)));

    d_dataRepository->readWriteLock().unlock();

    TransformDataWithPredefinedMetricsDialog transformDataWithPredefinedMetricsDialog(nullptr, baseAnnotatedMatrices);

    if (!transformDataWithPredefinedMetricsDialog.exec())
        return;

    d_controller->startWorker(new TransformDataWithPredefinedMetricsWorker(nullptr, transformDataWithPredefinedMetricsDialog.parameters()));




    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not perform set enrichment analysis, no data selected", 10000);

        return;

    }

    SetEnrichmentAnalysisDialog setEnrichmentAnalysisDialog(baseAnnotatedMatrix);

    if (!setEnrichmentAnalysisDialog.exec())
        return;

    d_controller->startWorker(new SetEnrichmentAnalysisWorker(nullptr, setEnrichmentAnalysisDialog.parameters()));

}

void MainWindow::on_actiondescriptives_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not calculate descriptives, no data selected", 10000);

        return;

    }

    DescriptivesDialog descriptivesDialog(baseAnnotatedMatrix);

    if (!descriptivesDialog.exec())
        return;

    d_controller->startWorker(new DescriptivesWorker(nullptr, descriptivesDialog.parameters()));

}

void MainWindow::on_actionusing_sequence_of_functions_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not transform data using sequence of functions, no data selected", 10000);

        return;

    }

    TransformDataUsingSequenceOfFunctionsDialog  transformDataUsingSequenceOfFunctionsDialog(baseAnnotatedMatrix);

    if (!transformDataUsingSequenceOfFunctionsDialog.exec())
        return;

    d_controller->startWorker(new TransformDataUsingSequenceOfFunctionsWorker(nullptr, transformDataUsingSequenceOfFunctionsDialog.parameters()));

}

void MainWindow::on_actiondistance_similarity_matrix_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (!d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("can create distance matrix, no project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    d_dataRepository->readWriteLock().unlock();

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    for (const QUuid &uuid : d_dataRepository->uuidsAnnotatedMatrix(currentUuid))
        baseAnnotatedMatrices.append(qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(uuid)));

    d_dataRepository->readWriteLock().unlock();

    DistanceSimilarityMatrixDialog distanceSimilarityMatrixDialog(nullptr, baseAnnotatedMatrices);

    if (!distanceSimilarityMatrixDialog.exec())
        return;

    d_controller->startWorker(new DistanceSimilarityMatrixWorker(nullptr, distanceSimilarityMatrixDialog.parameters()));

}

void MainWindow::on_actionreplace_with_annotation_value_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not replace identifier with annotation value, no data selected", 10000);

        return;

    }

    ReplaceIdentifierWithAnnotationValueDialog  replaceIdentifierWithAnnotationValueDialog(baseAnnotatedMatrix);

    if (!replaceIdentifierWithAnnotationValueDialog.exec())
        return;

    d_controller->startWorker(new ReplaceIdentifierWithAnnotationValueWorker(nullptr, replaceIdentifierWithAnnotationValueDialog.parameters()));

}

void MainWindow::on_actionsplit_identifiers_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not split identifiers, no data selected", 10000);

        return;

    }

    SplitIdentifiersDialog  splitIdentifiersDialog(baseAnnotatedMatrix);

    if (!splitIdentifiersDialog.exec())
        return;

    d_controller->startWorker(new SplitIdentifiersWorker(nullptr, splitIdentifiersDialog.parameters()));

}

void MainWindow::on_actionrandom_select_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not random select, no data selected", 10000);

        return;

    }

    RandomSelectDialog  randomSelectDialog(baseAnnotatedMatrix);

    if (!randomSelectDialog.exec())
        return;

    d_controller->startWorker(new RandomSelectWorker(nullptr, randomSelectDialog.parameters()));

}

void MainWindow::on_actionshuffle_identifiers_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not shuffle identifiers, no data selected", 10000);

        return;

    }

    ShuffleIdentifiersDialog  shuffleIdentifiersDialog(baseAnnotatedMatrix);

    if (!shuffleIdentifiersDialog.exec())
        return;

    d_controller->startWorker(new ShuffleIdentifiersWorker(nullptr, shuffleIdentifiersDialog.parameters()));

}

void MainWindow::on_actionshuffle_data_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not shuffle data, no data selected", 10000);

        return;

    }

    ShuffleDataDialog  shuffleDataDialog(baseAnnotatedMatrix);

    if (!shuffleDataDialog.exec())
        return;

    d_controller->startWorker(new ShuffleDataWorker(nullptr, shuffleDataDialog.parameters()));

}

void MainWindow::on_actioncompare_samples_continuous_data_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not calculate descriptives, no data selected", 10000);

        return;

    }

    CompareSamplesContinuousDialog compareSamplesContinuousDialog(baseAnnotatedMatrix);

    if (!compareSamplesContinuousDialog.exec())
        return;

    d_controller->startWorker(new CompareSamplesContinuousWorker(nullptr, compareSamplesContinuousDialog.parameters()));

}

void MainWindow::on_actionremove_duplicate_identifiers_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could remove duplicate identifiers, no data selected", 10000);

        return;

    }

    RemoveDuplicateIdentifiersDialog  removeDuplicateIdentifiersDialog(baseAnnotatedMatrix);

    if (!removeDuplicateIdentifiersDialog.exec())
        return;

    d_controller->startWorker(new RemoveDuplicateIdentifiersWorker(nullptr, removeDuplicateIdentifiersDialog.parameters()));

}

void MainWindow::on_actionICA_image_analysis_triggered()
{

    ICAImageAnalysisDialog icaImageAnalysisDialog;

    if (!icaImageAnalysisDialog.exec())
        return;

    d_controller->startWorker(new ICAImageAnalysisWorker(nullptr, icaImageAnalysisDialog.parameters()));

}

void MainWindow::on_actionmedian_polish_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not transform data using median polish, no data selected", 10000);

        return;

    }

    TransformDataUsingMedianPolishDialog  transformDataUsingMedianPolishDialog(baseAnnotatedMatrix);

    if (!transformDataUsingMedianPolishDialog.exec())
        return;

    d_controller->startWorker(new TransformDataUsingMedianPolishWorker(nullptr, transformDataUsingMedianPolishDialog.parameters()));

}

void MainWindow::on_actioncalculate_consensus_for_independent_components_triggered()
{

    CalculateConsensusForIndependentComponentsDialog calculateConsensusForIndependentComponentsDialog;

    if (!calculateConsensusForIndependentComponentsDialog.exec())
        return;

    d_controller->startWorker(new CalculateConsensusForIndependentComponentsWorker(nullptr, calculateConsensusForIndependentComponentsDialog.parameters()));

}

void MainWindow::on_actionSysmex_data_processor_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QUuid currentUuid = d_dataRepository->currentUuid();

    if (!d_dataRepository->isUuidProject(currentUuid)) {

        ui->statusbar->showMessage("cannot process Sysmex data, no project was selected", 10000);

        d_dataRepository->readWriteLock().unlock();

        return;

    }

    d_dataRepository->readWriteLock().unlock();

    QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices;

    for (const QUuid &uuid : d_dataRepository->uuidsAnnotatedMatrix(currentUuid))
        baseAnnotatedMatrices.append(qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(uuid)));

    d_dataRepository->readWriteLock().unlock();

    SysmexDataProcessorDialog sysmexDataProcessorDialog(nullptr, baseAnnotatedMatrices);

    if (!sysmexDataProcessorDialog.exec())
        return;

    d_controller->startWorker(new SysmexDataProcessorWorker(nullptr, sysmexDataProcessorDialog.parameters()));

}

void MainWindow::on_actionquantile_normalization_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not transform data using quantile normalization, no data selected", 10000);

        return;

    }

    TransformDataUsingQuantileNormalizationDialog  transformDataUsingQuantileNormalizationDialog(baseAnnotatedMatrix);

    if (!transformDataUsingQuantileNormalizationDialog.exec())
        return;

    d_controller->startWorker(new TransformDataUsingQuantileNormalizationWorker(nullptr, transformDataUsingQuantileNormalizationDialog.parameters()));

}

void MainWindow::on_actionremove_principal_components_triggered()
{

    d_dataRepository->readWriteLock().lockForRead();

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(d_dataRepository->objectPointer(d_dataRepository->currentUuid()));

    d_dataRepository->readWriteLock().unlock();

    if (!baseAnnotatedMatrix) {

        ui->statusbar->showMessage("could not transform data by removing principal components, no data selected", 10000);

        return;

    }

    TransformDataByRemovingPrincipalComponentsDialog  transformDataByRemovingPrincipalComponentsDialog(baseAnnotatedMatrix);

    if (!transformDataByRemovingPrincipalComponentsDialog.exec())
        return;

    d_controller->startWorker(new TransformDataByRemovingPrincipalComponentsWorker(nullptr, transformDataByRemovingPrincipalComponentsDialog.parameters()));

}

MainWindow::~MainWindow()
{

    this->writeApplicationSettings();

    delete ui;

}
