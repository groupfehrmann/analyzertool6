#include "basemetaanalysisapproach.h"

BaseMetaAnalysisApproach::BaseMetaAnalysisApproach(const QList<QString> &testDescriptiveLabels, const std::function<double (const double &)> &backtransformEffectSizeFunction) :
    d_statistic(std::numeric_limits<double>::quiet_NaN()),
    d_pValue(std::numeric_limits<double>::quiet_NaN()),
    d_testDescriptiveLabels(testDescriptiveLabels),
    d_backtransformEffectSizeFunction(backtransformEffectSizeFunction),
    d_isPValueCalculated(false),
    d_isStatisticCalculated(false),
    d_isTestDescriptiveValuesCalculated(false)
{

}

const double &BaseMetaAnalysisApproach::statistic()
{

    if (!d_isStatisticCalculated) {

        this->calculateStatistic();

        d_isStatisticCalculated = true;

    }

    return d_statistic;

}

const double &BaseMetaAnalysisApproach::pValue()
{

    if (!d_isPValueCalculated) {

        this->calculatePValue();

        d_isPValueCalculated = true;

    }

    return d_pValue;

}

double BaseMetaAnalysisApproach::zTransformedPValue()
{

    if (!d_isPValueCalculated) {

        this->calculatePValue();

        d_isPValueCalculated = true;

    }

    if (std::isnan(d_pValue))
        return std::numeric_limits<double>::quiet_NaN();

    boost::math::normal dist(0.0, 1.0);

    return (d_statistic < 0.0) ? -quantile(complement(dist, d_pValue / 2.0)) : quantile(complement(dist, d_pValue / 2.0));

}

const QList<double> &BaseMetaAnalysisApproach::testDescriptiveValues()
{

    if (!d_isTestDescriptiveValuesCalculated) {

        this->calculateTestDescriptiveValues();

        d_isTestDescriptiveValuesCalculated = true;

    }

    return d_testDescriptiveValues;

}
