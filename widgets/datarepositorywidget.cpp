#include "datarepositorywidget.h"
#include "ui_datarepositorywidget.h"

DataRepositoryWidget::DataRepositoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataRepositoryWidget)
{

    ui->setupUi(this);

}

void DataRepositoryWidget::setDataRepositoryModel(DataRepositoryModel *dataRepositoryModel)
{

    ui->treeView->setModel(dataRepositoryModel);

    this->connect(dataRepositoryModel, &DataRepositoryModel::objectAdded, [this, dataRepositoryModel](const QUuid &uuid, const QSharedPointer<QObject> &object){Q_UNUSED(object); ui->treeView->setCurrentIndex(dataRepositoryModel->index(uuid));});

    this->connect(ui->treeView->selectionModel(), &QItemSelectionModel::currentChanged, [dataRepositoryModel](const QModelIndex &current, const QModelIndex &previous){Q_UNUSED(previous); dataRepositoryModel->setCurrentUuid(current.isValid() ? *static_cast<QUuid *>(current.internalPointer()) : QUuid());});

}

DataRepositoryWidget::~DataRepositoryWidget()
{

    delete ui;

}
