#include "mannwhitneyutest.h"

qsizetype MannWhitneyUTest::d_minimumNumberOfSamples = 2;

qsizetype MannWhitneyUTest::d_maximumNumberOfSamples = 2;

qsizetype MannWhitneyUTest::d_minimumNumberOfItemsPerSample = 2;

bool MannWhitneyUTest::d_effectSizeAvailable = true;

QString MannWhitneyUTest::d_effectSizeLabel = "Fisher's z' transformation";

QList<QString> MannWhitneyUTest::d_testDescriptiveLabels = {"Point-biserial R", "Area under the curve (AUC)", "Mann-Whitney U", "Sig. (2-tailed)"};

std::function<double (const double &)> MannWhitneyUTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return std::tanh(effectSize);};

MannWhitneyUTest::MannWhitneyUTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

MannWhitneyUTest::MannWhitneyUTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    QList<double> rankedData;

    for (qsizetype i = 0; i < data.count(); ++i) {

        if (codedSampleIdentifiers.at(i) != -1)
            rankedData.append(data.at(i));

    }

    d_tiesCorrectionFactor = MathOperations::crankInplace(rankedData);

    d_samples.resize(2);

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1)
            d_samples[codedSampleIdentifier].append(rankedData.at(i));

    }

    d_sampleSize = d_samples.at(0).count() + d_samples.at(1).count();

}

void MannWhitneyUTest::calculateStatistic()
{

    double sumOfRankSample1;

    if (d_samples.at(0).size() < d_samples.at(1).size()) {

        sumOfRankSample1 = boost::accumulate(d_samples.at(0), 0.0);

        d_n1 = d_samples.at(0).size();

        d_n2 = d_samples.at(1).size();

    } else {

        sumOfRankSample1 = boost::accumulate(d_samples.at(1), 0.0);

        d_n1 = d_samples.at(1).size();

        d_n2 = d_samples.at(0).size();

    }

    double n = d_samples.size();

    double sumOfRankSample2 = ((n * (n + 1.0)) / 2.0) - sumOfRankSample1;

    d_u1 = d_n1 * d_n2 + ((d_n1 * (d_n1 + 1.0)) / 2.0) - sumOfRankSample1;

    double u2 = d_n1 * d_n2 + ((d_n2 * (d_n2 + 1.0)) / 2.0) - sumOfRankSample2;

    d_statistic =  d_u1 < u2 ? d_u1 : u2;

}

void MannWhitneyUTest::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    d_z = (d_statistic - (d_n1 * d_n2 / 2.0)) / std::sqrt(((d_n1 * d_n2 * (d_sampleSize + 1.0)) / 12.0) - ((d_n1 * d_n2 * d_tiesCorrectionFactor) / (12.0 * (d_sampleSize) * (d_sampleSize - 1.0))));

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, fabs(d_z)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void MannWhitneyUTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    this->calculateEffectSize();

    this->calculateStandardErrorEffectSize();

    d_testDescriptiveValues << d_pointBiserialR << ((d_samples.at(0).count() * d_samples.at(1).count() + ((d_samples.at(0).count() * (d_samples.at(0).count() + 1.0)) / 2.0) - boost::accumulate(d_samples.at(0), 0.0)) / (d_n1 * d_n2)) - 0.5 << d_statistic << d_pValue;

}

void MannWhitneyUTest::calculateEffectSize()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_pointBiserialR = std::fabs(d_z) / std::sqrt(d_sampleSize);

    if (d_pointBiserialR == -1.0) {

        d_effectSize = std::numeric_limits<double>::lowest();

        return;

    }

    if (d_pointBiserialR == 1.0) {

        d_effectSize = std::numeric_limits<double>::max();

        return;

    }

    d_effectSize = std::atanh(d_pointBiserialR);

}

void MannWhitneyUTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = (1.0 / std::sqrt(d_sampleSize - 3.0));

}
