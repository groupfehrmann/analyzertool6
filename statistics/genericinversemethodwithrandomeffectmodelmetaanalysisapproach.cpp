#include "genericinversemethodwithrandomeffectmodelmetaanalysisapproach.h"

QList<QString> GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::d_testDescriptiveLabels = {"Heterogeneity statistic (Q)", "Q Sig. (2-tailed)", "I-squared", "Mean effect size", "Lower 95% CI mean effect size", "Upper 95% CI mean effect size", "Z", "Sig. (2-tailed)"};

GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach(const QList<double> &effectSizes, const QList<double> &standardErrorOfEffectSizes, const std::function<double (const double &)> &backtransformEffectSizeFunction) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, backtransformEffectSizeFunction),
    d_effectSizes(effectSizes),
    d_standardErrorOfEffectSizes(standardErrorOfEffectSizes)
{

}

void GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::calculateStatistic()
{

    d_sumOfInverseVarianceWeights = 0.0;

    d_sumOfSquaredInverseVarianceWeights = 0.0;

    d_sumOfInverseVarianceWeightedEffectSizes = 0.0;

    d_sumOfInverseVarianceWeightedSquaredEffectSizes = 0.0;

    for (qsizetype i = 0; i < d_effectSizes.count(); ++i) {

        double effectSize = d_effectSizes.at(i);

        double standardError = d_standardErrorOfEffectSizes.at(i);

        double inverseVarianceWeight = 1.0 / (standardError * standardError);

        d_sumOfInverseVarianceWeights += inverseVarianceWeight;

        d_sumOfSquaredInverseVarianceWeights += inverseVarianceWeight * inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedEffectSizes += effectSize * inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedSquaredEffectSizes += inverseVarianceWeight * effectSize * effectSize;

    }

    d_q = d_sumOfInverseVarianceWeightedSquaredEffectSizes - ((d_sumOfInverseVarianceWeightedEffectSizes * d_sumOfInverseVarianceWeightedEffectSizes) / d_sumOfInverseVarianceWeights);

    double randomEffectsVarianceComponent = (d_q - (d_effectSizes.count() - 1.0)) / (d_sumOfInverseVarianceWeights - (d_sumOfSquaredInverseVarianceWeights / d_sumOfInverseVarianceWeights));

    if (randomEffectsVarianceComponent < 0.0)
        randomEffectsVarianceComponent = 0.0;

    d_sumOfInverseVarianceWeights = 0.0;

    d_sumOfInverseVarianceWeightedEffectSizes = 0.0;

    d_sumOfInverseVarianceWeightedSquaredEffectSizes = 0.0;

    for (qsizetype i = 0; i < d_effectSizes.count(); ++i) {

        double effectSize = d_effectSizes.at(i);

        double standardError = d_standardErrorOfEffectSizes.at(i);

        double inverseVarianceWeight = 1.0 / ((standardError * standardError) + randomEffectsVarianceComponent);

        d_sumOfInverseVarianceWeights += inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedEffectSizes += effectSize * inverseVarianceWeight;

        d_sumOfInverseVarianceWeightedSquaredEffectSizes += inverseVarianceWeight * effectSize * effectSize;

    }

    d_meanEffectSize = d_sumOfInverseVarianceWeightedEffectSizes / d_sumOfInverseVarianceWeights;

    d_standardErrorEffectSize = 1.0 / std::sqrt(d_sumOfInverseVarianceWeights);

    d_statistic = d_meanEffectSize / d_standardErrorEffectSize;

}

void GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    double q = d_sumOfInverseVarianceWeightedSquaredEffectSizes - ((d_sumOfInverseVarianceWeightedEffectSizes * d_sumOfInverseVarianceWeightedEffectSizes) / d_sumOfInverseVarianceWeights);

    double iSquared = 100.0 * ((q - (d_effectSizes.count() - 1)) / q);

    if (iSquared < 0.0)
        iSquared = 0.0;

    d_testDescriptiveValues << q << boost::math::cdf(boost::math::complement(boost::math::chi_squared(d_effectSizes.count() - 1), q)) << iSquared;

    double w = boost::math::quantile(boost::math::complement(boost::math::normal(0.0, 1.0), 0.025)) * d_standardErrorEffectSize;

    d_testDescriptiveValues << d_meanEffectSize << d_meanEffectSize - w << d_meanEffectSize + w << d_statistic << d_pValue;

}
