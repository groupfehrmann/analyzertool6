#ifndef DOUBLESPINBOXWIDGET_H
#define DOUBLESPINBOXWIDGET_H

#include <QWidget>
#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QDoubleSpinBox>

#include "dialogs/basedialog.h"

namespace Ui {

    class DoubleSpinBoxWidget;

}

class DoubleSpinBoxWidget : public QWidget
{

    Q_OBJECT

public:

    explicit DoubleSpinBoxWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QDoubleSpinBox *doubleSpinBox();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~DoubleSpinBoxWidget();

private:

    Ui::DoubleSpinBoxWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

};

#endif // DOUBLESPINBOXWIDGET_H
