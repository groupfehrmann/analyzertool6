#include "doublespinboxwidget.h"
#include "ui_doublespinboxwidget.h"

DoubleSpinBoxWidget::DoubleSpinBoxWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::DoubleSpinBoxWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

}

QDoubleSpinBox *DoubleSpinBoxWidget::doubleSpinBox()
{

    return ui->doubleSpinBox;

}

QLabel *DoubleSpinBoxWidget::label()
{

    return ui->label;

}

void DoubleSpinBoxWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/doubleSpinBox");

    if (settings.contains("decimals"))
        ui->doubleSpinBox->setDecimals(settings.value("decimals").toInt());

    if (settings.contains("maximum"))
        ui->doubleSpinBox->setMaximum(settings.value("maximum").toDouble());

    if (settings.contains("minimum"))
        ui->doubleSpinBox->setMinimum(settings.value("minimum").toDouble());

    if (settings.contains("prefix"))
        ui->doubleSpinBox->setPrefix(settings.value("prefix").toString());

    if (settings.contains("singleStep"))
        ui->doubleSpinBox->setSingleStep(settings.value("singleStep").toDouble());

    if (settings.contains("stepType"))
        ui->doubleSpinBox->setStepType(static_cast<QAbstractSpinBox::StepType>(settings.value("stepType").toInt()));

    if (settings.contains("suffix"))
        ui->doubleSpinBox->setSuffix(settings.value("suffix").toString());

    if (settings.contains("value"))
        ui->doubleSpinBox->setValue(settings.value("value").toDouble());

    settings.endGroup();

    d_readWidgetSettings = true;

}

void DoubleSpinBoxWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/doubleSpinBox");

    settings.setValue("decimals", ui->doubleSpinBox->decimals());

    settings.setValue("maximum", ui->doubleSpinBox->maximum());

    settings.setValue("minimum", ui->doubleSpinBox->minimum());

    settings.setValue("prefix", ui->doubleSpinBox->prefix());

    settings.setValue("singleStep", ui->doubleSpinBox->singleStep());

    settings.setValue("stepType", static_cast<int>(ui->doubleSpinBox->stepType()));

    settings.setValue("suffix", ui->doubleSpinBox->suffix());

    settings.setValue("value", ui->doubleSpinBox->value());

    settings.endGroup();

}

DoubleSpinBoxWidget::~DoubleSpinBoxWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
