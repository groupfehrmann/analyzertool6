#ifndef DISTANCESIMILARITYMATRIXDIALOG_H
#define DISTANCESIMILARITYMATRIXDIALOG_H

#include <QObject>
#include <QWidget>
#include <QSettings>
#include <QInputDialog>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "widgets/spinboxwidget.h"
#include "workerclasses/distancesimilaritymatrixworker.h"

class DistanceSimilarityMatrixDialog : public BaseDialog
{

    Q_OBJECT

public:

    DistanceSimilarityMatrixDialog(BaseDialog *parent = nullptr, QList<QSharedPointer<BaseAnnotatedMatrix>> baseAnnotatedMatrices = QList<QSharedPointer<BaseAnnotatedMatrix>>());

protected:

    void initWidgets_() override;

private:

    QList<QSharedPointer<BaseAnnotatedMatrix>> d_baseAnnotatedMatrices;

    OrientationWidget *d_orientationWidget_data1;

    ItemSelectorWidget *d_itemSelectorWidget_variables_data1;

    ItemSelectorWidget *d_itemSelectorWidget_items_data1;

    OrientationWidget *d_orientationWidget_data2;

    ItemSelectorWidget *d_itemSelectorWidget_variables_data2;

    ItemSelectorWidget *d_itemSelectorWidget_items_data2;

private slots:

    void orientation_data1_changed(Qt::Orientation orientation);

    void orientation_data2_changed(Qt::Orientation orientation);

    void dialogAccepted();

};

#endif // DISTANCESIMILARITYMATRIXDIALOG_H
