#ifndef PROJECTDATAONINDEPENDENTCOMPONENTSWORKER_H
#define PROJECTDATAONINDEPENDENTCOMPONENTSWORKER_H

#include "statistics/fittingdistributions.h"
#include <QObject>
#include <QList>
#include <QString>
#include <QUuid>
#include <QApplication>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "math/johnsontransformation.h"
#include "math/mathdescriptives.h"
#include "math/XoshiroCpp.hpp"
#include <boost/math/statistics/univariate_statistics.hpp>
#include "statistics/fittingdistributions.h"

#include "boost/math/tools/minima.hpp"
#include "math/symmetricgeneralizednormal.h"

class ProjectDataOnIndependentComponentsParameters : public BaseParameters
{

public:

    ProjectDataOnIndependentComponentsParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> data;

    Qt::Orientation orientationData;

    QList<qsizetype> selectedItemIndexesData;

    QList<QString> selectedItemIdentifiersData;

    QList<qsizetype> selectedVariableIndexesData;

    QList<QString> selectedVariableIdentifiersData;

    QSharedPointer<BaseAnnotatedMatrix> independentComponents;

    Qt::Orientation orientationIndependentComponents;

    QList<qsizetype> selectedItemIndexesIndependentComponents;

    QList<QString> selectedItemIdentifiersIndependentComponents;

    QList<qsizetype> selectedVariableIndexesIndependentComponents;

    QList<QString> selectedVariableIdentifiersIndependentComponents;

    qsizetype numberOfPermutations;

    bool flipICs;

    double pValue_threshold;

    qsizetype blockSize;

    QString exportDirectory;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class ProjectDataOnIndependentComponentsWorker : public BaseWorker
{

public:

    ProjectDataOnIndependentComponentsWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T, typename S> void doWork_(QSharedPointer<AnnotatedMatrix<T>> data, QSharedPointer<AnnotatedMatrix<S>> independentComponents);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    void apply_permutation(std::span<double> &span, QList<unsigned long> indices) const;

};

template<typename T, typename S>
void ProjectDataOnIndependentComponentsWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> data, QSharedPointer<AnnotatedMatrix<S>> independentComponents)
{

    QSharedPointer<ProjectDataOnIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<ProjectDataOnIndependentComponentsParameters>());

    // Matching items between new data and independent components
    ////////////////

    QUuid uuidProgress = this->startProgress("matching items between data and independent components", 0, 0);

    QHash<QString, qsizetype> itemIdentifierToIndex_originalData;

    QHash<QString, qsizetype> itemIdentifierToIndex_independentComponents;

    for (qsizetype i = 0; i < parameters->selectedItemIndexesData.count(); ++i)
        itemIdentifierToIndex_originalData.insert(parameters->selectedItemIdentifiersData.at(i), parameters->selectedItemIndexesData.at(i));

    QHash<qsizetype, qsizetype> oldIndexToNewIndex;

    for (qsizetype i = 0; i < parameters->selectedItemIndexesIndependentComponents.count(); ++i) {

        itemIdentifierToIndex_independentComponents.insert(parameters->selectedItemIdentifiersIndependentComponents.at(i), parameters->selectedItemIndexesIndependentComponents.at(i));

        oldIndexToNewIndex.insert(parameters->selectedItemIndexesIndependentComponents.at(i), i);

    }

    QList<qsizetype> alignedItemIndexes_originalData;

    QList<qsizetype> alignedItemIndexes_independentComponents;

    QHashIterator<QString, qsizetype> it1(itemIdentifierToIndex_originalData);

    QList<QString> alignedItemIdentifiers;

    while (it1.hasNext()) {

        it1.next();

        qsizetype matchedItemIndex = itemIdentifierToIndex_independentComponents.value(it1.key(), -1);

        if (matchedItemIndex != -1) {

            alignedItemIndexes_originalData.append(it1.value());

            alignedItemIndexes_independentComponents.append(oldIndexToNewIndex.value(matchedItemIndex));

            alignedItemIdentifiers.append(it1.key());

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (alignedItemIndexes_originalData.isEmpty()) {

        this->reportError("no items could be matched between data and independent components");

        this->requestInterruption();

        return;

    } else
        this->reportMessage("number of items that could be matched between data and independent components is " + QString::number(alignedItemIndexes_independentComponents.count()));


    ////////////////

    // Fetching data
    ////////////////

    uuidProgress = this->startProgress("fetching data", 0, 0);

    QList<double> originalData_aligned;

    if (parameters->orientationData == Qt::Vertical)
        originalData_aligned = data->template sliced<double>(parameters->selectedVariableIndexesData, alignedItemIndexes_originalData);
    else {

        originalData_aligned = data->template sliced<double>(alignedItemIndexes_originalData, parameters->selectedVariableIndexesData);

        MathOperations::transposeInplace(originalData_aligned, parameters->selectedVariableIndexesData.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // Fetching independent components
    ////////////////

    uuidProgress = this->startProgress("fetching independent components", 0, 0);

    QList<double> independentComponents_original;

    if (parameters->orientationIndependentComponents == Qt::Vertical)
        independentComponents_original = independentComponents->template sliced<double>(parameters->selectedVariableIndexesIndependentComponents, parameters->selectedItemIndexesIndependentComponents);
    else {

        independentComponents_original = independentComponents->template sliced<double>(parameters->selectedItemIndexesIndependentComponents, parameters->selectedVariableIndexesIndependentComponents);

        MathOperations::transposeInplace(independentComponents_original, parameters->selectedVariableIndexesIndependentComponents.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    if (parameters->flipICs) {

        // Flipping independent components based on skewness
        ////////////////

        uuidProgress = this->startProgress("flipping independent components based on skewness", 0, parameters->selectedVariableIndexesIndependentComponents.count());

        QList<std::span<double>> spansOfindependentComponents_original = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(independentComponents_original, parameters->selectedItemIndexesIndependentComponents.count());

        for (std::span<double> &span : spansOfindependentComponents_original) {

            if (boost::math::statistics::skewness(span) < 0.0)
                std::transform(span.begin(), span.end(), span.begin(), std::negate<double>());

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        this->stopProgress(uuidProgress);

        ////////////////


        AnnotatedMatrix<double> *result_flippedIndependentComponents = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiersIndependentComponents, parameters->selectedItemIdentifiersIndependentComponents, independentComponents_original);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("flipped independent components", result_flippedIndependentComponents));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_flippedIndependentComponents);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "flipped_independent_components");

        }

    }

    // Calculating pseudo-inverse of independent components
    ////////////////

    int info;

    uuidProgress = this->startProgress("calculating pseudo-inverse of independent components ", 0, 0);

    QList<double> independentComponents_notAligned_pseudoInverse = MathOperations::pseudoInverseSVD(independentComponents_original, parameters->selectedItemIndexesIndependentComponents.count(), parameters->numberOfThreadsToUse, &info);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (info < 0)
        this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");
    else if (info > 0)
        this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    // Creating aligned (pseudo-inverse) of independent components
    ////////////////

    QList<double> independentComponents_aligned_pseudoInverse;

    QList<double> independentComponents_aligned;

    independentComponents_aligned_pseudoInverse.reserve(parameters->selectedVariableIndexesIndependentComponents.count() * alignedItemIndexes_independentComponents.count());

    independentComponents_aligned.reserve(parameters->selectedVariableIndexesIndependentComponents.count() * alignedItemIndexes_independentComponents.count());

    uuidProgress = this->startProgress("creating aligned (pseudo-inverse) of independent components", 0, parameters->selectedVariableIndexesIndependentComponents.count());

    MathOperations::transposeInplace(independentComponents_notAligned_pseudoInverse, parameters->selectedVariableIndexesIndependentComponents.count());

    for (qsizetype i = 0; i < parameters->selectedVariableIndexesIndependentComponents.count(); ++i) {

        for (qsizetype j = 0; j < alignedItemIndexes_independentComponents.count(); ++j) {

            independentComponents_aligned_pseudoInverse.append(independentComponents_notAligned_pseudoInverse.at(i * parameters->selectedItemIndexesIndependentComponents.count() + alignedItemIndexes_independentComponents.at(j)));

            independentComponents_aligned.append(independentComponents_original.at(i * parameters->selectedItemIndexesIndependentComponents.count() + alignedItemIndexes_independentComponents.at(j)));

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    MathOperations::transposeInplace(independentComponents_aligned_pseudoInverse, alignedItemIndexes_independentComponents.count());

    this->stopProgress(uuidProgress);

    ////////////////

    // Calculating mixing matrix
    ////////////////

    uuidProgress = this->startProgress("calculating mixing matrix", 0, 0);

    QList<double> mixingMatrix(parameters->selectedVariableIndexesData.count() * parameters->selectedVariableIndexesIndependentComponents.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, parameters->selectedVariableIndexesData.count(), parameters->selectedVariableIndexesIndependentComponents.count(), alignedItemIndexes_originalData.count(), 1.0, originalData_aligned.data(), alignedItemIndexes_originalData.count(), independentComponents_aligned_pseudoInverse.data(), parameters->selectedVariableIndexesIndependentComponents.count(), 0.0, mixingMatrix.data(), parameters->selectedVariableIndexesIndependentComponents.count());

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    AnnotatedMatrix<double> *result_mixingMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiersData, parameters->selectedVariableIdentifiersIndependentComponents, mixingMatrix);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("mixing matrix", result_mixingMatrix));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_mixingMatrix);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "mixing_matrix");

    }

    ////////////////

    // Reconstructing data with non corrected mixing matrix
    ////////////////

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("projecting data", 0, 0);

    QList<double> matrixProjectedData_nonCorrectedMixingMatrix(parameters->selectedVariableIndexesData.count() * parameters->selectedItemIndexesIndependentComponents.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, parameters->selectedVariableIndexesData.count(), parameters->selectedItemIndexesIndependentComponents.count(), parameters->selectedVariableIndexesIndependentComponents.count(), 1.0, mixingMatrix.data(), parameters->selectedVariableIndexesIndependentComponents.count(), independentComponents_original.data(), parameters->selectedItemIndexesIndependentComponents.count(), 0.0, matrixProjectedData_nonCorrectedMixingMatrix.data(), parameters->selectedItemIndexesIndependentComponents.count());

    MathOperations::transposeInplace(matrixProjectedData_nonCorrectedMixingMatrix, parameters->selectedItemIndexesIndependentComponents.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    AnnotatedMatrix<double> *result_matrixProjectedData_nonCorrectedMixingMatrix = new AnnotatedMatrix<double>(parameters->selectedItemIdentifiersIndependentComponents, parameters->selectedVariableIdentifiersData, matrixProjectedData_nonCorrectedMixingMatrix);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("projected data", result_matrixProjectedData_nonCorrectedMixingMatrix));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_matrixProjectedData_nonCorrectedMixingMatrix);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "projected_data");

    }

    ////////////////

    // Calculating corrected mixing matrix
    ////////////////

    double z_lower = 0.01;

    double z_upper = 5.0;

    qsizetype z_intervals = 101;

    QList<QVariant> transformationParameters(mixingMatrix.count() * 12);

    double z_lim_delta = (z_upper - z_lower) / static_cast<double>(z_intervals);

    boost::math::normal normal_distribution(0.0, 1.0);

    QList<double> z_values;

    for (double z = z_lower; z < z_upper; z += z_lim_delta) {

        z_values << z;

        z_values << boost::math::cdf(normal_distribution, -3.0 * z);

        z_values << boost::math::cdf(normal_distribution, -z);

        z_values << boost::math::cdf(normal_distribution, z);

        z_values << boost::math::cdf(normal_distribution, 3.0 * z);

    }

    auto mapFunctor = [&](QPair<qsizetype, QList<std::pair<double *, qsizetype>>> &blocks) {

        boost::math::normal_distribution<double> normal_distribution(0, 1);

        QThreadPool threadPool;


        threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse / d_threadPool.maxThreadCount());

        qsizetype numberOfSamplesProcessed = 0;

        for (const std::pair<double *, qsizetype> &block: blocks.second) {

            XoshiroCpp::Xoshiro256Plus g(18041970);

            QList<double> permutationMatrix(parameters->numberOfPermutations * parameters->selectedVariableIndexesIndependentComponents.count() * block.second, 0.0);

            double *it_permutationMatrix = permutationMatrix.data();


            QList<unsigned long> indexes(alignedItemIndexes_originalData.count());

            std::iota(indexes.begin(), indexes.end(), 0);


            QList<std::span<double>> spans_originalData_aligned_block;

            double *pointer = block.first;

            for (qsizetype i = 0; i < block.second; ++i) {

                spans_originalData_aligned_block.append(std::span<double>(pointer, alignedItemIndexes_originalData.count()));

                pointer += alignedItemIndexes_originalData.count();

            }

            QUuid uuidProgress = this->startProgress("performing permutations for a block of " + QString::number(block.second) + " samples", 0, parameters->numberOfPermutations);

            for (qsizetype i = 0; i < parameters->numberOfPermutations; ++i) {

                std::shuffle(indexes.begin(), indexes.end(), g);

                QtConcurrent::blockingMap(&threadPool, spans_originalData_aligned_block, [&](std::span<double> &span){this->apply_permutation(span, indexes);});

                cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, block.second, parameters->selectedVariableIndexesIndependentComponents.count(), alignedItemIndexes_originalData.size(), 1.0, block.first, alignedItemIndexes_originalData.count(), independentComponents_aligned_pseudoInverse.data(), parameters->selectedVariableIndexesIndependentComponents.count(), 0.0, it_permutationMatrix, parameters->selectedVariableIndexesIndependentComponents.count());

                it_permutationMatrix += parameters->selectedVariableIndexesIndependentComponents.count() * block.second;

                if (this->thread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress, i + 1);

            }

            this->stopProgress(uuidProgress);


            uuidProgress = this->startProgress("transposing permutation matrix", 0, 0);

            MathOperations::transposeInplace(permutationMatrix, block.second * parameters->selectedVariableIndexesIndependentComponents.count(), parameters->numberOfThreadsToUse / d_threadPool.maxThreadCount());

            this->stopProgress(uuidProgress);


            QList<std::pair<qsizetype, std::span<double>>> indexes_spans_permutationMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(permutationMatrix, parameters->numberOfPermutations);

            auto mapFunctorAdjustNullDistribution = [&](std::pair<qsizetype, std::span<double>> &index_span_permutationMatrix) {

                std::span<double> &span_permutationMatrix(index_span_permutationMatrix.second);

                std::sort(span_permutationMatrix.begin(), span_permutationMatrix.end());

                auto div = std::div(index_span_permutationMatrix.first, parameters->selectedVariableIndexesIndependentComponents.count());

                qsizetype indexComponent = div.rem;

                qsizetype indexSample = div.quot + blocks.first + numberOfSamplesProcessed;

                double &value_mixingMatrix(mixingMatrix[indexSample * parameters->selectedVariableIndexesIndependentComponents.count() + indexComponent]);


                std::span<double>::iterator it = std::upper_bound(span_permutationMatrix.begin(), span_permutationMatrix.end(), value_mixingMatrix);

                unsigned long index = std::distance(span_permutationMatrix.begin(), it);

                double pValue1 = static_cast<double>(index) / static_cast<double>(parameters->numberOfPermutations);

                double pValue2 = static_cast<double>(span_permutationMatrix.size() - index + 1) / static_cast<double>(parameters->numberOfPermutations);

                std::pair<double, double> meanAndVariance = boost::math::statistics::mean_and_sample_variance(span_permutationMatrix);

                for (double &value : span_permutationMatrix)
                    value = (value - meanAndVariance.first) / std::sqrt(meanAndVariance.second);

                value_mixingMatrix = (value_mixingMatrix - meanAndVariance.first) / std::sqrt(meanAndVariance.second);

                JohnsonTransformation::Parameters jTransParameters = JohnsonTransformation::getTransformParameters(span_permutationMatrix, value_mixingMatrix, z_values, false);

                JohnsonTransformation::transform(span_permutationMatrix, jTransParameters);

                JohnsonTransformation::transform(value_mixingMatrix, jTransParameters);


                meanAndVariance = boost::math::statistics::mean_and_sample_variance(span_permutationMatrix);

                for (double &value : span_permutationMatrix)
                    value = (value - meanAndVariance.first) / std::sqrt(meanAndVariance.second);

                value_mixingMatrix = (value_mixingMatrix - meanAndVariance.first) / std::sqrt(meanAndVariance.second);


                double skewness_nullDistribution = boost::math::statistics::skewness(span_permutationMatrix);

                double excess_kurtosis_nullDistribution = boost::math::statistics::excess_kurtosis(span_permutationMatrix);


                SymmetricGeneralizedNormal dist(span_permutationMatrix);

                double pValue = dist.cdf(dist.location() - std::abs(value_mixingMatrix - dist.location()));

                double z = boost::math::quantile(normal_distribution, pValue);

                value_mixingMatrix = std::copysign(z, value_mixingMatrix - dist.location());


                qsizetype startIndex = indexComponent * parameters->selectedVariableIndexesData.count() * 12  + indexSample * 12;

                std::span<QVariant> span_transformationParameters(&transformationParameters[startIndex], 12);

                span_transformationParameters[0] = indexComponent + 1;

                span_transformationParameters[1] = indexSample + 1;

                span_transformationParameters[2] = jTransParameters.adStat;

                span_transformationParameters[3] = jTransParameters.functionType;

                span_transformationParameters[4] = jTransParameters.eta;

                span_transformationParameters[5] = jTransParameters.gamma;

                span_transformationParameters[6] = jTransParameters.epsilon;

                span_transformationParameters[7] = jTransParameters.lambda;

                span_transformationParameters[8] = jTransParameters.z;

                span_transformationParameters[9] = skewness_nullDistribution;

                span_transformationParameters[10] = excess_kurtosis_nullDistribution;

                span_transformationParameters[11] = pValue;

                if (pValue > parameters->pValue_threshold)
                    value_mixingMatrix = 0.0;

            };

            this->startProgress("adjusting null distribution for a block of " + QString::number(block.second) + " samples", QtConcurrent::map(&threadPool, indexes_spans_permutationMatrix, std::function<void(std::pair<qsizetype, std::span<double>> &)>(mapFunctorAdjustNullDistribution)));

            if (this->thread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            numberOfSamplesProcessed += block.second;

        }

    };

    auto div_numberOfBlocksRequires = std::div(parameters->selectedVariableIndexesData.count(), parameters->blockSize);

    qsizetype numberOfBlocks;

    if (div_numberOfBlocksRequires.rem != 0)
        numberOfBlocks = div_numberOfBlocksRequires.quot + 1;
    else
        numberOfBlocks = div_numberOfBlocksRequires.quot;

    if (numberOfBlocks < parameters->numberOfThreadsToUse) {

        d_threadPool.setMaxThreadCount(numberOfBlocks);

        MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(1);

    } else {

        d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

        MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(1);

    }

    auto div_numberOfBlocksPerThread = std::div(numberOfBlocks, static_cast<qsizetype>(d_threadPool.maxThreadCount()));

    QList<qsizetype> numberOfBlocksPerThread(d_threadPool.maxThreadCount(), div_numberOfBlocksPerThread.quot);

    for (qsizetype i = 0; i < div_numberOfBlocksPerThread.rem; ++i)
        numberOfBlocksPerThread[i]++;

    QList<QPair<qsizetype, QList<std::pair<double *, qsizetype>>>> dispatcher_threads_startIndex_blocks;

    double *pointer_originalData_aligned = originalData_aligned.data();

    qsizetype startIndex = 0;

    for (qsizetype i = 0; i < d_threadPool.maxThreadCount(); ++i) {

        QList<std::pair<double *, qsizetype>> blocks;

        for (qsizetype j = 0; j < numberOfBlocksPerThread.at(i); ++j) {

            blocks.append({pointer_originalData_aligned, parameters->blockSize});

            if (((i + 1) != d_threadPool.maxThreadCount()) || ((j + 1) != numberOfBlocksPerThread.at(i)))
                pointer_originalData_aligned += parameters->blockSize * alignedItemIndexes_originalData.count();

        }

        if (((i + 1) == d_threadPool.maxThreadCount()) && (div_numberOfBlocksRequires.rem != 0))
            blocks.last().second = div_numberOfBlocksRequires.rem;

        dispatcher_threads_startIndex_blocks.append({startIndex, blocks});

        if (((i + 1) != d_threadPool.maxThreadCount()))
            startIndex += numberOfBlocksPerThread.at(i) * parameters->blockSize;

    }

    this->startProgress("correcting mixing matrix", QtConcurrent::map(&d_threadPool, dispatcher_threads_startIndex_blocks, std::function<void(QPair<qsizetype, QList<std::pair<double *, qsizetype>>> &)>(mapFunctor)));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);


    QList<QString> rowIds;

    for (qsizetype i = 0; i < mixingMatrix.count(); ++i)
        rowIds << "row_" + QString::number(i);

    AnnotatedMatrix<QVariant> *result_transformationParameters = new AnnotatedMatrix<QVariant>(rowIds, {"component number", "sample number", "AD-stat", "functionType", "eta", "gamma", "epsilon", "lambda", "z", "skewness", "excess kurtosis", "p-value"}, transformationParameters);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("transformation parameters", result_transformationParameters));
    else {

        QSharedPointer<AnnotatedMatrix<QVariant>> sharedPointerToAnnotatedMatrix(result_transformationParameters);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "transformation_paramaters");

    }

    AnnotatedMatrix<double> *result_correctedMixingMatrix = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiersData, parameters->selectedVariableIdentifiersIndependentComponents, mixingMatrix);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("corrected mixing matrix", result_correctedMixingMatrix));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_correctedMixingMatrix);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "corrected_mixing_matrix");

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    ////////////////


    // Reconstructing data
    ////////////////

    uuidProgress = this->startProgress("projecting data", 0, 0);

    QList<double> matrixProjectedData(parameters->selectedVariableIndexesData.count() * parameters->selectedItemIndexesIndependentComponents.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, parameters->selectedVariableIndexesData.count(), parameters->selectedItemIndexesIndependentComponents.count(), parameters->selectedVariableIndexesIndependentComponents.count(), 1.0, mixingMatrix.data(), parameters->selectedVariableIndexesIndependentComponents.count(), independentComponents_original.data(), parameters->selectedItemIndexesIndependentComponents.count(), 0.0, matrixProjectedData.data(), parameters->selectedItemIndexesIndependentComponents.count());

    MathOperations::transposeInplace(matrixProjectedData, parameters->selectedItemIndexesIndependentComponents.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    AnnotatedMatrix<double> *result_matrixProjectedData = new AnnotatedMatrix<double>(parameters->selectedItemIdentifiersIndependentComponents, parameters->selectedVariableIdentifiersData, matrixProjectedData);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("projected data (with corrected mixing matrix)", result_matrixProjectedData));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_matrixProjectedData);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "projected_data_with_corrected_mixing_matrix");

    }


    // Calculating reconstruction statistics
    ////////////////

    uuidProgress = this->startProgress("projecting data on aligned items", 0, 0);

    QList<double> projectedData_aligned(parameters->selectedVariableIndexesData.count() * alignedItemIndexes_originalData.count(), 0.0);

    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, parameters->selectedVariableIndexesData.count(), alignedItemIndexes_originalData.count(), parameters->selectedVariableIndexesIndependentComponents.count(), 1.0, mixingMatrix.data(), parameters->selectedVariableIndexesIndependentComponents.count(), independentComponents_aligned.data(), alignedItemIndexes_independentComponents.count(), 0.0, projectedData_aligned.data(), alignedItemIndexes_independentComponents.count());

    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("creating copies of data and transposing", 0, 0);

    QList<double> projectedData_aligned_transposed = MathOperations::transpose(projectedData_aligned, alignedItemIndexes_originalData.count(), parameters->numberOfThreadsToUse);

    this->stopProgress(uuidProgress);


    uuidProgress = this->startProgress("fetching data", 0, 0);

    if (parameters->orientationData == Qt::Vertical)
        originalData_aligned = data->template sliced<double>(parameters->selectedVariableIndexesData, alignedItemIndexes_originalData);
    else {

        originalData_aligned = data->template sliced<double>(alignedItemIndexes_originalData, parameters->selectedVariableIndexesData);

        MathOperations::transposeInplace(originalData_aligned, parameters->selectedVariableIndexesData.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    QList<std::span<double>> originalData_aligned_spans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(originalData_aligned, alignedItemIndexes_originalData.count());

    QList<std::span<double>> projectedData_aligned_spans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(projectedData_aligned, alignedItemIndexes_originalData.count());


    this->startProgress("center original data sample-wise", QtConcurrent::map(&d_threadPool, originalData_aligned_spans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("scale original data sample-wise", QtConcurrent::map(&d_threadPool, originalData_aligned_spans, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("center projected data sample-wise", QtConcurrent::map(&d_threadPool, projectedData_aligned_spans, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->startProgress("scale projected data sample-wise", QtConcurrent::map(&d_threadPool, projectedData_aligned_spans, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("calculating correlations between original and projected data sample-wise", 0, parameters->selectedVariableIndexesData.count());

    QList<double> reconstructionStatistics;

    for (qsizetype i = 0; i < parameters->selectedVariableIndexesData.count(); ++i) {

        double pearsonCorrelation = cblas_ddot(alignedItemIndexes_originalData.count(), originalData_aligned_spans.at(i).data(), 1, projectedData_aligned_spans.at(i).data(), 1);

        reconstructionStatistics.append(pearsonCorrelation);

        reconstructionStatistics.append(pearsonCorrelation * pearsonCorrelation);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);


    AnnotatedMatrix<double> *result_reconstructionStatistics = new AnnotatedMatrix<double>(parameters->selectedVariableIdentifiersData, {"Pearson R", "explained variance"}, reconstructionStatistics);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("reconstruction statistics sample-wise", result_reconstructionStatistics));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_reconstructionStatistics);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "reconstruction_statistics");

    }

    ////////////////

    QList<double> originalData_aligned_transposed = MathOperations::transpose(originalData_aligned, alignedItemIndexes_originalData.count(), parameters->numberOfThreadsToUse);

    QList<std::span<double>> originalData_aligned_spans_transposed = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(originalData_aligned_transposed, parameters->selectedVariableIndexesData.count());

    QList<std::span<double>> projectedData_aligned_spans_transposed = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(projectedData_aligned_transposed, parameters->selectedVariableIndexesData.count());


    this->startProgress("center original data item-wise", QtConcurrent::map(&d_threadPool, originalData_aligned_spans_transposed, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("scale original data item-wise", QtConcurrent::map(&d_threadPool, originalData_aligned_spans_transposed, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    this->startProgress("center projected data item-wise", QtConcurrent::map(&d_threadPool, projectedData_aligned_spans_transposed, [](std::span<double> &span){MathOperations::centerVectorToMeanInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->startProgress("scale projected data item-wise", QtConcurrent::map(&d_threadPool, projectedData_aligned_spans_transposed, [](std::span<double> &span){MathOperations::scaleVectorToL2NormInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("calculating correlations between original and projected data item-wise", 0, alignedItemIndexes_originalData.count());

    QList<double> reconstructionStatistics_itemWise;

    for (qsizetype i = 0; i < alignedItemIndexes_originalData.count(); ++i) {

        double pearsonCorrelation = cblas_ddot(parameters->selectedVariableIndexesData.count(), originalData_aligned_spans_transposed.at(i).data(), 1, projectedData_aligned_spans_transposed.at(i).data(), 1);

        reconstructionStatistics_itemWise.append(pearsonCorrelation);

        reconstructionStatistics_itemWise.append(pearsonCorrelation * pearsonCorrelation);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    AnnotatedMatrix<double> *result_reconstructionStatistics_itemWise = new AnnotatedMatrix<double>(alignedItemIdentifiers, {"Pearson R", "explained variance"}, reconstructionStatistics_itemWise);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->data->property("uuidProject").toUuid(), new Result("reconstruction statistics item-wise", result_reconstructionStatistics_itemWise));
    else {

        QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_reconstructionStatistics_itemWise);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "result_reconstructionStatistics_itemWise");

    }

}

#endif // PROJECTDATAONINDEPENDENTCOMPONENTSWORKER_H
