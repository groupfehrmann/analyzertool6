#ifndef FISHERSMETAANALYSISAPPROACH_H
#define FISHERSMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/chi_squared.hpp"

#include "basemetaanalysisapproach.h"

class FishersMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    FishersMetaAnalysisApproach();

    FishersMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~FishersMetaAnalysisApproach() {}

private:

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // FISHERSMETAANALYSISAPPROACH_H
