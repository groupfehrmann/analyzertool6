#ifndef JOHNSONTRANSFORMATION_H
#define JOHNSONTRANSFORMATION_H

#include <QList>

#include <utility>
#include <span>

#include <boost/math/statistics/anderson_darling.hpp>
#include <boost/math/distributions/empirical_cumulative_distribution_function.hpp>
#include "boost/math/distributions/normal.hpp"

#include "math/mathdescriptives.h"

namespace JohnsonTransformation {

    enum TransformType {

        ORIGINAL = 0,

        SL = 1,

        SB = 2,

        SU = 3

    };

    struct Parameters {

        TransformType functionType;

        double eta;

        double gamma;

        double lambda;

        double epsilon;

        double z;

        double adStat;

    };

    struct Q {

        double xl;

        double xm;

        double xu;

        double QR;

        double z;

        double x2;

        double x3;

    };

    template <typename T> JohnsonTransformation::Parameters fit_sb(const QList<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters fit_su(const QList<T> &distribution, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters fit_sl(const QList<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters fit_sb(const std::span<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters fit_su(const std::span<T> &distribution, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters fit_sl(const std::span<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q);

    template <typename T> JohnsonTransformation::Parameters getTransformParameters(const std::span<T> &distribution, const T &realValue, bool exclude_original = true, std::pair<double, double> z_lim = std::pair<double, double>(.25, 1.25), qsizetype z_length = 101);

    template <typename T> JohnsonTransformation::Parameters getTransformParameters(const std::span<T> &distribution, const T &realValue, const QList<double> &z_values, bool exclude_original = true);

    template <typename T> void transform(QList<T> &distribution, JohnsonTransformation::Parameters parameters);

    template <typename T> void transform(std::span<T> &distribution, JohnsonTransformation::Parameters parameters);

    template <typename T> void transform(T &value, JohnsonTransformation::Parameters parameters);

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_sb(const QList<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q)
{

    double eta = q.z / std::acosh(.5 * std::sqrt((1.0 + q.xm / q.xu) * (1.0 + q.xm / q.xl)));

    double gamma = eta * std::asinh((q.xm / q.xl - q.xm / q.xu) * sqrt((1.0 + q.xm / q.xu) * (1.0 + q.xm / q.xl) - 4.0) / (2.0 * ((q.xm * q.xm) / q.xl / q.xu - 1.0)));

    double temp = ((1.0 + q.xm / q.xu) * (1.0 + q.xm / q.xl) - 2.0);

    double lambda = (q.xm * std::sqrt( temp * temp - 4.0) / ((q.xm * q.xm) / q.xl / q.xu - 1.0));

    double epsilon = .5 * (q.x2 + q.x3 - lambda + q.xm * (q.xm / q.xl - q.xm / q.xu) / ((q.xm * q.xm) / q.xl / q.xu - 1.0));

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0) || (lambda <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    if ((((realValue > epsilon) && (realValue < (epsilon + lambda)))) && std::all_of(distribution.cbegin(), distribution.cend(), [&epsilon, &lambda](const double &value){ return ((value > epsilon) && (value < (epsilon + lambda)));})) {

        QList<double> transformed;

        transformed.reserve(distribution.size());

        for (const double &value : distribution)
            transformed << gamma + eta *  std::log((value - epsilon) / (lambda + epsilon - value));

        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, eta, gamma, lambda, epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

    } else
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_su(const QList<T> &distribution, const JohnsonTransformation::Q &q)
{
    double eta = 2.0 * q.z / std::acosh(.5 * (q.xu / q.xm + q.xl / q.xm));

    double gamma = eta * std::asinh((q.xl / q.xm - q.xu / q.xm) / (2.0 * std::sqrt(q.xu * q.xl / (q.xm * q.xm) - 1.0)));

    double lambda = (2.0 * q.xm * std::sqrt(q.xu * q.xl / (q.xm * q.xm) - 1.0) /  (q.xu / q.xm + q.xl / q.xm - 2.0) / std::sqrt(q.xu / q.xm + q.xl / q.xm + 2.0));

    double epsilon = .5 * (q.x2 + q.x3 + q.xm * (q.xl / q.xm - q.xu / q.xm) / (q.xu / q.xm + q.xl / q.xm - 2.0));

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0) || (lambda <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SU, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    QList<double> transformed;

    transformed.reserve(distribution.size());

    for (const double &value : distribution)
        transformed << gamma + eta * std::asinh((value - epsilon) / lambda);

    return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SU, eta, gamma, lambda, epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_sl(const QList<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q)
{

    if (q.xu / q.xm <= 1.0)
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    double eta = 2.0 * q.z / std::log(q.xu / q.xm);

    double gamma = eta * std::log((q.xu / q.xm - 1.0) / std::sqrt(q.xu * q.xm));

    double epsilon = .5 * (q.x2 + q.x3 - q.xm * (q.xu / q.xm + 1.0) / (q.xu / q.xm - 1.0));

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    if ((realValue > epsilon) && std::all_of(distribution.cbegin(), distribution.cend(), [&epsilon](const double &value){ return (value > epsilon);})) {

        QList<double> transformed;

        transformed.reserve(distribution.size());

        for (const double &value : distribution)
            transformed << gamma + eta *  std::log(value - epsilon);

        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, eta, gamma, std::numeric_limits<double>::quiet_NaN(), epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

    } else
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_sb(const std::span<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q)
{

    double temp1 = q.xm / q.xu;

    double temp2 = q.xm / q.xl;

    double temp3 = (q.xm * q.xm / (q.xl / q.xu) - 1.0);

    double temp4 = (1.0 + temp1) * (1.0 + temp2);

    double temp5 = (temp2 - temp1);

    double eta = q.z / std::acosh(.5 * std::sqrt(temp4));

    double gamma = eta * std::asinh(temp5 * sqrt(temp4 - 4.0) / (2.0 * temp3));

    double temp = (temp4 - 2.0);

    double lambda = (q.xm * std::sqrt( temp * temp - 4.0) / temp3);

    double epsilon = .5 * (q.x2 + q.x3 - lambda + q.xm * temp5 / temp3);

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0) || (lambda <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    if ((((realValue > epsilon) && (realValue < (epsilon + lambda)))) && std::all_of(distribution.begin(), distribution.end(), [&epsilon, &lambda](const double &value){ return ((value > epsilon) && (value < (epsilon + lambda)));})) {

        QList<double> transformed;

        transformed.reserve(distribution.size());

        for (const double &value : distribution)
            transformed << gamma + eta *  std::log((value - epsilon) / (lambda + epsilon - value));

        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, eta, gamma, lambda, epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

    } else
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SB, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_su(const std::span<T> &distribution, const JohnsonTransformation::Q &q)
{
    double temp1 = q.xu / q.xm;

    double temp2 = q.xl / q.xm;

    double temp3 = temp1 + temp2;

    double temp4 = temp2 - temp1;

    double temp5 = std::sqrt(q.xu * q.xl / q.xm * q.xm - 1.0);

    double temp6 = temp5 - 2.0;

    double eta = 2.0 * q.z / std::acosh(.5 * temp3);

    double gamma = eta * std::asinh(temp4 / (2.0 * temp5));

    double lambda = (2.0 * q.xm * temp5 / temp6 / std::sqrt(temp3 + 2.0));

    double epsilon = .5 * (q.x2 + q.x3 + q.xm * temp4 / temp6);

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0) || (lambda <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SU, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    QList<double> transformed;

    transformed.reserve(distribution.size());

    for (const double &value : distribution)
        transformed << gamma + eta * std::asinh((value - epsilon) / lambda);

    return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SU, eta, gamma, lambda, epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::fit_sl(const std::span<T> &distribution, const T &realValue, const JohnsonTransformation::Q &q)
{

    double temp1 = q.xu / q.xm;

    double temp2 = temp1 - 1.0;

    if (temp1 <= 1.0)
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    double eta = 2.0 * q.z / std::log(temp1);

    double gamma = eta * std::log(temp2 / std::sqrt(q.xu * q.xm));

    double epsilon = .5 * (q.x2 + q.x3 - q.xm * (temp1 + 1.0) / temp2);

    if (std::isnan(gamma) || std::isnan(epsilon) || (eta <= 0))
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    if ((realValue > epsilon) && std::all_of(distribution.begin(), distribution.end(), [&epsilon](const double &value){ return (value > epsilon);})) {

        QList<double> transformed;

        transformed.reserve(distribution.size());

        for (const double &value : distribution)
            transformed << gamma + eta *  std::log(value - epsilon);

        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, eta, gamma, std::numeric_limits<double>::quiet_NaN(), epsilon, q.z, boost::math::statistics::anderson_darling_normality_statistic(transformed) / static_cast<double>(transformed.size())};

    } else
        return JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::SL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::getTransformParameters(const std::span<T> &distribution, const T &realValue, bool exclude_original, std::pair<double, double> z_lim, qsizetype z_length)
{

    QList<JohnsonTransformation::Parameters> params;

    params.reserve(z_length + 1);

    params << JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::ORIGINAL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), boost::math::statistics::anderson_darling_normality_statistic(distribution) / static_cast<double>(distribution.size())};

    double z_lim_delta = (z_lim.second - z_lim.first) / static_cast<double>(z_length);

    boost::math::normal normal_distribution(0.0, 1.0);

    JohnsonTransformation::Q q;

    for (double z = z_lim.first; z < z_lim.second; z += z_lim_delta) {

        double qtls_1 = MathDescriptives::percentileOfPresortedList(distribution, boost::math::cdf(normal_distribution, -3.0 * z));

        double qtls_2 = MathDescriptives::percentileOfPresortedList(distribution, boost::math::cdf(normal_distribution, -z));

        double qtls_3 = MathDescriptives::percentileOfPresortedList(distribution, boost::math::cdf(normal_distribution, z));

        double qtls_4 = MathDescriptives::percentileOfPresortedList(distribution, boost::math::cdf(normal_distribution, 3.0 * z));

        q.xl = qtls_2 - qtls_1;

        q.xm = qtls_3 - qtls_2;

        q.xu = qtls_4 - qtls_3;

        q.QR = (qtls_4 - qtls_3) * (qtls_2 - qtls_1) / ((qtls_3 - qtls_2) * (qtls_3 - qtls_2));

        q.z  = z;

        q.x2 = qtls_2;

        q.x3 = qtls_3;

        JohnsonTransformation::Parameters fitResult_sl = JohnsonTransformation::fit_sl(distribution, realValue, q);

        if (!std::isnan(fitResult_sl.eta))
            params << fitResult_sl;

        if (q.QR <= 1.0) {

            JohnsonTransformation::Parameters fitResult_sb = JohnsonTransformation::fit_sb(distribution, realValue, q);

            if (!std::isnan(fitResult_sb.eta))
                params << fitResult_sb;

        } else {

            JohnsonTransformation::Parameters fitResult_su = JohnsonTransformation::fit_su(distribution, q);

            if (!std::isnan(fitResult_su.eta))
                params << fitResult_su;

        }

    }

    if (exclude_original)
        return *std::min_element(params.begin() + 1, params.end(), [](const JohnsonTransformation::Parameters &a, const JohnsonTransformation::Parameters &b){return a.adStat < b.adStat;});
    else
        return *std::min_element(params.begin(), params.end(), [](const JohnsonTransformation::Parameters &a, const JohnsonTransformation::Parameters &b){return a.adStat < b.adStat;});

}

template <typename T>
JohnsonTransformation::Parameters JohnsonTransformation::getTransformParameters(const std::span<T> &distribution, const T &realValue, const QList<double> &z_values, bool exclude_original)
{

    QList<JohnsonTransformation::Parameters> params;

    params.reserve(z_values.count() / 5 + 1);

    params << JohnsonTransformation::Parameters{JohnsonTransformation::TransformType::ORIGINAL, std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), boost::math::statistics::anderson_darling_normality_statistic(distribution) / static_cast<double>(distribution.size())};

    JohnsonTransformation::Q q;

    for (qsizetype i = 0; i < z_values.count(); i += 5) {

        q.z  = z_values.at(i);

        double qtls_1 = MathDescriptives::percentileOfPresortedList(distribution, z_values.at(i + 1));

        double qtls_2 = MathDescriptives::percentileOfPresortedList(distribution, z_values.at(i + 2));

        double qtls_3 = MathDescriptives::percentileOfPresortedList(distribution, z_values.at(i + 3));

        double qtls_4 = MathDescriptives::percentileOfPresortedList(distribution, z_values.at(i + 4));

        q.xl = qtls_2 - qtls_1;

        q.xm = qtls_3 - qtls_2;

        q.xu = qtls_4 - qtls_3;

        q.QR = (q.xu) * (q.xl) / ((q.xm) * (q.xm));

        q.x2 = qtls_2;

        q.x3 = qtls_3;

        JohnsonTransformation::Parameters fitResult_sl = JohnsonTransformation::fit_sl(distribution, realValue, q);

        if (!std::isnan(fitResult_sl.eta))
            params << fitResult_sl;

        if (q.QR <= 1.0) {

            JohnsonTransformation::Parameters fitResult_sb = JohnsonTransformation::fit_sb(distribution, realValue, q);

            if (!std::isnan(fitResult_sb.eta))
                params << fitResult_sb;

        } else {

            JohnsonTransformation::Parameters fitResult_su = JohnsonTransformation::fit_su(distribution, q);

            if (!std::isnan(fitResult_su.eta))
                params << fitResult_su;

        }

    }

    if (exclude_original)
        return *std::min_element(params.begin() + 1, params.end(), [](const JohnsonTransformation::Parameters &a, const JohnsonTransformation::Parameters &b){return a.adStat < b.adStat;});
    else
        return *std::min_element(params.begin(), params.end(), [](const JohnsonTransformation::Parameters &a, const JohnsonTransformation::Parameters &b){return a.adStat < b.adStat;});

}
template <typename T>
void JohnsonTransformation::transform(QList<T> &distribution, JohnsonTransformation::Parameters parameters)
{

    if (parameters.functionType == TransformType::ORIGINAL)
        return;
    else if (parameters.functionType == TransformType::SL) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta *  std::log(value - parameters.epsilon);

    } else if (parameters.functionType == TransformType::SB) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta *  std::log((value - parameters.epsilon) / (parameters.lambda + parameters.epsilon - value));

    } else if (parameters.functionType == TransformType::SU) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta * std::asinh((value - parameters.epsilon) / parameters.lambda);

    }

}

template <typename T>
void JohnsonTransformation::transform(std::span<T> &distribution, JohnsonTransformation::Parameters parameters)
{

    if (parameters.functionType == TransformType::ORIGINAL)
        return;
    else if (parameters.functionType == TransformType::SL) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta *  std::log(value - parameters.epsilon);

    } else if (parameters.functionType == TransformType::SB) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta *  std::log((value - parameters.epsilon) / (parameters.lambda + parameters.epsilon - value));

    } else if (parameters.functionType == TransformType::SU) {

        for (double &value : distribution)
            value = parameters.gamma + parameters.eta * std::asinh((value - parameters.epsilon) / parameters.lambda);

    }

}

template <typename T>
void JohnsonTransformation::transform(T &value, JohnsonTransformation::Parameters parameters)
{

    if (parameters.functionType == TransformType::ORIGINAL)
        return;
    else if (parameters.functionType == TransformType::SL)
        value = parameters.gamma + parameters.eta *  std::log(value - parameters.epsilon);
    else if (parameters.functionType == TransformType::SB)
        value = parameters.gamma + parameters.eta *  std::log((value - parameters.epsilon) / (parameters.lambda + parameters.epsilon - value));
    else if (parameters.functionType == TransformType::SU)
        value = parameters.gamma + parameters.eta * std::asinh((value - parameters.epsilon) / parameters.lambda);

}

#endif // JOHNSONTRANSFORMATION_H
