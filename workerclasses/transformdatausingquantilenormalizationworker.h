#ifndef TRANSFORMDATAUSINGQUANTILENORMALIZATIONWORKER_H
#define TRANSFORMDATAUSINGQUANTILENORMALIZATIONWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"

class TransformDataUsingQuantileNormalizationParameters : public BaseParameters
{

public:

    TransformDataUsingQuantileNormalizationParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QString labelOfDescriptiveFunction;

    std::function<double (const QList<double> &)> descriptiveFunction;

    qsizetype numberOfThreadsToUse;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class TransformDataUsingQuantileNormalizationWorker : public BaseWorker
{

public:

    TransformDataUsingQuantileNormalizationWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void TransformDataUsingQuantileNormalizationWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<TransformDataUsingQuantileNormalizationParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingQuantileNormalizationParameters>());

    // Fetching data
    ////////////////

    QUuid uuidProgress = this->startProgress("fetching data", 0, 0);

    QList<std::reference_wrapper<T>> matrix;

    if (parameters->orientation == Qt::Vertical)
        matrix = annotatedMatrix->sliced_references(parameters->selectedVariableIndexes, parameters->selectedItemIndexes);
    else {

        matrix = annotatedMatrix->sliced_references(parameters->selectedItemIndexes, parameters->selectedVariableIndexes);

        MathOperations::transposeInplace(matrix, parameters->selectedVariableIndexes.count(), parameters->numberOfThreadsToUse);

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // sorting data
    ////////////////

    this->startProgress("sorting vectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedItemIndexes.count()), [](std::span<std::reference_wrapper<T>> &span){std::sort(span.begin(), span.end());}));

    if (QThread::currentThread()->isInterruptionRequested())
        return ;

    this->checkIfPauseWasRequested();

    ////////////////

    // transposing data
    ////////////////

    uuidProgress = this->startProgress("transposing data", 0, 0);

    MathOperations::transposeInplace(matrix, parameters->selectedItemIndexes.count(), parameters->numberOfThreadsToUse);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);

    ////////////////

    // transforming data
    ////////////////

    this->startProgress("transforming data", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(matrix, parameters->selectedVariableIndexes.count()), [&parameters](std::span<std::reference_wrapper<T>> &span){QList<double> temp(span.begin(), span.end()); double descriptive = parameters->descriptiveFunction(temp); for (std::reference_wrapper<T> &value : span) value.get() = descriptive;}));

    if (QThread::currentThread()->isInterruptionRequested())
        return ;

    this->checkIfPauseWasRequested();

    ////////////////

}

#endif // TRANSFORMDATAUSINGQUANTILENORMALIZATIONWORKER_H
