#include "guiltbyassociationanalysisdialog.h"

GuiltByAssociationAnalysisDialog::GuiltByAssociationAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "guilt-by-association analysis", "guiltbyassociationalysisdialog")
{

    GuiltByAssociationAnalysisParameters *guiltByAssociationAnalysisParameters = new GuiltByAssociationAnalysisParameters;

    guiltByAssociationAnalysisParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<GuiltByAssociationAnalysisParameters>(guiltByAssociationAnalysisParameters);

    BaseDialog::initWidgets();

    this->connect(this, &GuiltByAssociationAnalysisDialog::accepted, this, &GuiltByAssociationAnalysisDialog::dialogAccepted);

}

void GuiltByAssociationAnalysisDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &GuiltByAssociationAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items to include in the barcode");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &GuiltByAssociationAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes;});


    SelectMultipleFilesWidget *selectMultipleFilesWidget = new SelectMultipleFilesWidget(this, "selectMultipleFilesWidget");

    selectMultipleFilesWidget->label()->setText("files containing sets with variables used for generating barcodes");

    this->connect(selectMultipleFilesWidget, &SelectMultipleFilesWidget::pathsChanged, [this](const QList<QString> &paths){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->pathToSetCollections = paths;});

    selectMultipleFilesWidget->readWidgetSettings();

    d_gridLayout->addWidget(selectMultipleFilesWidget, 2, 0, 1, 2);


    d_comboBoxWidget_labelForMatching = new ComboBoxWidget(this, "d_comboBoxWidget_labelForMatching");

    this->connect(d_comboBoxWidget_labelForMatching->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->labelForMatching = text;});

    this->connect(d_comboBoxWidget_labelForMatching->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->indexForMatching = ((index == 0) ? -1 : index - 1);});

    d_comboBoxWidget_labelForMatching->label()->setText("label for matching between variables and members of sets");

    d_gridLayout->addWidget(d_comboBoxWidget_labelForMatching, 3, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_barcodeFunction = new ComboBoxWidget(this, "comboBoxWidget_barcodeFunction");

    this->connect(comboBoxWidget_barcodeFunction->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->barcodeFunction = text;});

    comboBoxWidget_barcodeFunction->comboBox()->addItems({"Student T", "Welch T", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Fisher exact", "mean", "median"});

    comboBoxWidget_barcodeFunction->comboBox()->setCurrentIndex(0);

    comboBoxWidget_barcodeFunction->label()->setText("barcode function");

    comboBoxWidget_barcodeFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_barcodeFunction, 3, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_minimumSizeOfSet = new SpinBoxWidget(this, "spinBoxWidget_minimumSizeOfSet");

    this->connect(spinBoxWidget_minimumSizeOfSet->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->minimumSizeOfSet = value;});

    spinBoxWidget_minimumSizeOfSet->label()->setText("minimum size of set");

    spinBoxWidget_minimumSizeOfSet->spinBox()->setRange(1, 100000);

    spinBoxWidget_minimumSizeOfSet->spinBox()->setValue(10);

    spinBoxWidget_minimumSizeOfSet->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_minimumSizeOfSet->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_minimumSizeOfSet, 4, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_maximumSizeOfSet = new SpinBoxWidget(this, "spinBoxWidget_maximumSizeOfSet");

    this->connect(spinBoxWidget_maximumSizeOfSet->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->maximumSizeOfSet = value;});

    spinBoxWidget_maximumSizeOfSet->label()->setText("maximum size of set");

    spinBoxWidget_maximumSizeOfSet->spinBox()->setRange(1, 100000);

    spinBoxWidget_maximumSizeOfSet->spinBox()->setValue(500);

    spinBoxWidget_maximumSizeOfSet->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_maximumSizeOfSet->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_maximumSizeOfSet, 4, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_associationFunction = new ComboBoxWidget(this, "comboBoxWidget_associationFunction");

    this->connect(comboBoxWidget_associationFunction->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->associationFunction = text;});

    comboBoxWidget_associationFunction->comboBox()->addItems({"Pearson R", "Spearman R", "Distance R", "rank-biased overlap"});

    comboBoxWidget_associationFunction->comboBox()->setCurrentIndex(0);

    comboBoxWidget_associationFunction->label()->setText("function to determining association between variable and barcode");

    comboBoxWidget_associationFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_associationFunction, 5, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_permutationTestMode = new ComboBoxWidget(this, "comboBoxWidget_permutationTestMode");

    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->enablePermutationMode = (text == "enable") ? true : false;});

    comboBoxWidget_permutationTestMode->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_permutationTestMode->label()->setText("permutation test mode");

    comboBoxWidget_permutationTestMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_permutationTestMode, 6, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfPermutations = new SpinBoxWidget(this, "spinBoxWidget_numberOfPermutations");

    this->connect(spinBoxWidget_numberOfPermutations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfPermutations = value;});

    spinBoxWidget_numberOfPermutations->label()->setText("number of permutations");

    spinBoxWidget_numberOfPermutations->spinBox()->setRange(1, 10000000);

    spinBoxWidget_numberOfPermutations->spinBox()->setValue(1000);

    spinBoxWidget_numberOfPermutations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfPermutations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfPermutations, 6, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_falseDiscoveryRate = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_falseDiscoveryRate");

    this->connect(doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->falseDiscoveryRate = value / 100.0;});

    doubleSpinBoxWidget_falseDiscoveryRate->label()->setText("false discovery rate to control for in permutation mode");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setValue(5);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_falseDiscoveryRate->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_falseDiscoveryRate, 7, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_confidenceLevel = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_confidenceLevel");

    this->connect(doubleSpinBoxWidget_confidenceLevel->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->confidenceLevel = value / 100.0;});

    doubleSpinBoxWidget_confidenceLevel->label()->setText("confidence level for multivariate threshold in permutation mode");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setValue(80);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_confidenceLevel->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_confidenceLevel, 7, 1, 1, 1);


    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [spinBoxWidget_numberOfPermutations, doubleSpinBoxWidget_confidenceLevel, doubleSpinBoxWidget_falseDiscoveryRate](const QString &text) {

        if (text == "enable") {

            spinBoxWidget_numberOfPermutations->setEnabled(true);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(true);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(true);

        } else {

            spinBoxWidget_numberOfPermutations->setEnabled(false);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(false);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(false);

        };
    });

    spinBoxWidget_numberOfPermutations->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_confidenceLevel->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");


    d_itemSelectorWidget_annotationLabelsForVariables = new ItemSelectorWidget(this, "d_itemSelectorWidget_annotationLabelsForVariables");

    d_itemSelectorWidget_annotationLabelsForVariables->label()->setText("select annotation labels for variables");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &GuiltByAssociationAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_annotationLabelsForVariables, 8, 0, 1, 1);

    this->connect(d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->annotationLabelsForVariables = itemLabels;});


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"barcodes per set collection", "guilt-by-association metrics per set collection"});

    //itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 8, 1, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 9, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 9, 1, 1, 1);


    this->orientationChanged(d_orientationWidget->orientation());

}

void GuiltByAssociationAnalysisDialog::orientationChanged(Qt::Orientation orientation)
{

    GuiltByAssociationAnalysisParameters *guiltByAssociationAnalysisParameters = static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data());


    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->identifiers(orientation, true), guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));


    QList<QString> itemLabels;

    itemLabels.append((orientation == Qt::Horizontal) ? "COLUMN IDENTIFIER" : "ROW IDENTIFIER");

    for (const QString &identifier : guiltByAssociationAnalysisParameters->baseAnnotatedMatrix->annotationLabels(orientation, false))
        itemLabels.append(identifier);

    d_comboBoxWidget_labelForMatching->comboBox()->clear();

    d_comboBoxWidget_labelForMatching->comboBox()->addItems(itemLabels);


    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(itemLabels);

    d_itemSelectorWidget_annotationLabelsForVariables->itemSelectorModel_selected()->clear();

}

void GuiltByAssociationAnalysisDialog::dialogAccepted()
{

    GuiltByAssociationAnalysisParameters *guiltByAssociationAnalysisParameters = static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data());

    if (guiltByAssociationAnalysisParameters->barcodeFunction == "Fisher exact") {

        double threshold = 3.0;

        QSettings settings;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("thresholdFisherExactTest"))
           threshold = settings.value("thresholdFisherExactTest").toDouble();

        settings.endGroup();

        threshold = QInputDialog::getDouble(this, QCoreApplication::applicationName() + " - input dialog", "select threshold for Fisher Exact test", threshold, 0.0);

        static_cast<GuiltByAssociationAnalysisParameters *>(BaseDialog::d_parameters.data())->thresholdForFisherExact = threshold;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("thresholdFisherExactTest", threshold);

        settings.endGroup();

    }

    if (guiltByAssociationAnalysisParameters->associationFunction == "rank-biased overlap") {

        double p_rankBiasedOverlap = 0.9;

        QSettings settings;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("p_rankBiasedOverlap"))
           p_rankBiasedOverlap = settings.value("p_rankBiasedOverlap").toDouble();

        settings.endGroup();

        p_rankBiasedOverlap = QInputDialog::getDouble(this, QCoreApplication::applicationName() + " - input dialog", "select p parameter for rank-biased overlap", p_rankBiasedOverlap, 0.0, 1.0, 2);

        guiltByAssociationAnalysisParameters->p_rankBiasedOverlap = p_rankBiasedOverlap;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("p_rankBiasedOverlap", p_rankBiasedOverlap);

        settings.endGroup();


        int maxDepth_rankBiasedOverlap = 250;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("maxDepth_rankBiasedOverlap"))
           maxDepth_rankBiasedOverlap = settings.value("maxDepth_rankBiasedOverlap").toInt();

        settings.endGroup();

        maxDepth_rankBiasedOverlap = QInputDialog::getInt(this, QCoreApplication::applicationName() + " - input dialog", "select maximum depth for rank-biased overlap", maxDepth_rankBiasedOverlap, 1);

        guiltByAssociationAnalysisParameters->maxDepth_rankBiasedOverlap = maxDepth_rankBiasedOverlap;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("maxDepth_rankBiasedOverlap", maxDepth_rankBiasedOverlap);

        settings.endGroup();

    }

}
