#ifndef LIPTAKSMETAANALYSISAPPROACH_H
#define LIPTAKSMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/normal.hpp"

#include "basemetaanalysisapproach.h"

class LiptaksMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    LiptaksMetaAnalysisApproach();

    LiptaksMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~LiptaksMetaAnalysisApproach() {}

private:

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // LIPTAKSMETAANALYSISAPPROACH_H
