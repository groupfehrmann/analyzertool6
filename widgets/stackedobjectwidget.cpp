#include "stackedobjectwidget.h"

StackedObjectWidget::StackedObjectWidget(QWidget *parent) :
    QStackedWidget(parent)
{

}

void StackedObjectWidget::addObject(const QUuid &uuidObject, const QSharedPointer<QObject> &object)
{

    QString objectType = object.data()->property("objectType").toString();

    if (objectType == "annotated matrix") {

        QWidget *widget = new AnnotatedMatrixWidget(nullptr, object);

        d_uuidObjectToWidget.insert(uuidObject, widget);

        this->addWidget(widget);


    } else if (objectType == "result") {

        QString resultType = object.data()->property("resultType").toString();

        if (resultType == "table") {

            QWidget *widget = new ResultTableWidget(nullptr, object);

            d_uuidObjectToWidget.insert(uuidObject, widget);

            this->addWidget(widget);

        }

    }

    this->setCurrentWidget(d_uuidObjectToWidget.value(uuidObject));

}

void StackedObjectWidget::removeAll()
{

    for (const QUuid &uuidObject : d_uuidObjectToWidget.keys())
        this->removeObject(uuidObject);

}

void StackedObjectWidget::removeObject(const QUuid &uuidObject)
{

    if (!d_uuidObjectToWidget.contains(uuidObject))
        return;

    this->removeWidget(d_uuidObjectToWidget.value(uuidObject));

    delete d_uuidObjectToWidget.value(uuidObject);

    d_uuidObjectToWidget.remove(uuidObject);

}

void StackedObjectWidget::showObject(const QUuid &uuidObject)
{

    if (!d_uuidObjectToWidget.contains(uuidObject)) {

        this->setCurrentIndex(0);

        return;

    }

    this->setCurrentWidget(d_uuidObjectToWidget.value(uuidObject));

}
