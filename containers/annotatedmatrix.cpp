#include "annotatedmatrix.h"

BaseAnnotatedMatrixData::BaseAnnotatedMatrixData()
{

}

BaseAnnotatedMatrixData::BaseAnnotatedMatrixData(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers)
{

    this->setDataForIdentifiers(rowIdentifiers, Qt::Vertical);

    this->setDataForIdentifiers(columnIdentifiers, Qt::Horizontal);

}

BaseAnnotatedMatrixData::BaseAnnotatedMatrixData(qsizetype rowCount, qsizetype columnCount, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier)
{

    this->setDataForIdentifiers(rowCount, Qt::Vertical, prefixRowIdentifier);

    this->setDataForIdentifiers(columnCount, Qt::Horizontal, prefixColumnIdentifier);

}

void BaseAnnotatedMatrixData::setDataForIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation)
{

    QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_columnIdentifiers : d_rowIdentifiers);

    QMultiHash<QString, qsizetype> &d_identifierToIndexes((orientation == Qt::Horizontal) ? d_columnIdentifierToIndexes : d_rowIdentifierToIndexes);

    QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_columnIdentifierSelectionStatus : d_rowIdentifierSelectionStatus);


    d_identifiers = identifiers;

    for (qsizetype i = 0; i < identifiers.count(); ++i)
        d_identifierToIndexes.insert(identifiers.at(i), i);

    d_selectionStatus.fill(true, identifiers.count());

}

void BaseAnnotatedMatrixData::setDataForIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier)
{

    QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_columnIdentifiers : d_rowIdentifiers);

    QMultiHash<QString, qsizetype> &d_identifierToIndexes((orientation == Qt::Horizontal) ? d_columnIdentifierToIndexes : d_rowIdentifierToIndexes);

    QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_columnIdentifierSelectionStatus : d_rowIdentifierSelectionStatus);


    d_identifiers.reserve(count);

    d_identifierToIndexes.reserve(count);

    for (qsizetype i = 0; i < count; ++i) {

        QString identifier = prefixIdentifier + QString::number(i);

        d_identifiers.append(identifier);

        d_identifierToIndexes.insert(identifier, i);

    }

    d_selectionStatus.fill(true, count);

}

BaseAnnotatedMatrix::BaseAnnotatedMatrix(BaseAnnotatedMatrix &&baseAnnotatedMatrix) :
    QObject(baseAnnotatedMatrix.parent()),
    d_data(std::move(baseAnnotatedMatrix.d_data))
{

    this->setProperty("objectType", "annotated matrix");

}

BaseAnnotatedMatrix::BaseAnnotatedMatrix(QObject *parent) :
    QObject(parent),
    d_data(new BaseAnnotatedMatrixData)
{

    this->setProperty("objectType", "annotated matrix");

}

BaseAnnotatedMatrix::BaseAnnotatedMatrix(const BaseAnnotatedMatrix &baseAnnotatedMatrix) :
    QObject(baseAnnotatedMatrix.parent()),
    d_data(baseAnnotatedMatrix.d_data)
{

    this->setProperty("objectType", "annotated matrix");

}

BaseAnnotatedMatrix::BaseAnnotatedMatrix(const QList<QString> &rowIdentifiers, const QList<QString> &columnIdentifiers, QObject *parent) :
    QObject(parent),
    d_data(new BaseAnnotatedMatrixData(rowIdentifiers, columnIdentifiers))
{

    this->setProperty("objectType", "annotated matrix");

}

BaseAnnotatedMatrix::BaseAnnotatedMatrix(qsizetype rowCount, qsizetype columnCount, const QString &prefixRowIdentifier, const QString &prefixColumnIdentifier, QObject *parent) :
    QObject(parent),
    d_data(new BaseAnnotatedMatrixData(rowCount, columnCount, prefixRowIdentifier, prefixColumnIdentifier))
{

    this->setProperty("objectType", "annotated matrix");

}

const QString &BaseAnnotatedMatrix::annotationLabel(qsizetype index, Qt::Orientation orientation) const
{

    Q_ASSERT_X((index >= 0) && (index < this->annotationLabelCount(orientation)), "const QString &BaseAnnotatedMatrix::annotationLabel(qsizetype index, Qt::Orientation orientation) const", "index is out of range");

    return this->annotationLabels(orientation).at(index);

}

qsizetype BaseAnnotatedMatrix::annotationLabelCount(Qt::Orientation orientation, bool selectedOnly) const
{

    if (selectedOnly)
        return (orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus.count(true) : d_data->d_rowAnnotationLabelSelectionStatus.count(true);
    else
        return this->annotationLabels(orientation).count();

}

QList<qsizetype> BaseAnnotatedMatrix::annotationLabelIndexes(Qt::Orientation orientation, bool selectedOnly) const
{

    const QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);

    QList<qsizetype> indexes(d_annotationLabelSelectionStatus.count());

    std::iota(indexes.begin(), indexes.end(), 0);

    if (!selectedOnly)
        return indexes;

    QList<qsizetype> selectedindexes;

    for (qsizetype i = 0; i < indexes.count(); ++i)
        if (d_annotationLabelSelectionStatus.at(i))
            selectedindexes.append(indexes.at(i));

    return selectedindexes;

}

const QList<QString> &BaseAnnotatedMatrix::annotationLabels(Qt::Orientation orientation) const
{

    return (orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels;

}

QList<QString> BaseAnnotatedMatrix::annotationLabels(Qt::Orientation orientation, bool selectedOnly) const
{

    const QList<QString> &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);

    const QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);

    if (selectedOnly) {

        QList<QString> annotationLabels;

        for (qsizetype i = 0; i < d_annotationLabelSelectionStatus.count(); ++i) {

            if (d_annotationLabelSelectionStatus.at(i))
                annotationLabels.append(d_annotationLabels.at(i));

        }

        return annotationLabels;

    } else
        return d_annotationLabels;

}

QList<QString> BaseAnnotatedMatrix::annotationLabels(const QList<qsizetype> indexes, Qt::Orientation orientation) const
{

    Q_ASSERT_X(std::all_of(indexes.begin(), indexes.end(), [this, orientation](qsizetype index){return (index >= 0) && (index < this->annotationLabelCount(orientation));}), "QList<QString> BaseAnnotatedMatrix::annotationLabels(const QList<qsizetype> indexes, Qt::Orientation orientation) const", "index is out of range");


    QList<QString> annotationLabels;

    annotationLabels.reserve(indexes.count());

    const QList<QString> &d_annotationLabels(this->annotationLabels(orientation));

    for (qsizetype index : indexes)
        annotationLabels << d_annotationLabels.at(index);

    return annotationLabels;

}

QVariant BaseAnnotatedMatrix::annotationValue(const QString &identifier, const QString &annotationLabel, Qt::Orientation orientation) const
{

    const QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    const QMultiHash<QString, qsizetype> &d_identifierToIndexes((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes: d_data->d_rowIdentifierToIndexes);

    const QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);


    qsizetype indexOfAnnotationLabel = d_annotationLabelToIndex.value(annotationLabel, -1);

    if (indexOfAnnotationLabel == -1)
        return QVariant();

    if (d_identifierToIndexes.contains(identifier) && d_identifierToAnnotations.contains(identifier))
        return d_identifierToAnnotations.value(identifier).at(indexOfAnnotationLabel);
    else
        return QVariant();

}

QVariant BaseAnnotatedMatrix::annotationValue(qsizetype indexOfIdentifier, qsizetype indexOfAnnotationLabel, Qt::Orientation orientation) const
{

    const QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    const QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers);

    const QList<QString> &d_annotationLabels(this->annotationLabels(orientation));


    Q_ASSERT_X((indexOfIdentifier >= 0) && (indexOfIdentifier < d_identifiers.count()), "QVariant BaseAnnotatedMatrix::annotationValue(qsizetype indexOfIdentifier, qsizetype indexOfAnnotationLabel, Qt::Orientation orientation) const", "indexOfIdentifier is out of range");

    Q_ASSERT_X((indexOfAnnotationLabel >= 0) && (indexOfAnnotationLabel < d_annotationLabels.count()), "QVariant BaseAnnotatedMatrix::annotationValue(qsizetype indexOfIdentifier, qsizetype indexOfAnnotationLabel, Qt::Orientation orientation) const", "indexOfAnnotationLabel is out of range");


    const QString &identifier(d_identifiers.at(indexOfIdentifier));

    if (d_identifierToAnnotations.contains(identifier))
        return d_identifierToAnnotations.value(identifier).at(indexOfAnnotationLabel);
    else
        return QVariant();

}

QList<QString> BaseAnnotatedMatrix::appendAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(this->annotationLabelCount(orientation), {annotationLabel}, orientation);

}

QList<QString> BaseAnnotatedMatrix::appendAnnotationLabels(const QList<QString> &annotationLabels, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(this->annotationLabelCount(orientation), annotationLabels, orientation);

}

QList<QString> BaseAnnotatedMatrix::appendAnnotationLabels(qsizetype count, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(this->annotationLabelCount(orientation), QList<QString>(count, "label"), orientation);

}

bool BaseAnnotatedMatrix::annotationLabelSelectionStatus(qsizetype index, Qt::Orientation orientation) const
{

    Q_ASSERT_X((index >= 0) && (index < this->annotationLabelCount(orientation)), "bool BaseAnnotatedMatrix::annotationLabelSelectionStatus(qsizetype index, Qt::Orientation orientation) const", "index is out of range");

    return (orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus.at(index) : d_data->d_rowAnnotationLabelSelectionStatus.at(index);

}

void BaseAnnotatedMatrix::beginResetColumnAnnotations()
{

    emit this->resetColumnAnnotationsAboutToBegin();

}

void BaseAnnotatedMatrix::beginResetData()
{

    emit this->resetDataAboutToBegin();

}

void BaseAnnotatedMatrix::beginResetRowAnnotations()
{

    emit this->resetRowAnnotationsAboutToBegin();

}

bool BaseAnnotatedMatrix::containsAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation) const
{

    return (orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex.contains(annotationLabel) : d_data->d_rowAnnotationLabelToIndex.contains(annotationLabel);

}

bool BaseAnnotatedMatrix::containsIdentifier(const QString &identifier, Qt::Orientation orientation, bool selectedOnly) const
{

    if (selectedOnly)
        return !this->indexes(identifier, orientation, selectedOnly).isEmpty();
    else
        return (orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes.contains(identifier) : d_data->d_rowIdentifierToIndexes.contains(identifier);

}

void BaseAnnotatedMatrix::endResetColumnAnnotations()
{

    emit this->columnAnnotationsIsReset();

}

void BaseAnnotatedMatrix::endResetData()
{

    emit this->dataIsReset();

}

void BaseAnnotatedMatrix::endResetRowAnnotations()
{

    emit this->rowAnnotationsIsReset();

}

const QString &BaseAnnotatedMatrix::identifier(qsizetype index, Qt::Orientation orientation) const
{

    Q_ASSERT_X((index >= 0) && (index < this->identifierCount(orientation)), "const QString &BaseAnnotatedMatrix::identifier(qsizetype index, Orientation orientation) const", "index is out of range");


    return this->identifiers(orientation).at(index);

}

qsizetype BaseAnnotatedMatrix::identifierCount(Qt::Orientation orientation, bool selectedOnly) const
{

    if (selectedOnly)
        return (orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus.count(true) : d_data->d_rowIdentifierSelectionStatus.count(true);
    else
        return this->identifiers(orientation).count();

}

const QList<QString> &BaseAnnotatedMatrix::identifiers(Qt::Orientation orientation) const
{

    return (orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers;

}


QList<QString> BaseAnnotatedMatrix::identifiers(Qt::Orientation orientation, bool selectedOnly) const
{

    const QList<QString> &d_identifiers(this->identifiers(orientation));

    const QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);


    if (selectedOnly) {

        QList<QString> identifiers;

        for (qsizetype i = 0; i < d_identifiers.count(); ++i)
            if (d_selectionStatus.at(i))
                identifiers.append(d_identifiers.at(i));

        return identifiers;

    } else
        return d_identifiers;

}

QList<qsizetype> BaseAnnotatedMatrix::indexes(const QString &identifier, Qt::Orientation orientation, bool selectedOnly) const
{

    QList<qsizetype> allIndexes = (orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes.values(identifier) : d_data->d_rowIdentifierToIndexes.values(identifier);

    const QList<bool> &selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);


    std::sort(allIndexes.begin(), allIndexes.end(), std::less<qsizetype>());

    if (selectedOnly) {

        QList<qsizetype> onlySelectedIndexes;

        for (qsizetype index : allIndexes) {

            if (selectionStatus.at(index))
                onlySelectedIndexes.append(index);

        }

        return onlySelectedIndexes;

    } else
        return allIndexes;

}

QList<qsizetype> BaseAnnotatedMatrix::indexes(Qt::Orientation orientation, bool selectedOnly) const
{

    const QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);

    QList<qsizetype> indexes(d_selectionStatus.count());

    std::iota(indexes.begin(), indexes.end(), 0);

    if (selectedOnly) {

        QList<qsizetype> indexesSelectedOnly;

        for (qsizetype i = 0; i < indexes.count(); ++i)
            if (d_selectionStatus.at(i))
                indexesSelectedOnly.append(indexes.at(i));

        return indexesSelectedOnly;

    } else
        return indexes;

}

QList<QString> BaseAnnotatedMatrix::insertAnnotationLabel(qsizetype index, const QString &annotationLabel, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(index, {annotationLabel}, orientation);

}

QList<QString> BaseAnnotatedMatrix::insertAnnotationLabels(qsizetype index, const QList<QString> &annotationLabels, Qt::Orientation orientation)
{

    QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    QList<QString> &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);

    QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);


    Q_ASSERT_X((index >= 0) && (index <= d_annotationLabels.count()), "bool BaseAnnotatedMatrix::insertAnnotationLabels(qsizetype index, const QList<QString> &annotationLabels, Qt::Orientation orientation)", "index is out of range");


    d_annotationLabels.resize(d_annotationLabels.count() + annotationLabels.count());

    d_annotationLabelSelectionStatus.resize(d_annotationLabelSelectionStatus.count() + annotationLabels.count());

    std::move_backward(std::next(d_annotationLabels.begin(), index), std::prev(d_annotationLabels.end(), annotationLabels.count()), d_annotationLabels.end());

    std::move_backward(std::next(d_annotationLabelSelectionStatus.begin(), index), std::prev(d_annotationLabelSelectionStatus.end(), annotationLabels.count()), d_annotationLabelSelectionStatus.end());

    QList<QString> annotationLabelsInserted;

    for (qsizetype i = 0; i < annotationLabels.count(); ++i) {

        const QString &annotationLabel(annotationLabels.at(i));

        if (!d_annotationLabelToIndex.contains(annotationLabel)) {

            d_annotationLabelToIndex.insert(annotationLabel, -1);

            d_annotationLabels[i + index] = annotationLabel;

            d_annotationLabelSelectionStatus[i + index] = true;

            annotationLabelsInserted.append(annotationLabel);

        } else {

            qsizetype counter = 1;

            while(d_annotationLabelToIndex.contains(annotationLabel + "_" + QString::number(counter)))
                ++counter;

            d_annotationLabelToIndex.insert(annotationLabel + "_" + QString::number(counter), -1);

            d_annotationLabels[i + index] = annotationLabel + "_" + QString::number(counter);

            d_annotationLabelSelectionStatus[i + index] = true;

            annotationLabelsInserted.append(annotationLabel + "_" + QString::number(counter));

        }

    }

    QMutableHashIterator<QString, QList<QVariant> > it(d_identifierToAnnotations);

    while (it.hasNext())
       it.next().value().insert(index, annotationLabels.count(), QVariant());

    d_annotationLabelToIndex.clear();

    for (qsizetype i = 0; i < d_annotationLabels.count(); ++i)
        d_annotationLabelToIndex.insert(d_annotationLabels.at(i), i);

    return annotationLabelsInserted;

}

QList<QString> BaseAnnotatedMatrix::insertAnnotationLabels(qsizetype index, qsizetype count, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(index, QList<QString>(count, "label"), orientation);

}

void BaseAnnotatedMatrix::lockForRead()
{

    d_readWriteLock.lockForRead();

}

void BaseAnnotatedMatrix::lockForWrite()
{

    d_readWriteLock.lockForWrite();

}

QList<QString> BaseAnnotatedMatrix::nonSelectedAnnotationLabels(Qt::Orientation orientation) const
{

    const QList<QString>  &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);

    const QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);

    QList<QString> nonSelectedLabels;

    for (qsizetype i = 0; i < d_annotationLabels.count(); ++i)
        if (!d_annotationLabelSelectionStatus.at(i))
            nonSelectedLabels.append(d_annotationLabels.at(i));

    return nonSelectedLabels;

}

QList<qsizetype> BaseAnnotatedMatrix::nonSelectedAnnotationLabelIndexes(Qt::Orientation orientation) const
{

    const QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);

    QList<qsizetype> indexes(d_annotationLabelSelectionStatus.count());

    std::iota(indexes.begin(), indexes.end(), 0);

    QList<qsizetype> nonSelectedindexes;

    for (qsizetype i = 0; i < indexes.count(); ++i)
        if (!d_annotationLabelSelectionStatus.at(i))
            nonSelectedindexes.append(indexes.at(i));

    return nonSelectedindexes;

}

QList<QString> BaseAnnotatedMatrix::nonSelectedIdentifiers(Qt::Orientation orientation) const
{

    const QList<QString> &d_identifiers(this->identifiers(orientation));

    const QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);


    QList<QString> identifiers;

    for (qsizetype i = 0; i < d_identifiers.count(); ++i)
        if (!d_selectionStatus.at(i))
            identifiers.append(d_identifiers.at(i));

    return identifiers;

}

QList<qsizetype> BaseAnnotatedMatrix::nonSelectedIdentifierIndexes(Qt::Orientation orientation) const
{

    const QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);

    QList<qsizetype> indexes(d_selectionStatus.count());

    std::iota(indexes.begin(), indexes.end(), 0);

    QList<qsizetype> nonSelectedindexes;

    for (qsizetype i = 0; i < indexes.count(); ++i)
        if (!d_selectionStatus.at(i))
            nonSelectedindexes.append(indexes.at(i));

    return nonSelectedindexes;

}

BaseAnnotatedMatrix &BaseAnnotatedMatrix::operator=(const BaseAnnotatedMatrix &baseAnnotatedMatrix)
{

    if (this != &baseAnnotatedMatrix) {

        this->setParent(baseAnnotatedMatrix.parent());

        d_data.operator=(baseAnnotatedMatrix.d_data);

    }

    return *this;

}

BaseAnnotatedMatrix &BaseAnnotatedMatrix::operator=(BaseAnnotatedMatrix &&baseAnnotatedMatrix)
{

    if (this != &baseAnnotatedMatrix) {

        this->setParent(baseAnnotatedMatrix.parent());

        d_data.operator=(baseAnnotatedMatrix.d_data);

    }

    return *this;

}

QList<QString> BaseAnnotatedMatrix::prependAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(0, {annotationLabel}, orientation);

}

QList<QString> BaseAnnotatedMatrix::prependAnnotationLabels(const QList<QString> &annotationLabels, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(0, annotationLabels, orientation);

}

QList<QString> BaseAnnotatedMatrix::prependAnnotationLabels(qsizetype count, Qt::Orientation orientation)
{

    return this->insertAnnotationLabels(0, QList<QString>(count, "label"), orientation);

}

void BaseAnnotatedMatrix::removeAnnotationLabel(const QString &annotationLabel, Qt::Orientation orientation)
{

    if (orientation == Qt::Horizontal)
        this->removeAnnotationLabel(d_data->d_columnAnnotationLabelToIndex.value(annotationLabel), orientation);
    else
        this->removeAnnotationLabel(d_data->d_rowAnnotationLabelToIndex.value(annotationLabel), orientation);

}

void BaseAnnotatedMatrix::removeAnnotationLabel(qsizetype index, Qt::Orientation orientation)
{

    QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    QList<QString> &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);

    QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);


    Q_ASSERT_X((index >= 0) && (index < d_annotationLabels.count()), "void BaseAnnotatedMatrix::removeAnnotationLabel(qsizetype index, Qt::Orientation orientation)", "index is out of range");


    d_annotationLabelToIndex.remove(d_annotationLabels.at(index));

    d_annotationLabels.removeAt(index);

    d_annotationLabelSelectionStatus.removeAt(index);

    QMutableHashIterator<QString, QList<QVariant> > it(d_identifierToAnnotations);

    while (it.hasNext())
       it.next().value().removeAt(index);

}

void BaseAnnotatedMatrix::removeAnnotationLabels(QList<qsizetype> indexes, Qt::Orientation orientation)
{

    QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    QList<QString> &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);

    QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);


    std::sort(indexes.begin(), indexes.end(), std::less<qsizetype>());

    this->removeIndexes(indexes, d_annotationLabels);

    this->removeIndexes(indexes, d_annotationLabelSelectionStatus);

    QMutableHashIterator<QString, QList<QVariant> > it(d_identifierToAnnotations);

    std::sort(indexes.begin(), indexes.end(), std::greater<qsizetype>());

    while (it.hasNext()) {

        it.next();

        for (qsizetype index : indexes)
            it.value().removeAt(index);

    }

    d_annotationLabelToIndex.clear();

    for (qsizetype i = 0; i < d_annotationLabels.count(); ++i)
        d_annotationLabelToIndex.insert(d_annotationLabels.at(i), i);

}

template <typename U> void BaseAnnotatedMatrix::removeIndexes(const QList<qsizetype> &sortedIndexesInAscendingOrder, U &container)
{

    QList<qsizetype> nSteps;

    std::adjacent_difference(sortedIndexesInAscendingOrder.begin(), sortedIndexesInAscendingOrder.end(), std::back_inserter(nSteps), [](qsizetype a, qsizetype b){return (a - b - 1);});

    nSteps.append(container.count() - sortedIndexesInAscendingOrder.last() - 1);

    auto itValuesTo = std::begin(container);

    auto itValuesFrom = std::cbegin(container);

    for (qsizetype nStep : nSteps) {

        itValuesTo = std::copy_n(itValuesFrom, nStep, itValuesTo);

        std::advance(itValuesFrom, nStep + 1);

    }

    container.resize(container.count() - sortedIndexesInAscendingOrder.count());

}

void BaseAnnotatedMatrix::removeList(const QString &identifier, Qt::Orientation orientation, bool selectedOnly)
{

    this->removeLists({identifier}, orientation, selectedOnly);

}

void BaseAnnotatedMatrix::removeList(qsizetype index, Qt::Orientation orientation)
{

    QList<qsizetype> indexes = {index};

    this->removeLists(indexes, orientation);

}

void BaseAnnotatedMatrix::removeLists(const QList<QString> &identifiers, Qt::Orientation orientation, bool selectedOnly)
{

    QList<qsizetype> indexes;

    for (const QString &identifier : identifiers)
        indexes.append(BaseAnnotatedMatrix::indexes(identifier, orientation, selectedOnly));

    this->removeLists(indexes, orientation);

}

bool BaseAnnotatedMatrix::identifierSelectionStatus(qsizetype index, Qt::Orientation orientation) const
{

    Q_ASSERT_X((index >= 0) && (index < this->identifierCount(orientation)), "bool BaseAnnotatedMatrix::identifierSelectionStatus(qsizetype index, Qt::Orientation orientation) const", "index is out of range");


    if (orientation == Qt::Horizontal)
        return d_data->d_columnIdentifierSelectionStatus.at(index);
    else
        return d_data->d_rowIdentifierSelectionStatus.at(index);

}

bool BaseAnnotatedMatrix::setAnnotationLabel(qsizetype index, const QString &annotationLabel, Qt::Orientation orientation)
{

    Q_ASSERT_X((index >= 0) && (index < this->annotationLabelCount(orientation)), "bool BaseAnnotatedMatrix::setAnnotationLabel(qsizetype index, const QString &annotationLabel, Orientation orientation)", "index is out of range");


    QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    QList<QString> &d_annotationLabels((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabels : d_data->d_rowAnnotationLabels);


    if (d_annotationLabelToIndex.contains(annotationLabel))
        return false;

    const QString &currentAnnotationLabel(d_annotationLabels.at(index));

    d_annotationLabelToIndex.insert(annotationLabel, d_annotationLabelToIndex.value(currentAnnotationLabel));

    d_annotationLabelToIndex.remove(currentAnnotationLabel);

    d_annotationLabels[index] = annotationLabel;

    return true;

}

void BaseAnnotatedMatrix::setAnnotationLabelSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation)
{

    Q_ASSERT_X((index >= 0) && (index < this->annotationLabelCount(orientation)), "bool BaseAnnotatedMatrix::setAnnotationLabelSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation)", "index is out of range");

    QList<bool> &d_annotationLabelSelectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelSelectionStatus : d_data->d_rowAnnotationLabelSelectionStatus);

    d_annotationLabelSelectionStatus[index] = status;

}

bool BaseAnnotatedMatrix::setAnnotationValue(const QString &identifier, const QString &annotationLabel, const QVariant &value, Qt::Orientation orientation)
{

    QHash<QString, qsizetype> &d_annotationLabelToIndex((orientation == Qt::Horizontal) ? d_data->d_columnAnnotationLabelToIndex : d_data->d_rowAnnotationLabelToIndex);

    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    const QMultiHash<QString, qsizetype> &d_identifierToIndexes((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes: d_data->d_rowIdentifierToIndexes);


    if (!d_identifierToIndexes.contains(identifier))
        return false;

    qsizetype annotationLabelIndex = d_annotationLabelToIndex.value(annotationLabel, -1);

    if (annotationLabelIndex == -1) {

        annotationLabelIndex = d_annotationLabelToIndex.count();

        this->appendAnnotationLabel(annotationLabel, orientation);

    }

    if (!d_identifierToAnnotations.contains(identifier)) {

        QList<QVariant> annotationValues(d_annotationLabelToIndex.count());

        annotationValues[annotationLabelIndex] = value;

        d_identifierToAnnotations.insert(identifier, annotationValues);

        return true;

    } else {

        QHash<QString, QList<QVariant>>::iterator it = d_identifierToAnnotations.find(identifier);

        QVariant currentValue = it.value().at(annotationLabelIndex);

        if (currentValue.isValid()) {

            if (currentValue == value)
                return true;
            else {

                it.value()[annotationLabelIndex] = value;

                return false;

            }

        }

        it.value()[annotationLabelIndex] = value;

        return true;

    }

}

bool BaseAnnotatedMatrix::setAnnotationValue(qsizetype identifierIndex, qsizetype annotationLabelIndex, const QVariant &value, Qt::Orientation orientation)
{

    Q_ASSERT_X((identifierIndex >= 0) && (identifierIndex < this->identifierCount(orientation)), "bool BaseAnnotatedMatrix::setAnnotationValue(qsizetype identifierIndex, qsizetype annotationLabelIndex, const QVariant &value, Qt::Orientation orientation)", "identifierIndex is out of range");

    Q_ASSERT_X((annotationLabelIndex >= 0) && (annotationLabelIndex < this->annotationLabelCount(orientation)), "bool BaseAnnotatedMatrix::setAnnotationValue(qsizetype identifierIndex, qsizetype annotationLabelIndex, const QVariant &value, Qt::Orientation orientation)", "annotationLabelIndex is out of range");


    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    const QString &identifier(this->identifier(identifierIndex, orientation));

    if (!d_identifierToAnnotations.contains(identifier)) {

        QList<QVariant> annotationValues(this->annotationLabelCount(orientation));

        annotationValues[annotationLabelIndex] = value;

        d_identifierToAnnotations.insert(identifier, annotationValues);

        return true;

    } else {

        QHash<QString, QList<QVariant>>::iterator it = d_identifierToAnnotations.find(identifier);

        QVariant currentValue = it.value().at(annotationLabelIndex);

        if (currentValue.isValid()) {

            if (currentValue == value)
                return true;
            else {

                it.value()[annotationLabelIndex] = value;

                return false;

            }

        }

        it.value()[annotationLabelIndex] = value;

        return true;

    }

}

void BaseAnnotatedMatrix::setIdentifier(qsizetype index, const QString &identifier, Qt::Orientation orientation)
{

    Q_ASSERT_X((index >= 0) && (index < this->identifierCount(orientation)), "void BaseAnnotatedMatrix::setIdentifier(qsizetype index, Qt::Orientation orientation)", "index is out of range");


    QHash<QString, QList<QVariant>> &d_identifierToAnnotations((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToAnnotations : d_data->d_rowIdentifierToAnnotations);

    QMultiHash<QString, qsizetype> &d_identifierToIndexes((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes: d_data->d_rowIdentifierToIndexes);

    QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers);


    const QString &currentIdentifier(d_identifiers.at(index));

    d_identifierToIndexes.remove(currentIdentifier, index);

    if (!d_identifierToAnnotations.contains(identifier))
        d_identifierToAnnotations.insert(identifier, d_identifierToAnnotations.value(currentIdentifier, QList<QVariant>(this->annotationLabelCount(orientation))));

    if (d_identifierToIndexes.values(currentIdentifier).isEmpty()) {

        d_identifierToIndexes.remove(currentIdentifier);

        d_identifierToAnnotations.remove(currentIdentifier);

    }

    d_identifiers[index] = identifier;

    d_identifierToIndexes.insert(identifier, index);

}

void BaseAnnotatedMatrix::setIdentifierSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation)
{

    Q_ASSERT_X((index >= 0) && (index < this->identifierCount(orientation)), "bool BaseAnnotatedMatrix::setIdentifierSelectionStatus(qsizetype index, bool status, Qt::Orientation orientation)", "index is out of range");


    if (orientation == Qt::Horizontal)
        d_data->d_columnIdentifierSelectionStatus[index] = status;
    else
        d_data->d_rowIdentifierSelectionStatus[index] = status;

}

bool BaseAnnotatedMatrix::tryLockForRead()
{

    return d_readWriteLock.tryLockForRead();

}

bool BaseAnnotatedMatrix::tryLockForWrite()
{

    return d_readWriteLock.tryLockForWrite();

}

void BaseAnnotatedMatrix::unlock()
{

    d_readWriteLock.unlock();

}

BaseAnnotatedMatrix::~BaseAnnotatedMatrix()
{

}

void BaseAnnotatedMatrix::appendIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique)
{

    this->insertIdentifiers(this->identifiers(orientation).count(), identifiers, orientation, makeUnique);

}

void BaseAnnotatedMatrix::appendIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier, bool makeUnique)
{

    QList<QString> identifiers(count);

    for (qsizetype i = 0; i < count; ++i)
        identifiers[i] = prefixIdentifier + QString::number(i);

    this->insertIdentifiers(this->identifiers(orientation).count(), identifiers, orientation, makeUnique);

}

void BaseAnnotatedMatrix::insertIdentifiers(qsizetype index, const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique)
{

    Q_ASSERT_X((index >= 0) && (index <= this->identifierCount(orientation)), "void BaseAnnotatedMatrix::insertIdentifiers(qsizetype index, const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique)", "index is out of range");


    QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers);

    const QMultiHash<QString, qsizetype> &d_identifiersToIndexes((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes : d_data->d_rowIdentifierToIndexes);


    d_identifiers.resize(d_identifiers.count() + identifiers.count());

    std::move_backward(std::next(d_identifiers.begin(), index), std::prev(d_identifiers.end(), identifiers.count()), d_identifiers.end());

    if (makeUnique) {

        QList<QString> identifiersToInsert;

        QSet<QString> setOfIdentifiersToInsert;

        identifiersToInsert.reserve(identifiers.count());

        for (const QString &identifier : identifiers) {

            qsizetype counter = 1;

            while(d_identifiersToIndexes.contains(identifier + "_" + QString::number(counter)) || setOfIdentifiersToInsert.contains(identifier + "_" + QString::number(counter)))
                ++counter;

            identifiersToInsert.append(identifier + "_" + QString::number(counter));

            setOfIdentifiersToInsert.insert(identifier + "_" + QString::number(counter));

        }

        std::move(identifiersToInsert.begin(), identifiersToInsert.end(), d_identifiers.begin() + index);

    } else
        std::copy(identifiers.begin(), identifiers.end(), d_identifiers.begin() + index);

    this->updateIdentifierToIndexes(orientation);

    QList<bool> &d_selectionStatus((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);

    d_selectionStatus.resize(d_selectionStatus.count() + identifiers.count());

    std::move_backward(std::next(d_selectionStatus.begin(), index), std::prev(d_selectionStatus.end(), identifiers.count()), d_selectionStatus.end());

    for (qsizetype i = index; i < index + identifiers.count(); ++i)
        d_selectionStatus[i] = true;

}

void BaseAnnotatedMatrix::insertIdentifiers(qsizetype index, qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier, bool makeUnique)
{

    QList<QString> identifiers(count);

    for (qsizetype i = 0; i < count; ++i)
        identifiers[i] = prefixIdentifier + QString::number(i);

    this->insertIdentifiers(index, identifiers, orientation, makeUnique);

}

void BaseAnnotatedMatrix::prependIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool makeUnique)
{

    this->insertIdentifiers(0, identifiers, orientation, makeUnique);

}

void BaseAnnotatedMatrix::prependIdentifiers(qsizetype count, Qt::Orientation orientation, const QString &prefixIdentifier, bool makeUnique)
{

    QList<QString> identifiers(count);

    for (qsizetype i = 0; i < count; ++i)
        identifiers[i] = prefixIdentifier + QString::number(i);

    this->insertIdentifiers(0, identifiers, orientation, makeUnique);

}

void BaseAnnotatedMatrix::removeIdentifiers(const QList<QString> &identifiers, Qt::Orientation orientation, bool selectedOnly)
{

    QList<qsizetype> indexes;

    for (const QString &identifier : identifiers)
        indexes.append(this->indexes(identifier, orientation, selectedOnly));

    this->removeIdentifiers(indexes, orientation);

}

void BaseAnnotatedMatrix::removeIdentifiers(QList<qsizetype> indexes, Qt::Orientation orientation)
{

    std::sort(indexes.begin(), indexes.end());

    this->removeIndexes(indexes, (orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers);

    this->removeIndexes(indexes, (orientation == Qt::Horizontal) ? d_data->d_columnIdentifierSelectionStatus : d_data->d_rowIdentifierSelectionStatus);

    this->updateIdentifierToIndexes(orientation);

}

void BaseAnnotatedMatrix::updateIdentifierToIndexes(Qt::Orientation orientation)
{

    QList<QString> &d_identifiers((orientation == Qt::Horizontal) ? d_data->d_columnIdentifiers : d_data->d_rowIdentifiers);

    QMultiHash<QString, qsizetype> &d_identifiersToIndexes((orientation == Qt::Horizontal) ? d_data->d_columnIdentifierToIndexes : d_data->d_rowIdentifierToIndexes);


    d_identifiersToIndexes.clear();

    for (qsizetype i = 0; i < d_identifiers.count(); ++i)
        d_identifiersToIndexes.insert(d_identifiers.at(i), i);

}

typename Qt::Orientation BaseAnnotatedMatrix::switchOrientation(Qt::Orientation orientation)
{

    return (orientation == Qt::Vertical) ? Qt::Horizontal : Qt::Vertical;

}

QDataStream& operator<<(QDataStream& out, BaseAnnotatedMatrix &baseAnnotatedMatrix)
{

    return baseAnnotatedMatrix.writeToDataStream(out);

}

QDataStream& operator>>(QDataStream& in, BaseAnnotatedMatrix &baseAnnotatedMatrix)
{

    return baseAnnotatedMatrix.readFromDataStream(in);

}
