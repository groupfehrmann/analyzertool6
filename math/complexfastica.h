#ifndef COMPLEXFASTICA_H
#define COMPLEXFASTICA_H

#include <QObject>
#include <QThreadPool>
#include <QPointer>

#include <complex>

#include "workerclasses/baseworker.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"

#define NC_C_FICA_LOG 1
#define NC_C_FICA_KURT 2
#define NC_C_FICA_SQRT 3
#define NC_C_FICA_TOL 1e-9

class ComplexFastICA : public QObject
{
    Q_OBJECT

public:

    explicit ComplexFastICA(const QList<std::complex<double>> &whitenedMatrix, const QList<std::complex<double>> &whiteningMatrix, const QList<std::complex<double>> &dewhiteningMatrix, qsizetype nSamples, qsizetype nItems, qsizetype nComponents, int contrastFunction = NC_C_FICA_LOG, int maximumNumberOfIterations = 1000, double mu = 1.0, double epsilon = 0.00001, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), QObject *parent = nullptr);

    const QList<std::complex<double>> &mixingMatrix() const;

    const QList<std::complex<double>> &seperatingMatrix() const;

    const QList<std::complex<double>> &independentComponents();

    void connectBaseWorker(BaseWorker *baseWorker);

    void setLabel(const QString &label);

private:

    QString d_label;

    const QList<std::complex<double>> &d_whitenedMatrix;

    const QList<std::complex<double>> &d_whiteningMatrix;

    const QList<std::complex<double>> &d_dewhiteningMatrix;

    qsizetype d_nSamples;

    qsizetype d_nItems;

    qsizetype d_nSubItems;

    qsizetype d_nComponents;

    QList<std::complex<double>> d_mixingMatrix;

    QList<std::complex<double>> d_seperatingMatrix;

    int d_contrastFunction;

    int d_maximumNumberOfIterations;

    double d_mu;

    double d_epsilon;

    int d_numberOfThreadsToUse;

    int d_previousNumberOfThreadsToUse;

    QThreadPool d_threadPool;

    QList<std::complex<double>> d_independentComponents;

    QList<std::complex<double>> d_B;

    QPointer<BaseWorker> d_connectedBaseworker;

public slots:

    bool run();

    void setNumberOfThreadsToUse(qsizetype numberOfThreadsToUse);

};

#endif // COMPLEXFASTICA_H
