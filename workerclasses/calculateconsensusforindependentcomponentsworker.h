#ifndef CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSWORKER_H
#define CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include <algorithm>
#include <ranges>

#include <boost/math/statistics/univariate_statistics.hpp>
#include <boost/math/statistics/bivariate_statistics.hpp>
#include "boost/range/numeric.hpp"

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"
#include "containers/result.h"
#include "math/trip.h"
#include "math/mathutilityfunctions.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"
#include "utils/conversionfunctions.h"
#include "math/fastdistancecorrelation.h"

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

class CalculateConsensusForIndependentComponentsParameters : public BaseParameters
{

public:

    CalculateConsensusForIndependentComponentsParameters(QObject *parent = nullptr);

    QString exportDirectory;

    Qt::Orientation orientationOfIndependentComponentsInFile;

    Qt::Orientation orientationOfVariablesInOriginalData;

    bool rMode_originalData;

    bool rMode_independentComponents;

    double threshold_credibilityIndex;

    double pearsonCorrelationThresholdForConsensus;

    QString metricToDetermineIndependence;

    double distanceCorrelationThresholdForConsensus;

    QList<QString> selectedResultItems;

    qsizetype numberOfThreadsToUse;

    QString pathToOriginalData;

    QString directoryContainingIndependentComponents;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class CalculateConsensusForIndependentComponentsWorker : public BaseWorker
{

public:

    CalculateConsensusForIndependentComponentsWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    QList<double> xiCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension);

    QList<double> distanceCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension);

    QList<double> pearsonCorrelationSymmetricalMatrix(const QList<double> &matrix, qsizetype leadingDimension);

    void readFile(const QString &path, QList<QString> &rowIdentifiers, QList<QString> &columnIdentifiers, QList<double> &data, bool rMode);

    QList<QString> scanDirectoryForIndependentComponentfiles(const QString &directory);

};

#endif // CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSWORKER_H
