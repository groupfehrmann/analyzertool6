#ifndef DATAFROMASINGLEFILEDIALOG_H
#define DATAFROMASINGLEFILEDIALOG_H

#include <QFile>
#include <QList>
#include <QString>
#include <QRegularExpression>

#include "basedialog.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectfilewidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/selectdelimiterwidget.h"
#include "workerclasses/datafromasinglefileworker.h"

class DataFromASingleFileDialog : public BaseDialog
{

public:

    DataFromASingleFileDialog(QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    QList<QString> d_bufferLinesHeadOfFile;

    QList<QString> d_bufferLinesHeadOfFileTrimmed;

    QList<QString> d_bufferLinesTailOfFile;

    QList<QString> d_bufferLinesTailOfFileTrimmed;

    ComboBoxWidget *d_comboBoxWidget_firstDataLine;

    ComboBoxWidget *d_comboBoxWidget_rowIdentifiersColumn;

    ComboBoxWidget *d_comboBoxWidget_firstDataColumn;

    ComboBoxWidget *d_comboBoxWidget_secondDataColumn;

    ComboBoxWidget *d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers;

    ComboBoxWidget *d_comboBoxWidget_headerLine;

    ComboBoxWidget *d_comboBoxWidget_lastDataLine;

    qsizetype d_lineNumberOfHeaderLine;

    qsizetype d_lineNumberOfFirstDataLine;

    qsizetype d_lineNumberOfLastDataLine;

    qsizetype d_offSetLineNumber;

    SelectDelimiterWidget *d_selectDelimiterWidget;

    SpinBoxWidget *d_spinBoxWidget_nBufferLinesHeadOfFile;

    SpinBoxWidget *d_spinBoxWidget_nBufferLinesTailOfFile;

    QList<QString> d_tokensForHeaderLabels;

    int d_indexOfColumnDefiningRowIdentifiers;

    int d_indexOfColumnDefiningFirstDataColumn;

private slots:

    void fileSelectionChanged(const QString &path);

    void firstDataLineChanged(int index);

    void headerLineChanged(int index);

    void lastDataLineChanged(int index);

    void updateHeaderLabels(const QList<QString> &tokens);

    void comboBoxWidget_rowIdentifiersColumn_indexChanged(int index);

    void comboBoxWidget_firstDataColumn_indexChanged(int index);

    void comboBoxWidget_secondDataColumn_indexChanged(int index);

};

#endif // DATAFROMASINGLEFILEDIALOG_H
