#include "transformdatausingmedianpolishworker.h"

TransformDataUsingMedianPolishParameters::TransformDataUsingMedianPolishParameters(QObject *parent) :
    BaseParameters(parent, "transformdatausingmedianpolish")
{

}

bool TransformDataUsingMedianPolishParameters::isValid_()
{

    if (selectedRowVariableIdentifiers.isEmpty()) {

        d_lastError = "no row variables selected";

        return false;

    }

    if (selectedColumnVariableIdentifiers.isEmpty()) {

        d_lastError = "no column variables selected";

        return false;

    }

    return true;

}

QString TransformDataUsingMedianPolishParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[transform data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[selected row variables = [\"" << selectedRowVariableIdentifiers.first() << "\", " << selectedRowVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedRowVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedRowVariableIdentifiers.at(i) << "\", " << selectedRowVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected column variables = [\"" << selectedColumnVariableIdentifiers.first() << "\", " << selectedColumnVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedColumnVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedColumnVariableIdentifiers.at(i) << "\", " << selectedColumnVariableIndexes.at(i) << "]";

    stream << "]";

    stream << "\t[epsilon = " << epsilon << "]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

TransformDataUsingMedianPolishWorker::TransformDataUsingMedianPolishWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("transform data using median polish"), parameters, "transformdatausingmedianpolish")
{

}

void TransformDataUsingMedianPolishWorker::doWork_()
{

    QSharedPointer<TransformDataUsingMedianPolishParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingMedianPolishParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("for transforming data, values in data need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool TransformDataUsingMedianPolishWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<TransformDataUsingMedianPolishParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingMedianPolishParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    return true;

}

void TransformDataUsingMedianPolishWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<TransformDataUsingMedianPolishParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataUsingMedianPolishParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}
