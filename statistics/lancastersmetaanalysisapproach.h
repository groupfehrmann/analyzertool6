#ifndef LANCASTERSMETAANALYSISAPPROACH_H
#define LANCASTERSMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/chi_squared.hpp"

#include "basemetaanalysisapproach.h"

class LancastersMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    LancastersMetaAnalysisApproach();

    LancastersMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~LancastersMetaAnalysisApproach() {}

private:

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    double d_degreeOfFreedom;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // LANCASTERSMETAANALYSISAPPROACH_H
