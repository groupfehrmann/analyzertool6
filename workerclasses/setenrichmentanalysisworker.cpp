#include "setenrichmentanalysisworker.h"

SetEnrichmentAnalysisParameters::SetEnrichmentAnalysisParameters(QObject *parent) :
    BaseParameters(parent, "setenrichmentanalysis")
{

}

bool SetEnrichmentAnalysisParameters::isValid_()
{

    if (selectedVariableIdentifiers.isEmpty()) {

        d_lastError = "no variables selected";

        return false;

    }

    if (selectedItemIdentifiers.isEmpty()) {

        d_lastError = "no items selected";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    if (pathToSetCollections.isEmpty()) {

        d_lastError = "no files containing definitions of sets collected";

        return false;

    }

    if (annotationLabelsForItems.isEmpty()) {

        d_lastError = "at least one annotation label for items needs to be selected";

        return false;

    }

    return true;

}

QString SetEnrichmentAnalysisParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[set enrichement analysis on data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables = [\"" << selectedVariableIdentifiers.first() << "\", " << selectedVariableIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers.at(i) << "\", " << selectedVariableIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items = [\"" << selectedItemIdentifiers.first() << "\", " << selectedItemIndexes.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers.at(i) << "\", " << selectedItemIndexes.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected files containing definitions of sets = [\"" << pathToSetCollections.first() << "\"]";

    for (qsizetype i = 1; i < pathToSetCollections.count(); ++i)
        stream << ", [\"" << pathToSetCollections.at(i) << "\"]";

    stream << "]";

    stream << Qt::endl << "\t[label for matching between items of lists and members of sets = \"" << labelForMatching << "\"]";

    stream << Qt::endl << "\t[test to determine enrichement = \"" << testToDetermineEnrichment << "\"]";

    if (testToDetermineEnrichment == "Fisher exact")
        stream << Qt::endl << "\t[threshold used for Fisher Exact = \"" << thresholdForFisherExact << "\"]";

    stream << Qt::endl << "\t[minimum size of set = " << minimumSizeOfSet << "]";

    stream << Qt::endl << "\t[maximum size of set = " << maximumSizeOfSet << "]";

    if (enablePermutationMode) {

        stream << Qt::endl << "\t[permutation mode is enabled with " << numberOfPermutations << " permutations]";

        stream << Qt::endl << "\t[false discovery rate to controle for in permutation mode " << falseDiscoveryRate <<"]";

        stream << Qt::endl << "\t[confidence level for multivariate threshold in permutation mode " << confidenceLevel <<"]";

    }

    stream << Qt::endl << "\t[annotation labels for items = [\"" << annotationLabelsForItems.first() << "\"]";

    for (qsizetype i = 1; i < annotationLabelsForItems.count(); ++i)
        stream << ", [\"" << annotationLabelsForItems.at(i) << "\"]";


    stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

SetEnrichmentAnalysisWorker::SetEnrichmentAnalysisWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("set enrichment analysis"), parameters, "setenrichmentanalysis")
{

}

void SetEnrichmentAnalysisWorker::doWork_()
{

    QSharedPointer<SetEnrichmentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<SetEnrichmentAnalysisParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeName = parameters->baseAnnotatedMatrix->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix));
    else if (*typeName == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix));
    else {

        this->reportError("values in data need to be numerical to perform set enrichement analysis");

        QThread::currentThread()->requestInterruption();

    }

}

bool SetEnrichmentAnalysisWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<SetEnrichmentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<SetEnrichmentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->lockForRead();

    return true;

}

void SetEnrichmentAnalysisWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<SetEnrichmentAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<SetEnrichmentAnalysisParameters>());

    parameters->baseAnnotatedMatrix->unlock();

}

bool SetEnrichmentAnalysisWorker::importSetCollections(const QString &path, const QHash<QString, qsizetype> &itemIdentifierToIndex, QList<QList<std::tuple<QString, QString, QList<QString>>>> &setCollections_sets_identifier_description_members, qsizetype minimumSizeOfSet, qsizetype maximumSizeOfSet)
{

    QFile fileIn(path);

    if (!fileIn.exists() || !fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + path + "\"");

        QThread::currentThread()->requestInterruption();

        return false;

    }

    QUuid uuidProgress = this->startProgress("importing set collection \"" + path + "\"", 0, fileIn.size());

    qsizetype numberOfLinesRead = 0;

    QList<std::tuple<QString, QString, QList<QString>>> sets_identifier_description_members;

    QSet<QString> setIdentifiers;

    while (!fileIn.atEnd()) {

        QList<QString> tokens = QString(fileIn.readLine()).remove(QRegularExpression("[\r\n]")).split(QRegularExpression("\t"), Qt::SkipEmptyParts);

        ++numberOfLinesRead;

        if (tokens.size() < 3) {

            this->reportError("in set collection file \"" + path + "\" at line number " + QString::number(numberOfLinesRead) + " not enough tokens could be read");

            fileIn.close();

            return false;

        }

        QSet<QString> setMembers;

        for (qsizetype i = 2; i < tokens.count(); ++i) {

            const QString &token(tokens.at(i));

            if (itemIdentifierToIndex.contains(token)) {

                if (setMembers.contains(token))
                    this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", set with identifier \"" + tokens.at(0) + "\" conatains duplicate members with identifier \"" + token + "\"");
                else
                    setMembers.insert(token);

            }

        }

        if ((setMembers.count() <= maximumSizeOfSet) && (setMembers.count() >= minimumSizeOfSet)) {

            if (setIdentifiers.contains(tokens.at(0))) {

                this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", duplicate set identifier \"" + tokens.at(0) + "\" was detected");

                fileIn.close();

                return false;

            } else {

                setIdentifiers.insert(tokens.at(0));

                sets_identifier_description_members.append({tokens.at(0), tokens.at(1), setMembers.values()});

            }

        } else
            this->reportWarning("in set collection file \"" + path + "\" at line " + QString::number(numberOfLinesRead) + ", set identifier \"" + tokens.at(0) + "\" with description \"" + tokens.at(1) + " was excluded as it did not valid number of members (" + QString::number(setMembers.count()) + ")");


        if (QThread::currentThread()->isInterruptionRequested())
            return false;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    this->stopProgress(uuidProgress);

    if (sets_identifier_description_members.isEmpty()) {

        this->reportError("remove \"" + path + "\" from set collections as there were no set definitions that met the criteria of a valid set");

        return false;

    }

    this->reportMessage("imported " + QString::number(sets_identifier_description_members.count()) + " set definitions from set collection file \"" + path + "\"");

    setCollections_sets_identifier_description_members.append(sets_identifier_description_members);

    return true;

}
