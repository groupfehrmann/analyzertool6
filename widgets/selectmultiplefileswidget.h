#ifndef SELECTMULTIPLEFILESWIDGET_H
#define SELECTMULTIPLEFILESWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QFile>
#include <QFileDialog>

#include "dialogs/basedialog.h"

namespace Ui {

    class SelectMultipleFilesWidget;

}

class SelectMultipleFilesWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectMultipleFilesWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString(), QFileDialog::AcceptMode acceptMode = QFileDialog::AcceptOpen);

    QFileDialog *fileDialog();

    QLabel *label();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~SelectMultipleFilesWidget();

private:

    Ui::SelectMultipleFilesWidget *ui;

    QFileDialog::AcceptMode d_acceptMode;

    QFileDialog *d_fileDialog;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

private slots:

    void on_pushButton_browse_clicked();

    void on_pushButton_clear_clicked();

    void on_pushButton_remove_clicked();

signals:

    void filterSelected(const QString &filter);

    void pathsChanged(const QList<QString> &paths);

};

#endif // SELECTMULTIPLEFILESWIDGET_H
