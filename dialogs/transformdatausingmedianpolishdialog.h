#ifndef TRANSFORMDATAUSINGMEDIANPOLISHDIALOG_H
#define TRANSFORMDATAUSINGMEDIANPOLISHDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "workerclasses/transformdatausingmedianpolishworker.h"

class TransformDataUsingMedianPolishDialog : public BaseDialog
{

public:

    TransformDataUsingMedianPolishDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    ItemSelectorWidget *d_itemSelectorWidget_rowVariables;

    ItemSelectorWidget *d_itemSelectorWidget_columnVariables;

};

#endif // TRANSFORMDATAUSINGMEDIANPOLISHDIALOG_H
