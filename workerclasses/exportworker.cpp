#include "exportworker.h"

ExportParameters::ExportParameters(QObject *parent) :
    BaseParameters(parent, "exportdataresult")
{

}

bool ExportParameters::isValid_()
{

    return true;

}

QString ExportParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[path to file = \"" << pathToFile << "\"]";

    if (objectPointer->property("objectType").toString() == "annotated matrix") {

        stream << Qt::endl << "\t[tab to export = \"" << annotatedMatrixTabToExport << "\"]";

        stream << Qt::endl << "\t[export selection mode = \"" << (exportSelectedOnly ? "export selected only" : "export all") << "\"]";

    }

    stream << Qt::endl << "\t[output format = \"" << selectedFilter << "\"]";

    return prettyPrintString;

}

ExportWorker::ExportWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("export data or result"), parameters, "exportdataresult")
{

}

void ExportWorker::doWork_()
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    if (parameters->objectPointer->property("objectType").toString() == "annotated matrix") {

        const char *typeName = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointer)->typeName();

        if (*typeName == *typeid(short).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->objectPointer));
        else if (*typeName == *typeid(unsigned short).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->objectPointer));
        else if (*typeName == *typeid(int).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->objectPointer));
        else if (*typeName == *typeid(unsigned int).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->objectPointer));
        else if (*typeName == *typeid(long long).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->objectPointer));
        else if (*typeName == *typeid(unsigned long long).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->objectPointer));
        else if (*typeName == *typeid(float).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->objectPointer));
        else if (*typeName == *typeid(double).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->objectPointer));
        else if (*typeName == *typeid(QString).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(parameters->objectPointer));
        else if (*typeName == *typeid(unsigned char).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned char>>(parameters->objectPointer));
        else if (*typeName == *typeid(bool).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<bool>>(parameters->objectPointer));
        else if (*typeName == *typeid(QVariant).name())
            this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<QVariant>>(parameters->objectPointer));

    } else if (parameters->objectPointer->property("objectType").toString() == "result")
        this->doWork_result();

}

void ExportWorker::doWork_result()
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    if (parameters->objectPointer->property("resultType").toString() == "table")
        this->doWork_result_table();

}

void ExportWorker::doWork_result_table()
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    QSharedPointer<Result> result = qSharedPointerDynamicCast<Result>(parameters->objectPointer);

    const char *typeName = qSharedPointerDynamicCast<BaseAnnotatedMatrix>(result->internalObject())->typeName();

    parameters->annotatedMatrixTabToExport = "data";

    if (*typeName == *typeid(short).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(int).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(long long).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(float).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(double).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(QString).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<QString>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(unsigned char).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned char>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(bool).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<bool>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));
    else if (*typeName == *typeid(QVariant).name())
        this->doWork_annotatedMatrix(qSharedPointerDynamicCast<AnnotatedMatrix<QVariant>>(qSharedPointerDynamicCast<Result>(parameters->objectPointer)->internalObject()));


}

bool ExportWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    if (parameters->objectPointer->property("objectType").toString() == "annotated matrix")
        qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointer)->lockForRead();

    return true;

}

void ExportWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<ExportParameters> parameters(d_data->d_parameters.dynamicCast<ExportParameters>());

    if (parameters->objectPointer->property("objectType").toString() == "annotated matrix")
        qSharedPointerDynamicCast<BaseAnnotatedMatrix>(parameters->objectPointer)->unlock();

}
