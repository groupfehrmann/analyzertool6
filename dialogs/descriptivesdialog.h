#ifndef DESCRIPTIVESDIALOG_H
#define DESCRIPTIVESDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>
#include <QInputDialog>

#include <boost/math/statistics/univariate_statistics.hpp>

#include <limits>
#include <cmath>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/descriptivesworker.h"

class DescriptivesDialog : public BaseDialog
{

public:

    DescriptivesDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

    ComboBoxWidget *d_comboBoxWidget_annotationLabelDefiningItemSubsets;

    ItemSelectorWidget *d_itemSelectorWidget_itemSubsetIdentifiers;

    ItemSelectorWidget *d_itemSelectorWidget_annotationLabelsForVariables;

    ItemSelectorWidget *d_itemSelectorWidget_descriptives;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void comboBoxWidget_annotationLabelDefiningItemSubsetsCurrentIndexChanged(qsizetype index);

    void dialogAccepted();

};

#endif // DESCRIPTIVESDIALOG_H
