#ifndef STOUFFERSTRENDMETAANALYSISAPPROACH_H
#define STOUFFERSTRENDMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/normal.hpp"

#include "basemetaanalysisapproach.h"

class StouffersTrendMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    StouffersTrendMetaAnalysisApproach();

    StouffersTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~StouffersTrendMetaAnalysisApproach() {}

private:

    QList<double> d_statistics;

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // STOUFFERSTRENDMETAANALYSISAPPROACH_H
