#include "baseparameters.h"

BaseParameters::BaseParameters(QObject *parent, const QString &parametersWorkerLink) :
    QObject(parent),
    d_parametersWorkerLink(parametersWorkerLink)
{

    d_threadPool.setMaxThreadCount( QThreadPool::globalInstance()->maxThreadCount());

}

BaseParameters::BaseParameters(const BaseParameters &baseParameters) :
    QObject(baseParameters.parent())
{

    this->operator=(baseParameters);

}

const QString &BaseParameters::lastError() const
{

    return d_lastError;

}

bool BaseParameters::isValid(const QString &parametersWorkerLink)
{

    if (d_parametersWorkerLink != parametersWorkerLink) {

        d_lastError = QString("parameters are not suitable for the worker");

        return false;

    }

    return this->isValid_();

}

BaseParameters &BaseParameters::operator=(const BaseParameters &baseParameters)
{
    if (this != &baseParameters) {

        d_parametersWorkerLink = baseParameters.d_parametersWorkerLink;

        d_lastError = baseParameters.d_lastError;

    }

    return *this;

}

QString BaseParameters::prettyPrint() const
{

    return this->prettyPrint_();

}

BaseParameters::~BaseParameters()
{

}
