#ifndef GUILTBYASSOCIATIONANALYSISWORKER_H
#define GUILTBYASSOCIATIONANALYSISWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>
#include <QRegularExpression>
#include <QSet>
#include <QMutex>

#include <tuple>
#include <random>
#include <algorithm>

#include "boost/math/distributions/normal.hpp"

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "statistics/studentttest.h"
#include "statistics/welchttest.h"
#include "statistics/mannwhitneyutest.h"
#include "statistics/kolmogorovsmirnovtest.h"
#include "statistics/fisherexact2x2test.h"
#include "statistics/fittingdistributions.h"
#include "math/mathoperations.h"
#include "math/mathdescriptives.h"
#include "math/fastdistancecorrelation.h"

class GuiltByAssociationAnalysisParameters : public BaseParameters
{

public:

    GuiltByAssociationAnalysisParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString exportDirectory;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<QString> selectedResultItems;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QList<QString> pathToSetCollections;

    QString labelForMatching;

    qsizetype indexForMatching;

    QString barcodeFunction;

    QString associationFunction;

    double thresholdForFisherExact;

    qsizetype minimumSizeOfSet;

    qsizetype maximumSizeOfSet;

    bool enablePermutationMode;

    qsizetype numberOfPermutations;

    int numberOfThreadsToUse;

    QList<QString> annotationLabelsForVariables;

    double falseDiscoveryRate;

    double confidenceLevel;

    double p_rankBiasedOverlap;

    qsizetype maxDepth_rankBiasedOverlap;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class GuiltByAssociationAnalysisWorker : public BaseWorker
{

public:

    GuiltByAssociationAnalysisWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    bool importSetCollections(const QString &path, const QHash<QString, qsizetype> &variableIdentifierToIndex, QList<QList<std::tuple<QString, QString, QSet<QString>>>> &setCollectionToSetsInfo, qsizetype minimumSizeOfSet, qsizetype maximumSizeOfSet);

    double rankBiasedOverlap(const std::span<double> &span1, const std::span<double> &span2, qsizetype maxDepth, long double p = 0.9);

};

template<typename T>
void GuiltByAssociationAnalysisWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<GuiltByAssociationAnalysisParameters> parameters(d_data->d_parameters.dynamicCast<GuiltByAssociationAnalysisParameters>());

    // creating variable identifier to index mapping
    ////////////////

    QUuid uuidProgress = this->startProgress("creating variable identifier to index mapping", 0, parameters->selectedVariableIndexes.count());

    QList<QString> variableIdentifiers;

    QHash<QString, qsizetype> variableIdentifierToIndex;

    QList<QString> concatenatedAnnotationValuesForVariables;

    if (parameters->indexForMatching == -1) {

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes.count(); ++i) {

            const QString &selectedVariableIdentifier(parameters->selectedVariableIdentifiers.at(i));

            if (variableIdentifierToIndex.contains(selectedVariableIdentifier)) {

                this->reportError("duplicate variable identifier \"" + selectedVariableIdentifier + "\" detected");

                this->requestInterruption();

                return;

            } else {

                variableIdentifiers.append(selectedVariableIdentifier);

                variableIdentifierToIndex.insert(selectedVariableIdentifier, i);

            }

            QString concatenatedAnnotationValuesForVariable;

            for (qsizetype j = 0; j < parameters->annotationLabelsForVariables.count(); ++j) {

                const QString &annotationLabelForVariables(parameters->annotationLabelsForVariables.at(j));

                if (j == 0) {

                    if ((annotationLabelForVariables == "COLUMN IDENTIFIER") || (annotationLabelForVariables == "ROW IDENTIFIER"))
                        concatenatedAnnotationValuesForVariable = "[" + selectedVariableIdentifier + "]";
                    else
                        concatenatedAnnotationValuesForVariable = "[" + parameters->baseAnnotatedMatrix->annotationValue(selectedVariableIdentifier, annotationLabelForVariables, parameters->orientation).toString() + "]";

                } else {

                    if ((annotationLabelForVariables == "COLUMN IDENTIFIER") || (annotationLabelForVariables == "ROW IDENTIFIER"))
                        concatenatedAnnotationValuesForVariable = " [" + selectedVariableIdentifier + "]";
                    else
                        concatenatedAnnotationValuesForVariable = " [" + parameters->baseAnnotatedMatrix->annotationValue(selectedVariableIdentifier, annotationLabelForVariables, parameters->orientation).toString() + "]";

                }

            }

            concatenatedAnnotationValuesForVariables.append(concatenatedAnnotationValuesForVariable);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

    } else {

        for (qsizetype i = 0; i < parameters->selectedVariableIndexes.count(); ++i) {

            QString selectedVariableIdentifier = annotatedMatrix->BaseAnnotatedMatrix::annotationValue(parameters->selectedVariableIndexes.at(i), parameters->indexForMatching, parameters->orientation).toString();

            if (variableIdentifierToIndex.contains(selectedVariableIdentifier)) {

                this->reportError("duplicate variable identifier \"" + selectedVariableIdentifier + "\" detected when annotations with label \"" + parameters->labelForMatching + "\" is used for matching");

                this->requestInterruption();

                return;

            } else {

                variableIdentifiers.append(selectedVariableIdentifier);

                variableIdentifierToIndex.insert(selectedVariableIdentifier, i);

            }

            QString concatenatedAnnotationValuesForVariable;

            for (qsizetype j = 0; j < parameters->annotationLabelsForVariables.count(); ++j) {

                const QString &annotationLabelForVariables(parameters->annotationLabelsForVariables.at(j));

                if (j == 0) {

                    if ((annotationLabelForVariables == "COLUMN IDENTIFIER") || (annotationLabelForVariables == "ROW IDENTIFIER"))
                        concatenatedAnnotationValuesForVariable = "[" + selectedVariableIdentifier + "]";
                    else
                        concatenatedAnnotationValuesForVariable = "[" + parameters->baseAnnotatedMatrix->annotationValue(selectedVariableIdentifier, annotationLabelForVariables, parameters->orientation).toString() + "]";

                } else {

                    if ((annotationLabelForVariables == "COLUMN IDENTIFIER") || (annotationLabelForVariables == "ROW IDENTIFIER"))
                        concatenatedAnnotationValuesForVariable = " [" + selectedVariableIdentifier + "]";
                    else
                        concatenatedAnnotationValuesForVariable = " [" + parameters->baseAnnotatedMatrix->annotationValue(selectedVariableIdentifier, annotationLabelForVariables, parameters->orientation).toString() + "]";

                }

            }

            concatenatedAnnotationValuesForVariables.append(concatenatedAnnotationValuesForVariable);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, i + 1);

        }

    }

    if (variableIdentifierToIndex.count() <= 3) {

        this->reportError("not enough variables remaining for analysis after variable identifier to index mapping (n = " + QString::number(variableIdentifierToIndex.count()) + ")");

        this->requestInterruption();

        return;

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // importing set collections
    ////////////////

    QList<QString> setCollectionToPrettyPrintLabels;

    QList<QList<QString>> setCollectionToSetLabels;

    QList<QList<std::tuple<QString, QString, QSet<QString>>>> setCollectionToSetsInfo;

    QList<QList<QList<qsizetype>>> collectionToSetToCodedSetMembers;

    uuidProgress = this->startProgress("importing set collections", 0, parameters->pathToSetCollections.count());

    for (qsizetype i = 0; i < parameters->pathToSetCollections.count(); ++i) {

        setCollectionToPrettyPrintLabels.append(QFileInfo(parameters->pathToSetCollections.at(i)).baseName());

        if (!this->importSetCollections(parameters->pathToSetCollections.at(i), variableIdentifierToIndex, setCollectionToSetsInfo, parameters->minimumSizeOfSet, parameters->maximumSizeOfSet)) {

            this->requestInterruption();

            return;

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        QUuid uuidProgress_1 = this->startProgress("matching variables to set members for \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", 0, setCollectionToSetsInfo.at(i).count());

        QList<QList<qsizetype>> setToCodedSetMembers;

        for (const std::tuple<QString, QString, QSet<QString>> &setInfo : setCollectionToSetsInfo.at(i)) {

            QList<qsizetype> codedSetMembers(parameters->selectedVariableIndexes.count(), 1);

            for (const QString &member : std::get<2>(setInfo))
                codedSetMembers[variableIdentifierToIndex.value(member)] = 0;

            setToCodedSetMembers.append(codedSetMembers);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        collectionToSetToCodedSetMembers.append(setToCodedSetMembers);

        this->stopProgress(uuidProgress_1);


        uuidProgress_1 = this->startProgress("constructing pretty print set labels for \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", 0, setCollectionToSetsInfo.last().count());

        QList<QString> setLabels;

        for (const std::tuple<QString, QString, QSet<QString>> &set : setCollectionToSetsInfo.at(i)) {

            setLabels.append("[" + std::get<0>(set) + "] [" + std::get<1>(set) + "]");

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress_1);

        }

        setCollectionToSetLabels.append(setLabels);

        this->stopProgress(uuidProgress_1);


        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // fetching data for barcode generation
    ////////////////

    QList<QList<double>> dataForBarcodeGeneration;

    uuidProgress = this->startProgress("fetching data for barcode generation", 0, parameters->selectedItemIndexes.count());

    for (qsizetype i = 0; i < parameters->selectedItemIndexes.count(); ++i) {

        if (parameters->orientation == Qt::Horizontal)
            dataForBarcodeGeneration.append(annotatedMatrix->template rowAt<double>(parameters->selectedItemIndexes.at(i), parameters->selectedVariableIndexes));
        else
            dataForBarcodeGeneration.append(annotatedMatrix->template columnAt<double>(parameters->selectedItemIndexes.at(i), parameters->selectedVariableIndexes));

        if (parameters->barcodeFunction == "Fisher exact")
            MathOperations::applyFunctor_elementWise_inplace(dataForBarcodeGeneration.last(), [&parameters](double &value){if (value >= parameters->thresholdForFisherExact) value = 1; else if (std::fabs(value) >= parameters->thresholdForFisherExact) value = 2; else value = 0;}, parameters->numberOfThreadsToUse);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // fetching data for variables
    ////////////////

    QList<double> dataForVariables;

    uuidProgress = this->startProgress("fetching data for variables", 0, parameters->selectedVariableIndexes.count());

    for (qsizetype i = 0; i < parameters->selectedVariableIndexes.count(); ++i) {

        if (parameters->orientation == Qt::Vertical)
            dataForVariables.append(annotatedMatrix->template rowAt<double>(parameters->selectedVariableIndexes.at(i), parameters->selectedItemIndexes));
        else
            dataForVariables.append(annotatedMatrix->template columnAt<double>(parameters->selectedVariableIndexes.at(i), parameters->selectedItemIndexes));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    ////////////////

    // Mapping item identifiers to coded item identifiers
    ////////////////

    QHash<QString, qsizetype> itemIdentifierToCodedItemIdentifier;

    for (qsizetype i = 0; i < parameters->selectedItemIndexes.count(); ++i) {

        if (!itemIdentifierToCodedItemIdentifier.contains(parameters->selectedItemIdentifiers.at(i)))
            itemIdentifierToCodedItemIdentifier.insert(parameters->selectedItemIdentifiers.at(i), itemIdentifierToCodedItemIdentifier.count());

    }

    // transforming variables or obtaining descriptives needed for distance function
    ////////////////

    QList<std::pair<qsizetype, std::span<double>>> variables_index_span = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(dataForVariables, parameters->selectedItemIndexes.count());

    QList<double> variables_tiesCorrectionFactor;

    QList<double> variables_variance;

    if (parameters->associationFunction == "Pearson R") {

        this->startProgress("center variables", QtConcurrent::map(&d_threadPool, variables_index_span, [](std::pair<qsizetype, std::span<double>> &index_span){MathOperations::centerVectorToMeanInplace(index_span.second);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        this->startProgress("scale variables", QtConcurrent::map(&d_threadPool, variables_index_span, [](std::pair<qsizetype, std::span<double>> &index_span){MathOperations::scaleVectorToL2NormInplace(index_span.second);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    if (parameters->associationFunction == "Spearman R") {

        variables_tiesCorrectionFactor.resize((variables_index_span.count()));

        this->startProgress("ranking variables", QtConcurrent::map(&d_threadPool, variables_index_span, [&](std::pair<qsizetype, std::span<double>> &index_span){variables_tiesCorrectionFactor[index_span.first] = MathOperations::crankInplace(index_span.second);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    if (parameters->associationFunction == "Distance R") {

        variables_variance.resize((variables_index_span.count()));

        this->startProgress("calculating variance of variables", QtConcurrent::map(&d_threadPool, variables_index_span, [&](std::pair<qsizetype, std::span<double>> &index_span){variables_variance[index_span.first] = boost::math::statistics::sample_variance(index_span.second);}));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    if (parameters->associationFunction == "rank-biased overlap") {

        this->startProgress("replacing values with ranks", QtConcurrent::map(&d_threadPool, variables_index_span, [&](std::pair<qsizetype, std::span<double>> &index_span){

            QList<std::pair<qsizetype, T>> pair_codedItemIdentifier_value;

            pair_codedItemIdentifier_value.reserve(parameters->selectedItemIndexes.count());

            for (qsizetype j = 0; j < parameters->selectedItemIndexes.count(); ++j)
                pair_codedItemIdentifier_value.append(std::pair<qsizetype, T>(itemIdentifierToCodedItemIdentifier.value(parameters->selectedItemIdentifiers.at(j)), index_span.second[j]));

            std::sort(pair_codedItemIdentifier_value.begin(), pair_codedItemIdentifier_value.end(), [](const std::pair<qsizetype, T> &pair1, const std::pair<qsizetype, T> &pair2){ return pair1.second > pair2.second;});

            for (qsizetype j = 0; j < pair_codedItemIdentifier_value.count(); ++j)
                index_span.second[j] = pair_codedItemIdentifier_value.at(j).first;

        }));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    ////////////////

    // setting up barcode function
    ////////////////

    std::function<double (const QList<double> &, const QList<qsizetype> &)> barcodeFunction;

    if (parameters->barcodeFunction == "Student T")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {StudentTTest studentTTest(list, codedSetMembers); return studentTTest.zTransformedPValue();};
    else if (parameters->barcodeFunction == "Welch T")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {WelchTTest welchT(list, codedSetMembers); return welchT.zTransformedPValue();};
    else if (parameters->barcodeFunction == "Mann-Whitney U")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {MannWhitneyUTest mannWhitneyUTest(list, codedSetMembers); return mannWhitneyUTest.zTransformedPValue();};
    else if (parameters->barcodeFunction == "Kolmogorov-Smirnov Z")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {KolmogorovSmirnovTest kolmogorovsmirnovZ(list, codedSetMembers); return kolmogorovsmirnovZ.zTransformedPValue();};
    else if (parameters->barcodeFunction == "Fisher exact")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {FisherExact2x2Test<double> fisherExact2x2Test1(list, codedSetMembers, 0.0, 1.0); FisherExact2x2Test<double> fisherExact2x2Test2(list, codedSetMembers, 0.0, 2.0); return ((fisherExact2x2Test1.zTransformedPValue() > fisherExact2x2Test2.zTransformedPValue()) ? fisherExact2x2Test1.zTransformedPValue() : -fisherExact2x2Test2.zTransformedPValue());};
    else if (parameters->barcodeFunction == "mean")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {

            double sum = 0.0;

            qsizetype n = 0;

            for (qsizetype i = 0; i < list.count(); ++i) {

                if (codedSetMembers.at(i) == 0) {

                    sum += list.at(i);

                    ++n;

                }

            }

            return sum / static_cast<double>(n);

        };
    else if (parameters->barcodeFunction == "median")
        barcodeFunction = [&](const QList<double> &list, const QList<qsizetype> &codedSetMembers) {

            QList<double> temp;

            for (qsizetype i = 0; i < list.count(); ++i) {

                if (codedSetMembers.at(i) == 0)
                    temp.append(list.at(i));

            }

            return boost::math::statistics::median(temp);

        };


    ////////////////

    // generation barcodes
    ////////////////

    QList<double> multivariatePermutationThresholdPerSetCollection;

    uuidProgress = this->startProgress("processing set collections", 0, parameters->pathToSetCollections.count());

    for (qsizetype i = 0; i < parameters->pathToSetCollections.count(); ++i) {

        this->updateProgressLabel(uuidProgress, "processing set collection \"" + setCollectionToPrettyPrintLabels.at(i) + "\"");

        const QList<std::tuple<QString, QString, QSet<QString>>> &setsInfo(setCollectionToSetsInfo.at(i));

        const QList<QList<qsizetype>> &setToCodedSetMembers(collectionToSetToCodedSetMembers.at(i));

        QUuid uuidProgress_1 = this->startProgress("calculating barcodes", 0, setToCodedSetMembers.count() * dataForBarcodeGeneration.count());

        std::function<QList<double>(const QList<qsizetype> &)> barcode_mapFunctor = [this, &dataForBarcodeGeneration, &barcodeFunction, &uuidProgress_1](const QList<qsizetype> &codedSetMembers) {

            QList<double> barcode(dataForBarcodeGeneration.count());

            for (qsizetype j = 0; j < dataForBarcodeGeneration.count(); ++j) {

                barcode[j] = barcodeFunction(dataForBarcodeGeneration.at(j), codedSetMembers);

                if (this->thread()->isInterruptionRequested())
                    return barcode;

                this->checkIfPauseWasRequested();

                this->updateProgressWithOne(uuidProgress_1);

            }

            return barcode;

        };

        auto barcode_reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

            result.append(intermediateResult);

        };

        QList<double> barcodes = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, setToCodedSetMembers, barcode_mapFunctor, std::function<void(QList<double> &, const QList<double> &)>(barcode_reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress_1);


        if (parameters->selectedResultItems.contains("barcodes per set collection")) {

            AnnotatedMatrix<double> *result_barcodesPerSetCollection = new AnnotatedMatrix<double>(setCollectionToSetLabels.at(i), parameters->selectedItemIdentifiers, barcodes);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("barcodes \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", result_barcodesPerSetCollection));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_barcodesPerSetCollection);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "barcodes_" + setCollectionToPrettyPrintLabels.at(i));

            }

        }

        ////////////////

        // transforming barcodes for distance function and setting up distance funtions
        ////////////////

        std::function<double (const std::pair<qsizetype, std::span<double>> &, const std::pair<qsizetype, std::span<double>> &)> associationFunction;

        std::function<double (QList<double> &, const std::pair<qsizetype, std::span<double>> &)> associationFunctionWhenVariableIsMemberOfSet;

        QList<std::pair<qsizetype, std::span<double>>> barcodes_index_span = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(barcodes, parameters->selectedItemIndexes.count());

        QList<double> barcodes_tiesCorrectionFactor;

        QList<QList<std::tuple<double, qsizetype, qsizetype, double>>> barcodes_vector_orderStatistics_orderIndices_rank_cummulativeSum;

        QList<double> barcodes_variance;

        QList<qsizetype> powersOfTwo;

        if (parameters->associationFunction == "Pearson R") {

            this->startProgress("center barcodes", QtConcurrent::map(&d_threadPool, barcodes_index_span, [](std::pair<qsizetype, std::span<double>> &index_span){MathOperations::centerVectorToMeanInplace(index_span.second);}));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();


            this->startProgress("scale barcodes", QtConcurrent::map(&d_threadPool, barcodes_index_span, [](std::pair<qsizetype, std::span<double>> &index_span){MathOperations::scaleVectorToL2NormInplace(index_span.second);}));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            associationFunction = [&](const std::pair<qsizetype, std::span<double>> &barcode_index_span, const std::pair<qsizetype, std::span<double>> &variable_index_span) {return cblas_ddot(barcode_index_span.second.size(), barcode_index_span.second.data(), 1.0, variable_index_span.second.data(), 1.0);};

            associationFunctionWhenVariableIsMemberOfSet = [&](QList<double> &barcode, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                MathOperations::centerVectorToMeanInplace(barcode);

                MathOperations::scaleVectorToL2NormInplace(barcode);

                return cblas_ddot(barcode.count(), barcode.data(), 1.0, variable_index_span.second.data(), 1.0);

            };

        }

        if (parameters->associationFunction == "Spearman R") {

            barcodes_tiesCorrectionFactor.resize((barcodes_index_span.count()));

            this->startProgress("ranking barcodes", QtConcurrent::map(&d_threadPool, barcodes_index_span, [&barcodes_tiesCorrectionFactor](std::pair<qsizetype, std::span<double>> &index_span){barcodes_tiesCorrectionFactor[index_span.first] = MathOperations::crankInplace(index_span.second);}));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            associationFunction = [&](const std::pair<qsizetype, std::span<double>> &barcode_index_span, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                double n = barcode_index_span.second.size();

                double d = 0.0;

                for (unsigned int j = 0; j < barcode_index_span.second.size(); ++j)
                    d += (variable_index_span.second[j] - barcode_index_span.second[j]) * (variable_index_span.second[j] - barcode_index_span.second[j]);

                double en3n = n * n * n - n;

                double fac = (1.0 - barcodes_tiesCorrectionFactor.at(barcode_index_span.first) / en3n) * (1.0 - variables_tiesCorrectionFactor.at(variable_index_span.first) / en3n);

                return (1.0 - (6.0 / en3n) * (d + (barcodes_tiesCorrectionFactor.at(barcode_index_span.first) + variables_tiesCorrectionFactor.at(variable_index_span.first)) / 12.0)) / std::sqrt(fac);

            };

            associationFunctionWhenVariableIsMemberOfSet = [&](QList<double> &barcode, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                double barcode_tiesCorrectionFactor = MathOperations::crankInplace(barcode);

                double n = barcode.count();

                double d = 0.0;

                for (unsigned int j = 0; j < barcode.count(); ++j)
                    d += (variable_index_span.second[j] - barcode.at(j)) * (variable_index_span.second[j] - barcode.at(j));

                double en3n = n * n * n - n;

                double fac = (1.0 - barcode_tiesCorrectionFactor / en3n) * (1.0 - variables_tiesCorrectionFactor.at(variable_index_span.first) / en3n);

                return (1.0 - (6.0 / en3n) * (d + (barcode_tiesCorrectionFactor + variables_tiesCorrectionFactor.at(variable_index_span.first)) / 12.0)) / std::sqrt(fac);

            };

        }

        if (parameters->associationFunction == "Distance R") {

            barcodes_vector_orderStatistics_orderIndices_rank_cummulativeSum.resize((barcodes_index_span.count()));

            barcodes_variance.resize((barcodes_index_span.count()));

            this->startProgress("creating order statistics for barcodes", QtConcurrent::map(&d_threadPool, barcodes_index_span, [&barcodes_vector_orderStatistics_orderIndices_rank_cummulativeSum, &barcodes_variance](std::pair<qsizetype, std::span<double>> &index_span){barcodes_vector_orderStatistics_orderIndices_rank_cummulativeSum[index_span.first] = FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(index_span.second); barcodes_variance[index_span.first] = boost::math::statistics::sample_variance(index_span.second);}));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            qsizetype L = static_cast<int>(std::ceil(std::log2(static_cast<double>(parameters->selectedItemIndexes.count()))));

            powersOfTwo.resize(L + 2);

            for (qsizetype i = 0; i <= L + 1; ++i)
                powersOfTwo[i] = static_cast<qsizetype>(std::pow(2.0, static_cast<double>(i)));

            associationFunction = [&](const std::pair<qsizetype, std::span<double>> &barcode_index_span, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                return FastDistanceCorrelation::fastDistanceCorrelation(barcode_index_span.second, variable_index_span.second, barcodes_vector_orderStatistics_orderIndices_rank_cummulativeSum.at(barcode_index_span.first), FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(variable_index_span.second), barcodes_variance.at(barcode_index_span.first), variables_variance.at(variable_index_span.first), powersOfTwo, L);

            };

            associationFunctionWhenVariableIsMemberOfSet = [&](QList<double> &barcode, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                return FastDistanceCorrelation::fastDistanceCorrelation(std::span<double>(barcode.data(), barcode.count()), variable_index_span.second, FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(barcode), FastDistanceCorrelation::createVector_orderStatistics_orderIndices_rank_cummulativeSum(variable_index_span.second), boost::math::statistics::sample_variance(barcode), variables_variance.at(variable_index_span.first), powersOfTwo, L);

            };

        }

        if (parameters->associationFunction == "rank-biased overlap") {

            this->startProgress("replacing values with ranks", QtConcurrent::map(&d_threadPool, barcodes_index_span, [&](std::pair<qsizetype, std::span<double>> &index_span){

                QList<std::pair<qsizetype, T>> pair_codedItemIdentifier_value;

                pair_codedItemIdentifier_value.reserve(parameters->selectedItemIndexes.count());

                for (qsizetype j = 0; j < parameters->selectedItemIndexes.count(); ++j)
                    pair_codedItemIdentifier_value.append(std::pair<qsizetype, T>(itemIdentifierToCodedItemIdentifier.value(parameters->selectedItemIdentifiers.at(j)), index_span.second[j]));

                std::sort(pair_codedItemIdentifier_value.begin(), pair_codedItemIdentifier_value.end(), [](const std::pair<qsizetype, T> &pair1, const std::pair<qsizetype, T> &pair2){ return pair1.second > pair2.second;});

                for (qsizetype j = 0; j < pair_codedItemIdentifier_value.count(); ++j)
                    index_span.second[j] = pair_codedItemIdentifier_value.at(j).first;

            }));

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            associationFunction = [&](const std::pair<qsizetype, std::span<double>> &barcode_index_span, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                return this->rankBiasedOverlap(barcode_index_span.second, variable_index_span.second, parameters->maxDepth_rankBiasedOverlap, parameters->p_rankBiasedOverlap);

            };

            associationFunctionWhenVariableIsMemberOfSet = [&](QList<double> &barcode, const std::pair<qsizetype, std::span<double>> &variable_index_span) {

                QList<std::pair<qsizetype, T>> pair_codedItemIdentifier_value;

                pair_codedItemIdentifier_value.reserve(parameters->selectedItemIndexes.count());

                for (qsizetype j = 0; j < parameters->selectedItemIndexes.count(); ++j)
                    pair_codedItemIdentifier_value.append(std::pair<qsizetype, T>(itemIdentifierToCodedItemIdentifier.value(parameters->selectedItemIdentifiers.at(j)), barcode.at(j)));

                std::sort(pair_codedItemIdentifier_value.begin(), pair_codedItemIdentifier_value.end(), [](const std::pair<qsizetype, T> &pair1, const std::pair<qsizetype, T> &pair2){ return pair1.second > pair2.second;});

                for (qsizetype j = 0; j < pair_codedItemIdentifier_value.count(); ++j)
                    barcode[j] = pair_codedItemIdentifier_value.at(j).first;

                return this->rankBiasedOverlap(std::span<double>(barcode.data(), barcode.count()), variable_index_span.second, parameters->maxDepth_rankBiasedOverlap, parameters->p_rankBiasedOverlap);

            };

        }

        ////////////////

        // defining functions to create distance matrix between variables and barcodes
        ////////////////

        auto distanceMatrix_mapFunctor = [&](const std::pair<qsizetype, std::span<double>> &variable_index_span) {

            QList<double> intermediateResults(barcodes_index_span.count(), 0.0);

            QString variableIdentifier = variableIdentifiers.at(variable_index_span.first);

            for (qsizetype i = 0; i < barcodes_index_span.count(); ++i) {

                if (!std::get<2>(setsInfo.at(i)).contains(variableIdentifier))
                    intermediateResults[i] = associationFunction(barcodes_index_span.at(i), variable_index_span);
                else {

                    QList<qsizetype> tempCodedSetMembers = setToCodedSetMembers.at(i);

                    tempCodedSetMembers[variableIdentifierToIndex.value(variableIdentifier)] = 0;

                    QList<double> tempBarcode(dataForBarcodeGeneration.count());

                    for (qsizetype j = 0; j < dataForBarcodeGeneration.count(); ++j) {

                        tempBarcode[j] = barcodeFunction(dataForBarcodeGeneration.at(j), tempCodedSetMembers);

                        if (this->thread()->isInterruptionRequested())
                            return intermediateResults;

                        this->checkIfPauseWasRequested();

                    }

                    intermediateResults[i] = associationFunctionWhenVariableIsMemberOfSet(tempBarcode, variable_index_span);

                }

                if (this->thread()->isInterruptionRequested())
                    return intermediateResults;

                this->checkIfPauseWasRequested();

                this->updateProgressWithOne(uuidProgress_1);

            }

            return intermediateResults;

        };

        auto distanceMatrix_reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

            result.append(intermediateResult);

        };

        ////////////////

        // calculating observed distance matrix between variables and barcodes
        ////////////////

        uuidProgress_1 = this->startProgress("calculating distance matrix (variables versus barcodes)", 0, barcodes_index_span.count() * variables_index_span.count());

        QList<double> observed_associationMatrix;

        observed_associationMatrix.reserve(barcodes_index_span.count() * variables_index_span.count());

        observed_associationMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, variables_index_span, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &)>(distanceMatrix_mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(distanceMatrix_reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress_1);


        if (parameters->selectedResultItems.contains("guilt-by-association metrics per set collection")) {

            AnnotatedMatrix<double> *result_associationMetricsPerSetCollection = new AnnotatedMatrix<double>(concatenatedAnnotationValuesForVariables, setCollectionToSetLabels.at(i), observed_associationMatrix);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("guilt-by-association metrics \"" + setCollectionToPrettyPrintLabels.at(i) + "\"", result_associationMetricsPerSetCollection));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_associationMetricsPerSetCollection);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "guilt_by_association_metrics_" + setCollectionToPrettyPrintLabels.at(i));

            }

        }

        ////////////////

        // performing permutations
        ////////////////

        if (parameters->enablePermutationMode) {

            QUuid uuidProgress_2 = this->startProgress("performing permutations", 0, parameters->numberOfPermutations);

            QList<double> multivariatePermutationDistribution;

            std::for_each(observed_associationMatrix.begin(), observed_associationMatrix.end(), [&](double &value){value = std::abs(value);});

            std::sort(observed_associationMatrix.begin(), observed_associationMatrix.end(), std::greater<double>());


            QList<qsizetype> indexes(parameters->selectedItemIndexes.count());

            std::iota (std::begin(indexes), std::end(indexes), 0);

            std::random_device rd;

            std::mt19937 g(rd());


            for (qsizetype i = 0; i < parameters->numberOfPermutations; ++i) {

                uuidProgress_1 = this->startProgress("calculating distance matrix (variables versus barcodes) for permutation round " + QString::number(i + 1), 0, barcodes_index_span.count() * variables_index_span.count());


                std::shuffle(indexes.begin(), indexes.end(), g);

                std::for_each(variables_index_span.begin(), variables_index_span.end(), [&](std::pair<qsizetype, std::span<double>> &variable_index_span){

                    QList<double> temp(indexes.count());

                    for (qsizetype j = 0; j < indexes.count(); ++j)
                        temp[j] = variable_index_span.second[indexes.at(j)];

                    for (qsizetype j = 0; j < indexes.count(); ++j)
                        variable_index_span.second[j] = temp.at(j);

                });


                QList<double> permutation_associationMatrix;

                permutation_associationMatrix.reserve(barcodes_index_span.count() * variables_index_span.count());

                permutation_associationMatrix = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, variables_index_span, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &)>(distanceMatrix_mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(distanceMatrix_reduceFunctor), QtConcurrent::UnorderedReduce | QtConcurrent::SequentialReduce);

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->stopProgress(uuidProgress_1);


                std::for_each(permutation_associationMatrix.begin(), permutation_associationMatrix.end(), [&](double &value){value = std::abs(value);});

                std::sort(permutation_associationMatrix.begin(), permutation_associationMatrix.end(), std::greater<double>());


                QList<std::pair<double, bool>> combinedAssociationMatrix;

                combinedAssociationMatrix.reserve(2 * observed_associationMatrix.count());


                for (qsizetype j = 0; j < observed_associationMatrix.count(); ++j) {

                    combinedAssociationMatrix.append(std::pair<double, bool>(observed_associationMatrix.at(j), true));

                    combinedAssociationMatrix.append(std::pair<double, bool>(permutation_associationMatrix.at(j), false));


                }

                std::sort(combinedAssociationMatrix.begin(), combinedAssociationMatrix.end(), [](const std::pair<double, bool> &value1, const std::pair<double, bool> & value2){return value1.first > value2.first;});


                qsizetype countObservation = 0;

                qsizetype countPermutation = 0;

                for (const std::pair<double, bool> &value : combinedAssociationMatrix) {

                    if (value.second)
                        ++countObservation;
                    else
                        ++countPermutation;

                    if ((static_cast<double>(countPermutation) / static_cast<double>(countObservation)) > parameters->falseDiscoveryRate) {

                        multivariatePermutationDistribution.append(value.first);

                        break;

                    }

                }

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress_2, i + 1);

            }


            std::sort(multivariatePermutationDistribution.begin(), multivariatePermutationDistribution.end(), std::greater<double>());

            multivariatePermutationThresholdPerSetCollection.append(MathDescriptives::percentileOfPresortedList(multivariatePermutationDistribution, parameters->confidenceLevel));

            this->stopProgress(uuidProgress_2);

            AnnotatedMatrix<double> *result_multivariatePermutationThresholdPerSetCollection = new AnnotatedMatrix<double>(setCollectionToPrettyPrintLabels, {"absolute MVP threshold (FDR = " + QString::number(parameters->falseDiscoveryRate * 100.0) + "%; CL = " + QString::number(parameters->confidenceLevel * 100.0) + "%)"}, multivariatePermutationThresholdPerSetCollection);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("multivariate permutation thresholds", result_multivariatePermutationThresholdPerSetCollection));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_multivariatePermutationThresholdPerSetCollection);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "multivariate_permutation_thresholds");

            }

        }

    }

    this->stopProgress(uuidProgress);

}

#endif // GUILTBYASSOCIATIONANALYSISWORKER_H
