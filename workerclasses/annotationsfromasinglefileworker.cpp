#include "annotationsfromasinglefileworker.h"

AnnotationsFromASingleFileParameters::AnnotationsFromASingleFileParameters(QObject *parent) :
    BaseParameters(parent, "importannotationsfromasinglefile")
{

}

bool AnnotationsFromASingleFileParameters::isValid_()
{

    if (!QFile::exists(pathToFile)) {

        d_lastError = "could not find file \"" + pathToFile + "\"";

        return false;

    }

    if (lineNumberOfLastDataLine < lineNumberOfFirstDataLine) {

        d_lastError = "line number of last data line (" + QString::number(lineNumberOfLastDataLine) + ") can not be less than line number of first data line (" + QString::number(lineNumberOfFirstDataLine) + ")";

        return false;

    }

    if ((lineNumberOfHeader != -1 ) && (lineNumberOfFirstDataLine <= lineNumberOfHeader)) {

        d_lastError = "line number of first data line (" + QString::number(lineNumberOfFirstDataLine) + ") can not be less than or equal to line number of header line (" + QString::number(lineNumberOfHeader) + ")";

        return false;

    }

    if ((indexOfColumnDefiningSecondDataColumn != -1) && (indexOfColumnDefiningSecondDataColumn < indexOfColumnDefiningFirstDataColumn)) {

        d_lastError = "index of column defining second data column (" + QString::number(indexOfColumnDefiningSecondDataColumn) + ") can not be less than index of column defining first data column (" + QString::number(indexOfColumnDefiningFirstDataColumn) + ")";

        return false;

    }

    if (indexOfColumnDefiningFirstDataColumn < indexOfColumnDefiningRowIdentifiers) {

        d_lastError = "index of column defining first data column (" + QString::number(indexOfColumnDefiningFirstDataColumn) + ") can not be less than index of column defining row identifiers (" + QString::number(indexOfColumnDefiningRowIdentifiers) + ")";

        return false;

    }

    return true;

}

QString AnnotationsFromASingleFileParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[import annotations in data with label = \"" + baseAnnotatedMatrix->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation = " << ((orientation == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[path to file = \"" << pathToFile << "\"]";

    stream << Qt::endl << "\t[seperator = \"" << delimiter << "\"]";

    if (lineNumberOfHeader == -1)
        stream << Qt::endl << "\t[line number of header = \"header not present\"]";
    else {

        stream << Qt::endl << "\t[first token in header defines column with row identifiers = " << ((firstTokenInHeaderDefinesColumnWithRowIdentifiers) ? "true" : "false") << "]";

        stream << Qt::endl << "\t[line number of header = " << lineNumberOfHeader << "]";

    }

    stream << Qt::endl << "\t[line number of first data line = " << lineNumberOfFirstDataLine << "]";

    if (lineNumberOfLastDataLine == -1)
        stream << Qt::endl << "\t[line number of last data line = \"automatically detected\"]";
    else
        stream << Qt::endl << "\t[line number of last data line = " << lineNumberOfLastDataLine << "]";

    stream << Qt::endl << "\t[index of column defining row identifiers = " << indexOfColumnDefiningRowIdentifiers << "]";

    stream << Qt::endl << "\t[index of column defining first data column = " << indexOfColumnDefiningFirstDataColumn << "]";

    stream << Qt::endl << "\t[index of column defining second data column = " << indexOfColumnDefiningSecondDataColumn << "]";

    return prettyPrintString;

}

AnnotationsFromASingleFileWorker::AnnotationsFromASingleFileWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("import annotations from a single file"), parameters, "importannotationsfromasinglefile")
{

}

void AnnotationsFromASingleFileWorker::doWork_()
{

    QSharedPointer<AnnotationsFromASingleFileParameters> parameters(d_data->d_parameters.dynamicCast<AnnotationsFromASingleFileParameters>());

    QFile fileIn(parameters->pathToFile);

    if (!fileIn.exists() || !fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    QTextStream textStreamIn(&fileIn);

    textStreamIn.setEncoding(QStringConverter::Latin1);

    QUuid uuidProgress = this->startProgress("importing file \"" + parameters->pathToFile + "\"", 0, fileIn.size());

    qsizetype numberOfLinesRead = 0;

    while (!textStreamIn.atEnd() && (numberOfLinesRead < (parameters->lineNumberOfHeader - 1))) {

        textStreamIn.readLine();

        ++numberOfLinesRead;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    QList<QString> tokens;

    qsizetype numberOfTokensInReferenceLine = 0;

    if (parameters->lineNumberOfHeader != -1) {

        if (!parameters->firstTokenInHeaderDefinesColumnWithRowIdentifiers)
            tokens << QString();

        if (!textStreamIn.atEnd()) {

            tokens << textStreamIn.readLine().split(QRegularExpression(parameters->delimiter), Qt::KeepEmptyParts);

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

        if (numberOfLinesRead != (parameters->lineNumberOfHeader)) {

            this->reportError("line number of header (" + QString::number(parameters->lineNumberOfHeader) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + parameters->pathToFile + "\"");

            QThread::currentThread()->requestInterruption();

            return;

        }

        numberOfTokensInReferenceLine = tokens.count();

    }

    qsizetype deltaIndex = (parameters->indexOfColumnDefiningSecondDataColumn != -1) ? parameters->indexOfColumnDefiningSecondDataColumn - parameters->indexOfColumnDefiningFirstDataColumn : tokens.count();

    QList<QString> annotationLabels;

    if (parameters->lineNumberOfHeader != -1) {

        for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.size(); i += deltaIndex) {

            if (!parameters->baseAnnotatedMatrix->containsAnnotationLabel(tokens.at(i).trimmed(), parameters->orientation))
                parameters->baseAnnotatedMatrix->appendAnnotationLabel(tokens.at(i).trimmed(), parameters->orientation);

            annotationLabels.append(tokens.at(i).trimmed());

        }

    }

    if (parameters->lineNumberOfHeader != -1) {

        while (!textStreamIn.atEnd() && (numberOfLinesRead < parameters->lineNumberOfFirstDataLine - parameters->lineNumberOfHeader - 1)) {

            textStreamIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    } else {

        while (!textStreamIn.atEnd() && (numberOfLinesRead < parameters->lineNumberOfFirstDataLine - 1)) {

            textStreamIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    }

    bool setNumberOfTokensInReferenceLine = (parameters->lineNumberOfHeader == -1) ? true : false;

    while (!textStreamIn.atEnd() && ((parameters->lineNumberOfLastDataLine != -1) || (numberOfLinesRead != parameters->lineNumberOfLastDataLine))) {

        tokens = textStreamIn.readLine().split(QRegularExpression(parameters->delimiter), Qt::KeepEmptyParts);

        if (setNumberOfTokensInReferenceLine) {

            for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.size(); i += deltaIndex)
                annotationLabels.append("label");

            annotationLabels = parameters->baseAnnotatedMatrix->appendAnnotationLabels(annotationLabels, parameters->orientation);

            numberOfTokensInReferenceLine = tokens.count();

            setNumberOfTokensInReferenceLine = false;

        }

        ++numberOfLinesRead;

        if (numberOfLinesRead == parameters->lineNumberOfFirstDataLine)
            numberOfTokensInReferenceLine = tokens.count();

        if (tokens.size() != numberOfTokensInReferenceLine) {

            if (parameters->lineNumberOfHeader != -1)
                this->reportError("the number of tokens read at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the header line");
            else
                this->reportError("the number of tokens read at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the first data line");

            QThread::currentThread()->requestInterruption();

            return;

        }

        QList<QString>::const_iterator iteratorAnnotationLabels = annotationLabels.cbegin();

        QString identifier = tokens.at(parameters->indexOfColumnDefiningRowIdentifiers).trimmed();

        if (!parameters->baseAnnotatedMatrix->containsIdentifier(identifier, parameters->orientation))
            this->reportWarning("identifier \"" + identifier + "\" at line " + QString::number(numberOfLinesRead) + " is not present in data \"" + parameters->baseAnnotatedMatrix->property("label").toString());
        else {

            for (qsizetype i = parameters->indexOfColumnDefiningFirstDataColumn; i < tokens.size(); i += deltaIndex) {

                QVariant currentAnnotationValue = parameters->baseAnnotatedMatrix->annotationValue(identifier, *iteratorAnnotationLabels, parameters->orientation);

                if (!tokens.at(i).trimmed().isEmpty()) {

                    if (!parameters->baseAnnotatedMatrix->setAnnotationValue(identifier, *iteratorAnnotationLabels, tokens.at(i).trimmed(), parameters->orientation))
                        this->reportWarning("annotation value \"" + tokens.at(i).trimmed() + "\" at line " + QString::number(numberOfLinesRead) + " for " + QString(parameters->orientation == Qt::Horizontal ? "column" : "row") + " annotation label \"" + *iteratorAnnotationLabels + "\" and identifier \"" + identifier + "\" overwrites current annotation value \"" + currentAnnotationValue.toString() + "\"");

                }

                ++iteratorAnnotationLabels;

            }

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    if ((parameters->lineNumberOfLastDataLine != -1) && (parameters->lineNumberOfLastDataLine != numberOfLinesRead)) {

        this->reportError("line number of last data line (" + QString::number(parameters->lineNumberOfLastDataLine) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + parameters->pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    this->reportMessage("total number of " + QString(parameters->orientation == Qt::Horizontal ? "column" : "row") + " annotation labels after importing : " + QString::number(parameters->baseAnnotatedMatrix->annotationLabelCount(parameters->orientation)));

}

bool AnnotationsFromASingleFileWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<AnnotationsFromASingleFileParameters> parameters(d_data->d_parameters.dynamicCast<AnnotationsFromASingleFileParameters>());

    parameters->baseAnnotatedMatrix->lockForWrite();

    if (parameters->orientation == Qt::Horizontal)
        parameters->baseAnnotatedMatrix->beginResetColumnAnnotations();
    else
        parameters->baseAnnotatedMatrix->beginResetRowAnnotations();

    return true;

}

void AnnotationsFromASingleFileWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<AnnotationsFromASingleFileParameters> parameters(d_data->d_parameters.dynamicCast<AnnotationsFromASingleFileParameters>());

    if (parameters->orientation == Qt::Horizontal)
        parameters->baseAnnotatedMatrix->endResetColumnAnnotations();
    else
        parameters->baseAnnotatedMatrix->endResetRowAnnotations();

    parameters->baseAnnotatedMatrix->unlock();

}
