#ifndef MATRIX_H
#define MATRIX_H

#include <QtGlobal>
#include <QList>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QTextStream>
#include <QDataStream>
#include <QIODevice>

#include <span>
#include <functional>
#include <typeinfo>
#include <typeindex>
#include <iterator>
#include <type_traits>
#include <algorithm>
#include <cstdlib>
#include <utility>

#include "boost/range/adaptor/strided.hpp"
#include "boost/range/algorithm.hpp"
#include "math/mathoperations.h"

class BaseMatrixData : public QSharedData
{

public:

    qsizetype d_columnCount;

    qsizetype d_rowCount;

    const char* d_typeName;

};

class BaseMatrix
{

public:

    BaseMatrix();

    explicit BaseMatrix(BaseMatrix &&baseMatrix);

    explicit BaseMatrix(const BaseMatrix &baseMatrix);

    BaseMatrix(qsizetype rowCount, qsizetype columnCount, const char *typeName = nullptr);

    qsizetype columnCount() const;

    qsizetype rowCount() const;

    virtual void transpose() = 0;

    const char* typeName() const;

    virtual ~BaseMatrix();

    friend QDataStream& operator<<(QDataStream& out, BaseMatrix &baseMatrix);

    friend QDataStream& operator>>(QDataStream& in, BaseMatrix &matrix);

protected:

    BaseMatrix &operator=(BaseMatrix &&baseMatrix);

    BaseMatrix &operator=(const BaseMatrix &baseMatrix);

    QSharedDataPointer<BaseMatrixData> d_data;

    virtual QDataStream &writeToDataStream(QDataStream &out) = 0;

    virtual QDataStream &readFromDataStream(QDataStream &in) = 0;

};

template <typename T>
class MatrixData : public QSharedData
{

public:

    QList<T> d_values;

};

template <typename T>
class Matrix : public BaseMatrix
{

    template<typename U> friend class Matrix;

public:

    Matrix();

    explicit Matrix(Matrix &&matrix);

    template <typename U> explicit Matrix(Matrix<U> &&matrix);

    explicit Matrix(const Matrix &matrix);

    template <typename U> explicit Matrix(const Matrix<U> &matrix);

    Matrix(qsizetype rowCount, qsizetype columnCount, QList<T> &&values);

    template <typename U> explicit Matrix(qsizetype rowCount, qsizetype columnCount, QList<U> &&values);

    Matrix(qsizetype rowCount, qsizetype columnCount, const QList<T> &values);

    template <typename U> explicit Matrix(qsizetype rowCount, qsizetype columnCount, const QList<U> &values);

    Matrix(qsizetype rowCount, qsizetype columnCount, const T &value);

    QList<T> columnAt(qsizetype index) const;

    template <typename U> QList<U> columnAt(qsizetype index) const;

    QList<T> columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const;

    template <typename U> QList<U> columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const;

    QList<std::reference_wrapper<T>> columnAt_references(qsizetype index);

    T *data();

    const T *data() const;

    template <typename U> friend QTextStream& operator<<(QTextStream& out, const Matrix<U> &matrix);

    Matrix &operator=(Matrix &&matrix);

    template <typename U> Matrix &operator=(Matrix<U> &&matrix);

    Matrix &operator=(const Matrix &matrix);

    template <typename U> Matrix &operator=(const Matrix<U> &matrix);

    std::span<T> operator[](qsizetype index);

    QList<T> rowAt(qsizetype index) const;

    template <typename U> QList<U> rowAt(qsizetype index) const;

    QList<T> rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const;

    template <typename U> QList<U> rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const;

    QList<std::reference_wrapper<T>> rowAt_references(qsizetype index);

    QList<T> sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const;

    template <typename U> QList<U> sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const;

    QList<std::reference_wrapper<T> > sliced_references(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes);

    const QList<T> values() const;

    void transpose() override;

    virtual ~Matrix();

protected:

    QSharedDataPointer<MatrixData<T> > d_data;

    void appendColumn(const QList<T> &column);

    template <typename U> void appendColumn(const QList<U> &column);

    void appendColumns_1d(const QList<T> &columns);

    template <typename U> void appendColumns_1d(const QList<U> &column);

    void appendColumns_2d(const QList<QList<T>> &columns);

    template <typename U> void appendColumns_2d(const QList<QList<U>> &columns);

    void appendRow(const QList<T> &row);

    template <typename U> void appendRow(const QList<U> &row);

    void appendRows_1d(const QList<T> &rows);

    template <typename U> void appendRows_1d(const QList<U> &rows);

    void appendRows_2d(const QList<QList<T>> &rows);

    template <typename U> void appendRows_2d(const QList<QList<U>> &rows);

    void insertColumn(qsizetype index, const QList<T> &column);

    template <typename U> void insertColumn(qsizetype index, const QList<U> &column);

    void insertColumns_1d(qsizetype index, const QList<T> &columns);

    template <typename U> void insertColumns_1d(qsizetype index, const QList<U> &columns);

    void insertColumns_2d(qsizetype index, const QList<QList<T>> &columns);

    template <typename U> void insertColumns_2d(qsizetype index, const QList<QList<U>> &columns);

    void insertRow(qsizetype index, const QList<T> &row);

    template <typename U> void insertRow(qsizetype index, const QList<U> &row);

    void insertRows_1d(qsizetype index, const QList<T> &rows);

    template <typename U> void insertRows_1d(qsizetype index, const QList<U> &rows);

    void insertRows_2d(qsizetype index, const QList<QList<T>> &rows);

    template <typename U> void insertRows_2d(qsizetype index, const QList<QList<U>> &rows);

    void prependColumn(const QList<T> &column);

    template <typename U> void prependColumn(const QList<U> &column);

    void prependColumns_1d(const QList<T> &columns);

    template <typename U> void prependColumns_1d(const QList<U> &column);

    void prependColumns_2d(const QList<QList<T>> &columns);

    template <typename U> void prependColumns_2d(const QList<QList<U>> &columns);

    void prependRow(const QList<T> &row);

    template <typename U> void prependRow(const QList<U> &row);

    void prependRows_1d(const QList<T> &rows);

    template <typename U> void prependRows_1d(const QList<U> &rows);

    void prependRows_2d(const QList<QList<T>> &rows);

    template <typename U> void prependRows_2d(const QList<QList<U>> &rows);

    void removeColumn(qsizetype index);

    void removeColumns(QList<qsizetype> indexes);

    void removeRow(qsizetype index);

    void removeRows(QList<qsizetype> indexes);

    void setData(qsizetype rowCount, qsizetype columnCount, QList<T> &&values);

    void setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &values);

    template <typename U> void setData(qsizetype rowCount, qsizetype columnCount, QList<U> &&values);

    template <typename U> void setData(qsizetype rowCount, qsizetype columnCount, const QList<U> &values);

    void setData(qsizetype rowCount, qsizetype columnCount, const T &value);

    QDataStream &writeToDataStream(QDataStream &out) override;

    QDataStream &readFromDataStream(QDataStream &in) override;

private:

    typename QList<T>::iterator beginRowAt(qsizetype index);

    typename QList<T>::const_iterator beginRowAt(qsizetype index) const;

};

template <typename T>
Matrix<T>::Matrix() :
    BaseMatrix(),
    d_data(new MatrixData<T>)
{

    BaseMatrix::d_data->d_typeName = typeid(T).name();

}

template <typename T>
Matrix<T>::Matrix(Matrix &&matrix) :
    BaseMatrix(std::move(matrix)),
    d_data(new MatrixData<T>)
{

    d_data->d_values = std::move(matrix.d_data->d_values);

}

template <typename T>
Matrix<T>::Matrix(const Matrix &matrix) :
    BaseMatrix(matrix),
    d_data(matrix.d_data)
{

}

template <typename T>
template <typename U> Matrix<T>::Matrix(Matrix<U> &&matrix) :
    BaseMatrix(std::move(matrix)),
    d_data(new MatrixData<T>)
{

    d_data->d_values.reserve(matrix.d_data->d_values.count());

    while (!matrix.d_data->d_values.isEmpty())
         d_data->d_values.append(matrix.d_data->d_values.takeLast());

    std::reverse(d_data->d_values.begin(), d_data->d_values.end());

    BaseMatrix::d_data->d_typeName = typeid(T).name();

}

template <typename T>
template <typename U> Matrix<T>::Matrix(const Matrix<U> &matrix) :
    BaseMatrix(matrix),
    d_data(new MatrixData<T>)
{

    d_data->d_values.reserve(matrix.values().count());

    std::copy(matrix.values().begin(), matrix.values().end(), std::back_inserter(d_data->d_values));

    BaseMatrix::d_data->d_typeName = typeid(T).name();

}

template <typename T>
Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, QList<T> &&values) :
    BaseMatrix(rowCount, columnCount, typeid(T).name()),
    d_data(new MatrixData<T>)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, QList<T> &&values)", "rowCount multiplied with columnCount does not match count of values");

    d_data->d_values = std::move(values);

}

template <typename T>
Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, const QList<T> &values) :
    BaseMatrix(rowCount, columnCount, typeid(T).name()),
    d_data(new MatrixData<T>)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, const QList<T> &values)", "rowCount multiplied with columnCount does not match count of values");

    d_data->d_values = values;

}

template <typename T>
template <typename U> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, QList<U> &&values) :
    BaseMatrix(rowCount, columnCount, typeid(T).name()),
    d_data(new MatrixData<T>)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> template <typename U> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, QList<U> &&values)", "rowCount multiplied with columnCount does not match count of values");

    d_data->d_values.reserve(values.count());

    while (!values.isEmpty())
         d_data->d_values.append(values.takeLast());

    std::reverse(d_data->d_values.begin(), d_data->d_values.end());

}

template <typename T>
template <typename U> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, const QList<U> &values) :
    BaseMatrix(rowCount, columnCount, typeid(T).name()),
    d_data(new MatrixData<T>)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> template <typename U> Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, const QList<U> &values)", "rowCount multiplied with columnCount does not match count of values");

    d_data->d_values.reserve(values.count());

    std::copy(values.begin(), values.end(), std::back_inserter(d_data->d_values));

}

template <typename T>
Matrix<T>::Matrix(qsizetype rowCount, qsizetype columnCount, const T &value) :
    BaseMatrix(rowCount, columnCount, typeid(T).name()),
    d_data(new MatrixData<T>)
{

    d_data->d_values.fill(value, rowCount * columnCount);

}

template <typename T>
void Matrix<T>::appendColumn(const QList<T> &column)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::appendColumn(const QList<T> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<T>(BaseMatrix::d_data->d_columnCount, column);

}

template <typename T>
template <typename U>
void Matrix<T>::appendColumn(const QList<U> &column)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::appendColumn(const QList<U> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<U>(BaseMatrix::d_data->d_columnCount, column);

}

template <typename T>
void Matrix<T>::appendColumns_1d(const QList<T> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> Matrix<T>::appendColumns_1d(const QList<T> &columns)", "count of columns does not allign with multiple of row count of matrix");

    this->insertColumns_1d<T>(BaseMatrix::d_data->d_columnCount, columns);

}

template <typename T>
template <typename U> void Matrix<T>::appendColumns_1d(const QList<U> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> template <typename U> Matrix<T>::appendColumns_1d(const QList<U> &columns)", "count of columns does not allign with multiple of row count of matrix");

    this->insertColumns_1d<U>(BaseMatrix::d_data->d_columnCount, columns);

}

template <typename T>
void Matrix<T>::appendColumns_2d(const QList<QList<T>> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (columns.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::appendColumns_2d(const QList<QList<T>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X(std::all_of(columns.begin(), columns.end(), [&columns](const QList<T> &column){return column.count() == columns.first().count();}), "template <typename T> Matrix<T>::appendColumns_2d(const QList<QList<T>> &columns)", "columns matrix is not rectangle");

    this->insertColumns_2d<T>(BaseMatrix::d_data->d_columnCount, columns);

}

template <typename T>
template <typename U> void Matrix<T>::appendColumns_2d(const QList<QList<U>> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (columns.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::appendColumns_2d(const QList<QList<U>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X(std::all_of(columns.begin(), columns.end(), [&columns](const QList<T> &column){return column.count() == columns.first().count();}), "template <typename T> Matrix<T>::appendColumns_2d(const QList<QList<U>> &columns)", "columns matrix is not rectangle");

    this->insertColumns_2d<U>(BaseMatrix::d_data->d_columnCount, columns);

}

template <typename T>
void Matrix<T>::appendRow(const QList<T> &row)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::appendRow(const QList<T> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<T>(BaseMatrix::d_data->d_rowCount, row);

}

template <typename T>
template <typename U>
void Matrix<T>::appendRow(const QList<U> &row)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::appendRow(const QList<U> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<U>(BaseMatrix::d_data->d_rowCount, row);

}

template <typename T>
void Matrix<T>::appendRows_1d(const QList<T> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> Matrix<T>::appendRows_1d(const QList<T> &rows)", "count of rows does not allign with multiple of column count of matrix");

    this->insertRows_1d<T>(BaseMatrix::d_data->d_rowCount, rows);

}

template <typename T>
template <typename U> void Matrix<T>::appendRows_1d(const QList<U> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> template <typename U> Matrix<T>::appendRows_1d(const QList<U> &rows)", "count of rows does not allign with multiple of column count of matrix");

    this->insertRows_1d<U>(BaseMatrix::d_data->d_rowCount, rows);

}

template <typename T>
void Matrix<T>::appendRows_2d(const QList<QList<T>> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<T> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> Matrix<T>::appendRows_2d(const QList<QList<T>> &rows)", "count of one of the rows does not match currrent column count of matrix");

    this->insertRows_2d<T>(BaseMatrix::d_data->d_rowCount, rows);

}

template <typename T>
template <typename U> void Matrix<T>::appendRows_2d(const QList<QList<U>> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<U> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> template <typename U> Matrix<T>::appendRows_2d(const QList<QList<U>> &rows)", "count of one of the rows does not match currrent column count of matrix");

    this->insertRows_2d<U>(BaseMatrix::d_data->d_rowCount, rows);

}

template <typename T>
QList<T> Matrix<T>::columnAt(qsizetype index) const
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_columnCount), "template <typename T> QList<T> Matrix<T>::columnAt(qsizetype index) const", "index is out of range");

    return this->columnAt<T>(index);

}

template <typename T>
template <typename U> QList<U> Matrix<T>::columnAt(qsizetype index) const
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> QList<U> Matrix<T>::columnAt(qsizetype index) const", "index is out of range");

    QList<U> column;

    column.reserve(BaseMatrix::d_data->d_rowCount);

    boost::range::copy(boost::adaptors::stride(boost::make_iterator_range(d_data->d_values.begin() + index, d_data->d_values.end()), BaseMatrix::d_data->d_columnCount), std::back_inserter(column));

    return column;

}

template <typename T>
QList<T> Matrix<T>::columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const
{

    Q_ASSERT_X((columnIndex >= 0) && (columnIndex < BaseMatrix::d_data->d_columnCount), "template <typename T> QList<T> Matrix<T>::columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const", "columnIndex is out of range");

    return this->columnAt<T>(columnIndex, rowIndexes);

}

template <typename T>
template <typename U> QList<U> Matrix<T>::columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const
{

    Q_ASSERT_X((columnIndex >= 0) && (columnIndex < BaseMatrix::d_data->d_columnCount), "template <typename T> QList<T> Matrix<T>::columnAt(qsizetype columnIndex, const QList<qsizetype> &rowIndexes) const", "columnIndex is out of range");

    return this->sliced<U>(rowIndexes, {columnIndex});

}

template <typename T>
QList<std::reference_wrapper<T>> Matrix<T>::columnAt_references(qsizetype index)
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_columnCount), "template <typename T> QList<std::reference_wrapper<T>> Matrix<T>::columnAt_references(qsizetype index)", "index is out of range");

    QList<std::reference_wrapper<T>> column;

    column.reserve(this->rowCount());

    for (T &value : boost::adaptors::stride(boost::make_iterator_range(d_data->d_values.begin() + index, d_data->d_values.end()), BaseMatrix::d_data->d_columnCount))
        column.append(std::ref(value));

    return column;

}

template <typename T>
T *Matrix<T>::data()
{

    return d_data->d_values.data();

}

template <typename T>
const T *Matrix<T>::data() const
{

    return d_data->d_values.data();

}

template <typename T>
void Matrix<T>::insertColumn(qsizetype index, const QList<T> &column)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::insertColumn(qsizetype index, const QList<T> &column)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertColumn(qsizetype index, const QList<T> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<T>(index, column);

}

template <typename T>
template <typename U>
void Matrix<T>::insertColumn(qsizetype index, const QList<U> &column)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::insertColumn(qsizetype index, const QList<U> &column)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::insertColumn(qsizetype index, const QList<U> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<U>(index, column);

}

template <typename T>
void Matrix<T>::insertColumns_1d(qsizetype index, const QList<T> &columns)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::insertColumns(qsizetype index, const QList<T> &columns)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> Matrix<T>::insertColumns_1d(const QList<T> &columns)", "count of columns does not allign with multiple of row count of matrix");

    this->insertColumns_1d<T>(index, columns);

}

template <typename T>
template <typename U> void Matrix<T>::insertColumns_1d(qsizetype index, const QList<U> &columns)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::insertColumns(qsizetype index, const QList<U> &columns)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> template <typename U> Matrix<T>::insertColumns_1d(const QList<U> &columns)", "count of columns does not allign with multiple of row count of matrix");

    if (BaseMatrix::d_data->d_rowCount == 0)
        BaseMatrix::d_data->d_rowCount = columns.count();

    if (BaseMatrix::d_data->d_rowCount == 0)
        return;

    qsizetype numberOfColumns = columns.count() / BaseMatrix::d_data->d_rowCount;

    qsizetype nRight = BaseMatrix::d_data->d_columnCount - index;

    d_data->d_values.resize(d_data->d_values.count() + columns.count());

    auto itValuesTo = std::rbegin(d_data->d_values);

    auto itValuesFrom = std::crbegin(d_data->d_values) + columns.count();

    auto itColumnsFrom = std::crbegin(columns);

    while (std::crend(columns) != itColumnsFrom) {

        itValuesTo = std::copy_n(itValuesFrom, nRight, itValuesTo);

        std::advance(itValuesFrom, nRight);

        itValuesTo = std::copy_n(itColumnsFrom, numberOfColumns, itValuesTo);

        std::advance(itColumnsFrom, numberOfColumns);

        itValuesTo = std::copy_n(itValuesFrom, index, itValuesTo);

        std::advance(itValuesFrom, index);

    }

    BaseMatrix::d_data->d_columnCount = BaseMatrix::d_data->d_columnCount + numberOfColumns;

}

template <typename T>
void Matrix<T>::insertColumns_2d(qsizetype index, const QList<QList<T>> &columns)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::insertColumns_2d(qsizetype index, const QList<QList<T>> &columns)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (columns.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertColumns_2d(qsizetype index, const QList<QList<T>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X(std::all_of(columns.begin(), columns.end(), [&columns](const QList<T> &column){return column.count() == columns.first().count();}), "template <typename T> Matrix<T>::insertColumns_2d(const QList<QList<T>> &columns)", "columns matrix is not rectangle");

    this->insertColumns_2d<T>(index, columns);

}

template <typename T>
template <typename U> void Matrix<T>::insertColumns_2d(qsizetype index, const QList<QList<U>> &columns)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::insertColumns(qsizetype index, const QList<QList<U>> &columns)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (columns.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::insertColumns(qsizetype index, const QList<QList<U>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X(std::all_of(columns.begin(), columns.end(), [&columns](const QList<U> &column){return column.count() == columns.first().count();}), "template <typename T> template <typename U> Matrix<T>::insertColumns(qsizetype index, const QList<QList<U>> &columns)", "columns matrix is not rectangle");

    if (BaseMatrix::d_data->d_rowCount == 0)
        BaseMatrix::d_data->d_rowCount = columns.count();

    qsizetype numberOfColumns = columns.first().count();

    qsizetype nRight = BaseMatrix::d_data->d_columnCount - index;

    d_data->d_values.resize(d_data->d_values.count() + numberOfColumns * BaseMatrix::d_data->d_rowCount);

    auto itValuesTo = std::rbegin(d_data->d_values);

    auto itValuesFrom = std::crbegin(d_data->d_values) + numberOfColumns * BaseMatrix::d_data->d_rowCount;

   for (qsizetype i = columns.count() - 1; i >= 0; --i) {

        itValuesTo = std::copy_n(itValuesFrom, nRight, itValuesTo);

        std::advance(itValuesFrom, nRight);

        itValuesTo = std::copy_n(std::crbegin(columns.at(i)), numberOfColumns, itValuesTo);

        itValuesTo = std::copy_n(itValuesFrom, index, itValuesTo);

        std::advance(itValuesFrom, index);

    }

    BaseMatrix::d_data->d_columnCount = BaseMatrix::d_data->d_columnCount + columns.first().count();

}

template <typename T>
void Matrix<T>::insertRow(qsizetype index, const QList<T> &row)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertRow(qsizetype index, const QList<T> &row)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::insertRow(qsizetype index, const QList<T> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<T>(index, row);

}

template <typename T>
template <typename U>
void Matrix<T>::insertRow(qsizetype index, const QList<U> &row)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::insertRow(qsizetype index, const QList<U> &row)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::insertRow(qsizetype index, const QList<U> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<U>(index, row);

}

template <typename T>
void Matrix<T>::insertRows_1d(qsizetype index, const QList<T> &rows)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertRows_1d(qsizetype index, const QList<T> &rows)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> Matrix<T>::insertRows_1d(qsizetype index, const QList<T> &rows)", "count of rows does not allign with multiple of column count of matrix");

    this->insertRows_1d<T>(index, rows);

}

template <typename T>
template <typename U> void Matrix<T>::insertRows_1d(qsizetype index, const QList<U> &rows)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertRows_1d(qsizetype index, const QList<U> &rows)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> Matrix<T>::insertRows_1d(qsizetype index, const QList<U> &rows)", "count of rows does not allign with multiple of column count of matrix");

    if (BaseMatrix::d_data->d_columnCount == 0)
        BaseMatrix::d_data->d_columnCount = rows.count();

    if (BaseMatrix::d_data->d_columnCount == 0)
        return;

    d_data->d_values.resize(d_data->d_values.count() + rows.count());

    std::move_backward(std::next(d_data->d_values.begin(), index * BaseMatrix::d_data->d_columnCount), std::prev(d_data->d_values.end(), rows.count()), d_data->d_values.end());

    std::copy(rows.begin(), rows.end(), d_data->d_values.begin() + index * BaseMatrix::d_data->d_columnCount);

    BaseMatrix::d_data->d_rowCount = BaseMatrix::d_data->d_rowCount + (rows.count() / BaseMatrix::d_data->d_columnCount);

}

template <typename T>
void Matrix<T>::insertRows_2d(qsizetype index, const QList<QList<T>> &rows)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::insertRows_2d(qsizetype index, const QList<QList<T>> &rows)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<T> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> Matrix<T>::appendRows_2d(const QList<QList<T>> &rows)", "count of one if the rows does not match currrent column count of matrix");

    this->insertRows_2d<T>(index, rows);

}

template <typename T>
template <typename U> void Matrix<T>::insertRows_2d(qsizetype index, const QList<QList<U>> &rows)
{

    Q_ASSERT_X((index >= 0) && (index <= BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::insertRows_2d(qsizetype index, const QList<QList<U>> &rows)", "index is out of range");

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<U> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> template <typename U> Matrix<T>::appendRows_2d(const QList<QList<U>> &rows)", "count of one of the rows does not match currrent column count of matrix");

    if (BaseMatrix::d_data->d_columnCount == 0)
        BaseMatrix::d_data->d_columnCount = rows.first().count();

    d_data->d_values.resize(d_data->d_values.count() + rows.count());

    auto itValuesTo = std::rbegin(d_data->d_values);

    auto itValuesFrom = std::crbegin(d_data->d_values) + rows.count();

    itValuesTo = std::copy_n(itValuesFrom, rows.count(), itValuesTo);

    for (qsizetype i = rows.count() - 1; i >= 0; --i)
        itValuesTo = std::copy_n(std::crbegin(rows.at(i)), rows.count(), itValuesTo);

    BaseMatrix::d_data->d_rowCount = BaseMatrix::d_data->d_rowCount + rows.count();

}

template <typename U> QTextStream& operator<<(QTextStream& out, const Matrix<U> &matrix)
{

    for (qsizetype i = 0; i < matrix.rowCount(); ++i) {

        const QList<U> &row(matrix.rowAt(i));

        out << row.first();

        for (qsizetype j = 1; j < row.count(); ++j)
            out << "\t" << row.at(j);

        out << Qt::endl;

    }

    return out;

}

template <typename T>
Matrix<T> &Matrix<T>::operator=(Matrix<T> &&matrix)
{

    if (this != &matrix) {

        BaseMatrix::operator=(std::move(matrix));

        d_data->d_values = std::move(matrix.d_data->d_values);

    }

    return *this;

}

template <typename T>
template <typename U> Matrix<T> &Matrix<T>::operator=(Matrix<U> &&matrix)
{

    BaseMatrix::operator=(std::move(matrix));

    d_data->d_values.reserve(matrix.d_data->d_values.count());

    while (!matrix.d_data->d_values.isEmpty())
         d_data->d_values.append(matrix.d_data->d_values.takeLast());

    std::reverse(d_data->d_values.begin(), d_data->d_values.end());

    BaseMatrix::d_data->d_typeName = typeid(T).name();

}

template <typename T>
Matrix<T> &Matrix<T>::operator=(const Matrix<T> &matrix)
{

    if (this != &matrix) {

        BaseMatrix::operator=(matrix);

        d_data.operator=(matrix.d_data);

    }

    return *this;

}

template <typename T>
template <typename U> Matrix<T> &Matrix<T>::operator=(const Matrix<U> &matrix)
{

    BaseMatrix::operator=(matrix);

    d_data->d_values.reserve(matrix.values().count());

    std::copy(matrix.values().begin(), matrix.values().end(), std::back_inserter(d_data->d_values));

    return *this;

}

template <typename T>
std::span<T> Matrix<T>::operator[](qsizetype index)
{

    Q_ASSERT_X((index >=0 ) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> std::span<T> Matrix<T>::operator[](qsizetype index)", "index is out of range");

    return std::span<T>(d_data->d_values.data() + index * BaseMatrix::d_data->d_columnCount, BaseMatrix::d_data->d_columnCount);

}

template <typename T>
void Matrix<T>::prependColumn(const QList<T> &column)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::prependColumn(const QList<T> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<T>(0, column);

}

template <typename T>
template <typename U>
void Matrix<T>::prependColumn(const QList<U> &column)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || (column.count() == BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> Matrix<T>::prependColumn(const QList<U> &column)", "count of column does not match currrent row count of matrix");

    this->insertColumns_1d<U>(0, column);

}

template <typename T>
void Matrix<T>::prependColumns_1d(const QList<T> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> Matrix<T>::prependColumns_1d(const QList<T> &columns)", "count of columns does not allign with multiple of row count of matrix");

    this->insertColumns_1d<T>(0, columns);

}

template <typename T>
template <typename U> void Matrix<T>::prependColumns_1d(const QList<U> &columns)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || ((columns.count() % BaseMatrix::d_data->d_rowCount) == 0), "template <typename T> template <typename U> Matrix<T>::prependColumns_1d(const QList<U> &columns)", "count of columns does not allign with multiple of row count of matrix");

    this->insertColumns_1d<U>(0, columns);

}

template <typename T>
void Matrix<T>::prependColumns_2d(const QList<QList<T>> &columns)
{

    Q_ASSERT_X(columns.count() == BaseMatrix::d_data->d_rowCount, "template <typename T> Matrix<T>::prependColumns_2d(const QList<QList<T>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || std::all_of(columns.begin(), columns.end(), [&columns](const QList<T> &column){return column.count() == columns.first().count();}), "template <typename T> Matrix<T>::prependColumns_2d(const QList<QList<T>> &columns)", "columns matrix is not rectangle");

    this->insertColumns_2d<T>(0, columns);

}

template <typename T>
template <typename U> void Matrix<T>::prependColumns_2d(const QList<QList<U>> &columns)
{

    Q_ASSERT_X(columns.count() == BaseMatrix::d_data->d_rowCount, "template <typename T> template <typename U> Matrix<T>::prependColumns_2d(const QList<QList<U>> &columns)", "count of columns does not match current row count of matrix");

    Q_ASSERT_X((BaseMatrix::d_data->d_rowCount == 0) || std::all_of(columns.begin(), columns.end(), [&columns](const QList<T> &column){return column.count() == columns.first().count();}), "template <typename T> template <typename U> Matrix<T>::prependColumns_2d(const QList<QList<U>> &columns)", "columns matrix is not rectangle");

    this->insertColumns_2d<U>(0, columns);

}

template <typename T>
void Matrix<T>::prependRow(const QList<T> &row)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T>::prependRow(const QList<T> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<T>(row);

}

template <typename T>
template <typename U> void Matrix<T>::prependRow(const QList<U> &row)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || (row.count() == BaseMatrix::d_data->d_columnCount), "template <typename T> template <typename U> Matrix<T>::prependRow(const QList<U> &row)", "count of row does not match currrent column count of matrix");

    this->insertRows_1d<U>(row);

}

template <typename T>
void Matrix<T>::prependRows_1d(const QList<T> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> Matrix<T>::prependRows_1d(const QList<T> &rows)", "count of rows does not allign with multiple of column count of matrix");

    this->insertRows_1d<T>(rows);

}

template <typename T>
template <typename U> void Matrix<T>::prependRows_1d(const QList<U> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || ((rows.count() % BaseMatrix::d_data->d_columnCount) == 0), "template <typename T> template <typename U> Matrix<T>::prependRows_1d(const QList<U> &rows)", "count of rows does not allign with multiple of column count of matrix");

    this->insertRows_1d<U>(rows);

}

template <typename T>
void Matrix<T>::prependRows_2d(const QList<QList<T>> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<T> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> Matrix<T>::prependRows_2d(const QList<QList<T>> &rows)", "count of one of the rows does not match currrent column count of matrix");

    this->insertRows_2d<T>(0, rows);

}

template <typename T>
template <typename U> void Matrix<T>::prependRows_2d(const QList<QList<U>> &rows)
{

    Q_ASSERT_X((BaseMatrix::d_data->d_columnCount == 0) || std::all_of(rows.begin(), rows.end(), [this](const QList<T> &row){return row.count() == BaseMatrix::d_data->d_columnCount;}), "template <typename T> template <typename U> Matrix<T>::appendRows_2d(const QList<QList<U>> &rows)", "count of one of the rows does not match currrent column count of matrix");

    this->insertRows_2d<U>(0, rows);

}

template <typename T>
void Matrix<T>::removeColumn(qsizetype index)
{

    Q_ASSERT_X((index >= 0 ) && (index < BaseMatrix::d_data->d_columnCount), "template <typename T> QList<T> Matrix<T>::removeColumn(qsizetype index) const", "index is out of range");

    this->removeColumns({index});

}

template <typename T>
void Matrix<T>::removeColumns(QList<qsizetype> indexes)
{

    Q_ASSERT_X(std::all_of(indexes.begin(), indexes.end(), [this](qsizetype index){return (index >= 0) && (index < BaseMatrix::d_data->d_columnCount);}), "template <typename T> template <typename U> Matrix<T>::removeColumns(QList<qsizetype> indexes)", "index is out of range");

    std::sort(indexes.begin(), indexes.end(), std::less<qsizetype>());

    QList<qsizetype> nSteps;

    std::adjacent_difference(indexes.begin(), indexes.end(), std::back_inserter(nSteps), [](qsizetype a, qsizetype b){return a - b - 1;});

    nSteps.append(BaseMatrix::d_data->d_columnCount - indexes.last() - 1);

    auto itValuesTo = std::begin(d_data->d_values);

    auto itValuesFrom = std::cbegin(d_data->d_values);

    for (qsizetype i = 0; i < BaseMatrix::d_data->d_rowCount; ++i) {

        for (qsizetype nStep : nSteps) {

            itValuesTo = std::copy_n(itValuesFrom, nStep, itValuesTo);

            std::advance(itValuesFrom, nStep + 1);

        }

        std::advance(itValuesFrom, -1);

    }

    d_data->d_values.resize(d_data->d_values.count() - indexes.count() * BaseMatrix::d_data->d_rowCount);

    BaseMatrix::d_data->d_columnCount = BaseMatrix::d_data->d_columnCount - indexes.count();

}

template <typename T>
void Matrix<T>::removeRow(qsizetype index)
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> QList<T> Matrix<T>::removeColumn(qsizetype index) const", "index is out of range");

    this->removeRows({index});

}

template <typename T>
void Matrix<T>::removeRows(QList<qsizetype> indexes)
{

    Q_ASSERT_X(std::all_of(indexes.begin(), indexes.end(), [this](qsizetype index){return (index >= 0) && (index < BaseMatrix::d_data->d_rowCount);}), "template <typename T> template <typename U> Matrix<T>::removeRows(QList<qsizetype> indexes)", "index is out of range");

    std::sort(indexes.begin(), indexes.end(), std::less<qsizetype>());

    QList<qsizetype> nSteps;

    std::adjacent_difference(indexes.begin(), indexes.end(), std::back_inserter(nSteps), [](qsizetype a, qsizetype b){return (a - b - 1);});

    nSteps.append((BaseMatrix::d_data->d_rowCount - indexes.last() - 1));

    auto itValuesTo = std::begin(d_data->d_values);

    auto itValuesFrom = std::cbegin(d_data->d_values);

    for (qsizetype nStep : nSteps) {

        itValuesTo = std::copy_n(itValuesFrom, nStep * BaseMatrix::d_data->d_columnCount, itValuesTo);

        std::advance(itValuesFrom, nStep * BaseMatrix::d_data->d_columnCount + BaseMatrix::d_data->d_columnCount);

    }

    d_data->d_values.resize(d_data->d_values.count() - indexes.count() * BaseMatrix::d_data->d_columnCount);

    BaseMatrix::d_data->d_rowCount = BaseMatrix::d_data->d_rowCount - indexes.count();

}

template <typename T>
QList<T> Matrix<T>::rowAt(qsizetype index) const
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> QList<T> Matrix<T>::rowAt(qsizetype index) const", "index is out of range");

    return d_data->d_values.sliced(index * BaseMatrix::d_data->d_columnCount, BaseMatrix::d_data->d_columnCount);

}

template <typename T>
template <typename U> QList<U> Matrix<T>::rowAt(qsizetype index) const
{

    Q_ASSERT_X((index >= 0 ) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> QList<U> Matrix<T>::rowAt(qsizetype index) const", "index is out of range");

    QList<U> row;

    row.reserve(BaseMatrix::d_data->d_columnCount);

    for (auto value : const_cast<Matrix<T> *>(this)->operator[](index))
        row.append(value);

    return row;

}

template <typename T>
QList<T> Matrix<T>::rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const
{

    Q_ASSERT_X((rowIndex >= 0) && (rowIndex < BaseMatrix::d_data->d_rowCount), "template <typename T> QList<T> Matrix<T>::rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const", "rowIndex is out of range");

    return this->rowAt<T>(rowIndex, columnIndexes);

}

template <typename T>
template <typename U> QList<U> Matrix<T>::rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const
{

    Q_ASSERT_X((rowIndex >= 0 ) && (rowIndex < BaseMatrix::d_data->d_rowCount), "template <typename T> template <typename U> QList<U> Matrix<T>::rowAt(qsizetype rowIndex, const QList<qsizetype> &columnIndexes) const", "rowIndex is out of range");

    return this->sliced<U>({rowIndex}, columnIndexes);

}

template <typename T>
QList<std::reference_wrapper<T>> Matrix<T>::rowAt_references(qsizetype index)
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> QList<std::reference_wrapper<T>> Matrix<T>::rowAt_references(qsizetype index)", "index is out of range");

    QList<std::reference_wrapper<T>> row;

    row.reserve(BaseMatrix::d_data->d_columnCount);

    std::span<T> rowSpan = this->operator[](index);

    for (std::size_t i = 0; i < rowSpan.count(); ++i)
        row.append(std::ref(rowSpan[i]));

    return row;

}

template <typename T>
QList<T> Matrix<T>::sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const
{

    return this->sliced<T>(rowIndexes, columnIndexes);

}

template <typename T>
template <typename U> QList<U> Matrix<T>::sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const
{

    QList<U> sliced_matrix;

    sliced_matrix.reserve(rowIndexes.count() * columnIndexes.count());

    for (qsizetype rowIndex : rowIndexes) {

        Q_ASSERT_X((rowIndex >=0) && (rowIndex < BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T> Matrix<T>::sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const", "rowIndex is out of range");

        std::span<T> rowSpan = const_cast<Matrix<T> *>(this)->operator[](rowIndex);

        for (qsizetype columnIndex : columnIndexes) {

            Q_ASSERT_X((columnIndex >= 0) && (columnIndex < BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<T> Matrix<T>::sliced(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const", "columnIndex is out of range");

            sliced_matrix.append(rowSpan[columnIndex]);

        }

    }

    return sliced_matrix;

}

template <typename T>
QList<std::reference_wrapper<T>> Matrix<T>::sliced_references(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes)
{

    QList<std::reference_wrapper<T>> sliced_matrix;

    sliced_matrix.reserve(rowIndexes.count() * columnIndexes.count());

    for (qsizetype rowIndex : rowIndexes) {

        Q_ASSERT_X((rowIndex >= 0) && (rowIndex < BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<std::reference_wrapper<T>> Matrix<T>::sliced_references(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const", "rowIndex is out of range");

        std::span<T> rowSpan = this->operator[](rowIndex);

        for (qsizetype columnIndex : columnIndexes) {

            Q_ASSERT_X((columnIndex >= 0) && (columnIndex < BaseMatrix::d_data->d_columnCount), "template <typename T> Matrix<std::reference_wrapper<T>> Matrix<T>::sliced_references(const QList<qsizetype> &rowIndexes, const QList<qsizetype> &columnIndexes) const", "columnIndex is out of range");

            sliced_matrix.append(std::ref(rowSpan[columnIndex]));

        }

    }

    return sliced_matrix;

}

template <typename T>
void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, QList<T> &&values)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &&values)", "rowCount multiplied with columnCount does not match count of values");

    BaseMatrix::d_data->d_columnCount = columnCount;

    BaseMatrix::d_data->d_rowCount = rowCount;

    d_data->d_values = std::move(values);

}

template <typename T>
void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &values)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<T> &values)", "rowCount multiplied with columnCount does not match count of values");

    BaseMatrix::d_data->d_columnCount = columnCount;

    BaseMatrix::d_data->d_rowCount = rowCount;

    d_data->d_values = values;

}

template <typename T>
template <typename U> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, QList<U> &&values)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> template <typename U> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, QList<U> &&values)", "rowCount multiplied with columnCount does not match count of values");

    d_data->d_values.reserve(values.count());

    while (!values.isEmpty())
         d_data->d_values.append(values.takeLast());

    std::reverse(d_data->d_values.begin(), d_data->d_values.end());

    BaseMatrix::d_data->d_columnCount = columnCount;

    BaseMatrix::d_data->d_rowCount = rowCount;

}

template <typename T>
template <typename U> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<U> &values)
{

    Q_ASSERT_X(rowCount * columnCount == values.count(), "template <typename T> template <typename U> void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const QList<U> &values)", "rowCount multiplied with columnCount does not match count of values");

    BaseMatrix::d_data->d_rowCount = rowCount;

    BaseMatrix::d_data->d_columnCount = columnCount;

    d_data->d_values.reserve(rowCount * columnCount);

    std::copy(values.begin(), values.end(), std::back_inserter(d_data->d_values));

}

template <typename T>
void Matrix<T>::setData(qsizetype rowCount, qsizetype columnCount, const T &value)
{

    BaseMatrix::d_data->d_rowCount = rowCount;

    BaseMatrix::d_data->d_columnCount = columnCount;

    d_data->d_values.fill(value, rowCount * columnCount);

}

template <typename T>
void Matrix<T>::transpose()
{

    MathOperations::transposeInplace(d_data->d_values, BaseMatrix::d_data->d_columnCount);

    std::swap(BaseMatrix::d_data->d_rowCount, BaseMatrix::d_data->d_columnCount);

}

template <typename T>
const QList<T> Matrix<T>::values() const
{

    return d_data->d_values;

}

template <typename T>
Matrix<T>::~Matrix()
{

}

template <typename T>
typename QList<T>::const_iterator Matrix<T>::beginRowAt(qsizetype index) const
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::beginRowAt(qsizetype index) const", "index is out of range");

    return d_data->d_values.begin() + index * BaseMatrix::d_data->d_columnCount;

}

template <typename T>
typename QList<T>::iterator Matrix<T>::beginRowAt(qsizetype index)
{

    Q_ASSERT_X((index >= 0) && (index < BaseMatrix::d_data->d_rowCount), "template <typename T> Matrix<T>::beginRowAt(qsizetype index)", "index is out of range");

    return d_data->d_values.begin() + index * BaseMatrix::d_data->d_columnCount;

}

template <typename T>
QDataStream &Matrix<T>::writeToDataStream(QDataStream &out)
{

    out << QString("_Matrix_begin#");

    out << (qint32)1;

    out << QString("d_columnCount") << BaseMatrix::d_data->d_columnCount;

    out << QString("d_rowCount") << BaseMatrix::d_data->d_rowCount;

    out << QString("d_values");

    out << quint64(d_data->d_values.count());

    for (const typename QList<T>::value_type &t : d_data->d_values)
        out << t;

    out << QString("_Matrix_end#");

    return out;

}

template <typename T>
QDataStream &Matrix<T>::readFromDataStream(QDataStream &in)
{

    qint64 pos = in.device()->pos();

    QString label;

    in >> label;

    if (label != QString("_Matrix_begin#")) {

        in.setStatus(QDataStream::ReadCorruptData);

        in.device()->seek(pos);

        return in;

    }

    qint32 version;

    in >> version;

    while (!in.atEnd()) {

        in >> label;

        if (label == QString("d_columnCount"))
            in >> BaseMatrix::d_data->d_columnCount;
        else if (label == QString("d_rowCount"))
            in >> BaseMatrix::d_data->d_rowCount;
        else if (label == QString("d_values")) {

            quint64 n;

            in >> n;

            d_data->d_values.reserve(n);

            for (quint64 i = 0; i < n; ++i) {

                typename QList<T>::value_type t;

                in >> t;

                if (in.status() != QDataStream::Ok) {

                    d_data->d_values.clear();

                    in.setStatus(QDataStream::ReadCorruptData);

                    in.device()->seek(pos);

                    return in;

                }

                d_data->d_values.append(t);
            }

        } else if (label == QString("_Matrix_end#"))
            return in;
        else {

            in.setStatus(QDataStream::ReadCorruptData);

            in.device()->seek(pos);

            return in;

        }

    }

    return in;

}

#endif // MATRIX_H
