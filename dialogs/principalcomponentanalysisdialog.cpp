#include "principalcomponentanalysisdialog.h"

PrincipalComponentAnalysisDialog::PrincipalComponentAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "principal component analysis", "principalcomponentanalysisdialog")
{

    PrincipalComponentAnalysisParameters *principalComponentAnalysisParameters = new PrincipalComponentAnalysisParameters;

    principalComponentAnalysisParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<PrincipalComponentAnalysisParameters>(principalComponentAnalysisParameters);

    BaseDialog::initWidgets();

}

void PrincipalComponentAnalysisDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select variables");

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes; this->updateMaximumNumberOfComponents();});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes; this->updateMaximumNumberOfComponents();});


    DoubleSpinBoxWidget *doubleSpinBoxWidget_thresholdCumulativeExplainedVariance = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_thresholdCumulativeExplainedVariance");

    this->connect(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_cumulativeExplainedVariance = value;});

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->label()->setText("threshold maximum cumulative explained variance");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setValue(100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance, 2, 0, 1, 1);


    d_spinBoxWidget_thresholdMaximumNumberOfComponents = new SpinBoxWidget(this, "d_spinBoxWidget_thresholdMaximumNumberOfComponents");

    this->connect(d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_maximumNumberOfComponents = value;});

    d_spinBoxWidget_thresholdMaximumNumberOfComponents->label()->setText("threshold maximum number of components");

    d_gridLayout->addWidget(d_spinBoxWidget_thresholdMaximumNumberOfComponents, 2, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_mode = new ComboBoxWidget(this, "comboBoxWidget_mode");

    this->connect(comboBoxWidget_mode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_mode](){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->mode = comboBoxWidget_mode->comboBox()->currentText();});

    comboBoxWidget_mode->label()->setText("mode");

    comboBoxWidget_mode->comboBox()->addItems({"covariance", "correlation"});

    comboBoxWidget_mode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_mode, 3, 0, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 4, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 5, 0, 1, 2);


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"covariance or correlation matrix" ,  "eigenvalues", "eigenvectors", "whitening matrix", "dewhitening/factorloadings matrix", "principal component scores"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 6, 0, 1, 2);


    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &PrincipalComponentAnalysisDialog::orientationChanged);

    this->orientationChanged(d_orientationWidget->orientation());

}

void PrincipalComponentAnalysisDialog::orientationChanged(Qt::Orientation orientation)
{

    PrincipalComponentAnalysisParameters *principalComponentAnalysisParameters = static_cast<PrincipalComponentAnalysisParameters *>(BaseDialog::d_parameters.data());

    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), principalComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->baseAnnotatedMatrix->identifiers(orientation, true), principalComponentAnalysisParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), principalComponentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(principalComponentAnalysisParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), principalComponentAnalysisParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));

}

void PrincipalComponentAnalysisDialog::updateMaximumNumberOfComponents()
{

    qsizetype numberOfVariablesSelected = d_itemSelectorWidget_variables->itemSelectorModel_selected()->itemIndexes().count();

    qsizetype numberOfItemsSelected = d_itemSelectorWidget_items->itemSelectorModel_selected()->itemIndexes().count();

    qsizetype maxNumberOfComponents = (numberOfVariablesSelected < numberOfItemsSelected) ? numberOfVariablesSelected : numberOfItemsSelected;


    d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setRange(1, maxNumberOfComponents);

    d_spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setValue(maxNumberOfComponents);

}
