#ifndef LOGMONITORMODEL_H
#define LOGMONITORMODEL_H

#include <QAbstractTableModel>
#include <QColor>
#include <QModelIndex>
#include <QApplication>
#include <QSortFilterProxyModel>
#include <QSet>
#include <QString>

#include "logmonitor.h"

class LogMonitorSortFilterProxyModel : public QSortFilterProxyModel
{

public:

    LogMonitorSortFilterProxyModel(QObject *parent = 0);

protected:

    bool filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const;

};

class LogMonitorModel : public QAbstractTableModel
{

    Q_OBJECT

public:

    explicit LogMonitorModel(QObject *parent = nullptr, const QSharedPointer<LogMonitor> &logMonitor = nullptr);

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    bool show(qsizetype index) const;

public slots:

    void clear();

    void reportError(const QUuid &uuidWorker, const QString &error);

    void reportMessage(const QUuid &uuidWorker, const QString &message);

    void reportWarning(const QUuid &uuidWorker, const QString &warning);

    void setShowError(bool show);

    void setShowMessage(bool show);

    void setShowWarning(bool show);

    void setUuidWorkerToShow(const QUuid &uuidWorker);

    void setCharacterLimit(qsizetype limit);

private:

    QSharedPointer<LogMonitor> d_logMonitor;

    bool d_showError;

    bool d_showMessage;

    bool d_showWarning;

    qsizetype d_characterLimit;

    QUuid d_uuidWorkerToShow;

    QSet<QUuid> d_setOfUuidWorker;

    void addUuidWorkerToSet(const QUuid &uuidWorker);

signals:

    void uniqueUuidWorkerAdded(const QUuid &uuidWorker);

};

#endif // LOGMONITORMODEL_H
