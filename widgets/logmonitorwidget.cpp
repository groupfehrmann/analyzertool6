#include "logmonitorwidget.h"
#include "ui_logmonitorwidget.h"

LogMonitorWidget::LogMonitorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogMonitorWidget)
{

    ui->setupUi(this);

    QSettings settings;

    settings.beginGroup("logMonitorWidget");

    if (settings.contains("checkBox_showErrors/checked"))
        ui->checkBox_showErrors->setChecked(settings.value("checkBox_showErrors/checked").toBool());

    if (settings.contains("checkBox_showMessages/checked"))
        ui->checkBox_showMessages->setChecked(settings.value("checkBox_showMessages/checked").toBool());

    if (settings.contains("checkBox_showWarnings/checked"))
        ui->checkBox_showWarnings->setChecked(settings.value("checkBox_showWarnings/checked").toBool());

    ui->comboBox_uuidWorker->addItem("show all", QUuid());

}

void LogMonitorWidget::exportLogMonitorToFile()
{

    QFileDialog fileDialog(nullptr, QCoreApplication::applicationName() + " - select file");

    fileDialog.setAcceptMode(QFileDialog::AcceptSave);

    fileDialog.setDefaultSuffix("txt");

    QSettings settings;

    settings.beginGroup("fileDialog");

    if (settings.contains("state"))
        fileDialog.restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        fileDialog.restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        fileDialog.setDirectory(QDir(settings.value("lastDirectory").toString()));

    settings.endGroup();

    if (!fileDialog.exec())
        return;

    settings.beginGroup("fileDialog");

    settings.setValue("state", fileDialog.saveState());

    settings.setValue("geometry", fileDialog.saveGeometry());

    settings.setValue("lastDirectory", fileDialog.directory().path());

    settings.endGroup();

    QFile file(fileDialog.selectedFiles().first());

    if (fileDialog.selectedFiles().first().isEmpty() || !file.open(QIODevice::WriteOnly | QIODevice::Text)) {

        QMessageBox::critical(nullptr, QCoreApplication::applicationName(), "could not open file \"" + file.fileName() + "\"");

        return;

    }

    QTextStream out(&file);

    LogMonitorSortFilterProxyModel *model = static_cast<LogMonitorSortFilterProxyModel *>(ui->tableView->model());

    static_cast<LogMonitorModel *>(model->sourceModel())->setCharacterLimit(-1);

    out << model->headerData(0, Qt::Horizontal).toString();

    for (qsizetype i = 1; i < model->columnCount(); ++i)
        out << "\t" << model->headerData(i, Qt::Horizontal).toString();

    out << Qt::endl;

    for (qsizetype i = 0; i < model->rowCount(); ++i) {

        out << model->data(model->index(i, 0)).toString();

        for (qsizetype j = 1; j < model->columnCount(); ++j)
            out << "\t" << model->data(model->index(i, j)).toString();

        out << Qt::endl;

    }

    static_cast<LogMonitorModel *>(model->sourceModel())->setCharacterLimit(1000);

    file.close();

}

void LogMonitorWidget::setLogMonitorModel(LogMonitorModel *logMonitorModel)
{

    logMonitorModel->setShowError(ui->checkBox_showErrors->isChecked());

    logMonitorModel->setShowMessage(ui->checkBox_showMessages->isChecked());

    logMonitorModel->setShowWarning(ui->checkBox_showWarnings->isChecked());


    LogMonitorSortFilterProxyModel *logSortFilterProxyModel = new LogMonitorSortFilterProxyModel(ui->tableView);

    logSortFilterProxyModel->setSourceModel(logMonitorModel);

    ui->tableView->setModel(logSortFilterProxyModel);


    this->connect(ui->checkBox_showErrors, &QCheckBox::toggled, logMonitorModel, &LogMonitorModel::setShowError);

    this->connect(ui->checkBox_showMessages, &QCheckBox::toggled, logMonitorModel, &LogMonitorModel::setShowMessage);

    this->connect(ui->checkBox_showWarnings, &QCheckBox::toggled, logMonitorModel, &LogMonitorModel::setShowWarning);

    this->connect(ui->pushButton_clear, &QPushButton::clicked, logMonitorModel, &LogMonitorModel::clear);

    this->connect(ui->pushButton_clear, &QPushButton::clicked, [this](){this->ui->comboBox_uuidWorker->clear(); this->ui->comboBox_uuidWorker->addItem("show all", QUuid());});

    this->connect(ui->comboBox_uuidWorker, &QComboBox::currentIndexChanged, [this, logMonitorModel](int index){logMonitorModel->setUuidWorkerToShow(ui->comboBox_uuidWorker->itemData(index).toUuid());});

    this->connect(logMonitorModel, &LogMonitorModel::uniqueUuidWorkerAdded, [this](const QUuid &uuidWorker){this->ui->comboBox_uuidWorker->addItem(uuidWorker.toString(), uuidWorker);});

    this->connect(ui->pushButton_export, &QPushButton::clicked, this, &LogMonitorWidget::exportLogMonitorToFile);


    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);

    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);

    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);

    ui->tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);

    ui->tableView->horizontalHeader()->resizeSection(0, 300);

    ui->tableView->horizontalHeader()->resizeSection(1, 65);

    ui->tableView->horizontalHeader()->resizeSection(2, 160);

}

LogMonitorWidget::~LogMonitorWidget()
{

    QSettings settings;

    settings.beginGroup("logMonitorWidget");

    settings.setValue("checkBox_showErrors/checked", ui->checkBox_showErrors->isChecked());

    settings.setValue("checkBox_showMessages/checked", ui->checkBox_showMessages->isChecked());

    settings.setValue("checkBox_showWarnings/checked", ui->checkBox_showWarnings->isChecked());

    settings.endGroup();

    delete ui;

}
