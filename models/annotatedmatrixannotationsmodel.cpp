#include "annotatedmatrixannotationsmodel.h"

AnnotatedMatrixAnnotationsSortFilterProxyModel::AnnotatedMatrixAnnotationsSortFilterProxyModel(QObject *parent, Qt::Orientation orientation) :
    QSortFilterProxyModel(parent),
    d_orientation(orientation),
    d_showSelectedOnly(false)
{

}

bool AnnotatedMatrixAnnotationsSortFilterProxyModel::filterAcceptsColumn(int sourceColumn, QModelIndex const &sourceParent) const
{
    Q_UNUSED(sourceParent);

    if (!d_showSelectedOnly)
        return true;

    return (static_cast<Qt::CheckState>(this->sourceModel()->headerData(sourceColumn, Qt::Horizontal, Qt::CheckStateRole).toInt()) == Qt::Checked);

}

bool AnnotatedMatrixAnnotationsSortFilterProxyModel::filterAcceptsRow(int sourceRow, QModelIndex const &sourceParent) const
{

    Q_UNUSED(sourceParent);

    if (!d_showSelectedOnly)
        return true;

//    return (static_cast<Qt::CheckState>(this->sourceModel()->headerData(sourceRow, d_orientation, Qt::CheckStateRole).toInt()) == Qt::Checked);

    return (static_cast<Qt::CheckState>(this->sourceModel()->headerData(sourceRow, Qt::Vertical, Qt::CheckStateRole).toInt()) == Qt::Checked);

}

void AnnotatedMatrixAnnotationsSortFilterProxyModel::setShowSelectedOnly(bool showSelectedOnly)
{

    d_showSelectedOnly = showSelectedOnly;

    this->invalidateFilter();

}

bool AnnotatedMatrixAnnotationsSortFilterProxyModel::showSelectedOnly()
{

    return d_showSelectedOnly;

}

AnnotatedMatrixAnnotationsModel::AnnotatedMatrixAnnotationsModel(QObject *parent, QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix, Qt::Orientation orientation) :
    QAbstractTableModel(parent),
    d_baseAnnotatedMatrix(baseAnnotatedMatrix),
    d_orientation(orientation)
{

    if (orientation == Qt::Horizontal) {

        this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::resetColumnAnnotationsAboutToBegin, this, &AnnotatedMatrixAnnotationsModel::beginResetModel);

        this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::columnAnnotationsIsReset, this, &AnnotatedMatrixAnnotationsModel::endResetModel);

    } else {

        this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::resetRowAnnotationsAboutToBegin, this, &AnnotatedMatrixAnnotationsModel::beginResetModel);

        this->connect(d_baseAnnotatedMatrix.data(), &BaseAnnotatedMatrix::rowAnnotationsIsReset, this, &AnnotatedMatrixAnnotationsModel::endResetModel);

    }

}

const BaseAnnotatedMatrix *AnnotatedMatrixAnnotationsModel::baseAnnotatedMatrix() const
{

    return d_baseAnnotatedMatrix.data();

}

int AnnotatedMatrixAnnotationsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return d_baseAnnotatedMatrix->annotationLabelCount(d_orientation);

}

QVariant AnnotatedMatrixAnnotationsModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return d_baseAnnotatedMatrix->annotationValue(index.row(), index.column(), d_orientation);

        case Qt::EditRole:
            return d_baseAnnotatedMatrix->annotationValue(index.row(), index.column(), d_orientation);

        case Qt::ForegroundRole:
            return (d_baseAnnotatedMatrix->identifierSelectionStatus(index.row(), d_orientation) && (d_baseAnnotatedMatrix->annotationLabelSelectionStatus(index.column(), d_orientation))) ? QColor(Qt::black) : QColor(Qt::gray);

        case Qt::StatusTipRole:
            return "row index " + QString::number(index.row()) + ", column index " + QString::number(index.column())  + " - \"" + d_baseAnnotatedMatrix->identifier(index.row(), d_orientation) + "\", \"" + d_baseAnnotatedMatrix->annotationLabel(index.column(), d_orientation) + "\"";

        default:
            return QVariant();

    }

    return QVariant();

}

Qt::ItemFlags AnnotatedMatrixAnnotationsModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;

}

QVariant AnnotatedMatrixAnnotationsModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if ((section < 0) || section >= ((orientation == Qt::Horizontal) ? d_baseAnnotatedMatrix->annotationLabelCount(d_orientation) : d_baseAnnotatedMatrix->identifierCount(d_orientation)))
        return QVariant();

    switch (role) {

        case Qt::DisplayRole:
            return (orientation == Qt::Horizontal) ? d_baseAnnotatedMatrix->annotationLabel(section, d_orientation) : d_baseAnnotatedMatrix->identifier(section, d_orientation);

        case Qt::EditRole:
            return (orientation == Qt::Horizontal) ? d_baseAnnotatedMatrix->annotationLabel(section, d_orientation) : d_baseAnnotatedMatrix->identifier(section, d_orientation);

        case Qt::ForegroundRole:
            return (orientation == Qt::Horizontal) ? (d_baseAnnotatedMatrix->annotationLabelSelectionStatus(section, d_orientation) ? QColor(Qt::black) : QColor(Qt::white)) : (d_baseAnnotatedMatrix->identifierSelectionStatus(section, d_orientation) ? QColor(Qt::black) : QColor(Qt::white));

        case Qt::BackgroundRole:
        return (orientation == Qt::Horizontal) ? (d_baseAnnotatedMatrix->annotationLabelSelectionStatus(section, d_orientation) ? QVariant() : QColor(128, 0, 0)) : (d_baseAnnotatedMatrix->identifierSelectionStatus(section, d_orientation) ? QVariant() : QColor(128, 0, 0));

        case Qt::StatusTipRole:
            return (orientation == Qt::Horizontal) ? "column index " + QString::number(section) : "row index " + QString::number(section);

        case Qt::CheckStateRole:
            return (orientation == Qt::Horizontal) ? (d_baseAnnotatedMatrix->annotationLabelSelectionStatus(section, d_orientation) ? Qt::Checked : Qt::Unchecked) : (d_baseAnnotatedMatrix->identifierSelectionStatus(section, d_orientation) ? Qt::Checked : Qt::Unchecked);

        default:
            return QVariant();

    }

}

bool AnnotatedMatrixAnnotationsModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);

    d_baseAnnotatedMatrix->insertAnnotationLabels(column, count, d_orientation);

    endInsertColumns();

    return true;

}

bool AnnotatedMatrixAnnotationsModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    d_baseAnnotatedMatrix->insertLists(row, count, d_orientation);

    endInsertRows();

    return true;

}

bool AnnotatedMatrixAnnotationsModel::removeColumns(int column, int count, const QModelIndex &parent)
{

    beginRemoveColumns(parent, column, column + count - 1);

    QList<qsizetype> indexes(count);

    std::iota(std::begin(indexes), std::end(indexes), column);

    d_baseAnnotatedMatrix->removeAnnotationLabels(indexes, d_orientation);

    endRemoveColumns();

    return true;

}

bool AnnotatedMatrixAnnotationsModel::removeRows(int row, int count, const QModelIndex &parent)
{

    beginRemoveRows(parent, row, row + count - 1);

    QList<qsizetype> indexes(count);

    std::iota(std::begin(indexes), std::end(indexes), row);

    d_baseAnnotatedMatrix->removeLists(indexes, d_orientation);

    endRemoveRows();

    return true;

}

int AnnotatedMatrixAnnotationsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return d_baseAnnotatedMatrix->identifierCount(d_orientation);

}

bool AnnotatedMatrixAnnotationsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    if (!d_baseAnnotatedMatrix->tryLockForWrite())
        return false;

    if (data(index, role) != value) {

        switch (role) {

            case Qt::EditRole:
                d_baseAnnotatedMatrix->setAnnotationValue(index.row(), index.column(), value, d_orientation);
            break;

            default: {

                d_baseAnnotatedMatrix->unlock();

                return false;
            }

        }

        emit dataChanged(index, index, QVector<int>() << role);

        d_baseAnnotatedMatrix->unlock();

        return true;

    }

    d_baseAnnotatedMatrix->unlock();

    return false;

}

bool AnnotatedMatrixAnnotationsModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{

    if (value != headerData(section, orientation, role)) {

        switch (role) {

            case Qt::EditRole:

                if (orientation == Qt::Horizontal)
                    d_baseAnnotatedMatrix->setAnnotationLabel(section, value.toString(), d_orientation);
                else
                    d_baseAnnotatedMatrix->setIdentifier(section, value.toString(), d_orientation);

            break;

            case Qt::CheckStateRole:

                if (orientation == Qt::Vertical)
                    d_baseAnnotatedMatrix->setIdentifierSelectionStatus(section, (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked) ? true : false, d_orientation);
                else
                    d_baseAnnotatedMatrix->setAnnotationLabelSelectionStatus(section, (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked) ? true : false, d_orientation);

            break;

            default:
                return false;

        }

        emit headerDataChanged(orientation, section, section);

        return true;

    }

    return false;

}
