#include "selectmultiplefileswidget.h"
#include "ui_selectmultiplefileswidget.h"

SelectMultipleFilesWidget::SelectMultipleFilesWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier, QFileDialog::AcceptMode acceptMode) :
    QWidget(parent),
    ui(new Ui::SelectMultipleFilesWidget),
    d_acceptMode(acceptMode),
    d_fileDialog(new QFileDialog(this, QCoreApplication::applicationName() + " - select files")),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{
    ui->setupUi(this);

    d_fileDialog->setFileMode(QFileDialog::ExistingFiles);

}

QFileDialog *SelectMultipleFilesWidget::fileDialog()
{

    return d_fileDialog;

}

QLabel *SelectMultipleFilesWidget::label()
{

    return ui->label;

}

void SelectMultipleFilesWidget::on_pushButton_browse_clicked()
{

    d_fileDialog->setAcceptMode(d_acceptMode);

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

    if (settings.contains("state"))
        d_fileDialog->restoreState(settings.value("state").toByteArray());

    if (settings.contains("geometry"))
        d_fileDialog->restoreGeometry(settings.value("geometry").toByteArray());

    if (settings.contains("lastDirectory"))
        d_fileDialog->setDirectory(QDir(settings.value("lastDirectory").toString()));

    if (settings.contains("selectedNameFilter")) {

        d_fileDialog->selectNameFilter(settings.value("selectedNameFilter").toString());

        emit filterSelected(d_fileDialog->selectedNameFilter());

    }

    settings.endGroup();

    if (d_fileDialog->exec()) {

        for (const QString &path : d_fileDialog->selectedFiles())
            ui->listWidget_paths->addItem(path);

        QList<QString> paths;

        for (int i = 0; i < ui->listWidget_paths->count(); ++i)
            paths.append(ui->listWidget_paths->item(i)->text());

        emit pathsChanged(paths);

        QSettings settings;

        settings.beginGroup(d_widgetSettingsIdentifier + "/fileDialog");

        settings.setValue("state", d_fileDialog->saveState());

        settings.setValue("geometry", d_fileDialog->saveGeometry());

        settings.setValue("lastDirectory", d_fileDialog->directory().path());

        settings.setValue("selectedNameFilter", d_fileDialog->selectedNameFilter());

        settings.endGroup();

    }

}

void SelectMultipleFilesWidget::on_pushButton_clear_clicked()
{

    ui->listWidget_paths->clear();

    QList<QString> paths;

    emit pathsChanged(paths);

}

void SelectMultipleFilesWidget::on_pushButton_remove_clicked()
{

    qDeleteAll(ui->listWidget_paths->selectedItems());

    QList<QString> paths;

    for (int i = 0; i < ui->listWidget_paths->count(); ++i)
        paths.append(ui->listWidget_paths->item(i)->text());

    emit pathsChanged(paths);

}

void SelectMultipleFilesWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/listWidget_paths");

    if (settings.contains("items")) {

        QList<QString> paths = settings.value("items").toStringList();

        ui->listWidget_paths->addItems(paths);

        for (int i = 0; i < ui->listWidget_paths->count(); ++i) {

           if (!QFile::exists(ui->listWidget_paths->item(i)->text()))
               ui->listWidget_paths->item(i)->setForeground(Qt::red);

        }

        emit pathsChanged(paths);

    }

    settings.endGroup();

    d_readWidgetSettings = true;

}

void SelectMultipleFilesWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/listWidget_paths");

    QStringList paths;

    for (int i = 0; i < ui->listWidget_paths->count(); ++i)
        paths.append(ui->listWidget_paths->item(i)->text());

    settings.setValue("items", paths);

    settings.endGroup();

}

SelectMultipleFilesWidget::~SelectMultipleFilesWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
