#ifndef SHUFFLEIDENTIFIERSDIALOG_H
#define SHUFFLEIDENTIFIERSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "workerclasses/shuffleidentifiersworker.h"

class ShuffleIdentifiersDialog : public BaseDialog
{

public:

    ShuffleIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};

#endif // SHUFFLEIDENTIFIERSDIALOG_H
