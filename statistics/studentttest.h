#ifndef STUDENTTTEST_H
#define STUDENTTTEST_H

#include <QList>

#include "boost/math/distributions/students_t.hpp"
#include <boost/math/statistics/univariate_statistics.hpp>

#include "basestatisticaltest.h"

class StudentTTest : public BaseStatisticalTest
{

public:

    StudentTTest();

    StudentTTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers);

    virtual ~StudentTTest() {}

private:

    double d_degreeOfFreedom;

    double d_n1;

    double d_n2;

    std::pair<double, double> d_meanAndVarianceOfSample1;

    std::pair<double, double> d_meanAndVarianceOfSample2;

    double d_meanDifference;

    double d_pooledVariance;

    double d_standardErrorOfMeanDifference;

    double d_hedgeCorrectionFactor;

    double d_hedgeM;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // STUDENTTTEST_H
