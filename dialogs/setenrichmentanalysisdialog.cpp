#include "setenrichmentanalysisdialog.h"

SetEnrichmentAnalysisDialog::SetEnrichmentAnalysisDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent) :
    BaseDialog(parent, "set enrichment analysis", "setenrichmentanalysisdialog")
{

    SetEnrichmentAnalysisParameters *setEnrichmentAnalysisParameters = new SetEnrichmentAnalysisParameters;

    setEnrichmentAnalysisParameters->baseAnnotatedMatrix = baseAnnotationMatrix;

    BaseDialog::d_parameters = QSharedPointer<SetEnrichmentAnalysisParameters>(setEnrichmentAnalysisParameters);

    BaseDialog::initWidgets();

    this->connect(this, &SetEnrichmentAnalysisDialog::accepted, this, &SetEnrichmentAnalysisDialog::dialogAccepted);

}

void SetEnrichmentAnalysisDialog::initWidgets_()
{

    d_orientationWidget = new OrientationWidget(this, "orientationWidget");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, [this](Qt::Orientation orientation){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->orientation = orientation;});

    d_orientationWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_orientationWidget, 0, 0, 1, 2);


    d_itemSelectorWidget_variables = new ItemSelectorWidget(this, "itemSelectorWidget_variables");

    d_itemSelectorWidget_variables->label()->setText("select lists");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SetEnrichmentAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_variables, 1, 0, 1, 1);

    this->connect(d_itemSelectorWidget_variables->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIdentifiers = itemLabels; static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedVariableIndexes = itemIndexes;});


    d_itemSelectorWidget_items = new ItemSelectorWidget(this, "itemSelectorWidget_items");

    d_itemSelectorWidget_items->label()->setText("select items");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SetEnrichmentAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_items, 1, 1, 1, 1);

    this->connect(d_itemSelectorWidget_items->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIdentifiers = itemLabels; static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedItemIndexes = itemIndexes;});


    SelectMultipleFilesWidget *selectMultipleFilesWidget = new SelectMultipleFilesWidget(this, "selectMultipleFilesWidget");

    selectMultipleFilesWidget->label()->setText("files containing definitions of sets");

    this->connect(selectMultipleFilesWidget, &SelectMultipleFilesWidget::pathsChanged, [this](const QList<QString> &paths){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->pathToSetCollections = paths;});

    selectMultipleFilesWidget->readWidgetSettings();

    d_gridLayout->addWidget(selectMultipleFilesWidget, 2, 0, 1, 2);


    d_comboBoxWidget_labelForMatching = new ComboBoxWidget(this, "d_comboBoxWidget_labelForMatching");

    this->connect(d_comboBoxWidget_labelForMatching->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->labelForMatching = text;});

    this->connect(d_comboBoxWidget_labelForMatching->comboBox(), &QComboBox::currentIndexChanged, [this](int index){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->indexForMatching = (index == 0) ? -1 : index - 1;});

    d_comboBoxWidget_labelForMatching->label()->setText("label for matching between items of lists and members of sets");

    d_gridLayout->addWidget(d_comboBoxWidget_labelForMatching, 3, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_testToDetermineEnrichment = new ComboBoxWidget(this, "comboBoxWidget_testToDetermineEnrichment");

    this->connect(comboBoxWidget_testToDetermineEnrichment->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->testToDetermineEnrichment = text;});

    comboBoxWidget_testToDetermineEnrichment->comboBox()->addItems({"Student T", "Welch T", "Mann-Whitney U", "Kolmogorov-Smirnov Z", "Fisher exact"});

    comboBoxWidget_testToDetermineEnrichment->comboBox()->setCurrentIndex(0);

    comboBoxWidget_testToDetermineEnrichment->label()->setText("test to determine enrichement");

    comboBoxWidget_testToDetermineEnrichment->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_testToDetermineEnrichment, 3, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_minimumSizeOfSet = new SpinBoxWidget(this, "spinBoxWidget_minimumSizeOfSet");

    this->connect(spinBoxWidget_minimumSizeOfSet->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->minimumSizeOfSet = value;});

    spinBoxWidget_minimumSizeOfSet->label()->setText("minimum size of set");

    spinBoxWidget_minimumSizeOfSet->spinBox()->setRange(1, 100000);

    spinBoxWidget_minimumSizeOfSet->spinBox()->setValue(10);

    spinBoxWidget_minimumSizeOfSet->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_minimumSizeOfSet->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_minimumSizeOfSet, 4, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_maximumSizeOfSet = new SpinBoxWidget(this, "spinBoxWidget_maximumSizeOfSet");

    this->connect(spinBoxWidget_maximumSizeOfSet->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->maximumSizeOfSet = value;});

    spinBoxWidget_maximumSizeOfSet->label()->setText("maximum size of set");

    spinBoxWidget_maximumSizeOfSet->spinBox()->setRange(1, 100000);

    spinBoxWidget_maximumSizeOfSet->spinBox()->setValue(500);

    spinBoxWidget_maximumSizeOfSet->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_maximumSizeOfSet->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_maximumSizeOfSet, 4, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_permutationTestMode = new ComboBoxWidget(this, "comboBoxWidget_permutationTestMode");

    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [this](const QString &text) {static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->enablePermutationMode = (text == "enable") ? true : false;});

    comboBoxWidget_permutationTestMode->comboBox()->addItems({"enable", "disable"});

    comboBoxWidget_permutationTestMode->label()->setText("permutation test mode");

    comboBoxWidget_permutationTestMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_permutationTestMode, 5, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfPermutations = new SpinBoxWidget(this, "spinBoxWidget_numberOfPermutations");

    this->connect(spinBoxWidget_numberOfPermutations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfPermutations = value;});

    spinBoxWidget_numberOfPermutations->label()->setText("number of permutations");

    spinBoxWidget_numberOfPermutations->spinBox()->setRange(100, 10000000);

    spinBoxWidget_numberOfPermutations->spinBox()->setValue(1000);

    spinBoxWidget_numberOfPermutations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfPermutations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfPermutations, 5, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_falseDiscoveryRate = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_falseDiscoveryRate");

    this->connect(doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->falseDiscoveryRate = value / 100.0;});

    doubleSpinBoxWidget_falseDiscoveryRate->label()->setText("false discovery rate to control for in permutation mode");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setValue(5);

    doubleSpinBoxWidget_falseDiscoveryRate->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_falseDiscoveryRate->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_falseDiscoveryRate, 6, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_confidenceLevel = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_confidenceLevel");

    this->connect(doubleSpinBoxWidget_confidenceLevel->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->confidenceLevel = value / 100.0;});

    doubleSpinBoxWidget_confidenceLevel->label()->setText("confidence level for multivariate threshold in permutation mode");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setValue(80);

    doubleSpinBoxWidget_confidenceLevel->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_confidenceLevel->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_confidenceLevel, 6, 1, 1, 1);


    this->connect(comboBoxWidget_permutationTestMode->comboBox(), &QComboBox::currentTextChanged, [spinBoxWidget_numberOfPermutations, doubleSpinBoxWidget_confidenceLevel, doubleSpinBoxWidget_falseDiscoveryRate](const QString &text) {

        if (text == "enable") {

            spinBoxWidget_numberOfPermutations->setEnabled(true);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(true);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(true);

        } else {

            spinBoxWidget_numberOfPermutations->setEnabled(false);

            doubleSpinBoxWidget_confidenceLevel->setEnabled(false);

            doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(false);

        };
    });

    spinBoxWidget_numberOfPermutations->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_confidenceLevel->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");

    doubleSpinBoxWidget_falseDiscoveryRate->setEnabled(comboBoxWidget_permutationTestMode->comboBox()->currentText() == "enable");


    d_itemSelectorWidget_annotationLabelsForItems = new ItemSelectorWidget(this, "d_itemSelectorWidget_annotationLabelsForItems");

    d_itemSelectorWidget_annotationLabelsForItems->label()->setText("select annotation labels for items in lists");

    this->connect(d_orientationWidget, &OrientationWidget::orientationChanged, this, &SetEnrichmentAnalysisDialog::orientationChanged);

    d_gridLayout->addWidget(d_itemSelectorWidget_annotationLabelsForItems, 7, 0, 1, 1);

    this->connect(d_itemSelectorWidget_annotationLabelsForItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->annotationLabelsForItems = itemLabels;});


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"enrichment matrix per set collection", "enrichment summary"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 7, 1, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (OPTIONAL)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 8, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 8, 1, 1, 1);


    this->orientationChanged(d_orientationWidget->orientation());

}

void SetEnrichmentAnalysisDialog::orientationChanged(Qt::Orientation orientation)
{

    SetEnrichmentAnalysisParameters *setEnrichmentAnalysisParameters = static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data());


    d_itemSelectorWidget_variables->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(setEnrichmentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(orientation), setEnrichmentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(orientation));

    d_itemSelectorWidget_variables->itemSelectorModel_selected()->setItemLabelsAndIndexes(setEnrichmentAnalysisParameters->baseAnnotatedMatrix->identifiers(orientation, true), setEnrichmentAnalysisParameters->baseAnnotatedMatrix->indexes(orientation, true));


    d_itemSelectorWidget_items->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(setEnrichmentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifiers(BaseAnnotatedMatrix::switchOrientation(orientation)), setEnrichmentAnalysisParameters->baseAnnotatedMatrix->nonSelectedIdentifierIndexes(BaseAnnotatedMatrix::switchOrientation(orientation)));

    d_itemSelectorWidget_items->itemSelectorModel_selected()->setItemLabelsAndIndexes(setEnrichmentAnalysisParameters->baseAnnotatedMatrix->identifiers(BaseAnnotatedMatrix::switchOrientation(orientation), true), setEnrichmentAnalysisParameters->baseAnnotatedMatrix->indexes(BaseAnnotatedMatrix::switchOrientation(orientation), true));


    QList<QString> itemLabels;

    itemLabels.append((orientation == Qt::Vertical) ? "COLUMN IDENTIFIER" : "ROW IDENTIFIER");

    for (const QString &identifier : setEnrichmentAnalysisParameters->baseAnnotatedMatrix->annotationLabels(BaseAnnotatedMatrix::switchOrientation(orientation), false))
        itemLabels.append(identifier);

    d_comboBoxWidget_labelForMatching->comboBox()->clear();

    d_comboBoxWidget_labelForMatching->comboBox()->addItems(itemLabels);


    d_itemSelectorWidget_annotationLabelsForItems->itemSelectorModel_notSelected()->setItemLabelsAndIndexes(itemLabels);

    d_itemSelectorWidget_annotationLabelsForItems->itemSelectorModel_selected()->clear();

}

void SetEnrichmentAnalysisDialog::dialogAccepted()
{

    if (static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->testToDetermineEnrichment == "Fisher exact") {

        double threshold = 3.0;

        QSettings settings;

        settings.beginGroup(d_dialogSettingsIdentifier);

        if (settings.contains("thresholdFisherExactTest"))
           threshold = settings.value("thresholdFisherExactTest").toDouble();

        settings.endGroup();

        threshold = QInputDialog::getDouble(this, QCoreApplication::applicationName() + " - input dialog", "select threshold for Fisher Exact test", threshold, 0.0);

        static_cast<SetEnrichmentAnalysisParameters *>(BaseDialog::d_parameters.data())->thresholdForFisherExact = threshold;

        settings.beginGroup(d_dialogSettingsIdentifier);

        settings.setValue("thresholdFisherExactTest", threshold);

        settings.endGroup();

    }

}
