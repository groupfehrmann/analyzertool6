#ifndef KOLMOGOROVSMIRNOVTEST_H
#define KOLMOGOROVSMIRNOVTEST_H

#include <QList>

#include "boost/math/distributions/kolmogorov_smirnov.hpp"

#include "basestatisticaltest.h"

class KolmogorovSmirnovTest : public BaseStatisticalTest
{

public:

    KolmogorovSmirnovTest();

    KolmogorovSmirnovTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers);

    virtual ~KolmogorovSmirnovTest() {}

private:

    QList<QList<double>> d_samples;

    double d_d;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};


#endif // KOLMOGOROVSMIRNOVTEST_H
