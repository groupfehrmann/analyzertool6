#include "calculateconsensusforindependentcomponentsworker.h"


CalculateConsensusForIndependentComponentsParameters::CalculateConsensusForIndependentComponentsParameters(QObject *parent) :
    BaseParameters(parent, "calculateconsensusforindependentcomponents")
{

}

bool CalculateConsensusForIndependentComponentsParameters::isValid_()
{

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    if (selectedResultItems.isEmpty()) {

        d_lastError = "no result items selected";

        return false;

    }

    return true;

}

QString CalculateConsensusForIndependentComponentsParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[pearson correlation threshold for clustering independent components from multiple runs = " << pearsonCorrelationThresholdForConsensus << "]";

    stream << Qt::endl << "\t[metric to determine independence = \"" << metricToDetermineIndependence << "\"]";

    stream << Qt::endl << "\t[distance or XI correlation threshold for clustering independent components from multiple runs = " << distanceCorrelationThresholdForConsensus << "]";

    stream << Qt::endl << "\t[threshold for credibility index = " << threshold_credibilityIndex << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    stream << Qt::endl << "\t[selected result items = [" << selectedResultItems.first() << "\"]";

    for (qsizetype i = 1; i < selectedResultItems.count(); ++i)
        stream << ", [" << selectedResultItems.at(i) << "\"]";

    stream << "]";

    return prettyPrintString;

}

CalculateConsensusForIndependentComponentsWorker::CalculateConsensusForIndependentComponentsWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("calculate consensus for independent component"), parameters, "calculateconsensusforindependentcomponents")
{

}

void CalculateConsensusForIndependentComponentsWorker::doWork_()
{

    QSharedPointer<CalculateConsensusForIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<CalculateConsensusForIndependentComponentsParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);


    // Fetching original data
    ////////////////

    QList<QString> identifiersOfVariablesUsedToCalculateIndependentComponents;

    QList<QString> identifiersOfItemsUsedToCalculateIndependentComponents;

    QList<double> originalData;

    this->readFile(parameters->pathToOriginalData, identifiersOfItemsUsedToCalculateIndependentComponents, identifiersOfVariablesUsedToCalculateIndependentComponents, originalData, parameters->rMode_originalData);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (parameters->orientationOfVariablesInOriginalData == Qt::Horizontal)
         MathOperations::transposeInplace(originalData, identifiersOfVariablesUsedToCalculateIndependentComponents.count(), parameters->numberOfThreadsToUse);
    else
        std::swap(identifiersOfVariablesUsedToCalculateIndependentComponents, identifiersOfItemsUsedToCalculateIndependentComponents);

    qsizetype numberOfItemsInIndependentComponent = identifiersOfItemsUsedToCalculateIndependentComponents.count();

    qsizetype numberOfVariablesUsedToCalculateIndependentComponents = identifiersOfVariablesUsedToCalculateIndependentComponents.count();


    // Fetching independent components
    ////////////////

    QList<QString> pathsToIndependentComponentFiles = this->scanDirectoryForIndependentComponentfiles(parameters->directoryContainingIndependentComponents);

    qsizetype numberOfIndependentComponentFiles = pathsToIndependentComponentFiles.count();

    qsizetype numberOfIndependentComponentsPerFile = 0;

    QList<double> independentComponentsForAllRuns;

    QUuid uuidProgress = this->startProgress("consensus step : importing independent components", 0, pathsToIndependentComponentFiles.count());

    QList<QString> rowIdentifiersOfFirstFile;

    QList<QString> columnIdentifiersOfFirstFile;

    for (qsizetype i = 0; i < pathsToIndependentComponentFiles.count(); ++i) {

        QList<QString> rowIdentifiers;

        QList<QString> columnIdentifiers;

        QList<double> values;

        this->readFile(pathsToIndependentComponentFiles.at(i), rowIdentifiers, columnIdentifiers, values, parameters->rMode_independentComponents);

        if (i == 0) {

            rowIdentifiersOfFirstFile = rowIdentifiers;

            columnIdentifiersOfFirstFile = columnIdentifiers;

            if (parameters->orientationOfIndependentComponentsInFile == Qt::Horizontal)
                numberOfIndependentComponentsPerFile = columnIdentifiers.count();
            else
                numberOfIndependentComponentsPerFile = rowIdentifiers.count();

        } else {

            if (rowIdentifiersOfFirstFile != rowIdentifiers) {

                this->reportError("row identifiers of file \"" + pathsToIndependentComponentFiles.at(i) + "\" does not match with file \"" + pathsToIndependentComponentFiles.at(0) + "\"");

                this->requestInterruption();

                return;

            }

            if (columnIdentifiersOfFirstFile != columnIdentifiers) {

                this->reportError("column identifiers of file \"" + pathsToIndependentComponentFiles.at(i) + "\" does not match with file \"" + pathsToIndependentComponentFiles.at(0) + "\"");

                this->requestInterruption();

                return;

            }

        }

        if (parameters->orientationOfIndependentComponentsInFile == Qt::Horizontal) {

            MathOperations::transposeInplace(values, columnIdentifiers.count(), parameters->numberOfThreadsToUse);

        }

        independentComponentsForAllRuns.append(values);

        this->updateProgress(uuidProgress, i + 1);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    this->stopProgress(uuidProgress);


    // Creating correlation matrix
    ////////////////

    QMutex mutex;

    QList<std::pair<qsizetype, std::span<double>>> independentComponentsForAllRuns_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(independentComponentsForAllRuns, numberOfItemsInIndependentComponent);


    QList<double> independentComponentsForAllRuns_means;

    independentComponentsForAllRuns_means.reserve(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

    QList<double> independentComponentsForAllRuns_l2Norms;

    independentComponentsForAllRuns_l2Norms.reserve(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

    for (const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan : independentComponentsForAllRuns_rowIndexes_rowSpans) {

        double mean = boost::math::statistics::mean(independentComponentsForAllRuns_rowIndex_rowSpan.second);

        independentComponentsForAllRuns_means << mean;

        for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
            value = value - mean;

        double l2Norm = boost::math::tools::l2_norm(independentComponentsForAllRuns_rowIndex_rowSpan.second.begin(), independentComponentsForAllRuns_rowIndex_rowSpan.second.end());

        independentComponentsForAllRuns_l2Norms << l2Norm;

        for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
            value = value / l2Norm;

    }

    QList<QList<qsizetype>> setsOflinkedIndexes(independentComponentsForAllRuns_rowIndexes_rowSpans.count());

    for (QList<qsizetype> &linkedIndexes : setsOflinkedIndexes)
        linkedIndexes.reserve(numberOfIndependentComponentFiles);

    uuidProgress = this->startProgress("consensus step : calculating pearson distance matrix", 0, (independentComponentsForAllRuns_rowIndexes_rowSpans.count() * independentComponentsForAllRuns_rowIndexes_rowSpans.count() - independentComponentsForAllRuns_rowIndexes_rowSpans.count()) / 2.0);

    qsizetype progress = 0;

    auto mapFunctor1 = [&](const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan) {

        for (const std::pair<qsizetype, std::span<double>> &temp : std::ranges::views::drop(independentComponentsForAllRuns_rowIndexes_rowSpans, independentComponentsForAllRuns_rowIndex_rowSpan.first + 1)) {

            double r_coeff = 0.0;

            for (unsigned long j = 0; j < temp.second.size(); ++j)
                r_coeff += independentComponentsForAllRuns_rowIndex_rowSpan.second[j] * temp.second[j];

            if (std::abs(r_coeff) >= parameters->pearsonCorrelationThresholdForConsensus) {

                mutex.lock();

                setsOflinkedIndexes[independentComponentsForAllRuns_rowIndex_rowSpan.first].append(temp.first);

                setsOflinkedIndexes[temp.first].append(independentComponentsForAllRuns_rowIndex_rowSpan.first);

                mutex.unlock();

            }

        }

        mutex.lock();

        setsOflinkedIndexes[independentComponentsForAllRuns_rowIndex_rowSpan.first].append(independentComponentsForAllRuns_rowIndex_rowSpan.first);

        progress += independentComponentsForAllRuns_rowIndexes_rowSpans.count() - (independentComponentsForAllRuns_rowIndex_rowSpan.first + 1);

        mutex.unlock();

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, progress);

    };

    this->startProgress("consensus step : calculating pearson distance matrix", QtConcurrent::map(&d_threadPool, independentComponentsForAllRuns_rowIndexes_rowSpans, mapFunctor1), false);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->progressStopped(uuidProgress);


    for (const std::pair<qsizetype, std::span<double>> &independentComponentsForAllRuns_rowIndex_rowSpan : independentComponentsForAllRuns_rowIndexes_rowSpans) {

        double l2Norm = independentComponentsForAllRuns_l2Norms.at(independentComponentsForAllRuns_rowIndex_rowSpan.first);

        for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
            value = value * l2Norm;

        double mean = independentComponentsForAllRuns_means.at(independentComponentsForAllRuns_rowIndex_rowSpan.first);

        for (double &value : independentComponentsForAllRuns_rowIndex_rowSpan.second)
            value = value + mean;

    }


    QSet<qsizetype> availableIndexes;

    for (qsizetype i = 0; i< independentComponentsForAllRuns_rowIndexes_rowSpans.count(); ++i)
        availableIndexes.insert(i);


    QList<QList<qsizetype>> consensusClusterToMemberIndexes;

    uuidProgress = this->startProgress("consensus step : creating consensus clusters", 0, setsOflinkedIndexes.count());

    while (!setsOflinkedIndexes.isEmpty()) {

        std::sort(setsOflinkedIndexes.begin(), setsOflinkedIndexes.end(), [](const QList<qsizetype> &set1, const QList<qsizetype> &set2){return set1.count() < set2.count();});

        QList<qsizetype> memberIndexes;

        for (const qsizetype &index : setsOflinkedIndexes.last()) {

            availableIndexes.remove(index);

            memberIndexes.append(index);

        }

        if (!memberIndexes.isEmpty())
            consensusClusterToMemberIndexes.append(memberIndexes);

        for (qsizetype i = setsOflinkedIndexes.count() - 2; i >= 0; --i) {

            QMutableListIterator<qsizetype> it(setsOflinkedIndexes[i]);

            while (it.hasNext()) {

                qsizetype index = it.next();

                if (!availableIndexes.contains(index))
                    it.remove();

            }

        }

        setsOflinkedIndexes.removeLast();

        this->updateProgressWithOne(uuidProgress);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

    }

    this->progressStopped(uuidProgress);

    if (consensusClusterToMemberIndexes.isEmpty()) {

        this->reportMessage("no clusters were detected after clustering pearson distance matrix");

        this->requestInterruption();

        return;

    } else
        this->reportMessage("number of cluster detected after clustering pearson distance matrix : " + QString::number(consensusClusterToMemberIndexes.count()));

    this->stopProgress(uuidProgress);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    uuidProgress = this->startProgress("consensus step : filtering consensus components based on credibility index", 0, 0);

    qsizetype consensus_counter = 0;

    qsizetype index_counter = 0;

    QList<double> independentComponentsForAllRuns_afterFilteringOnCredibilityIndex;

    for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

        double credibilityIndex = static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(numberOfIndependentComponentFiles);

        if ((consensusClusterToMemberIndexes.at(i).count() > 1) && (credibilityIndex > parameters->threshold_credibilityIndex)) {

            for (qsizetype &index : consensusClusterToMemberIndexes[i]) {

                const QPair<qsizetype, std::span<double>> &index_span(independentComponentsForAllRuns_rowIndexes_rowSpans.at(index));

                independentComponentsForAllRuns_afterFilteringOnCredibilityIndex.append(QList<double>(index_span.second.begin(), index_span.second.end()));

                index = index_counter;

                ++index_counter;

            }

            ++consensus_counter;

        } else
            break;

    }

    consensusClusterToMemberIndexes.resize(consensus_counter);

    this->reportMessage("number of cluster remaining after filtering based on credibility index : " + QString::number(consensus_counter));

    this->stopProgress(uuidProgress);


    if (consensus_counter == 0) {

        this->reportError("no consensus clusters remaining after filtering based on credibility index");

        this->requestInterruption();

        return;

    }

    independentComponentsForAllRuns_rowIndexes_rowSpans.clear();

    independentComponentsForAllRuns.clear();


    QList<std::pair<qsizetype, std::span<double>>> independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex, numberOfItemsInIndependentComponent);


    QList<double> pearsonCorrelationMatrix = this->pearsonCorrelationSymmetricalMatrix(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex, numberOfItemsInIndependentComponent);

    MathOperations::mirrorSymmetrixMatrix(pearsonCorrelationMatrix, independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count());

    QList<std::span<double>> pearsonCorrelationMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(pearsonCorrelationMatrix,  independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count());


    auto mapFunctor2 = [&](const QList<qsizetype> &memberIndexes) {

        QSet<qsizetype> set(memberIndexes.begin(), memberIndexes.constEnd());

        QList<double> meanAbsPearsonCorrelationWithAllOthersOutsideCluster;

        meanAbsPearsonCorrelationWithAllOthersOutsideCluster.reserve(memberIndexes.count());

        for (qsizetype index : memberIndexes) {

            const std::span<double> &span(pearsonCorrelationMatrix_rowSpans.at(index));

            double meanAbsPearsonCorrelation = 0.0;

            for (unsigned long i = 0; i < span.size(); ++i) {

                if (!set.contains(i))
                    meanAbsPearsonCorrelation += std::abs(span[i]);

            }

            meanAbsPearsonCorrelationWithAllOthersOutsideCluster.append(meanAbsPearsonCorrelation / static_cast<double>(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.count() - memberIndexes.count()));

        }

        qsizetype indexOfMin = cblas_idamin(meanAbsPearsonCorrelationWithAllOthersOutsideCluster.count(), meanAbsPearsonCorrelationWithAllOthersOutsideCluster.data(), 1);

        const std::span<double> &spanOfWinner(independentComponentsForAllRuns_afterFilteringOnCredibilityIndex_rowIndexes_rowSpans.at(memberIndexes.at(indexOfMin)).second);

        if (memberIndexes.count() == 1)
            return std::pair<double, QList<double>>(std::numeric_limits<double>::quiet_NaN(), QList<double>(spanOfWinner.begin(), spanOfWinner.end()));
        else
            return std::pair<double, QList<double>>(meanAbsPearsonCorrelationWithAllOthersOutsideCluster.at(indexOfMin), QList<double>(spanOfWinner.begin(), spanOfWinner.end()));

    };


    auto reduceFunctor = [](QList<std::pair<double, QList<double>>> &result, const std::pair<double, QList<double>> &intermediateResult) {

        result.append(intermediateResult);

    };

    QFuture<QList<std::pair<double, QList<double>>>> future = QtConcurrent::mappedReduced(&d_threadPool, consensusClusterToMemberIndexes, std::function<std::pair<double, QList<double>>(const QList<qsizetype> &memberIndexes)>(mapFunctor2), std::function<void( QList<std::pair<double, QList<double>>> &, const std::pair<double, QList<double>> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->startProgress("consensus step : get winner per clusters", future);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();



    QList<double> consensusIndependentComponents;

    for (auto &item : future.result())
        consensusIndependentComponents.append(item.second);

    if (parameters->metricToDetermineIndependence == "XICOR")
        uuidProgress = this->startProgress("consensus step : filtering consensus components based on XICOR correlation", 0, 0);
    else
        uuidProgress = this->startProgress("consensus step : filtering consensus components based on distance correlation", 0, 0);

    QSet<qsizetype> indexPool;

    for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i)
        indexPool.insert(i);

    QList<double> distanceCorrelationMatrix;

    if (parameters->metricToDetermineIndependence == "XICOR")
        distanceCorrelationMatrix = this->xiCorrelationSymmetricalMatrix(consensusIndependentComponents, numberOfItemsInIndependentComponent);
    else
        distanceCorrelationMatrix = this->distanceCorrelationSymmetricalMatrix(consensusIndependentComponents, numberOfItemsInIndependentComponent);

    distanceCorrelationMatrix.detach();

    for (qsizetype i = 0; i < distanceCorrelationMatrix.count();) {

        distanceCorrelationMatrix[i] = 0.0;

        i += consensusClusterToMemberIndexes.count() + static_cast<qsizetype>(1);

    }

    QList<std::span<double>> rowSpansOfDistanceCorrelationMatrix = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(distanceCorrelationMatrix, consensusClusterToMemberIndexes.count());


    consensusIndependentComponents.clear();

    QList<QString> componentLabels;


    QList<double> robustnessMetricsConsensusIndependentComponents;

    consensus_counter = 0;

    for (qsizetype i = 0; i < consensusClusterToMemberIndexes.count(); ++i) {

        if (indexPool.contains(i)) {

            if (i >= numberOfIndependentComponentsPerFile)
                break;

            robustnessMetricsConsensusIndependentComponents.append(static_cast<double>(consensusClusterToMemberIndexes.at(i).count()) / static_cast<double>(numberOfIndependentComponentFiles));

            robustnessMetricsConsensusIndependentComponents.append(consensusClusterToMemberIndexes.at(i).count());

            robustnessMetricsConsensusIndependentComponents.append(future.result().at(i).first);

            consensusIndependentComponents.append(future.result().at(i).second);

            componentLabels.append("consensus independent component " + QString::number(consensus_counter + 1));

            ++consensus_counter;

        }

        QMutableSetIterator<qsizetype> it(indexPool);

        while (it.hasNext()) {

            if (rowSpansOfDistanceCorrelationMatrix.at(i)[it.next()] >= parameters->distanceCorrelationThresholdForConsensus)
                it.remove();

        }

    }

    if (parameters->metricToDetermineIndependence == "XICOR")
        this->reportMessage("number of cluster remaining after filtering based on XICOR : " + QString::number(componentLabels.count()));
    else
        this->reportMessage("number of cluster remaining after filtering based on distance correlation : " + QString::number(componentLabels.count()));

    if (componentLabels.count() == 0) {

        if (parameters->metricToDetermineIndependence == "XICOR")
            this->reportError("no consensus clusters remaining after filtering based on XICOR");
        else
            this->reportError("no consensus clusters remaining after filtering based on distance correlation");

        this->requestInterruption();

        return;

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    if (parameters->selectedResultItems.contains("ICA: robustness metrics independent components (consensus)")) {

        AnnotatedMatrix<double> *result_robustnessMetricsConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, {"credibility index", "number of members in cluster", "average pearson correlation of medoid to other cluster members"}, robustnessMetricsConsensusIndependentComponents);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(QUuid(), new Result("ICA: robustness metrics independent components (consensus)", result_robustnessMetricsConsensusIndependentComponents));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_robustnessMetricsConsensusIndependentComponents);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_robustness_metrics_independent_components_consensus");

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    if (parameters->selectedResultItems.contains("ICA: XICOR between independent components (consensus)")) {

        AnnotatedMatrix<double> *result_dCorrelationConsensusIndependentComponents;

        if (parameters->metricToDetermineIndependence == "XICOR")
            result_dCorrelationConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->xiCorrelationSymmetricalMatrix(consensusIndependentComponents, numberOfItemsInIndependentComponent));
        else
            result_dCorrelationConsensusIndependentComponents = new AnnotatedMatrix<double>(componentLabels, componentLabels, this->distanceCorrelationSymmetricalMatrix(consensusIndependentComponents, numberOfItemsInIndependentComponent));

        if (parameters->exportDirectory.isEmpty()) {

            if (parameters->metricToDetermineIndependence == "XICOR")
                emit objectAvailable(QUuid(), new Result("ICA: XICOR between independent components (consensus)", result_dCorrelationConsensusIndependentComponents));
            else
                emit objectAvailable(QUuid(), new Result("ICA: distance correlation between independent components (consensus)", result_dCorrelationConsensusIndependentComponents));

        } else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_dCorrelationConsensusIndependentComponents);

            if (parameters->metricToDetermineIndependence == "XICOR")
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_XICOR_between_independent_components_consensus");
            else
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_distance_correlation_between_independent_components_consensus");

        }

    }

    QList<double> flippedConsensusMixingMatrix;

    QList<double> flippedConsensusIndependentComponents;

    if (parameters->selectedResultItems.contains("ICA: mixing matrix (consensus)") || parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)")) {

        uuidProgress = this->startProgress("consensus step : calculate consensus mixing matrix", 0, 0);

        int info;

        QList<double> pseudoInverseConcensusIndependentComponents = MathOperations::pseudoInverse(consensusIndependentComponents, numberOfItemsInIndependentComponent, parameters->numberOfThreadsToUse, &info);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        if (info < 0)
            this->reportError("pseudo inverse -> DSYEVR: " + QString::number(info) + "th parameter had illegal value");
        else if (info > 0)
            this->reportError("pseudo inverse -> DSYEVR: Error code -> " + QString::number(info));

        QList<double> consensusMixingMatrix(numberOfVariablesUsedToCalculateIndependentComponents * componentLabels.count(), 0.0);

        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, numberOfVariablesUsedToCalculateIndependentComponents, componentLabels.count(), numberOfIndependentComponentsPerFile, 1.0, originalData.data(), numberOfItemsInIndependentComponent, pseudoInverseConcensusIndependentComponents.data(), numberOfItemsInIndependentComponent, 0.0, consensusMixingMatrix.data(), componentLabels.count());

        this->stopProgress(uuidProgress);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();


        if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)"))
            flippedConsensusMixingMatrix = consensusMixingMatrix;

        AnnotatedMatrix<double> *result_consensusMixingMatrix = new AnnotatedMatrix<double>(identifiersOfVariablesUsedToCalculateIndependentComponents, componentLabels, consensusMixingMatrix);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(QUuid(), new Result("ICA: mixing matrix (consensus)", result_consensusMixingMatrix));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_consensusMixingMatrix);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_mixing_matrix_consensus");

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();


    if (parameters->selectedResultItems.contains("ICA: independent components (consensus)")) {

        uuidProgress = this->startProgress("consensus step : transposing consensus independent components", 0, 0);

        if (parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)"))
            flippedConsensusIndependentComponents = consensusIndependentComponents;

        MathOperations::transposeInplace(consensusIndependentComponents, numberOfItemsInIndependentComponent, parameters->numberOfThreadsToUse);

        this->stopProgress(uuidProgress);

        AnnotatedMatrix<double> *result_consensusIndependentComponents = new AnnotatedMatrix<double>(identifiersOfItemsUsedToCalculateIndependentComponents, componentLabels, consensusIndependentComponents);

        if (parameters->exportDirectory.isEmpty())
            emit objectAvailable(QUuid(), new Result("ICA: independent components (consensus)", result_consensusIndependentComponents));
        else {

            QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_consensusIndependentComponents);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_independent_components_consensus");

        }

    }

    if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)") || parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)")) {

        uuidProgress = this->startProgress("consensus step : transposing consensus mixing matrix", 0, 0);

        MathOperations::transposeInplace(flippedConsensusMixingMatrix, componentLabels.count(), parameters->numberOfThreadsToUse);

        QList<std::span<double>> flippedConsensusMixingMatrix_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(flippedConsensusMixingMatrix, numberOfVariablesUsedToCalculateIndependentComponents);

        QList<std::span<double>> flippedConsensusIndependentComponents_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowSpans(flippedConsensusIndependentComponents, numberOfItemsInIndependentComponent);

        for (qsizetype i = 0; i < componentLabels.count(); ++i) {

            std::span<double> &flippedConsensusIndependentComponents_rowSpan_i(flippedConsensusIndependentComponents_rowSpans[i]);

            std::span<double> &flippedConsensusMixingMatrix_rowSpan_i(flippedConsensusMixingMatrix_rowSpans[i]);

            if (boost::math::statistics::skewness(flippedConsensusIndependentComponents_rowSpan_i) < 0.0) {

                std::transform(flippedConsensusIndependentComponents_rowSpan_i.begin(), flippedConsensusIndependentComponents_rowSpan_i.end(), flippedConsensusIndependentComponents_rowSpan_i.begin(), std::negate<double>());

                std::transform(flippedConsensusMixingMatrix_rowSpan_i.begin(), flippedConsensusMixingMatrix_rowSpan_i.end(), flippedConsensusMixingMatrix_rowSpan_i.begin(), std::negate<double>());

            }

        }

        MathOperations::transposeInplace(flippedConsensusMixingMatrix, numberOfVariablesUsedToCalculateIndependentComponents, parameters->numberOfThreadsToUse);

        MathOperations::transposeInplace(flippedConsensusIndependentComponents, numberOfItemsInIndependentComponent, parameters->numberOfThreadsToUse);

        if (parameters->selectedResultItems.contains("ICA: flipped mixing matrix (consensus)")) {

            AnnotatedMatrix<double> *result_flippedConsensusMixingMatrix = new AnnotatedMatrix<double>(identifiersOfVariablesUsedToCalculateIndependentComponents, componentLabels, flippedConsensusMixingMatrix);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(QUuid(), new Result("ICA: flipped mixing matrix (consensus)", result_flippedConsensusMixingMatrix));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_flippedConsensusMixingMatrix);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_flipped_mixing_matrix_consensus");

            }

        }

        if (parameters->selectedResultItems.contains("ICA: flipped independent components (consensus)")) {

            AnnotatedMatrix<double> *result_flippedConsensusIndependentComponents = new AnnotatedMatrix<double>(identifiersOfItemsUsedToCalculateIndependentComponents, componentLabels, flippedConsensusIndependentComponents);

            if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(QUuid(), new Result("ICA: flipped independent components (consensus)", result_flippedConsensusIndependentComponents));
            else {

                QSharedPointer<AnnotatedMatrix<double>> sharedPointerToAnnotatedMatrix(result_flippedConsensusIndependentComponents);

                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "ica_flipped_independent_components_consensus");

            }

        }

    }

}

bool CalculateConsensusForIndependentComponentsWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<CalculateConsensusForIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<CalculateConsensusForIndependentComponentsParameters>());

    return true;

}

void CalculateConsensusForIndependentComponentsWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<CalculateConsensusForIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<CalculateConsensusForIndependentComponentsParameters>());

}

QList<double> CalculateConsensusForIndependentComponentsWorker::xiCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension)
{

    QList<double> copyOfVectors = vectors;

    copyOfVectors.detach();

    QSharedPointer<CalculateConsensusForIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<CalculateConsensusForIndependentComponentsParameters>());

    this->startProgress("ranking vectors", QtConcurrent::map(&d_threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(copyOfVectors, leadingDimension), [](std::span<double> &span){ MathOperations::rankInplace(span);}));

    if (QThread::currentThread()->isInterruptionRequested())
        return QList<double>();

    this->checkIfPauseWasRequested();

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(copyOfVectors, leadingDimension);

    qsizetype nRows = copyOfVectors.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("calculating XICOR", 0, static_cast<qsizetype>(nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    auto mapFunctor = [this, &uuidProgress, &nRows, &dispatcher](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> xiCorrelations(nRows, 0.0);

        this->updateProgressWithOne(uuidProgress);

        for (qsizetype i = index_span.first; i < nRows; ++i) {

            double xiCorrelation_1 = MathDescriptives::xiCorrelation_preRanked(index_span.second, dispatcher.at(i).second);

            double xiCorrelation_2 = MathDescriptives::xiCorrelation_preRanked(dispatcher.at(i).second, index_span.second);

            xiCorrelations[i] = std::max(xiCorrelation_1, xiCorrelation_2);

            if (this->thread()->isInterruptionRequested())
                return xiCorrelations;

            this->checkIfPauseWasRequested();

            this->updateProgressWithOne(uuidProgress);

        }

        return xiCorrelations;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QList<double> mat = QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    this->stopProgress(uuidProgress);

    return mat;

}

void CalculateConsensusForIndependentComponentsWorker::readFile(const QString &pathToFile, QList<QString> &rowIdentifiers, QList<QString> &columnIdentifiers, QList<double> &values, bool rMode)
{

    qsizetype lineNumberOfHeader = 1;

    qsizetype lineNumberOfFirstDataLine = 2;

    qsizetype indexOfColumnDefiningRowIdentifiers = 0;

    qsizetype indexOfColumnDefiningFirstDataColumn = 1;

    qsizetype indexOfColumnDefiningSecondDataColumn = 2;

    qsizetype lineNumberOfLastDataLine = -1;

    bool firstTokenInHeaderDefinesColumnWithRowIdentifiers = rMode;

    QString delimiter = QString("\\t");


    QFile fileIn(pathToFile);

    if (!fileIn.exists() || !fileIn.open(QIODevice::ReadOnly | QIODevice::Text)) {

        this->reportError("could not open file \"" + pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    QUuid uuidProgress = this->startProgress("importing file \"" + pathToFile + "\"", 0, fileIn.size());

    qsizetype numberOfLinesRead = 0;

    while (!fileIn.atEnd() && (numberOfLinesRead < (lineNumberOfHeader - 1))) {

        fileIn.readLine();

        ++numberOfLinesRead;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    QList<QString> tokens;

    qsizetype numberOfTokensInReferenceLine = 0;

    if (lineNumberOfHeader != -1) {

        if (!firstTokenInHeaderDefinesColumnWithRowIdentifiers)
            tokens << QString();

        if (!fileIn.atEnd()) {

            tokens << QString(fileIn.readLine()).remove(QRegularExpression("[\r\n]")).split(QRegularExpression(delimiter), Qt::KeepEmptyParts);

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

        if (numberOfLinesRead != (lineNumberOfHeader)) {

            this->reportError("line number of header (" + QString::number(lineNumberOfHeader) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + pathToFile + "\"");

            QThread::currentThread()->requestInterruption();

            return;

        }

        numberOfTokensInReferenceLine = tokens.count();

    }

    qsizetype deltaIndex = (indexOfColumnDefiningSecondDataColumn != -1) ? indexOfColumnDefiningSecondDataColumn - indexOfColumnDefiningFirstDataColumn : tokens.count();

    columnIdentifiers.clear();

    if (lineNumberOfHeader != -1) {

        for (qsizetype i = indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex)
            columnIdentifiers.append(tokens.at(i));

    }

    if (lineNumberOfHeader != -1) {

        while (!fileIn.atEnd() && (numberOfLinesRead < lineNumberOfFirstDataLine - lineNumberOfHeader)) {

            fileIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    } else {

        while (!fileIn.atEnd() && (numberOfLinesRead < lineNumberOfFirstDataLine - 1)) {

            fileIn.readLine();

            ++numberOfLinesRead;

            this->checkIfPauseWasRequested();

            this->updateProgress(uuidProgress, fileIn.pos());

        }

    }

    values.clear();

    if (lineNumberOfLastDataLine != -1)
        values.reserve(columnIdentifiers.count() * (lineNumberOfLastDataLine - lineNumberOfFirstDataLine + 1));

    rowIdentifiers.clear();

    bool setNumberOfTokensInReferenceLine = (lineNumberOfHeader == -1) ? true : false;

    while (!fileIn.atEnd() && ((lineNumberOfLastDataLine == -1) || (numberOfLinesRead != lineNumberOfLastDataLine))) {

        tokens = QString(fileIn.readLine()).remove(QRegularExpression("[\r\n]")).split(QRegularExpression(delimiter), Qt::KeepEmptyParts);

        if (setNumberOfTokensInReferenceLine) {

            for (qsizetype i = indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex)
                columnIdentifiers.append("column_" + QString::number(columnIdentifiers.count()));

            numberOfTokensInReferenceLine = tokens.count();

            setNumberOfTokensInReferenceLine = false;

        }

        ++numberOfLinesRead;

        if (tokens.count() != numberOfTokensInReferenceLine) {

            if (lineNumberOfHeader != -1)
                this->reportError("the number of tokens read (" + QString::number(tokens.count()) + ") at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the header line");
            else
                this->reportError("the number of tokens read (" + QString::number(tokens.count()) + ") at line " + QString::number(numberOfLinesRead) + " is unequal to the " + QString::number(numberOfTokensInReferenceLine) + " tokens detected in the first data line");

            QThread::currentThread()->requestInterruption();

            return;

        }

        if (indexOfColumnDefiningRowIdentifiers != -1)
            rowIdentifiers.append(tokens.at(indexOfColumnDefiningRowIdentifiers));
        else
            rowIdentifiers.append("row_" + QString::number(rowIdentifiers.count()));

        double value;

        for (qsizetype i = indexOfColumnDefiningFirstDataColumn; i < tokens.count(); i += deltaIndex) {

            if (!ConversionFunctions::convert(tokens.at(i), value))
                this->reportWarning("could not convert \"" + tokens.at(i) + "\" at line " + QString::number(numberOfLinesRead) + " to type \"double\"");

            values.append(value);

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, fileIn.pos());

    }

    if ((lineNumberOfLastDataLine != -1) && (lineNumberOfLastDataLine != numberOfLinesRead)) {

        this->reportError("line number of last data line (" + QString::number(lineNumberOfLastDataLine) + ") exceeds the number of lines (" + QString::number(numberOfLinesRead) + ") in \"" + pathToFile + "\"");

        QThread::currentThread()->requestInterruption();

        return;

    }

    this->reportMessage("number or rows imported from \"" + pathToFile + "\" : " + QString::number(rowIdentifiers.count()));

    this->reportMessage("number or columns imported from \"" + pathToFile + "\" : " + QString::number(columnIdentifiers.count()));

    this->stopProgress(uuidProgress);

}

QList<QString> CalculateConsensusForIndependentComponentsWorker::scanDirectoryForIndependentComponentfiles(const QString &directory)
{

    this->reportMessage("scanning directory \"" + directory + "\" for independent component files");

    QDir dir(directory);

    QFileInfoList fileInfoList = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDir::LocaleAware);

    QStringList pathsToFiles;

    for (qsizetype i = 0; i < fileInfoList.count(); ++i) {

        if (fileInfoList.at(i).isDir())
            pathsToFiles << this->scanDirectoryForIndependentComponentfiles(fileInfoList.at(i).absoluteFilePath());
        else
            pathsToFiles.append(fileInfoList.at(i).absoluteFilePath());

    }

    return pathsToFiles;

}

QList<double> CalculateConsensusForIndependentComponentsWorker::distanceCorrelationSymmetricalMatrix(const QList<double> &vectors, qsizetype leadingDimension)
{


    QSharedPointer<CalculateConsensusForIndependentComponentsParameters> parameters(d_data->d_parameters.dynamicCast<CalculateConsensusForIndependentComponentsParameters>());

    FastDistanceCorrrelationSymmetricMatrix fastDistanceCorrrelationSymmetricMatrix(vectors, leadingDimension, 1.0, parameters->numberOfThreadsToUse);

    fastDistanceCorrrelationSymmetricMatrix.connectBaseWorker(this);

    fastDistanceCorrrelationSymmetricMatrix.run();

    if (QThread::currentThread()->isInterruptionRequested())
        QList<double>();;

    this->checkIfPauseWasRequested();


    return fastDistanceCorrrelationSymmetricMatrix.symmetricMatrix();

}

QList<double> CalculateConsensusForIndependentComponentsWorker::pearsonCorrelationSymmetricalMatrix(const QList<double> &matrix, qsizetype leadingDimension)
{

    QList<double> pearsonCorrelationMatrix;

    pearsonCorrelationMatrix.reserve(matrix.count());

    QList<std::pair<qsizetype, std::span<double>>> dispatcher = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(*const_cast<QList<double> *>(&matrix), leadingDimension);

    qsizetype nRows = matrix.count() / leadingDimension;

    QUuid uuidProgress = this->startProgress("creating Pearson correlation matrix", 0, static_cast<qsizetype>( nRows * (nRows + static_cast<qsizetype>(1)) / static_cast<qsizetype>(2)));

    qsizetype progress = 0.0;

    QMutex mutex;

    auto mapFunctor = [this, &uuidProgress, &dispatcher, &progress, &mutex](const std::pair<qsizetype, std::span<double>> &index_span) {

        QList<double> counts(dispatcher.count(), 0);

        for (qsizetype i = index_span.first; i < dispatcher.count(); ++i) {

            counts[i] = boost::math::statistics::correlation_coefficient(index_span.second, dispatcher.at(i).second);

        }

        mutex.lock();

        progress += dispatcher.count() - index_span.first;

        mutex.unlock();

        if (QThread::currentThread()->isInterruptionRequested())
            return counts;

        this->updateProgress(uuidProgress, progress);

        this->checkIfPauseWasRequested();

        return counts;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    return QtConcurrent::blockingMappedReduced<QList<double>>(&d_threadPool, dispatcher, std::function<QList<double>(const std::pair<qsizetype, std::span<double>> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}
