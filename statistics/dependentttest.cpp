#include "dependentttest.h"

qsizetype DependentTTest::d_minimumNumberOfSamples = 2;

qsizetype DependentTTest::d_maximumNumberOfSamples = 2;

qsizetype DependentTTest::d_minimumNumberOfItemsPerSample = 2;

bool DependentTTest::d_effectSizeAvailable = true;

QString DependentTTest::d_effectSizeLabel = "Cohen's d";

QList<QString> DependentTTest::d_testDescriptiveLabels = {"Mean Difference", "Std. Error Difference", "Lower 95% CI Difference", "Upper 95% CI Difference", "Hedges's Gs*", "Std. Error Hedges's Gs*", "Dependent T", "df", "Sig. (2-tailed)"};

std::function<double (const double &)> DependentTTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

DependentTTest::DependentTTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

DependentTTest::DependentTTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_samples.resize(2);

    d_samples[0].resize(data.count() / 2);

    d_samples[1].resize(data.count() / 2);

    for (int i = 0; i < data.size(); ++i)
        d_samples[codedSampleIdentifiers.at(i)][codedPairIdentifiers.at(i)] = data.at(i);

    d_sampleSize = data.size() / 2;

}

void DependentTTest::calculateStatistic()
{

    QList<double> samples1(d_samples[0]);

    QList<double> samples2(d_samples[1]);

    QList<double> differences(d_sampleSize);

    for (qsizetype i = 0; i < d_sampleSize; ++i)
        differences[i] = std::abs(samples1.at(i) - samples2.at(i));

    std::pair<double, double> meanAndVarianceOfDifferences = boost::math::statistics::mean_and_sample_variance(differences);

    d_meanDifference = meanAndVarianceOfDifferences.first;

    d_pooledVariance = meanAndVarianceOfDifferences.second;

    d_standardErrorOfMeanDifference = std::sqrt(d_pooledVariance) / std::sqrt(d_sampleSize);

    d_statistic = d_meanDifference / d_standardErrorOfMeanDifference;

}

void DependentTTest::calculatePValue()
{

    this->calculateStatistic();

    d_degreeOfFreedom = d_sampleSize - 1;

    if ((d_degreeOfFreedom <= 0.0) || std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::students_t students_t_distribution(d_degreeOfFreedom);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(students_t_distribution, std::abs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void DependentTTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    this->calculateEffectSize();

    this->calculateStandardErrorEffectSize();

    boost::math::normal normal_distribution(0.0, 1.0);

    double w = boost::math::quantile(boost::math::complement(normal_distribution, 0.025)) * d_standardErrorOfMeanDifference;

    d_testDescriptiveValues << d_meanDifference << d_standardErrorOfMeanDifference << d_meanDifference - w << d_meanDifference + w << d_effectSize << d_standardErrorOfEffectSize  << d_statistic << d_degreeOfFreedom << d_pValue;

}

void DependentTTest::calculateEffectSize()
{

    this->calculateStatistic();

    d_hedgeM = d_sampleSize - 2.0;

    d_hedgeCorrelation = boost::math::statistics::correlation_coefficient(d_samples.at(0), d_samples.at(1));

    d_hedgeCorrectionFactor = boost::math::tgamma(d_hedgeM / 2.0) / (std::sqrt(d_hedgeM / 2.0) * boost::math::tgamma((d_hedgeM - 1.0) / 2.0));

    d_effectSize = d_hedgeCorrectionFactor * (d_meanDifference / std::sqrt(d_pooledVariance) / std::sqrt(2.0 * (1.0 - d_hedgeCorrelation)));

}

void DependentTTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = std::sqrt((d_hedgeM / (d_hedgeM - 2.0) * 2.0 * (1.0 - d_hedgeCorrelation) / d_sampleSize * (1.0 + d_effectSize * d_effectSize * d_sampleSize / 2.0 * (1 - d_hedgeCorrelation)) - d_effectSize * d_effectSize / d_hedgeCorrectionFactor * d_hedgeCorrectionFactor) * d_hedgeCorrectionFactor * d_hedgeCorrectionFactor);

}
