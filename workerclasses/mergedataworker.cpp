#include "mergedataworker.h"

MergeDataParameters::MergeDataParameters(QObject *parent) :
    BaseParameters(parent, "mergedata")
{

}

bool MergeDataParameters::isValid_()
{

    if (baseAnnotatedMatrices.isEmpty()) {

        d_lastError = "no data to merge";

        return false;

    }

    const char *typeName = baseAnnotatedMatrices.first()->typeName();

    for (qsizetype i = 1; i < baseAnnotatedMatrices.count(); ++i) {

        if (*typeName != *baseAnnotatedMatrices.at(i)->typeName()) {

            d_lastError = "cannot merge -> detected unequal data types";

            return false;

        }

    }

    for (qsizetype i = 0; i < baseAnnotatedMatrices.count(); ++i) {

        QList<QString> identifiers = baseAnnotatedMatrices.at(i)->identifiers(orientation);

        if (identifiers.count() != QSet<QString>(identifiers.begin(), identifiers.end()).count()) {

            if (orientation == Qt::Vertical)
                d_lastError = "cannot merge -> data \"" + baseAnnotatedMatrices.at(i)->property("label").toString() + "\" has duplicate vertical identifiers";
            else
                d_lastError = "cannot merge -> data \"" + baseAnnotatedMatrices.at(i)->property("label").toString() + "\" has duplicate horizontal identifiers";

            return false;

        }

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString MergeDataParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[project = \"" << baseAnnotatedMatrices.first()->property("projectLabel").toString() << "\"]";

    for (qsizetype i = 0; i < baseAnnotatedMatrices.count() ; ++i)
        stream << Qt::endl << "\t[merge data with label = \"" + baseAnnotatedMatrices.at(i)->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrices.at(i)->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[merge orientation = " << ((orientation == Qt::Vertical) ? "vertical" : "horizontal") << "]";

    stream << Qt::endl << "\t[merge column annotations = " << (mergeColumnAnnotations ? "enable" : "disable") << "]";

    stream << Qt::endl << "\t[merge row annotations = " << (mergeRowAnnotations ? "enable" : "disable") << "]";

    stream << Qt::endl << "\t[add data specific suffix to identifiers = " << (addDataSpecificSuffixtoIdentifiers ? "enable" : "disable") << "]";

    stream << Qt::endl << "\t[merge mode = " << mergeMode << "]";

    stream << Qt::endl << "\t[merge selection mode = " << (selectedOnly ? "merge selected only" : "merg all") << "]";

    if (!exportDirectory.isEmpty())
        stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    return prettyPrintString;

}

MergeDataWorker::MergeDataWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("merge data"), parameters, "mergedata")
{

}

void MergeDataWorker::doWork_()
{

    QSharedPointer<MergeDataParameters> parameters(d_data->d_parameters.dynamicCast<MergeDataParameters>());

    const char *typeName = parameters->baseAnnotatedMatrices.first()->typeName();

    if (*typeName == *typeid(short).name())
        this->doWork_(new AnnotatedMatrix<short>);
    else if (*typeName == *typeid(unsigned short).name())
        this->doWork_(new AnnotatedMatrix<unsigned short>);
    else if (*typeName == *typeid(int).name())
        this->doWork_(new AnnotatedMatrix<int>);
    else if (*typeName == *typeid(unsigned int).name())
        this->doWork_(new AnnotatedMatrix<unsigned int>);
    else if (*typeName == *typeid(long long).name())
        this->doWork_(new AnnotatedMatrix<long long>);
    else if (*typeName == *typeid(unsigned long long).name())
        this->doWork_(new AnnotatedMatrix<unsigned long long>);
    else if (*typeName == *typeid(float).name())
        this->doWork_(new AnnotatedMatrix<float>);
    else if (*typeName == *typeid(double).name())
        this->doWork_(new AnnotatedMatrix<double>);
    else if (*typeName == *typeid(QString).name())
        this->doWork_(new AnnotatedMatrix<QString>);
    else if (*typeName == *typeid(unsigned char).name())
        this->doWork_(new AnnotatedMatrix<unsigned char>);
    else if (*typeName == *typeid(bool).name())
        this->doWork_(new AnnotatedMatrix<bool>);

}

bool MergeDataWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<MergeDataParameters> parameters(d_data->d_parameters.dynamicCast<MergeDataParameters>());

    for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i)
        parameters->baseAnnotatedMatrices[i]->lockForRead();

    return true;

}

void MergeDataWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<MergeDataParameters> parameters(d_data->d_parameters.dynamicCast<MergeDataParameters>());

    for (qsizetype i = 0; i < parameters->baseAnnotatedMatrices.count(); ++i)
        parameters->baseAnnotatedMatrices[i]->unlock();

}
