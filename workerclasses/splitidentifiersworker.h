#ifndef SPLITIDENTIFIERSWORKER_H
#define SPLITIDENTIFIERSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class SplitIdentifiersParameters : public BaseParameters
{

public:

    SplitIdentifiersParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QString delimiter;

    qsizetype tokenToKeep;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class SplitIdentifiersWorker : public BaseWorker
{

public:

    SplitIdentifiersWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void SplitIdentifiersWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<SplitIdentifiersParameters> parameters(d_data->d_parameters.dynamicCast<SplitIdentifiersParameters>());

    parameters->tokenToKeep -= 1;

    QUuid uuidProgress = this->startProgress("shuffeling identifiers", 0, parameters->selectedVariableIndexes.count());

    for (qsizetype i = 0; i< parameters->selectedVariableIndexes.count(); ++i) {

        QString currentIdentifier = parameters->selectedVariableIdentifiers.at(i);

        QList<QString> tokens = currentIdentifier.split(QRegularExpression(parameters->delimiter), Qt::SkipEmptyParts);

        if (tokens.count() <= parameters->tokenToKeep)
            this->reportWarning("identifier \"" + currentIdentifier + "\" at index " + QString::number(parameters->selectedVariableIndexes.at(i)) + " has not enough tokens after splitting");
        else
            annotatedMatrix->setIdentifier(parameters->selectedVariableIndexes.at(i), tokens.at(parameters->tokenToKeep), parameters->orientation);

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

}

#endif // SPLITIDENTIFIERSWORKER_H
