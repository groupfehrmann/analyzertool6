#include "transformdatawithpredefinedmetricsworker.h"

TransformDataWithPredefinedMetricsParameters::TransformDataWithPredefinedMetricsParameters(QObject *parent) :
    BaseParameters(parent, "transformdatawithpredefinedmetrics")
{

}

bool TransformDataWithPredefinedMetricsParameters::isValid_()
{

    if (selectedVariableIdentifiers_dataToTransform.isEmpty()) {

        d_lastError = "no variables selected in data to transform";

        return false;

    }

    if (selectedItemIdentifiers_dataToTransform.isEmpty()) {

        d_lastError = "no items selected in data to transform";

        return false;

    }

    if (selectedItemIdentifiers_dataContainingPredefinedMetrics.isEmpty()) {

        d_lastError = "no items selected in data containing predefined metrics";

        return false;

    }


    if (selectedItemIdentifiers_dataToTransform.count() != QSet<QString>(selectedItemIdentifiers_dataToTransform.begin(), selectedItemIdentifiers_dataToTransform.end()).count()) {

        d_lastError = "data to transform \"" + baseAnnotatedMatrix_dataToTranform->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (selectedItemIdentifiers_dataContainingPredefinedMetrics.count() != QSet<QString>(selectedItemIdentifiers_dataContainingPredefinedMetrics.begin(), selectedItemIdentifiers_dataContainingPredefinedMetrics.end()).count()) {

        d_lastError = "data containing predefined metrics \"" + baseAnnotatedMatrix_dataContainingPredefinedMetrics->property("label").toString() + "\" has duplicate identifiers within the selected items";

        return false;

    }

    if (!exportDirectory.isEmpty() && !QDir(exportDirectory).exists()) {

        d_lastError = "no valid export directory : \"" + exportDirectory + "\"";

        return false;

    }

    return true;

}

QString TransformDataWithPredefinedMetricsParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[transform data with label = \"" + baseAnnotatedMatrix_dataToTranform->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix_dataToTranform->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in data to transform containing variables = " << ((orientation_dataToTransform == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variables in data to transform = [\"" << selectedVariableIdentifiers_dataToTransform.first() << "\", " << selectedVariableIndexes_dataToTransform.first() << "]";

    for (qsizetype i = 1; i < selectedVariableIdentifiers_dataToTransform.count(); ++i)
        stream << ", [\"" << selectedVariableIdentifiers_dataToTransform.at(i) << "\", " << selectedVariableIndexes_dataToTransform.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[selected items in data to transform = [\"" << selectedItemIdentifiers_dataToTransform.first() << "\", " << selectedItemIndexes_dataToTransform.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers_dataToTransform.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers_dataToTransform.at(i) << "\", " << selectedItemIndexes_dataToTransform.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[predefined metric in data with label = \"" + baseAnnotatedMatrix_dataContainingPredefinedMetrics->property("label").toString() + "\" and uuid = \"" + baseAnnotatedMatrix_dataContainingPredefinedMetrics->property("uuidObject").toString() + "\"]";

    stream << Qt::endl << "\t[orientation in data with predefined metrics containing variables = " << ((orientation_dataContainingPredefinedMetrics == Qt::Vertical) ? "row" : "column") << "]";

    stream << Qt::endl << "\t[selected variable containing predefined metrics = [\"" << variableIdentifierPredefinedMetrics << "\", index = " + QString::number(variableIndexPredefinedMetrics) + "]";

    stream << Qt::endl << "\t[selected items in data with predefined metrics = [\"" << selectedItemIdentifiers_dataContainingPredefinedMetrics.first() << "\", " << selectedItemIndexes_dataContainingPredefinedMetrics.first() << "]";

    for (qsizetype i = 1; i < selectedItemIdentifiers_dataContainingPredefinedMetrics.count(); ++i)
        stream << ", [\"" << selectedItemIdentifiers_dataContainingPredefinedMetrics.at(i) << "\", " << selectedItemIndexes_dataContainingPredefinedMetrics.at(i) << "]";

    stream << "]";

    stream << Qt::endl << "\t[transform function = [\"" << transformLabel << "\"]";

    stream << Qt::endl << "\t[export directory = \"" << exportDirectory << "\"]";

    stream << Qt::endl << "\t[number of threads to use = " << numberOfThreadsToUse << "]";

    return prettyPrintString;

}

TransformDataWithPredefinedMetricsWorker::TransformDataWithPredefinedMetricsWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("transform data with predefined metrics"), parameters, "transformdatawithpredefinedmetrics")
{

}

void TransformDataWithPredefinedMetricsWorker::doWork_()
{

    QSharedPointer<TransformDataWithPredefinedMetricsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataWithPredefinedMetricsParameters>());

    d_threadPool.setMaxThreadCount(parameters->numberOfThreadsToUse);

    MathUtilityFunctions::setNumberOfThreadsUsedForBLASandLAPACKroutines(parameters->numberOfThreadsToUse);

    const char *typeNameDataToTranform = parameters->baseAnnotatedMatrix_dataToTranform->typeName();

    const char *typeNameDataContainingPredefinedMetrics = parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->typeName();

    if (*typeNameDataToTranform != *typeNameDataContainingPredefinedMetrics) {

        this->reportError("for transforming data with predefined metrics, data to transform and data containing predefined metrics need to be the same numerical type");

        QThread::currentThread()->requestInterruption();

    }

    if (*typeNameDataToTranform == *typeid(short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<short>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(unsigned short).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned short>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<int>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(unsigned int).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned int>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<long long>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(unsigned long long).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<unsigned long long>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(float).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<float>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else if (*typeNameDataToTranform == *typeid(double).name())
        this->doWork_(qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix_dataToTranform), qSharedPointerDynamicCast<AnnotatedMatrix<double>>(parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics));
    else {

        this->reportError("for transforming data with predefined metrics, data and indepependent components need to be numerical");

        QThread::currentThread()->requestInterruption();

    }

}

bool TransformDataWithPredefinedMetricsWorker::performReadWriteLockLogistics_()
{

    QSharedPointer<TransformDataWithPredefinedMetricsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataWithPredefinedMetricsParameters>());

    parameters->baseAnnotatedMatrix_dataToTranform->lockForWrite();

    parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->lockForRead();

    return true;

}

void TransformDataWithPredefinedMetricsWorker::performReadWriteUnlockLogistics_()
{

    QSharedPointer<TransformDataWithPredefinedMetricsParameters> parameters(d_data->d_parameters.dynamicCast<TransformDataWithPredefinedMetricsParameters>());

    parameters->baseAnnotatedMatrix_dataToTranform->unlock();

    parameters->baseAnnotatedMatrix_dataContainingPredefinedMetrics->unlock();

}
