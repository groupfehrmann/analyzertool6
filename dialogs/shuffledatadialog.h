#ifndef SHUFFLEDATADIALOG_H
#define SHUFFLEDATADIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "workerclasses/shuffledataworker.h"

class ShuffleDataDialog : public BaseDialog
{

public:

    ShuffleDataDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    ItemSelectorWidget *d_itemSelectorWidget_items;

private slots:

    void orientationChanged(Qt::Orientation orientation);

};

#endif // SHUFFLEDATADIALOG_H
