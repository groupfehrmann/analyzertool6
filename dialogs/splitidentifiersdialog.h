#ifndef SPLITIDENTIFIERSDIALOG_H
#define SPLITIDENTIFIERSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/selectdelimiterwidget.h"
#include "widgets/spinboxwidget.h"
#include "workerclasses/splitidentifiersworker.h"

class SplitIdentifiersDialog : public BaseDialog
{

public:

    SplitIdentifiersDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    OrientationWidget *d_orientationWidget;

    ItemSelectorWidget *d_itemSelectorWidget_variables;

    SelectDelimiterWidget *d_selectDelimiterWidget;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void dialogAccepted();

};

#endif // SPLITIDENTIFIERSDIALOG_H
