#ifndef CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSDIALOG_H
#define CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/selectfilewidget.h"
#include "widgets/orientationwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/spinboxwidget.h"
#include "widgets/doublespinboxwidget.h"
#include "widgets/selectdirectorywidget.h"
#include "workerclasses/calculateconsensusforindependentcomponentsworker.h"

class CalculateConsensusForIndependentComponentsDialog : public BaseDialog
{

public:

    CalculateConsensusForIndependentComponentsDialog(QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    DoubleSpinBoxWidget *d_doubleSpinBoxWidget_credibilityIndexThreshold;

};

#endif // CALCULATECONSENSUSFORINDEPENDENTCOMPONENTSDIALOG_H
