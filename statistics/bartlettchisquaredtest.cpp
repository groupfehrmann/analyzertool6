#include "bartlettchisquaredtest.h"

qsizetype BartlettChiSquaredTest::d_minimumNumberOfSamples = 2;

qsizetype BartlettChiSquaredTest::d_maximumNumberOfSamples = std::numeric_limits<qsizetype>::max();

qsizetype BartlettChiSquaredTest::d_minimumNumberOfItemsPerSample = 2;

bool BartlettChiSquaredTest::d_effectSizeAvailable = false;

QString BartlettChiSquaredTest::d_effectSizeLabel = QString();

QList<QString> BartlettChiSquaredTest::d_testDescriptiveLabels = {"Bartlett chi-squared", "df", "Sig. (2-tailed)"};

std::function<double (const double &)> BartlettChiSquaredTest::d_backtransformEffectSizeFunction = [](const double &effectSize){return effectSize;};

BartlettChiSquaredTest::BartlettChiSquaredTest() :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

}

BartlettChiSquaredTest::BartlettChiSquaredTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples) :
    BaseStatisticalTest(d_minimumNumberOfSamples, d_maximumNumberOfSamples, d_minimumNumberOfItemsPerSample, d_testDescriptiveLabels, d_effectSizeAvailable, d_effectSizeLabel, d_backtransformEffectSizeFunction)
{

    d_samples.resize(numberOfSamples);

    d_sampleSize = 0;

    for (qsizetype i = 0; i < data.count(); ++i) {

        const qsizetype &codedSampleIdentifier(codedSampleIdentifiers.at(i));

        if (codedSampleIdentifier != -1) {

            d_samples[codedSampleIdentifier].append(data.at(i));

            ++d_sampleSize;
        }

    }

}

void BartlettChiSquaredTest::calculateStatistic()
{

    double k = d_samples.size();

    double n = d_sampleSize;

    double varp = 0.0;

    double chi1 = 0.0;

    double chi2 = 0.0;

    for (const QList<double> &sample : d_samples) {

        double vari = boost::math::statistics::variance(sample);

        double ni = sample.count();

        varp += ((ni - 1.0) * vari);

        chi1 += (ni - 1.0) * std::log(vari);

        chi2 += 1.0 / (ni - 1.0);

    }

    varp /=  (n - k);

    double chi = ((n - k) * std::log(varp) - chi1) / (1.0 + (1.0 / (3.0 * (k - 1.0))) * (chi2 - (1.0 / (n - k))));

    d_statistic = (chi < 0.0) ? 0.0 : chi;

}

void BartlettChiSquaredTest::calculatePValue()
{

    this->calculateStatistic();

    d_degreeOfFreedom = d_samples.count() - 2.0;

    if ((d_degreeOfFreedom <= 0.0) || d_statistic < 0.0 || std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_degreeOfFreedom);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void BartlettChiSquaredTest::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_degreeOfFreedom << d_pValue;

}

void BartlettChiSquaredTest::calculateEffectSize()
{

    this->calculateStatistic();

    d_effectSize = std::numeric_limits<double>::quiet_NaN();

}

void BartlettChiSquaredTest::calculateStandardErrorEffectSize()
{

    this->calculateEffectSize();

    d_standardErrorOfEffectSize = std::numeric_limits<double>::quiet_NaN();

}
