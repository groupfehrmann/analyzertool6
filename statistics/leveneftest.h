#ifndef LEVENEFTEST_H
#define LEVENEFTEST_H

#include <QList>

#include <boost/math/statistics/univariate_statistics.hpp>
#include "boost/math/distributions/fisher_f.hpp"

#include "basestatisticaltest.h"

class LeveneFTest : public BaseStatisticalTest
{

public:

    LeveneFTest();

    LeveneFTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers, qsizetype numberOfSamples);

    virtual ~LeveneFTest() {}

private:

    double d_df1;

    double d_df2;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // LEVENEFTEST_H
