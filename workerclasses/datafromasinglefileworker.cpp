#include "datafromasinglefileworker.h"

DataFromASingleFileParameters::DataFromASingleFileParameters(QObject *parent) :
    BaseParameters(parent, "importdatafromasinglefile")
{

}

bool DataFromASingleFileParameters::isValid_()
{

    if (!QFile::exists(pathToFile)) {

        d_lastError = "could not find file \"" + pathToFile + "\"";

        return false;

    }

    if (lineNumberOfLastDataLine < lineNumberOfFirstDataLine) {

        d_lastError = "line number of last data line (" + QString::number(lineNumberOfLastDataLine) + ") can not be less than line number of first data line (" + QString::number(lineNumberOfFirstDataLine) + ")";

        return false;

    }

    if ((lineNumberOfHeader != -1 ) && (lineNumberOfFirstDataLine <= lineNumberOfHeader)) {

        d_lastError = "line number of first data line (" + QString::number(lineNumberOfFirstDataLine) + ") can not be less than or equal to line number of header line (" + QString::number(lineNumberOfHeader) + ")";

        return false;

    }

    if ((indexOfColumnDefiningSecondDataColumn != -1) && (indexOfColumnDefiningSecondDataColumn < indexOfColumnDefiningFirstDataColumn)) {

        d_lastError = "index of column defining second data column (" + QString::number(indexOfColumnDefiningSecondDataColumn) + ") can not be less than index of column defining first data column (" + QString::number(indexOfColumnDefiningFirstDataColumn) + ")";

        return false;

    }

    if ((indexOfColumnDefiningRowIdentifiers != -1) && (indexOfColumnDefiningFirstDataColumn < indexOfColumnDefiningRowIdentifiers)) {

        d_lastError = "index of column defining first data column (" + QString::number(indexOfColumnDefiningFirstDataColumn) + ") can not be less than index of column defining row identifiers (" + QString::number(indexOfColumnDefiningRowIdentifiers) + ")";

        return false;

    }

    return true;

}

QString DataFromASingleFileParameters::prettyPrint_() const
{

    QString prettyPrintString;

    QTextStream stream(&prettyPrintString);

    stream << "parameters ->";

    stream << Qt::endl << "\t[type name = \"" << typeName << "\"]";

    stream << Qt::endl << "\t[path to file = \"" << pathToFile << "\"]";

    stream << Qt::endl << "\t[seperator = \"" << delimiter << "\"]";

    if (lineNumberOfHeader == -1)
        stream << Qt::endl << "\t[line number of header = \"header not present\"]";
    else {

        stream << Qt::endl << "\t[first token in header defines column with row identifiers = " << ((firstTokenInHeaderDefinesColumnWithRowIdentifiers) ? "true" : "false") << "]";

        stream << Qt::endl << "\t[line number of header = " << lineNumberOfHeader << "]";

    }

    stream << Qt::endl << "\t[line number of first data line = " << lineNumberOfFirstDataLine << "]";

    if (lineNumberOfLastDataLine == -1)
        stream << Qt::endl << "\t[line number of last data line = \"automatically detected\"]";
    else
        stream << Qt::endl << "\t[line number of last data line = " << lineNumberOfLastDataLine << "]";

    if (indexOfColumnDefiningRowIdentifiers != -1)
        stream << Qt::endl << "\t[index of column defining row identifiers = " << indexOfColumnDefiningRowIdentifiers << "]";
    else
        stream << Qt::endl << "\t[index of column defining row identifiers = \"row identifiers not present\"";

    stream << Qt::endl << "\t[index of column defining first data column = " << indexOfColumnDefiningFirstDataColumn << "]";

    stream << Qt::endl << "\t[index of column defining second data column = " << indexOfColumnDefiningSecondDataColumn << "]";

    return prettyPrintString;

}

DataFromASingleFileWorker::DataFromASingleFileWorker(QObject *parent, const QSharedPointer<BaseParameters> &parameters) :
    BaseWorker(parent, QString("import data from a single file"), parameters, "importdatafromasinglefile")
{

}

void DataFromASingleFileWorker::doWork_()
{

    QSharedPointer<DataFromASingleFileParameters> parameters(d_data->d_parameters.dynamicCast<DataFromASingleFileParameters>());

    if (parameters->typeName == "short")
        this->doWork_(new AnnotatedMatrix<short>);
    else if (parameters->typeName == "unsigned short")
        this->doWork_(new AnnotatedMatrix<unsigned short>);
    else if (parameters->typeName == "int")
        this->doWork_(new AnnotatedMatrix<int>);
    else if (parameters->typeName == "unsigned int")
        this->doWork_(new AnnotatedMatrix<unsigned int>);
    else if (parameters->typeName == "long long")
        this->doWork_(new AnnotatedMatrix<long long>);
    else if (parameters->typeName == "unsigned long long")
        this->doWork_(new AnnotatedMatrix<unsigned long long>);
    else if (parameters->typeName == "float")
        this->doWork_(new AnnotatedMatrix<float>);
    else if (parameters->typeName == "double")
        this->doWork_(new AnnotatedMatrix<double>);
    else if (parameters->typeName == "string")
        this->doWork_(new AnnotatedMatrix<QString>);
    else if (parameters->typeName == "unsigned char")
        this->doWork_(new AnnotatedMatrix<unsigned char>);
    else if (parameters->typeName == "bool")
        this->doWork_(new AnnotatedMatrix<bool>);

}

bool DataFromASingleFileWorker::performReadWriteLockLogistics_()
{

    return true;

}

void DataFromASingleFileWorker::performReadWriteUnlockLogistics_()
{

}
