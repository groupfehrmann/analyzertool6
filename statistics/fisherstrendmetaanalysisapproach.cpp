#include "fisherstrendmetaanalysisapproach.h"

QList<QString> FishersTrendMetaAnalysisApproach::d_testDescriptiveLabels = {"Chi-squared", "df", "Sig. (2-tailed)"};

FishersTrendMetaAnalysisApproach::FishersTrendMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

FishersTrendMetaAnalysisApproach::FishersTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr),
    d_statistics(statistics),
    d_pValues(pValues),
    d_sampleSizes(sampleSizes)
{

}

void FishersTrendMetaAnalysisApproach::calculateStatistic()
{

    d_statistic = 0;

    double chiSquared = 0.0;

    for (qsizetype i = 0; i < d_statistics.count(); ++i) {

        if (d_statistics.at(i) < 0.0)
            chiSquared -= std::log(d_pValues.at(i));
        else
            chiSquared += std::log(d_pValues.at(i));

    }

    d_statistic = -2.0 * chiSquared;

}

void FishersTrendMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::chi_squared dist(d_pValues.count() * 2.0);

    d_pValue = boost::math::cdf(boost::math::complement(dist, d_statistic));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void FishersTrendMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_pValues.count() * 2.0 << d_pValue;

}
