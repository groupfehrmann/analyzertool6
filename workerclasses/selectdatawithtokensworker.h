#ifndef SELECTDATAWITHTOKENSWORKER_H
#define SELECTDATAWITHTOKENSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QRegularExpression>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"

class SelectDataWithTokensParameters : public BaseParameters
{

public:

    SelectDataWithTokensParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::CaseSensitivity caseSensitivity;

    QString delimiter;

    QString matchType;

    Qt::Orientation orientation;

    QList<qsizetype> selectedIdentifiersIndexes;

    QList<QString> selectedIdentifiersLabels;

    QList<qsizetype> selectedIndexesToApplyOn;

    QList<QString> selectedLabelsToApplyOn;

    QString selectionMode1;

    QString selectionMode2;

    QList<QString> tokensToMatch;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class SelectDataWithTokensWorker : public BaseWorker
{

public:

    SelectDataWithTokensWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    bool hashMatch(const QString &str, QList<QRegularExpression> regularExpressions);

};

#endif // SELECTDATAWITHTOKENSWORKER_H
