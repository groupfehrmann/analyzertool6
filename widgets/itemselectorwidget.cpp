#include "itemselectorwidget.h"
#include "ui_itemselectorwidget.h"


ItemSelectorModel::ItemSelectorModel(QObject *parent) :
    QAbstractListModel(parent),
    d_dragDropIdentifier(QUuid::createUuid().toString())
{

}

void ItemSelectorModel::clear()
{

    this->beginResetModel();

    d_itemLabels.clear();

    d_itemIndexes.clear();

    this->endResetModel();

    emit this->selectionChanged(d_itemLabels, d_itemIndexes);

}

QVariant ItemSelectorModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole)
        return d_itemLabels.at(index.row());
    if (role == Qt::UserRole)
        return d_itemIndexes.at(index.row());
    else
        return QVariant();

}

const QString &ItemSelectorModel::dragDropIdentifier() const
{

    return d_dragDropIdentifier;

}

bool ItemSelectorModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{

    if (action == Qt::IgnoreAction)
        return true;

    if (!data->hasFormat(d_dragDropIdentifier))
        return false;

    if (column > 0)
        return false;

    int beginRow;

    if (row != -1)
        beginRow = row;
    else if (parent.isValid())
        beginRow = parent.row();
    else
        beginRow = this->rowCount(QModelIndex());

    QByteArray encodedData = data->data(d_dragDropIdentifier);

    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    qsizetype rows;

    stream >> rows;

    this->beginInsertRows(QModelIndex(), beginRow, beginRow + rows - 1);

    while (!stream.atEnd()) {

        QString label;

        qsizetype index;

        stream >> label;

        stream >> index;

        d_itemLabels.insert(beginRow, label);

        d_itemIndexes.insert(beginRow, index);

        ++beginRow;

    }

    this->endInsertRows();

    emit this->selectionChanged(d_itemLabels, d_itemIndexes);

    return true;

}

Qt::ItemFlags ItemSelectorModel::flags(const QModelIndex &index) const
{

    Qt::ItemFlags defaultFlags = this->QAbstractItemModel::flags(index);

    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    else
        return Qt::ItemIsDropEnabled | Qt::ItemIsSelectable;

}

const QList<QString> &ItemSelectorModel::itemLabels() const
{

    return d_itemLabels;

}

const QList<qsizetype> &ItemSelectorModel::itemIndexes() const
{

    return d_itemIndexes;

}

QMimeData *ItemSelectorModel::mimeData(const QModelIndexList &indexes) const
{

    QMimeData *mimeData = new QMimeData();

    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    stream << indexes.count();

    for (const QModelIndex &index : indexes)
        stream << data(index, Qt::DisplayRole).toString() << static_cast<qsizetype>(data(index, Qt::UserRole).toLongLong());

    mimeData->setData(d_dragDropIdentifier, encodedData);

    return mimeData;

}

QStringList ItemSelectorModel::mimeTypes() const
{

    QStringList types;

    types << d_dragDropIdentifier;

    return types;

}

bool ItemSelectorModel::removeRows(int row, int count, const QModelIndex &parent)
{

    Q_UNUSED(parent);

    this->beginRemoveRows(parent, row, row + count - 1);

    for (int i = row + count - 1; i >= row; --i) {

        d_itemLabels.removeAt(i);

        d_itemIndexes.removeAt(i);

    }

    this->endRemoveRows();

    emit this->selectionChanged(d_itemLabels, d_itemIndexes);

    return true;

}

int ItemSelectorModel::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;
    else
        return d_itemLabels.count();

}

void ItemSelectorModel::setDragDropIdentifier(const QString &dragDropIdentifier)
{

    d_dragDropIdentifier = dragDropIdentifier;

}

void ItemSelectorModel::setItemLabelsAndIndexes(const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes)
{

    this->beginResetModel();

    d_itemLabels = itemLabels;

    d_itemIndexes = itemIndexes;

    this->endResetModel();

    emit this->selectionChanged(d_itemLabels, d_itemIndexes);

}

void ItemSelectorModel::setItemLabelsAndIndexes(const QList<QString> &itemLabels)
{

    QList<qsizetype> itemIndexes(itemLabels.count());

    std::iota(itemIndexes.begin(), itemIndexes.end(), 0);

    this->setItemLabelsAndIndexes(itemLabels, itemIndexes);

}

Qt::DropActions ItemSelectorModel::supportedDropActions() const
{

    return Qt::MoveAction;

}


ItemSelectorWidget::ItemSelectorWidget(BaseDialog *parent, const QString &widgetSettingsIdentifier) :
    QWidget(parent),
    ui(new Ui::ItemSelectorWidget),
    d_readWidgetSettings(false),
    d_widgetSettingsIdentifier(parent->dialogSettingsIdentifier() + "_" + widgetSettingsIdentifier)
{

    ui->setupUi(this);

    ui->splitter->setStretchFactor(0, 0);

    ui->splitter->setStretchFactor(1, 1);

    d_itemSelectorModel_notSelected = new ItemSelectorModel(this);

    d_itemSelectorModel_selected = new ItemSelectorModel(this);

    d_itemSelectorModel_selected->setDragDropIdentifier(d_itemSelectorModel_notSelected->dragDropIdentifier());

    QSortFilterProxyModel *sortFilterProxyModel_notSelected = new QSortFilterProxyModel(this);

    sortFilterProxyModel_notSelected->setSourceModel(d_itemSelectorModel_notSelected);

    ui->listView_notSelected->setModel(sortFilterProxyModel_notSelected);

    ui->listView_selected->setModel(d_itemSelectorModel_selected);

    this->connect(ui->lineEdit_filter, &QLineEdit::textChanged, sortFilterProxyModel_notSelected, &QSortFilterProxyModel::setFilterFixedString);

}

ItemSelectorModel *ItemSelectorWidget::itemSelectorModel_notSelected()
{

    return d_itemSelectorModel_notSelected;

}

ItemSelectorModel *ItemSelectorWidget::itemSelectorModel_selected()
{

    return d_itemSelectorModel_selected;

}

void ItemSelectorWidget::clear()
{

    d_itemSelectorModel_notSelected->clear();

    d_itemSelectorModel_selected->clear();

}

QLabel *ItemSelectorWidget::label()
{

    return ui->label;

}

void ItemSelectorWidget::readWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/d_itemSelectorModel_notSelected");

    if (settings.contains("itemLabels") && settings.contains("itemIndexes"))
        d_itemSelectorModel_notSelected->setItemLabelsAndIndexes(settings.value("itemLabels").value<QList<QString>>(), settings.value("itemIndexes").value<QList<qsizetype>>());

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/d_itemSelectorModel_selected");

    if (settings.contains("itemLabels") && settings.contains("itemIndexes"))
        d_itemSelectorModel_selected->setItemLabelsAndIndexes(settings.value("itemLabels").value<QList<QString>>(), settings.value("itemIndexes").value<QList<qsizetype>>());

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/splitter");

    if (settings.contains("state"))
        ui->splitter->restoreState(settings.value("state").toByteArray());

    settings.endGroup();

    d_readWidgetSettings = true;


}

void ItemSelectorWidget::writeWidgetSettings()
{

    QSettings settings;

    settings.beginGroup(d_widgetSettingsIdentifier + "/d_itemSelectorModel_notSelected");

    settings.setValue("itemLabels", d_itemSelectorModel_notSelected->itemLabels());

    settings.setValue("itemIndexes", QVariant::fromValue(d_itemSelectorModel_notSelected->itemIndexes()));

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/d_itemSelectorModel_selected");

    settings.setValue("itemLabels", d_itemSelectorModel_selected->itemLabels());

    settings.setValue("itemIndexes", QVariant::fromValue(d_itemSelectorModel_selected->itemIndexes()));

    settings.endGroup();

    settings.beginGroup(d_widgetSettingsIdentifier + "/splitter");

    settings.setValue("state", ui->splitter->saveState());

    settings.endGroup();

}

ItemSelectorWidget::~ItemSelectorWidget()
{

    if (d_readWidgetSettings)
        this->writeWidgetSettings();

    delete ui;

}
