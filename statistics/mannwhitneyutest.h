#ifndef MANNWHITNEYUTEST_H
#define MANNWHITNEYUTEST_H

#include <QList>

#include <boost/math/statistics/univariate_statistics.hpp>
#include "boost/math/distributions/normal.hpp"
#include "boost/range/numeric.hpp"

#include "basestatisticaltest.h"
#include "math/mathoperations.h"

class MannWhitneyUTest : public BaseStatisticalTest
{

public:

    MannWhitneyUTest();

    MannWhitneyUTest(const QList<double> &data, const QList<qsizetype> &codedSampleIdentifiers);

    virtual ~MannWhitneyUTest() {}

private:

    double d_tiesCorrectionFactor;

    double d_n1;

    double d_n2;

    double d_u1;

    double d_z;

    double d_pointBiserialR;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    void calculateEffectSize() override;

    void calculateStandardErrorEffectSize() override;

    static qsizetype d_minimumNumberOfSamples;

    static qsizetype d_maximumNumberOfSamples;

    static qsizetype d_minimumNumberOfItemsPerSample;

    static QList<QString> d_testDescriptiveLabels;

    static bool d_effectSizeAvailable;

    static QString d_effectSizeLabel;

    static std::function<double (const double &)> d_backtransformEffectSizeFunction;

};

#endif // MANNWHITNEYUTEST_H
