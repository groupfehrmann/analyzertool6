#ifndef MATHOPERATIONS_H
#define MATHOPERATIONS_H

#include <QList>
#include <QThreadPool>
#include <QtConcurrent>

#include <span>
#include <random>
#include <chrono>
#include <cmath>
#include <algorithm>
#include <iterator>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>

#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include <boost/math/statistics/anderson_darling.hpp>
#include <boost/math/statistics/univariate_statistics.hpp>
#include <boost/math/tools/norms.hpp>

#include "trip.h"
#include "mathutilityfunctions.h"
#include "math/mins.h"

namespace MathOperations {

    QList<double> &fillWithRandomNumbersFromUniformRealDistribution(QList<double> &sequence, const double &minimumValue, const double &maximumValue);

    QList<std::complex<double>> &fillWithRandomComplexNumbersFromUniformRealDistribution(QList<std::complex<double>> &sequence, const double &minimumValue, const double &maximumValue);

    QList<double> &orthogonalizeMatrixInplace(QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<std::complex<double>> &orthogonalizeMatrixInplace(QList<std::complex<double>> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<double> &mPowerSymmetricMatrixInplace(QList<double> &symmetricMatrix, qsizetype leadingDimension, double power, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<std::complex<double>> &mPowerSymmetricMatrixInplace(QList<std::complex<double>> &symmetricMatrix, qsizetype leadingDimension, double power, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<double> &createSymmetricMatrixInPlace(QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    QList<double> elementWise_multiplication(const QList<double> &vector1, const QList<double> &vector2, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    QList<double> elementWise_multiplication_inplace(QList<double> &vector1, const QList<double> &vector2, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <typename T> QList<T> &transposeInplace(QList<T> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <typename T> QList<QList<T> > &transposeInplace(QList<QList<T> > &matrix);

    template <typename T> QList<T> transpose(const QList<T> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <typename T> QList<T> sampleColumns(const QList<T> &matrix, qsizetype leadingDimension, double proportionToSample, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount());

    template <typename Functor> QList<double> applyFunctor_elementWise(const QList<double> &vector, const Functor &functor, int numberOfThreadsToUse);

    template <typename Functor> QList<std::complex<double>> applyFunctor_elementWise(const QList<std::complex<double>> &vector, const Functor &functor, int numberOfThreadsToUse);

    template <typename Functor> QList<double> &applyFunctor_elementWise_inplace(QList<double> &vector, const Functor &functor, int numberOfThreadsToUse);

    template <typename Functor> QList<std::complex<double>> &applyFunctor_elementWise_inplace(QList<std::complex<double>> &vector, const Functor &functor, int numberOfThreadsToUse);

    template <typename T> QList<T> repeatVector(const QList<T> &vector, qsizetype n);

    QList<double> &centerVectorToMeanInplace(QList<double> &vector);

    std::span<double> &centerVectorToMeanInplace(std::span<double> &vector);

    QList<std::complex<double>> &centerVectorToMeanInplace(QList<std::complex<double>> &vector);

    std::span<std::complex<double>> &centerVectorToMeanInplace(std::span<std::complex<double>> &vector);

    std::span<double> &centerVectorToMeanInplaceIgnoreNaNs(std::span<double> &vector);

    QList<double> &scaleVectorToL2NormInplace(QList<double> &vector);

    std::span<double> &scaleVectorToL2NormInplace(std::span<double> &vector);

    QList<std::complex<double>> &scaleVectorToL2NormInplace(QList<std::complex<double>> &vector);

    std::span<std::complex<double>> &scaleVectorToL2NormInplace(std::span<std::complex<double>> &vector);

    std::span<double> &scaleVectorToL2NormInplaceIgnoreNaNs(std::span<double> &vector);

    QList<double> inverse(QList<double> matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<double> pseudoInverse(const QList<double> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<std::complex<double>> pseudoInverse(const QList<std::complex<double>> &matrix, qsizetype leadingDimension, bool checkError, double &error, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<double> pseudoInverseSVD(QList<double> matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    QList<std::complex<double>> pseudoInverseSVD(QList<std::complex<double>> matrix, qsizetype leadingDimension, int numberOfThreadsToUse = QThreadPool::globalInstance()->maxThreadCount(), int *info = nullptr);

    template <typename T> double crankInplace(QList<T> &vector);

    template <typename T> double crankInplace(std::span<T> &vector);

    template <typename T> void rankInplace(QList<T> &vector);

    template <typename T> void rankInplace(std::span<T> &vector);

    template <typename T> double cox_box_transformation(const T &value, const double &lambda);

    template <typename T> void cox_box_transformation(QList<T> &vector, const double &lambda);

    template <typename T> void cox_box_transformation(std::span<T> &vector, const double &lambda);

    template <typename T> std::pair<double, double> cox_box_transformation(QList<T> &vector_sorted, bool &succes);

    template <typename T> std::pair<double, double> cox_box_transformation(std::span<T> &vector_sorted, bool &succes);

    template <typename T> void medianCenteredForcedSymmetric(QList<T> &vector_sorted, double &value);

    template <typename T> void medianCenteredForcedSymmetric(std::span<T> &vector_sorted, double &value);

    std::span<double> &standardize(std::span<double> &vector);

    template <typename T> void mirrorSymmetrixMatrix(QList<T> &matrix, qsizetype leadingDimenstion);

}

template <typename T>
QList<T> &MathOperations::transposeInplace(QList<T> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse)
{

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    TRIP_transpose_algorithm::transpose(matrix.data(), 0, numberOfRows, 0, leadingDimension, leadingDimension, numberOfThreadsToUse);

    return matrix;

}

template <typename T>
QList<QList<T> > &MathOperations::transposeInplace(QList<QList<T> > &matrix)
{

    qsizetype numberOfRows = matrix.size();

    qsizetype numberOfColumns = matrix.isEmpty() ? 0 : matrix.first().size();

    if (matrix.isEmpty() || ((numberOfRows == 1) && (numberOfColumns == 1)))
        return matrix;

    if (numberOfRows == numberOfColumns) {

        for (qsizetype i = 0; i < numberOfRows; ++i) {

            QList<T> &currentRowVector(matrix[i]);

            for (qsizetype j = i + 1; j < numberOfColumns; ++j)
                std::swap(currentRowVector[j], matrix[j][i]);

        }

    } else if (numberOfRows < numberOfColumns) {

        matrix << QList<QList<T> >(numberOfColumns - numberOfRows, QList<T>(numberOfRows));

        for (qsizetype i = 0; i < numberOfRows; ++i) {

            QList<T> &currentRowVector(matrix[i]);

            for (qsizetype j = i + 1; j < numberOfRows; ++j)
                std::swap(currentRowVector[j], matrix[j][i]);

            for (qsizetype j = numberOfRows; j < numberOfColumns; ++j)
                matrix[j][i] = currentRowVector.at(j);

            currentRowVector.resize(numberOfRows);

        }

    } else {

        for (qsizetype i = 0; i < numberOfColumns; ++i) {

            QList<T> &currentRowVector(matrix[i]);

            for (qsizetype j = i + 1; j < numberOfColumns; ++j)
                std::swap(currentRowVector[j], matrix[j][i]);

            currentRowVector.resize(numberOfRows);

        }

        for (qsizetype i = numberOfColumns; i < numberOfRows; ++i) {

            QList<T> &currentRowVector(matrix[i]);

            for (qsizetype j = 0; j < numberOfColumns; ++j)
                matrix[j][i] = currentRowVector.at(j);

        }

        matrix.resize(numberOfColumns);

    }

    return matrix;

}

template <typename T>
QList<T> MathOperations::transpose(const QList<T> &matrix, qsizetype leadingDimension, int numberOfThreadsToUse)
{

    qsizetype numberOfRows = matrix.count() / leadingDimension;

    QList<T> transposedMatrix = matrix;

    TRIP_transpose_algorithm::transpose(transposedMatrix.data(), 0, numberOfRows, 0, leadingDimension, leadingDimension, numberOfThreadsToUse);

    return transposedMatrix;

}

template <typename T>
QList<T> MathOperations::sampleColumns(const QList<T> &matrix, qsizetype leadingDimension, double proportionToSample, int numberOfThreadsToUse)
{

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);


    QList<qsizetype> indexes(leadingDimension);

    std::iota(indexes.begin(), indexes.end(), 0);

    std::random_device rd;

    std::mt19937 g(rd());

    std::shuffle(indexes.begin(), indexes.end(), g);

    QList<qsizetype> sampledIndexes = indexes.mid(0, static_cast<qsizetype>(std::floor(static_cast<double>(leadingDimension) * proportionToSample)));


    auto mapFunctor = [&sampledIndexes](const std::span<T> &span) {

        QList<T> temp;

        temp.reserve(sampledIndexes.count());

        for (qsizetype index : sampledIndexes)
            temp.append(span[index]);

        return temp;

    };

    auto reduceFunctor = [](QList<T> &result, const QList<T> &intermediateResult) {

        result.append(intermediateResult);

    };

    return QtConcurrent::blockingMappedReduced<QList<T>>(&threadPool, MathUtilityFunctions::rowMajor_matrix1d_rowSpans(*const_cast<QList<T> *>(&matrix), leadingDimension), std::function<QList<T>(const std::span<T> &span)>(mapFunctor), std::function<void(QList<T> &, const QList<T> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}

template <typename Functor>
QList<double> MathOperations::applyFunctor_elementWise(const QList<double> &vector, const Functor &functor, int numberOfThreadsToUse)
{

    auto mapFunctor = [&functor](const std::span<double> &span) {

        QList<double> intermediateResultVector;

        intermediateResultVector.reserve(span.size());

        for (const double &value : span)
            intermediateResultVector << functor(value);

        return intermediateResultVector;

    };

    auto reduceFunctor = [](QList<double> &result, const QList<double> &intermediateResult) {

        result.append(intermediateResult);

    };

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    return QtConcurrent::blockingMappedReduced<QList<double>>(&threadPool, MathUtilityFunctions::elementWiseDispatcher(*const_cast<QList<double> *>(&vector), numberOfThreadsToUse), std::function<QList<double>(const std::span<double> &index_span)>(mapFunctor), std::function<void(QList<double> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}

template <typename Functor>
QList<std::complex<double>> MathOperations::applyFunctor_elementWise(const QList<std::complex<double>> &vector, const Functor &functor, int numberOfThreadsToUse)
{

    auto mapFunctor = [&functor](const std::span<std::complex<double>> &span) {

        QList<std::complex<double>> intermediateResultVector;

        intermediateResultVector.reserve(span.size());

        for (const std::complex<double> &value : span)
            intermediateResultVector << functor(value);

        return intermediateResultVector;

    };

    auto reduceFunctor = [](QList<std::complex<double>> &result, const QList<std::complex<double>> &intermediateResult) {

        result.append(intermediateResult);

    };

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    return QtConcurrent::blockingMappedReduced<QList<std::complex<double>>>(&threadPool, MathUtilityFunctions::elementWiseDispatcher(*const_cast<QList<std::complex<double>> *>(&vector), numberOfThreadsToUse), std::function<QList<std::complex<double>>(const std::span<std::complex<double>> &index_span)>(mapFunctor), std::function<void(QList<std::complex<double>> &, const QList<std::complex<double>> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

}

template <typename Functor>
QList<double> &MathOperations::applyFunctor_elementWise_inplace(QList<double> &vector, const Functor &functor, int numberOfThreadsToUse)
{

    auto mapFunctor = [&functor](std::span<double> &span) {

        for (double &value : span)
            functor(value);

    };

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::elementWiseDispatcher(vector, numberOfThreadsToUse), std::function<void(std::span<double> &span)>(mapFunctor));

    return vector;

}

template <typename Functor>
QList<std::complex<double>> &MathOperations::applyFunctor_elementWise_inplace(QList<std::complex<double>> &vector, const Functor &functor, int numberOfThreadsToUse)
{

    auto mapFunctor = [&functor](std::span<std::complex<double>> &span) {

        for (std::complex<double> &value : span)
            functor(value);

    };

    QThreadPool threadPool;

    threadPool.setMaxThreadCount(numberOfThreadsToUse);

    QtConcurrent::blockingMap(&threadPool, MathUtilityFunctions::elementWiseDispatcher(vector, numberOfThreadsToUse), std::function<void(std::span<std::complex<double>> &span)>(mapFunctor));

    return vector;

}
template <typename T> QList<T> MathOperations::repeatVector(const QList<T> &vector, qsizetype n)
{

    QList<T> result;

    result.reserve(n * vector.count());

    while (n--)
        result.append(vector);

    return result;

}

template <typename T>
double MathOperations::crankInplace(QList<T> &vector)
{
    QList<T*> _vector(vector.size());

    for (qsizetype i = 0; i < vector.count(); ++i)
        _vector[i] = &vector[i];

    std::sort(_vector.begin(), _vector.end(), [](T *value1, T *value2) {return *value1 < *value2;});

    qsizetype j = 1;

    qsizetype ji;

    qsizetype jt;

    qsizetype n = _vector.size();

    double t;

    double rank;

    long double s = 0.0;

    while (j < n ) {

        if ((*_vector.at(j)) != (*_vector.at(j - 1))) {

            (*_vector.at(j - 1)) = j;

            ++j;

        } else {

            for (jt = j + 1; jt <= n && (*_vector.at(jt - 1)) == (*_vector.at(j - 1)); ++jt) ;

            rank = 0.5 * (j + jt - 1);

            for (ji = j; ji <= (jt - 1); ++ji)
                (*_vector[ji - 1]) = rank;

            t = jt - j;

            s += (t * t * t - t);

            j = jt;
        }

    }

    if (j == n)
        (*_vector[n - 1]) = n;

    return s;

}

template <typename T>
double MathOperations::crankInplace(std::span<T> &vector)
{
    QList<T*> _vector(vector.size());

    for (qsizetype i = 0; i < _vector.count(); ++i)
        _vector[i] = &vector[i];

    std::sort(_vector.begin(), _vector.end(), [](T *value1, T *value2) {return *value1 < *value2;});

    qsizetype j = 1;

    qsizetype ji;

    qsizetype jt;

    qsizetype n = _vector.size();

    double t;

    double rank;

    long double s = 0.0;

    while (j < n ) {

        if ((*_vector.at(j)) != (*_vector.at(j - 1))) {

            (*_vector.at(j - 1)) = j;

            ++j;

        } else {

            for (jt = j + 1; jt <= n && (*_vector.at(jt - 1)) == (*_vector.at(j - 1)); ++jt) ;

            rank = 0.5 * (j + jt - 1);

            for (ji = j; ji <= (jt - 1); ++ji)
                (*_vector[ji - 1]) = rank;

            t = jt - j;

            s += (t * t * t - t);

            j = jt;
        }

    }

    if (j == n)
        (*_vector[n - 1]) = n;

    return s;

}

template <typename T>
void MathOperations::rankInplace(QList<T> &vector)
{

    std::random_device rd;

    std::mt19937 g(rd());

    QList<T *> pointerCopyOfVector;

    pointerCopyOfVector.reserve(vector.count());

    for (qsizetype i = 0; i < vector.count(); ++i)
        pointerCopyOfVector.append(&vector[i]);

    std::shuffle(pointerCopyOfVector.begin(), pointerCopyOfVector.end(), g);

    std::sort(pointerCopyOfVector.begin(), pointerCopyOfVector.end(), [](const double *value1, const double *value2){return *value1 < *value2;});

    for (qsizetype i = 0; i < vector.count(); ++i)
        *pointerCopyOfVector[i] = i + 1;

}

template <typename T>
void MathOperations::rankInplace(std::span<T> &vector)
{

    std::random_device rd;

    std::mt19937 g(rd());

    QList<T *> pointerCopyOfVector;

    pointerCopyOfVector.reserve(vector.size());

    for (unsigned long i = 0; i < vector.size(); ++i)
        pointerCopyOfVector.append(&vector[i]);

    std::shuffle(pointerCopyOfVector.begin(), pointerCopyOfVector.end(), g);

    std::sort(pointerCopyOfVector.begin(), pointerCopyOfVector.end(), [](const double *value1, const double *value2){return *value1 < *value2;});

    for (qsizetype i = 0; i < pointerCopyOfVector.count(); ++i)
        *pointerCopyOfVector[i] = i + 1;

}

template <typename T>
double MathOperations::cox_box_transformation(const T &value, const double &lambda)
{

    if (lambda == 0)
        return std::log(value);

    return (std::pow(value, lambda) - 1.0) / lambda;

}

template <typename T>
void MathOperations::cox_box_transformation(QList<T> &vector, const double &lambda)
{

    if (almost_equal(lambda, 0.0, 2))
        std::transform(vector.begin(), vector.end(), vector.begin(), [](const T &value){return std::log(static_cast<double>(value));});
    else
        std::transform(vector.begin(), vector.end(), vector.begin(), [&lambda](const T &value){return (std::pow(static_cast<double>(value), lambda) - 1.0) / lambda;});

}

template <typename T>
void MathOperations::cox_box_transformation(std::span<T> &vector, const double &lambda)
{

    if (almost_equal(lambda, 0.0, 2))
        std::transform(vector.begin(), vector.end(), vector.begin(), [](const T &value){return std::log(static_cast<double>(value));});
    else
        std::transform(vector.begin(), vector.end(), vector.begin(), [&lambda](const T &value){return (std::pow(static_cast<double>(value), lambda) - 1.0) / lambda;});

}

template <typename T>
std::pair<double, double> MathOperations::cox_box_transformation(QList<T> &vector_sorted, bool &succes)
{

    auto functionP = [&vector_sorted](const double &lambda) {

        QList<T> copyOfVector(vector_sorted.begin(), vector_sorted.end());

        cox_box_transformation(copyOfVector, lambda);

        return boost::math::statistics::anderson_darling_normality_statistic(copyOfVector) / static_cast<double>(copyOfVector.size());

    };

    bool succes1;

    Brent brent1;

    brent1.bracket(-5, -4.0, functionP);

    double lambda1 = brent1.minimize(functionP, succes1);


    bool succes2;

    Brent brent2;

    brent2.bracket(5, 4.0, functionP);

    double lambda2 = brent2.minimize(functionP, succes2);

    if ((std::isnan(brent2.fmin)) || (brent1.fmin < brent2.fmin)) {

        cox_box_transformation(vector_sorted, lambda1);

        succes = succes1;

        return std::pair<double, double>(lambda1, brent1.fmin);

    } else {

        cox_box_transformation(vector_sorted, lambda2);

        succes = succes2;

        return std::pair<double, double>(lambda2, brent2.fmin);

    }

}

template <typename T>
std::pair<double, double> MathOperations::cox_box_transformation(std::span<T> &vector_sorted, bool &succes)
{

    auto functionP = [&vector_sorted](const double &lambda) {

        QList<T> copyOfVector(vector_sorted.begin(), vector_sorted.end());

        cox_box_transformation(copyOfVector, lambda);

        return boost::math::statistics::anderson_darling_normality_statistic(copyOfVector) / static_cast<double>(copyOfVector.size());

    };

    bool succes1;

    Brent brent1;

    brent1.bracket(-5, -4.0, functionP);

    double lambda1 = brent1.minimize(functionP, succes1);


    bool succes2;

    Brent brent2;

    brent2.bracket(5, 4.0, functionP);

    double lambda2 = brent2.minimize(functionP, succes2);

    if ((std::isnan(brent2.fmin)) || (brent1.fmin < brent2.fmin)) {

        cox_box_transformation(vector_sorted, lambda1);

        succes = succes1;

        return std::pair<double, double>(lambda1, brent1.fmin);

    } else {

        cox_box_transformation(vector_sorted, lambda2);

        succes = succes2;

        return std::pair<double, double>(lambda2, brent2.fmin);

    }

}

template <typename T> void MathOperations::medianCenteredForcedSymmetric(QList<T> &vector_sorted, double &value)
{
    if (vector_sorted.count() <= 1)
        return;

    long double integerPart;

    long double fractionalPart = std::modf(static_cast<double>(vector_sorted.size() - 1) * 0.5, &integerPart);

    double shiftFactor = static_cast<double>(vector_sorted[int(integerPart)]) + fractionalPart * (vector_sorted[int(integerPart) + 1] - vector_sorted[int(integerPart)]);

    for (double &value : vector_sorted)
        value -= shiftFactor;

    value -= shiftFactor;

    double div = static_cast<double>(vector_sorted.count() - 1) / 2.0;

    qsizetype leftIndex = std::floor(div);

    qsizetype rightIndex = std::ceil(div);

    qsizetype startIndex;

    if (leftIndex == rightIndex)
        startIndex = leftIndex - 1;
    else
        startIndex = leftIndex;

    for (qsizetype i = startIndex; i >=0; --i) {

        const double &valueLeft(vector_sorted.at(i));

        double &valueRight(vector_sorted[vector_sorted.count() - 1 - i]);

        double correctionFactor = (valueRight / std::abs(valueLeft));

        valueRight /= correctionFactor;

        if (value >= valueRight)
            value /= correctionFactor;

    }

}

template <typename T> void MathOperations::medianCenteredForcedSymmetric(std::span<T> &vector_sorted, double &value)
{
    if (vector_sorted.size() <= 1)
        return;

    long double integerPart;

    long double fractionalPart = std::modf(static_cast<double>(vector_sorted.size() - 1) * 0.5, &integerPart);

    double shiftFactor = static_cast<double>(vector_sorted[int(integerPart)]) + fractionalPart * (vector_sorted[int(integerPart) + 1] - vector_sorted[int(integerPart)]);

    for (double &value : vector_sorted)
        value -= shiftFactor;

    value -= shiftFactor;

    double div = static_cast<double>(vector_sorted.size() - 1) / 2.0;

    qsizetype leftIndex = std::floor(div);

    qsizetype rightIndex = std::ceil(div);

    qsizetype startIndex;

    if (leftIndex == rightIndex)
        startIndex = leftIndex - 1;
    else
        startIndex = leftIndex;

    for (qsizetype i = startIndex; i >=0; --i) {

        double &valueLeft(vector_sorted[i]);

        double &valueRight(vector_sorted[vector_sorted.size() - 1 - i]);

        double correctionFactor = (valueRight / std::abs(valueLeft));

        valueRight /= correctionFactor;

        if (value >= valueRight)
            value /= correctionFactor;

    }

}

template <typename T> void MathOperations::mirrorSymmetrixMatrix(QList<T> &matrix, qsizetype leadingDimension)
{

    QList<std::pair<qsizetype, std::span<T>>> rowIndexes_rowSpans = MathUtilityFunctions::rowMajor_matrix1d_rowIndexes_rowSpans(matrix, leadingDimension);

    for (std::pair<qsizetype, std::span<double>> index_span : rowIndexes_rowSpans) {

        for (qsizetype i = index_span.first + 1; i < rowIndexes_rowSpans.count(); ++i) {

            matrix[i * leadingDimension + index_span.first] = index_span.second[i];

        }

    }

}

#endif // MATHOPERATIONS_H
