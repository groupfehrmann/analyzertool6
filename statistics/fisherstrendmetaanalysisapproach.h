#ifndef FISHERSTRENDMETAANALYSISAPPROACH_H
#define FISHERSTRENDMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/chi_squared.hpp"

#include "basemetaanalysisapproach.h"

class FishersTrendMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    FishersTrendMetaAnalysisApproach();

    FishersTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~FishersTrendMetaAnalysisApproach() {}

private:

    QList<double> d_statistics;

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // FISHERSTRENDMETAANALYSISAPPROACH_H
