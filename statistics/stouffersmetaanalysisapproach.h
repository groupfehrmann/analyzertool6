#ifndef STOUFFERSMETAANALYSISAPPROACH_H
#define STOUFFERSMETAANALYSISAPPROACH_H

#include <QList>

#include "boost/math/distributions/normal.hpp"

#include "basemetaanalysisapproach.h"

class StouffersMetaAnalysisApproach : public BaseMetaAnalysisApproach
{

public:

    StouffersMetaAnalysisApproach();

    StouffersMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes);

    virtual ~StouffersMetaAnalysisApproach() {}

private:

    QList<double> d_pValues;

    QList<qsizetype> d_sampleSizes;

    void calculateStatistic() override;

    void calculatePValue() override;

    void calculateTestDescriptiveValues() override;

    static QList<QString> d_testDescriptiveLabels;

};

#endif // STOUFFERSMETAANALYSISAPPROACH_H
