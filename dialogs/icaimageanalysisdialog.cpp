#include "icaimageanalysisdialog.h"

ICAImageAnalysisDialog::ICAImageAnalysisDialog(QWidget *parent) :
    BaseDialog(parent, "ICA image analysis", "icaimageanalysisdialog")
{

    BaseDialog::d_parameters = QSharedPointer<ICAImageAnalysisParameters>(new ICAImageAnalysisParameters);

    BaseDialog::initWidgets();

}

void ICAImageAnalysisDialog::initWidgets_()
{

    SelectDirectoryWidget *selectDirectoryWidget_directoryToScanForTrainingImages = new SelectDirectoryWidget(this, "selectDirectoryWidget_directoryToScanForTrainingImages");

    selectDirectoryWidget_directoryToScanForTrainingImages->label()->setText("select directory to scan for images (MANDATORY)");

    this->connect(selectDirectoryWidget_directoryToScanForTrainingImages, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->directoryToScanForTrainingImages = path;});

    selectDirectoryWidget_directoryToScanForTrainingImages->fileDialog()->setLabelText(QFileDialog::Accept, "Select");

    selectDirectoryWidget_directoryToScanForTrainingImages->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_directoryToScanForTrainingImages, 0, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_indexOfPageToReadFromMultiPageImage = new SpinBoxWidget(this, "spinBoxWidget_indexOfPageToReadFromMultiPageImage");

    this->connect(spinBoxWidget_indexOfPageToReadFromMultiPageImage->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->indexOfPageToReadFromMultiPageImage = value;});

    spinBoxWidget_indexOfPageToReadFromMultiPageImage->label()->setText("index of page to read from multi page image");

    spinBoxWidget_indexOfPageToReadFromMultiPageImage->spinBox()->setRange(0, std::numeric_limits<int>::max());

    spinBoxWidget_indexOfPageToReadFromMultiPageImage->spinBox()->setValue(0);

    spinBoxWidget_indexOfPageToReadFromMultiPageImage->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_indexOfPageToReadFromMultiPageImage->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_indexOfPageToReadFromMultiPageImage, 1, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_windowSize = new SpinBoxWidget(this, "spinBoxWidget_windowSize");

    this->connect(spinBoxWidget_windowSize->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->windowSize = value;});

    spinBoxWidget_windowSize->label()->setText("window size");

    spinBoxWidget_windowSize->spinBox()->setRange(2, std::numeric_limits<int>::max());

    spinBoxWidget_windowSize->spinBox()->setValue(100);

    spinBoxWidget_windowSize->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_windowSize->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_windowSize, 1, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_thresholdCumulativeExplainedVariance = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_thresholdCumulativeExplainedVariance");

    this->connect(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_cumulativeExplainedVariance = value;});

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->label()->setText("cumulative explained variance in whitening step");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setRange(0, 100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setSuffix("%");

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setValue(100);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_thresholdCumulativeExplainedVariance->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_thresholdCumulativeExplainedVariance, 2, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_thresholdMaximumNumberOfComponents = new SpinBoxWidget(this, "d_spinBoxWidget_thresholdMaximumNumberOfComponents");

    this->connect(spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_maximumNumberOfComponents = value;});

    spinBoxWidget_thresholdMaximumNumberOfComponents->label()->setText("maximum number of components in whitening step");

    spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setRange(0, 1000000000);

    spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setValue(100);

    spinBoxWidget_thresholdMaximumNumberOfComponents->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_thresholdMaximumNumberOfComponents->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_thresholdMaximumNumberOfComponents, 2, 1, 1, 1);


    ComboBoxWidget *comboBoxWidget_contrastFunction = new ComboBoxWidget(this, "comboBoxWidget_contrastFunction");

    this->connect(comboBoxWidget_contrastFunction->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_contrastFunction](){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->contrastFunction = comboBoxWidget_contrastFunction->comboBox()->currentText();});

    comboBoxWidget_contrastFunction->label()->setText("contrast function");

    comboBoxWidget_contrastFunction->comboBox()->addItems({"Log", "Kurtosis", "Sqrt"});

    comboBoxWidget_contrastFunction->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_contrastFunction, 3, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_epsilon = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_epsilon");

    this->connect(doubleSpinBoxWidget_epsilon->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->epsilon = value;});

    doubleSpinBoxWidget_epsilon->label()->setText("stopping criteria (epsilon)");

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setRange(0.0000000000000001, 1.0);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setValue(0.00000001);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setDecimals(16);

    doubleSpinBoxWidget_epsilon->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_epsilon->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_epsilon, 3, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_maximumNumberOfIterations = new SpinBoxWidget(this, "spinBoxWidget_maximumNumberOfIterations");

    this->connect(spinBoxWidget_maximumNumberOfIterations->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->maximumNumberOfIterations = value;});

    spinBoxWidget_maximumNumberOfIterations->label()->setText("maximum number of iterations");

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setRange(0, 10000);

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setValue(2000);

    spinBoxWidget_maximumNumberOfIterations->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_maximumNumberOfIterations->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_maximumNumberOfIterations, 4, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_mu = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_mu");

    this->connect(doubleSpinBoxWidget_mu->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->mu = value;});

    doubleSpinBoxWidget_mu->label()->setText("step size (mu)");

    doubleSpinBoxWidget_mu->doubleSpinBox()->setDecimals(4);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setRange(0.0001, 1.0);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setValue(0.75);

    doubleSpinBoxWidget_mu->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_mu->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_mu, 4, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfRunsToPerform = new SpinBoxWidget(this, "spinBoxWidget_numberOfRunsToPerform");

    this->connect(spinBoxWidget_numberOfRunsToPerform->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfRunsToPerform = value;});

    spinBoxWidget_numberOfRunsToPerform->label()->setText("number of runs to perform for consensus ICA");

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setRange(1, 1000);

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setValue(100);

    spinBoxWidget_numberOfRunsToPerform->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfRunsToPerform->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfRunsToPerform, 5, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_multiThreadMode = new ComboBoxWidget(this, "comboBoxWidget_multiThreadMode");

    this->connect(comboBoxWidget_multiThreadMode->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_multiThreadMode](){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->multiThreadMode = comboBoxWidget_multiThreadMode->comboBox()->currentText();});

    comboBoxWidget_multiThreadMode->label()->setText("multithread mode");

    comboBoxWidget_multiThreadMode->comboBox()->addItems({"perform runs simultaneously", "perform runs sequentially"});

    comboBoxWidget_multiThreadMode->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_multiThreadMode, 5, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->pearsonCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->label()->setText("pearson correlation threshold for clustering components from multiple runs");

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.25, 1.0);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.9);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_pearsonCorrelationThresholdForConsensus, 6, 0, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_credibilityIndexThreshold = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_credibilityIndexThreshold");

    this->connect(doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->threshold_credibilityIndex = value;});

    doubleSpinBoxWidget_credibilityIndexThreshold->label()->setText("threshold for credibility index");

    doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setRange(0.0, 1.0);

    doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setValue(0.1);

    doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_credibilityIndexThreshold->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_credibilityIndexThreshold->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_credibilityIndexThreshold, 6, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_xiCorrelationThresholdForConsensus = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_xiCorrelationThresholdForConsensus");

    this->connect(doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->xiCorrelationThresholdForConsensus = value;});

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->label()->setText("xi correlation threshold for filtering consensus independent components");

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->doubleSpinBox()->setRange(0.1, 1.0);

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->doubleSpinBox()->setValue(0.1);

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_xiCorrelationThresholdForConsensus->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_xiCorrelationThresholdForConsensus, 7, 0, 1, 1);


    SpinBoxWidget *spinBoxWidget_numberOfPermutationsForNullDistribution = new SpinBoxWidget(this, "spinBoxWidget_numberOfPermutationsForNullDistribution");

    this->connect(spinBoxWidget_numberOfPermutationsForNullDistribution->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfPermutationsForNullDistribution = value;});

    spinBoxWidget_numberOfPermutationsForNullDistribution->label()->setText("number of permutations for null distribution");

    spinBoxWidget_numberOfPermutationsForNullDistribution->spinBox()->setRange(0, 1000);

    spinBoxWidget_numberOfPermutationsForNullDistribution->spinBox()->setValue(10);

    spinBoxWidget_numberOfPermutationsForNullDistribution->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfPermutationsForNullDistribution->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfPermutationsForNullDistribution, 7, 1, 1, 1);


    SpinBoxWidget *spinBoxWidget_librarySize = new SpinBoxWidget(this, "spinBoxWidget_librarySize");

    this->connect(spinBoxWidget_librarySize->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->maximumLibrarySize = value;});

    spinBoxWidget_librarySize->label()->setText("maximum library size");

    spinBoxWidget_librarySize->spinBox()->setRange(1, 10000);

    spinBoxWidget_librarySize->spinBox()->setValue(1000);

    spinBoxWidget_librarySize->spinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    spinBoxWidget_numberOfRunsToPerform->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_librarySize, 8, 0, 1, 1);


    ComboBoxWidget *comboBoxWidget_reconstructTrainingImagesWithLibrary = new ComboBoxWidget(this, "comboBoxWidget_reconstructTrainingImagesWithLibrary");

    this->connect(comboBoxWidget_reconstructTrainingImagesWithLibrary->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_reconstructTrainingImagesWithLibrary](){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->reconstructTrainingImagesWithLibrary = comboBoxWidget_reconstructTrainingImagesWithLibrary->comboBox()->currentData().toBool();});

    comboBoxWidget_reconstructTrainingImagesWithLibrary->label()->setText("reconstruct training images with library");

    comboBoxWidget_reconstructTrainingImagesWithLibrary->comboBox()->addItem("enable", true);

    comboBoxWidget_reconstructTrainingImagesWithLibrary->comboBox()->addItem("disable", false);

    comboBoxWidget_reconstructTrainingImagesWithLibrary->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_reconstructTrainingImagesWithLibrary, 8, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary");

    this->connect(doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->xiCorrelationLowerThresholdForLibrary = value;});

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->label()->setText("xi correlation lower threshold for matching to library");

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->doubleSpinBox()->setRange(0.0, 1.0);

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->doubleSpinBox()->setValue(0.1);

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_xiCorrelationLowerThresholdForLibrary, 9, 0, 1, 1);



    DoubleSpinBoxWidget *doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary");

    this->connect(doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->xiCorrelationUpperThresholdForLibrary = value;});

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->label()->setText("xi correlation upper threshold for matching to library");

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->doubleSpinBox()->setRange(0.00, 1.0);

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->doubleSpinBox()->setValue(0.9);

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->doubleSpinBox()->setDecimals(2);

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_xiCorrelationUpperThresholdForLibrary, 9, 1, 1, 1);


    DoubleSpinBoxWidget *doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction = new DoubleSpinBoxWidget(this, "doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction");

    this->connect(doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->doubleSpinBox(), &QDoubleSpinBox::valueChanged, [this](double value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->frequencyThresholdForLibraryReconstruction = value;});

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->label()->setText("frequency threshold for library reconstruction");

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->doubleSpinBox()->setRange(0.00, 1.0);

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->doubleSpinBox()->setValue(0.05);

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->doubleSpinBox()->setDecimals(4);

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->doubleSpinBox()->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

    doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction->readWidgetSettings();

    d_gridLayout->addWidget(doubleSpinBoxWidget_frequencyThresholdForLibraryReconstruction, 10, 0, 1, 1);


    SelectDirectoryWidget *selectDirectoryWidget_directoryToScanForTestingImages = new SelectDirectoryWidget(this, "selectDirectoryWidget_directoryToScanForTestingImages");

    selectDirectoryWidget_directoryToScanForTestingImages->label()->setText("select directory to scan for testing images (OPTIONAL)");

    this->connect(selectDirectoryWidget_directoryToScanForTestingImages, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->directoryToScanForTestingImages = path;});

    selectDirectoryWidget_directoryToScanForTestingImages->fileDialog()->setLabelText(QFileDialog::Accept, "Select");

    selectDirectoryWidget_directoryToScanForTestingImages->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_directoryToScanForTestingImages, 11, 0, 1, 2);



    SelectDirectoryWidget *selectDirectoryWidget_exportDirectory = new SelectDirectoryWidget(this, "selectDirectoryWidget_exportDirectory");

    selectDirectoryWidget_exportDirectory->label()->setText("select export directory (MANDATORY)");

    this->connect(selectDirectoryWidget_exportDirectory, &SelectDirectoryWidget::pathChanged, [this](const QString &path){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->exportDirectory = path;});

    selectDirectoryWidget_exportDirectory->fileDialog()->setLabelText(QFileDialog::Accept, "Export");

    selectDirectoryWidget_exportDirectory->readWidgetSettings();

    d_gridLayout->addWidget(selectDirectoryWidget_exportDirectory, 12, 0, 1, 2);


    SpinBoxWidget *spinBoxWidget_numberOfThreadsToUse = new SpinBoxWidget(this, "spinBoxWidget_numberOfThreadsToUse");

    this->connect(spinBoxWidget_numberOfThreadsToUse->spinBox(), &QSpinBox::valueChanged, [this](int value){static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->numberOfThreadsToUse = value;});

    spinBoxWidget_numberOfThreadsToUse->label()->setText("number of threads to use");

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setRange(1, QThreadPool::globalInstance()->maxThreadCount());

    spinBoxWidget_numberOfThreadsToUse->spinBox()->setValue(QThreadPool::globalInstance()->maxThreadCount() - QThreadPool::globalInstance()->activeThreadCount());

    spinBoxWidget_numberOfThreadsToUse->readWidgetSettings();

    d_gridLayout->addWidget(spinBoxWidget_numberOfThreadsToUse, 13, 0, 1, 2);


    ItemSelectorWidget *itemSelectorWidget_resultItems = new ItemSelectorWidget(this, "itemSelectorWidget_resultItems");

    this->connect(itemSelectorWidget_resultItems->itemSelectorModel_selected(), &ItemSelectorModel::selectionChanged, [this](const QList<QString> &itemLabels, const QList<qsizetype> &itemIndexes){Q_UNUSED(itemIndexes); static_cast<ICAImageAnalysisParameters *>(BaseDialog::d_parameters.data())->selectedResultItems = itemLabels;});

    itemSelectorWidget_resultItems->label()->setText("select result items to export");

    itemSelectorWidget_resultItems->itemSelectorModel_selected()->setItemLabelsAndIndexes({"image - original", "image - image windows", "image - vector of image windows", "image - fourier transformeded vector of image windows", "image - whitening: covariance matrix" ,  "image - whitening: eigenvalues", "image - whitening: eigenvectors", "image - whitening: whitening matrix", "image - whitening: dewhitening matrix", "image - whitening: whitened fourier transformed vector of image windows", "image - whitening: whitened image windows", "image - whitening: reconstructed image with whitened fourier transformed vector of image windows", "whitening summary for all training images", "image - NC cICA: independent image windows (per run)", "image - NC cICA: mixing matrix (per run)", "image - NC cICA: reconstructed image with independent fourier transformed vector of image windows (per run)", "image - NC cICA: independent image windows (consensus)", "image - NC cICA: mixing matrix (consensus)", "image - NC cICA: credibility index of independent image windows (consensus)", "image - NC cICA: reconstructed image with ALL independent fourier transformed vector of image windows (consensus)", "image - NC cICA: reconstructed image FOR EACH independent fourier transformed vector of image windows (consensus)", "image - NC cICA: reconstruction quality heatmap (consensus)", "library - library size tracker", "library - features", "library - library feature frequencies"});

    itemSelectorWidget_resultItems->readWidgetSettings();

    d_gridLayout->addWidget(itemSelectorWidget_resultItems, 14, 0, 1, 2);

}
