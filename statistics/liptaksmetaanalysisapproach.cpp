#include "liptaksmetaanalysisapproach.h"

QList<QString> LiptaksMetaAnalysisApproach::d_testDescriptiveLabels = {"Z", "Sig. (2-tailed)"};

LiptaksMetaAnalysisApproach::LiptaksMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

LiptaksMetaAnalysisApproach::LiptaksMetaAnalysisApproach(const QList<double> &pValues, const QList<qsizetype> &sampleSizes) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr),
    d_pValues(pValues),
    d_sampleSizes(sampleSizes)
{

}

void LiptaksMetaAnalysisApproach::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0.0;

    double w = 0.0;

    for (qsizetype i = 0; i < d_pValues.count(); ++i) {

        double sampleSize = static_cast<double>(d_sampleSizes.at(i));

        d_statistic += -1.0 * boost::math::quantile(dist, d_pValues.at(i) / 2.0) * std::sqrt(sampleSize);

        w += sampleSize;

    }


    d_statistic /= std::sqrt(w);

}

void LiptaksMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void LiptaksMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_pValue;

}
