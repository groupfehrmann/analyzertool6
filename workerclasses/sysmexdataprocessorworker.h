#ifndef SYSMEXDATAPROCESSORWORKER_H
#define SYSMEXDATAPROCESSORWORKER_H

#include <QObject>
#include <QList>
#include <QString>
#include <QUuid>
#include <QApplication>
#include <QDateTime>

#ifdef Q_OS_MAC
#include <cblas.h>
#include <lapacke.h>
#endif

#ifdef Q_OS_LINUX
#include <cblas.h>
#include <lapacke.h>
#endif

#include <algorithm>
#include <cmath>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "math/mathoperations.h"
#include "math/fastdistancecorrelation.h"
#include "math/mathdescriptives.h"

class SysmexDataProcessorParameters : public BaseParameters
{

public:

    SysmexDataProcessorParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> data_patientInfo;

    Qt::Orientation orientation_data_patientInfo;

    QList<qsizetype> selectedVariableIndexes_data_patientInfo;

    QList<QString> selectedVariableIdentifiers_data_patientInfo;

    QString patientInfo_variableLabelDefiningColumnWithUniquePatientId;

    qsizetype patientInfo_variableIndexDefiningColumnWithUniquePatientId;

    QString patientInfo_variableLabelDefiningColumnWithSysmexId;

    qsizetype patientInfo_variableIndexDefiningColumnWithSysmexId;

    QSharedPointer<BaseAnnotatedMatrix> data_sysmexInfo;

    Qt::Orientation orientation_data_sysmexInfo;

    QList<qsizetype> selectedVariableIndexes_data_sysmexInfo;

    QList<QString> selectedVariableIdentifiers_data_sysmexInfo;

    QString sysmexInfo_variableLabelDefiningColumnWithSysmexId;

    qsizetype sysmexInfo_variableIndexDefiningColumnWithSysmexId;

    QString sysmexInfo_variableLabelDefiningColumnWithDate;

    qsizetype sysmexInfo_variableIndexDefiningColumnWithDate;

    QString sysmexInfo_variableLabelDefiningColumnWithTime;

    qsizetype sysmexInfo_variableIndexDefiningColumnWithTime;

    QString exportDirectory;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class SysmexDataProcessorWorker : public BaseWorker
{

public:

    SysmexDataProcessorWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

#endif // SYSMEXDATAPROCESSORWORKER_H
