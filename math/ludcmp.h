#ifndef LUDCMP_H
#define LUDCMP_H

#include <QList>

#include <cmath>

struct LUdcmp
{
    qsizetype n;

    QList<QList<double>> lu;

    QList<qsizetype> indx;

    double d;

    QList<QList<double>> &aref;

    LUdcmp(QList<QList<double>> &a);

    void solve(QList<double> &b, QList<double> &x);

    void solve(QList<QList<double>> &b, QList<QList<double>> &x);

    void inverse(QList<QList<double>> &ainv);

    double det();

    void mprove(QList<double> &b, QList<double> &x);
};

#endif
