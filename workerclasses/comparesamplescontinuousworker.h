#ifndef COMPARESAMPLESCONTINUOUSWORKER_H
#define COMPARESAMPLESCONTINUOUSWORKER_H

#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>
#include <QHash>
#include <QSharedPointer>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/result.h"
#include "statistics/basestatisticaltest.h"
#include "statistics/studentttest.h"
#include "statistics/welchttest.h"
#include "statistics/mannwhitneyutest.h"
#include "statistics/kolmogorovsmirnovtest.h"
#include "statistics/leveneftest.h"
#include "statistics/bartlettchisquaredtest.h"
#include "statistics/kruskalwallischisquaredtest.h"
#include "statistics/dependentttest.h"
#include "statistics/fishersmetaanalysisapproach.h"
#include "statistics/fisherstrendmetaanalysisapproach.h"
#include "statistics/stouffersmetaanalysisapproach.h"
#include "statistics/stoufferstrendmetaanalysisapproach.h"
#include "statistics/liptaksmetaanalysisapproach.h"
#include "statistics/liptakstrendmetaanalysisapproach.h"
#include "statistics/lancastersmetaanalysisapproach.h"
#include "statistics/lancasterstrendmetaanalysisapproach.h"
#include "statistics/genericinversemethodwithfixedeffectmodelmetaanalysisapproach.h"
#include "statistics/genericinversemethodwithrandomeffectmodelmetaanalysisapproach.h"
#include "math/mathdescriptives.h"

class CompareSamplesContinuousParameters : public BaseParameters
{

public:

    CompareSamplesContinuousParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    QString exportDirectory;

    Qt::Orientation orientation;

    QList<qsizetype> selectedItemIndexes;

    QList<QString> selectedItemIdentifiers;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    QString annotationLabelDefiningSamples;

    qsizetype annotationIndexDefiningSamples;

    QString annotationLabelDefiningPairs;

    qsizetype annotationIndexDefiningPairs;

    QList<QString> selectedSampleIdentifiers;

    QString annotationLabelDefiningItemSubsets;

    qsizetype annotationIndexDefiningItemSubsets;

    QList<QString> selectedItemSubsetIdentifiers;

    QList<QString> annotationLabelsForVariables;

    int numberOfThreadsToUse;

    bool enablePermutationMode;

    qsizetype numberOfPermutations;

    double falseDiscoveryRate;

    double confidenceLevel;

    QString statisticalTest;

    QSharedPointer<BaseStatisticalTest> baseStatisticalTest;

    QSharedPointer<BaseMetaAnalysisApproach> baseMetaAnalysisApproach;

    QString metaAnalysisApproach;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class CompareSamplesContinuousWorker : public BaseWorker
{

public:

    CompareSamplesContinuousWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

    void apply_permutation(QList<qsizetype> &list, QList<qsizetype> indices);

};

template<typename T>
void CompareSamplesContinuousWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<CompareSamplesContinuousParameters> parameters(d_data->d_parameters.dynamicCast<CompareSamplesContinuousParameters>());


    // create sample identifier to coded sample identifier mapping
    ///////////////////

    QHash<QString, qsizetype> sampleIdentifierToCodedSampleIdentifier;

    for (const QString &sampleIdentifier : parameters->selectedSampleIdentifiers)
        sampleIdentifierToCodedSampleIdentifier.insert(sampleIdentifier, sampleIdentifierToCodedSampleIdentifier.count());


    // create item subset identifier to coded item subset identifier mapping
    ///////////////////

    QHash<QString, qsizetype> itemSubsetIdentifierToCodedItemSubsetIdentifier;

    if (parameters->annotationIndexDefiningItemSubsets == -1) {

        parameters->selectedItemSubsetIdentifiers.append(QString());

        itemSubsetIdentifierToCodedItemSubsetIdentifier.insert(QString(), 0);

    } else {

        for (const QString &itemSubsetIdentifier : parameters->selectedItemSubsetIdentifiers)
            itemSubsetIdentifierToCodedItemSubsetIdentifier.insert(itemSubsetIdentifier, itemSubsetIdentifierToCodedItemSubsetIdentifier.count());

    }

    // create pair identifier to coded item subset identifier mapping
    ///////////////////

    QHash<QString, qsizetype> pairIdentifierToCodedPairIdentifier;

    QList<QString> pairIdentifiers;

    if (parameters->annotationIndexDefiningPairs != -1) {

        for (qsizetype itemIndex : parameters->selectedItemIndexes) {

            QString pairIdentifier = annotatedMatrix->annotationValue(itemIndex, parameters->annotationIndexDefiningPairs, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString();

            if (!pairIdentifierToCodedPairIdentifier.contains(pairIdentifier)) {

                pairIdentifierToCodedPairIdentifier.insert(pairIdentifier, pairIdentifierToCodedPairIdentifier.count());

                pairIdentifiers.append(pairIdentifier);

            }
        }
    }

    ///////////////////

    // organizing items in correct subset and sample
    ///////////////////

    QUuid uuidProgress = this->startProgress("mapping items to subset and sample", 0, parameters->selectedItemIndexes.count());

    QList<QList<qsizetype>> itemSubsetToIndexes(parameters->selectedItemSubsetIdentifiers.count());

    QList<QList<qsizetype>> itemSubsetToCodedSampleIdentifiers(parameters->selectedItemSubsetIdentifiers.count());

    QList<QList<qsizetype>> itemSubsetToCodedPairIdentifiers(parameters->selectedItemSubsetIdentifiers.count());

    for (qsizetype itemIndex : parameters->selectedItemIndexes) {

        qsizetype codedSampleIdentifier = sampleIdentifierToCodedSampleIdentifier.value(annotatedMatrix->annotationValue(itemIndex, parameters->annotationIndexDefiningSamples, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString(), -1);

        qsizetype codedItemSubsetIdentifier = (parameters->annotationIndexDefiningItemSubsets == -1) ? 0 : itemSubsetIdentifierToCodedItemSubsetIdentifier.value(annotatedMatrix->annotationValue(itemIndex, parameters->annotationIndexDefiningItemSubsets, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString(), -1);

        if ((codedSampleIdentifier != -1) && (codedItemSubsetIdentifier != -1)) {

            itemSubsetToIndexes[codedItemSubsetIdentifier].append(itemIndex);

            itemSubsetToCodedSampleIdentifiers[codedItemSubsetIdentifier].append(codedSampleIdentifier);

            if (parameters->annotationIndexDefiningPairs != -1)
                itemSubsetToCodedPairIdentifiers[codedItemSubsetIdentifier].append(pairIdentifierToCodedPairIdentifier.value(annotatedMatrix->annotationValue(itemIndex, parameters->annotationIndexDefiningPairs, BaseAnnotatedMatrix::switchOrientation(parameters->orientation)).toString(), -1));

        }

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    }

    this->stopProgress(uuidProgress);


    // checking if mapping of items to subset and sample is valid for selected test
    ///////////////////

    uuidProgress = this->startProgress("checking if mapping of items to subset and sample is valid for selected test", 0, 0);

    for (qsizetype i = itemSubsetToCodedSampleIdentifiers.count() - 1; i >= 0; --i) {

        bool removeItemSubset = false;

        const QList<qsizetype> &codedSampleIdentifiers(itemSubsetToCodedSampleIdentifiers.at(i));

        for (qsizetype j = 0; j < parameters->selectedSampleIdentifiers.count(); ++j) {

            qsizetype count = codedSampleIdentifiers.count(j);

            if (count < parameters->baseStatisticalTest->minimumNumberOfItemsPerSample()) {

                if (parameters->selectedItemSubsetIdentifiers.count() == 1) {

                    this->reportError("not enough items (" + QString::number(count) + ") in sample \"" + parameters->selectedSampleIdentifiers.at(j) + "\"");

                    this->requestInterruption();

                    return;

                } else {

                    this->reportWarning("not enough items (" + QString::number(count) + ") in sample \"" + parameters->selectedSampleIdentifiers.at(j) + "\" for item subset \"" + parameters->selectedItemSubsetIdentifiers.at(i) + "\", therefore removing this item subset from analysis");

                    removeItemSubset = true;

                }

            }

        }

        if (parameters->annotationIndexDefiningPairs != -1) {

            const QList<qsizetype> &codedPairIdentifiers(itemSubsetToCodedPairIdentifiers.at(i));

            for (qsizetype j = 0; j < pairIdentifierToCodedPairIdentifier.count(); ++j) {

                qsizetype count = codedPairIdentifiers.count(j);

                if (count < parameters->selectedSampleIdentifiers.count()) {

                    if (parameters->selectedItemSubsetIdentifiers.count() == 1) {

                        this->reportError("detected " + QString::number(count) + " + pair identifier \"" + pairIdentifiers.at(j) + "\", expected " + QString::number(parameters->selectedSampleIdentifiers.count()) + "");

                        this->requestInterruption();

                        return;

                    } else {

                        this->reportWarning("detected " + QString::number(count) + " + pair identifier \"" + pairIdentifiers.at(j) + "\", expected " + QString::number(parameters->selectedSampleIdentifiers.count()) + " for item subset \"" + parameters->selectedItemSubsetIdentifiers.at(i) + "\", therefore removing this item subset from analysis");

                        removeItemSubset = true;

                    }

                }

            }

        }

        if (removeItemSubset) {

            itemSubsetToCodedSampleIdentifiers.removeAt(i);

            itemSubsetToCodedPairIdentifiers.removeAt(i);

            itemSubsetToIndexes.removeAt(i);

            parameters->selectedItemSubsetIdentifiers.removeAt(i);

            if (parameters->selectedItemSubsetIdentifiers.count() == 1) {

                this->reportError("only one item subsets \"" + parameters->selectedItemSubsetIdentifiers.at(i) + "\" remained after filtering out item subsets that meet the requirements for the selected test, meta-analysis approach is no longer allowed.");

                this->requestInterruption();

                return;

            }

        }

    }

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    // fetching data
    ///////////////////

    uuidProgress = this->startProgress("fetching data", 0, parameters->selectedVariableIndexes.count());

    QList<std::pair<qsizetype, QList<QList<double>>>> data;

    for (qsizetype selectedVariableIndex : parameters->selectedVariableIndexes) {

        QList<QList<double>> temp;

        for (const QList<qsizetype> &indexes : itemSubsetToIndexes) {

            if (parameters->orientation == Qt::Vertical)
                temp.append(annotatedMatrix->template sliced<double>({selectedVariableIndex}, indexes));
            else
                temp.append(annotatedMatrix->template sliced<double>(indexes, {selectedVariableIndex}));

        }

        data.append(std::pair<qsizetype, QList<QList<double>>>(data.count(), temp));

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgressWithOne(uuidProgress);

    }

    this->stopProgress(uuidProgress);


    // initialize statistical test object
    ///////////////////

    std::function<QSharedPointer<BaseStatisticalTest>(const QList<double> &, const QList<qsizetype> &, const QList<qsizetype> &)> createStatisticalTestObject;

    if (parameters->statisticalTest == "Student T")
        createStatisticalTestObject = [](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new StudentTTest(list, codedSampleIdentifiers));};
    else if (parameters->statisticalTest == "Welch T")
        createStatisticalTestObject = [](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new WelchTTest(list, codedSampleIdentifiers));};
    else if (parameters->statisticalTest == "Mann-Whitney U")
        createStatisticalTestObject = [](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new MannWhitneyUTest(list, codedSampleIdentifiers));};
    else if (parameters->statisticalTest == "Kolmogorov-Smirnov Z")
        createStatisticalTestObject = [](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new KolmogorovSmirnovTest(list, codedSampleIdentifiers));};
    else if (parameters->statisticalTest == "Levene F")
        createStatisticalTestObject = [&sampleIdentifierToCodedSampleIdentifier](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new LeveneFTest(list, codedSampleIdentifiers, sampleIdentifierToCodedSampleIdentifier.count()));};
    else if (parameters->statisticalTest == "Bartlett Chi-squared")
        createStatisticalTestObject = [&sampleIdentifierToCodedSampleIdentifier](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new BartlettChiSquaredTest(list, codedSampleIdentifiers, sampleIdentifierToCodedSampleIdentifier.count()));};
    else if (parameters->statisticalTest == "Kruskal-Wallis Chi-squared")
        createStatisticalTestObject = [&sampleIdentifierToCodedSampleIdentifier](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {Q_UNUSED(codedPairIdentifiers); return QSharedPointer<BaseStatisticalTest>(new KruskalWallisChiSquaredTest(list, codedSampleIdentifiers, sampleIdentifierToCodedSampleIdentifier.count()));};
    else if (parameters->statisticalTest == "Dependent T")
        createStatisticalTestObject = [](const QList<double> &list, const QList<qsizetype> &codedSampleIdentifiers, const QList<qsizetype> &codedPairIdentifiers) {return QSharedPointer<BaseStatisticalTest>(new DependentTTest(list, codedSampleIdentifiers, codedPairIdentifiers));};


    // initialize meta analysis approach object
    ///////////////////

    std::function<QSharedPointer<BaseMetaAnalysisApproach>(const QList<QSharedPointer<BaseStatisticalTest>> &)> createMetaAnalysisApproachObject;

    QList<qsizetype> sampleSizes;

    for (const QList<qsizetype> &indexes : itemSubsetToIndexes)
        sampleSizes.append(indexes.count());

    if (parameters->metaAnalysisApproach == "Fisher's method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) pValues.append(statisticalTestObject->pValue()); return QSharedPointer<BaseMetaAnalysisApproach>(new FishersMetaAnalysisApproach(pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Fisher's trend method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> statistics; QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {statistics.append(statisticalTestObject->statistic()); pValues.append(statisticalTestObject->pValue());} return QSharedPointer<BaseMetaAnalysisApproach>(new FishersTrendMetaAnalysisApproach(statistics, pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Stouffer's method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) pValues.append(statisticalTestObject->pValue()); return QSharedPointer<BaseMetaAnalysisApproach>(new StouffersMetaAnalysisApproach(pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Stouffer's trend method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> statistics; QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {statistics.append(statisticalTestObject->statistic()); pValues.append(statisticalTestObject->pValue());} return QSharedPointer<BaseMetaAnalysisApproach>(new StouffersTrendMetaAnalysisApproach(statistics, pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Liptak's method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) pValues.append(statisticalTestObject->pValue()); return QSharedPointer<BaseMetaAnalysisApproach>(new LiptaksMetaAnalysisApproach(pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Liptak's trend method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> statistics; QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {statistics.append(statisticalTestObject->statistic()); pValues.append(statisticalTestObject->pValue());} return QSharedPointer<BaseMetaAnalysisApproach>(new LiptaksTrendMetaAnalysisApproach(statistics, pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Lancaster's method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) pValues.append(statisticalTestObject->pValue()); return QSharedPointer<BaseMetaAnalysisApproach>(new LancastersMetaAnalysisApproach(pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Lancaster's trend method")
        createMetaAnalysisApproachObject = [&sampleSizes](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> statistics; QList<double> pValues; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {statistics.append(statisticalTestObject->statistic()); pValues.append(statisticalTestObject->pValue());} return QSharedPointer<BaseMetaAnalysisApproach>(new LancastersTrendMetaAnalysisApproach(statistics, pValues, sampleSizes));};
    else if (parameters->metaAnalysisApproach == "Generic inverse method with fixed effect model")
        createMetaAnalysisApproachObject = [](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> effectSizes; QList<double> standardErrorOfEfectSizes; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {effectSizes.append(statisticalTestObject->effectSize()); standardErrorOfEfectSizes.append(statisticalTestObject->standardErrorOfEffectSize());} return QSharedPointer<BaseMetaAnalysisApproach>(new GenericInverseMethodWithFixedEffectModelMetaAnalysisApproach(effectSizes, standardErrorOfEfectSizes, statisticalTestObjects.first()->backtransformEffectSizeFunction()));};
    else if (parameters->metaAnalysisApproach == "Generic inverse method with random effect model")
        createMetaAnalysisApproachObject = [](const QList<QSharedPointer<BaseStatisticalTest>> &statisticalTestObjects) {QList<double> effectSizes; QList<double> standardErrorOfEfectSizes; for (const QSharedPointer<BaseStatisticalTest> &statisticalTestObject : statisticalTestObjects) {effectSizes.append(statisticalTestObject->effectSize()); standardErrorOfEfectSizes.append(statisticalTestObject->standardErrorOfEffectSize());} return QSharedPointer<BaseMetaAnalysisApproach>(new GenericInverseMethodWithRandomEffectModelMetaAnalysisApproach(effectSizes, standardErrorOfEfectSizes, statisticalTestObjects.first()->backtransformEffectSizeFunction()));};


    // performing permutations
    ///////////////////

    QList<double> itemSubsetToMultivariatePermutationThreshold;

    QList<QList<double>> itemSubsetToPermutationPValues((parameters->selectedItemSubsetIdentifiers.count() > 1) ? parameters->selectedItemSubsetIdentifiers.count() + 1 : 1, QList<double>(parameters->selectedVariableIndexes.count(), 0.0));

    if (parameters->enablePermutationMode) {

        QList<QList<qsizetype>> itemSubsetToCodedSampleIdentifiersForShuffling = itemSubsetToCodedSampleIdentifiers;

        QList<QList<qsizetype>> itemSubsetToCodedPairIdentifiersForShuffling = itemSubsetToCodedPairIdentifiers;

        QList<QList<qsizetype>> itemSubsetToIndexesForShuffling;

        for (qsizetype i = 0; i < itemSubsetToCodedSampleIdentifiersForShuffling.count(); ++i) {

            itemSubsetToIndexesForShuffling.append(QList<qsizetype>(itemSubsetToCodedSampleIdentifiersForShuffling.count()));

            std::iota(std::begin(itemSubsetToIndexesForShuffling[i]), std::end(itemSubsetToIndexesForShuffling[i]), 0);

        }

        std::function<QList<double>(const std::pair<qsizetype, QList<QList<double>>> &)> mapFunctor = [&](const std::pair<qsizetype, QList<QList<double>>> &data) {

            QList<QSharedPointer<BaseStatisticalTest>> statisticalTestObjects;

            QList<double> pValues;

            if (this->thread()->isInterruptionRequested())
                return pValues;

            this->checkIfPauseWasRequested();

            for (qsizetype i = 0; i < data.second.count(); ++i) {

                statisticalTestObjects.append(createStatisticalTestObject(data.second.at(i), itemSubsetToCodedSampleIdentifiersForShuffling.at(i), itemSubsetToCodedPairIdentifiersForShuffling.at(i)));

                pValues.append(statisticalTestObjects.last()->pValue());

            }

            if (data.second.count() > 1)
                pValues.append(createMetaAnalysisApproachObject(statisticalTestObjects)->pValue());

            this->updateProgressWithOne(uuidProgress);

            return pValues;

        };

        auto reduceFunctor = [](QList<QList<double>> &result, const QList<double> &intermediateResult) {

            result.resize(intermediateResult.count());

            for (qsizetype i = 0; i < result.count(); ++i)
                result[i].append(intermediateResult.at(i));

        };


        uuidProgress = this->startProgress("calculating observed p-values", 0, parameters->selectedVariableIndexes.count());

        QList<QList<double>> itemSubsetToObservedPValues = QtConcurrent::blockingMappedReduced<QList<QList<double>>>(&d_threadPool, data, mapFunctor, std::function<void(QList<QList<double>> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

        if (QThread::currentThread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->stopProgress(uuidProgress);


        QUuid uuidProgress_1 = this->startProgress("performing permutations", 0, parameters->numberOfPermutations);

        std::random_device rd;

        std::mt19937 g(rd());

        QList<QList<double>> itemSubsetToMultivariatePermutationDistribution((parameters->selectedItemSubsetIdentifiers.count() > 1) ? parameters->selectedItemSubsetIdentifiers.count() + 1 : 1);

        for (qsizetype i = 0; i < parameters->numberOfPermutations; ++i) {

            for (qsizetype j = 0; j < itemSubsetToIndexesForShuffling.count(); ++j) {

                QList<qsizetype> &indexesForShuffling(itemSubsetToIndexesForShuffling[j]);

                QList<qsizetype> &codedSampleIdentifiersForShuffling(itemSubsetToCodedSampleIdentifiersForShuffling[j]);

                QList<qsizetype> &codedParIdentifiersForShuffling(itemSubsetToCodedPairIdentifiersForShuffling[j]);


                std::shuffle(indexesForShuffling.begin(), indexesForShuffling.end(), g);


                this->apply_permutation(codedSampleIdentifiersForShuffling, indexesForShuffling);

                if (parameters->annotationIndexDefiningPairs != -1)
                    this->apply_permutation(codedParIdentifiersForShuffling, indexesForShuffling);


                while (codedSampleIdentifiersForShuffling == itemSubsetToCodedSampleIdentifiers.at(j)) {

                    std::shuffle(indexesForShuffling.begin(), indexesForShuffling.end(), g);

                    this->apply_permutation(codedSampleIdentifiersForShuffling, indexesForShuffling);

                    if (parameters->annotationIndexDefiningPairs != -1)
                        this->apply_permutation(codedParIdentifiersForShuffling, indexesForShuffling);

                }

            }

            uuidProgress = this->startProgress("calculating p-values for permutation round " + QString::number(i + 1), 0, parameters->selectedVariableIndexes.count());

            QList<QList<double>> itemSubsetToPermutationPValuesInCurrentRound = QtConcurrent::blockingMappedReduced<QList<QList<double>>>(&d_threadPool, data, mapFunctor, std::function<void(QList<QList<double>> &, const QList<double> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

            if (QThread::currentThread()->isInterruptionRequested())
                return;

            this->checkIfPauseWasRequested();

            this->stopProgress(uuidProgress);


            uuidProgress = this->startProgress("obtaining mpv thresholds for permutation round " + QString::number(i + 1), 0, itemSubsetToPermutationPValuesInCurrentRound.count());

            for (qsizetype j = 0; j < itemSubsetToObservedPValues.count(); ++j) {

                QList<std::pair<double, bool>> combinedPValues;

                combinedPValues.reserve(2 * parameters->selectedVariableIndexes.count());

                const QList<double> &observedPValues(itemSubsetToObservedPValues.at(j));

                const QList<double> &permutationPValuesInCurrentRound(itemSubsetToPermutationPValuesInCurrentRound.at(j));

                QList<double> &permutationPValues(itemSubsetToPermutationPValues[j]);

                QList<double> &multivariatePermutationDistribution(itemSubsetToMultivariatePermutationDistribution[j]);


                for (qsizetype k = 0; k < parameters->selectedVariableIndexes.count(); ++k) {

                    const double &observedPValue(observedPValues.at(k));

                    const double &permutationRoundPValue(permutationPValuesInCurrentRound.at(k));

                    combinedPValues.append(std::pair<double, bool>(observedPValue, true));

                    combinedPValues.append(std::pair<double, bool>(permutationRoundPValue, false));

                    if (permutationRoundPValue <= observedPValue)
                        permutationPValues[k] += 1.0;

                }

                std::sort(combinedPValues.begin(), combinedPValues.end(), [](const std::pair<double, bool> &value1, const std::pair<double, bool> & value2){return value1.first < value2.first;});

                qsizetype countEnrichement = 0;

                qsizetype countPermutation = 0;

                for (const std::pair<double, bool> &value : combinedPValues) {

                    if (value.second)
                        ++countEnrichement;
                    else
                        ++countPermutation;

                    if ((static_cast<double>(countPermutation) / static_cast<double>(countEnrichement)) > parameters->falseDiscoveryRate) {

                        multivariatePermutationDistribution.append(value.first);

                        break;

                    }

                }

                if (QThread::currentThread()->isInterruptionRequested())
                    return;

                this->checkIfPauseWasRequested();

                this->updateProgress(uuidProgress, j + 1);

            }

            this->stopProgress(uuidProgress);

            this->updateProgress(uuidProgress_1, i + 1);

        }

        for (QList<double> &multivariatePermutationDistribution : itemSubsetToMultivariatePermutationDistribution) {

            std::sort(multivariatePermutationDistribution.begin(), multivariatePermutationDistribution.end(), std::greater<double>());

            itemSubsetToMultivariatePermutationThreshold.append(MathDescriptives::percentileOfPresortedList(multivariatePermutationDistribution, parameters->confidenceLevel));

        }

        this->stopProgress(uuidProgress_1);

    }

    std::function<QList<QList<QVariant>>(const std::pair<qsizetype, QList<QList<double>>> &)> mapFunctor = [&](const std::pair<qsizetype, QList<QList<double>>> &data) {

        QList<QSharedPointer<BaseStatisticalTest>> statisticalTestObjects;

        QList<QList<QVariant>> itemSubsetToResults;

        if (this->thread()->isInterruptionRequested())
            return itemSubsetToResults;

        this->checkIfPauseWasRequested();

        for (qsizetype i = 0; i < data.second.count(); ++i) {

            statisticalTestObjects.append(createStatisticalTestObject(data.second.at(i), itemSubsetToCodedSampleIdentifiers.at(i), itemSubsetToCodedPairIdentifiers.at(i)));

            QList<QVariant> temp;

            for (const QString &annotationLabelForVariable: parameters->annotationLabelsForVariables)
                temp.append(parameters->baseAnnotatedMatrix->annotationValue(parameters->selectedVariableIdentifiers.at(data.first), annotationLabelForVariable, parameters->orientation));

            for (const double &testDescriptiveValue : statisticalTestObjects.last()->testDescriptiveValues())
                temp.append(testDescriptiveValue);

            itemSubsetToResults.append(temp);

            if (parameters->enablePermutationMode)
                itemSubsetToResults.last() << static_cast<double>(itemSubsetToPermutationPValues.at(i).at(data.first)) / static_cast<double>(parameters->numberOfPermutations) << ((statisticalTestObjects.last()->pValue() < itemSubsetToMultivariatePermutationThreshold.at(i)) ? 1.0 : 0.0);

        }

        if (data.second.count() > 1) {

            QSharedPointer<BaseMetaAnalysisApproach> metaAnalysisApproachObject = createMetaAnalysisApproachObject(statisticalTestObjects);

            QList<QVariant> temp;

            for (const QString &annotationLabelForVariable: parameters->annotationLabelsForVariables)
                temp.append(parameters->baseAnnotatedMatrix->annotationValue(parameters->selectedVariableIdentifiers.at(data.first), annotationLabelForVariable, parameters->orientation));

            for (const double &testDescriptiveValue : metaAnalysisApproachObject->testDescriptiveValues())
                temp.append(testDescriptiveValue);

            itemSubsetToResults.append(temp);

            if (parameters->enablePermutationMode)
                itemSubsetToResults.last() << static_cast<double>(itemSubsetToPermutationPValues.at(data.second.count()).at(data.first)) / static_cast<double>(parameters->numberOfPermutations) << ((metaAnalysisApproachObject->pValue() < itemSubsetToMultivariatePermutationThreshold.at(data.second.count())) ? 1.0 : 0.0);

        }

        this->updateProgressWithOne(uuidProgress);

        return itemSubsetToResults;

    };

    auto reduceFunctor = [](QList<QList<QVariant>> &result, const QList<QList<QVariant>> &intermediateResult) {

        result.resize(intermediateResult.count());

        for (qsizetype i = 0; i < result.count(); ++i)
            result[i].append(intermediateResult.at(i));

    };

    uuidProgress = this->startProgress("performing statistical tests", 0, parameters->selectedVariableIndexes.count());

    QList<QList<QVariant>> itemSubsetToTestDescriptiveValues = QtConcurrent::blockingMappedReduced<QList<QList<QVariant>>>(&d_threadPool, data, mapFunctor, std::function<void(QList<QList<QVariant>> &, const QList<QList<QVariant>> &)>(reduceFunctor), QtConcurrent::OrderedReduce | QtConcurrent::SequentialReduce);

    if (QThread::currentThread()->isInterruptionRequested())
        return;

    this->checkIfPauseWasRequested();

    this->stopProgress(uuidProgress);


    QList<QString> columnLabels = parameters->annotationLabelsForVariables;

    columnLabels.append(parameters->baseStatisticalTest->testDescriptiveLabels());

    if (parameters->enablePermutationMode)
        columnLabels << "permutation p-value" << "MVP reject (FDR = " + QString::number(parameters->falseDiscoveryRate * 100.0) + "%; CL = " + QString::number(parameters->confidenceLevel * 100.0) + "%)";

    for (qsizetype i = 0; i < parameters->selectedItemSubsetIdentifiers.count(); ++i) {

        AnnotatedMatrix<QVariant> *result_testDescriptiveValues = new AnnotatedMatrix<QVariant>(parameters->selectedVariableIdentifiers, columnLabels, itemSubsetToTestDescriptiveValues.at(i));

        if (parameters->exportDirectory.isEmpty()) {

            if (parameters->selectedItemSubsetIdentifiers.count() == 1)
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("\"" + parameters->statisticalTest + "\" results", result_testDescriptiveValues));
            else
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("\"" + parameters->statisticalTest + "\" results for subset \"" + parameters->selectedItemSubsetIdentifiers.at(i) + "\"", result_testDescriptiveValues));

        } else {

            QSharedPointer<AnnotatedMatrix<QVariant>> sharedPointerToAnnotatedMatrix(result_testDescriptiveValues);

            if (parameters->selectedItemSubsetIdentifiers.count() == 1)
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, QString(parameters->statisticalTest + " results").replace(" ", "_"));
            else
                this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, QString(parameters->statisticalTest + " results for subset " + parameters->selectedItemSubsetIdentifiers.at(i)).replace(" ", "_"));

        }

    }

    if (parameters->selectedItemSubsetIdentifiers.count() > 1) {

        columnLabels = parameters->annotationLabelsForVariables;

        columnLabels.append(parameters->baseMetaAnalysisApproach->testDescriptiveLabels());

        if (parameters->enablePermutationMode)
            columnLabels << "permutation p-value" << "MVP reject (FDR = " + QString::number(parameters->falseDiscoveryRate * 100.0) + "%; CL = " + QString::number(parameters->confidenceLevel * 100.0) + "%)";

        AnnotatedMatrix<QVariant> *result_metaAnalysisDescriptiveValues = new AnnotatedMatrix<QVariant>(parameters->selectedVariableIdentifiers, columnLabels, itemSubsetToTestDescriptiveValues.last());

        if (parameters->exportDirectory.isEmpty())
                emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("\"" + parameters->metaAnalysisApproach + "\" results", result_metaAnalysisDescriptiveValues));

        else {

            QSharedPointer<AnnotatedMatrix<QVariant>> sharedPointerToAnnotatedMatrix(result_metaAnalysisDescriptiveValues);

            this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, QString(parameters->metaAnalysisApproach + " results").replace(" ", "_"));

        }

    }

}

#endif // COMPARESAMPLESCONTINUOUSWORKER_H
