#ifndef RANDOMSELECTWORKER_H
#define RANDOMSELECTWORKER_H


#include <QObject>
#include <QString>
#include <QUuid>
#include <QApplication>

#include "baseworker.h"
#include "baseparameters.h"
#include "containers/annotatedmatrix.h"
#include "containers/matrix.h"
#include "containers/result.h"


class RandomSelectParameters : public BaseParameters
{

public:

    RandomSelectParameters(QObject *parent = nullptr);

    QSharedPointer<BaseAnnotatedMatrix> baseAnnotatedMatrix;

    Qt::Orientation orientation;

    QList<qsizetype> selectedVariableIndexes;

    QList<QString> selectedVariableIdentifiers;

    double proportionToSelect;

    QString exportDirectory;

private:

    bool isValid_();

    QString prettyPrint_() const;

};

class RandomSelectWorker : public BaseWorker
{

public:

    RandomSelectWorker(QObject *parent = nullptr, const QSharedPointer<BaseParameters> &parameters = nullptr);

private:

    void doWork_() override;

    template<typename T> void doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix);

    bool performReadWriteLockLogistics_() override;

    void performReadWriteUnlockLogistics_() override;

};

template<typename T>
void RandomSelectWorker::doWork_(QSharedPointer<AnnotatedMatrix<T>> annotatedMatrix)
{

    QSharedPointer<RandomSelectParameters> parameters(d_data->d_parameters.dynamicCast<RandomSelectParameters>());

    QList<qsizetype> indexes = parameters->selectedVariableIndexes;

    QList<qsizetype> selectedIndexes;

    QList<qsizetype> nonSelectedIndexes;

    QList<QString> selectedIdentifiers;

    QList<QString> nonSelectedIdentifiers;


    std::random_device rd;

    std::mt19937 g(rd());

    std::shuffle(indexes.begin(), indexes.end(), g);

    qsizetype index = std::floor(static_cast<double>(indexes.count()) * parameters->proportionToSelect);


    QUuid uuidProgress = this->startProgress("random selecting", 0, indexes.count());

    for (qsizetype i = 0; i < indexes.count(); ++i) {

        qsizetype currentIndex = indexes.at(i);

        if (i <= index) {

            annotatedMatrix->setIdentifierSelectionStatus(currentIndex, true, parameters->orientation);

            selectedIndexes.append(currentIndex);

            selectedIdentifiers.append(annotatedMatrix->identifier(currentIndex, parameters->orientation));

        } else {

            annotatedMatrix->setIdentifierSelectionStatus(currentIndex, false, parameters->orientation);

            nonSelectedIndexes.append(currentIndex);

            nonSelectedIdentifiers.append(annotatedMatrix->identifier(currentIndex, parameters->orientation));

        }

        if (this->thread()->isInterruptionRequested())
            return;

        this->checkIfPauseWasRequested();

        this->updateProgress(uuidProgress, i + 1);

    }

    this->stopProgress(uuidProgress);

    AnnotatedMatrix<qsizetype> *result_selected = new AnnotatedMatrix<qsizetype>(selectedIdentifiers, {"index"}, selectedIndexes);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("selected", result_selected));
    else {

        QSharedPointer<AnnotatedMatrix<qsizetype>> sharedPointerToAnnotatedMatrix(result_selected);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "selected");

    }

    AnnotatedMatrix<qsizetype> *result_nonSelected = new AnnotatedMatrix<qsizetype>(nonSelectedIdentifiers, {"index"}, nonSelectedIndexes);

    if (parameters->exportDirectory.isEmpty())
        emit objectAvailable(parameters->baseAnnotatedMatrix->property("uuidProject").toUuid(), new Result("non selected", result_nonSelected));
    else {

        QSharedPointer<AnnotatedMatrix<qsizetype>> sharedPointerToAnnotatedMatrix(result_nonSelected);

        this->exportToFile(sharedPointerToAnnotatedMatrix, parameters->exportDirectory, "non_selected");

    }

}

#endif // RANDOMSELECTWORKER_H
