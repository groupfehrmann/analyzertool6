#include "stoufferstrendmetaanalysisapproach.h"

QList<QString> StouffersTrendMetaAnalysisApproach::d_testDescriptiveLabels = {"Z", "Sig. (2-tailed)"};

StouffersTrendMetaAnalysisApproach::StouffersTrendMetaAnalysisApproach() :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr)
{

}

StouffersTrendMetaAnalysisApproach::StouffersTrendMetaAnalysisApproach(const QList<double> &statistics, const QList<double> &pValues, const QList<qsizetype> &sampleSizes) :
    BaseMetaAnalysisApproach(d_testDescriptiveLabels, nullptr),
    d_statistics(statistics),
    d_pValues(pValues),
    d_sampleSizes(sampleSizes)
{

}

void StouffersTrendMetaAnalysisApproach::calculateStatistic()
{

    boost::math::normal dist(0.0, 1.0);

    d_statistic = 0;

    for (qsizetype i = 0; i < d_statistics.count(); ++i) {

        if (d_statistics.at(i) < 0.0)
             d_statistic -= -1.0 *boost::math::quantile(dist, d_pValues.at(i) / 2.0);
        else
            d_statistic += -1.0 *boost::math::quantile(dist, d_pValues.at(i) / 2.0);

    }

    d_statistic /= std::sqrt(d_pValues.count());

}

void StouffersTrendMetaAnalysisApproach::calculatePValue()
{

    this->calculateStatistic();

    if (std::isnan(d_statistic)) {

        d_pValue = std::numeric_limits<double>::quiet_NaN();

        return;

    }

    boost::math::normal dist(0.0, 1.0);

    d_pValue = 2.0 * boost::math::cdf(boost::math::complement(dist, std::fabs(d_statistic)));

    if (d_pValue == 0)
        d_pValue = std::numeric_limits<double>::min();
    else if (d_pValue == 1.0)
        d_pValue -= std::numeric_limits<double>::epsilon();

}

void StouffersTrendMetaAnalysisApproach::calculateTestDescriptiveValues()
{

    this->calculateStatistic();

    this->calculatePValue();

    d_testDescriptiveValues << d_statistic << d_pValue;

}
