#include "datafromasinglefiledialog.h"

DataFromASingleFileDialog::DataFromASingleFileDialog(QWidget *parent) :
    BaseDialog(parent, "data from a single file", "datafromasinglefiledialog")
{

    BaseDialog::d_parameters = QSharedPointer<DataFromASingleFileParameters>(new DataFromASingleFileParameters);

    BaseDialog::initWidgets();

}

void DataFromASingleFileDialog::fileSelectionChanged(const QString &path)
{

    d_comboBoxWidget_headerLine->comboBox()->clear();

    d_bufferLinesHeadOfFile.clear();

    d_bufferLinesHeadOfFileTrimmed.clear();

    d_bufferLinesTailOfFile.clear();

    d_bufferLinesTailOfFileTrimmed.clear();

    QFile file(path);

    if (path.isEmpty() || !file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    int nBufferLinesHeadOfFile = d_spinBoxWidget_nBufferLinesHeadOfFile->spinBox()->value();

    int nBufferLinesTailOfFile = d_spinBoxWidget_nBufferLinesTailOfFile->spinBox()->value();

    int lineCounter = 0;

    QList<qint64> filePositions;

    while (!file.atEnd()) {

        if (++lineCounter > nBufferLinesHeadOfFile)
            break;

        filePositions << file.pos();

        d_bufferLinesHeadOfFile << QString(file.readLine()).remove(QRegularExpression("[\r\n]"));

        if (d_bufferLinesHeadOfFile.last().size() > 150)
            d_bufferLinesHeadOfFileTrimmed << d_bufferLinesHeadOfFile.last().left(75).simplified() + " ...]----[... " + d_bufferLinesHeadOfFile.last().right(75).simplified();
        else
            d_bufferLinesHeadOfFileTrimmed << d_bufferLinesHeadOfFile.last().simplified();

    }

    while (!file.atEnd()) {

        filePositions << file.pos();

        file.readLine();

    }

    d_offSetLineNumber = filePositions.size() - nBufferLinesTailOfFile + 1;

    if (d_offSetLineNumber < 1)
        d_offSetLineNumber = 1;

    file.seek(filePositions[d_offSetLineNumber - 1]);

    while (!file.atEnd()) {

        d_bufferLinesTailOfFile << QString(file.readLine()).remove(QRegularExpression("[\r\n]"));

        if (d_bufferLinesTailOfFile.last().size() > 150)
            d_bufferLinesTailOfFileTrimmed << d_bufferLinesTailOfFile.last().left(75).simplified() + " ...]----[... " + d_bufferLinesTailOfFile.last().right(75).simplified();
        else
            d_bufferLinesTailOfFileTrimmed << d_bufferLinesTailOfFile.last().simplified();

    }

    file.close();

    d_comboBoxWidget_headerLine->comboBox()->addItem("NO HEADER LINE");

    d_comboBoxWidget_headerLine->comboBox()->addItems(d_bufferLinesHeadOfFileTrimmed);

    if (d_comboBoxWidget_headerLine->comboBox()->count() > 1)
        d_comboBoxWidget_headerLine->comboBox()->setCurrentIndex(1);

}

void DataFromASingleFileDialog::firstDataLineChanged(int index)
{

    d_comboBoxWidget_lastDataLine->comboBox()->clear();

    if (index == -1) {

        d_lineNumberOfFirstDataLine = -1;

        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfFirstDataLine = d_lineNumberOfFirstDataLine;

        return;

    }

    if (d_lineNumberOfHeaderLine == -1) {

        d_lineNumberOfFirstDataLine = index + 1;

        QList<QString> tokens;

        tokens << d_bufferLinesHeadOfFile.at(index).split(QRegularExpression(d_selectDelimiterWidget->comboBox()->currentData().toString()), Qt::KeepEmptyParts);

        qsizetype numberOfTokens = tokens.count();

        tokens.clear();

        for (qsizetype i = 0; i < numberOfTokens; ++i)
            tokens.append("column " + QString::number(i + 1));

        this->updateHeaderLabels(tokens);

    } else
        d_lineNumberOfFirstDataLine = d_lineNumberOfHeaderLine + index + 1;

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfFirstDataLine = d_lineNumberOfFirstDataLine;

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_bufferLinesTailOfFile.size())
        d_comboBoxWidget_lastDataLine->comboBox()->addItems(d_bufferLinesTailOfFileTrimmed.mid(d_lineNumberOfFirstDataLine - 1, 1));
    else
        d_comboBoxWidget_lastDataLine->comboBox()->addItems(d_bufferLinesTailOfFileTrimmed.mid(((lineNumberStart <= 0) ? 0 : lineNumberStart + 1)));

    d_comboBoxWidget_lastDataLine->comboBox()->setCurrentIndex(d_comboBoxWidget_lastDataLine->comboBox()->count() - 1);

}

void DataFromASingleFileDialog::headerLineChanged(int index)
{

    if (index == -1) {

        d_lineNumberOfHeaderLine = -1;

        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfHeader = d_lineNumberOfHeaderLine;

        d_comboBoxWidget_firstDataLine->comboBox()->clear();

        this->updateHeaderLabels(QList<QString>());

        return;

    }

    if (index == 0)
        d_lineNumberOfHeaderLine = -1;
    else {

        d_lineNumberOfHeaderLine = index;

        QList<QString> tokens;

        if (!d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox()->currentData().toBool())
            tokens.append("..........");

        tokens << d_bufferLinesHeadOfFile.at(index - 1).split(QRegularExpression(d_selectDelimiterWidget->comboBox()->currentData().toString()), Qt::KeepEmptyParts);

        this->updateHeaderLabels(tokens);

    }

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfHeader = d_lineNumberOfHeaderLine;

    d_comboBoxWidget_firstDataLine->comboBox()->clear();

    d_comboBoxWidget_firstDataLine->comboBox()->addItems(d_bufferLinesHeadOfFileTrimmed.mid(index));

}

void DataFromASingleFileDialog::lastDataLineChanged(int index)
{

    if (index == -1) {

        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfLastDataLine = -1;

        return;

    }

    int lineNumberStart = d_lineNumberOfFirstDataLine - d_offSetLineNumber;

    if (lineNumberStart + 1 == d_bufferLinesTailOfFile.size())
        d_lineNumberOfLastDataLine = d_lineNumberOfFirstDataLine;
    else
        d_lineNumberOfLastDataLine = (lineNumberStart <= 0) ? d_offSetLineNumber + index : d_lineNumberOfFirstDataLine + index + 1;

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->lineNumberOfLastDataLine = d_lineNumberOfLastDataLine;

}

void DataFromASingleFileDialog::initWidgets_()
{

    ComboBoxWidget *comboBoxWidget_dataType = new ComboBoxWidget(this, "comboBoxWidget_dataType");

    this->connect(comboBoxWidget_dataType->comboBox(), &QComboBox::currentIndexChanged, [this, comboBoxWidget_dataType](){static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->typeName = comboBoxWidget_dataType->comboBox()->currentText();});

    comboBoxWidget_dataType->label()->setText("select data type");

    comboBoxWidget_dataType->comboBox()->addItems({"short", "unsigned short", "int","unsigned int", "long long", "unsigned long long", "float", "double", "string", "unsigned char"});

    comboBoxWidget_dataType->comboBox()->setCurrentIndex(7);

    comboBoxWidget_dataType->readWidgetSettings();

    d_gridLayout->addWidget(comboBoxWidget_dataType, 0, 0, 1, 6);


    SelectFileWidget *selectFileWidget = new SelectFileWidget(this, "selectFileWidget");

    selectFileWidget->fileDialog()->setLabelText(QFileDialog::Accept, "Import");

    this->connect(selectFileWidget, &SelectFileWidget::pathChanged, [this](const QString &path){static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->pathToFile = path;});

    this->connect(selectFileWidget, &SelectFileWidget::pathChanged, this, &DataFromASingleFileDialog::fileSelectionChanged);

    this->connect(selectFileWidget, &SelectFileWidget::invalidPath, [this](){d_comboBoxWidget_headerLine->comboBox()->clear();});

    d_gridLayout->addWidget(selectFileWidget, 1, 0, 1, 6);


    d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers = new ComboBoxWidget(this, "d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers");

    this->connect(d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox(), &QComboBox::currentIndexChanged, [this](){static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->firstTokenInHeaderDefinesColumnWithRowIdentifiers = d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox()->currentData().toBool();});

    d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->label()->setText("first token in header defines column with row identifiers");

    d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox()->addItem("true", true);

    d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox()->addItem("false", false);

    d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->readWidgetSettings();

    d_gridLayout->addWidget(d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers, 2, 0, 1, 6);

    this->connect(d_comboBoxWidget_firstTokenInHeaderDefinesColumnWithRowIdentifiers->comboBox(), &QComboBox::currentIndexChanged, [this](){this->headerLineChanged(d_comboBoxWidget_headerLine->comboBox()->currentIndex());});


    d_spinBoxWidget_nBufferLinesHeadOfFile = new SpinBoxWidget(this, "d_spinBoxWidget_nBufferLinesHeadOfFile");

    d_spinBoxWidget_nBufferLinesHeadOfFile->label()->setText("number of lines to buffer in head of file");

    d_spinBoxWidget_nBufferLinesHeadOfFile->spinBox()->setRange(1, 1000000);

    d_spinBoxWidget_nBufferLinesHeadOfFile->spinBox()->setValue(100);

    d_spinBoxWidget_nBufferLinesHeadOfFile->readWidgetSettings();

    d_gridLayout->addWidget(d_spinBoxWidget_nBufferLinesHeadOfFile, 3, 0, 1, 3);


    d_spinBoxWidget_nBufferLinesTailOfFile = new SpinBoxWidget(this, "d_spinBoxWidget_nBufferLinesTailOfFile");

    d_spinBoxWidget_nBufferLinesTailOfFile->label()->setText("number of lines to buffer in tail of file");

    d_spinBoxWidget_nBufferLinesTailOfFile->spinBox()->setRange(1, 1000000);

    d_spinBoxWidget_nBufferLinesTailOfFile->spinBox()->setValue(100);

    d_spinBoxWidget_nBufferLinesTailOfFile->readWidgetSettings();

    d_gridLayout->addWidget(d_spinBoxWidget_nBufferLinesTailOfFile, 3, 3, 1, 3);


    d_comboBoxWidget_headerLine = new ComboBoxWidget(this, "d_comboBoxWidget_headerLine");

    this->connect(d_comboBoxWidget_headerLine->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::headerLineChanged);

    d_comboBoxWidget_headerLine->label()->setText("select header line");

    d_gridLayout->addWidget(d_comboBoxWidget_headerLine, 4, 0, 1, 6);


    d_comboBoxWidget_firstDataLine = new ComboBoxWidget(this, "d_comboBoxWidget_firstDataLine");

    this->connect(d_comboBoxWidget_firstDataLine->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::firstDataLineChanged);

    d_comboBoxWidget_firstDataLine->label()->setText("select first data line");

    d_gridLayout->addWidget(d_comboBoxWidget_firstDataLine, 5, 0, 1, 6);


    d_comboBoxWidget_lastDataLine = new ComboBoxWidget(this, "d_comboBoxWidget_lastDataLine");

    this->connect(d_comboBoxWidget_lastDataLine->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::lastDataLineChanged);

    d_comboBoxWidget_lastDataLine->label()->setText("select last data line");

    d_gridLayout->addWidget(d_comboBoxWidget_lastDataLine, 6, 0, 1, 6);


    d_selectDelimiterWidget = new SelectDelimiterWidget(this, "d_selectDelimiterWidget");

    this->connect(d_selectDelimiterWidget->comboBox(), &QComboBox::currentIndexChanged, [this](){static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->delimiter = d_selectDelimiterWidget->comboBox()->currentData().toString();});

    d_selectDelimiterWidget->readWidgetSettings();

    d_gridLayout->addWidget(d_selectDelimiterWidget, 7, 0, 1, 6);

    this->connect(d_selectDelimiterWidget, &SelectDelimiterWidget::delimiterChanged, [this](){static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->delimiter = d_selectDelimiterWidget->comboBox()->currentData().toString(); this->headerLineChanged(d_comboBoxWidget_headerLine->comboBox()->currentIndex());});

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->delimiter = d_selectDelimiterWidget->comboBox()->currentData().toString();


    d_comboBoxWidget_rowIdentifiersColumn = new ComboBoxWidget(this, "d_comboBoxWidget_rowIdentifiersColumn");

    this->connect(d_comboBoxWidget_rowIdentifiersColumn->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::comboBoxWidget_rowIdentifiersColumn_indexChanged);

    d_comboBoxWidget_rowIdentifiersColumn->label()->setText("select column with row identifiers");

    d_gridLayout->addWidget(d_comboBoxWidget_rowIdentifiersColumn, 8, 0, 1, 2);


    d_comboBoxWidget_firstDataColumn = new ComboBoxWidget(this, "d_comboBoxWidget_firstDataColumn");

    this->connect(d_comboBoxWidget_firstDataColumn->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::comboBoxWidget_firstDataColumn_indexChanged);

    d_comboBoxWidget_firstDataColumn->label()->setText("select first data column");

    d_gridLayout->addWidget(d_comboBoxWidget_firstDataColumn, 8, 2, 1, 2);


    d_comboBoxWidget_secondDataColumn = new ComboBoxWidget(this, "d_comboBoxWidget_secondDataColumn");

    this->connect(d_comboBoxWidget_secondDataColumn->comboBox(), &QComboBox::currentIndexChanged, this, &DataFromASingleFileDialog::comboBoxWidget_secondDataColumn_indexChanged);

    d_comboBoxWidget_secondDataColumn->label()->setText("select second data column");

    d_gridLayout->addWidget(d_comboBoxWidget_secondDataColumn, 8, 4, 1, 2);

}

void DataFromASingleFileDialog::updateHeaderLabels(const QList<QString> &tokens)
{

    d_tokensForHeaderLabels = tokens;

    d_comboBoxWidget_rowIdentifiersColumn->comboBox()->clear();

    if (tokens.isEmpty())
        return;

    d_comboBoxWidget_rowIdentifiersColumn->comboBox()->addItem("NO ROW IDENTIFIERS");

    d_comboBoxWidget_rowIdentifiersColumn->comboBox()->addItems(tokens);

    d_comboBoxWidget_rowIdentifiersColumn->comboBox()->setCurrentIndex(1);

}

void DataFromASingleFileDialog::comboBoxWidget_rowIdentifiersColumn_indexChanged(int index)
{

    d_comboBoxWidget_firstDataColumn->comboBox()->clear();

    if (index == -1) {

        d_indexOfColumnDefiningRowIdentifiers = -1;

        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningRowIdentifiers = d_indexOfColumnDefiningRowIdentifiers;

        return;

    } else if (index == 0)
        d_indexOfColumnDefiningRowIdentifiers = -1;
    else
        d_indexOfColumnDefiningRowIdentifiers = index - 1;

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningRowIdentifiers = d_indexOfColumnDefiningRowIdentifiers;

    d_comboBoxWidget_firstDataColumn->comboBox()->addItems(d_tokensForHeaderLabels.mid(d_indexOfColumnDefiningRowIdentifiers + 1));

}

void DataFromASingleFileDialog::comboBoxWidget_firstDataColumn_indexChanged(int index)
{

    d_comboBoxWidget_secondDataColumn->comboBox()->clear();

    if (index == -1) {

        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningFirstDataColumn = -1;

        return;

    }
    if (d_indexOfColumnDefiningRowIdentifiers == -1)
        d_indexOfColumnDefiningFirstDataColumn = index;
    else
        d_indexOfColumnDefiningFirstDataColumn = d_indexOfColumnDefiningRowIdentifiers + index + 1;

    static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningFirstDataColumn = d_indexOfColumnDefiningFirstDataColumn;

    d_comboBoxWidget_secondDataColumn->comboBox()->addItem(QString());

    d_comboBoxWidget_secondDataColumn->comboBox()->addItems(d_tokensForHeaderLabels.mid(d_indexOfColumnDefiningFirstDataColumn + 1));

    if (d_comboBoxWidget_secondDataColumn->comboBox()->count() > 1)
        d_comboBoxWidget_secondDataColumn->comboBox()->setCurrentIndex(1);

}

void DataFromASingleFileDialog::comboBoxWidget_secondDataColumn_indexChanged(int index)
{

    if (index <= 0)
        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningSecondDataColumn = -1;
    else
        static_cast<DataFromASingleFileParameters *>(BaseDialog::d_parameters.data())->indexOfColumnDefiningSecondDataColumn = d_indexOfColumnDefiningFirstDataColumn + index;

}
