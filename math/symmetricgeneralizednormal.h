#ifndef SYMMETRICGENERALIZEDNORMAL_H
#define SYMMETRICGENERALIZEDNORMAL_H

#include <span>

#include <boost/math/distributions/normal.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/digamma.hpp>
#include <boost/math/special_functions/trigamma.hpp>
#include <boost/math/statistics/univariate_statistics.hpp>
#include <boost/math/tools/roots.hpp>
#include "boost/math/tools/minima.hpp"

class SymmetricGeneralizedNormal
{

public:
    SymmetricGeneralizedNormal(const double &location, const double &scale, const double &shape);

    SymmetricGeneralizedNormal(const std::span<double> &data);

    double pdf(const double &x);

    double cdf(const double &x);

    inline const double &location() {return d_location;};

    inline const double &scale() {return d_scale;};

    inline const double &shape() {return d_shape;};

private:

    double d_location;

    double d_scale;

    double d_shape;

};

#endif // SYMMETRICGENERALIZEDNORMAL_H
