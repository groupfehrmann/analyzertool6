#ifndef SELECTDATAWITHTOKENSDIALOG_H
#define SELECTDATAWITHTOKENSDIALOG_H

#include "basedialog.h"

#include <QList>
#include <QString>
#include <QRegularExpression>

#include "basedialog.h"
#include "containers/annotatedmatrix.h"
#include "widgets/orientationwidget.h"
#include "widgets/itemselectorwidget.h"
#include "widgets/comboboxwidget.h"
#include "widgets/texteditwidget.h"
#include "widgets/selectdelimiterwidget.h"
#include "workerclasses/selectdatawithtokensworker.h"

class SelectDataWithTokensDialog : public BaseDialog
{

public:

    SelectDataWithTokensDialog(QSharedPointer<BaseAnnotatedMatrix> baseAnnotationMatrix, QWidget *parent = nullptr);

protected:

    void initWidgets_() override;

private:

    ItemSelectorWidget *d_itemSelectorWidget_identifiers;

    ItemSelectorWidget *d_itemSelectorWidget_labelToApplyOn;

    OrientationWidget *d_orientationWidget;

    TextEditWidget *d_textEditWidget_tokens;

    SelectDelimiterWidget *d_selectDelimiterWidget;

private slots:

    void orientationChanged(Qt::Orientation orientation);

    void dialogAccepted();

};

#endif // SELECTDATAWITHTOKENSDIALOG_H
