#ifndef SELECTDELIMITERWIDGET_H
#define SELECTDELIMITERWIDGET_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QCheckBox>

#include "dialogs/basedialog.h"


namespace Ui {

    class SelectDelimiterWidget;

}

class SelectDelimiterWidget : public QWidget
{

    Q_OBJECT

public:

    explicit SelectDelimiterWidget(BaseDialog *parent = nullptr, const QString &widgetSettingsIdentifier = QString());

    QComboBox *comboBox();

    QLabel *label();

    QLineEdit *lineEdit();

    void readWidgetSettings();

    void writeWidgetSettings();

    ~SelectDelimiterWidget();

private:

    Ui::SelectDelimiterWidget *ui;

    bool d_readWidgetSettings;

    QString d_widgetSettingsIdentifier;

signals:

    void delimiterChanged();

};

#endif // SELECTDELIMITERWIDGET_H
