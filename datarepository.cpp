#include "datarepository.h"

class DataRepositoryData : public QSharedData
{

public:

    QUuid d_currentUuid;

    QHash<QUuid, QSharedPointer<QObject>> d_uuidToObjectPointer;

    QHash<QUuid, QList<QUuid>> d_uuidProjectToUuidAnnotatedMatrices;

    QHash<QUuid, QList<QUuid>> d_uuidProjectToUuidResults;

    QList<QUuid> d_uuidProjects;

    QList<QString> d_projectLabels;

    QList<QUuid> d_emptyListOfQUuid;

    QUuid d_emptyUuid;

};

DataRepository::DataRepository(QObject *parent) :
    QObject(parent),
    d_data(new DataRepositoryData)
{

    d_data->d_currentUuid = QUuid();

}

QUuid DataRepository::addObject(const QUuid &uuidProject, QObject *object)
{

    d_readWriteLock.lockForWrite();

    QUuid uuidProject_;

    if (uuidProject == QUuid()) {

        if(d_data->d_uuidProjects.contains(d_data->d_currentUuid))
            uuidProject_ = d_data->d_currentUuid;
        else if (d_data->d_uuidToObjectPointer.contains(d_data->d_currentUuid))
            uuidProject_ = d_data->d_uuidToObjectPointer.find(d_data->d_currentUuid).value()->property("uuidProject").toUuid();
        else
        {
            if (object->property("uuidProject").isValid())
                uuidProject_ = object->property("uuidProject").toUuid();
            else
                uuidProject_ = QUuid::createUuid();

        }

    } else
        uuidProject_ = uuidProject;

    if(!d_data->d_uuidProjects.contains(uuidProject_)) {

        d_data->d_uuidProjects.append(uuidProject_);

        QString projectLabel = object->property("projectLabel").toString();

        if (projectLabel.isEmpty())
            d_data->d_projectLabels.append(QString("project ") + QString::number(d_data->d_projectLabels.count()));
        else
            d_data->d_projectLabels.append(projectLabel);

        d_data->d_uuidProjectToUuidAnnotatedMatrices.insert(uuidProject_, QList<QUuid>());

        d_data->d_uuidProjectToUuidResults.insert(uuidProject_, QList<QUuid>());

    }

    qsizetype indexOfUuidProject = d_data->d_uuidProjects.indexOf(uuidProject_);

    object->setProperty("uuidProject", uuidProject_);

    object->setProperty("projectLabel", d_data->d_projectLabels.at(indexOfUuidProject));

    QUuid uuidObject;

    if (!object->property("uuidObject").isValid()) {

        uuidObject = QUuid::createUuid();

        object->setProperty("uuidObject", uuidObject);

    } else {

        uuidObject = object->property("uuidObject").toUuid();

        if (d_data->d_uuidToObjectPointer.contains(uuidObject)) {

            uuidObject = QUuid::createUuid();

            object->setProperty("uuidObject", uuidObject);

        }

    }

    if (!object->property("label").isValid())
        object->setProperty("label", uuidObject.toString());

    d_data->d_uuidToObjectPointer.insert(uuidObject, QSharedPointer<QObject>(object));

    QString objectType = object->property("objectType").toString();

    if (objectType == "annotated matrix")
        d_data->d_uuidProjectToUuidAnnotatedMatrices[uuidProject_].append(uuidObject);
    else if (objectType == "result")
        d_data->d_uuidProjectToUuidResults[uuidProject_].append(uuidObject);

    d_readWriteLock.unlock();

    emit this->objectAdded(uuidObject, this->objectPointer(uuidObject));

    return uuidObject;

}

QUuid DataRepository::addSharedPointerToObject(const QUuid &uuidProject, QSharedPointer<QObject> object)
{

    d_readWriteLock.lockForWrite();

    QUuid uuidProject_;

    if (uuidProject == QUuid()) {

        if(d_data->d_uuidProjects.contains(d_data->d_currentUuid))
            uuidProject_ = d_data->d_currentUuid;
        else if (d_data->d_uuidToObjectPointer.contains(d_data->d_currentUuid))
            uuidProject_ = d_data->d_uuidToObjectPointer.find(d_data->d_currentUuid).value()->property("uuidProject").toUuid();
        else
        {
            if (object->property("uuidProject").isValid())
                uuidProject_ = object->property("uuidProject").toUuid();
            else
                uuidProject_ = QUuid::createUuid();

        }

    } else
        uuidProject_ = uuidProject;

    if(!d_data->d_uuidProjects.contains(uuidProject_)) {

        d_data->d_uuidProjects.append(uuidProject_);

        QString projectLabel = object->property("projectLabel").toString();

        if (projectLabel.isEmpty())
            d_data->d_projectLabels.append(QString("project ") + QString::number(d_data->d_projectLabels.count()));
        else
            d_data->d_projectLabels.append(projectLabel);

        d_data->d_uuidProjectToUuidAnnotatedMatrices.insert(uuidProject_, QList<QUuid>());

        d_data->d_uuidProjectToUuidResults.insert(uuidProject_, QList<QUuid>());

    }

    qsizetype indexOfUuidProject = d_data->d_uuidProjects.indexOf(uuidProject_);

    object->setProperty("uuidProject", uuidProject_);

    object->setProperty("projectLabel", d_data->d_projectLabels.at(indexOfUuidProject));

    QUuid uuidObject;

    if (!object->property("uuidObject").isValid()) {

        uuidObject = QUuid::createUuid();

        object->setProperty("uuidObject", uuidObject);

    } else {

        uuidObject = object->property("uuidObject").toUuid();

        if (d_data->d_uuidToObjectPointer.contains(uuidObject)) {

            uuidObject = QUuid::createUuid();

            object->setProperty("uuidObject", uuidObject);

        }

    }

    if (!object->property("label").isValid())
        object->setProperty("label", uuidObject.toString());

    d_data->d_uuidToObjectPointer.insert(uuidObject, object);

    QString objectType = object->property("objectType").toString();

    if (objectType == "annotated matrix")
        d_data->d_uuidProjectToUuidAnnotatedMatrices[uuidProject_].append(uuidObject);
    else if (objectType == "result")
        d_data->d_uuidProjectToUuidResults[uuidProject_].append(uuidObject);

    d_readWriteLock.unlock();

    emit this->objectAdded(uuidObject, this->objectPointer(uuidObject));

    return uuidObject;

}


QUuid DataRepository::addProject()
{

    QWriteLocker writeLocker(&d_readWriteLock);

    QUuid uuidProject = QUuid::createUuid();

    d_data->d_uuidProjects.append(uuidProject);

    d_data->d_projectLabels.append(QString("project ") + QString::number(d_data->d_projectLabels.count()));

    d_data->d_uuidProjectToUuidAnnotatedMatrices.insert(uuidProject, QList<QUuid>());

    d_data->d_uuidProjectToUuidResults.insert(uuidProject, QList<QUuid>());

    return uuidProject;

}

qsizetype DataRepository::indexOfUuidAnnotatedMatrix(const QUuid &uuidAnnotatedMatrix) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it1 = d_data->d_uuidToObjectPointer.find(uuidAnnotatedMatrix);

    if (it1 == d_data->d_uuidToObjectPointer.end())
        return -1;

    QUuid uuidProject = it1.value()->property("uuidProject").toUuid();

    QHash<QUuid, QList<QUuid>>::const_iterator it2 = d_data->d_uuidProjectToUuidAnnotatedMatrices.find(uuidProject);

    if (it2 == d_data->d_uuidProjectToUuidAnnotatedMatrices.end())
        return -1;
    else
        return it2.value().indexOf(uuidAnnotatedMatrix);

}

qsizetype DataRepository::indexOfUuidProject(const QUuid &uuidProject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidProjects.indexOf(uuidProject);

}

qsizetype DataRepository::indexOfProjectOfWhichUuidObjectIsMember(const QUuid &uuidObject) const
{

    return this->indexOfUuidProject(this->uuidProject(uuidObject));

}

qsizetype DataRepository::indexOfUuidResult(const QUuid &uuidResult) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it1 = d_data->d_uuidToObjectPointer.find(uuidResult);

    if (it1 == d_data->d_uuidToObjectPointer.end())
        return -1;

    QUuid uuidProject = it1.value()->property("uuidProject").toUuid();

    QHash<QUuid, QList<QUuid>>::const_iterator it2 = d_data->d_uuidProjectToUuidResults.find(uuidProject);

    if (it2 == d_data->d_uuidProjectToUuidResults.end())
        return -1;
    else
        return it2.value().indexOf(uuidResult);

}

bool DataRepository::isUuidAnnotatedMatrix(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it = d_data->d_uuidToObjectPointer.find(uuid);

    if (it == d_data->d_uuidToObjectPointer.end())
        return false;
    else if (it.value()->property("objectType").toString() == "annotated matrix")
        return true;
    else
        return false;

}

bool DataRepository::isUuidProject(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidProjects.contains(uuid);

}

bool DataRepository::isUuidResult(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it = d_data->d_uuidToObjectPointer.find(uuid);

    if (it == d_data->d_uuidToObjectPointer.end())
        return false;
    else if (it.value()->property("objectType").toString() == "result")
        return true;
    else
        return false;

}

bool DataRepository::isValidUuid(const QUuid &uuid) const
{

    if (this->isUuidProject(uuid))
        return true;

    if (this->isUuidAnnotatedMatrix(uuid))
        return true;

    if (this->isUuidResult(uuid))
        return true;

    return false;

}

QString DataRepository::label(const QUuid &uuid) const
{

    QReadLocker readLocker(&d_readWriteLock);

    qsizetype index = d_data->d_uuidProjects.indexOf(uuid);

    if (index != -1)
        return d_data->d_projectLabels.at(index);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it = d_data->d_uuidToObjectPointer.find(uuid);

    if (it == d_data->d_uuidToObjectPointer.end())
        return QString();
    else
        return it.value()->property("label").toString();

}

qsizetype DataRepository::numberOfAnnotatedMatrices(const QUuid &uuidProject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QList<QUuid>>::const_iterator it = d_data->d_uuidProjectToUuidAnnotatedMatrices.find(uuidProject);

    if (it == d_data->d_uuidProjectToUuidAnnotatedMatrices.end())
        return 0;
    else
        return it->count();

}

qsizetype DataRepository::numberOfResults(const QUuid &uuidProject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QList<QUuid>>::const_iterator it = d_data->d_uuidProjectToUuidResults.find(uuidProject);

    if (it == d_data->d_uuidProjectToUuidResults.end())
        return 0;
    else
        return it->count();

}

const QString &DataRepository::projectLabel(qsizetype index) const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_projectLabels.at(index);

}

QReadWriteLock &DataRepository::readWriteLock()
{

    return d_readWriteLock;

}

QSharedPointer<QObject> DataRepository::objectPointer(const QUuid &uuidObject)
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::iterator it = d_data->d_uuidToObjectPointer.find(uuidObject);

    if (it == d_data->d_uuidToObjectPointer.end())
        return QSharedPointer<QObject>();
    else
        return it.value();

}

QList<QSharedPointer<QObject>> DataRepository::objectPointers(const QUuid &uuidProject)
{

    QReadLocker readLocker(&d_readWriteLock);

    if (!this->isUuidProject(uuidProject))
        return QList<QSharedPointer<QObject>>();

    QList<QSharedPointer<QObject>> objectPointers_;

    for (const QUuid &uuid : this->uuidsAnnotatedMatrix(uuidProject))
        objectPointers_.append(this->objectPointer(uuid));

    for (const QUuid &uuid : this->uuidsResult(uuidProject))
        objectPointers_.append(this->objectPointer(uuid));

    return objectPointers_;

}

void DataRepository::removeAll()
{

    QWriteLocker writeLocker(&d_readWriteLock);

    d_data->d_currentUuid = QUuid();

    d_data->d_uuidToObjectPointer.clear();

    d_data->d_uuidProjectToUuidAnnotatedMatrices.clear();

    d_data->d_uuidProjectToUuidResults.clear();

    d_data->d_projectLabels.clear();

    d_data->d_uuidProjects.clear();

}


void DataRepository::removeAllDataAssociatedWithUuid(const QUuid &uuid)
{

    d_readWriteLock.lockForWrite();

    bool emitCurrentUuidChanged = false;

    bool emitObjectRemoved = false;

    if (d_data->d_currentUuid == uuid) {

        d_data->d_currentUuid = QUuid();

        emitCurrentUuidChanged = true;

    }

    if (d_data->d_uuidToObjectPointer.contains(uuid))
        emitObjectRemoved = true;

    d_data->d_uuidToObjectPointer.remove(uuid);

    d_data->d_uuidProjectToUuidAnnotatedMatrices.remove(uuid);

    d_data->d_uuidProjectToUuidResults.remove(uuid);

    qsizetype index = d_data->d_uuidProjects.indexOf(uuid);

    if (index != -1) {

        d_data->d_projectLabels.removeAt(index);

        d_data->d_uuidProjects.removeAt(index);

    }

    QHash<QUuid, QList<QUuid>>::iterator it;

    for (it = d_data->d_uuidProjectToUuidAnnotatedMatrices.begin(); it != d_data->d_uuidProjectToUuidAnnotatedMatrices.end(); ++it)
        it.value().removeOne(uuid);

    for (it = d_data->d_uuidProjectToUuidResults.begin(); it != d_data->d_uuidProjectToUuidResults.end(); ++it)
        it.value().removeOne(uuid);

    d_readWriteLock.unlock();

    if (emitCurrentUuidChanged)
        emit currentUuidChanged(d_data->d_currentUuid);

    if (emitObjectRemoved)
        emit this->objectRemoved(uuid);

}

const QUuid &DataRepository::currentUuid() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_currentUuid;

}

QUuid DataRepository::currentUuidProject() const
{

    QReadLocker readLocker(&d_readWriteLock);

    if (d_data->d_uuidProjectToUuidAnnotatedMatrices.contains(d_data->d_currentUuid))
        return d_data->d_currentUuid;

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it = d_data->d_uuidToObjectPointer.find(d_data->d_currentUuid);

    if (it == d_data->d_uuidToObjectPointer.end())
        return QUuid();
    else
        return it.value()->property("uuidProject").toUuid();

}

void DataRepository::setProjectLabel(qsizetype index, const QString &label)
{

    QWriteLocker writeLocker(&d_readWriteLock);

    if (d_data->d_projectLabels.count() <= index)
        return;

    d_data->d_projectLabels[index] = label;

    QUuid uuidProject = d_data->d_uuidProjects.at(index);

    for (const QUuid &uuid : d_data->d_uuidProjectToUuidAnnotatedMatrices.value(uuidProject))
        d_data->d_uuidToObjectPointer[uuid]->setProperty("projectLabel", label);

    for (const QUuid &uuid : d_data->d_uuidProjectToUuidResults.value(uuidProject))
        d_data->d_uuidToObjectPointer[uuid]->setProperty("projectLabel", label);

}

void DataRepository::setCurrentUuid(const QUuid &uuid)
{

    d_readWriteLock.lockForWrite();

    d_data->d_currentUuid = uuid;

    d_readWriteLock.unlock();

    emit this->currentUuidChanged(d_data->d_currentUuid);

}

QUuid DataRepository::uuidProject(const QUuid &uuidObject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QSharedPointer<QObject>>::const_iterator it = d_data->d_uuidToObjectPointer.find(uuidObject);

    if (it == d_data->d_uuidToObjectPointer.end())
        return QUuid();
    else
        return it.value()->property("uuidProject").toUuid();

}

const QList<QUuid> &DataRepository::uuidsAnnotatedMatrix(const QUuid& uuidProject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QList<QUuid>>::const_iterator it = d_data->d_uuidProjectToUuidAnnotatedMatrices.find(uuidProject);

    if (it == d_data->d_uuidProjectToUuidAnnotatedMatrices.end())
        return d_data->d_emptyListOfQUuid;
    else
        return it.value();

}

const QList<QUuid> &DataRepository::uuidsProject() const
{

    QReadLocker readLocker(&d_readWriteLock);

    return d_data->d_uuidProjects;

}

const QList<QUuid> &DataRepository::uuidsResult(const QUuid& uuidProject) const
{

    QReadLocker readLocker(&d_readWriteLock);

    QHash<QUuid, QList<QUuid>>::const_iterator it = d_data->d_uuidProjectToUuidResults.find(uuidProject);

    if (it == d_data->d_uuidProjectToUuidResults.end())
        return d_data->d_emptyListOfQUuid;
    else
        return it.value();

}

DataRepository::~DataRepository()
{

}
